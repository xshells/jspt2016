<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>北京市教师管理服务平台</title>
    <link rel="stylesheet" type="text/css" href="../css/newcss.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
    <script type="text/javascript" src="../easyui/jquery.min.js"></script>
    <script type="text/javascript" src="../easyui/easyloader.js"></script>
    <script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx}/js/refresh.js"></script>
    <script type="text/javascript" src="${ctx}/basicInformation/ValidateUtils.js"></script>
<script type="text/javascript">

		var url;
		//表单提交
		function submit(){
			$("#frm").submit();
		}
		//增加用户 保存功能
		function addUser() {
			
			
			var sexlx=$("input[name='sex']").val();
			var tCNo=$("input[name='tCNo']").val();
			var shenfenlx=$("input[name='shenfen']").val();
			var name=$("input[name='tName']").val();
			var birthday=$("input[name='birthday']").val();

			
			if(shenfenlx == ""){
				tips("#tips","身份证类型不能为空！");
				return;
			}
			if(name == ""){
				tips("#tips","姓名不能为空！");
				return;
			}
			if(tCNo == ""){
				tips("#tips","身份证号不能为空！");
				return;
			}
			if(sexlx == ""){
				tips("#tips","请设置性别！");
				return;
			}
			if(birthday == ""){
				tips("#tips","出生日期不能为空！");
				return;
			}

			 $.ajax({
	 				url:'${ctx}/schoolManager/TeacherAccountManageAction.a?checkAddHm&time='+new Date().getTime(),
	 				data:{shenfen:shenfenlx,tCNo:tCNo},
	 				dataType : "json",
	 				success:function(data){
	 					if(data.isValid==1){
// 							window.location.href = "${ctx}/schoolManager/TeacherAccountManageAction.a?add&shenfen="+shenfenlx+"&sex="+sexlx+"&tCNo="+tCNo+"&tName="+name+"&birthday="+birthday;
// 							$.messager.alert('温馨提示','保存成功','info',function(){
// 					          	window.location.reload();
// 					  	  	});
							
							$('#fm').form('submit',{
						        url: '${ctx}/schoolManager/TeacherAccountManageAction.a?add',
						        onSubmit: function(){
						            return true;
						        },
						        success: function(){
						            $.messager.alert('温馨提示','保存成功','info',function(){
						              	window.location.reload();
						      	  	});
						        }
						    });
							
	 					}else{
	 						$.messager.alert('温馨提示','此证件号码已存在','info');
	 					}
	 				},
	 				error:function(XMLHttpRequest, textStatus, errorThrown){
	 					var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
	 					sessionTimeout(sessionstatus);
	 				}
			 });

		}
		//编辑用户  保存功能
		function edit() {
			var shenfenlxEdit=$("#shenfenlxEdit").combobox('getValue');
			var cardNoEdit=$("#cardNoEdit").val();
			var teacherNameEdit=$("#teacherNameEdit").val();
			var sexlxEdit=$("#sexlxEdit").combobox('getValue');
			var birthdayEdit=$("#birthdayEdit").combobox('getValue');
			var jsIdEdit=$("input[type='hidden'][name='jsId']").val();
			if(shenfenlxEdit == ""){
				tips("#tips","身份证类型不能为空！");
				return;
			}
			if(teacherNameEdit == ""){
				tips("#tips","姓名不能为空！");
				return;
			}
			if(cardNoEdit == ""){
				tips("#tips","身份证号不能为空！");
				return;
			}
			if(sexlxEdit == ""){
				tips("#tips","请设置性别！");
				return;
			}
			if(birthdayEdit == ""){
				tips("#tips","出生日期不能为空！");
				return;
			}


			 $.ajax({
	 				url:'${ctx}/schoolManager/TeacherAccountManageAction.a?checkEditHm&time='+new Date().getTime(),
	 				data:{shenfen:shenfenlxEdit,tCNo:cardNoEdit,jsId:jsIdEdit},
	 				dataType : "json",
	 				success:function(data){
	 					if(data.isValid==1){
// 							window.location.href = "${ctx}/schoolManager/TeacherAccountManageAction.a?edit&jsId="+jsIdEdit+"&shenfen="+shenfenlxEdit+"&sex="+sexlxEdit+"&tCNo="+cardNoEdit+"&tName="+teacherNameEdit+"&birthday="+birthdayEdit;
// 							$.messager.alert('温馨提示','编辑成功','info',function(){
// 					          	window.location.reload();
// 					  	  	});
							
							$('#fmedit').form('submit',{
						        url: '${ctx}/schoolManager/TeacherAccountManageAction.a?edit',
						        onSubmit: function(){
						            return true;
						        },
						        success: function(){
						            $.messager.alert('温馨提示','保存成功','info',function(){
						 
						              	window.location.reload();
						      	  	});
						        }
						    });
	 					}else{
	 						$.messager.alert('温馨提示','此证件号码已存在','info');
	 					}
	 				},
	 				error:function(XMLHttpRequest, textStatus, errorThrown){
	 					var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
	 					sessionTimeout(sessionstatus);
	 				}
			 })		


		}
		
		function newUser(){
			$('#dlg').dialog('open').dialog('setTitle','编辑机构信息');
			$('#fm').form('clear');
			url = 'save_user.php';
		}
		function editUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg').dialog('open').dialog('setTitle','编辑用户');
				$('#fm').form('load',row);
				url = 'update_user.php?id='+row.id;
			}
		}
		function saveUser(){
			$('#fm').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.success){
						$('#dlg').dialog('close');		
						$('#dg').datagrid('reload');	
					} else {
						$.messager.show({
							title: 'Error',
							msg: result.msg
						});
					}
				}
			});
		}
		//删除
		function removeUser(){
			var selected=$("input[type='checkbox'][name='ck']:checked");
			if(selected.length<=0){
				tips("#tips","请选择一条记录！");
				return ;
			}
			$.messager.confirm('温馨提示', '是否确认改为待调出状态？', function(r){
				if(r){
					var frmObj=$("#frm");
					frmObj.attr("action","${ctx}/schoolManager/TeacherAccountManageAction.a?delete");
					frmObj.submit();
					$.messager.alert('温馨提示','修改成功','info',function(){
			          	window.location.reload();
			  	  	});
				}
			});
			
		}
		
		//找回账号
		function retrieveAccount(){
			var selected=$("input[type='checkbox'][name='ck']:checked");
			if(selected.length<=0){
				tips("#tips","请选择一条记录！");
				return ;
			}
			$.messager.confirm('温馨提示', '是否要找回账号？', function(r){
				if(r){
					var frmObj=$("#frm");
					frmObj.attr("action","${ctx}/schoolManager/TeacherAccountManageAction.a?retrieveAccount");
					frmObj.submit();
				}
			});
			
		}
		/* 重置 */
		function reset(){
			$("input[name='tEduIdQuery']").val("");
			$("input[name='tCNoQuery']").val("");
			$("input[name='tNameQuery']").val("");
			$("input[name='tAccountStatusQuery']").val("");
			window.location.href = "${ctx}/schoolManager/TeacherAccountManageAction.a";
		}
		
		function downloadTmp(){
			var frmObj=$("#frm");
			frmObj.attr("action","${ctx}/sysuserManager/FileAction.a?getFile&fileName=teacher.xlsx");
			frmObj.submit();
			frmObj.attr("action","${ctx}/schoolManager/TeacherAccountManageAction.a");
		}
	</script>
</head>
<body class="easyui-layout" style="margin:0 1px;background-color:#f5f5f5 !important; margin-top:20px">
		        <div class="easyui-layout" data-options="fit:true">
					<div data-options="region:'center',border:false">
						<form action="${ctx}/schoolManager/TeacherAccountManageAction.a" id="frm" method="post">
                    	<div>
    						<div class="easyui-panel" data-options="region:'center',border:false,collapsible:true" iconCls="icon-dingdan" title="查询条件"  style="margin-bottom:0px; width:100%">
                            	<div id="toolbarxx" style=" border-bottom:1px dotted #ccc">
									<a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="submit()">查 询</a>
									<a href="#" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="reset()">重 置</a>
							    
                                </div>
                                <table id="dg1" class="fitem" data-options="idField:'id',rownumbers:false,fitColumns:true,toolbar:'#toolbarxx',checkOnSelect:'true',selectOnCheck:'true'">
        								<tr>
                                        	<td>教育ID</td>
                                            <th><input type="text" name="tEduIdQuery" class="easyui-textbox" value="${tEduIdQuery }"></th>
                                            <td>证件号码</td>
                                            <th><input type="text" name="tCNoQuery" class="easyui-textbox"  value="${tCNoQuery }"></th>
                                            <td>姓名</td>
                                            <th><input type="text" name="tNameQuery" class="easyui-textbox"  value="${tNameQuery }"></th>
                                        </tr>
<!--                                         <tr class="ml20"> -->
<!--                                             <td>账号状态</td> -->
<%--                                             <td><select class="easyui-combobox"  name="tAccountStatusQuery" value="${tAccountStatusQuery }" id="tAccountStatusQuery"> --%>
<!--                                                 <option value="">请选择</option> -->
<%--                                                 <option value="0" <c:if test="${tAccountStatusQuery =='0'}">selected='selected'</c:if>>未删除</option> --%>
<%--                                                 <option value="1"  <c:if test="${tAccountStatusQuery =='1'}">selected='selected'</c:if>>已删除</option> --%>
<!--                                             </select></td> -->
<!--                                         </tr> -->
    							</table>
                            
    						</div>
					    </div>
                        


                        
						<div  data-options="region:'center',border:false" style="margin-top:20px;">
                            <table id="dg" title="账户列表" iconCls="icon-dingdan" class="easyui-datagrid"
								data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,collapsible:true,toolbar:'#toolbar',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
							<!---获取数据--->
							<thead>
								<tr>
									<th field="ck" checkbox="true"></th>
									<th data-options="field:'id',width:40">序 号</th>
                                    <th data-options="field:'chachong',width:100">教育ID</th>
									<th data-options="field:'name',width:60">姓 名</th>
									<th data-options="field:'sex',width:130">证件号码</th>
                                    <th data-options="field:'i',width:100">出生日期</th>
<!-- 									<th data-options="field:'imef',width:100">账号状态</th> -->
								</tr>
							</thead>
							<tbody>
								<c:if test="${not empty pageObj.pageElements}">
									<c:forEach items="${pageObj.pageElements}" var="item" varStatus="status">
										<tr>
											<td>${item.jsId}</td>
											<td>${item.jsId}</td>
											<td>${item.educationId}</td>
											<td><a href="#" class="easyui-linkbutton"  plain="true" onclick="chakan(${item.jsId})" style="color:blue">${item.teacherName}</a></td>
											<td>${item.cardNo}</td>
											<td>${item.birthday}</td>
<!-- 											<td> -->
<%-- 											<c:choose> --%>
<%-- 												<c:when test="${item.accountStatus=='0'}"> --%>
<!-- 													未删除 -->
<%-- 												</c:when> --%>
<%-- 												<c:when test="${item.accountStatus=='1'}"> --%>
<!-- 													已删除 -->
<%-- 												</c:when> --%>
<%-- 												<c:otherwise> --%>
<!-- 													账号转态出现异常 -->
<%-- 												</c:otherwise> --%>
<%-- 											</c:choose> --%>
<!-- 											</td> -->
										</tr>
									</c:forEach>
								</c:if>
								<c:if test="${empty pageObj.pageElements}">
									<tr>
										<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
									</tr>
								</c:if>
							</tbody>
							</table>
							<div id="toolbar">
								<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUserZH()">新 增</a>
                                <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">编 辑</a>
                                <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()">待调出</a>
                    <!--             <a href="#" class="easyui-linkbutton" iconCls="icon-xyy-hello" plain="true" onclick="retrieveAccount()">账号找回</a> -->
                                <a href="#" class="easyui-linkbutton" iconCls="icon-daoru" plain="true" onclick="daoru()">导入</a>
                                <a href="#" class="easyui-linkbutton" iconCls="icon-daochu" plain="true" onclick="daochu()">导出</a>
							</div>
							<!-- 分页所必须的 -->
							<div class="page tar mb15">
								<input name="pageNo" value="${pageNo }" type="hidden" />
								<c:if test="${not empty pageObj.pageElements}">
									<jsp:include page="../common/pager-nest.jsp">
										<jsp:param name="toPage" value="1" />
										<jsp:param name="showCount" value="1" />
										<jsp:param name="action" value="doSearch" />
									</jsp:include>
								</c:if>
							</div>
							</form>
							<!-- 新增账户  开始 -->
                            <div id="dlgZH" class="easyui-dialog" title="新增账户" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        <div id="table-content">
                                        <form id="fm" method="post" novalidate>
		                            		<input type="hidden" id="idCardVal" />
	                                        <table class="fitem clean">
	                                            <tbody>
	                                                <tr>
										            <td>身份证件类型：</td>
										            <td>
										                <select class="easyui-combobox" required="true" name="shenfen" id="shenfenlx">
										                	<option value="">请选择</option>
										                	<app:dictselect dictType="JSXX_SFZJLX" />
										                </select>
										            </td>
										            <td>证件号：</td>
										            <td>
										                <input type="text" class="easyui-textbox" name="tCNo" id="idCard" required="true">
										            </td>
										        </tr>
										        <tr>
										            <td>姓名：</td>
										            <td>
										                <input type="text" class="easyui-textbox" name="tName" id="teacherName" required="true">
										            </td>
										            <td>性别：</td>
										            <td>
										                <select class="easyui-combobox" required="true" name="sex" id="sexlx">
										                	<option value="">请选择</option>
	                                                		<app:dictselect dictType="JSXX_XB" />
										                </select>
										            </td>
										        </tr>
										        <tr>
										             <td>出生日期：</td>
										            <td>
										                <input type="text" class="easyui-datebox" name="birthday" required id="birthday">
										            </td>
										        </tr>
	                                            </tbody>
	                                        </table>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="addUser()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgZH').dialog('close')">取消</a>
                                </div>
                                <!-- 新增账户  结束 -->
                                <!-- 编辑 页面-->
                            	<div id="edit" class="easyui-dialog" title="编辑账户" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        <div id="table-content">
                                        <form id="fmedit" method="post" novalidate>
		                            		<input type="hidden" id="idCardVal" />
	                                        <table class="fitem clean">
	                                            <tbody>
	                                                <tr>
										            <td>身份证件类型：</td>
										            <td>
										                <select class="easyui-combobox" required="true" name="shenfen" id="shenfenlxEdit">
										                	<option value="">请选择</option>
										                	<app:dictselect dictType="JSXX_SFZJLX" />
										                </select>
										            </td>
										            <td>证件号：</td>
										            <td>
										                <input type="text" class="easyui-textbox" name="tCNo" required id="cardNoEdit" >
										            </td>
										        </tr>
										        <tr>
										            <td>姓名：</td>
										            <td>
										                <input type="text" class="easyui-textbox" name="tName" required id="teacherNameEdit">
										            </td>
										            <td>性别：</td>
										            <td>
										                <select class="easyui-combobox" required="true" name="sex" id="sexlxEdit">
										                	<option value="">请选择</option>
		                                                	<app:dictselect dictType="JSXX_XB" />
										                </select>
										            </td>
										        </tr>
										        <tr>
										             <td>出生日期：</td>
										            <td>
										                <input type="text" class="easyui-datebox" name="birthday" required id="birthdayEdit" >
										            </td>
										        </tr>
	                                            </tbody>
	                                        </table>
	                                        <input type="hidden" name="jsId" id="jsIdEdit" />
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="edit()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="$('#edit').dialog('close')">取消</a>
                                </div>
                                <!-- 编辑 页面 结束-->

                                
                                <!-- 弹框展示开始 -->
                                <div id="chakan" class="easyui-dialog" title="查看账户" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" data-options="modal:true">
                                    <div class="right_table">
                                        <div id="table-content">
                                        <table class="fitem clean">
                                            <tbody>
                                                <tr>
									            <td>身份证件类型：</td>
									            <td>
									                <select class="easyui-combobox" required="true" name="shenfenFind" id="shenfenFind" disabled="disabled">
									                	<option value="">请选择</option>
									                	<app:dictselect dictType="JSXX_SFZJLX" />
									                </select>
									            </td>
									            <td>证件号：</td>
									            <td>
									                <input type="text" class="easyui-textbox" name="cardNoFind" id="cardNoFind" required disabled="disabled">
									            </td>
									        </tr>
									        <tr>
									            <td>姓名：</td>
									            <td>
									                <input type="text" class="easyui-textbox" name="teacherNameFind" id="teacherNameFind" required disabled="disabled">
									            </td>
									            <td>性别：</td>
									            <td>
									                <select class="easyui-combobox" required="true" name="sexFind" id="sexFind" disabled="disabled">
									                	<option value="">请选择</option>
                                                		<app:dictselect dictType="JSXX_XB" />
									                </select>
									            </td>
									        </tr>
									        <tr>
									             <td>出生日期：</td>
									            <td>
									                <input type="text" class="easyui-datebox" name="birthdayFind" id="birthdayFind" required disabled="disabled">
									            </td>
									        </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- 弹框展示结束 -->

						</div>
						
						        	<!-- 导出页面   开始 -->
						<div id="importdlg" class="easyui-dialog" style="width:350px;height:200px; overflow:auto;padding:10px 20px" closed="true" buttons="#importdlg-buttons">
                               <div>
								<a href="#" class="easyui-linkbutton" iconCls="icon-print" onclick="downloadTmp()">点击下载模版</a>
								</div>
<%--                                <form action="${ctx}/districtManager/InstitutionsImportAction.a" id="importfrm" method="post" enctype="multipart/form-data"> --%>
                                    <form id="importfrm" onsubmit="return false;">
                                    <table class="fitem clean">
                                            <tbody>
                                                <tr>
                                                    <td>选择文件：</td>
                                                    <td>
                                                        <input name="fileImport" id="fileImport" class="easyui-filebox" required="true" style="width:200px;"/>
                                                    </td>
                                            </tbody>
                                        </table>
                                 </form>
							</div>
							<div id="importdlg-buttons">
								<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="importinfo()">导入</a>
								<a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#importdlg').dialog('close')">关闭</a>
							</div>
					</div>
				</div>
			</div>
		</div>
		<div id="tips" class="easyui-dialog" title="温馨提示" style="padding:10px 20px;width:280px;height:120px;line-height:40px;text-align:center;overflow:auto;font-size:20px;" closed="true" data-options="modal:true">请选择一条记录</div>
	</div>  
<script type="text/javascript">

$('#birthday').datebox({ editable:false });
$('#tAccountStatusQuery').combobox({ editable:false });
$('#shenfenlx').combobox({ editable:false });
$('#sexlx').combobox({ editable:false });
$('#birthdayEdit').datebox({ editable:false });
$('#sexlxEdit').combobox({ editable:false });
$('#shenfenlxEdit').combobox({ editable:false });

//页面为空显示提示
$('#dg').datagrid({
	onLoadSuccess:function(data){
		if(data.rows[0].id==undefined){
			$('#datagrid-row-r2-2-0').html(data.rows[0].ck);
			$('.datagrid-btable').css({'text-align':'center','width':'100%','padding':'30px 0'});
		}
	}
});
function tips(dlgNmae,msg){
	$(dlgNmae).html(msg).dialog('open');
};

var url;
function save_user(dlgNmae){
    $(dlgNmae).dialog('open');
    $('#fm').form('clear');
    url = 'save_user.php';
}
//弹出新增框
function newUserZH(){
    save_user('#dlgZH');
    $('#shenfenlx').combobox('setValue',1);
    $('#idCard').textbox('setValue','');
    $('#teacherName').textbox('setValue','');
    $('#sexlx').combobox('setValue','');
    $('#birthday').textbox('setValue','');
}
//弹出编辑框 
function editUser(){
	var selected=$("input[type='checkbox'][name='ck']:checked");
	
	if(selected.length<=0){
		tips("#tips","请选择一条记录");
		return ;
	}else if(selected.length>1){
		tips("#tips","只能编辑一条记录");
		return ;
	}else{	
		var jsId=selected.val();
		$.ajax({
		    url:"${ctx}/schoolManager/TeacherAccountManageAction.a?toEdit&time="+new Date().getTime(),    //请求的url地址
		    dataType:"json",   //返回格式为json
		    //async:true,//请求是否异步，默认为异步，这也是ajax重要特性
		    data:{"jsId":jsId},    //参数值
		    type:"post",   //请求方式
		    success:function(data){
		    	console.log(data);
		        $("#shenfenlxEdit").combobox('setValue',data.tbBizJzgjbxx.shenfen);
		        $("#cardNoEdit").textbox('setValue',data.tbBizJzgjbxx.cardNo);
		        $("#teacherNameEdit").textbox('setValue',data.tbBizJzgjbxx.teacherName);
		        $("#sexlxEdit").combobox('setValue',data.tbBizJzgjbxx.sex);
		        $("#birthdayEdit").datebox('setValue',data.tbBizJzgjbxx.birthday);
		        $("#jsIdEdit").val(data.tbBizJzgjbxx.jsId);
		    },
		    error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
		});
		
    save_user('#edit');
	}
}
/* 弹框展示 */
 function chakan(jsId){
	 $.ajax({
	    url:"${ctx}/schoolManager/TeacherAccountManageAction.a?toEdit&time="+new Date().getTime(),    //请求的url地址
	    dataType:"json",   //返回格式为json
	    //async:true,//请求是否异步，默认为异步，这也是ajax重要特性
	    data:{"jsId":jsId},    //参数值
	    type:"post",   //请求方式
	    success:function(data){
	    	console.log(data);
	        $("#shenfenFind").combobox('setValue',data.tbBizJzgjbxx.shenfen);
	        $("#cardNoFind").textbox('setValue',data.tbBizJzgjbxx.cardNo);
	        $("#teacherNameFind").textbox('setValue',data.tbBizJzgjbxx.teacherName);
	        $("#sexFind").combobox('setValue',data.tbBizJzgjbxx.sex);
	        $("#birthdayFind").datebox('setValue',data.tbBizJzgjbxx.birthday);
	        save_user('#chakan');
	    },
	    error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
	}); 
}
 
 
function addTab(title, url){
	if ($('#tb').tabs('exists', title)){
		$('#tb').tabs('select', title);
	} else {
		var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
		$('#tb').tabs('add',{
			title:title,
			content:content,
			closable:true
		});
	}
}
//关闭所有的tab  
    function closeAll(){  
        var tiles = new Array();  
          var tabs = $('#tb').tabs('tabs');      
          var len =  tabs.length;           
          if(len>0){  
            for(var j=0;j<len;j++){  
                var a = tabs[j].panel('options').title;               
                tiles.push(a);  
            }  
            for(var i=1;i<tiles.length;i++){               
                $('#tb').tabs('close', tiles[i]);  
            }  
          }  
    } 
//关闭当前的tab 
	function removePanel(){
		var tab = $('#tb').tabs('getSelected');
		    if (tab){
			var index = $('#tb').tabs('getTabIndex', tab);
			$('#tb').tabs('close', index);
			}
		}
	
	/* 导入*/
	function daoru(){
		$('#importdlg').dialog('open').dialog('setTitle','导入账户信息');
	}
	
	/** 上传导入**/
	function importinfo(){
		
		var formData = new FormData($("#importfrm")[0]);
		
		var file = $('#fileImport').filebox('getValue');
        if (file == null || file == "") { 
            	$.messager.alert('温馨提示','请选择文件');
            	return;
        }
        var file_typename = file.substring(file.lastIndexOf('.'), file.length);
        if (file_typename == '.xlsx' || file_typename == '.xls'){
        }else{
     	   $.messager.alert('温馨提示','文件类型错误');
     	   return;
        }
        
//         $.messager.progress({ 
// 	        title: '请稍等', 
// 	        msg: '正在导入数据...', 
// 	        text: '.......' 
// 	    });
        
		$.ajax({
    		type : "post",
    		url : "${ctx}/schoolManager/TeacherAccountExImportAction.a?importFile=aaa",
    		dataType : "json",
    		data : formData,
    		async : false,
    		cache: false,
            contentType: false,
            processData: false,
    		success : function(data) {
//     			$.messager.progress('close');
    			$.messager.alert('温馨提示',data.result.msg,'info',function(){
    				if(data.result.status == '2'){
    					downloadFail(data.result.file);
    				}else{
    					window.location.reload();
    				}
			  	});
    		},
    		error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
    	});
		/*
		$('#importfrm').form('submit',{
	           url: "${ctx}/schoolManager/TeacherAccountExImportAction.a",
	           onSubmit: function(){
	        	   var file = $('#fileImport').filebox('getValue');
	               if (file == null || file == "") { 
		               	tips("#tips","请选择文件！");
		               	return false;
	               }
	               var file_typename = file.substring(file.lastIndexOf('.'), file.length);
	               if (file_typename == '.xlsx' || file_typename == '.xls'){
	               		return true;
	               }else{
	            	   tips("#tips","文件类型错误！");
	            	   return false;
	               }
	           },
	           success: function(){
	        	   window.location.reload();
	           }
	       });
		*/
	}
	
	function downloadFail(file){
		var frmObj=$("#frm");
		frmObj.attr("action","${ctx}/sysuserManager/FileAction.a?getFile&fileName="+file);
		frmObj.submit();
		frmObj.attr("action","${ctx}/schoolManager/TeacherAccountManageAction.a");
	}
	
	/* 导出 */
	function daochu(){
		
		$('#frm').form('submit',{
	           url: "${ctx}/schoolManager/TeacherAccountExImportAction.a?export",
	           onSubmit: function(){
	        	   return true;
	           },
	           success: function(){
// 	        	   window.location.reload();
	           }
	       });
		$('#frm').attr("action","${ctx}/schoolManager/TeacherAccountManageAction.a");
	}
	
	/* ///将时间放到页面加载完初始化，避免原方式多次执行的问题 */
	window.onload=function(){  
		$('#shenfenlx').combobox({
	    	onSelect:function(record){
	    		$('#idCardVal').attr('val',record.value);
	    	}
	    });
	    
	    $('#shenfenlx').combobox("setValue","1");
	    
	    $('input[name="tCNo"]').prev().blur(function(){
	    	var cardType=$('#shenfenlx').combobox("getValue");
	    	
	    	if(cardType=='1'){
				if(ValidateUtils.validIdCard($('#idCard').val())==false){
					$.messager.alert('温馨提示','请输入正确的身份证号','info');
					return false;
				}else{
					var birthday = $('#idCard').val().substring(6,14).replace(/(\d{4})(\d{2})(\d{2})/g,'$1-$2-$3');
					$('#birthday').datebox('setValue',birthday);
				}

			}
	    	
	    	
	    	var cardId = $('#idCard').val();
	    	if(cardType == ""){
	    		cardType="1";
	    	}
	    	 $.ajax({
					url:'${ctx}/schoolManager/TeacherAccountManageAction.a?checkAddHm&time='+new Date().getTime(),
					data:{shenfen:cardType,tCNo:cardId},
					dataType : "json",
					success:function(data){
						if(data.isValid==1){
						}else{
							$.messager.alert('温馨提示','输入的证件号已经存在，请执行调入操作！','info');
						}
					},
					error:function(XMLHttpRequest, textStatus, errorThrown){
						var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
						sessionTimeout(sessionstatus);
					}
			 });
	    	 
		});
	}
</script>
</body>
</html>
