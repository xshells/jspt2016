<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>北京市教师管理服务平台</title>
    <link rel="stylesheet" type="text/css" href="../css/newcss.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
    <script type="text/javascript" src="../easyui/jquery.min.js"></script>
    <script type="text/javascript" src="../easyui/easyloader.js"></script>
    <script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx}/js/refresh.js"></script>
	<script type="text/javascript" >
	/* 编辑*/
	function lookUser(tid){
		
		var id=tid;
		$.ajax({
		    url:"${ctx}/schoolManager/InputPInfoManagerAction.a?getTree&time="+new Date().getTime(),    //请求的url地址
		    dataType:"json",
		    data:{tid:id},
		    type:"post",
		    success:function(data){
		    	console.log(data);
		    	$('#tt').tree({
		    		data:data.tree,
		    		onClick:function(node){
		    			addTabD(node.text,window.location.protocol+'//'+window.location.host+node.url);
		    		}
		    	});
		    	$('#tt').tree('collapseAll');
		    	var n = $('#tt').tree('find', data.tree[0].children[0].id);
	          $('#tt').tree('select', n.target);
	          addTabD(n.text,window.location.protocol+'//'+window.location.host+n.url);
		    },
		    error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
		});
		
		$('#dlg').dialog('open').panel("move",{top:0}).dialog('setTitle','查看信息');
		$('#fm').form('clear');
		closeTabs();
	}
	
	function addTabD(title, url){
		if($('.tabs li').size()>19){
			$('#linkTabs').dialog('open');
			return false;
		}
		if ($('#tb').tabs('exists', title)){
			$('#tb').tabs('select', title);
		} else {
			var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
			$('#tb').tabs('add',{
				title:title,
				content:content,
				closable:true
			});
		}
	}
	
	function closeTabs(){  
        var tiles = new Array();  
          var tabs = $('#tb').tabs('tabs');      
          var len =  tabs.length;           
          if(len>0){
     	  
     	   for(var j=0;j<len;j++){  
               var a = tabs[j].panel('options').title;               
               tiles.push(a);  
           }
            for(var i=0;i<tiles.length;i++){               
                $('#tb').tabs('close', tiles[i]);  
            }  
          }  
    }
	
	</script>
</head>
<body class="easyui-layout" style="margin:0 1px;background-color:#f5f5f5 !important;">
	<input id="userType" type="hidden" name="userType" value="${userType }"/> 
    <div data-options="region:'center'" style="background:#fff;border:0;margin-top:20px;">
            
		        <div class="easyui-layout" data-options="fit:true">
					<div data-options="region:'center',border:false">
                    <form action="${ctx}/schoolManager/STeacherInfoAduitAction.a" id="aduitfrm" method="post">
                    	<div id="anchor66">
    						<div class="easyui-panel" data-options="region:'center',border:false,collapsible:true" iconCls="icon-dingdan" title="查询条件"  style=" width:100%;">
                            	<div id="toolbarxx" style=" border-bottom:1px dotted #ccc">
								<a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="submit()">查 询</a>
								<a href="#" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="reset()">重 置</a>
							    
                                </div>
                                <table id="dg1" class=" fitem" data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,toolbar:'#toolbar',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
        								<tr>
                                        	<td>教育ID</td>
                                            <th><input type="text" class="easyui-textbox" name="eduIdQuery" value="${eduIdQuery }"/></th>
                                            <td>证件号码</td>
                                            <th><input type="text" class="easyui-textbox" name="cardNoQuery"  value="${cardNoQuery }"/></th>
                                        </tr>
                                        <tr class="ml20">
                                        	<td>姓名</td>
                                            <th><input type="text" class="easyui-textbox" name="teacherNameQuery" value="${teacherNameQuery }"/></th>
                                            <td>审核状态</td>
                                            <th>
	                                            <select class="easyui-combobox"  name="aduitStutasQuery" id="aduitStutasQuery" >
		                                            <option value="">请选择</option>
		                                            <%-- <app:dictselect dictType="JSXX_SHZT" selectValue="${aduitStutasQuery }"/> --%>
		                                            <appadd:dictselectadd dictType="JSXX_SHZT" schoolType="${ssxd}" selectValue="${aduitStutasQuery }"/>
	                                        	</select>
                                            </th>
                                        </tr>
    							</table>
                            
    						</div>
					    </div>
						<div  data-options="region:'center',border:false" style="margin-top:20px;">
                            <table id="dg" title="审核列表" iconCls="icon-dingdan" class="easyui-datagrid"
								data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,collapsible:true,toolbar:'#toolbar',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
							<!---获取数据--->
							<thead>
								<tr>
									<th field="ck" checkbox="true"></th>
									<th data-options="field:'id',width:40">序 号</th>
                                    <th data-options="field:'shencha',width:100">审核状态</th>
                                    <th data-options="field:'scha',width:100">审核意见</th>
                                    <th data-options="field:'chachong',width:100">教育ID</th>
									<th data-options="field:'name',width:60">姓 名</th>
                                    <th data-options="field:'named',width:60">性 别</th>
									<th data-options="field:'sex',width:130">证件号码</th>
                                    <th data-options="field:'i',width:100">出生日期</th>
									<th data-options="field:'imef',width:100">申请日期</th>
								</tr>
							</thead>
							<tbody>
							<c:if test="${not empty pageObj.pageElements}">
									<c:forEach items="${pageObj.pageElements}" var="item" varStatus="status">
										<tr>
											<td>${item.jsId }</td>
											<td>${item.jsId }</td>
											<%-- <td><app:dictname dictid="JSXX_SHZT${item.aduitStutas }"/></td> --%>
											<td><appadd:dictnameadd dictid="JSXX_SHZT" dictbm="${item.aduitStutas}" schoolType="${ssxd}"/></td>
											<td><a href="#" class="easyui-linkbutton"  plain="true" onclick="chakan(${item.jsId })" style="color:blue">审核意见</a></td>
											<td>${item.eduId }</td>
											<td><a style="color:#19aa8d; text-decoration:none" href="javascript:void(0);" onclick="lookUser(${item.jsId });">${item.teacherName }</a></td>
											<td><app:dictname dictid="JSXX_XB${item.sex }"/></td>
											<td>${item.cardNo }</td>
											<td>${item.birthDay }</td>
											<td>${item.aduitDay }</td>
										</tr>
									</c:forEach>
								</c:if>
								<c:if test="${empty pageObj.pageElements}">
									<tr>
										<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
									</tr>
								</c:if>
							</tbody>
							</table>
							<div id="shyjDlg" class="easyui-dialog" style="width:700px;height:300px; overflow:auto;padding:10px 20px" title="审核意见"  closed="true" data-options="modal:true" >
								
							</div>
							<div id="toolbar">
								<a href="#" class="easyui-linkbutton" iconCls="icon-filter" plain="true" onclick="aduit(1)">审 核</a>
								<a href="#" class="easyui-linkbutton" iconCls="icon-filter" plain="true" onclick="aduit(2)">全部审核</a>
								<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">编 辑</a>
								<a href="#" class="easyui-linkbutton" iconCls="icon-daoru" plain="true" onclick="daoru('1')">导入师德信息</a>
                                <a href="#" class="easyui-linkbutton" iconCls="icon-daoru" plain="true" onclick="daoru('2')">导入考核信息</a>
                                <a href="#" class="easyui-linkbutton" iconCls="icon-daoru" plain="true" onclick="daoru('3')">导入基本待遇信息</a>
							</div>
							<!-- 分页所必须的 -->
							<div class="page tar mb15">
								<input name="pageNo" value="${pageNo }" type="hidden" />
								<c:if test="${not empty pageObj.pageElements}">
									<jsp:include page="../common/pager-nest.jsp">
										<jsp:param name="toPage" value="1" />
										<jsp:param name="showCount" value="1" />
										<jsp:param name="action" value="doSearch" />
									</jsp:include>
								</c:if>
							</div>
							</div>
							</form>
							<!-- 审核页面  开始 -->
						<div id="dlgZH" class="easyui-dialog" title="审核" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        
                                        <div id="table-content">
                                        <form id="fm" action="" method="post" novalidate>
                                        <table class="fitem clean">
                                            <tbody>
                                            <tr>
									            <td>审核结果：</td>
									            <th>
									                <select class="easyui-combobox"  name="isAdopt" id="isAdopt" >
		                                            <app:dictselect dictType="JSXX_SHJG" />
                                        	</select>
									            </th>
									        </tr>
									        <tr>
									            <td>审核意见：</td>
									            <td>
									                <textarea style="width:400px; line-height:24px; padding:6px; text-indent:24px; height:100px" name="aduitOpinion"></textarea>
									            </td>
									        </tr>
                                            </tbody>
                                        </table>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUserAudit()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgZH').dialog('close')">取消</a>
                                </div>
							<!-- 审核页面   结束 -->
							<div id="dlg" class="easyui-dialog" title=" " style="width:100%;height:100%;" closed="true" data-options="modal:true">
                               <div class="easyui-layout" fit="true">
									<div region="west" split="true" style="width:150px;">
										<ul id="tt" class="easyui-tree"></ul>
									</div>
									<div region="center" border="false" border="false">
										<div id="tb" class="easyui-tabs" fit="true">
										</div>
									</div>
									<div id="linkTabs" class="easyui-dialog" closed="true" data-options="modal:true">标签页开启数量已达到最大上限</div>
								</div>
						</div>
							
							        	<!-- 导出页面   开始 -->
						<div id="importdlg" class="easyui-dialog" style="width:350px;height:200px; overflow:auto;padding:10px 20px" closed="true" buttons="#importdlg-buttons">
                               <div>
								<a href="#" class="easyui-linkbutton" iconCls="icon-print" onclick="downloadTmp()">点击下载模版</a>
								</div>
<%--                                <form action="${ctx}/districtManager/InstitutionsImportAction.a" id="importfrm" method="post" enctype="multipart/form-data"> --%>
                               <form id="importfrm" onsubmit="return false;">
                                    <table class="fitem clean">
                                            <tbody>
                                                <tr>
                                                    <td>选择文件：</td>
                                                    <td>
                                                        <input name="fileImport" id="fileImport" class="easyui-filebox" required="true" style="width:200px;"/>
                                                        <input id="impType" type="hidden" value=""/>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    <!-- </form> -->
                                 </form>
							</div>
							<div id="importdlg-buttons">
								<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="importinfo()">导入</a>
								<a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#importdlg').dialog('close')">关闭</a>
							</div>
						
					</div>
				</div>
			</div>
		<div id="tips" class="easyui-dialog" title="温馨提示" style="padding:10px 20px;width:220px;height:120px;line-height:40px;text-align:center;overflow:auto;font-size:20px;" closed="true" data-options="modal:true">请选择一条记录</div>
<script type="text/javascript">
// $("#dlg").panel("move",{top:$(document).scrollTop() + ($(window).height()-250) * 0.5});

</script>
<script type="text/javascript">
var allId = [];
console.log(allId);
$("#aduitStutasQuery").combobox({ editable:false });

setTimeout(function(){
	$('#_easyui_combobox_i2_1,#_easyui_combobox_i2_5,#_easyui_combobox_i2_6').remove();
},1000);
//审核弹框

function aduit(data){
	if(data==1){
		var selected=$("input[type='checkbox'][name='ck']:checked");
		if(selected.length<=0){
			$.messager.alert('温馨提示','请选择一条记录','info');
			return ;
		}
		
	    var ids = "";
		selected.each(function(i){
		    if(this.checked){
		    	ids = ids + this.value + ",";
		    }
		}); 
	  	//判断师德信息
		$.ajax({
		    url:"${ctx}/sysuserManager/SysuserManageAction.a?isSdAndCfArr&time="+new Date().getTime(),    //请求的url地址
		    dataType:"json",
		    data:{jsIdArr:ids},
		    type:"post",
		    success:function(data){
		    	if(data.isOk.status=="1"){
		    		save_user('#dlgZH');
		    	}else{
					$.messager.alert('温馨提示','序号' + data.isOk.msg + '教师的考核信息和师德信息还没有录入，请录入后再审核教师信息！','info');
		    	}
		    },
		    error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
		});
	  
	}else if(data==2){
		var selected=$("input[type='checkbox'][name='ck']:checked");
		if(selected.length<=0){
			$.messager.alert('温馨提示','请选择一条记录','info');
			return ;
		}
		
		$('.datagrid-cell-c2-id').each(function(){
			var id = $(this).html();
			var id = id.match(/\d/g);
			if(id!==null){
				var id = id.join('');
				allId.push(id);
				console.log(allId);
			}
		});
		
		if(allId!='[]'&&allId!=null&&allId.length>0){
			
		}else{
			$.messager.alert('温馨提示','请选择至少一条记录！','info');
			return ;
		}
		
		var idsArr="";
		for(var i=0;i<allId.length;i++){
			idsArr = idsArr + allId[i];
			if(i<allId.length-1){
				idsArr = idsArr + ",";
			}
		}
		
		//判断师德信息
		$.ajax({
		    url:"${ctx}/sysuserManager/SysuserManageAction.a?isSdAndCfArr&time="+new Date().getTime(),    //请求的url地址
		    dataType:"json",
		    data:{jsIdArr:idsArr},
		    type:"post",
		    success:function(data){
		    	if(data.isOk.status=="1"){
		    		save_user('#dlgZH');
		    	}else{
					$.messager.alert('温馨提示','序号' + data.isOk.msg + '教师的考核信息和师德信息还没有录入，请录入后再审核教师信息！','info');
		    	}
		    },
		    error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
		});
	}
}

function tips(dlgNmae,msg){
	$(dlgNmae).html(msg).dialog('open');
};
function addTab(title, url){
	if ($('#tb').tabs('exists', title)){
		$('#tb').tabs('select', title);
	} else {
		var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
		$('#tb').tabs('add',{
			title:title,
			content:content,
			closable:true
		});
	}
}
//关闭所有的tab  
    function closeAll(){  
        var tiles = new Array();  
          var tabs = $('#tb').tabs('tabs');      
          var len =  tabs.length;           
          if(len>0){  
            for(var j=0;j<len;j++){  
                var a = tabs[j].panel('options').title;               
                tiles.push(a);  
            }  
            for(var i=1;i<tiles.length;i++){               
                $('#tb').tabs('close', tiles[i]);  
            }  
          }  
    } 
//关闭当前的tab 
	function removePanel(){
		var tab = $('#tb').tabs('getSelected');
		    if (tab){
			var index = $('#tb').tabs('getTabIndex', tab);
			$('#tb').tabs('close', index);
			}
		}
//表单提交
	function submit(){
		$("#aduitfrm").submit();
	}
	
	Array.prototype.indexOf = function(val) {
		for (var i = 0; i < this.length; i++) {
		if (this[i] == val) return i;
		}
		return -1;
		};
		
		Array.prototype.remove = function(val) {
			var index = this.indexOf(val);
			if (index > -1) {
			this.splice(index, 1);
			}
			};
//获取JSID
	var teatchId=[];
	$('#dg').datagrid({
		onCheck:function(rowIndex,rowData){
			
			if(rowData.shencha=='学校（机构）审核通过'){
				$('#dg').datagrid('unselectRow',rowIndex);
				$.messager.alert('温馨提示','已审核，无需再次审核！','info');
				return ;
			}else if(rowData.shencha=='学校（机构）审核不通过'){
				$('#dg').datagrid('unselectRow',rowIndex);
				$.messager.alert('温馨提示','已返回用户修改，不能审核此用户！','info');
				return ;
			}
			
			console.log(teatchId);
			//判断师德信息
			$.ajax({
			    url:"${ctx}/sysuserManager/SysuserManageAction.a?isSdAndCf&time="+new Date().getTime(),    //请求的url地址
			    dataType:"json",
			    data:{jsId:rowData.id},
			    type:"post",
			    success:function(data){
			    	if(data.isOk!=null&&data.isOk=="1"){
			    		teatchId.push(rowData.id);
			    	}else{
			    		$('#dg').datagrid('unselectRow',rowIndex);
						$.messager.alert('温馨提示','教师的考核信息和师德信息还没有录入，请录入后再审核教师信息！','info');
			    	}
			    },
			    error:function(XMLHttpRequest, textStatus, errorThrown){
					var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
					sessionTimeout(sessionstatus);
				}
			});
// 			teatchId.push(rowData.id);
		},
		onUncheck:function(rowIndex,rowData){
			teatchId.remove(rowData.id);
			console.log(teatchId);
		},
		onLoadSuccess:function(data){
			if(data.rows[0].id==undefined){
				$('#datagrid-row-r2-2-0').html(data.rows[0].ck);
				$('.datagrid-btable').css({'text-align':'center','width':'100%','padding':'30px 0'});
			}
		}
	})

	var shText;
	$('#isAdopt').combobox({
		onSelect:function(record){
			shText = record.value;
		}
	});
	
	var url;
	//审核保存
	function saveUserAudit(){
		var ck;
		if(teatchId!='[]'&&teatchId!=null&&teatchId.length>0){
			ck=teatchId;
		}else{
			ck=allId;
		}
		
		var result = $("#isAdopt").combobox("getValue");
		if(result == null || result == ""){
			$.messager.alert('温馨提示','请填写审核结果！','info');
			return;
		}
		
		var yjText = $('textarea[name="aduitOpinion"]').val();
		$('#fm').form('submit',{
			url: "${ctx}/schoolManager/STeacherInfoAduitAction.a?aduit&ck="+ck,
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				window.location.href = "${ctx}/schoolManager/STeacherInfoAduitAction.a";
			}
		});
	}
	/* 全部审核 */
	/* function aduitAll(){
		$('#dg').datagrid('checkAll');
		//window.location.href = "${ctx}/schoolManager/STeacherInfoAduitAction.a?aduit&ck="+allId;
	} */
	//查询审核意见
	function chakan(jsId){
		$.ajax({
		    url:"${ctx}/districtManager/TeacherInfoAuditAction.a?queryAduitComment&time="+new Date().getTime(),    //请求的url地址
		    dataType:"json",   //返回格式为json
		    //async:true,//请求是否异步，默认为异步，这也是ajax重要特性
		    data:{"jsId":jsId},    //参数值
		    type:"post",   //请求方式
		    success:function(data){
		    	var str = "<table>";
		    	for(var i=0;i<data.list.length;i++){
		    	/* 	str += "<tr>"+(i+1*1);
		    		str += data.list[i].aduitOpinion; */
		    		if(data.list[i].aduitOpinion==null||data.list[i].aduitOpinion.trim()==""){
		    			str += "<tr>无审核意见!";
		    		}else{
		    		str += "<tr>"+data.list[i].aduitOpinion;
		    		}
		    		str += "</tr>";
		    	}
		    	str+="</tbody></table>";
		    	$("#shyjDlg").html(str);
		       $('#shyjDlg').dialog('open');
		    },
		    error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
		});
		$('#shyjDlg').dialog('open');
	}
	//重置
	function reset(){
		window.location.href = "${ctx}/schoolManager/STeacherInfoAduitAction.a";
	}
	function save_user(dlgNmae){
	    $(dlgNmae).dialog('open');
	    $('#fm').form('clear');
	}
	
	
	/* 编辑*/
	function editUser(){
		var selected=$("input[type='checkbox'][name='ck']:checked");
		
		if(selected.length<=0){
			$.messager.alert('温馨提示','请选择一条记录','info');
			return ;
		}else if(selected.length>1){
			$.messager.alert('温馨提示','只能编辑一条记录','info');
			return ;
		}
		
		var id = selected.val();
		
		$.ajax({
		    url:"${ctx}/schoolManager/InputPInfoManagerAction.a?getTree&time="+new Date().getTime(),    //请求的url地址
		    dataType:"json",
		    data:{tid:id},
		    type:"post",
		    success:function(data){
		    	console.log(data);
		    	$('#tt').tree({
		    		data:data.tree,
		    		onClick:function(node){
		    			//TODO 验证基本信息是否完善
		    		$.ajax({
		    			    url:"${ctx}/schoolManager/InputPInfoManagerAction.a?checkTeacher&time="+new Date().getTime(),    //请求的url地址
		    			    dataType:"json",
		    			    data:{infoid:node.id},
		    			    type:"post",
		    			    success:function(dat){
		    			    	if(dat.open == "1"){
		    			    		addTabD(node.text,window.location.protocol+'//'+window.location.host+node.url);
		    			    	}else{
			    			    		$.messager.alert('温馨提示','请先完善基本信息！','info',function(){
			    			    			var n = $('#tt').tree('find', data.tree[0].children[0].id);
							         		 $('#tt').tree('select', n.target);
							          		addTabD(n.text,window.location.protocol+'//'+window.location.host+n.url);
			    			    		})
		    			    		}
		    			    },
		    			    error:function(){}
		    			}); 
		    		}
		    	});
		    	$('#tt').tree('collapseAll');
		    	var n = $('#tt').tree('find', data.tree[0].children[0].id);
	          $('#tt').tree('select', n.target);
	          addTabD(n.text,window.location.protocol+'//'+window.location.host+n.url);
		    },
		    error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
		});
		
		$('#dlg').dialog('open').panel("move",{top:0}).dialog('setTitle','编辑信息');
		$('#fm').form('clear');
		closeTabs();
	}
	
	function closeTabs(){  
        var tiles = new Array();  
          var tabs = $('#tb').tabs('tabs');      
          var len =  tabs.length;           
          if(len>0){
     	  
     	   for(var j=0;j<len;j++){  
               var a = tabs[j].panel('options').title;               
               tiles.push(a);  
           }
            for(var i=0;i<tiles.length;i++){               
                $('#tb').tabs('close', tiles[i]);  
            }  
          }  
    }
	
	function addTabD(title, url){
		if($('.tabs li').size()>19){
			$('#linkTabs').dialog('open');
			return false;
		}
		if ($('#tb').tabs('exists', title)){
			$('#tb').tabs('select', title);
		} else {
			var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
			$('#tb').tabs('add',{
				title:title,
				content:content,
				closable:true
			});
		}
	}
	
	////
	function downloadTmp(){
	var filename = "";
	var type = $("#impType").val();
	
	
	if(type == "0"){
		filename = "teacher.xlsx";
	}else if(type == "1"){
		filename = "shide.xlsx";
	}else if(type == "2"){
		filename = "kaohe.xlsx";
	}else if(type == "3"){
		filename = "daiyu.xlsx";
	}
	
	if(filename == ""){
		$.messager.alert('温馨提示','没有对应模版，请联系系统管理员!','warning');
		return;
	}
	
	var frmObj=$("#aduitfrm");
	frmObj.attr("action","${ctx}/sysuserManager/FileAction.a?getFile&fileName=" + filename);
	frmObj.submit();
	frmObj.attr("action","${ctx}/schoolManager/STeacherInfoAduitAction.a");
}

/* 导入*/
function daoru(type){
	var title = "";
	if(type == "0"){
		title = "导入人员信息";
	}else if(type == "1"){
		title = "导入师德信息";
	}else if(type == "2"){
		title = "导入考核信息";
	}else if(type == "3"){
		title = "导入基本待遇信息";
	}
	
	$('#importdlg').dialog('open').dialog('setTitle',title);
	$("#impType").val(type);
	$("#fileImport").filebox("clear");
}

/** 上传导入**/
function importinfo(){
	var type = $("#impType").val();
	var actionUrl = "";
	if(type == "0"){
		actionUrl = "${ctx}/schoolManager/TeacherAccountExImportAction.a?importFile=aaa";
	}else if(type == "1"){
		actionUrl = "${ctx}/schoolManager/ShiDeInfoImportAction.a?importFile=aaa";
	}else if(type == "2"){
		actionUrl = "${ctx}/schoolManager/YearTestImportAction.a?importFile=aaa";
	}else if(type == "3"){
		actionUrl = "${ctx}/schoolManager/DaiyuImportAction.a?importFile=aaa";
	}
	
	if(actionUrl == ""){
		$.messager.alert('温馨提示','上传路径错误，请联系系统管理员！','warning');
		return;
	}
	
	var formData = new FormData($("#importfrm")[0]);
	
	var file = $('#fileImport').filebox('getValue');
    if (file == null || file == "") { 
        	$.messager.alert('温馨提示','请选择文件');
        	return;
    }
    var file_typename = file.substring(file.lastIndexOf('.'), file.length);
    if (file_typename == '.xlsx' || file_typename == '.xls'){
    }else{
 	   $.messager.alert('温馨提示','文件类型错误');
 	   return;
    }
	
	$.ajax({
		type : "post",
		url : actionUrl,
		dataType : "json",
		data : formData,
		async : false,
		cache: false,
        contentType: false,
        processData: false,
		success : function(data) {
			$.messager.alert('温馨提示',data.result.msg,'info',function(){
				window.location.reload();
			});
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
	});
	
	/*
	$('#importfrm').form('submit',{
           url: actionUrl,
           onSubmit: function(){
        	   var file = $('#fileImport').filebox('getValue');
               if (file == null || file == "") { 
            	   $.messager.alert('温馨提示','请选择文件','warning');
	               	return false;
               }
               var file_typename = file.substring(file.lastIndexOf('.'), file.length);
               if (file_typename == '.xlsx' || file_typename == '.xls'){
               		return true;
               }else{
            	   $.messager.alert('温馨提示','文件类型错误','warning');
            	   return false;
               }
           },
           success: function(){
        	   window.location.reload();
           }
       });
	*/
}

$('#dlg').dialog({
    onClose:function(){
    	window.location.reload();
    }
});
</script>
</body>
</html>
