<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>北京市教师管理服务平台</title>
    <link rel="stylesheet" type="text/css" href="../css/newcss.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
    <script type="text/javascript" src="../easyui/jquery.min.js"></script>
    <script type="text/javascript" src="../easyui/easyloader.js"></script>
    <script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx}/js/refresh.js"></script>
    <link rel="stylesheet" type="text/css" href="../My97DatePicker/skin/WdatePicker.css">
    <script type="text/javascript" src="../My97DatePicker/WdatePicker.js"></script>
	<script type="text/javascript" >
		
	</script>
</head>
<body class="easyui-layout" style="margin:0 1px;background-color:#f5f5f5 !important;">
    <div data-options="region:'center'" style="background:#fff;border:0;margin-top:20px;">
		        <div class="easyui-layout" data-options="fit:true">
					<div data-options="region:'center',border:false">
                    <form id="frm" action="${ctx}/schoolManager/InputPInfoManagerAction.a" method="post">
                    <div id="anchor66">
    						<div class="easyui-panel" data-options="region:'center',border:false,collapsible:true" iconCls="icon-dingdan" title="查询条件" >
                            	<div id="toolbarxx" style=" border-bottom:1px dotted #ccc">
									<a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="submit()">查 询</a>
									<a href="#" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="reset()">重 置</a>
                                </div>
                                <table id="dg1" class=" fitem" data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,toolbar:'#toolbar',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
    								<tr>
                                    	<td>教育ID</td>
                                        <th><input type="text" class="easyui-textbox" name="eduIdQuery" value="${eduIdQuery }"></th>
                                        <td>证件号码</td>
                                        <th><input type="text" class="easyui-textbox" name="cardNOQuery" value="${cardNOQuery }"></th>
                                        <td>姓名</td>
                                        <th><input type="text" class="easyui-textbox"  name="teacherNameQuery" value="${teacherNameQuery }"></th>
                                    </tr>
                                    <tr>
                                    	<td>进本校年月</td>
                                        <th><input type="text" class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM',maxDate:'#F{$dp.$D(\'comeSchoolDay2Query\')}'})" id="comeSchoolDay1Query" name="comeSchoolDay1Query" value="${comeSchoolDay1Query }" style="width:76px;"> - <input type="text" class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM',minDate:'#F{$dp.$D(\'comeSchoolDay1Query\')}'})"   name="comeSchoolDay2Query" id="comeSchoolDay2Query" value="${comeSchoolDay2Query }" style="width:76px;"></th>
                                        <td>教职工来源</td>
                                        <th><select class="easyui-combobox"  name="teacherSourceQuery" id="teacherSourceQuery" >
                                            <option value="">请选择</option>
                                            <appadd:dictselectadd dictType="JSXX_JZGLY" schoolType="${ssxd}" selectValue="${teacherSourceQuery }"/>
                                        </select></th>
                                        <td>教职工类别</td>
                                        <th><select class="easyui-combobox" name="teacherTypeQuery" id="teacherTypeQuery" >
                                            <option value="">请选择</option>
                                            <appadd:dictselectadd dictType="JSXX_JZGLB" schoolType="${ssxd}" selectValue="${teacherTypeQuery }"/>
                                        </select></th>
                                    </tr>
                                    <tr>
                                    	<td>人员状态</td>
                                        <th><select class="easyui-combobox"  name="personStatusQuery" id="personStatusQuery" >
                                            <option value="">请选择</option>
                                            <app:dictselect dictType="JSXX_ZGQK" selectValue="${personStatusQuery }"/>
                                        </select></th>
                                        <td>审核状态</td>
                                        <th><select class="easyui-combobox"  name="reviewStatusQuery" id="reviewStatusQuery" >
                                            <option value="">请选择</option>
                                            <%-- <app:dictselect dictType="JSXX_SHZT" selectValue="${reviewStatusQuery }"/> --%>
                                            <appadd:dictselectadd dictType="JSXX_SHZT" schoolType="${ssxd}" selectValue="${reviewStatusQuery }"/>
                                        </select></th>
                                        <td>是否在编</td>
                                        <th><select class="easyui-combobox"  name="isSeriesQuery" id="isSeriesQuery" >
                                            <option value="">请选择</option>
                                            <app:dictselect dictType="JSXX_SFZB" selectValue="${isSeriesQuery }"/>
                                        </select></th>
                                    </tr>
    							</table>
    						</div>
					    </div>
                        
						<div data-options="region:'center',border:false">
                            <table id="dg" title="新增信息" iconCls="icon-dingdan" class="easyui-datagrid" style="width:100%"
								data-options="idField:'id',rownumbers:false,fitColumns:false,singleSelect:false,collapsible:true,toolbar:'#toolbar',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
							<!--获取数据-->
							<thead>
								<tr>
									<th field="ck" checkbox="true"></th>
									<th data-options="field:'id',width:40">序 号</th>
                                    <th data-options="field:'shencha',width:100">审核状态</th>
                                    <th data-options="field:'chachong',width:100">教育ID</th>
									<th data-options="field:'name',width:100">姓 名</th>
                                    <th data-options="field:'named',width:80">性 别</th>
									<th data-options="field:'num',width:100">国籍/地区</th>
									<th data-options="field:'beizhu',width:100">身份证类别</th>
									<th data-options="field:'timef',width:130">证件号码</th>
                                    <th data-options="field:'i',width:100">出生日期</th>
                                    <th data-options="field:'shecha',width:100">出生地</th>
                                    <th data-options="field:'chahong',width:60">民 族</th>
									<th data-options="field:'nae',width:100">政治面貌</th>
                                    <th data-options="field:'naed',width:100">参加工作年月</th>
									<th data-options="field:'sx',width:100">进本校年月</th>
									<th data-options="field:'nm',width:100">教职工来源</th>
									<th data-options="field:'bizhu',width:100">教职工类别</th>
									<th data-options="field:'timf',width:100">是否在编</th>
									<th data-options="field:'se',width:100">用人形式</th>
									<th data-options="field:'um',width:100">签订合同情况</th>
									<th data-options="field:'eizhu',width:100">人员状态</th>
									<th data-options="field:'imef',width:150">创建时间</th>
								</tr>
							</thead>
							<tbody>
								<c:if test="${not empty pageObj.pageElements}">
									<c:forEach items="${pageObj.pageElements}" var="item" varStatus="status">
										<tr>
										<td>${item.jsId }</td><td>${item.jsId }</td>
										<td><app:dictname dictid="JSXX_SHZT${item.reviewStatus }"/><input type="hidden" id="${item.jsId }" value="${item.reviewStatus }"/></td><td>${item.eduId }</td>
										<td>${item.teacherName }</td><td><app:dictname dictid="JSXX_XB${item.sex }"/></td>
										<td><app:dictname dictid="JSXX_GJDQ${item.country }"/></td>
										<td><app:dictname dictid="JSXX_SFZJLX${item.shenFenType }"/></td><td>${item.cardNO }</td>
										<td>${item.birthday }</td><td>${item.birthdayAdress }</td>
										<td><app:dictname dictid="JSXX_MZ${item.nation }"/></td>
										<td>
										<c:forEach items="${item.politicalLandscape}" var="zzmm">
										<app:dictname dictid="JSXX_ZZMM${zzmm }"/>
										</c:forEach>
										</td>
										<td>${item.beginWorkDay }</td><td>${item.comeSchoolDay }</td>
										<td><appadd:dictnameadd dictid="JSXX_JZGLY" dictbm="${item.teacherSource }" schoolType="${ssxd}"/></td><td><app:dictname dictid="JSXX_JZGLB${item.teacherType }"/></td>
										<td><app:dictname dictid="JSXX_SFZB${item.isSeries }"/></td><td><app:dictname dictid="JSXX_YRXS${item.usePersonType }"/></td>
										<td><app:dictname dictid="JSXX_QDHTQK${item.signContract }"/></td><td><app:dictname dictid="JSXX_ZGQK${item.personStatus }"/></td>
										<td>${item.createTime }</td>
										</tr>
									</c:forEach>
								</c:if>
								<c:if test="${empty pageObj.pageElements}">
									<tr>
										<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
									</tr>
								</c:if>
								
								
							</tbody>
							</table>
							<div id="toolbar">
								<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUser()">新 增</a>
								<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">编 辑</a>
								<!-- <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a> -->
								
								<a href="#" class="easyui-linkbutton" iconCls="icon-filter" plain="true" onclick="submitInfo()">报 送</a>
                                <a href="#" class="easyui-linkbutton" iconCls="icon-filter" plain="true" onclick="submitAll()">全部报送</a>
<!--                                 <a href="#" class="easyui-linkbutton" iconCls="icon-daoru" plain="true" onclick="daoru('0')">导 入</a> -->
                               <c:choose>
                                  <c:when test="${ssxd==10||ssxd==20}">
	                                  <a href="#" class="easyui-linkbutton" iconCls="icon-daoru" plain="true" onclick="daoru('2')">导入考核信息</a>
                                  </c:when>
                                  <c:otherwise>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-daoru" plain="true" onclick="daoru('1')">导入师德信息</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-daoru" plain="true" onclick="daoru('2')">导入考核信息</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-daoru" plain="true" onclick="daoru('3')">导入基本待遇信息</a>
                                  </c:otherwise>
							 </c:choose>							
							</div>
							<!-- 分页所必须的 -->
							<div class="page tar mb15">
								<input name="pageNo" value="${pageNo }" type="hidden" />
								<c:if test="${not empty pageObj.pageElements}">
									<jsp:include page="../common/pager-nest.jsp">
										<jsp:param name="toPage" value="1" />
										<jsp:param name="showCount" value="1" />
										<jsp:param name="action" value="doSearch" />
									</jsp:include>
								</c:if>
							</div>
						</div>
						</form>
						<div id="dlg" class="easyui-dialog" title=" " style="width:100%;height:100%;" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                               <div class="easyui-layout" fit="true">
									<div region="west" split="true" style="width:150px;">
										<ul id="tt" class="easyui-tree"></ul>
									</div>
									<div region="center" border="false" border="false">
										<div id="tb" class="easyui-tabs" fit="true">
										</div>
									</div>
									<div id="linkTabs" class="easyui-dialog" closed="true" data-options="modal:true">标签页开启数量已达到最大上限</div>
								</div>
						</div>
						
						        	<!-- 导出页面   开始 -->
						<div id="importdlg" class="easyui-dialog" style="width:350px;height:200px; overflow:auto;padding:10px 20px" closed="true" buttons="#importdlg-buttons">
                               <div>
								<a href="#" class="easyui-linkbutton" iconCls="icon-print" onclick="downloadTmp()">点击下载模版</a>
								</div>
<%--                                <form action="${ctx}/districtManager/InstitutionsImportAction.a" id="importfrm" method="post" enctype="multipart/form-data"> --%>
                               <form id="importfrm" onsubmit="return false;">
                                    <table class="fitem clean">
                                            <tbody>
                                                <tr>
                                                    <td>选择文件：</td>
                                                    <td>
                                                        <input name="fileImport" id="fileImport" class="easyui-filebox" required="true" style="width:200px;"/>
                                                        <input id="impType" type="hidden" value=""/>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    <!-- </form> -->
                                 </form>
							</div>
							<div id="importdlg-buttons">
								<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="importinfo()">导入</a>
								<a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#importdlg').dialog('close')">关闭</a>
							</div>
					</div>
				</div>
			</div>
		<div id="tips" class="easyui-dialog" title="温馨提示" style="padding:10px 20px;width:300px;height:120px;line-height:40px;text-align:center;overflow:auto;font-size:20px;" closed="true" data-options="modal:true">请选择一条记录</div>
		<div id="tips2" class="easyui-dialog" title="温馨提示" style="padding:10px 20px;width:400px;height:200px;line-height:40px;text-align:center;overflow:auto;font-size:20px;" closed="true" data-options="modal:true">请选择一条记录</div>
<script type="text/javascript">

$("#dlg").panel("move",{top:$(document).scrollTop() + ($(window).height()-250) * 0.5});

$('#teacherSourceQuery').combobox({ editable:false }); //教职工来源
$('#teacherTypeQuery').combobox({ editable:false }); //教职工类别
$('#personStatusQuery').combobox({ editable:false });
$('#reviewStatusQuery').combobox({ editable:false });
$('#isSeriesQuery').combobox({ editable:false });



function downloadTmp(){
	var filename = "";
	var type = $("#impType").val();
	
	if(type == "0"){
		filename = "teacher.xlsx";
	}else if(type == "1"){
		filename = "shide.xlsx";
	}else if(type == "2"){
		filename = "kaohe.xlsx";
	}else if(type == "3"){
		filename = "daiyu.xlsx";
	}
	
	if(filename == ""){
		$.messager.alert('温馨提示','没有对应模版，请联系系统管理员!','warning');
		return;
	}
	
	var frmObj=$("#frm");
	frmObj.attr("action","${ctx}/sysuserManager/FileAction.a?getFile&fileName=" + filename);
	frmObj.submit();
	frmObj.attr("action","${ctx}/schoolManager/InputPInfoManagerAction.a");
}

/* 导入*/
function daoru(type){
	var title = "";
	if(type == "0"){
		title = "导入人员信息";
	}else if(type == "1"){
		title = "导入师德信息";
	}else if(type == "2"){
		title = "导入考核信息";
	}else if(type == "3"){
		title = "导入基本待遇信息";
	}
	$('#importdlg').dialog('open').dialog('setTitle',title);
	$("#impType").val(type);
	$("#fileImport").filebox("clear");
}

/** 上传导入**/
function importinfo(){
	var type = $("#impType").val();
	var actionUrl = "";
	if(type == "0"){
		actionUrl = "${ctx}/schoolManager/TeacherAccountExImportAction.a?importFile=aaa";
	}else if(type == "1"){
		actionUrl = "${ctx}/schoolManager/ShiDeInfoImportAction.a?importFile=aaa";
	}else if(type == "2"){
		actionUrl = "${ctx}/schoolManager/YearTestImportAction.a?importFile=aaa";
	}else if(type == "3"){
		actionUrl = "${ctx}/schoolManager/DaiyuImportAction.a?importFile=aaa";
	}
	
	if(actionUrl == ""){
		$.messager.alert('温馨提示','上传路径错误，请联系系统管理员！','warning');
		return;
	}
	
	var formData = new FormData($("#importfrm")[0]);
	
	var file = $('#fileImport').filebox('getValue');
    if (file == null || file == "") { 
        	$.messager.alert('温馨提示','请选择文件');
        	return;
    }
    var file_typename = file.substring(file.lastIndexOf('.'), file.length);
    if (file_typename == '.xlsx' || file_typename == '.xls'){
    }else{
 	   $.messager.alert('温馨提示','文件类型错误');
 	   return;
    }
	
	$.ajax({
		type : "post",
		url : actionUrl,
		dataType : "json",
		data : formData,
		async : false,
		cache: false,
        contentType: false,
        processData: false,
		success : function(data) {
			$.messager.alert('温馨提示',data.result.msg,'info',function(){
				window.location.reload();
			});
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
	});
	
	/*
	$('#importfrm').form('submit',{
           url: actionUrl,
           onSubmit: function(){
        	   var file = $('#fileImport').filebox('getValue');
               if (file == null || file == "") { 
            	   $.messager.alert('温馨提示','请选择文件','warning');
	               	return false;
               }
               var file_typename = file.substring(file.lastIndexOf('.'), file.length);
               if (file_typename == '.xlsx' || file_typename == '.xls'){
               		return true;
               }else{
            	   $.messager.alert('温馨提示','文件类型错误','warning');
            	   return false;
               }
           },
           success: function(){
        	   window.location.reload();
           }
       });
	*/
}




$('#dlg').dialog({
    onClose:function(){
    	window.location.reload();
    }
});


$('#dg').datagrid({
	onLoadSuccess:function(data){
		if(data.rows[0].id==undefined){
			$('#datagrid-row-r3-2-0').html(data.rows[0].ck);
			$('.datagrid-btable').css({'text-align':'center','width':'100%','padding':'30px 0'});
		}
	}
});

function tips(dlgNmae,msg){
	$(dlgNmae).html(msg).dialog('open');
};
function addTab(title, url){
	if ($('#tb').tabs('exists', title)){
		$('#tb').tabs('select', title);
	} else {
		var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
		$('#tb').tabs('add',{
			title:title,
			content:content,
			closable:true
		});
	}
}

//关闭所有的tab  
    function closeAll(){  
        var tiles = new Array();  
          var tabs = $('#tb').tabs('tabs');      
          var len =  tabs.length;           
          if(len>0){  
            for(var j=0;j<len;j++){  
                var a = tabs[j].panel('options').title;               
                tiles.push(a);  
            }  
            for(var i=1;i<tiles.length;i++){               
                $('#tb').tabs('close', tiles[i]);  
            }  
          }  
    } 
//关闭当前的tab 
	function removePanel(){
		var tab = $('#tb').tabs('getSelected');
		    if (tab){
			var index = $('#tb').tabs('getTabIndex', tab);
			$('#tb').tabs('close', index);
			}
		}
	//所属学段 的change事件
	$('#affiliatedSchool').combobox({
        onSelect:function(record){
        	var affiliatedSchool=$('#affiliatedSchool').combobox('getValue');
        	if(affiliatedSchool=='30'){
        		$('#unitType').combobox("enable");
        	}else{
        		$('#unitType').combobox("disable");
        	}
        	if(affiliatedSchool=='50'){
        		$("#institutionCode").textbox("enable");
        	}else{
        		$("#institutionCode").textbox("disable");
        	}
        }
});
	/* 单位类型 */
	$('#unitType').combobox({
        onSelect:function(record){
        	var affiliatedSchool=$('#affiliatedSchool').combobox('getValue');
        	var unitType=$('#unitType').combobox('getValue');
        	if(affiliatedSchool=='30'&&unitType=='1'){
        		$("#institutionCode").textbox("enable");
        	}else{
        		$("#institutionCode").textbox("disable");
        	}
        }
});
	
	var url;
	//表单提交
	function submit(){
		$("#frm").submit();
	}
	/* 新增 */
	function newUser(){
		$.ajax({
		    url:"${ctx}/schoolManager/InputPInfoManagerAction.a?getTree&time="+new Date().getTime(),    //请求的url地址
		    dataType:"json",
		    data:{},
		    type:"post",
		    success:function(data){
		    	console.log(data);
		    	
		    	$('#tt').tree({
		    		data:data.tree,
		    		onClick:function(node){
		    			
		    			$.ajax({
		    			    url:"${ctx}/schoolManager/InputPInfoManagerAction.a?isHaveTea&time="+new Date().getTime(),    //请求的url地址
		    			    dataType:"json",
		    			    data:{infoid:node.id},
		    			    type:"post",
		    			    success:function(dat){
		    			    	if(dat.open == "1"){
		    			    		addTabD(node.text,window.location.protocol+'//'+window.location.host+node.url);
		    			    	}else{
		    			    				
		    				    	$.messager.alert('温馨提示','请先添加基本信息！','info',function(){
		    				    	 var n = $('#tt').tree('find', data.tree[0].children[0].id);
							         		 $('#tt').tree('select', n.target);
							          		addTabD(n.text,window.location.protocol+'//'+window.location.host+n.url);
			    			    		})  
		    			    	}
		    			    },
		    			    error:function(){}
		    			}); 
		    			
		    		}
		    	});
		    	//$('#tt').tree('collapseAll');
		      var n = $('#tt').tree('find', data.tree[0].children[0].id);
	          $('#tt').tree('select', n.target);
	          addTabD(n.text,window.location.protocol+'//'+window.location.host+n.url);
		    },
		    error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
		});
		
		$('#dlg').dialog('open').dialog('setTitle','新增信息');
		$('#fm').form('clear');
		closeTabs();
	}
	
	function addTabD(title, url){
		if($('.tabs li').size()>19){
			$('#linkTabs').dialog('open');
			return false;
		}
		if ($('#tb').tabs('exists', title)){
			$('#tb').tabs('select', title);
		} else {
			var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
			$('#tb').tabs('add',{
				title:title,
				content:content,
				closable:true
			});
		}
	}
	
	/* 编辑*/
	function editUser(){
		var selected=$("input[type='checkbox'][name='ck']:checked");
		
		if(selected.length<=0){
			$.messager.alert('温馨提示','请选择一条记录','info');
			return ;
		}else if(selected.length>1){
			$.messager.alert('温馨提示','只能编辑一条记录','info');
			return ;
		}
		
		var id = selected.val();
		
		$.ajax({
		    url:"${ctx}/schoolManager/InputPInfoManagerAction.a?getTree&time="+new Date().getTime(),    //请求的url地址
		    dataType:"json",
		    data:{tid:id},
		    type:"post",
		    success:function(data){
		    	console.log(data);
		    	$('#tt').tree({
		    		data:data.tree,
		    		onClick:function(node){
		    			//TODO 验证基本信息是否完善
		    		$.ajax({
		    			    url:"${ctx}/schoolManager/InputPInfoManagerAction.a?checkTeacher&time="+new Date().getTime(),    //请求的url地址
		    			    dataType:"json",
		    			    data:{infoid:node.id},
		    			    type:"post",
		    			    success:function(dat){
		    			    	if(dat.open == "1"){
		    			    		addTabD(node.text,window.location.protocol+'//'+window.location.host+node.url);
		    			    	}else{
			    			    		$.messager.alert('温馨提示','请先完善基本信息！','info',function(){
			    			    			var n = $('#tt').tree('find', data.tree[0].children[0].id);
							         		 $('#tt').tree('select', n.target);
							          		addTabD(n.text,window.location.protocol+'//'+window.location.host+n.url);
			    			    		})
		    			    		}
		    			    },
		    			    error:function(){}
		    			}); 
		    		}
		    	});
		    	//$('#tt').tree('collapseAll');
		    	var n = $('#tt').tree('find', data.tree[0].children[0].id);
	          $('#tt').tree('select', n.target);
	          addTabD(n.text,window.location.protocol+'//'+window.location.host+n.url);
		    },
		    error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
		});
		
		$('#dlg').dialog('open').dialog('setTitle','编辑信息');
		$('#fm').form('clear');
		closeTabs();
	}
	
	function closeTabs(){  
        var tiles = new Array();  
          var tabs = $('#tb').tabs('tabs');      
          var len =  tabs.length;           
          if(len>0){
     	  
     	   for(var j=0;j<len;j++){  
               var a = tabs[j].panel('options').title;               
               tiles.push(a);  
           }
            for(var i=0;i<tiles.length;i++){               
                $('#tb').tabs('close', tiles[i]);  
            }  
          }  
    }
	
	$('#dg').datagrid({
		onCheck:function(rowIndex,rowData){
			rowData.index = rowIndex;
		}
	});
	
	/* 删除 */
	function removeUser(){
		var selected=$("input[type='checkbox'][name='ck']:checked");
		if(selected.length<=0){
			$.messager.alert('温馨提示','请选择一条记录！','info');
			return ;
		}
		
		var ids = "";
		selected.each(function(i){
		    if(this.checked){
		    	ids = ids + this.value + ",";
		    }
		});  
		
		$.ajax({
		    url:"${ctx}/schoolManager/InputPInfoManagerAction.a?shStatus&time="+new Date().getTime(),    //请求的url地址
		    dataType:"json",
		    data:{jsId:ids},
		    type:"post",
		    success:function(dat){
		    	if(dat.result == ""){
		    		$.messager.confirm('温馨提示', '是否要删除？', function(r){
		    			if(r){
		    				var frmObj=$("#frm");
		    				frmObj.attr("action","${ctx}/schoolManager/InputPInfoManagerAction.a?delete");
		    				frmObj.submit();
		    			}
		    		});
		    	}else{
		    		$.messager.alert('温馨提示',dat.result + '不能被删除！','info');
		    	}
		    },
		    error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
		});
		
	}
	
	function  st(value){
		$.ajax({
		    url:"${ctx}/schoolManager/InputPInfoManagerAction.a?shStatus&time="+new Date().getTime(),    //请求的url地址
		    dataType:"json",
		    data:{jsId:value},
		    type:"post",
		    success:function(dat){
		    	
		    },
		    error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
		});
	}
	
	
       
	
	/* 重置 */
	function reset(){
		$("input[name='eduIdQuery']").val("");
		$("input[name='cardNOQuery']").val("");
		$("input[name='teacherNameQuery']").val("");
		$("input[name='comeSchoolDay1Query']").val("");
		$("input[name='teacherSourceQuery']").val("");
		$("input[name='teacherTypeQuery']").val("");
		$("input[name='personStatusQuery']").val("");
		$("input[name='reviewStatusQuery']").val("");
		$("input[name='isSeriesQuery']").val("");
		window.location.href = "${ctx}/schoolManager/InputPInfoManagerAction.a";
	}
	
	   /**
	    *报送报送
		*/
		function submitInfo(){
			var selected=$("input[type='checkbox'][name='ck']:checked");
			if(selected.length<=0){
				$.messager.alert('温馨提示','请选择要报送的记录','warning');
				return ;
			}else{
				
				var strSel = '';
				var can = true;
			     $("input[name='ck']:checked").each(function(index, element) {
			    	 var jsid = $(this).val();
			    	  var shzt = $('#'+jsid).val();
			    	  if(shzt==01||shzt==12||shzt==22){
			            strSel += $(this).val()+",";
			    	  }else{
			    		  can=false;
			    		  return;
			    	  }
			      }); 
			    if(can){ 
			      var arrCk = strSel.substring(0,strSel.length-1);
						$.ajax({
							url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?yxSubmitted&time='+new Date().getTime(),
							data:{jsids:arrCk},
							success:function(data){
								$.messager.alert('温馨提示','报送成功','info',function(){
					            	window.location.reload();
					    	  	});
							},
							error:function(data){
								$.messager.alert('系统提示','具有不能报送的教师  不能进行报送全部报送！','warning');
							}
							
						})  
			    }else{
			    	$.messager.alert('系统提示','具有不能报送的教师  不能进行报送全部报送！','warning');
			    }	
			
			}
	    		 
	    	 }
		  /**
		    *全部报送
			*/
			function submitAll(){
				var selected=$("input[type='checkbox'][name='ck']");
				if(selected.length<=0){
					$.messager.alert('温馨提示','没有报送的记录','warning');
					return ;
				}else{
					   var eduIdQuery = $("input[type='checkbox'][name='eduIdQuery']").val();
					   var cardNOQuery = $("input[type='checkbox'][name='cardNOQuery']").val();
					   var teacherNameQuery = $("input[type='checkbox'][name='teacherNameQuery']").val();
					   var comeSchoolDay1Query = $("input[type='checkbox'][name='comeSchoolDay1Query']").val();
					   var comeSchoolDay2Query = $("input[type='checkbox'][name='comeSchoolDay2Query']").val();
					   var teacherSourceQuery = $("input[type='checkbox'][name='teacherSourceQuery']").val();
					   var teacherTypeQuery = $("input[type='checkbox'][name='teacherTypeQuery']").val();
					   var personStatusQuery = $("input[type='checkbox'][name='personStatusQuery']").val();
					   var reviewStatusQuery = $("input[type='checkbox'][name='reviewStatusQuery']").val();
					   var isSeriesQuery = $("input[type='checkbox'][name='isSeriesQuery']").val();
					 
						$.ajax({
							url:'${ctx}/schoolManager/InputPInfoManagerAction.a?yxSubmittedAll&time='+new Date().getTime(),
							data:{ eduIdQuery:eduIdQuery,
								   cardNOQuery:cardNOQuery,
								   teacherNameQuery:teacherNameQuery,
								   comeSchoolDay1Query:comeSchoolDay1Query,
								   comeSchoolDay2Query:comeSchoolDay2Query,
								   teacherSourceQuery:teacherSourceQuery,
								   teacherTypeQuery:teacherTypeQuery,
								   personStatusQuery:personStatusQuery,
								   reviewStatusQuery:reviewStatusQuery,
								   isSeriesQuery:isSeriesQuery
								   },
							success:function(data){
								  $.messager.alert('温馨提示','报送成功','info',function(){
						            	window.location.reload();
						    	  	});
							},
							error:function(data){
								$.messager.alert('系统提示','具有不能报送的教师  不能进行报送全部报送！','warning');
							}
							
						})  
				}
				
				}
</script>
</body>
</html>
