<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>北京市教师管理服务平台</title>
    <link rel="stylesheet" type="text/css" href="../css/newcss.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
    <script type="text/javascript" src="../easyui/jquery.min.js"></script>
    <script src="../js/echarts/build/dist/echarts-all.js"></script>
    <script src="../js/echarts/build/dist/echarts.js"></script>
    <script type="text/javascript" src="../easyui/easyloader.js"></script>
    <script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx}/js/refresh.js"></script>
    <script type="text/javascript" src="${ctx}/js/upload.js"></script>
    <script type="text/javascript" src="../My97DatePicker/WdatePicker.js"></script>
    <link rel="stylesheet" type="text/css" href="../My97DatePicker/skin/WdatePicker.css">
     <script type="text/javascript" src="../basicInformation/ValidateUtils.js"></script>
     <script type="text/javascript">
    var serverPath = '${ctx}';
    function tishi(value){
		$.messager.alert('系统提示',value,'warning');
	}

    </script>
 </head>   
<body>
 <div id="anchor1">
	<div class="easyui-panel" data-options="region:'center',border:false,collapsible:true" iconCls="icon-dingdan" title="基本信息"  style="margin-bottom:20px; width:100%">
                            	<div id="toolbarxd" style=" border-bottom:1px dotted #ccc; ">
								<a href="#" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="saveUser()">保 存</a>
							    </div>
                               <form id="fm" method="post"  novalidate>
                               <input type="hidden" value="${tbJzgxxsh.shzt}" id="xxshzt"/>
                                <table id="dg" class=" fitem clean" data-options="idField:'id',rownumbers:false,fitColumns:true,pagination:true,singleSelect:false,toolbar:'#toolbar',method:'get',checkOnSelect:'true',selectOnCheck:'true'" style="width:700px;height:300px;" >
                                	<thead></thead>
                                   
                                            <tbody class="fitem">
                                                <tr>
													<td>学校代码：</td>
													   <td  ><input type="text" class="easyui-textbox" name="schoolInfo.institutionId" id="institutionCode"  value="${schoolInfo.institutionId}"  required  disabled="true"/></td>
													    
                                                    <td>单位名称：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="schoolInfo.institutionName" id="institutionName"  value="${schoolInfo.institutionName}"  required />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>组织单位代码：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="schoolInfo.unitCode" id="unitCode"  value="${schoolInfo.unitCode}" />
                                                    </td>
                                                    <td>所在区：</td>                                             
                                                    <td>
	                                          		<select class="easyui-combobox" required="true" name="schoolInfo.districtCode" id="districtCode"  disabled="disabled">
	                                                  <c:forEach items="${disCodes }" var="list">
	                                                  		<option value="${list.disCode }"  >${list.disName }</option>
	                                                  </c:forEach>
	                                               </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                	<td>所属学段：</td>
                                                    <td>
                                                         <select class="easyui-combobox" required="true" name="schoolInfo.affiliatedSchool" id="affiliatedSchool" disabled="disabled">
                                                            <app:dictselect dictType="XXJG_SSXD" selectValue="${schoolInfo.affiliatedSchool}" />
                                                        </select>
                                                    </td>
                                                	<td>单位类别：</td>
                                                    <td>
                                                         <select class="easyui-combobox" required="true" name="schoolInfo.unitType" id="unitType" disabled="disabled">
                                                            <option value="">请选择</option>
                                                            <app:dictselect dictType="XXJG_ZXXFL"  selectValue="${schoolInfo.unitType}"/>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                	<td>地址：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="schoolInfo.institutionAddress" id="institutionAddress" value="${schoolInfo.institutionAddress}" required />
                                                    </td>
                                                    <td>办学类型：</td>
                                                    <td>
                                                         <select class="easyui-combobox" required="true" name="schoolInfo.schoolType" id="schoolType">
                                                            <option value="">请选择</option>
                                                            <appadd:dictselectadd dictType="XXJG_BXLX" schoolType="${schoolInfo.affiliatedSchool}"   selectValue="${schoolInfo.schoolType}"/>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                	<td>城乡类型：</td>
                                                    <td>
                                                         <select class="easyui-combobox" required="true" name="schoolInfo.residentType" id="residentType">
                                                            <option value="">请选择</option>
                                                            <app:dictselect dictType="XXJG_ZDCXLX" selectValue="${schoolInfo.residentType}"/>
                                                        </select>
                                                    </td>
                                                    <td>举办单位：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="schoolInfo.jbdw" id="jbdw"  value="${schoolInfo.jbdw}"  required="true" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                	<td>举办者类型：</td>
                                                    <td>
                                                         <select class="easyui-combobox" required="true" name="schoolInfo.jbzlx" id="jbzlx">
                                                            <option value="">请选择</option>	
                                                            <appadd:dictselectadd dictType="XXJG_JBZLX" schoolType="${schoolInfo.affiliatedSchool}" selectValue="${schoolInfo.jbzlx}"/>
                                                        </select>
                                                    </td>
                                                    <td>联系人：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="schoolInfo.contactPerson" id="contactPerson"  value="${schoolInfo.contactPerson}" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                 <td>联系电话：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="schoolInfo.contactTel" id="contactTel"  value="${schoolInfo.contactTel}" />
                                                    </td>
                                                    <td>邮箱：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="schoolInfo.email" id="email" value="${schoolInfo.email}"  />
                                                    </td>
                                                </tr>
                                            </tbody>
                                 				     <input type="hidden" value="${schoolInfo.institutionId}" name="schoolInfo.institutionId""/>
    							</table>
    							</form>            
    						</div>
    </div>
	<script type="text/javascript">
	function saveUser(){
		var name = $("#institutionName").textbox("getValue");
		$.ajax({
		    url:"${ctx}/schoolManager/SchoolInfoManageAction.a?fetchInstitutionByName",    //请求的url地址
		    dataType:"json",   //返回格式为json
		    //async:true,//请求是否异步，默认为异步，这也是ajax重要特性
		    data:{"jgName":name},    //参数值
		    type:"post",   //请求方式
		    success:function(data){
		    	console.log(data);
		   	if(data.ins==null){
		    		saveUser1();
		   	}else{
		    		$.messager.alert('温馨提示','该单位名称已经存在！');
		   	}
		    },
		    error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
		});
	}
	
	/* 新增或者编辑的保存方法 */
	function saveUser1(){
		var contactTel= $("#contactTel").textbox("getValue");
		var email= $("#email").textbox("getValue");
		if(contactTel != null){
			if(!(/^1[34578]\d{9}$/.test(contactTel))){ 
		    	$.messager.alert('温馨提示','请输入正确的联系电话');
				return false;
		    }
		}
	    
		if(email!=null){
			if(ValidateUtils.validEmailAddress($('#email').val())==false){
				$.messager.alert('温馨提示','请输入正确的邮箱');
				return false;
			}
		}
		if(ValidateUtils.validIntegerss($('#unitCode').val())==false){
			$.messager.alert('温馨提示','请输入正确的组织单位代码');
			return false;
		}
		var iname=$("#institutionName").textbox("getValue");
		if(iname==''){
			$.messager.alert('温馨提示','请填写单位名称！');
			return ;
		}
		var disCode=$("#districtCode").combobox("getValue");
		if(disCode==''){
			$.messager.alert('温馨提示','请填写所在区！');
			return ;
		}
		var affiliatedSchool=$('#affiliatedSchool').combobox('getValue');
		var unitType=$('#unitType').combobox('getValue');
		if((affiliatedSchool=='30'&&unitType=='1')||affiliatedSchool=='50'){
			var codes=$("#institutionCode").textbox("getValue");
			if(codes==''){
				$.messager.alert('温馨提示','请填写单位代码！');
				return ;
			}
		}
		if(affiliatedSchool==''){
			$.messager.alert('温馨提示','请填写所属学段！');
			return ;
		}
		if(affiliatedSchool=='30'){
			if(unitType==''){
				$.messager.alert('温馨提示','请填写单位类别！');
				return ;
			}
		}
		
		
		var address=$("#institutionAddress").textbox("getValue");
		if(address==''){
			$.messager.alert('温馨提示','请填写单位地址！');
			return ;
		}
		
		var schoolType=$("#schoolType").combobox("getValue");
		if(schoolType==''){
			$.messager.alert('温馨提示','请填写办学类型！');
			return ;
		}
		
		var residentType=$("#residentType").combobox("getValue");
		if(residentType==''){
			$.messager.alert('温馨提示','请填写城乡类型！');
			return ;
		}
		
		var jbdw=$("#jbdw").textbox("getValue");
		if(jbdw==''){
			$.messager.alert('温馨提示','请填写举办单位！');
			return ;
		}
		
		var jbzlx=$("#jbzlx").combobox("getValue");
		if(jbzlx==''){
			$.messager.alert('温馨提示','请填写举办者类型！');
			return ;
		}
		var code=$("#institutionCode").val();
		$.ajax({
		    url:"${ctx}/schoolManager/SchoolInfoManageAction.a?fetchInstitution",
		    dataType:"json", 
		    data:{"institutionCode":code},
		    type:"post",
		    success:function(data){
		    	console.log(data);
		
		    	if(data.ins==null){

						$.messager.alert('温馨提示','编辑成功','info',function(){
			              	window.location.reload();
			      	  	});
						var frmObj=$("#fm");
						frmObj.attr("action","${ctx}/schoolManager/SchoolInfoManageAction.a?edit");
						frmObj.submit();

		    	}else{
		    		$.messager.alert('温馨提示','单位代码已存在，请重新填写！');
		    	}
		    },
		    error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
		});
	}
	</script>
</body>
</html>
