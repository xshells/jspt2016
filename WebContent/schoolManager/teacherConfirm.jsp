<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>北京市教师管理服务平台</title>
    <link rel="stylesheet" type="text/css" href="../css/newcss.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
    <script type="text/javascript" src="../easyui/jquery.min.js"></script>
    <script type="text/javascript" src="../easyui/easyloader.js"></script>
    <script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx}/js/refresh.js"></script>
<script type="text/javascript">
function tips(dlgNmae,msg){
	$(dlgNmae).html(msg).dialog('open');
};

function submit(){
	$("#frm").submit();
}
/* 重置 */
function resetQuery(){
	$("input[name='eduIdQuery']").val("");
	$("input[name='nameQuery']").val("");
	$("input[name='cardNoQuery']").val("");
	$("input[name='outNameQuery']").val("");
	$("select[name='statusQuery']").combobox('setValue',"");
	window.location.href = "${ctx}/schoolManager/TeacherChangeConfirmAction.a";
}

function shenhe(){
	var selected=$("input[type='checkbox'][name='ck']:checked");
	
	if(selected.length<=0){
		tips("#tips","请选择一条记录");
		return ;
	}else if(selected.length>1){
		tips("#tips","只能编辑一条记录");
		return ;
	}
	var userId=selected.val();
	$("#id").textbox('setValue',userId);
    $("#dlgZH").dialog('open');
    $('#fm').form('clear');
}

function subSH(){
	var frmObj=$("#shfm");
	frmObj.attr("action","${ctx}/schoolManager/TeacherChangeConfirmAction.a?sub");
	frmObj.submit();
	$.messager.alert('温馨提示','审核成功','info',function(){
      	window.location.reload();
	  	});
}

function shenheAll(){
	var selected=$("input[type='checkbox'][name='ck']:checked");
	if(selected.length<=0){
		tips("#tips","请选择一条记录");
		return ;
	}
	
	$.messager.confirm('温馨提示', '确认要提交', function(r){
		if(r){
			var frmObj=$("#frm");
			frmObj.attr("action","${ctx}/schoolManager/TeacherChangeConfirmAction.a?subAll");
			frmObj.submit();
			$.messager.alert('温馨提示','审核成功','info',function(){
		      	window.location.reload();
			  	});
		}
	});
}
	</script>
</head>
<body class="easyui-layout" style="margin:0 1px;background-color:#f5f5f5 !important; margin-top:20px">
		        <div class="easyui-layout" data-options="fit:true">
					<div data-options="region:'center',border:false">
					<form action="${ctx}/schoolManager/TeacherChangeConfirmAction.a" id="frm" method="post">
                    	<div id="anchor66">
    						<div class="easyui-panel" data-options="region:'center',border:false,collapsible:true" iconCls="icon-dingdan" title="查询条件"  style="margin-bottom:0px; width:100%">
                            	<div id="toolbarxx" style=" border-bottom:1px dotted #ccc">
								<a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="submit()">查 询</a>
								<a href="#" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="resetQuery()">重 置</a>
							    
                                </div>
                               <table id="dg1" class=" fitem" data-options="idField:'id',rownumbers:false,fitColumns:true,pagination:true,singleSelect:false,toolbar:'#toolbar',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
        								<tr>
                                        	<td>教育ID</td>
                                            <th><input type="text" class="easyui-textbox" name="eduIdQuery" value="${eduIdQuery }"></th>
                                            <td>证件号码</td>
                                            <th><input type="text" class="easyui-textbox" name="cardNoQuery" value="${cardNoQuery }"></th>
                                            <td>姓名</td>
                                            <th><input type="text" class="easyui-textbox" name="nameQuery" value="${nameQuery }"></th>
                                        </tr>
                                        <tr>
                                        	<td>调入学校</td>
                                            <th><input type="text" class="easyui-textbox" name="inNameQuery" value="${inNameQuery }"></th>
                                            <td>审核状态</td>
                                            <th>
                                            	<select class="easyui-combobox"  name="statusQuery" id="statusQuery">
		                                            <option value=" ">请选择</option>
		                                            <app:dictselect dictType="JSDD_SHZT" selectValue="${statusQuery }"/>
		                                        </select>
                                            </th>
                                        </tr>
    							</table>
                            
    						</div>
					    </div>
                        
                        
                        
                        
                        
						<div  data-options="region:'center',border:false" style="margin-top:20px;">
                            <table id="dg" title="审核列表" iconCls="icon-dingdan" class="easyui-datagrid"
								data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,collapsible:true,toolbar:'#toolbar',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
							<!---获取数据--->
							<thead>
								<tr>
									<th field="ck" checkbox="true"></th>
									<th data-options="field:'id',width:40">序 号</th>
                                    <th data-options="field:'shencha',width:100">审核状态</th>
									<th data-options="field:'name',width:60">姓 名</th>
                                    <th data-options="field:'named',width:60">性 别</th>
									<th data-options="field:'sex',width:130">证件号码</th>
									<th data-options="field:'chachong',width:100">教育ID</th>
									<th data-options="field:'imef',width:100">调入学校</th>
                                    <!--<th data-options="field:'sx',width:100">调出方式</th>-->
                                    <th data-options="field:'iff',width:100">调出日期</th>
									<th data-options="field:'ief',width:100">申请日期</th>
								</tr>
							</thead>
							<tbody>
								<c:if test="${not empty pageObj.pageElements}">
									<c:forEach items="${pageObj.pageElements}" var="item" varStatus="status">
										<tr>
											<td>${item.id}</td>
											<td>${item.id}</td>
											<td>${item.status}<app:dictname dictid="JSDD_SHZT${item.status}"/></td>
											<td>${item.name}</td>
											<td><app:dictname dictid="JSXX_XB${item.sex}"/></td></td>
											<td>${item.cardNo}</td>
											<td>${item.eduId}</td>
											<td>${item.inName}</td>
											<td>${item.outTime}</td>
											<td>${item.applyTime}</td>
										</tr>
									</c:forEach>
								</c:if>
								<c:if test="${empty pageObj.pageElements}">
									<tr>
										<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
									</tr>
								</c:if>
							</tbody>
							</table>
							<div id="toolbar">
								<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="shenhe()">审 核</a>
								<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="shenheAll()">全部审核</a>
							</div>
							<!-- 分页所必须的 -->
							<div class="page tar mb15">
								<input id="pageNo" name="pageNo" value="${pageNo }" type="hidden" />
								<c:if test="${not empty pageObj.pageElements}">
									<jsp:include page="../common/pager-nest.jsp">
										<jsp:param name="toPage" value="1" />
										<jsp:param name="showCount" value="1" />
										<jsp:param name="action" value="doSearch" />
									</jsp:include>
								</c:if>
							</div>
							</form>
							<div id="dlgZH" class="easyui-dialog" title="审核" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        
                                        <div id="table-content">
                                        <form id="shfm" method="post" novalidate>
                                        <table class="fitem clean">
                                            <tbody>
                                            <tr>
									            <td>审核结果：</td>
									            <th>
									                <select class="easyui-combobox" required="true" name="statusT" id="statusT">
                                                <option></option>
									                	<option value="2">通过</option>
                                                        <option value="3">驳回</option>
									                </select>
									            </th>
									        </tr>
									        <tr>
									            <td>审核意见：</td>
									            <td>
									                <textarea name="statusContent" id="statusContent" style="width:400px; line-height:24px; padding:6px; text-indent:24px; height:100px"></textarea>
									            </td>
									        </tr>
                                            </tbody>
                                        </table>
                                         <input type="hidden" name="idT" id="id">
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="subSH()">确认</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgZH').dialog('close')">取消</a>
                                </div>
						</div>
					</div>
<script type="text/javascript">

$('#statusQuery').combobox({ editable:false });
$('#statusT').combobox({ editable:false });

function addTab(title, url){
	if ($('#tb').tabs('exists', title)){
		$('#tb').tabs('select', title);
	} else {
		var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
		$('#tb').tabs('add',{
			title:title,
			content:content,
			closable:true
		});
	}
}
//关闭所有的tab  
    function closeAll(){  
        var tiles = new Array();  
          var tabs = $('#tb').tabs('tabs');      
          var len =  tabs.length;           
          if(len>0){  
            for(var j=0;j<len;j++){  
                var a = tabs[j].panel('options').title;               
                tiles.push(a);  
            }  
            for(var i=1;i<tiles.length;i++){               
                $('#tb').tabs('close', tiles[i]);  
            }  
          }  
    } 
//关闭当前的tab 
	function removePanel(){
		var tab = $('#tb').tabs('getSelected');
		    if (tab){
			var index = $('#tb').tabs('getTabIndex', tab);
			$('#tb').tabs('close', index);
			}
		}
//表格分页	
var pager = $('#dg').datagrid('getPager');    // get the pager of datagrid
	pager.pagination({
		showPageList:false,
		buttons:[{
			iconCls:'icon-search',
			handler:function(){
				alert('search');
			}
		},{
			iconCls:'icon-add',
			handler:function(){
				alert('add');
			}
		},{
			iconCls:'icon-edit',
			handler:function(){
				alert('edit');
			}
		}],
		onBeforeRefresh:function(){
			alert('before refresh');
			return true;
		}
	});		
</script>
</body>
</html>