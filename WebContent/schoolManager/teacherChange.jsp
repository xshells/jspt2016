<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>北京市教师管理服务平台</title>
    <link rel="stylesheet" type="text/css" href="../css/newcss.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
    <script type="text/javascript" src="../easyui/jquery.min.js"></script>
    <script type="text/javascript" src="../easyui/easyloader.js"></script>
    <script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx}/js/refresh.js"></script>
<script type="text/javascript">
function tips(dlgNmae,msg){
	$(dlgNmae).html(msg).dialog('open');
};

function submit(){
	$("#frm").submit();
}
/* 重置 */
function resetQuery(){
	$("input[name='eduIdQuery']").val("");
	$("input[name='nameQuery']").val("");
	$("input[name='cardNoQuery']").val("");
	$("input[name='outNameQuery']").val("");
	$("select[name='statusQuery']").combobox('setValue',"");
	window.location.href = "${ctx}/schoolManager/TeacherChangeAction.a";
}

/* 新增或者编辑的保存方法 */
function saveUser(){
	
	if(!yanzheng()){
		return;
	}
	
    var changeId=$("#id").val();
    var jsid = $('#tid').val();
	$.ajax({
		url:'${ctx}/schoolManager/TeacherChangeAction.a?getShzt&time='+new Date().getTime(),
		data:{jsid:jsid},
		dataType : "json",
		success:function(data){
			var ssxd = $('#ssxd').val();
			var flag;
			if(ssxd==10||ssxd==20){
				flag=(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22||data.tbJzgxxsh.shzt==11);
			}else{
				flag=(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22||data.tbJzgxxsh.shzt==21);
			}
			if(flag){
					if(changeId!=null&&changeId>0){
						var frmObj=$("#addfrm");
						frmObj.attr("action","${ctx}/schoolManager/TeacherChangeAction.a?edit");
						frmObj.submit();
						$.messager.alert('温馨提示','编辑成功','info',function(){
				          	window.location.reload();
				  	  	});
					}else{
						var frmObj=$("#addfrm");
						frmObj.attr("action","${ctx}/schoolManager/TeacherChangeAction.a?add");
						frmObj.submit();
						$.messager.alert('温馨提示','添加成功','info',function(){
				          	window.location.reload();
				  	  	});
					} 
			}else{
				$.messager.alert('温馨提示','报送审核中不可调入','warning');	
			}	
		},error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
	});
}

function searchTeacher(value,name){
	if(value == null || value==""){
		$.messager.alert('温馨提示','请输入证件号码！','warning');
		return;
	}
	loadInfo(value);
}

function loadInfo(value){
	var cardNo=value;
	$.ajax({
	    url:"${ctx}/schoolManager/TeacherChangeAction.a?getTeacher&time="+new Date().getTime(),    //请求的url地址
	    dataType:"json",   //返回格式为json
	    data:{"cardNoT":cardNo},    //参数值
	    type:"post",   //请求方式
	    success:function(data){
	    	console.log(data);
	        $("#outName").textbox('setValue',data.tmap.outName);
	        $("#name").textbox('setValue',data.tmap.name);
	        $("#signNo").textbox('setValue',data.tmap.signNo);
	        $("#birth").textbox('setValue',data.tmap.birth);
	        $("#outId").val(data.tmap.jgId);
	        $("#tid").val(data.tmap.tId);
	        $("#ssxd").val(data.tmap.ssxd);
	        $("#sex").combobox('setValue',data.tmap.sex);
	        $("#cardType").combobox('setValue',data.tmap.cardType);
	    },
	    error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
	});
}

/* 弹出编辑框 */
function editUser(){
	var selected=$("input[type='checkbox'][name='ck']:checked");
	
	if(selected.length<=0){
		tips("#tips","请选择一条记录");
		return ;
	}else if(selected.length>1){
		tips("#tips","只能编辑一条记录");
		return ;
	}else{	
		var userId=selected.val();
		$.ajax({
		    url:"${ctx}/schoolManager/TeacherChangeAction.a?toEdit&time="+new Date().getTime(),    //请求的url地址
		    dataType:"json",   //返回格式为json
		    data:{"idT":userId},    //参数值
		    type:"post",   //请求方式
		    success:function(data){
		    	console.log(JSON.stringify(data));
		    	$("#cardNo").textbox('setValue',data.obj.cardNo);
		    	$("#id").val(data.obj.id);
		        $("#name").textbox('setValue',data.obj.name);
		        $("#outId").val(data.obj.outId);
		        $("#tid").val(data.obj.tid);
		        //$("#inTime").textbox('setValue',data.obj.inTime);
		        $("#applyTime").textbox('setValue',data.obj.applyTime);
		        $("#causeStr").text(data.obj.causeStr);
		        loadInfo(data.obj.cardNo);
		    },
		    error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
		});
		var row = $('#dg').datagrid('getSelected');
		if (row){
			$('#dlgDR').dialog('open');
			$('#fm').form('load',row);
		}
	}
}

/* 删除 */
function removeUser(){
	var selected=$("input[type='checkbox'][name='ck']:checked");
	if(selected.length<=0){
		tips("#tips","请选择一条记录");
		return ;
	}
	var node = $('#dg').datagrid('getSelections')
	for(var i=0;i<node.length;i++){
		var isTrueStr = node[i].name;
		var isTrue = isTrueStr.slice(-3,-2);
		if(!isTrue){
			$('#dg').datagrid('unselectRow',node[i].index);
		}
	}
	
	$.messager.confirm('温馨提示', '是否要删除', function(r){
		if(r){
			var frmObj=$("#frm");
			frmObj.attr("action","${ctx}/schoolManager/TeacherChangeAction.a?delete");
			frmObj.submit();
			$.messager.alert('温馨提示','删除成功','info',function(){
              	window.location.reload();
      	  	});
		}
	});
}

function yanzheng(){
	var cardNo = $("#cardNo").textbox('getValue');
	var name = $("#name").textbox('getValue');
	//var inTime = $("#inTime").textbox('getValue');
	var applyTime = $("#applyTime").textbox('getValue');
	var sex = $("#sex").combobox('getValue');
	var causeStr = $("#causeStr").text();
	var outId = $("#outId").val();
	
	if(outId==""){
		$.messager.alert('温馨提示','调出学校不能为空','warning');
		return false;
	}
	
	if(cardNo==""){
		$.messager.alert('温馨提示','证件号码不能为空','warning');
		return false;
	}
	
	if(name==""){
		$.messager.alert('温馨提示','姓名不能为空','warning');
		return false;
	}
	
	/* if(inTime==""){
		$.messager.alert('温馨提示','调入日期为空','warning');
		return false;
	} */
	
	if(applyTime==""){
		$.messager.alert('温馨提示','申请日期不能为空','warning');
		return false;
	}
	
	if(sex==""){
		$.messager.alert('温馨提示','性别不能为空','warning');
		return false;
	}
	
	return true;
}

function setNull(){
	$("#cardNo").textbox('setValue',"");
	$("#id").val("");
    $("#name").textbox('setValue',"");
    $("#outId").val("");
    $("#tid").val("");
    //$("#inTime").textbox('setValue',"");
    var newDate = new Date();
    $("#applyTime").textbox('setValue',newDate.getFullYear()+'-'+(newDate.getMonth()+1)+'-'+newDate.getDate());
    $("#causeStr").text("");
    
    $("#outName").textbox('setValue',"");
    $("#signNo").textbox('setValue',"");
    $("#birth").textbox('setValue',"");
    $("#sex").combobox('setValue',"");
    $("#cardType").combobox('setValue',"");
}

</script>
</head>
<body class="easyui-layout" style="margin:0 1px;background-color:#f5f5f5 !important; margin-top:20px">
		        <div class="easyui-layout" data-options="fit:true">
					<div data-options="region:'center',border:false">
					<form action="${ctx}/schoolManager/TeacherChangeAction.a" id="frm" method="post">
                    	<div id="anchor66">
    						<div class="easyui-panel" data-options="region:'center',border:false,collapsible:true" iconCls="icon-dingdan" title="查询条件"  style="margin-bottom:0px; width:100%">
                            	<div id="toolbarxx" style=" border-bottom:1px dotted #ccc">
								<a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="submit()">查 询</a>
								<a href="#" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="resetQuery()">重 置</a>
							    
                                </div>
                                <table id="dg1" class=" fitem" data-options="idField:'id',rownumbers:false,fitColumns:true,pagination:true,singleSelect:false,toolbar:'#toolbar',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
        								
        								<tr>
                                        	<td>教育ID</td>
                                            <th><input type="text" class="easyui-textbox" name="eduIdQuery" value="${eduIdQuery }">
                                                <input type="hidden" id="ssxd"/>
                                            </th>
                                            <td>证件号码</td>
                                            <th><input type="text" class="easyui-textbox" name="cardNoQuery" value="${cardNoQuery }"></th>
                                        </tr>
                                        <tr>
                                        	<td>姓名</td>
                                            <th><input type="text" class="easyui-textbox" name="nameQuery" value="${nameQuery }"></th>
                                        	<td>调出学校</td>
                                            <th><input type="text" class="easyui-textbox" name="outNameQuery" value="${outNameQuery }"></th>
<!--                                             <td>审核状态</td> -->
<!--                                             <th> -->
<!--                                             	<select class="easyui-combobox"  name="statusQuery" id="statusQuery"> -->
<!-- 		                                            <option value=" ">请选择</option> -->
<%-- 		                                            <app:dictselect dictType="JSDD_SHZT" selectValue="${statusQuery }"/> --%>
<!-- 		                                        </select> -->
<!--                                             </th> -->
                                        </tr>
    							</table>
                            
    						</div>
					    </div>
                        
                        
                        
                        
                        
						<div  data-options="region:'center',border:false" style="margin-top:20px;">
                            <table id="dg" title="申请列表" iconCls="icon-dingdan" class="easyui-datagrid"
								data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,collapsible:true,toolbar:'#toolbar',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
							<!---获取数据--->
							<thead>
								<tr>
									<th field="ck" checkbox="true"></th>
									<th data-options="field:'id',width:40">序 号</th>
<!--                                     <th data-options="field:'shencha',width:100">审核状态</th> -->
                                    <th data-options="field:'chachong',width:100">教育ID</th>
									<th data-options="field:'name',width:60">姓 名</th>
                                    <th data-options="field:'named',width:60">性 别</th>
									<th data-options="field:'sex',width:130">证件号码</th>
									<th data-options="field:'imef',width:100">调出学校</th>
                                  <!--  <th data-options="field:'iff',width:100">调入日期</th> --> 
									<th data-options="field:'ief',width:100">申请日期</th>
								</tr>
							</thead>
							<tbody>
								<c:if test="${not empty pageObj.pageElements}">
									<c:forEach items="${pageObj.pageElements}" var="item" varStatus="status">
										<tr>
											<td>${item.id}</td>
											<td>${item.id}</td>
<%-- 											<td>${item.status}<app:dictname dictid="JSDD_SHZT${item.status}"/></td> --%>
											<td>${item.eduId}</td>
											<td>${item.name}</td>
											<td><app:dictname dictid="JSXX_XB${item.sex}"/></td></td>
											<td>${item.cardNo}</td>
											<td>${item.outName}</td>
											<%-- <td>${item.inTime}</td> --%>
											<td>${item.applyTime}</td>
										</tr>
									</c:forEach>
								</c:if>
								<c:if test="${empty pageObj.pageElements}">
									<tr>
										<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
									</tr>
								</c:if>
							</tbody>
							</table>
							<div id="toolbar">
                                <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUserDR()">新 增</a>
<!-- 								<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">编 辑</a> -->
<!-- 								<a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a> -->
								
								<!--<a href="#" class="easyui-linkbutton" iconCls="icon-filter" plain="true" onclick="addTab('新增用户','add.html')">报送</a>-->
							</div>
							<!-- 分页所必须的 -->
							<div class="page tar mb15">
								<input id="pageNo" name="pageNo" value="${pageNo }" type="hidden" />
								<c:if test="${not empty pageObj.pageElements}">
									<jsp:include page="../common/pager-nest.jsp">
										<jsp:param name="toPage" value="1" />
										<jsp:param name="showCount" value="1" />
										<jsp:param name="action" value="doSearch" />
									</jsp:include>
								</c:if>
							</div>
						</form>
							<div id="dlgDR" class="easyui-dialog" title="新增调入申请" style="width:700px;height:460px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table" >
                                    <span style="color:red;font-size:15px;">提示:请输入身份证号，点击身份证号的查询图标，其他信息系统会自动查询出来。</span>
                                        <div id="table-content" >
                                        <form id="addfrm" method="post" novalidate>
                                        <table>
                                        <tbody>
                                            <tr>
                                                <td>证件号码：</td>
                                                <th>
                                                    <input type="text" class="easyui-searchbox" name="teacherChangeDto.cardNo" id="cardNo" data-options="searcher:searchTeacher" required>
                                                </th>
                                                <td>调出学校：</td>
                                                <th>
                                                    <input type="text" class="easyui-textbox" name="teacherChangeDto.outName" id="outName" readonly="readonly">
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>姓名：</td>
                                                <th>
                                                    <input type="text" class="easyui-textbox" name="teacherChangeDto.name" id="name" required>
                                                </th>
                                                <td>性别：</td>
                                                <th>
                                                    <select class="easyui-combobox"  name="teacherChangeDto.sex" id="sex" required>
		                                            <option value=" ">请选择</option>
		                                            <app:dictselect dictType="JSXX_XB"/>
		                                        	</select>
                                                </th>
                                            </tr>
                                            <tr>
                                                 <td>出生日期：</td>
                                                <th>
                                                    <input type="text" class="easyui-textbox" name="" id="birth" readonly="readonly">
                                                </th>
                                                <td>身份证件类型：</td>
                                                <th>
                                                    <select class="easyui-combobox"  name="" id="cardType" readonly="readonly">
		                                            <option value=" ">请选择</option>
		                                            <app:dictselect dictType="JSXX_SFZJLX"/>
		                                            </select>
                                                </th>
                                                
                                            </tr>
                                            <tr>
                                                <td>申请日期：</td>
                                                <th>
                                                    <input type="text" class="easyui-datebox" name="teacherChangeDto.applyTime" id="applyTime" required >
                                                </th>
                                                <!-- <td>调入日期：</td>
                                                <th>
                                                    <input type="text" class="easyui-datebox" name="teacherChangeDto.inTime" id="inTime" required >
                                                </th> -->
                                                
                                            </tr>
                                            <tr>
                                                <td>教育ID：</td>
                                                <th>
                                                    <input type="text" class="easyui-textbox" name="" id="signNo" readonly="readonly">
                                                </th>
                                                
                                                
                                            </tr>
                                            <tr>
                                                <td>调动原因：</td>
                                                <th colspan="3">
                                                    <textarea name="teacherChangeDto.causeStr" id="causeStr" cols="60" rows="10"></textarea>
                                                </th>
                                                
                                            </tr>
<!--                                             <tr> -->
<!--                                                 <td>附件：</td> -->
<!--                                                 <td> -->
<!--                                                 	<a href="#" class="easyui-linkbutton" iconCls="icon-add" onclick="">添 加</a> -->
<!--                                                     <a href="#" class="easyui-linkbutton" iconCls="icon-jian" onclick="">删 除</a> -->
<!--                                                     <a href="#" class="easyui-linkbutton" iconCls="icon-remove" onclick="">清 空</a> -->
<!--                                                 </td> -->
<!--                                             </tr> -->
                                        </tbody>
                                    </table>
                                    <input type="hidden" name="teacherChangeDto.id"  id="id">
                                    <input type="hidden" name="teacherChangeDto.outId"  id="outId">
                                    <input type="hidden" name="teacherChangeDto.tid"  id="tid">
                                    </form>
                                    </div>
                                    </div>
                                    <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgDR').dialog('close')">取消</a>
                                </div>
						</div>
					</div>
				</div>
			</div>
			<div id="tips" class="easyui-dialog" title="温馨提示" style="padding:10px 20px;width:220px;height:120px;line-height:40px;text-align:center;overflow:auto;font-size:20px;" closed="true" data-options="modal:true">请选择一条记录</div>
<script type="text/javascript">
$('#statusQuery').combobox({ editable:false }); 
//$('#inTime').datebox({ editable:false });
$('#applyTime').datebox({ editable:false });
$('#sex').combobox({ editable:false });
$('#cardType').combobox({ editable:false });


var url;
function save_user(dlgNmae){
    $(dlgNmae).dialog('open');
    $('#fm').form('clear');
    url = 'save_user.php';
}
//新增
function newUserDR(){
	setNull();
    save_user('#dlgDR');
}

function addTab(title, url){
	if ($('#tb').tabs('exists', title)){
		$('#tb').tabs('select', title);
	} else {
		var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
		$('#tb').tabs('add',{
			title:title,
			content:content,
			closable:true
			
		});
	}
}
//关闭所有的tab  
    function closeAll(){  
        var tiles = new Array();  
          var tabs = $('#tb').tabs('tabs');      
          var len =  tabs.length;           
          if(len>0){  
            for(var j=0;j<len;j++){  
                var a = tabs[j].panel('options').title;               
                tiles.push(a);  
            }  
            for(var i=1;i<tiles.length;i++){               
                $('#tb').tabs('close', tiles[i]);  
            }  
          }  
    } 
//关闭当前的tab 
	function removePanel(){
		var tab = $('#tb').tabs('getSelected');
		    if (tab){
			var index = $('#tb').tabs('getTabIndex', tab);
			$('#tb').tabs('close', index);
			}
		}
</script>
</body>
</html>
