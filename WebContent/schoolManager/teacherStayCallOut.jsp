<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>北京市教师管理服务平台</title>
    <link rel="stylesheet" type="text/css" href="../css/newcss.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
    <script type="text/javascript" src="../easyui/jquery.min.js"></script>
    <script type="text/javascript" src="../easyui/easyloader.js"></script>
    <script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx}/js/refresh.js"></script>
    <script type="text/javascript" src="${ctx}/basicInformation/ValidateUtils.js"></script>
<script type="text/javascript">

		var url;
		//表单提交
		function submit(){
			$("#frm").submit();
		}

		function editUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg').dialog('open').dialog('setTitle','编辑用户');
				$('#fm').form('load',row);
				url = 'update_user.php?id='+row.id;
			}
		}


		/* 重置 */
		function reset(){
			$("input[name='tEduIdQuery']").val("");
			$("input[name='tCNoQuery']").val("");
			$("input[name='tNameQuery']").val("");
			$("input[name='tAccountStatusQuery']").val("");
			window.location.href = "${ctx}/schoolManager/TeacherStayCallOutAction.a";
		}
		
	</script>
</head>
<body class="easyui-layout" style="margin:0 1px;background-color:#f5f5f5 !important; margin-top:20px">
		        <div class="easyui-layout" data-options="fit:true">
					<div data-options="region:'center',border:false">
						<form action="${ctx}/schoolManager/TeacherStayCallOutAction.a" id="frm" method="post">
                    	<div>
    						<div class="easyui-panel" data-options="region:'center',border:false,collapsible:true" iconCls="icon-dingdan" title="查询条件"  style="margin-bottom:0px; width:100%">
                            	<div id="toolbarxx" style=" border-bottom:1px dotted #ccc">
									<a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="submit()">查 询</a>
									<a href="#" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="reset()">重 置</a>
							    
                                </div>
                                <table id="dg1" class="fitem" data-options="idField:'id',rownumbers:false,fitColumns:true,toolbar:'#toolbarxx',checkOnSelect:'true',selectOnCheck:'true'">
        								<tr>
                                        	<td>教育ID</td>
                                            <th><input type="text" name="tEduIdQuery" class="easyui-textbox" value="${tEduIdQuery }"></th>
                                            <td>证件号码</td>
                                            <th><input type="text" name="tCNoQuery" class="easyui-textbox"  value="${tCNoQuery }"></th>
                                            <td>姓名</td>
                                            <th><input type="text" name="tNameQuery" class="easyui-textbox"  value="${tNameQuery }"></th>
                                        </tr>
<!--                                         <tr class="ml20"> -->
<!--                                             <td>账号状态</td> -->
<%--                                             <td><select class="easyui-combobox"  name="tAccountStatusQuery" value="${tAccountStatusQuery }" id="tAccountStatusQuery"> --%>
<!--                                                 <option value="">请选择</option> -->
<%--                                                 <option value="0" <c:if test="${tAccountStatusQuery =='0'}">selected='selected'</c:if>>未删除</option> --%>
<%--                                                 <option value="1"  <c:if test="${tAccountStatusQuery =='1'}">selected='selected'</c:if>>已删除</option> --%>
<!--                                             </select></td> -->
<!--                                         </tr> -->
    							</table>
                            
    						</div>
					    </div>
                        


                        
						<div  data-options="region:'center',border:false" style="margin-top:20px;">
                            <table id="dg" title="账户列表" iconCls="icon-dingdan" class="easyui-datagrid"
								data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,collapsible:true,toolbar:'#toolbar',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
							<!---获取数据--->
							<thead>
								<tr>
									<th field="ck" checkbox="true"></th>
									<th data-options="field:'id',width:40">序 号</th>
                                    <th data-options="field:'chachong',width:100">教育ID</th>
									<th data-options="field:'name',width:60">姓 名</th>
									<th data-options="field:'sex',width:60">性别</th>
									<th data-options="field:'card',width:130">证件号码</th>
                                    <th data-options="field:'i',width:100">出生日期</th>
<!-- 									<th data-options="field:'imef',width:100">账号状态</th> -->
								</tr>
							</thead>
							<tbody>
								<c:if test="${not empty pageObj.pageElements}">
									<c:forEach items="${pageObj.pageElements}" var="item" varStatus="status">
										<tr>
											<td>${item.jsId}</td>
											<td>${item.jsId}</td>
											<td>${item.educationId}</td>
											<td><a href="#" class="easyui-linkbutton"  plain="true" onclick="chakan(${item.jsId})" style="color:blue">${item.teacherName}</a></td>
							   
											<td><app:dictname dictid="JSXX_XB${item.sex}" /></td>
											<td>${item.cardNo}</td>
											<td>${item.birthday}</td>
<!-- 											<td> -->
<%-- 											<c:choose> --%>
<%-- 												<c:when test="${item.accountStatus=='0'}"> --%>
<!-- 													未删除 -->
<%-- 												</c:when> --%>
<%-- 												<c:when test="${item.accountStatus=='1'}"> --%>
<!-- 													已删除 -->
<%-- 												</c:when> --%>
<%-- 												<c:otherwise> --%>
<!-- 													账号转态出现异常 -->
<%-- 												</c:otherwise> --%>
<%-- 											</c:choose> --%>
<!-- 											</td> -->
										</tr>
									</c:forEach>
								</c:if>
								<c:if test="${empty pageObj.pageElements}">
									<tr>
										<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
									</tr>
								</c:if>
							</tbody>
							</table>
							<!-- 分页所必须的 -->
							<div class="page tar mb15">
								<input name="pageNo" value="${pageNo }" type="hidden" />
								<c:if test="${not empty pageObj.pageElements}">
									<jsp:include page="../common/pager-nest.jsp">
										<jsp:param name="toPage" value="1" />
										<jsp:param name="showCount" value="1" />
										<jsp:param name="action" value="doSearch" />
									</jsp:include>
								</c:if>
							</div>

                                <!-- 弹框展示开始 -->
                                <div id="chakan" class="easyui-dialog" title="查看账户" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" data-options="modal:true">
                                    <div class="right_table">
                                        <div id="table-content">
                                        <table class="fitem clean">
                                            <tbody>
                                                <tr>
									            <td>身份证件类型：</td>
									            <td>
									                <select class="easyui-combobox" required="true" name="shenfenFind" id="shenfenFind" disabled="disabled">
									                	<option value="">请选择</option>
									                	<app:dictselect dictType="JSXX_SFZJLX" />
									                </select>
									            </td>
									            <td>证件号：</td>
									            <td>
									                <input type="text" class="easyui-textbox" name="cardNoFind" id="cardNoFind" required disabled="disabled">
									            </td>
									        </tr>
									        <tr>
									            <td>姓名：</td>
									            <td>
									                <input type="text" class="easyui-textbox" name="teacherNameFind" id="teacherNameFind" required disabled="disabled">
									            </td>
									            <td>性别：</td>
									            <td>
									                <select class="easyui-combobox" required="true" name="sexFind" id="sexFind" disabled="disabled">
									                	<option value="">请选择</option>
                                                		<app:dictselect dictType="JSXX_XB" />
									                </select>
									            </td>
									        </tr>
									        <tr>
									             <td>出生日期：</td>
									            <td>
									                <input type="text" class="easyui-datebox" name="birthdayFind" id="birthdayFind" required disabled="disabled">
									            </td>
									        </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- 弹框展示结束 -->

						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div id="tips" class="easyui-dialog" title="温馨提示" style="padding:10px 20px;width:280px;height:120px;line-height:40px;text-align:center;overflow:auto;font-size:20px;" closed="true" data-options="modal:true">请选择一条记录</div>
	</div>  
<script type="text/javascript">

$('#birthday').datebox({ editable:false });
$('#tAccountStatusQuery').combobox({ editable:false });
$('#shenfenlx').combobox({ editable:false });
$('#sexlx').combobox({ editable:false });
$('#birthdayEdit').datebox({ editable:false });
$('#sexlxEdit').combobox({ editable:false });
$('#shenfenlxEdit').combobox({ editable:false });

//页面为空显示提示
$('#dg').datagrid({
	onLoadSuccess:function(data){
		if(data.rows[0].id==undefined){
			$('#datagrid-row-r2-2-0').html(data.rows[0].ck);
			$('.datagrid-btable').css({'text-align':'center','width':'100%','padding':'30px 0'});
		}
	}
});
function tips(dlgNmae,msg){
	$(dlgNmae).html(msg).dialog('open');
};

var url;
function save_user(dlgNmae){
    $(dlgNmae).dialog('open');
    $('#fm').form('clear');
    url = 'save_user.php';
}

/* 弹框展示 */
 function chakan(jsId){
	 $.ajax({
	    url:"${ctx}/schoolManager/TeacherStayCallOutAction.a?toEdit&time="+new Date().getTime(),    //请求的url地址
	    dataType:"json",   //返回格式为json
	    //async:true,//请求是否异步，默认为异步，这也是ajax重要特性
	    data:{"jsId":jsId},    //参数值
	    type:"post",   //请求方式
	    success:function(data){
	    	console.log(data);
	        $("#shenfenFind").combobox('setValue',data.tbBizJzgjbxx.shenfen);
	        $("#cardNoFind").textbox('setValue',data.tbBizJzgjbxx.cardNo);
	        $("#teacherNameFind").textbox('setValue',data.tbBizJzgjbxx.teacherName);
	        $("#sexFind").combobox('setValue',data.tbBizJzgjbxx.sex);
	        $("#birthdayFind").datebox('setValue',data.tbBizJzgjbxx.birthday);
	        save_user('#chakan');
	    },
	    error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
	}); 
}
 
 
function addTab(title, url){
	if ($('#tb').tabs('exists', title)){
		$('#tb').tabs('select', title);
	} else {
		var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
		$('#tb').tabs('add',{
			title:title,
			content:content,
			closable:true
		});
	}
}
//关闭所有的tab  
    function closeAll(){  
        var tiles = new Array();  
          var tabs = $('#tb').tabs('tabs');      
          var len =  tabs.length;           
          if(len>0){  
            for(var j=0;j<len;j++){  
                var a = tabs[j].panel('options').title;               
                tiles.push(a);  
            }  
            for(var i=1;i<tiles.length;i++){               
                $('#tb').tabs('close', tiles[i]);  
            }  
          }  
    } 
//关闭当前的tab 
	function removePanel(){
		var tab = $('#tb').tabs('getSelected');
		    if (tab){
			var index = $('#tb').tabs('getTabIndex', tab);
			$('#tb').tabs('close', index);
			}
		}


	
	/* 导出 */
	function daochu(){
		
		$('#frm').form('submit',{
	           url: "${ctx}/schoolManager/TeacherAccountExImportAction.a?export",
	           onSubmit: function(){
	        	   return true;
	           },
	           success: function(){
// 	        	   window.location.reload();
	           }
	       });
		$('#frm').attr("action","${ctx}/schoolManager/TeacherStayCallOutAction.a");
	}
	
	/* ///将时间放到页面加载完初始化，避免原方式多次执行的问题 */
	window.onload=function(){  
		$('#shenfenlx').combobox({
	    	onSelect:function(record){
	    		$('#idCardVal').attr('val',record.value);
	    	}
	    });
	    
	    $('#shenfenlx').combobox("setValue","1");
	    
	    $('input[name="cardNo"]').prev().blur(function(){
	    	var cardType=$('#shenfenlx').combobox("getValue");
	    	
	    	if(cardType=='1'){
				if(ValidateUtils.validIdCard($('#idCard').val())==false){
					$.messager.alert('温馨提示','请输入正确的身份证号','info');
					return false;
				}else{
					var birthday = $('#idCard').val().substring(6,14).replace(/(\d{4})(\d{2})(\d{2})/g,'$1-$2-$3');
					$('#birthday').datebox('setValue',birthday);
				}

			}
	    	
	    	
	    	var cardId = $('#idCard').val();
	    	if(cardType == ""){
	    		cardType="1";
	    	}
	    	 $.ajax({
					url:'${ctx}/schoolManager/TeacherStayCallOutAction.a?checkAddHm&time='+new Date().getTime(),
					data:{shenfen:cardType,tCNo:cardId},
					dataType : "json",
					success:function(data){
						if(data.isValid==1){
						}else{
							$.messager.alert('温馨提示','输入的证件号已经存在，请执行调入操作！','info');
						}
					},
					error:function(XMLHttpRequest, textStatus, errorThrown){
						var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
						sessionTimeout(sessionstatus);
					}
			 });
	    	 
		});
	}
</script>
</body>
</html>
