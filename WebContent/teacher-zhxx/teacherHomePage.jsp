<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>北京市教师基础信息平台</title>
    <link rel="stylesheet" type="text/css" href="../css/newcss.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
    <script type="text/javascript" src="../easyui/jquery.min.js"></script>
    <script src="../js/echarts/build/dist/echarts-all.js"></script>
    <script src="../js/echarts/build/dist/echarts.js"></script>
    <script type="text/javascript" src="../easyui/easyloader.js"></script>
    <script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
    
	
</head>
<body class="easyui-layout" style="margin:0 1px;background-color:#f5f5f5 !important;">  
    <div data-options="region:'north',split:true" style="height:80px;" id="xyy_header">
	    <div class="easyui-layout" data-options="fit:true">   
            <div style="float:left;width:40%;padding:18px 0 0 15px;">
			    <img style="width:30px; margin-bottom:-12px" src="../img/logo-white.png">
				<a href="#" style="font-size:20px;color:#fff;text-decoration:none;">北京市教师基础信息平台</a>
			</div>
            <div style="float:right;width:56%;">
				<div class="xyy_tool">	
					<ul style="float:right;position: relative;right:0;margin:0;">
						<li><a href="index.html"><i class="xyy-home" style="position: absolute;display:block;width: 16px;height: 16px;top: 50%;margin: -8px 12px;"></i>主 页</a></li>						
						<li><a href="#"><i class="shezhi" style="position: absolute;display:block;width: 16px;height: 16px;top: 50%;margin: -8px 12px;"></i>设 置</a></li>						
						<li><a href="#"><i class="xyy-mima" style="position: absolute;display:block;width: 16px;height: 16px;top: 50%;margin: -8px 12px;"></i>修改密码</a></li>				
						<li><a target="_self" href="../login.html"><i class="xyy-tuichu" style="position: absolute;display:block;width: 16px;height: 16px;top: 50%;margin: -8px 12px;"></i>退出登录</a></li>
					</ul>
				</div>				
			</div>			
        </div>
	</div>   
    <div data-options="region:'south',split:true" style="height:40px;text-align:center;line-height:30px;background:#19aa8d;color:#f5f5f5;">北京市教师管理服务平台（版本：1.2.2.16082516）</div>
    <div data-options="region:'west',title:'中小学教师主菜单',split:true" style="width:200px;">
	    <div id="menu" class="easyui-accordion" fit="true" border="false">
        	<div title="教师个人信息" data-options="iconCls:'icon-shezhi',selected:true" style="overflow:auto;padding:10px;">
                <ul id="tt" class="easyui-tree" data-options="animate:true,lines:true">                
                    <li><span><a href="#anchor1">基本信息*</a></span></li>
                    <li><span><a href="#anchor2">学习经历*</a></span></li>
                    <li><span><a href="#anchor3">工作经历*</a></span></li>  
                    <li><span><a href="#anchor4">岗位聘任*</a></span></li>
                    <li><span><a href="#anchor5">专业技术职务聘任*</a></span></li>
                    <li><span><a href="#anchor50">基本待遇</a></span></li>
                    <li><span><a href="#anchor6">年度考核*</a></span></li>
                    <li><span><a href="#anchor7">教师资格*</a></span></li>
                    
                    <li><span><a href="#anchor70">师德信息</a></span></li>
                    <li><span><a href="#anchor8">教育教学*</a></span></li>  
                    <!--<li><span><a href="#anchor9">教学科研成果及获奖信息</a></span></li>-->
                    <li><span><a href="#anchor10">入选人才项目*</a></span></li>
                    <li><span><a href="#anchor100">国内培训</a></span></li>
                    <li><span><a href="#anchor12">海外研修（访学）*</a></span></li>
                    <li><span><a href="#anchor13">技能及证书*</a></span></li>
                    <li><span><a href="#anchor130">交流轮岗</a></span></li>
                    <li><span><a href="#anchor14">联系方式*</a></span></li>
                </ul>
            </div>
        </div>
	
	</div>  
    <div data-options="region:'center'" style="background:#fff;border:0;">
	    <div id="tb" class="easyui-tabs" data-options="tools:'#tab-tools',fit:true">
			<div title="个人信息维护" iconCls="icon-home" style="padding:15px;">
		        <div class="easyui-layout" data-options="fit:true">
                    
                    
                    <div data-options="region:'center',border:false">
                    	<div id="toolbarxd" style=" border:1px dotted #19aa8d; background:white;margin-bottom:4px; border-radius:4px; width:60px">
                        <a href="#" class="easyui-linkbutton" iconCls="icon-ok" plain="true" onclick="newUser()">报 送</a>
                        </div>
                        

                        <!-- 交流轮岗 -->
                        <%@ include file="/basicInformation/NewFile.jsp"%>

					</div>
				</div>
			</div>
		</div>
	</div>  
<script type="text/javascript">

/*$(function(){
   $('#tt').tree({
        onClick: function(node){
            $("html,body").animate({scrollTop: $("#anchor"+node.domId.substr(13)).offset().top}, 500)
        }
    }); 
})*/
var url;
function save_user(dlgNmae){
    $(dlgNmae).dialog('open');
    $('#fm').form('clear');
    url = 'save_user.php';
}
//新增
function newUserXX(){
    save_user('#dlgXX');
}
function newUserGZ(){
    save_user('#dlgGZ');
}
function newUserDY(){
    save_user('#dlgDY');
}
function newUserGW(){
    save_user('#dlgGW');
}
function newUserZY(){
    save_user('#dlgZY');
}
function newUserND(){
    save_user('#dlgND');
}
function newUserJS(){
    save_user('#dlgJS');
}
function newUserJY(){
    save_user('#dlgJY');
}
function newUserJX(){
    save_user('#dlgJX');
}
function newUserRX(){
    save_user('#dlgRX');
}
function newUserGN(){
    save_user('#dlgGN');
}
function newUserHW(){
    save_user('#dlgHW');
}
function newUserJN(){
    save_user('#dlgJN');
}
function newUserLX(){
    save_user('#dlgLX');
}
function newUserSD(){
    save_user('#dlgSD');
}
function newUserJL(){
    save_user('#dlgJL');
}
//编辑
function editUser(){
    var row = $('#dg').datagrid('getSelected');
    if (row){
        $('#dlg').dialog('open').dialog('setTitle','编辑用户');
        $('#fm').form('load',row);
        url = 'update_user.php?id='+row.id;
    }
}
//删除
function removeUser(){
    var row = $('#dg').datagrid('getSelected');
    if (row){
        $.messager.confirm('Confirm','Are you sure you want to remove this user?',function(r){
            if (r){
                $.post('remove_user.php',{id:row.id},function(result){
                    if (result.success){
                        $('#dg').datagrid('reload');    // reload the user data
                    } else {
                        $.messager.show({   // show error message
                            title: 'Error',
                            msg: result.msg
                        });
                    }
                },'json');
            }
        });
    }
}
//保存
function saveUser(){
    $('#fm').form('submit',{
        url: url,
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            var result = eval('('+result+')');
            if (result.success){
                $('#dlg').dialog('close');      // close the dialog
                $('#dg').datagrid('reload');    // reload the user data
            } else {
                $.messager.show({
                    title: 'Error',
                    msg: result.msg
                });
            }
        }
    });
}
//搜索
function doSearch(){
    $('#search').dialog('open');
}

function addTab(title, url){
	if ($('#tb').tabs('exists', title)){
		$('#tb').tabs('select', title);
	} else {
		var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
		$('#tb').tabs('add',{
			title:title,
			content:content,
			closable:true
		});
	}
}
//关闭所有的tab  
    function closeAll(){  
        var tiles = new Array();  
          var tabs = $('#tb').tabs('tabs');      
          var len =  tabs.length;           
          if(len>0){  
            for(var j=0;j<len;j++){  
                var a = tabs[j].panel('options').title;               
                tiles.push(a);  
            }  
            for(var i=1;i<tiles.length;i++){               
                $('#tb').tabs('close', tiles[i]);  
            }
          }
    } 
//关闭当前的tab 
	function removePanel(){
		var tab = $('#tb').tabs('getSelected');
		    if (tab){
			var index = $('#tb').tabs('getTabIndex', tab);
			$('#tb').tabs('close', index);
			}
		}
//表格分页	
var pager = $('#dg').datagrid('getPager');    // get the pager of datagrid
	pager.pagination({
		showPageList:false,
		buttons:[{
			iconCls:'icon-search',
			handler:function(){
				alert('search');
			}
		},{
			iconCls:'icon-add',
			handler:function(){
				alert('add');
			}
		},{
			iconCls:'icon-edit',
			handler:function(){
				alert('edit');
			}
		}],
		onBeforeRefresh:function(){
			alert('before refresh');
			return true;
		}
	});	
</script>
</body>
</html>