<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=7" /> 
<meta name="copyright" content="北控软件版权所有"/>
<title>教师管理系统入口</title>
<link href="${ctx }/css/becom_global.css" rel="stylesheet" type="text/css" />
<link href="${ctx }/css/becom_login.css" rel="stylesheet" type="text/css" /></link>
<link href="${ctx }/css/becom_library.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="easyui/jquery.min.js"></script>
    <script type="text/javascript" src="easyui/easyloader.js"></script>
    <script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
<!--   <script type="text/javascript" src="js/jquery.min.js"></script> -->
    <script type="text/javascript" src="js/ajaxfileupload.js"></script>
  
<style type="text/css">
	ul{margin: 200px auto;padding: 50px;background: #FFF;border-radius: 5px;border: 1px solid #ccc;width: 500px;list-style-type: none;}
	li{font-size:20px;line-height:36px;}
</style>
<script type="text/javascript">
function upload(){
	
	 var filename = $("#file").val();
	 alert(filename);
	
		
		
		$.ajaxFileUpload({  
	        url : '${ctx}/sysuserManager/FileAction.a',
	        secureuri : false, 
	        fileElementId : 'file',
	        dataType : 'json',//返回值类型 一般设置为json  
	        success : function(data, status) //服务器成功响应处理函数  
	        {  
	            alert(data);//从服务器返回的json中取出message中的数据,其中message为在struts2中定义的成员变量  
	            if (typeof (data.error) != 'undefined') {  
	                if (data.error != '') {  
	                    alert(data.error);  
	                } else {  
	                    alert(data.message);  
	                }  
	            }  
	        },
	        handleError: function( s, xhr, status, e ){
	        	alert(s);
	        },
	        error : function(data, status, e)//服务器响应失败处理函数  
	        {  
	            alert(e);  
	        }  
	    });  
	}
</script>
</head>
<body>
<form action="" enctype="multipart/form-data" method="post">
	<input type="file" name="file" id="file">
<a href="javascript:void(0);" onclick="upload()">上传</a>
</form>

</body>
</html>
