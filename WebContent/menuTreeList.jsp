<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>教师管理信息系统</title>
    <link rel="stylesheet" type="text/css" href="${ctx}/easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/easyui/themes/xyy.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/css/newcss.css">
    <script src="${ctx}/easyui/jquery.min.js" type="text/javascript"></script>
    <script src="${ctx}/easyui/easyloader.js" type="text/javascript"></script>
    <script type="text/javascript" src="${ctx}/easyui/jquery.easyui.min.js"></script>
    <%-- <script src="${ctx}/js/echarts/build/dist/echarts-all.js"></script>
    <script src="${ctx}/js/echarts/build/dist/echarts.js"></script> --%>
    <script type="text/javascript" src="${ctx}/easyui/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx}/js/json2.js"></script>
    <script type="text/javascript" src="${ctx}/js/refresh.js"></script>
    <script src="${ctx}/js/jQuery.md5.js"></script>
	<script type="text/javascript">
		var url;
		function newUser(){
			$('#dlg').dialog('open').dialog('setTitle','统计');
			$('#fm').form('clear');
			url = 'save_user.php';
		}
		function editUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg').dialog('open').dialog('setTitle','编辑用户');
				$('#fm').form('load',row);
				url = 'update_user.php?id='+row.id;
			}
		}
		function saveUser(){
			$('#fm').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.success){
						$('#dlg').dialog('close');		// close the dialog
						$('#dg').datagrid('reload');	// reload the user data
					} else {
						$.messager.show({
							title: 'Error',
							msg: result.msg
						});
					}
				}
			});
		}
		function removeUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$.messager.confirm('Confirm','Are you sure you want to remove this user?',function(r){
					if (r){
						$.post('remove_user.php',{id:row.id},function(result){
							if (result.success){
								$('#dg').datagrid('reload');	// reload the user data
							} else {
								$.messager.show({	// show error message
									title: 'Error',
									msg: result.msg
								});
							}
						},'json');
					}
				});
			}
		}
		

		
	</script>
    
    <!----------------------------------------->
    <!-- <script type="text/javascript">
   
     require.config({
            paths: {
                echarts: 'js/echarts/build/dist'
            }
        });
        
        
//地图和仪表盘 
 	require(
            [
                'echarts',
                'echarts/chart/funnel'
            ],
            function (ec) {
/////////////////////////////////////////option2开始处

var myChart2 = echarts.init(document.getElementById('dg'));
var option2 = {
				    title : {
				        text: '',
				        x:'center'
				    },
				    tooltip : {
				        trigger: 'item',
				        formatter: "{a} <br/>{b} : {c}"
				    },
				    legend: {
				  		orient: 'vertical',
				        data : ['一级教师','二级教师','三级教师','高级教师','特级教师'],
				        x:'left'
				    },
				    calculable : true,
				    series : [
				      
				        {
				            name:'教师职称情况',
				            type:'funnel',
				            sort : 'ascending',	//该属性设置倒金字塔 还是正金字塔
				            label: {
				                normal: {
				                    position: 'inside'
				                },
				                emphasis: {
				                    textStyle: {
				                    fontSize: 20
				                    }
				                }
				            },
				            itemStyle: {
				                normal: {
				                    // color: 各异,
				                    label: {
				                        position: 'inside',	//设定文字位置显示在金字塔图上面
				                        formatter: '{b}:{c}'	//设定在图上显示文字和值
				                    }
				                }
				            },
				            data:[
				                {value:180, name:'一级教师'},
				                {value:80, name:'二级教师'},
				                {value:60, name:'三级教师'},
				                {value:40, name:'高级教师'},
				                {value:20, name:'特级教师'}
				            ]
				        }
				    ]
				};	  
                    
/////////////////////////////////////////option2结束处
       myChart2.setOption(option2, true);
    });  
           
   </script> -->
   <!----------------------------------------->
   
   <!-- <script type="text/javascript">
   
     require.config({
            paths: {
                echarts: 'js/echarts/build/dist'
            }
        });
        
        
//北京地图
 	require(
            [
                'echarts',
                'echarts/chart/pie' // 使用柱状图就加载bar模块，按需加载
            ],
            function (ec) {
            
/////////////////////////////////////////option1开始处
		   var myChart = echarts.init(document.getElementById('dg2'));
		   
		  
		   var option = {
			   title : {
				        text: '',
				        x:'top'
				    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} "
    },
    legend: {
        x : 'center',
        data:['一级教师','二级教师','三级教师','高级教师','特级教师']
    },
    calculable : true,
    series : [
        {
            name:'教师人数',
            type:'pie',
            radius : ['50%', '70%'],
            itemStyle : {
                normal : {
                    label : {
                        show : false
                    },
                    labelLine : {
                        show : false
                    }
                },
                emphasis : {
                    label : {
                        show : true,
                        position : 'center',
                        textStyle : {
                            fontSize : '30',
                            fontWeight : 'bold'
                        }
                    }
                }
            },
            data:[
                {value:120, name:'一级教师'},
                {value:400, name:'二级教师'},
                {value:610, name:'三级教师'},
                {value:800, name:'高级教师'},
                {value:100, name:'特级教师'}
            ]
        }
    ]
};
                    
/////////////////////////////////////////option结束处
		myChart.setOption(option, true);
    });
   </script> -->
   
   
</head>
<body class="easyui-layout" style="margin:0 1px;background-color:#f5f5f5 !important;">  
    <div data-options="region:'north',split:false" style="height:80px;" id="xyy_header">
	    <div class="easyui-layout" data-options="fit:true">   
            <div style="float:left;width:40%;padding:18px 0 0 15px;">
			    <!-- <img style="width:30px; margin-bottom:-12px" src="img/logo-white.png"> -->
				<a href="#" style="font-size:20px;color:#fff;text-decoration:none;">教师管理信息系统</a>
			</div>
            <div style="float:right;width:56%;">
				<div class="xyy_tool">	
					<ul style="float:right;position: relative;right:0;margin:0;">
						<li><a href="javascript:;" onclick="closeAll()"><i class="xyy-home" style="position: absolute;display:block;width: 16px;height: 16px;top: 50%;margin: -8px 12px;"></i>主 页</a></li>						
					<!-- 	<li><a href="#"><i class="shezhi" style="position: absolute;display:block;width: 16px;height: 16px;top: 50%;margin: -8px 12px;"></i>设 置</a></li>-->						
						<c:if test="${userType=='1' }">
							<li><a href="javascript:void(0);" onclick="setPwd()"><i class="xyy-mima" style="position: absolute;display:block;width: 16px;height: 16px;top: 50%;margin: -8px 12px;"></i>修改密码</a></li>	 			
						</c:if>
						<li><a target="_self" href="${ctx}/LoginAction.a?loginout"><i class="xyy-tuichu" style="position: absolute;display:block;width: 16px;height: 16px;top: 50%;margin: -8px 12px;"></i>退出登录</a></li>
					</ul>
				</div>				
			</div>			
        </div>
	</div>
    <div data-options="region:'south',split:false" style="height:40px;text-align:center;line-height:30px;background:#19aa8d;color:#f5f5f5;">教师管理信息系统（版本：1.2.2.16082516）</div>
    <div data-options="region:'west',title:'平台管理主菜单',split:true" style="width:200px;">
	    <div id="menu" class="easyui-accordion" fit="true" border="false">
             <input type="hidden" name="treeData" data="${jsonArray}"/>
		     <ul id="tt" class="easyui-tree"></ul>
        </div>
	</div>  
    <div data-options="region:'center'" style="background:#fff;border:0;">
	    <div id="tb" class="easyui-tabs" data-options="tools:'#tab-tools',fit:true">
			<div title="我的主页" iconCls="icon-home" style="padding:15px;" closable="false">
		        <div class="easyui-layout" data-options="fit:true">
					<div data-options="region:'north',border:false" style="height:auto; margin-bottom:10px;border-bottom:1px #a7b1c2 dashed;padding:5px;">
						<div class="easyui-layout" data-options="fit:true">
							<div data-options="region:'west',border:false" style="width:350px;height:160px;border-right:1px #a7b1c2 dashed;padding:5px;color:#2f4050; overflow:hidden">
								<h1 style="margin:0 0 10px 0; font-size:18px">欢迎使用<br>教师管理信息系统</h1>
								<input type="hidden" value="${sUser}" id="loginIsTeacher"/>
								<ul style="list-style-type:none;padding:0;">
									<li style="margin-bottom:10px;">
										<a href="#" style="text-decoration:none;font-size:16px;color:#2f4050;"><i class="xyy-hello" style="display:block;width: 16px;height:16px;float:left;margin:2px 3px;"></i>欢迎你,<span class="tag" style="color:red;">
											<c:if test="${sUser != null }">
												${sUser.userXm }
											</c:if>
											<c:if test="${sUser == null }">
												${teacher.xm }
											</c:if>
										</span>!</a>
									</li>	
									<li style="margin-bottom:10px;"><a href="#" style="text-decoration:none;font-size:16px;color:#2f4050;"><i class="xyy-time" style="display:block;width: 16px;height:16px;float:left;margin:2px 3px;"></i>现在是：<span id="timer" style="color:#19aa8d;font:16px tahoma;height:17px;"><script>setInterval("timeStr=new Date().toLocaleString();timer.innerText=timeStr;",1000)</script></span></a>
                                    </li>	
								</ul>																
							</div>
							<div data-options="region:'center',border:false" style="padding-left:20px;">
								<div class="jiankong" data-options="region:'center',border:false">
							 
									 <div class="juxing" >
									 <c:if test="${sUser==null}"> 
									 <a href="#">待办事项</a> 
							          </c:if>
									<table style="width:100%;padding-top:0px;text-align:center;line-height:24px;">
									<c:if test="${managerType==1}">
									  <tr>
									    <th>所属学段</th>
									    <th>教师上报数</th>
									   <tr>
									  <c:forEach items="${teacherNum.tnums}" var="tnum">
									  <tr>
									    <td><app:dictname dictid="XXJG_SSXD${tnum.ssxd}" /></td>
									    <td>${tnum.num}</td>
									   <tr>
									  </c:forEach>
									</c:if>
									<c:if test="${managerType==2}">
									  <tr>
										<th>教师总人数</th>
										<th>待审核人数</th>
										<th>已通过审核人数</th>
									</tr>	
									 
									  <tr>
									    <td>${teacherNum.num==null?0:teacherNum.num}</td>
									    <td>${teacherNum.dsh==null?0:teacherNum.dsh}</td>
									    <td>${teacherNum.shtg==null?0:teacherNum.shtg}</td>
									  </tr>
									</c:if>
									
									<c:if test="${managerType==3}">
									  <tr>
										<th>教师总人数</th>
										<th>待审核人数</th>
										<th>已通过审核人数</th>
									</tr>	
									 
									  <tr>
									    <td>${teacherNum.num==null?0:teacherNum.num}</td>
									    <td>${teacherNum.dsh==null?0:teacherNum.dsh}</td>
									    <td>${teacherNum.shtg==null?0:teacherNum.shtg}</td>
									  </tr>
									</c:if>
									</table>
									</div>
								</div>
							<%-- 	<c:if test="${managerType==1}">
								<div class="jiankong" data-options="region:'center',border:false">
							 
									 <div class="juxing">
									 <c:if test="${sUser==null}"> 
									 <a href="#">待办事项</a> 
							          </c:if>
									<table style="width:100%;padding-top:0px;text-align:center;line-height:24px;">
									
									  <tr>
									    <th>所属学段</th>
									    <th>教师上报数</th>
									    <th>待审核数</th>
									   <tr>
									  <c:forEach items="${teacherNum.tnums}" var="tnum">
									  <tr>
									    <td><app:dictname dictid="XXJG_SSXD${tnum.ssxd}" /></td>
									    <td>${tnum.num}</td>
									    <td>${tnum.num}</td>
									   <tr>
									  </c:forEach>
									</table>
									</div>
								</div>
								</c:if> --%>
							</div>
						</div>
					</div>
					
					<!-- 待办事项开始 -->	
				   
				   <c:if test="${managerType!=1&&managerType!=9}">
						<div data-options="region:'center',border:true" title="<c:if test="${sUser != null }">待办事项</c:if><c:if test="${sUser == null }">审核状态</c:if>" style="width:50%;height:150px;border-right:1px #a7b1c2 solid;padding:5px;color:#2f4050; overflow:hidden">
							<ul class="dbsx">
								<c:if test="${sUser != null }">
								   <c:forEach items="${sysTodoList}" var="todoList" varStatus="a">
										  <c:if test="${a.index<=4}">
											<li><span>${todoList.xm}老师信息审核</span>
											    <span>${todoList.sj}</span>
												<c:if test="${managerType==3}">
												<a class="school" href="#">处理</a>		
												</c:if>
												<c:if test="${managerType==2}">
												<a class="district" href="#">处理</a>		
												</c:if>
											</li>
										</c:if>	
								   </c:forEach>
								</c:if>
								<c:if test="${sUser == null }"><!-- 教师审核状态 -->
										 <c:if test="${tbJzgxxshLogDto!=null}">
										  <c:if test="${tbJzgxxsh.shzt==02||tbJzgxxsh.shzt==01}">
										  <li><app:dictname dictid="JSXX_SHZT${tbJzgxxsh.shzt}" /></li>
										  </c:if>
										  <c:if test="${tbJzgxxsh.shzt!=02&&tbJzgxxsh.shzt!=01}">
											<li>审核时间：${tbJzgxxshLogDto.shsj}
											        审核人：${tbJzgxxshLogDto.shr}
											        审核状态：<app:dictname dictid="JSXX_SHZT${tbJzgxxsh.shzt}" />
												审核结果：<app:dictname dictid="JSXX_SHJG${tbJzgxxshLogDto.shjg}" />
												审核意见：${tbJzgxxshLogDto.shyj}
											</li>
										  </c:if>
										</c:if>
										<c:if test="${tbJzgxxshLogDto==null}">
										     <c:if test="${tbJzgxxsh==null}"><li>未报送</li></c:if>
										     <c:if test="${tbJzgxxsh!=null}"><li><app:dictname dictid="JSXX_SHZT${tbJzgxxsh.shzt}" /></li></c:if>
										</c:if>	
								</c:if>
								
							</ul>															
						</div>
							<!-- 和需求沟通，已办事项不要了 -->
					<%-- 		<c:if test="${sUser != null }">
									<div data-options="region:'east',border:false" title="已办事项" style="width:50%;padding-left:20px;">
										<div class="jiankong" data-options="region:'center',border:false">
											<ul>
												<li>easyui is a collection of user-interface plugin based on jQuery.</li>
												<li>easyui provides essential functionality for building modem, interactive, javascript applications.</li>
												<li>using easyui you don't need to write many javascript code, you usually defines user-interface by writing some HTML markup.</li>
												<li>complete framework for HTML5 web page.</li>
												<li>easyui save your time and scales while developing your products.</li>
												<li>easyui is very easy but powerful.</li>
											</ul>	
										</div>
									</div>
						  </c:if> --%>
				</c:if>
				<!-- 待办事项结束 -->	
					<!-- <div data-options="region:'center',border:false" style="margin-top:20px; width:100%; height:500px; border:1px solid #a7b1c2">
                    	<div class="chart1" style="height:100%; width:100%;">
                        	<div style="width:100%; height:30px; font-size:14px; color:white; padding-left:20px; background:#192b3b; line-height:30px;">教师职称情况</div>
                        	<div id="dg" title="教师职称情况"  style="height:500px; width:400px; margin:20px auto; "></div>
                        </div>
                    </div>
                	<div data-options="region:'east',border:false" style="margin-top:20px;width:50%; height:500px; border:1px solid #a7b1c2">
                    	<div class="chart1" style="height:100%; width:100%;">
                        	<div style="width:100%; height:30px; font-size:14px; color:white; padding-left:20px; background:#192b3b; line-height:30px;">教师职称分布比例</div>
                        	<div id="dg2" title="教师职称分布比例"  style="height:500px; width:400px; margin:20px auto; "></div>
                        </div>
					</div> -->
				</div>
			</div>
		</div>
		<div id="tab-tools" style="background-color:#3c4656;">
			<a href="javascript:void(0)" onclick="removePanel()">关闭当前</a>
			<span>丨</span>
			<a href="javascript:void(0)" onclick="closeAll()">关闭全部</a>
	    </div>
	</div>  
	<div id="linkTabs" title="温馨提示" class="easyui-dialog" closed="true" data-options="modal:true">标签页开启数量已达到最大上限</div>

<!-- start -->
<div id="base" class="easyui-dialog" data-options="modal:true"  title="修改密码" closed="true" style="padding:10px 20px;width:500px;line-height:40px;text-align:center;overflow:auto;font-size:20px;">
	<div class="easyui-layout">
		<div class="easyui-panel">
			<div style="margin-bottom:20px">
				<lable>请输入旧密码：</lable>
				<input type="password" class="easyui-textbox" data-options="prompt:'请输入旧密码'" style="width:80%;height:32px" name="oldPassWord" id="oldPassword"/>
			</div>
			<div style="margin-bottom:20px">
				<lable>请输入新密码：</lable>
				<input type="password" class="easyui-textbox" data-options="prompt:'请输入新密码'" style="width:80%;height:32px" name="newPassWord" id="newPassword"/>
			</div>
			<div style="margin-bottom:20px">
				<lable>请确认新密码：</lable>
				<input type="password" class="easyui-textbox" data-options="prompt:'请确认新密码'" style="width:80%;height:32px"  name="confirmPassword" id="confirmPassword"/>
			</div>
			<div>
				<a href="#" class="easyui-linkbutton" iconCls="icon-ok" style="width:46%;height:32px;margin-right:10px;" onclick="save()">保存</a>
				<a href="#" class="easyui-linkbutton" iconCls="icon-ok" style="width:50%;height:32px" onclick="javascript:$('#base').dialog('close')">关闭</a>
			</div>
		</div>
	</div>
</div>
<c:if test="${sUser != null && sUser.userPwd == 'e10adc3949ba59abbe56e057f20f883e' }">
	<input type="hidden" id="resetPwd" value="1">
</c:if>
<input type="hidden" >
<!-- end -->
<script type="text/javascript">
var dataStrKey = $('input[name="treeData"][type="hidden"]').attr('data').replace(/\'/g,'\"');
var data = JSON.parse(dataStrKey);
  	$('#tt').tree({
  		data:data,
     onClick:function(node){
	var syUser = $('#loginIsTeacher').val();
	if(syUser==null||syUser==""){
		$.ajax({
		    url:"${ctx}/basicInformation/TeacherBaseInfoAction.a?checkBaseInfo",    //请求的url地址
		    dataType:"json",
		    data:{infoid:node.id},
		    type:"post",
		    success:function(dat){
		    	if(dat.open == "1"){/* &&node.url!=='null'&&node.url!==undefined */
		    		if(node.url!==undefined){
			    		addTab(node.text,window.location.protocol+'//'+window.location.host+node.url);
		    		}
		    	}else{
		    		$.messager.alert('温馨提示','请先完善基本信息！','info',function(){
		    			var n = $('#tt').tree('find', '4001');
		         		 $('#tt').tree('select', n.target);
		          		addTab(n.text,window.location.protocol+'//'+window.location.host+n.url); 
		    			//addTab("基本信息",window.location.protocol+'//'+window.location.host+'${ctx}/basicInformation/TeacherBaseInfoAction.a');
		    		});
		    	}
		    },
		    error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
		});
	}else if(node.url!=='null'&&node.url!==undefined){
		addTab(node.text,window.location.protocol+'//'+window.location.host+node.url);
	}
	
}
});

//$('input[name="treeData"][type="hidden"]').attr('data','');
function addTab(title, url){
	if($('.tabs li').size()>19){
		$('#linkTabs').dialog('open');
		return false;
	}
	if ($('#tb').tabs('exists', title)){
		$('#tb').tabs('select', title);
	} else {
		var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
		$('#tb').tabs('add',{
			title:title,
			content:content,
			closable:true
		});
	}
}
//关闭所有的tab  
    function closeAll(){  
        var tiles = new Array();  
          var tabs = $('#tb').tabs('tabs');      
          var len =  tabs.length;           
          if(len>0){  
            for(var j=0;j<len;j++){  
                var a = tabs[j].panel('options').title;               
                tiles.push(a);  
            }  
            for(var i=1;i<tiles.length;i++){               
                $('#tb').tabs('close', tiles[i]);  
            }
          }
    } 
//关闭当前的tab 
	function removePanel(){
		var tabObj = $('#tb').tabs('getSelected');
		var tabs = $('#tb').tabs('getTabIndex',tabObj);
		if(tabs!==0){
			var tab = $('#tb').tabs('getSelected');
		    if (tab){
				var index = $('#tb').tabs('getTabIndex', tab);
				$('#tb').tabs('close', index);
			}
		}else{
			$.messager.alert('温馨提示','不能关闭主页','info');
		}
	}
	//修改密码  保存
	function save(){
		var oldPassWord=$("#oldPassword").textbox("getValue");
		var oldPassWordMd5 = $.md5(oldPassWord);
		$.ajax({
		    url:"${ctx}/districtManager/InstitutionsManageAction.a?isOldPassWordRight",    //请求的url地址
		    dataType:"json",   //返回格式为json
		    //async:true,//请求是否异步，默认为异步，这也是ajax重要特性
		    data:{"oldPassWord":oldPassWordMd5},    //参数值
		    type:"post",   //请求方式
		    success:function(data){
		    	if(data.ins==true){
		    		var newPassWord=$("#newPassword").textbox("getValue");
		    		var newPassWordMd5 = $.md5(newPassWord);
		    		var confirmPassword=$("#confirmPassword").textbox("getValue");
		    		if(newPassWord!=confirmPassword){
		    			$.messager.alert('温馨提示','新密码和确认密码不一致，请重新输入！','info');
		    			return;
		    		}
		    		
		    		$.ajax({
		    		    url:"${ctx}/districtManager/InstitutionsManageAction.a?modifyPassWord",    //请求的url地址
		    		    dataType:"json",   //返回格式为json
		    		    //async:true,//请求是否异步，默认为异步，这也是ajax重要特性
		    		    data:{"newPassWord":newPassWordMd5},    //参数值
		    		    type:"post",   //请求方式
		    		    success:function(data){
		    		    	$("#base").dialog('close');
		    		    	$.messager.alert('温馨提示','密码修改成功！请重新登录！','info',function(){
		    		    		window.location.href = "${ctx}/LoginAction.a?loginout";
		    		    	});
		    		    },
		    		    error:function(XMLHttpRequest, textStatus, errorThrown){
		    				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
		    				sessionTimeout(sessionstatus);
		    			}
		    		});
		    		
		    	}else if(data.ins==false){
		    		$.messager.alert('温馨提示','原密码输入错误，请重新输入！','info');
		    		return;
		    	}
		    },
		    error:function(){
		        //请求出错处理
		    }
		});
	};
	
	//待办事项处理按钮跳转(校级)
	$('.school').click(function(){
		var n = $('#tt').tree('find', '3030');
		 $('#tt').tree('select', n.target);
 		addTab(n.text,window.location.protocol+'//'+window.location.host+n.url); 
	})
	//待办事项处理按钮跳转(区级)
	$('.district').click(function(){
		var n = $('#tt').tree('find', '202020');
		 $('#tt').tree('select', n.target);
 		addTab(n.text,window.location.protocol+'//'+window.location.host+n.url); 
	})
	
	
	
	/**打开修改框*/
	
	function setPwd(){
		$("#base").dialog('open');
	}
	

	window.onload=function(){
		//alert($("#resetPwd").length);
		$('#base').dialog({
		    onClose:function(){
		    	if($("#resetPwd").length > 0){
		    		$.messager.confirm('温馨提示','当前密码过于简单，请先修改密码！',function(){
		    			setPwd();
		    		});
				}
		    }
		});
		if($("#resetPwd").length > 0){
			$("#base").dialog('open');
		}
		
	}
</script>
</body>
</html>
