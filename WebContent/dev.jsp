<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=7" /> 
<meta name="copyright" content="北控软件版权所有"/>
<title>教师管理系统入口</title>
<link href="${ctx }/css/becom_global.css" rel="stylesheet" type="text/css" />
<link href="${ctx }/css/becom_login.css" rel="stylesheet" type="text/css" /></link>
<link href="${ctx }/css/becom_library.css" rel="stylesheet" type="text/css" />
<style type="text/css">
	ul{margin: 200px auto;padding: 50px;background: #FFF;border-radius: 5px;border: 1px solid #ccc;width: 500px;list-style-type: none;}
	li{font-size:20px;line-height:36px;}
</style>
</head>
<body>
	<div class="easyui-layout">
		<div class="easyui-menu">
			<ul>                  
				<li><a href="http://localhost:8080/jspt2016/loginTestAction.a?organizationLogin&organization_code=sj&pwd_md5=111111&token=61dc732e4fb8622bec0cdded14b0f7c7" title="机构（市级）">机构（市级）</a></li>
				<li><a href="http://localhost:8080/jspt2016/loginTestAction.a?organizationLogin&organization_code=dc&pwd_md5=111111&token=c7e40e5adcb6008059a3e11c1f526a20" title="机构(区级--东城)">机构(区级--东城)</a></li>
				<li><a href="http://localhost:8080/jspt2016/loginTestAction.a?organizationLogin&organization_code=01064010&pwd_md5=96e79218965eb72c92a549dd5a330112&token=8b88469e48f4b83c9d99ae634e4f0820" title="机构(学校--中小学校)">机构(学校--中小学校)</a></li>
				<li><a href="http://localhost:8080/jspt2016/loginTestAction.a?teacherLogin&teacher_edu_id=1&login_time=17:40:36&token=d1daa55b9f3c2186aae1861a4ee35fb3" title="教师（本科">教师（本科）</a></li>
				<li><a href="http://localhost:8080/jspt2016/loginTestAction.a?teacherLogin&teacher_edu_id=90000004&login_time=17:40:36&token=f5c625625721b2fe30f50307b3ad88e5" title="教师（高职）">教师（高职）</a></li>
				<li><a href="http://localhost:8080/jspt2016/loginTestAction.a?teacherLogin&teacher_edu_id=90100011&login_time=17:40:36&token=33c9655ed9153463345604715254f5cc" title="教师（中等）">教师（中等）</a></li>
				<li><a href="http://localhost:8080/jspt2016/loginTestAction.a?teacherLogin&teacher_edu_id=90100012&login_time=17:40:36&token=eed085d62a6f7bb058b819a3c20367b4" title="教师（幼儿园）">教师（幼儿园）</a></li>
				<li><a href="http://localhost:8080/jspt2016/loginTestAction.a?teacherLogin&teacher_edu_id=90100013&login_time=17:40:36&token=0039ba7b20374369ad9a9304297170fb" title="教师（特教）">教师（特教）</a></li>
				<li><a href="http://localhost:8080/jspt2016/loginTestAction.a?teacherLogin&teacher_edu_id=90600004&login_time=17:40:36&token=586081bb803cb472eaee6c0d14ec2e1d" title="教师（中小学）">教师（中小学）</a></li>
			    <li><a href="http://localhost:8080/jspt2016/loginTestAction.a?organizationLogin&organization_code=0100D012&pwd_md5=111111&token=cda0a66ad006d256dc8411620a4c5ec0" title="机构
(学校--本科)">机构(学校--本科)</a></li>
				<li><a href="http://localhost:8080/jspt2016/loginTestAction.a?organizationLogin&organization_code=0100C024&pwd_md5=111111&token=575194c00342c78a28014b81761030ee" title="机构
(学校--高职)">机构(学校--高职)</a></li>
				<li><a href="http://localhost:8080/jspt2016/loginTestAction.a?teacherLogin&teacher_edu_id=90100125&login_time=17:40:36&token=29efd45717dadcd1e2aa68b41a950d19" title="教师（

本科）">教师（本科）</a></li>
				<li><a href="http://localhost:8080/jspt2016/loginTestAction.a?teacherLogin&teacher_edu_id=90100127&login_time=17:40:36&token=6285090e857a5ac0b5dcbf4614d4c4eb" title="教师（

高职）">教师（高职）</a></li>
			</ul>
		</div>
	</div>
</body>
</html>
