<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>北京市教师管理服务平台</title>
    <link rel="stylesheet" type="text/css" href="../css/newcss.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
    <script type="text/javascript" src="../easyui/jquery.min.js"></script>
    <script type="text/javascript" src="../easyui/easyloader.js"></script>
    <script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx}/js/refresh.js"></script>
    <script type="text/javascript" src="../basicInformation/ValidateUtils.js"></script>
	<script type="text/javascript" >
		function downloadTmp(){
			var frmObj=$("#frm");
			frmObj.attr("action","${ctx}/sysuserManager/FileAction.a?getFile&fileName=company.xlsx");
			frmObj.submit();
			frmObj.attr("action","${ctx}/districtManager/InstitutionsManageAction.a");
		}
		
		function getFormData(){
			var map = '{';
			var institutionNameQuery = $("#institutionNameQuery").textbox("getValue");
			var institutionCodeQuery = $("#institutionCodeQuery").textbox("getValue");
			var affiliatedSchoolQuery = $("#affiliatedSchoolQuery2").combobox("getValue");
			var unitTypeQuery = $("#unitTypeQuery").combobox("getValue");
			
			var schoolTypeQuery = $("#schoolTypeQuery").combobox("getValue");
			var residentTypeQuery = $("#residentTypeQuery").combobox("getValue");
			if(institutionNameQuery != ""){
				map += 'institutionNameQuery:' + institutionNameQuery +',';
			}
			
			if(institutionCodeQuery != ""){
				map += 'institutionCodeQuery:' + institutionCodeQuery +',';
			}
			
			if($("#districtCodeQuery")){
				var districtCodeQuery = $("#districtCodeQuery").combobox("getValue");
				if(districtCodeQuery != ""){
					map += 'districtCodeQuery:' + districtCodeQuery +',';
				}
			}
			
			if(affiliatedSchoolQuery != ""){
				map += 'affiliatedSchoolQuery:' + affiliatedSchoolQuery +',';
			}
			
			if(schoolTypeQuery != ""){
				map += 'schoolTypeQuery:' + schoolTypeQuery +',';
			}
			
			if(unitTypeQuery != ""){
				map += 'unitTypeQuery:' + unitTypeQuery +',';
			}
			if(residentTypeQuery != ""){
				map += 'residentTypeQuery:' + residentTypeQuery +',';
			}
			map += 'time:100}';
			return map;
		}
	</script>
</head>
<body class="easyui-layout" style="margin:0 1px;background-color:#f5f5f5 !important;">
	<input id="userType" type="hidden" name="userType" value="${userType }"/> 
    <div data-options="region:'center'" style="background:#fff;border:0;margin-top:20px;">
	    <!--<div id="tb" class="easyui-tabs" data-options="tools:'#tab-tools',fit:true">
			<div title="单位管理" iconCls="icon-home" style=" ">-->
            
		        <div class="easyui-layout" data-options="fit:true">
					<div data-options="region:'center',border:false">
                    <form action="${ctx}/districtManager/InstitutionsManageAction.a" id="frm" method="post">
                        <div class="easyui-panel" data-options="region:'center',border:false,collapsible:true" iconCls="icon-dingdan" title="查询条件"  style="margin-bottom:0px; width:100%">
                            <div id="toolbarxx" style=" border-bottom:1px dotted #ccc">
                            <a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="submit()">查 询</a>
                            <a href="#" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="reset()">重 置</a>
                            
                            </div>
                            <table id="dg1" class=" fitem" data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,toolbar:'#toolbar',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
                                <tr>
                                    <td>单位名称</td>
                                    <th><input type="text" name="institutionNameQuery" class="easyui-textbox" value="${institutionNameQuery }" id="institutionNameQuery"></th>
                                    <td>单位代码</td>
                                    <th><input type="text" name="institutionCodeQuery" class="easyui-textbox"  value="${institutionCodeQuery }" id="institutionCodeQuery"></th>
                                    
	                                   	<c:choose>
	                                         	<c:when test="${userType=='2' }">
<!-- 	                                           		<select class="easyui-combobox" required="true" name="districtCodeQuery" id="districtCodeQuery"> -->
<%-- 	                                                  <c:forEach items="${disCodes }" var="list"> --%>
<%-- 	                                                  		<option value="${list.disCode }">${list.disName }</option> --%>
<%-- 	                                                  </c:forEach> --%>
<!-- 	                                               </select> -->
	                                         	</c:when>
	                                         	<c:when test="${userType=='1' }">
	                                         	<td>所在区</td>
                                    			<th>
	                                          		<select class="easyui-combobox" name="districtCodeQuery" id="districtCodeQuery" value="${districtCodeQuery }">
	                                                  <option value="">请选择</option>
	                                                  <c:forEach items="${disCodes }" var="list">
	                                                  		<option value="${list.disCode }" <c:if test="${list.disCode==districtCodeQuery }">selected='selected'</c:if> >${list.disName }</option>
	                                                  </c:forEach>
	                                               </select>
	                                             </th>
	                                               </c:when>
	                                       </c:choose>
                                        <%--  <select class="easyui-combobox"  name="districtCodeQuery" id="" value="${ districtCodeQuery}" >
                                            <option value="">请选</option>
                                            <app:dictselect dictType="XXJG_SSXD"  selectValue="${ districtCodeQuery}"/>
                                        </select> --%>
                                    
                                </tr>
                                <tr>
                                    <td>所属学段</td>
                                    <th>
                                         <select class="easyui-combobox"  name="affiliatedSchoolQuery" id="affiliatedSchoolQuery2" value="${affiliatedSchoolQuery }" >
                                            	<option value="">请选择</option>
                                            <%-- <app:dictselect dictType="XXJG_SSXD" selectValue="${affiliatedSchoolQuery }"/> --%>
                                            <c:if test="${userType==1}">
                                            	<option value="10" <c:if test="${affiliatedSchoolQuery ==10}">selected="selected"</c:if> >10-本科学校</option>
                                            	<option value="20" <c:if test="${affiliatedSchoolQuery ==20}">selected="selected"</c:if> >20-高等职业学校</option>
                                           </c:if> 	
                                            	<option value="30" <c:if test="${affiliatedSchoolQuery ==30}">selected="selected"</c:if> >30-中小学校</option>
                                            	<option value="40" <c:if test="${affiliatedSchoolQuery ==40}">selected="selected"</c:if> >40-中等职业学校</option>
                                            	<option value="50" <c:if test="${affiliatedSchoolQuery ==50}">selected="selected"</c:if> >50-特殊教育学校</option>
                                            	<option value="60" <c:if test="${affiliatedSchoolQuery ==60}">selected="selected"</c:if> >60-幼儿园</option>
                                        </select>
                                    </th>
                                    <td>单位类别</td>
                                    <th>
                                         <select class="easyui-combobox"  name="unitTypeQuery" id="unitTypeQuery">
                                            <option value="">请选择</option>
                                            <app:dictselect dictType="XXJG_ZXXFL"  selectValue="${unitTypeQuery }"/>
                                        </select>
                                    </th>
                                    <td>办学类型</td>
                                    <th>
                                         <select class="easyui-combobox"  name="schoolTypeQuery" id="schoolTypeQuery" value="${ schoolTypeQuery}" >
                                            <option value="">请选择</option>
                                            <app:dictselect dictType="XXJG_BXLX" selectValue="${ schoolTypeQuery}"/>
                                        </select>
                                    </th>
                                    <td>城乡类型</td>
                                    <th>
                                         <select class="easyui-combobox"  name="residentTypeQuery" id="residentTypeQuery" value="${residentTypeQuery }" >
                                            <option value="">请选择</option>
                                            <app:dictselect dictType="XXJG_ZDCXLX"  selectValue="${residentTypeQuery }"/>
                                        </select>
                                    </th>
                                </tr>
                            </table>
                        </div>
						<div data-options="region:'center',border:false" style="margin-top:20px;">
                            <table id="dg" title="单位信息" iconCls="icon-dingdan" class="easyui-datagrid" style="width:100%;"
								data-options="idField:'id',rownumbers:false,fitColumns:false,singleSelect:false,collapsible:true,toolbar:'#toolbar',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
							<!---获取数据--->
							<thead>
								<tr>
									<th field="ck" checkbox="true"></th>
                                <th data-options="field:'id',width:50">序号</th>
								<th data-options="field:'ne',width:80">学校代码</th>
                                <th data-options="field:'ns',width:100">组织单位代码</th>
                                <th data-options="field:'nacgvme',width:200">单位名称</th>
                                <th data-options="field:'nafsame',width:80">所在区</th>
                                <th data-options="field:'cfgvme',width:80">所属学段</th>
                                <th data-options="field:'cme',width:80">单位类别</th>
                                <th data-options="field:'nazme',width:200">地址</th>
                                <th data-options="field:'navx',width:80">办学类型</th>
                                <th data-options="field:'nadgme',width:80">城乡类型</th>
                                <th data-options="field:'navxme',width:150">举办单位</th>
                                <th data-options="field:'me',width:150">举办者类型</th>
                                <th data-options="field:'nacxgme',width:80">联系人</th>
                                <th data-options="field:'gme',width:100">联系人电话</th>
                                <th data-options="field:'njfgde',width:150">联系人邮箱</th>
								</tr>
							</thead>
							<tbody>
								<c:if test="${not empty pageObj.pageElements}">
									<c:forEach items="${pageObj.pageElements}" var="item" varStatus="status">
										<tr>
											<td>${item.institutionId}</td>
											<td>${item.institutionId}</td>
											<td>${item.institutionCode}</td>
											<td>${item.unitCode}</td>
											<td>${item.institutionName}</td>
											<td>${item.disName}</td>
											<td><app:dictname dictid="XXJG_SSXD${item.affiliatedSchool}"/></td>
											<td>
												<c:choose>
													<c:when test="${item.unitType !=null}">
														<app:dictname dictid="XXJG_ZXXFL${item.unitType}"/>
													</c:when>
													<c:otherwise>
														无
													</c:otherwise>
												</c:choose>
											</td>
											<td>${item.institutionAddress}</td>
											<td><app:dictname dictid="XXJG_BXLX${item.schoolType}"/></td>
											<td><app:dictname dictid="XXJG_ZDCXLX${item.residentType}"/></td>
											<td>${item.jbdw}</td>
											<td><appadd:dictnameadd dictid="XXJG_JBZLX" dictbm="${item.jbzlx}" schoolType="${item.affiliatedSchool}"/></td>
											<td>${item.contactPerson}<input type="hidden" name="count" value="${item.count}"/></td>
											<td>${item.contactTel}</td>
											<td>${item.email}</td>
										</tr>
									</c:forEach>
								</c:if>
								<c:if test="${empty pageObj.pageElements}">
									<tr>
										<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
									</tr>
								</c:if>
								
							</tbody>
							</table>
							<div id="toolbar">
							<c:if test="${userType==1}">
								<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUser()">新 增</a>
							</c:if>	
								<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">编 辑</a>
							<c:if test="${userType==1}">
								<a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a>
							</c:if>	
<!--                                 <a href="#" class="easyui-linkbutton" iconCls="icon-daoru" plain="true" onclick="daoru()">导入</a> -->
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-daochu" plain="true" onclick="daochu()">导出</a>
							</div>
							<!-- 分页所必须的 -->
							<div class="page tar mb15">
								<input name="pageNo" value="${pageNo }" type="hidden" />
								<c:if test="${not empty pageObj.pageElements}">
									<jsp:include page="../common/pager-nest.jsp">
										<jsp:param name="toPage" value="1" />
										<jsp:param name="showCount" value="1" />
										<jsp:param name="action" value="doSearch" />
									</jsp:include>
								</c:if>
							</div>
						</div>
						</form>
							<!-- 添加页面   开始 -->
						<div id="dlg" class="easyui-dialog" style="width:700px;height:400px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                               <form action="${ctx}/districtManager/InstitutionsManageAction.a?add" id="addfrm" method="post">
                                <div class="right_table">
                                    <div id="table-content">
                                    <!-- <form id="fm" method="post" novalidate> -->
                                    <table class="fitem clean">
                                            <tbody>
                                                <tr>
													<td>学校代码：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="institutionDto.institutionCode" id="institutionCode"   required  />
                                                    </td>
                                                    <td>单位名称：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="institutionDto.institutionName" id="institutionName"  required />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>组织单位代码：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="institutionDto.unitCode" id="unitCode"  />
                                                    </td>
                                                    <td>所在区：</td>
                                                    <td>
	                                                    <c:choose>
				                                         	<c:when test="${userType=='2' }">
				                                           	   <select class="easyui-combobox" required="true" name="institutionDto.districtCode" id="districtCode" >
				                                                  <c:forEach items="${disCodes }" var="list">
				                                                  		<option value="${list.disCode }">${list.disName }</option>
				                                                  </c:forEach>
				                                               </select>
				                                         	</c:when>
				                                         	<c:when test="${userType=='1' }">
				                                          		<select class="easyui-combobox" required="true" name="institutionDto.districtCode" id="districtCode">
				                                                  <option value="">请选择</option>
				                                                  <c:forEach items="${disCodes }" var="list">
				                                                  		<option value="${list.disCode }" >${list.disName }</option>
				                                                  </c:forEach>
				                                               </select>
				                                         	</c:when>
				                                       </c:choose>
                                                    </td>
                                                </tr>
                                                <tr>
                                                	<td>所属学段：</td>
                                                    <td>
                                                         <select class="easyui-combobox" required="true" name="institutionDto.affiliatedSchool" id="affiliatedSchool">
                                                            <option value="">请选择</option>
                                                            <app:dictselect dictType="XXJG_SSXD" />
                                                        </select>
                                                    </td>
                                                	<td>单位类别：</td>
                                                    <td>
                                                         <select class="easyui-combobox" required="true" name="institutionDto.unitType" id="unitType" disabled="disabled">
                                                            <option value="">请选择</option>
                                                            <app:dictselect dictType="XXJG_ZXXFL" />
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                	<td>地址：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="institutionDto.institutionAddress" id="institutionAddress" required />
                                                    </td>
                                                    <td>办学类型：</td>
                                                    <td>
                                                         <select class="easyui-combobox" required="true" name="institutionDto.schoolType" id="schoolType">
                                                            <option value="">请选择</option>
                                                            <app:dictselect dictType="XXJG_BXLX" />
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                	<td>城乡类型：</td>
                                                    <td>
                                                         <select class="easyui-combobox" required="true" name="institutionDto.residentType" id="residentType">
                                                            <option value="">请选择</option>
                                                            <app:dictselect dictType="XXJG_ZDCXLX" />
                                                        </select>
                                                    </td>
                                                    <td>举办单位：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="institutionDto.jbdw" id="jbdw" required="true" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                	<td>举办者类型：</td>
                                                    <td>
                                                         <select class="easyui-combobox" required="true" name="institutionDto.jbzlx" id="jbzlx">
                                                            <option value="">请选择</option>	
                                                            <app:dictselect dictType="XXJG_JBZLX" />
                                                        </select>
                                                    </td>
                                                    <td>联系人：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="institutionDto.contactPerson" id="contactPerson"  />
                                                    </td>
                                                </tr>
                                                <tr>
                                                 <td>联系电话：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="institutionDto.contactTel" id="contactTel"  />
                                                    </td>
                                                    <td>邮箱：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="institutionDto.email" id="email"  />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    <!-- </form> -->
                                    </div>
                                </div>
									<input type="hidden" name="institutionDto.institutionId" id="institutionIdEdit"/>
                                 </form>
							</div>
							<div id="dlg-buttons">
								<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()">保存</a>
								<a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">关闭</a>
							</div>
							<!-- 添加页面   结束 -->
							
							<!-- 导出页面   开始 -->
						<div id="importdlg" class="easyui-dialog" style="width:350px;height:200px; overflow:auto;padding:10px 20px" closed="true" buttons="#importdlg-buttons">
                               <div>
								<a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-print" onclick="downloadTmp()">点击下载模版</a>
								</div>
<%--                                <form action="${ctx}/districtManager/InstitutionsImportAction.a" id="importfrm" method="post" enctype="multipart/form-data"> --%>
                                    <form id="importfrm" onsubmit="return false;">
<!--                                     	选择照片： <input style="margin-right:10px;" type="file" class="filetype" id="fileImport" name="fileImport" value=""> -->
                                    <table class="fitem clean">
                                            <tbody>
                                                <tr>
                                                    <td>选择文件：</td>
                                                    <td>
                                                        <input name="fileImport" id="fileImport" class="easyui-filebox" required="true" style="width:200px;"/>
                                                    </td>
                                            </tbody>
                                        </table>
                                 </form>
							</div>
							<div id="importdlg-buttons">
								<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="importinfo()">导入</a>
								<a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#importdlg').dialog('close')">关闭</a>
							</div>
					</div>
				</div>
			</div>
		<div id="tips" class="easyui-dialog" title="温馨提示" style="padding:10px 20px;width:220px;height:120px;line-height:40px;text-align:center;overflow:auto;font-size:20px;" closed="true" data-options="modal:true">请选择一条记录</div>
<script type="text/javascript">
$('#districtCodeQuery').combobox({ editable:false }); //所在区
$('#affiliatedSchoolQuery2').combobox({ editable:false }); //所属学段
$('#unitTypeQuery').combobox({ editable:false }); //单位类别
$('#schoolTypeQuery').combobox({ editable:false }); //办学类型
$('#residentTypeQuery').combobox({ editable:false }); //城乡类型

$('#districtCode').combobox({ editable:false }); //所在区
$('#affiliatedSchool').combobox({ editable:false }); //所属学段
$('#unitType').combobox({ editable:false }); //单位类别
$('#schoolType').combobox({ editable:false }); //办学类型
$('#residentType').combobox({ editable:false }); //城乡类型
$('#jbzlx').combobox({ editable:false }); //举办者类型
$('#schoolType').combobox({ editable:false }); //办学类型
$('#schoolType').combobox({ editable:false }); //办学类型



//页面为空显示提示
$('#dg').datagrid({
	onLoadSuccess:function(data){
		if(data.rows[0].id==undefined){
			$('#datagrid-row-r3-2-0').html(data.rows[0].ck);
			$('.datagrid-btable').css({'text-align':'center','width':'100%','padding':'30px 0'});
		}
	}
});

function tips(dlgNmae,msg){
	$(dlgNmae).html(msg).dialog('open');
};
function addTab(title, url){
	if ($('#tb').tabs('exists', title)){
		$('#tb').tabs('select', title);
	} else {
		var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
		$('#tb').tabs('add',{
			title:title,
			content:content,
			closable:true
		});
	}
}
//关闭所有的tab  
    function closeAll(){  
        var tiles = new Array();  
          var tabs = $('#tb').tabs('tabs');      
          var len =  tabs.length;           
          if(len>0){  
            for(var j=0;j<len;j++){  
                var a = tabs[j].panel('options').title;               
                tiles.push(a);  
            }  
            for(var i=1;i<tiles.length;i++){               
                $('#tb').tabs('close', tiles[i]);  
            }  
          }  
    } 
//关闭当前的tab 
	function removePanel(){
		var tab = $('#tb').tabs('getSelected');
		    if (tab){
			var index = $('#tb').tabs('getTabIndex', tab);
			$('#tb').tabs('close', index);
			}
		}
		
		
	var url;
	//表单提交
	function submit(){
		$("#frm").submit();
	}
	
	/* 弹出新增框 */
	function newUser(){
		$('#dlg').dialog('open').dialog('setTitle','新增单位信息');
		$('#fm').form('clear');
		
		//区级用户 所属类型控制
		var userType=$("#userType").val();
		 if(userType=='2'){
			//区级
			$('#_easyui_combobox_i8_1,#_easyui_combobox_i8_2').remove();
		}
	}
	/* 弹出编辑框 */
	function editUser(){
		var selected=$("input[type='checkbox'][name='ck']:checked");
		
		if(selected.length<=0){
			$.messager.alert('温馨提示','请选择一条记录');
			return ;
		}else if(selected.length>1){
			$.messager.alert('温馨提示','只能编辑一条记录');
			return ;
		}else{	
			var institutionId=selected.val();
			$.ajax({
			    url:"${ctx}/districtManager/InstitutionsManageAction.a?toEdit&time="+new Date().getTime(),    //请求的url地址
			    dataType:"json",   //返回格式为json
			    //async:true,//请求是否异步，默认为异步，这也是ajax重要特性
			    data:{"institutionId":institutionId},    //参数值
			    type:"post",   //请求方式
			    success:function(data){
			    	console.log(data);
			    	console.log(data.ins.schoolType);
			        $("#institutionIdEdit").val(data.ins.institutionId);
			        $("#unitCode").textbox('setValue',data.ins.unitCode);
			        $("#institutionCode").textbox('setValue',data.ins.institutionCode);
			        $("#institutionName").textbox('setValue',data.ins.institutionName);
			        $("#institutionAddress").textbox('setValue',data.ins.institutionAddress);
			        $("#jbdw").textbox('setValue',data.ins.jbdw);
			        $("#contactPerson").textbox('setValue',data.ins.contactPerson);
			        $("#contactTel").textbox('setValue',data.ins.contactTel);
			        $("#email").textbox('setValue',data.ins.email);
			        $("#affiliatedSchool").combobox('setValue',data.ins.affiliatedSchool);
			        
			        init(data.ins.schoolType,data.ins.jbzlx);//
// 			        $("#schoolType").combobox('setValue',data.ins.schoolType);
// 			        $("#jbzlx").combobox('setValue',data.ins.jbzlx);
			        
			        $("#residentType").combobox('setValue',data.ins.residentType);
			        
			        $("#districtCode").combobox('setValue',data.ins.districtCode);//可能有修改
			        $("#unitType").combobox('setValue',data.ins.unitType);
			        //不需要提交的
			        $("#institutionCode").textbox("disable");
			        $("#unitType").combobox("disable")
			        $("#affiliatedSchool").combobox("disable")
			        $("#districtCode").combobox("disable");
			        
			        if(data.ins.affiliatedSchool == 50){
			        	$.messager.alert('温馨提示','学段为特殊教育的不能修改！');
			        }else if(data.ins.affiliatedSchool == 30 && data.ins.unitType == 1){
			        	$.messager.alert('温馨提示','单位类别为中小学的不能修改！');
			        }else{
			        	///打开编辑框
				        var row = $('#dg').datagrid('getSelected');
						if (row){
							$('#dlg').dialog('open').dialog('setTitle','编辑单位信息');
							$('#fm').form('load',row);
							
						}
			        }
			        
			    },
			    error:function(XMLHttpRequest, textStatus, errorThrown){
					var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
					sessionTimeout(sessionstatus);
				}
			});
		}
		
		
	}
	
	function saveUser(){
		var name = $("#institutionName").textbox("getValue");
		var jgId = "";
		if($("#institutionIdEdit")){
			jgId = $("#institutionIdEdit").val();
		}
		$.ajax({
		    url:"${ctx}/districtManager/InstitutionsManageAction.a?fetchInstitutionByName&time="+new Date().getTime(),    //请求的url地址
		    dataType:"json",   //返回格式为json
		    //async:true,//请求是否异步，默认为异步，这也是ajax重要特性
		    data:{"jgName":name,"jgId":jgId},    //参数值
		    type:"post",   //请求方式
		    success:function(data){
		    	console.log(data);
		    	if(data.ins==null){
		    		saveUser1();
		    	}else{
		    		$.messager.alert('温馨提示','该单位名称已经存在！');
		    	}
		    },
		    error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
		});
	}
	
	
	/* 新增或者编辑的保存方法 */
	function saveUser1(){
		//var contactTel= $("#contactTel").textbox("getValue");
		var email= $("#email").textbox("getValue");
		/* if(contactTel != null){
			if(!(/^1[34578]\d{9}$/.test(contactTel))){ 
		    	$.messager.alert('温馨提示','请输入正确的联系电话');
				return false;
		    }
		} */
	    
		if(email!=null){
			if(ValidateUtils.validEmailAddress($('#email').val())==false){
				$.messager.alert('温馨提示','请输入正确的邮箱');
				return false;
			}
		}
		if(ValidateUtils.validIntegerss($('#unitCode').val())==false){
			$.messager.alert('温馨提示','请输入正确的组织单位代码');
			return false;
		}
		var iname=$("#institutionName").textbox("getValue");
		if(iname==''){
			$.messager.alert('温馨提示','请填写单位名称！');
			return ;
		}
		var disCode=$("#districtCode").combobox("getValue");
		if(disCode==''){
			$.messager.alert('温馨提示','请填写所在区！');
			return ;
		}
		var affiliatedSchool=$('#affiliatedSchool').combobox('getValue');
		var unitType=$('#unitType').combobox('getValue');
		if((affiliatedSchool=='30'&&unitType=='1')||affiliatedSchool=='50'){
			var codes=$("#institutionCode").textbox("getValue");
			if(codes==''){
				$.messager.alert('温馨提示','请填写单位代码！');
				return ;
			}
		}
		if(affiliatedSchool==''){
			$.messager.alert('温馨提示','请填写所属学段！');
			return ;
		}
		if(affiliatedSchool=='30'){
			if(unitType==''){
				$.messager.alert('温馨提示','请填写单位类别！');
				return ;
			}
		}
		
		
		var address=$("#institutionAddress").textbox("getValue");
		if(address==''){
			$.messager.alert('温馨提示','请填写单位地址！');
			return ;
		}
		
		var schoolType=$("#schoolType").combobox("getValue");
		if(schoolType==''){
			$.messager.alert('温馨提示','请填写办学类型！');
			return ;
		}
		
		var residentType=$("#residentType").combobox("getValue");
		if(residentType==''){
			$.messager.alert('温馨提示','请填写城乡类型！');
			return ;
		}
		
		var jbdw=$("#jbdw").textbox("getValue");
		if(jbdw==''){
			$.messager.alert('温馨提示','请填写举办单位！');
			return ;
		}
		
		var jbzlx=$("#jbzlx").combobox("getValue");
		if(jbzlx==''){
			$.messager.alert('温馨提示','请填写举办者类型！');
			return ;
		}
		var code=$("#institutionCode").val();
		$.ajax({
		    url:"${ctx}/districtManager/InstitutionsManageAction.a?fetchInstitution&time="+new Date().getTime(),
		    dataType:"json", 
		    data:{"institutionCode":code},
		    type:"post",
		    success:function(data){
		    	console.log(data);
			    var institutionIdEdit=$("#institutionIdEdit").val();
		    	if(data.ins==null||(institutionIdEdit!=null&&institutionIdEdit!="")){
					if(institutionIdEdit!=null&&institutionIdEdit>0){
						$.messager.alert('温馨提示','编辑成功','info',function(){
			              	window.location.reload();
			      	  	});
						var frmObj=$("#addfrm");
						frmObj.attr("action","${ctx}/districtManager/InstitutionsManageAction.a?edit");
						frmObj.submit();
					}else{
						$.messager.alert('温馨提示','保存成功','info',function(){
			              	window.location.reload();
			      	  	});
						var frmObj=$("#addfrm");
						frmObj.attr("action","${ctx}/districtManager/InstitutionsManageAction.a?add");
						frmObj.submit();
					} 
		    	}else{
		    		$.messager.alert('温馨提示','单位代码已存在，请重新填写！');
		    	}
		    },
		    error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
		});
		
		
	}
	
	$('#dg').datagrid({
		onCheck:function(rowIndex,rowData){
			rowData.index = rowIndex;
		}
	});
	
	/* 删除 */
	function removeUser(){
		var selected=$("input[type='checkbox'][name='ck']:checked");
		if(selected.length<=0){
			$.messager.alert('温馨提示','请选择一条记录');
			return ;
		}
		var node = $('#dg').datagrid('getSelections')
		for(var i=0;i<node.length;i++){
			var isTrueStr = node[i].nacxgme;
			var isTrue = isTrueStr.slice(-3,-2);
			if(!isTrue){
				$('#dg').datagrid('unselectRow',node[i].index);
			}
		}
		
		$.messager.confirm('温馨提示', '是否要删除', function(r){
			if(r){
				var frmObj=$("#frm");
				frmObj.attr("action","${ctx}/districtManager/InstitutionsManageAction.a?delete");
				frmObj.submit();
				$.messager.alert('温馨提示','删除成功','info',function(){
	              	window.location.reload();
	      	  	});
			}
		});
	}
	
	/* 重置 */
	function reset(){
		$("input[name='institutionNameQuery']").val("");
		$("input[name='institutionCodeQuery']").val("");
		$("input[name='districtCodeQuery']").val("");
		$("input[name='affiliatedSchoolQuery']").val("");
		$("input[name='schoolTypeQuery']").val("");
		$("input[name='residentTypeQuery']").val("");
		$("input[name='unitTypeQuery']").val("");
		window.location.href = "${ctx}/districtManager/InstitutionsManageAction.a";
	}
	
	/* 导入*/
	function daoru(){
		$('#importdlg').dialog('open').dialog('setTitle','导入单位信息');
	}
	
	/** 上传导入**/
	function importinfo(){
		var formData = new FormData($("#importfrm")[0]);
		
		var file = $('#fileImport').filebox('getValue');
// 		var file = $('#fileImport').val();
        if (file == null || file == "") { 
            	$.messager.alert('温馨提示','请选择文件');
            	return;
        }
        var file_typename = file.substring(file.lastIndexOf('.'), file.length);
        if (file_typename == '.xlsx' || file_typename == '.xls'){
        }else{
     	   $.messager.alert('温馨提示','文件类型错误');
     	   return;
        }
		
		$.ajax({
    		type : "post",
    		url : "${ctx}/districtManager/InstitutionsImportAction.a?importFile=aaa",
    		dataType : "json",
    		data : formData,
    		async : false,
    		cache: false,
            contentType: false,
            processData: false,
    		success : function(data) {
    			$.messager.alert('温馨提示',data.result.msg,'info');
    		},
    		error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
    	});
		
		/*
		$('#importfrm').form('submit',{
	           url: "${ctx}/districtManager/InstitutionsImportAction.a",
	           onSubmit: function(){
	        	   var file = $('#fileImport').filebox('getValue');
	               if (file == null || file == "") { 
		               	$.messager.alert('温馨提示','请选择文件');
		               	return false;
	               }
	               var file_typename = file.substring(file.lastIndexOf('.'), file.length);
	               if (file_typename == '.xlsx' || file_typename == '.xls'){
	               		return true;
	               }else{
	            	   $.messager.alert('温馨提示','文件类型错误');
	            	   return false;
	               }
	           },
	           success: function(data){
	        	   window.location.reload();
	           }
	       });
        */
	}
	
	/* 导出 */
	function daochu(){
		/*
     $.messager.progress({ 
	        title: '请稍等', 
	        msg: '正在导出数据...', 
	        text: 'wating.......' 
	    });
     	var formmap = getFormData();
     	alert(formmap);
		$.ajax({
    		type : "post",
    		url : "${ctx}/districtManager/InstitutionsExportAction.a?export",
    		dataType : "json",
    		data : formmap,
    		async : true,
    		cache: false,
            contentType: false,
            processData: false,
    		success : function(data) {
    			$.messager.progress('close');
    			if(data.fileName != null){
    				$.messager.alert('温馨提示',"导出成功,点击确定下载！",'info',function(){
    					downloadFail(data.fileName);
    			  	});
    			}else{
    				$.messager.alert('温馨提示','服务器出错，导出失败','error');
    			}
     			
    		},
    		error : function(XMLHttpRequest, textStatus, errorThrown){
              $.messager.alert('温馨提示','服务器出错，导出失败','error');
           }
    	});
		*/
		var frmObj=$("#frm");
		frmObj.attr("action","${ctx}/districtManager/InstitutionsExportAction.a?export");
		frmObj.submit();
		$('#frm').attr("action","${ctx}/districtManager/InstitutionsManageAction.a");
		
		
// 		$.messager.progress({ 
// 	        title: '请稍等', 
// 	        msg: '正在导出数据...', 
// 	        text: 'wating.......' 
// 	    });
		
// 		$('#frm').form('submit',{
// 	           url: "${ctx}/districtManager/InstitutionsExportAction.a?export",
// 	           onSubmit: function(){
// 	        	   return true;
// 	           },
// 	           success: function(){
// 	        	   $.messager.progress('close');
// 	           }
// 	       });
// 		$('#frm').attr("action","${ctx}/districtManager/InstitutionsManageAction.a");
		
	}
	
	function downloadFail(fileName){
		var frmObj=$("#frm");
		frmObj.attr("action","${ctx}/sysuserManager/FileAction.a?getFile&fileName=" + fileName);
		frmObj.submit();
		frmObj.attr("action","${ctx}/districtManager/InstitutionsManageAction.a");
	}
	
	/* 处理级联问题 */
	function  initSelect(dictType,sxd,id,v){
		$.ajax({  
			url : '${ctx}/districtManager/InstitutionsManageAction.a?getNameByType&ssxd=' + sxd + '&dictType='+dictType +'&time='+new Date().getTime(),
			data: null,
			dataType : 'json',
			type : 'POST',
			success : function(data) {
				$("#" + id).combobox({
			        url: null,
			        valueField: 'zdxbm',
			        textField: 'zdxmc',
			        editable: false,
			        data: data.dto
			    });
				$("#" + id).combobox("setValue",v);
			}
		});
	}
	
	function init(v1,v2){
		var affiliatedSchool=$('#affiliatedSchool').combobox('getValue');
		if(affiliatedSchool != null && affiliatedSchool != ""){
			initSelect("XXJG_BXLX",affiliatedSchool,"schoolType",v1);
			initSelect("XXJG_JBZLX",affiliatedSchool,"jbzlx",v2);
		}
	}
	
	window.onload=function(){  
	    
		//所属学段 的change事件
		$('#affiliatedSchool').combobox({
	        onSelect:function(record){
	        	var affiliatedSchool=$('#affiliatedSchool').combobox('getValue');
	        	
	        	
	        	if(affiliatedSchool=='30'){
	        		$('#unitType').combobox("enable");
	        	}else{
	        		$('#unitType').combobox("disable");
	        	}
	        	if(affiliatedSchool=='50'){
	        		$.messager.alert('温馨提示','系统不能新增学段为特殊教育的单位，请选择其他类别');
	    			$('#affiliatedSchool').combobox("setValue","");
	        	}
	        	
	        	initSelect("XXJG_BXLX",affiliatedSchool,"schoolType","");
	        	initSelect("XXJG_JBZLX",affiliatedSchool,"jbzlx","");
	        }
		});
		/* 单位类型 */
		$('#unitType').combobox({
	        onSelect:function(record){
	        	if(record.value == 1){
        			$.messager.alert('温馨提示','系统不能新增中小学校，请选择其他类别');
        			$('#unitType').combobox("setValue","");
        		}
	        }
		});
		
		
	    $("#institutionName").next("span").find('input:first').blur(function(){
	    	var name = $("#institutionName").textbox("getValue");
			$.ajax({
			    url:"${ctx}/districtManager/InstitutionsManageAction.a?fetchInstitutionByName&time="+new Date().getTime(),    //请求的url地址
			    dataType:"json",   //返回格式为json
			    //async:true,//请求是否异步，默认为异步，这也是ajax重要特性
			    data:{"jgName":name},    //参数值
			    type:"post",   //请求方式
			    success:function(data){
			    	console.log(data);
			    	if(data.ins==null){
						 
			    	}else{
			    		$.messager.alert('温馨提示','该单位已经存在！');
			    	}
			    },
			    error:function(XMLHttpRequest, textStatus, errorThrown){
					var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
					sessionTimeout(sessionstatus);
				}
			});
		});
	    
	    /*
	    1、所属学段为中小学校，单位类别为中小学，2、所属学段为特殊教育的，从其他系统同步机构信息，不能新增、修改、删除
	    */
	    
	    $('#dg').datagrid({
			onCheck:function(rowIndex,rowData){
				if(rowData.cfgvme.indexOf('特殊教育')>=0){
					$('#dg').datagrid('unselectRow',rowIndex);
					$.messager.alert('温馨提示','学段为特殊教育的不能修改或删除！','info');
					return ;
				}else if(rowData.cfgvme.indexOf('中小学')>=0 && rowData.cme.indexOf('中小学')>=0){
					$('#dg').datagrid('unselectRow',rowIndex);
					$.messager.alert('温馨提示','单位类别为中小学的不能修改或删除！','info');
					return ;
				}
				
			},
			onUncheck:function(rowIndex,rowData){
				
			},
			onLoadSuccess:function(data){
				if(data.rows[0].id==undefined){
					$('#datagrid-row-r2-2-0').html(data.rows[0].ck);
					$('.datagrid-btable').css({'text-align':'center','width':'100%','padding':'30px 0'});
				}
			}
		});
	    
	};
</script>
</body>
</html>
