<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>         
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="system.title"/></title>
<link href="${ctx }/css/becom_global.css" rel="stylesheet" type="text/css" />
<link href="${ctx }/css/becom_library.css" rel="stylesheet" type="text/css" />
<link href="${ctx }/css/becom_master.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../easyui/themes/default/easyui.css">
<script type="text/javascript" src="../easyui/jquery.min.js"></script>
<script type="text/javascript" src="../easyui/easyloader.js"></script>
<script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${ctx}/js/refresh.js"></script>
<style type="text/css">
.style1 {color: #FF0000}
.style2 {font-size: 12px}
.style3 {
	font-size: 14px;
	font-weight: bold;
	color: #003399;
}
</style>
</head>

<body>
<form action="ChangePasswordAction.a" method="POST">
<input type="hidden" name="isClick" value="${isClick}" />
<div id="main">
<div id="titlearea">
  <h1>当前操作：</h1>
  <h2>修改密码</h2>
    <c:if test="${not empty error}"><div id="errormsg">${error}</div></c:if> 
    <c:if test="${not empty message}"><div id="successmsg">${message}</div></c:if>
  </div>
  <div id="searcharea">
<table width="350" border="0" cellspacing="1" cellpadding="0">
  <tr>
    <td height="30" colspan="2"><div align="center" class="style3">修改密码</div></td>
  </tr>
  <tr>
    <td><div align="right" class="style2">原密码：</div></td>
    <td>
      <div align="left">
        <input type="password" name="oldPassword"  id="oldPassword">
        <span class="style1">*</span></div></td>
  </tr>
  <tr>
    <td><div align="right" class="style2">新密码：</div></td>
    <td><div align="left">
      <input type="password" name="newPassword" id="newPassword"/>
      <span class="style1">*</span></div></td>
  </tr>
  <tr>
    <td><div align="right" class="style2">重复新密码：</div></td>
    <td><div align="left">
      <input type="password" name="confirmPassword" id="confirmPassword"/>
      <span class="style1">*</span></div></td>
  </tr>
  <tr>
    <td colspan="2" align="center">
    <input class="btnsearch_mouseout" name="doSave" type="button" id="button" onclick="save()" value=" 确定 " />
    <input class="btnsearch_mouseout" type="button" id="button" value=" 关闭 " onclick="window.close();"/>
    </td>
  </tr>
</table>
</div>
<div id="tips" class="easyui-dialog" title="温馨提示" style="padding:10px 20px;width:220px;height:120px;line-height:40px;text-align:center;overflow:auto;font-size:20px;" closed="true" data-options="modal:true">请选择一条记录</div>
</div>
</form>

<script type="text/javascript">
function save(){
	var oldPassWord=$("#oldPassword").val();
	$.ajax({
	    url:"${ctx}/districtManager/InstitutionsManageAction.a?isOldPassWordRight",    //请求的url地址
	    dataType:"json",   //返回格式为json
	    //async:true,//请求是否异步，默认为异步，这也是ajax重要特性
	    data:{"oldPassWord":oldPassWord},    //参数值
	    type:"post",   //请求方式
	    success:function(data){
	    	if(data.ins==true){
	    		var newPassWord=$("#newPassword").val();
	    		var confirmPassword=$("#confirmPassword").val();
	    		if(newPassWord!=confirmPassword){
	    			$.messager.alert('温馨提示','新密码和确认密码不一致，请重新输入！','info');
	    			return;
	    		}
	    		$.ajax({
	    		    url:"${ctx}/districtManager/InstitutionsManageAction.a?modifyPassWord",    //请求的url地址
	    		    dataType:"json",   //返回格式为json
	    		    //async:true,//请求是否异步，默认为异步，这也是ajax重要特性
	    		    data:{"newPassWord":newPassWord},    //参数值
	    		    type:"post",   //请求方式
	    		    success:function(data){
	    		    	$.messager.alert('温馨提示','密码修改成功！','info',function(){
	    		          	window.location.reload();
	    		  	  	});
	    		    	
	    		    },
	    		    error:function(){
	    				//错误处理
	    			}
	    		});
	    	}else if(data.ins==false){
	    		$.messager.alert('温馨提示','原密码输入错误，请重新输入！','info');
	    		return;
	    	}
	    },
	    error:function(){
			//错误处理
		}
	});
}

</script>
</body>
</html>
