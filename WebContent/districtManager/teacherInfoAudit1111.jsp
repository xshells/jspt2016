<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>北京市教师管理服务平台</title>
    <link rel="stylesheet" type="text/css" href="../css/newcss.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
    <script type="text/javascript" src="../easyui/jquery.min.js"></script>
    <script type="text/javascript" src="../easyui/easyloader.js"></script>
    <script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript">
		
	</script>
</head>
<body class="easyui-layout" style="margin:0 1px;background-color:#f5f5f5 !important; margin-top:20px">
		        <div class="easyui-layout" data-options="fit:true">
					<div data-options="region:'center',border:false">
					<form action="${ctx}/districtManager/TeacherInfoAuditAction.a" id="aduitfrm" method="post">
                    	<div id="anchor66">
    						<div class="easyui-panel" data-options="region:'center',border:false,collapsible:true" iconCls="icon-dingdan" title="查询条件"  style=" width:100%;">
                            	<div id="toolbarxx" style=" border-bottom:1px dotted #ccc">
								<a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="submit()">查 询</a>
								<a href="#" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="reset()">重 置</a>
							    
                                </div>
                                <table id="dg1" class=" fitem" data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,toolbar:'#toolbar',method:'get',checkOnSelect:'true',selectOnCheck:'true'" style="width:800px;height:60px;" >
        								<tr>
                                        	<td>教育ID</td>
                                            <th><input type="text" class="easyui-textbox" name="eduIdQuery" value="${eduIdQuery }"/></th>
                                            <td>证件号码</td>
                                            <th><input type="text" class="easyui-textbox" name="cardNoQuery"  value="${cardNoQuery }"/></th>
                                        </tr>
                                        <tr class="ml20">
                                        	<td>姓名</td>
                                            <th><input type="text" class="easyui-textbox" name="teacherNameQuery" value="${teacherNameQuery }"/></th>
                                            <td>审核状态</td>
                                            <th>
                                            <select class="easyui-combobox"  name="aduitStutasQuery" id="" >
                                            <option value="">请选择</option>
                                            <app:dictselect dictType="JSXX_SHZT" selectValue="${aduitStutasQuery }"/>
                                        </select>
                                            </th>
                                        </tr>
    							</table>
                            
    						</div>
					    </div>
						<div  data-options="region:'center',border:false" style="margin-top:20px;">
                            <table id="dg" title="审核列表" iconCls="icon-dingdan" class="easyui-datagrid" style="width:100%;height:400px;"
								data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,collapsible:true,toolbar:'#toolbar',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
							<!---获取数据--->
							<thead>
								<tr>
									<th field="ck" checkbox="true"></th>
									<th data-options="field:'id',width:40">序 号</th>
                                    <th data-options="field:'shencha',width:100">审核状态</th>
                                    <th data-options="field:'scha',width:100">审核内容</th>
                                    <th data-options="field:'chachong',width:100">教育ID</th>
									<th data-options="field:'name',width:60">姓 名</th>
                                    <th data-options="field:'named',width:60">性 别</th>
									<th data-options="field:'sex',width:130">证件号码</th>
                                    <th data-options="field:'i',width:100">出生日期</th>
									<th data-options="field:'imef',width:100">申请日期</th>
								</tr>
							</thead>
							<tbody>
							<c:if test="${not empty pageObj.pageElements}">
									<c:forEach items="${pageObj.pageElements}" var="item" varStatus="status">
										<tr>
											<td>${item.jsId }</td>
											<td>${item.jsId }</td>
											<td><app:dictname dictid="JSXX_SHZT${item.aduitStutas }"/></td>
											<td>审核意见</td>
											<td>${item.eduId }</td>
											<td><a style="color:#19aa8d; text-decoration:none" href="teacher-shenhe-xiangxi.html">${item.teacherName }</a></td>
											<td>${item.sex }</td>
											<td>${item.cardNo }</td>
											<td>${item.birthDay }</td>
											<td>${item.aduitDay }</td>
										</tr>
									</c:forEach>
								</c:if>
								<c:if test="${empty pageObj.pageElements}">
									<tr>
										<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
									</tr>
								</c:if>
							</tbody>
							</table>
							<div id="toolbar">
								<a href="#" class="easyui-linkbutton" iconCls="icon-filter" plain="true" onclick="aduit()">审 核</a>
								<a href="#" class="easyui-linkbutton" iconCls="icon-filter" plain="true" onclick="aduitAll()">全部审核</a>
							</div>
							<!-- 分页所必须的 -->
							<div class="page tar mb15">
								<input name="pageNo" value="${pageNo }" type="hidden" />
								<c:if test="${not empty pageObj.pageElements}">
									<jsp:include page="../common/pager-nest.jsp">
										<jsp:param name="toPage" value="1" />
										<jsp:param name="showCount" value="1" />
										<jsp:param name="action" value="doSearch" />
									</jsp:include>
								</c:if>
							</div>
							</form>
                            <div id="dlgZH" class="easyui-dialog" title="审核" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        
                                        <div id="table-content">
                                        <form id="fm" action="" method="post" novalidate>
                                        <table class="fitem clean">
                                            <tbody>
                                            <tr>
									            <td>审核结果：</td>
									            <th>
									                <select class="easyui-combobox" required="true" name="isAdopt" id="">
                                                		<option value="">请选择</option>
									                	<app:dictselect dictType="JSXX_SHJG" />
									                </select>
									            </th>
									        </tr>
									        <tr>
									            <td>审核意见：</td>
									            <td>
									                <textarea style="width:400px; line-height:24px; padding:6px; text-indent:24px; height:100px" name="aduitOpinion"></textarea>
									            </td>
									        </tr>
                                            </tbody>
                                        </table>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgZH').dialog('close')">取消</a>
                                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>  
<script type="text/javascript">

var teatchId=[];
$('#dg').datagrid({
	onCheck:function(rowIndex,rowData){
		teatchId.push(rowData.id);
	}
})



var url;
//审核保存
function saveUser(){
	$('#fm').form('submit',{
		url: "${ctx}/districtManager/TeacherInfoAuditAction.a?aduit&aduitOpinion="++"&isAdopt="+,
		onSubmit: function(){
			return $(this).form('validate');
		},
		success: function(result){
		}
	});
}

function save_user(dlgNmae){
    $(dlgNmae).dialog('open');
    $('#fm').form('clear');
}
//审核弹框
function aduit(){
    save_user('#dlgZH');
}

function addTab(title, url){
	if ($('#tb').tabs('exists', title)){
		$('#tb').tabs('select', title);
	} else {
		var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
		$('#tb').tabs('add',{
			title:title,
			content:content,
			closable:true
		});
	}
}
//关闭所有的tab  
    function closeAll(){  
        var tiles = new Array();  
          var tabs = $('#tb').tabs('tabs');      
          var len =  tabs.length;           
          if(len>0){  
            for(var j=0;j<len;j++){  
                var a = tabs[j].panel('options').title;               
                tiles.push(a);  
            }  
            for(var i=1;i<tiles.length;i++){               
                $('#tb').tabs('close', tiles[i]);  
            }  
          }  
    } 
//关闭当前的tab 
	function removePanel(){
		var tab = $('#tb').tabs('getSelected');
		    if (tab){
			var index = $('#tb').tabs('getTabIndex', tab);
			$('#tb').tabs('close', index);
			}
		}
</script>
</body>
</html>