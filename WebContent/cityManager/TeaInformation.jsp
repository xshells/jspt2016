<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>北京市教师管理服务平台</title>
<link rel="stylesheet" type="text/css" href="../css/newcss.css">
<link rel="stylesheet" type="text/css"
	href="../easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
<script type="text/javascript" src="../easyui/jquery.min.js"></script>
<script src="../js/echarts/build/dist/echarts-all.js"></script>
<script src="../js/echarts/build/dist/echarts.js"></script>
<script type="text/javascript" src="../easyui/easyloader.js"></script>
<script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${ctx}/js/refresh.js"></script>
<script type="text/javascript" >
	/* 编辑*/
	function lookUser(tid){
		
		var id=tid;
		$.ajax({
		    url:"${ctx}/schoolManager/InputPInfoManagerAction.a?getTree&time="+new Date().getTime(),    //请求的url地址
		    dataType:"json",
		    data:{tid:id},
		    type:"post",
		    success:function(data){
		    	console.log(data);
		    	$('#tt').tree({
		    		data:data.tree,
		    		onClick:function(node){
		    			addTabD(node.text,window.location.protocol+'//'+window.location.host+node.url);
		    		}
		    	});
		    	$('#tt').tree('collapseAll');
		    	var n = $('#tt').tree('find', data.tree[0].children[0].id);
	          $('#tt').tree('select', n.target);
	          addTabD(n.text,window.location.protocol+'//'+window.location.host+n.url);
		    },
		    error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
		   /*  error:function(){
		        $.messager.confirm('温馨提示', '获取树菜单出错','warning');
		    } */
		});
		
		$('#dlg').dialog('open').dialog('setTitle','编辑信息');
		$('#fm').form('clear');
		closeTabs();
	}
	
	function addTabD(title, url){
		if($('.tabs li').size()>19){
			$('#linkTabs').dialog('open');
			return false;
		}
		if ($('#tb').tabs('exists', title)){
			$('#tb').tabs('select', title);
		} else {
			var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
			$('#tb').tabs('add',{
				title:title,
				content:content,
				closable:true
			});
		}
	}
	
	function closeTabs(){  
        var tiles = new Array();  
          var tabs = $('#tb').tabs('tabs');      
          var len =  tabs.length;           
          if(len>0){
     	  
     	   for(var j=0;j<len;j++){  
               var a = tabs[j].panel('options').title;               
               tiles.push(a);  
           }
            for(var i=0;i<tiles.length;i++){               
                $('#tb').tabs('close', tiles[i]);  
            }  
          }  
    }
	</script>
</head>
<div id="anchor4">
	<div data-options="region:'center',border:false,collapsible:true"
		style="margin-bottom: 20px;">
		<form action="${ctx}/cityManager/TeacherInformatAction.a" id="frm" method="post">
		<div id="anchor66">
		<div class="easyui-panel" data-options="region:'center',border:false,collapsible:true" iconCls="icon-dingdan" title="查询条件"  style=" width:100%;">
            <div id="toolbarxx" style=" border-bottom:1px dotted #ccc">
				<a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="querySub()">查 询</a>
				<a  class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="rest()">重 置</a>
            </div>
              <table id="dg1" class=" fitem" data-options="idField:'id',rownumbers:false,fitColumns:true,pagination:true,singleSelect:false,toolbar:'#toolbar',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
					<tr>
                    	<td>所在区</td>
                        <th>
                        <select  class="easyui-combobox"  name="disCodeQuery" id="disCodeQuery" value="" >
                        	<option value="">-请选择-</option>
	                       <c:forEach items ="${disCodes}" var="con">
	                       	<option value="${con.disCode}" <c:if test="${disCodeQuery eq con.disCode}">selected</c:if>>${con.disName}</option> 
	                       </c:forEach>
                   		</select>
                        </th>
                        <td>所属学段</td>
                        <th>
                             <select class="easyui-combobox"  name="affiliatedSchoolQuery" id="affiliatedSchoolQuery" value="${affiliatedSchoolQuery }" >
                                <option value="">请选择</option>
                                <app:dictselect dictType="XXJG_SSXD" selectValue="${affiliatedSchoolQuery }"/>
                            </select>
                        </th>
                        <td>单位类别</td>
                        <th>
                             <select class="easyui-combobox"  name="unitTypeQuery" id="unitTypeQuery">
                                <option value="">请选择</option>
                                <app:dictselect dictType="XXJG_ZXXFL"  selectValue="${unitTypeQuery }"/>
                            </select>
                        </th>
                        <td>机构名称</td>
                        <th><input type="text" name="institutionNameQuery" class="easyui-textbox" value="${institutionNameQuery }" ></th>
                    </tr>
                    <tr class="ml20">
                    	<td>教育ID</td>
                        <th><input type="text" class="easyui-textbox" name="eduIdQuery" id="eduIdQuery" value="${eduIdQuery}"></th>
                        <td>证件号码</td>
                        <th><input type="text" class="easyui-textbox" name="cardNoQuery" id="cardNoQuery" value="${cardNoQuery}"></th>
                    	<td>姓名</td>
                        <th><input type="text" class="easyui-textbox" name="teacherNameQuery" id="teacherNameQuery" value="${teacherNameQuery}"></th>
                       
                    </tr>
                    
                    
			</table>
		</div>
	    </div>
	    
					    
		<table id="dg" class="easyui-datagrid" title="审核列表"
			iconCls="icon-dingdan" style="width: 100%;"
			data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,collapsible:true,toolbar:'#toolbarZY',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
			<thead>
				<tr>
					<th field="ck" checkbox="true"></th>
					<th data-options="field:'id',width:40">序 号</th>
					<th data-options="field:'shjg',width:100">审核状态</th>
                    <th data-options="field:'shencha',width:100">所在区</th>
                    <th data-options="field:'cfgvme',width:83">所属学段</th>
                    <th data-options="field:'cme',width:83">单位类别</th>
                    <th data-options="field:'nacgvme',width:200">机构名称</th>
                    <th data-options="field:'chachong',width:100">教育ID</th>
					<th data-options="field:'name',width:100">姓 名</th>
                    <th data-options="field:'named',width:100">性 别</th>
					<th data-options="field:'sex',width:130">证件号码</th>
                    <th data-options="field:'i',width:100">出生日期</th>
					<th data-options="field:'imef',width:150">上报日期</th>
				</tr>
			</thead>
			<tbody>
			<c:if test="${not empty pageObj.pageElements}">
				<c:forEach items="${pageObj.pageElements}" var="item" 
					varStatus="status">
					<tr>
						<td>${item.jsId}</td>
						<td>${item.jsId}</td>
						<td><app:dictname dictid="JSXX_SHZT${item.aduitStutas}"/></td>
						<td>${item.disName}</td>
						
						<td><app:dictname dictid="XXJG_SSXD${item.xd}"/></td>
						<td><app:dictname dictid="XXJG_ZXXFL${item.unitType}"/></td>
						<td>${item.jgName}</td>
						
						<td>${item.eduId}</td>
						<td><a style="color:#19aa8d; text-decoration:none" href="javascript:void(0);" onclick="lookUser(${item.jsId });">${item.teacherName }</a></td>
						<td><app:dictname dictid="JSXX_XB${item.sex}"/></td>
						<td>${item.cardNo}</td>
						<td>${item.birthDay}</td>
						<td>${item.aduitDay}</td>
					</tr>
				</c:forEach>
				</c:if>
				<c:if test="${empty pageObj.pageElements}">
					<tr>
						<td colspan="13" class="prompt">未查询到符合条件的记录。</td>
					</tr>
				</c:if>
			</tbody>
		</table>
		
	<div class="page tar mb15">
		<input name="pageNo" value="${pageNo }" type="hidden" />
		<c:if test="${not empty pageObj.pageElements}">
			<jsp:include page="../common/pager-nest.jsp">
				<jsp:param name="toPage" value="1" />
				<jsp:param name="showCount" value="1" />
				<jsp:param name="action" value="doSearch" />
			</jsp:include>
		</c:if>
	</div>
</form>

	<div id="dlg" class="easyui-dialog" title=" " style="width:1100px;height:610px; overflow:auto;padding:2px" closed="true" data-options="modal:true">
                	<div class="easyui-layout" fit="true">
			<div region="west" split="true" style="width:150px;">
				<ul id="tt" class="easyui-tree"></ul>
			</div>
			<div region="center" border="false" border="false">
				<div id="tb" class="easyui-tabs" fit="true">
				</div>
			</div>
			<div id="linkTabs" class="easyui-dialog" closed="true" data-options="modal:true">标签页开启数量已达到最大上限</div>
		</div>
	</div>

</div>
</div>

<script type="text/javascript">

$('#qxdm').combobox({ editable:false }); //所在区
//表单提交
function querySub(){
	$("#frm").submit();
}
//清空表单
function rest(){
	/* $("input[name='edu_id']").html("");
	$("#sfzjh").val("");//身份证号码
	$("#xm").val("");//姓名
	$("select").val(-1); */
	window.location.href="${ctx}/cityManager/TeacherInformatAction.a";
}
</script>
