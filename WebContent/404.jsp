<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="x-ua-compatible" content="ie=7" /> 
<meta name="Keywords" content="" />
<meta name="Description" content="" />
<meta name="robots" content="index，follow" />
<meta name="googlebot" content="index，follow" />
<meta name="author" content="becom"/>
<meta name="copyright" content="北控软件版权所有"/>
<title>北京市教师管理信息系统</title>
<link type="text/css" rel="stylesheet" href="${ctx}/css/style.css" />
<script type="text/javascript" src="${ctx}/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="${ctx}/js/desp_public.js"></script>
<script type="text/javascript">
  window.onload = function(){
    menu();
  }
  </script>
</head>

<body>

<div style="margin-top: 10px; font-size: 12px; margin-right: 20px" id="MarRight">
    <div class="content">
    	<div class="location">当前位置：<a href="#">404页面</div>
		<div class="err404">
        	<p><img src="images/404.png" width="280" height="250" /></p>
            <div class="rowbox mt20">
       		</div>
        </div>
    </div>
</div>

</body>
</html>
