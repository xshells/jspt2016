<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div id="base" class="easyui-dialog hide"  title="修改密码" style="padding:10px 20px;width:500px;line-height:40px;text-align:center;overflow:auto;font-size:20px;">
	<div class="easyui-layout">
		<div class="easyui-panel">
			<div style="margin-bottom:20px">
				<lable>请输入旧密码：</lable>
				<input class="easyui-textbox" data-options="prompt:'请输入旧密码'" style="width:80%;height:32px" name="oldPassWord" id="oldPassword"/>
			</div>
			<div style="margin-bottom:20px">
				<lable>请输入新密码：</lable>
				<input class="easyui-textbox" data-options="prompt:'请输入新密码'" style="width:80%;height:32px" name="newPassWord" id="newPassword"/>
			</div>
			<div style="margin-bottom:20px">
				<lable>请确认新密码：</lable>
				<input class="easyui-textbox" data-options="prompt:'请确认新密码'" style="width:80%;height:32px"  name="confirmPassword" id="confirmPassword"/>
			</div>
			<div>
				<a href="#" class="easyui-linkbutton" iconCls="icon-ok" style="width:46%;height:32px;margin-right:10px;" onclick="save()">保存</a>
				<a href="#" class="easyui-linkbutton" iconCls="icon-ok" style="width:50%;height:32px" onclick="javascript:$('#base').dialog('close')">关闭</a>
			</div>
		</div>
	</div>
</div>