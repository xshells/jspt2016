<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>北京市教师管理服务平台</title>
    <link rel="stylesheet" type="text/css" href="../css/newcss.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
    <script type="text/javascript" src="../easyui/jquery.min.js"></script>
    <script src="../js/echarts/build/dist/echarts-all.js"></script>
    <script src="../js/echarts/build/dist/echarts.js"></script>
    <script type="text/javascript" src="../easyui/easyloader.js"></script>
    <script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx}/js/refresh.js"></script>
    <script type="text/javascript" src="../My97DatePicker/WdatePicker.js"></script>
    <link rel="stylesheet" type="text/css" href="../My97DatePicker/skin/WdatePicker.css">
 </head>   
<body>
<div id="anchor3">
	  <div data-options="region:'center',border:false,collapsible:true" style="margin-bottom:20px;">
                                <div id="toolbarGZ">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUserGZ()">新 增</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUserGZ()">编 辑</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUserGZ()">删 除</a>
                                 <span style="color:red">注意:从参加工作起开始填写，填写至当前所在单位</span>   
                                </div>
                                <form action="${ctx}/basicInformation/TeacherWorkAction.a" id="frmGZ" method="post">
                                <input type="hidden" value="${tbJzgxxsh.shzt}" id="xxshzt"/>
                                <input type="hidden" value="${tbBizJzgjbxx.jsid}" name="jsid" id="jsid"/>
    							<input type="hidden" value="${tbBizJzgjbxx.tbXxjgb.ssxd}" name="ssxd" id="ssxd"/>
                                <table id="dgGZ" title="工作经历" iconCls="icon-dingdan" class="easyui-datagrid"
                                    data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,collapsible:true,toolbar:'#toolbarGZ',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
                                
                                <thead>
                                    <tr>
                                        <th field="ck" checkbox="true"></th>
                                        <th data-options="field:'id',width:100">任职单位名称</th>
                                        <th data-options="field:'shencha',width:100">任职开始年月</th>
                                        <th data-options="field:'chachong',width:100">任职结束年月</th>
                                        <th data-options="field:'name',width:100">单位性质类别</th>
                                        <th data-options="field:'named',width:100">任职岗位</th>
                                        <th data-options="field:'sex',width:100">创建时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   <c:if test="${not empty pageObj.pageElements}">
										<c:forEach items="${pageObj.pageElements}" var="item"
											varStatus="status">
											<tr>
											    <td>${item.gzjlId }</td>
                                        		<td>${item.rzdwmc}</td>
                                        		<td>${item.rzksny}</td>
                                        	 	<td>${item.rzjsny}</td>
                                        		<td><app:dictname dictid="JSXX_DWXZLB${item.dwxzlb}"/></td>
                                        		<td>${item.rzgw}</td>
                                        		<td>${item.cjsj}</td>
                                    		</tr>
										</c:forEach>
									</c:if>
									<c:if test="${empty pageObj.pageElements}">
											<tr>
												<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
											</tr>
									</c:if>
                                       
                                </tbody>
                                </table>
                               
                                  <div class="page tar mb15">
											<input name="pageNo" value="${pageNo }" type="hidden" />
									<c:if test="${not empty pageObj.pageElements}">
										<jsp:include page="/common/pager-nest.jsp">
											<jsp:param name="toPage" value="1" />
											<jsp:param name="showCount" value="1" />
											<jsp:param name="action" value="doSearch" />
										</jsp:include>
									</c:if>
								 </div> 
								 </form> 
								 
								<!-- 新增工作经历 --> 
                                <div id="dlgGZ" class="easyui-dialog" title="新增工作经历" style="width:700px;height:300px; overflow:auto;padding:0px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        
                                        <div id="table-content">
                                        <form id="fm" method="post" novalidate>
                                          <input type="hidden" name="jsid" value="${tbBizJzgjbxx.jsid}"/>
                                         <input type="hidden" value="${tbBizJzgjbxx.tbXxjgb.ssxd}" name="ssxd"/>
                                        <table class="fitem">
                                            <tbody>
                                                <tr>
                                                    <td>任职单位名称：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="rzdwmc" id="rzdwmc" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>任职开始年月：</td>
                                                    <td>
                                                   		 <input id="rzksny" name="rzksny" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM',maxDate:'#F{$dp.$D(\'rzjsny\')}'})"/>
                                                       <!--  <input type="text" class="easyui-datebox" name="rzksny" id="rzksny" required> -->
                                                    </td>
                                                    <td>任职结束年月：</td>
                                                    <td>
                                                    <input id="rzjsny" name="rzjsny" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM',minDate:'#F{$dp.$D(\'rzksny\')}'})"/><span style="color:red" id="addrzjsnydlg"></span>
                                                        <!-- <input type="text" class="easyui-datebox" name="rzjsny" id="rzjsny" required> -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>单位性质类别：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="dwxzlb" id="dwxzlb">
                                            				<app:dictselect dictType="JSXX_DWXZLB"/>
                                                        </select>
                                                    </td>
                                                    <td>任职岗位：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="rzgw" id="rzgw" required>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUserGZ()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgGZ').dialog('close')">取消</a>
                                </div>
                                
                                <!-- 编辑工作经历 -->
                                     <div id="dlgGZEdit" class="easyui-dialog" title="编辑工作经历" style="width:700px;height:300px; overflow:auto;padding:0px 20px" closed="true" buttons="#dlgEdit-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        
                                        <div id="table-content">
                                        <form id="fmEdit" method="post" novalidate>
                                          <input type="hidden" name="jsid" value="${tbBizJzgjbxx.jsid}"/>
                                         <input type="hidden" value="${tbBizJzgjbxx.tbXxjgb.ssxd}" name="ssxd"/>
                                        <input type="hidden" name="gzjlId" id="gzjlIdEdit"/>
                                        <table class="fitem">
                                            <tbody>
                                                <tr>
                                                    <td>任职单位名称：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="rzdwmc" id="rzdwmcEdit" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>任职开始年月：</td>
                                                    <td>
                                                     <input id="rzksnyEdit" name="rzksny" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM',maxDate:'#F{$dp.$D(\'rzjsnyEdit\')}'})"/>
                                                       <!--  <input type="text" class="easyui-datebox" name="rzksny" id="rzksnyEdit" required> -->
                                                    </td>
                                                    <td>任职结束年月：</td>
                                                    <td>
                                                     <input id="rzjsnyEdit" name="rzjsny" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM',minDate:'#F{$dp.$D(\'rzksnyEdit\')}'})"/><span style="color:red" id="addrzjsnydlgs"></span>
                                                        <!-- <input type="text" class="easyui-datebox" name="rzjsny" id="rzjsnyEdit" required> -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>单位性质类别：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="dwxzlb" id="dwxzlbEdit">
                                            				<app:dictselect dictType="JSXX_DWXZLB"/>
                                                        </select>
                                                    </td>
                                                    <td>任职岗位：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="rzgw" id="rzgwEdit" required>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                <div id="dlgEdit-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="editGZ()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgGZEdit').dialog('close')">取消</a>
                                </div>
                                
                                 <div id="tips" class="easyui-dialog" title="温馨提示" style="padding:10px 20px;width:200px;height:100px;line-height:40px;text-align:center;overflow:auto;font-size:20px;" closed="true" data-options="modal:true">请选择一条记录</div>
                            </div>
  </div>        
  <c:if test="${SESSION_ISPUB == '1'}">
    	<div id="btn-wrap">
        <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="submitInfo()">报送</a>
        <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="cancelSubmit()">取消</a><span style="color:red">各项信息填写完整后，才能报送。报送后，不能再次修改教师信息。只有审核不通过的教师信息，才能再次编辑报送。</span>
    </div>
    </c:if>
<script type="text/javascript">
$('#dwxzlb').combobox({ editable:false }); //下拉框禁止输入
$('#dwxzlbEdit').combobox({ editable:false });

$('#dgGZ').datagrid({
	onLoadSuccess:function(data){
		if(data.rows[0].id==undefined){ 
			$('.datagrid-body td[field="id"]').html(data.rows[0].ck);
		}
	}
});

$('#dgGZ').datagrid({
	onLoadSuccess:function(data){
		if(data.rows[0].id==undefined){
			$('#datagrid-row-r3-2-0').html(data.rows[0].ck);
			$('.datagrid-btable').css({'text-align':'center','width':'100%','padding':'30px 0'});
		}
	}
});

var url;
function save_user(dlgNmae){
    $(dlgNmae).dialog('open');
    $('#fm').form('clear');
    <!--url = 'save_user.php';-->
}
//关闭dialog
function save_close(dlgNmae){
    $(dlgNmae).dialog('close');
}

//新增
function newUserGZ(){
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
	    save_user('#dlgGZ');
    }else{
		$.messager.alert('温馨提示','信息审核中或已通过审核，不可添加','warning');
	} 
},
error:function(XMLHttpRequest, textStatus, errorThrown){
	var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
	sessionTimeout(sessionstatus);
}

})
}



    /**
	    *报送
		*/
		function submitInfo(){
	    		 
					$.ajax({
						url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
						data:{},
						dataType : "json",
						success:function(data){
							    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
							    	$.ajax({
										url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?submitted&time='+new Date().getTime(),
										data:{},
										success:function(data){
											$.messager.alert('温馨提示','报送成功','info',function(){
												window.parent.location.reload();
								    	  	});
										},
										error:function(data){
											$.messager.alert('温馨提示','报送失败,请检查各项是否都已填写！','warning');
										}
										
									});   
								}else{
									$.messager.alert('温馨提示','信息已报送，不可重复报送！','warning');
								}  
						},
						error:function(XMLHttpRequest, textStatus, errorThrown){
							var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
							sessionTimeout(sessionstatus);
						}
						
					})
	    		 
	    	 }
	    /**
	    *取消报送
		*/
		function cancelSubmit(){
			 
			$.ajax({
				url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
				data:{},
				dataType : "json",
				success:function(data){
					    if(data.tbJzgxxsh.shzt==02){
					    	$.ajax({
								url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?cancelSubmit&time='+new Date().getTime(),
								data:{},
								success:function(data){
									$.messager.alert('温馨提示','取消成功','info',function(){
						            	
						    	  	});
								},
								error:function(data){
								
								}
								
							});  
						}else{
							$.messager.alert('温馨提示','信息审核中(审核通过)或未报送，不可取消保送！','warning');
						}  
				},
				error:function(XMLHttpRequest, textStatus, errorThrown){
					var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
					sessionTimeout(sessionstatus);
				}
				
			})
				
	    	 }

//弹出编辑框 
  function editUserGZ(){
		$.ajax({
			url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
			data:{},
			dataType : "json",
			success:function(data){
				    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
	var selected=$("input[type='checkbox'][name='ck']:checked");
	if(selected.length<=0){
		$.messager.alert('温馨提示','请选择一条记录','warning');
		return ;
	}else if(selected.length>1){
		$.messager.alert('温馨提示','只能编辑一条记录','warning');
		return ;
	}else{
		var jsid = $('#jsid').val();
		var gzjlId=selected.val();
		$.ajax({
		    url:"${ctx}/basicInformation/TeacherWorkAction.a?toEdit&time="+new Date().getTime(),    //请求的url地址
		    dataType:"json",   //返回格式为json
		    //async:true,//请求是否异步，默认为异步，这也是ajax重要特性
		    data:{"gzjlId":gzjlId,"jsid":jsid},    //参数值
		    type:"post",   //请求方式
		    success:function(data){
	    	console.log(data);
	            $("#gzjlIdEdit").val(data.tbBizJzggzjlxx.gzjlId);
		        $("#rzdwmcEdit").textbox('setValue',data.tbBizJzggzjlxx.rzdwmc);
		        $("#rzksnyEdit").val(data.tbBizJzggzjlxx.rzksny);
		        $("#rzjsnyEdit").val(data.tbBizJzggzjlxx.rzjsny);
		        $("#dwxzlbEdit").combobox('setValue',data.tbBizJzggzjlxx.dwxzlb);
		        $("#rzgwEdit").textbox('setValue',data.tbBizJzggzjlxx.rzgw);
	 	    },
		    error:function(){
		    	$.messager.alert('温馨提示','非本人操作!','warning');
		    }
		});
		save_user('#dlgGZEdit');
	}  
	    }else{
			$.messager.alert('温馨提示','信息审核中或已通过审核，不可编辑','warning');
		} 
},
error:function(XMLHttpRequest, textStatus, errorThrown){
	var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
	sessionTimeout(sessionstatus);
}

})
}	

//编辑工作经历
  function editGZ(){
	  var rzdwmcEdit =$('#rzdwmcEdit').textbox("getValue");
	  if(rzdwmcEdit==""){
			$.messager.alert('温馨提示','单位名称不能为空!','warning');
			return;
		}
	  
	  if(rzdwmcEdit.length > 30){
			$.messager.alert('温馨提示','单位名称超出长度!','warning');
			return;
		}
	  
	  var dwxzlbEdit =$('#dwxzlbEdit').combobox("getValue");
	  if(dwxzlbEdit==""){
			$.messager.alert('温馨提示','单位性质类别不能为空!','warning');
			return;
		}
	  
	  var rzgwEdit =$('#rzgwEdit').textbox("getValue");
	  if(rzgwEdit==""){
			$.messager.alert('温馨提示','任职岗位不能为空!','warning');
			return;
		}
	  
	  if(rzgwEdit.length > 30){
			$.messager.alert('温馨提示','任职岗位超出长度!','warning');
			return;
		}
	
	  var rzksnyEdit= $("#rzksnyEdit").val();
      var rzjsnyEdit= $("#rzjsnyEdit").val();
	
	  if(rzksnyEdit=="" || rzjsnyEdit==""){
			$.messager.alert('温馨提示','任职时间不能为空!','warning');
			return;
		}
	  if(rzksnyEdit>rzjsnyEdit){
			$('#addrzjsnydlgs').text("任职开始时间不能大于等于结束时间!");
			return;
		}
	
  	$('#fmEdit').form('submit',{
          url: '${ctx}/basicInformation/TeacherWorkAction.a?edit',
          onSubmit: function(){
              return $(this).form('validate');
          },
          success: function(){
              save_close("#dlgGZEdit");      // close the dialog
              $.messager.alert('温馨提示','编辑成功','info',function(){
              	window.location.reload();
      	  	});
          },
          error:function(){
        	  $.messager.alert('温馨提示','非本人操作!','warning');
        	 
          }
      });
  }
	
	
 function removeUserGZ(){
	 
		var selected=$("input[type='checkbox'][name='ck']:checked");
		if(selected.length<=0){
			$.messager.alert('温馨提示','请选择记录','warning');
			return;
		}
		$.ajax({
			url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
			data:{},
			dataType : "json",
			success:function(data){
				    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
	 	 var strSel = '';
	     $("input[name='ck']:checked").each(function(index, element) {
	          strSel += $(this).val()+",";
	      }); 
	      var arrCk = strSel.substring(0,strSel.length-1); 
	      var jsid = $('#jsid').val();
	      var ssxd = $('#ssxd').val();
	      $.messager.confirm('温馨提示', '请确认是否删除信息？', function(r){
	      if(r){
			window.location.href ="${ctx}/basicInformation/TeacherWorkAction.a?deleteGz&ck="+arrCk+"&jsid="+jsid+"&ssxd="+ssxd;
			 $.messager.alert('温馨提示','删除成功','info',function(){
             	window.location.reload();
     	  	});
		}
	      });
	    }else{
			$.messager.alert('温馨提示','信息审核中或已通过审核，不可删除','warning');
		} 
},
error:function(XMLHttpRequest, textStatus, errorThrown){
	var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
	sessionTimeout(sessionstatus);
}
})
	}

//保存
function saveUserGZ(){
	var rzdwmc =$('#rzdwmc').textbox("getValue");
	
	if(rzdwmc==""){
		$.messager.alert('温馨提示','单位名称不能为空!','warning');
		return;
	}
	
	if(rzdwmc.length > 30){
		$.messager.alert('温馨提示','单位名称超出长度!','warning');
		return;
	}
	
	var dwxzlbEdit =$('#dwxzlb').combobox("getValue");
	  if(dwxzlbEdit==""){
			$.messager.alert('温馨提示','单位性质类别不能为空!','warning');
			return;
		}
	
	var rzgwEdit =$('#rzgw').textbox("getValue");
	  if(rzgwEdit==""){
			$.messager.alert('温馨提示','任职岗位不能为空!','warning');
			return;
		}
	  
	  if(rzgwEdit.length > 30){
			$.messager.alert('温馨提示','任职岗位超出长度!','warning');
			return;
		}
	
	var rzksny =$('#rzksny').val();;
	var rzjsny =$('#rzjsny').val();;
	if(rzksny=="" || rzjsny==""){
		$.messager.alert('温馨提示','任职时间不能为空!','warning');
		return;
	}
	if(rzksny>rzjsny){
		$('#addrzjsnydlg').text("任职开始时间不能大于等于结束时间!");
		return;
	}
	
	$.messager.confirm('温馨提示', '确认保存？', function(r){
		 if(r){
			 
			$('#fm').form('submit',{
				
		        url: '${ctx}/basicInformation/TeacherWorkAction.a?add',
		        onSubmit: function(){
		            return $(this).form('validate');
		        },
		        success: function(){
		            save_close("#dlgGZ");      // close the dialog
		            $.messager.alert('温馨提示','保存成功','info',function(){
		            	window.location.reload();
		    	  	});
		        },
		        error:function(){
		        	$.messager.alert('温馨提示','非本人操作!','warning');
		        }
		    });
	
		 }
	 });
}
//搜索
function doSearch(){
    $('#search').dialog('open');
}

function addTab(title, url){
	if ($('#tb').tabs('exists', title)){
		$('#tb').tabs('select', title);
	} else {
		var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
		$('#tb').tabs('add',{
			title:title,
			content:content,
			closable:true
		});
	}
}
//关闭所有的tab  
    function closeAll(){  
        var tiles = new Array();  
          var tabs = $('#tb').tabs('tabs');      
          var len =  tabs.length;           
          if(len>0){  
            for(var j=0;j<len;j++){  
                var a = tabs[j].panel('options').title;               
                tiles.push(a);  
            }  
            for(var i=1;i<tiles.length;i++){               
                $('#tb').tabs('close', tiles[i]);  
            }
          }
    } 
//关闭当前的tab 
	function removePanel(){
		var tab = $('#tb').tabs('getSelected');
		    if (tab){
			var index = $('#tb').tabs('getTabIndex', tab);
			$('#tb').tabs('close', index);
			}
		}

</script>			
</body>
</html>
