<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="${ctx}/css/newcss.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/easyui/themes/xyy.css">
    <script type="text/javascript" src="${ctx}/easyui/jquery.min.js"></script>
    <script src="${ctx}/js/echarts/build/dist/echarts-all.js"></script>
    <script src="${ctx}/js/echarts/build/dist/echarts.js"></script>
    <script type="text/javascript" src="${ctx}/easyui/easyloader.js"></script>
    <script type="text/javascript" src="${ctx}/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="${ctx}/easyui/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="${ctx}/js/refresh.js"></script>
	<script type="text/javascript" src="../My97DatePicker/WdatePicker.js"></script>
	<link rel="stylesheet" type="text/css" href="../My97DatePicker/skin/WdatePicker.css">
	<script type="text/javascript">
	var myDate = new Date();
	var year = myDate.getFullYear();///业务上的默认年
	var mouth = myDate.getMonth() + 1;
	var day = myDate.getDate();
	function setDefaultYear(){
		$("#defaultYear").val(year+"-"+mouth+"-"+day);
	}
</script>
</head>
<body>
         <form action="${ctx}/basicInformation/TeacherMoralityAction.a" id="fm1" method="post" novalidate> 
		<div class="content" id="userList">
 			<div class="easyui-panel">
 				<div id="anchor70">
                            <div data-options="region:'center',border:false,collapsible:true" style="margin-bottom:20px;">
                                <div id="toolbarSDTwo">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUserCF()">新 增</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUserCF()">编 辑</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUserCF()">删 除</a>
                                    
                                </div>
                                <input type="hidden" value="${tbJzgxxsh.shzt}" id="xxshzt"/>
                                <table id="cf" class="easyui-datagrid" title="处分信息" iconCls="icon-dingdan"
                                    data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,collapsible:true,toolbar:'#toolbarSDTwo',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
                                
                                <thead>
                                    <tr>
                                        <th field="ck" checkbox="true"></th>
                                        <th data-options="field:'chachong',width:100">处分类别</th>
                                        <th data-options="field:'name',width:100">处分原因</th>
                                        <th data-options="field:'named',width:100">处分发生日期</th>
                                        <th data-options="field:'sex',width:100">创建时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:if test="${empty pageObj.pageElements}">
									<tr>
										<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
									</tr>
								</c:if>
								<div class="page tar mb15">
									<c:if test="${not empty pageObj.pageElements}">
									<c:forEach items="${pageObj.pageElements}" var="item"
										varStatus="status">
										<tr>
										<%-- <td field="ck" checkbox="true">k${item.khid}j${item.jsid }</td> --%>
										<td field="ck" checkbox="true">${item.cfid}</td>
											<%-- <td field="ck" checkbox="true">k${item.khid}j${item.jsid }</td> --%>
											<input type="hidden"  name="jsid" id="jsid" value="${item.jsid }">
											<input type="hidden"  name="cfid" id="cfid" value="${item.cfid}">
											 <td><app:dictname dictid="JSXX_CFLB${item.cflb}" /></td>
											 <td><appadd:dictnameadd dictid="JSXX_CFYY" dictbm="${item.cfyy}" schoolType="${ssxd}"/>
											 
											 </td>
											<%-- <td><app:dictname dictid="JSXX_CFYY${item.cfyy}" /></td> --%>
											<td>${item.ccfsrq}</td> 
											<td>${item.cjsj}</td>
										</tr>
									</c:forEach>
									
									</c:if>
								</div>
                                </tbody>
                                </table>
                                  <div id="dlgCF" class="easyui-dialog" title="新增处分信息" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        
                                        <div id="table-content">
                        
                                        <table class="fitem clean">
                                            <tbody>
                                                <tr>
                                                    <td>处分类别：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="cflb" id="cflb" value="${dto.cflb}">
                                                            <app:dictselect dictType="JSXX_CFLB" />
                                                        </select>
                                                    </td>
                                                    <td>处分原因：</td>
                                                    <td>
                                                        <select  class="easyui-combobox" required="true" name="cfyy" id="cfyy" value="${dto.cfyy}">
                                                           <%-- <app:dictselect dictType="JSXX_CFYY" /> --%>
                                                           <appadd:dictselectadd dictType="JSXX_CFYY" schoolType="${ssxd}" />
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>处分发生日期：</td>
                                                    <td>
                                                        <input type="text" class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',maxDate:'#F{$dp.$D(\'defaultYear\')}'})" name="ccfsrq" id="ccfsrq" value="${dto.ccfsrq}">
                                                        <input type="hidden" id="defaultYear"/>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                   
                                        </div>
                                    </div>
                                </div>
                                <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUserCF()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgCF').dialog('close')">取消</a>
                                </div>
                            </div>
					<div class="page tar mb15">
						<input name="pageNo" value="${pageNo }" type="hidden" />
						<c:if test="${not empty pageObj.pageElements}">
							<jsp:include page="../common/pager-nest.jsp">
								<jsp:param name="toPage" value="1" />
								<jsp:param name="showCount" value="1" />
								<jsp:param name="action" value="doSearch" />
							</jsp:include>
						</c:if>
					</div>
                 </div>

		</div>
<div id="tips" class="easyui-dialog" title="温馨提示" style="padding:10px 20px;width:200px;height:100px;line-height:40px;text-align:center;overflow:auto;font-size:20px;" closed="true" data-options="modal:true">请选择一条记录</div>

		<!-- 新增 -->
		<!-- <div class="content" style="display: none;" id="addUser">
			<div class="modelbox modelbox3">
				<div class="rowbox mb10 wper15">
					<input type="submit" value="保存" class="btnyellow fl"
						onclick="return saveUser();" /> <input type="button" value="返回"
						class="btnyellow fr" onclick="return hideAdd();" />
				</div>
			</div>
		</div> -->
  </form> 

<script type="text/javascript">

/* $(function(){
				$.ajax({
					url:'${ctx}/basicInformation/PunishmentInfoAction.a&time='+new Date().getTime(),
					success:function(data){
						console.log("success");
					},
					error:function(XMLHttpRequest, textStatus, errorThrown){
						var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
						sessionTimeout(sessionstatus);
					}
				})
}) */


var url;
function save_user(dlgNmae){
    $(dlgNmae).dialog('open');
    $('#fm').form('clear');
    setDefaultYear();
}
//新增
function newUserCF(){
	$('#cfyy').combobox({ editable:false }); //下拉框禁止输入
	$('#cflb').combobox({ editable:false }); //下拉框禁止输入
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22||data.tbJzgxxsh.shzt==02){
	$('#cfyy').combobox("clear");
	$('#cfyy').combobox("disable");
	$('#ccfsrq').val("");
	$('#ccfsrq').attr("disabled","disabled");
	 $("#cflb").combobox('setValue',"0"); //处分类别
		var cflb=$('#cflb').combobox('getValue');
	 if (cflb!="0") {
		 var d = new Date()
		 var vYear = d.getFullYear();
		 var vMon = d.getMonth() + 1;
		 var vDay = d.getDate();
		 $("#ccfsrq").val(vYear+"-"+vMon+"-"+vDay);
	}
     save_user('#dlgCF'); 
			    }else{
					$.messager.alert('温馨提示','信息审核中或已通过审核，不可添加','warning');
				} 
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}

			})
}
//编辑
function editUserCF(){
	$('#cfyy').combobox({ editable:false }); //下拉框禁止输入
	$('#cflb').combobox({ editable:false }); //下拉框禁止输入
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22||data.tbJzgxxsh.shzt==02){
	var selected=$("input[type='checkbox'][name='ck']:checked");
		
		if(selected.length<=0){
			$.messager.alert('温馨提示','请选择一条记录','warning');
			return ;
		}else if(selected.length>1){
			$.messager.alert('温馨提示','只能编辑一条记录','warning');
			return ;
		}
	      var cfid = selected.val();
		  var row = $('#cf').datagrid('getSelected');
	      if (row){
	          $('#dlgCF').dialog('open').dialog('setTitle','编辑用户');
	          $('#fm1').form('load',row);
	          $.ajax({
		      	url : '${ctx}/basicInformation/PunishmentInfoAction.a?doQuery&cfid='+cfid+'&time='+new Date().getTime(),
		      	dataType: 'json',
		      	success:function(data){
					 $("#cflb").combobox('setValue',data.dto.cflb); //处分类别
					$("#cfyy").combobox('setValue',data.dto.cfyy); //处分原因
					 $("#ccfsrq").val(data.dto.ccfsrq); 
		      	}
		      });
	      }
			    }else{
					$.messager.alert('温馨提示','信息审核中或已通过审核，不可编辑','warning');
				} 
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}

			})
	}
//关闭
function save_close(dlgNmae){
	$(dlgNmae).dialog('close');
}
//删除
function removeUserCF(){
	var selected=$("input[type='checkbox'][name='ck']:checked");
	
	if(selected.length<=0){
		return $.messager.alert('温馨提示','请选择一条记录','warning');
	}
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22||data.tbJzgxxsh.shzt==02){
 	 var strSel = '';
     $("input[name='ck']:checked").each(function(index, element) {
          strSel += $(this).val()+",";
      }); 
      var arrCk = strSel.substring(0,strSel.length-1); 
	$.messager.confirm('温馨提示', '请确认是否删除信息？', function(r){
		if (r){
			$.messager.alert('温馨提示','删除成功','info',function(){
	window.location.href ="${ctx}/basicInformation/PunishmentInfoAction.a?delPunishmentInfo&ck="+arrCk;
			  	});
		}
	});
			    }else{
					$.messager.alert('温馨提示','信息审核中或已通过审核，不可删除','warning');
				} 
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}

			})
}
//保存
function saveUserCF(){
	var id="";
	var ckData = $('#cf').datagrid('getSelected');
/* 	var id = ckData.ck.match(/\d/g); */
if(ckData){
	id = ckData.ck;	
}else{
	id="";
}
    var cfid=$("input[type='hidden'][name='cfid']").val();
    var khny=$("input[name='khny']").val();
    var khjl=$("input[type='hidden'][name='khjl']").val();
    var sdkhdwmc=$("input[type='hidden'][name='sdkhdwmc']").val();
	var cflb=$("input[type='hidden'][name='cflb']").val();
	var cfyy=$("input[type='hidden'][name='cfyy']").val();
	var ccfsrq=$("input[name='ccfsrq']").val();
	if (cflb!=0) {
		if(!cfyy){
			$.messager.alert('温馨提示','处分原因不能为空！','warning');
			return;
		}
		if(!ccfsrq){
			$.messager.alert('温馨提示','处分发生日期不能为空！','warning');
			return;
		}
	}
	$.messager.confirm('温馨提示', '请确认是否保存信息？', function(r){
		if (r){
			if(id&&id!=""){
				$('#fm1').form('submit',{
			        url: "${ctx}/basicInformation/PunishmentInfoAction.a?editPunishmentInfo&khid="+id+"&cfid="+id+"&khny="+khny+"&khjl="+khjl+"&cflb="+cflb+"&cfyy="+cfyy+"&ccfsrq="+ccfsrq+"&cfid="+cfid+"&sdkhdwmc="+sdkhdwmc,
			        onSubmit: function(){
			            return $(this).form('validate');
			        },
			        success: function(){
			        	save_close('#dlgCF');     // close the dialog
			        	$.messager.alert('温馨提示','编辑成功','info',function(){
			    	      	window.location.reload();
			    		  	});
			                /* $.messager.show({
			                    title: 'Error',
			                }); */
			        }
			    }); 
				
			}else{
		     $('#fm1').form('submit',{	   
		    	 url: "${ctx}/basicInformation/PunishmentInfoAction.a?addPunishmentInfo&khid="+id+"&cfid="+id+"&khny="+khny+"&khjl="+khjl+"&cflb="+cflb+"&cfyy="+cfyy+"&ccfsrq="+ccfsrq+"&cfid="+cfid+"&sdkhdwmc="+sdkhdwmc,
			        onSubmit: function(){
			            return $(this).form('validate');
			        },
		        
		        success: function(){
		        	save_close('#dlgCF');     // close the dialog
		        	 $.messager.alert('温馨提示','保存成功','info',function(){
		     	      	window.location.reload();
		     		  	}); 
		                /* $.messager.show({
		                    title: 'Error',
		                }); */
		        }
		    });  
		    
		}  
		}
	});
	
    
}
//提示信息
function tips(dlgNmae,msg){
	$(dlgNmae).html(msg).dialog('open');
}
$('#cf').datagrid({
	onLoadSuccess:function(data){
		if(data.rows[0].chachong==undefined){
			$('#datagrid-row-r2-2-0').html(data.rows[0].ck);
			$('.datagrid-btable').css({'text-align':'center','width':'100%','padding':'30px 0'});
		}
	}
});
//是否双肩挑事件
$('#cflb').combobox({
    	onSelect:function(record){
    	var cflb=$('#cflb').combobox('getValue');
    	if(cflb=='0'){
    		$('#cfyy').combobox("clear");
    		$('#cfyy').combobox("disable");
    		$('#ccfsrq').val("");
    		$('#ccfsrq').attr("disabled","disabled");
    	}else{
    		$('#cfyy').combobox("enable");
    		$('#ccfsrq').removeAttr("disabled");
    		 var d = new Date()
			 var vYear = d.getFullYear();
			 var vMon = d.getMonth() + 1;
			 var vDay = d.getDate();
			 $("#ccfsrq").val(vYear+"-"+vMon+"-"+vDay);
    	}
    }
});
</script>
   
</body>
</html>
