<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>北京市教师管理服务平台</title>
    <link rel="stylesheet" type="text/css" href="../css/newcss.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
    <script type="text/javascript" src="../easyui/jquery.min.js"></script>
    <script src="../js/echarts/build/dist/echarts-all.js"></script>
    <script src="../js/echarts/build/dist/echarts.js"></script>
    <script type="text/javascript" src="../easyui/easyloader.js"></script>
    <script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx}/js/refresh.js"></script>
    <script type="text/javascript" src="${ctx}/js/upload.js"></script>
    <script type="text/javascript" src="../My97DatePicker/WdatePicker.js"></script>
    <link rel="stylesheet" type="text/css" href="../My97DatePicker/skin/WdatePicker.css">
     <script type="text/javascript">
    var serverPath = '${ctx}';
    function tishi(value){
		$.messager.alert('系统提示',value,'warning');
	}
    function upload(){
		$.ajax({
			url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
			data:{},
			cache: false,
			dataType : "json",
			success:function(data){
				    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
				    	var formData = new FormData($("#fileform2")[0]);
				    	$.ajax({
				    		type : "post",
				    		url : "${ctx}/sysuserManager/FileAction.a?upload=aaa&time="+new Date().getTime(),
				    		dataType : "json",
				    		data : formData,
				    		async : false,
				    		cache: false,
				            contentType: false,
				            processData: false,
				    		success : function(data) {
				   	        	tishi(data.result.msg);
				   	            if(data.result.status == "1"){
				   	            	$("#grzp").val(data.result.filepath);
				   	            	$("#img").attr("src",serverPath + data.result.filepath);
				   	            	$('#updlg').dialog('close');
				   	            }
				    		},
				    		error : function(XMLHttpRequest, textStatus, errorThrown){
				              $.messager.alert('温馨提示','出错了！','error');
				           }
				    	});
				    	return false;;  
				    }else{
						$.messager.alert('温馨提示','信息审核中或已通过审核，不可上传照片','warning');
					} 
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
			
		})
    }
    
    
/* var dzzw=$('#dzzw').combobox('getValue');
	
	if(dzzw == '00' && record.value != '00'){
		$('#dzzw').combobox("clear");
		$('#dzzw').combobox("setValue",record.value);
	}
	 
	if(record.value == '00'){
 		$('#dzzw').combobox("clear");
		$('#dzzw').combobox("setValue",record.value); 
		
		$('#rzksny').attr("disabled",true);
		$('#rzksny').val("");
		$('#dzjb').combobox("clear");
		$('#dzjb').combobox("disable");
	}else{
		$('#rzksny').attr("disabled",false);
		$('#dzjb').combobox("enable");
		$('#dzjb').combobox('setValue',"");
	}

*/
    </script>
 </head>   
<body>
 <div id="anchor1">
	<div class="easyui-panel" data-options="region:'center',border:false,collapsible:true" iconCls="icon-dingdan" title="基本信息"  style="margin-bottom:20px; width:100%">
                            	<div id="toolbarxd" style=" border-bottom:1px dotted #ccc; ">
								<a href="#" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="saveUser()">保 存</a>
							    </div>
                               <form id="fm" method="post" encType="multipart/form-data" novalidate>
                               <input type="hidden" value="${tbJzgxxsh.shzt}" id="xxshzt"/>
                                <table id="dg" class=" fitem" data-options="idField:'id',rownumbers:false,fitColumns:true,pagination:true,singleSelect:false,toolbar:'#toolbar',method:'get',checkOnSelect:'true',selectOnCheck:'true'" style="width:800px;height:300px;" >
    							     <input type="hidden" value="${tbBizJzgjbxx.jsid}" name="jsid"/>
    							     <input type="hidden" value="${tbBizJzgjbxx.tbXxjgb.ssxd}" name="ssxd" id="ssxd"/>
                                	<thead></thead>
                                	  <c:choose>
                                               <c:when test="${tbBizJzgjbxx.tbXxjgb.ssxd==10||tbBizJzgjbxx.tbXxjgb.ssxd==20||tbBizJzgjbxx.tbXxjgb.ssxd==40}">
                                      <tbody class="fitem ">
        								<tr>
                                            <td>学校名称：</td>
                                            <td><p id="text">${tbBizJzgjbxx.tbXxjgb.xxjgmc}</p></td>
                                            <td>地址：</td>
                                            <td><p id="text">${tbBizJzgjbxx.tbXxjgb.dz}</p></td>
                                        </tr>
                                        <tr>
                                            <td>所在区：</td>
                                            <td><p id="text">${tbCfgXzqhdm.xzqhjc}</p></td>
                                            <td>办学类型：</td>
                                            <td><p id="text"><app:dictname dictid="XXJG_BXLX${tbBizJzgjbxx.tbXxjgb.bxlx}"/></p></td>
                                        </tr>
                                        <tr>
                                            <td>举办单位：</td>
                                            <td><p id="text">${tbBizJzgjbxx.tbXxjgb.jbdw}</p></td>
                                            <td>举办者类型：</td>
                                            <td><p id="text"><app:dictname dictid="XXJG_JBZLX${tbBizJzgjbxx.tbXxjgb.jbzlx}"/></p></td>
                                        </tr>
                                              <c:if test="${tbBizJzgjbxx.tbXxjgb.ssxd==40}">
	                                        <tr>
	                                        <td>驻地城乡类型：</td>
	                                            <td>
	                                               <p id="text"><app:dictname dictid="XXJG_ZDCXLX${tbBizJzgjbxx.tbXxjgb.zdcxlx}"/></p>
	                                            </td>
	                                        </tr>  
                                        </c:if>  
                                        <tr>
                                            <td><label>姓名：</label></td>
                                            <td><p id="text">${tbBizJzgjbxx.xm}</p></td>
                                            <td rowspan="3">照片预览：<br><span style="color:#19aa8d; line-height:20px;">注：近期免冠半身照片，<br>照片规格：宽154像素，<br>高189像素，<br>分辨率150dpi以上，<br>照片要求为jpg格式，<br>文件大小应小于60K。</span></td>
                                            <td rowspan="3" >
                                            <input type="hidden" id="fileName" name="fileName" value="${tbBizJzgjbxx.grzp}"/>
                                            <img id="img" style="width:100px; margin-right:35px" src="${ctx}${tbBizJzgjbxx.grzp}"><br>
                                            <input style="background:#ccc; color:white; border:1px solid #999; width:100px; margin-right:35px" type="button" id="deletePhoto"
                                             value="删除照片">
                                          </td>
                                        </tr>
                                        
                                        <tr>
                                            <td>性别：</td>
                                             <td><select class="easyui-combobox" required="true" name="xb" id="xb">
                                                 <app:dictselect dictType="JSXX_XB" selectValue="${tbBizJzgjbxx.xb}"/>
                                            </select></td>
                                        </tr>
                                        <tr>
                                        	<td>教育ID：</td>
                                            <td><p id="text">${tbBizJzgjbxx.eduId}</p></td>
                                        </tr>
                                        <tr>
                                        	<td>出生日期：</td>
                                            <td><input type="text" name="csrq" id="csrq" class="Wdate" value="${tbBizJzgjbxx.csrq}" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"/></td> 
                                            <td>个人照片：</td>
                                            <td>
                                            <input type="hidden" name="grzp" id="grzp" value="${tbBizJzgjbxx.grzp}"/>
	                                            <a href="#" style="width:100px" class="easyui-linkbutton" iconCls="icon-add" onclick="javascript:$('#updlg').dialog('open')">上传照片</a>
	                                            <a href="#" style="width:68px" class="easyui-linkbutton" iconCls="icon-help" onclick="javascript:$('#help').dialog('open')">帮助</a>
                                            	</td>
                                        </tr>
                                        <tr>
                                        	<td>身份证件类型：	</td>
                                            <td>
                                            <p id="text"><app:dictname dictid="JSXX_SFZJLX${tbBizJzgjbxx.sfzjlx}"/></p> 
                                            </td>
                                            <td>证件号码：</td>
                                            <td><p id="text">${tbBizJzgjbxx.sfzjh}</p></td>    
                                        </tr>
                                        <tr>
                                            <td>国籍/地区：</td>
                                            <td>
                                            <select class="easyui-combobox" required="true" name="gjdq" id="gjdq" >
                                                <c:choose>
                                                    <c:when test="${empty tbBizJzgjbxx.gjdq}">
                                             		 <app:dictselect dictType="JSXX_GJDQ" selectValue="156"/>
                                                    </c:when>
                                                    <c:otherwise>
                                             		 <app:dictselect dictType="JSXX_GJDQ" selectValue="${tbBizJzgjbxx.gjdq}"/>
                                             		</c:otherwise> 
                                                </c:choose>
                                            </select>
                                            </td>
                                           <td>出生地：</td>
                                            <td>
                                            	<input type="text" class="easyui-searchbox" name="csd1" data-options="searcher:doSearch2" value="${tbCfgXzqhdm2.xzqhmc}" id="csd" required>
                                           		<input type="hidden" name="csd" value="${tbBizJzgjbxx.csd}"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>民族：</td>
                                            <td><select class="easyui-combobox" required="true" name="mz" id="mz">
                                                 <app:dictselect dictType="JSXX_MZ" selectValue="${tbBizJzgjbxx.mz}"/>
                                            </select></td>
                                            <td>政治面貌：</td>
                                            <td><select class="easyui-combobox" required="true" name="zzmm" id="zzmm" data-options="multiple:true,multiline:true">
                                                 <app:dictselect dictType="JSXX_ZZMM" />
                                                 <input type="hidden" value="${tbBizJzgjbxx.zzmm}" id="zzmmhx"/>
                                            </select></td>
                                        </tr>
                                        <tr>
                                            <td>参加工作年月：</td>

                                            <td>
                                            <input id="cjgzny" name="cjgzny" class="Wdate" type="text" value="${tbBizJzgjbxx.cjgzny}" onfocus="WdatePicker({dateFmt:'yyyy-MM'})"/>
                                            </td>

                                            <%-- <td><input id="cjgzny" type="text" class="easyui-datebox" name="cjgzny" value="${tbBizJzgjbxx.cjgzny}" required ></td> --%>

                                            <td>进本校年月：</td>
                                            <td>
                                            <input id="jbxny" name="jbxny" class="Wdate" type="text" value="${tbBizJzgjbxx.jbxny}" onfocus="WdatePicker({dateFmt:'yyyy-MM'})"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>教职工来源：</td>
                                            <td>
                                          <%--   <select class="easyui-combobox" required="true" name="jzgly" id="">
                                                 <app:dictselect dictType="JSXX_JZGLY" selectValue="${tbBizJzgjbxx.jzgly}"/>
                                            </select> --%>
                                             <input type="text" class="easyui-searchbox" name="jzgly1" data-options="searcher:doSearch" value="<app:dictname dictid='JSXX_JZGLY${tbBizJzgjbxx.jzgly}'/>" id="jzgly" required>
                                            	<input type="hidden" name="jzgly" value="${tbBizJzgjbxx.jzgly}"/>
                                            </td>
                                            <td>教职工类别：</td>
                                            <td><select class="easyui-combobox" required="true" name="jzglb" id="jzglb">
                                             <appadd:dictselectadd dictType="JSXX_JZGLB" schoolType="${tbBizJzgjbxx.tbXxjgb.ssxd}" selectValue="${tbBizJzgjbxx.jzglb}"/>
                                            </select></td>
                                        </tr>
                                        
                                        <c:choose>
                                       
                                          <c:when test="${tbBizJzgjbxx.tbXxjgb.ssxd==10}">
                                        <tr>
                                            <td>是否在编：</td>
                                            <td><select class="easyui-combobox" required="true" name="sfzb" id="sfzb">
                                            <c:choose>
                                              <c:when test="${tbBizJzgjbxx.sfzb==null||tbBizJzgjbxx.sfzb==''}">
                                              <app:dictselect dictType="JSXX_SFZB" selectValue="1"/>
                                              </c:when>
                                              <c:otherwise>
                                             <app:dictselect dictType="JSXX_SFZB" selectValue="${tbBizJzgjbxx.sfzb}"/>
                                              </c:otherwise>
                                            </c:choose>
                                            </select>
                                              <input type="hidden" value="${tbBizJzgjbxx.sfzb}" id="sfzbhx"/>
                                            </td>
                                            <td>学缘结构：</td>
                                            <td><select class="easyui-combobox" required="true" name="xyjg" id="xyjg" data-options="multiple:true,multiline:true">
                                                <app:dictselect dictType="JSXX_XYJG" />
                                                <input type="hidden" value="${tbBizJzgjbxx.xyjg}" id="xyjghx"/>
                                            </select></td>
                                        </tr>
                                        <tr>
                                            <td>签订合同情况：</td>
                                            <td><select class="easyui-combobox" required="true" name="qdhtqk" id="qdhtqk">
                                              <c:choose>
                                                 <c:when test="${tbBizJzgjbxx.sfzb==null||tbBizJzgjbxx.sfzb==''}">
                                                <app:dictselect dictType="JSXX_QDHTQK" selectValue="1"/>
                                                 </c:when>
                                                 <c:otherwise>
                                                <app:dictselect dictType="JSXX_QDHTQK" selectValue="${tbBizJzgjbxx.qdhtqk}"/>
                                                 </c:otherwise>
                                              </c:choose>
                                            </select></td>
                                            <td>用人形式：</td>
                                            <td><select class="easyui-combobox" required="true" name="yrxs" id="yrxs">
                                               <appadd:dictselectadd dictType="JSXX_YRXS" schoolType="${tbBizJzgjbxx.tbXxjgb.ssxd}" selectValue="${tbBizJzgjbxx.yrxs}"/>
                                            </select></td>
                                        </tr>
                                        <tr style="text-align:right">
                                            <td>人员状态：</td>
                                          <td> <p id="text"><app:dictname dictid="JSXX_ZGQK${tbBizJzgjbxx.zgqk}"/></p></td>
                                        </tr>
                                      </c:when> 
                                      
                                      <c:otherwise>
	                                            <tr>
	                                            <td>是否在编：</td>
	                                            <td><select class="easyui-combobox" required="true" name="sfzb" id="sfzb">
	                                              <c:choose>
	                                              <c:when test="${tbBizJzgjbxx.sfzb==null||tbBizJzgjbxx.sfzb==''}">
	                                              <app:dictselect dictType="JSXX_SFZB" selectValue="1"/>
	                                              </c:when>
	                                              <c:otherwise>
	                                             <app:dictselect dictType="JSXX_SFZB" selectValue="${tbBizJzgjbxx.sfzb}"/>
	                                              </c:otherwise>
	                                            </c:choose>
	                                            </select>
	                                            <input type="hidden" value="${tbBizJzgjbxx.sfzb}" id="sfzbhx"/>
	                                            </td>
	                                           <td>企业工作(实践)时长：</td>
	                                            <td>
	                                            <select class="easyui-combobox" required="true" name="qygzsjsc" id="qygzsjsc">
	                                              <c:choose>
	                                              <c:when test="${tbBizJzgjbxx.qygzsjsc==null||tbBizJzgjbxx.qygzsjsc==''}">
	                                               <appadd:dictselectadd dictType="JSXX_QYGZSJSC" schoolType="${tbBizJzgjbxx.tbXxjgb.ssxd}" selectValue="0"/>
	                                              </c:when>
	                                              <c:otherwise>
	                                               <appadd:dictselectadd dictType="JSXX_QYGZSJSC" schoolType="${tbBizJzgjbxx.tbXxjgb.ssxd}" selectValue="${tbBizJzgjbxx.qygzsjsc}"/>
	                                              </c:otherwise>
	                                            </c:choose>
	                                            </select>
	                                             </td>   
	                                        </tr>
	                                            <tr>
	                                            <td>是否“双师型”教师：</td>
	                                            <td><select class="easyui-combobox" required="true" name="sfss" id="sfss">
	                                             <app:dictselect dictType="JSXX_SFSS" selectValue="${tbBizJzgjbxx.sfss}"/>
	                                            </select></td>
	                                            <td>是否具备职业技能等级证书：</td>
	                                            <td><select class="easyui-combobox" required="true" name="sfjbzyjndjzs" id="sfjbzyjndjzs">
	                                                <app:dictselect dictType="JSXX_SFJBZYJNDJZS" selectValue="${tbBizJzgjbxx.sfjbzyjndjzs}"/>
	                                            </select></td>
	                                        </tr>
	                                        <tr>
	                                            <td>签订合同情况：</td>
	                                            <td><select class="easyui-combobox" required="true" name="qdhtqk" id="qdhtqk">
	                                                  <c:choose>
                                                 <c:when test="${tbBizJzgjbxx.sfzb==null||tbBizJzgjbxx.sfzb==''}">
                                                <app:dictselect dictType="JSXX_QDHTQK" selectValue="1"/>
                                                 </c:when>
                                                 <c:otherwise>
                                                <app:dictselect dictType="JSXX_QDHTQK" selectValue="${tbBizJzgjbxx.qdhtqk}"/>
                                                 </c:otherwise>
                                              </c:choose>
	                                            </select></td>
	                                            <td>用人形式：</td>
	                                            <td><select class="easyui-combobox" required="true" name="yrxs" id="yrxs">
	                                              <appadd:dictselectadd dictType="JSXX_YRXS" schoolType="${tbBizJzgjbxx.tbXxjgb.ssxd}" selectValue="${tbBizJzgjbxx.yrxs}"/>
	                                            </select></td>
	                                        </tr>
	                                        <c:if test="${tbBizJzgjbxx.tbXxjgb.ssxd==20}">
		                                        <tr style="text-align:right">
		                                            <td>人员状态：</td>
		                                            <td> <p id="text"><app:dictname dictid="JSXX_ZGQK${tbBizJzgjbxx.zgqk}"/></p></td>
		                                        </tr>
	                                        </c:if>
	                                        <c:if test="${tbBizJzgjbxx.tbXxjgb.ssxd==40}">
		                                        <tr style="text-align:right">
		                                            <td>骨干教师类型：</td>
		                                            <td><select class="easyui-combobox" required="true" name="ggjslx" id="ggjslx">
		                                              <c:choose>
		                                                <c:when test="${tbBizJzgjbxx.ggjslx==''||tbBizJzgjbxx.ggjslx==null}">
		                                                  <app:dictselect dictType="JSXX_GGJSLX" selectValue="0"/>
		                                                </c:when>
		                                                <c:otherwise>
		                                                  <app:dictselect dictType="JSXX_GGJSLX" selectValue="${tbBizJzgjbxx.ggjslx}"/>
		                                                </c:otherwise>
		                                              </c:choose>
		                                            </select></td>
		                                            <td>人员状态：</td>
		                                            <td> <p id="text"><app:dictname dictid="JSXX_ZGQK${tbBizJzgjbxx.zgqk}"/></p></td>
		                                        </tr>
	                                        </c:if>
                                      </c:otherwise> 
                                     </c:choose> 
                                    </tbody>
                             </c:when>
                             
                            
                             <c:otherwise>
                      			<tbody class="fitem">
        								<tr>
                                            <td>学校名称：</td>
                                            <td><p id="text">${tbBizJzgjbxx.tbXxjgb.xxjgmc}</p></td>
                                            <td>地址：</td>
                                            <td><p id="text">${tbBizJzgjbxx.tbXxjgb.dz}</p></td>
                                        </tr>
                                        <tr>
                                            <td>所在区：</td>
                                            <td><p id="text">${tbCfgXzqhdm.xzqhjc}</p></td>
                                            <td>办学类型：</td>
                                            <td><p id="text"><app:dictname dictid="XXJG_BXLX${tbBizJzgjbxx.tbXxjgb.bxlx}"/></p></td>
                                        </tr>
                                        <tr>
                                            <td>举办单位：</td>
                                            <td><p id="text">${tbBizJzgjbxx.tbXxjgb.jbdw}</p></td>
                                            <td>举办者类型：</td>
                                            <td><p id="text"><app:dictname dictid="XXJG_JBZLX${tbBizJzgjbxx.tbXxjgb.jbzlx}"/></p></td>
                                        </tr>
	                                        <tr>
	                                        <td>驻地城乡类型：</td>
	                                            <td>
	                                               <p id="text"><app:dictname dictid="XXJG_ZDCXLX${tbBizJzgjbxx.tbXxjgb.zdcxlx}"/></p>
	                                            </td>
	                                        </tr>  
                                        <tr>
                                            <td><label>姓名：</label></td>
                                            <td><p id="text">${tbBizJzgjbxx.xm}</p></td>
                                            <td rowspan="5">照片预览：<br><span style="color:#19aa8d; line-height:20px;">注：近期免冠半身照片，<br>照片规格：宽154像素，<br>高189像素，<br>分辨率150dpi以上，<br>照片要求为jpg格式，<br>文件大小应小于60K。</span></td>
                                            <td rowspan="5" >
                                            <input type="hidden" id="fileName" name="fileName" value="${tbBizJzgjbxx.grzp}"/>
                                            <img id="img" style="width:100px; margin-right:35px" src="${ctx}${tbBizJzgjbxx.grzp}"><br>
                                            <input style="background:#ccc; color:white; border:1px solid #999; width:100px; margin-right:35px" type="button" id="deletePhoto"
                                             value="删除照片"> 
                                             </td>
                                        </tr>
                                        
                                        <tr>
                                            <td>性别：</td>
                                                 <td><select class="easyui-combobox" required="true" name="xb" id="xb">
                                                 <app:dictselect dictType="JSXX_XB" selectValue="${tbBizJzgjbxx.xb}"/>
                                            </select></td>
                                        </tr>
                                        <tr>
                                        	<td>教育ID：</td>
                                           <td><p id="text">${tbBizJzgjbxx.eduId}</p></td>
                                        </tr>
                                        <tr>
                                        	<td>身份证件类型：	</td>
                                            <td><p id="text"><app:dictname dictid="JSXX_SFZJLX${tbBizJzgjbxx.sfzjlx}"/></p></td>
                                        </tr>
                                        <tr>
                                            <td>证件号码：</td>
                                              <td><p id="text">${tbBizJzgjbxx.sfzjh}</p></td>                                         
                                          </tr>
                                      <tr>
                                        	<td>出生日期：</td>
                                            <td><input type="text" name="csrq" id="csrq" class="Wdate" value="${tbBizJzgjbxx.csrq}" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"/></td> 
                                            <td>个人照片：</td>
                                            <td>
                                            <input type="hidden" name="grzp" id="grzp" value="${tbBizJzgjbxx.grzp}"/>
	                                            <a href="#" style="width:100px" class="easyui-linkbutton" iconCls="icon-add" onclick="javascript:$('#updlg').dialog('open')">上传照片</a>
	                                            <a href="#" style="width:68px" class="easyui-linkbutton" iconCls="icon-help" onclick="javascript:$('#help').dialog('open')">帮助</a>
                                            	</td>
                                        </tr>
                                            <tr>
                                            <td>国籍/地区：</td>
                                            <td>
                                            <select class="easyui-combobox" required="true" name="gjdq" id="gjdq" >
                                                <c:choose>
                                                    <c:when test="${empty tbBizJzgjbxx.gjdq}">
                                             		 <app:dictselect dictType="JSXX_GJDQ" selectValue="156"/>
                                                    </c:when>
                                                    <c:otherwise>
                                             		 <app:dictselect dictType="JSXX_GJDQ" selectValue="${tbBizJzgjbxx.gjdq}"/>
                                             		</c:otherwise> 
                                                </c:choose>
                                            </select>
                                            </td>
                                            <td>出生地：</td>
                                            <td>
                                            <input type="text" class="easyui-searchbox" name="csd1" data-options="searcher:doSearch2" value="${tbCfgXzqhdm2.xzqhmc}" id="csd" required>
                                           	<input type="hidden" name="csd" value="${tbBizJzgjbxx.csd}"/>
                                            </td>
                                        </tr>
                                          <tr>
                                            <td>民族：</td>
                                            <td><select class="easyui-combobox" required="true" name="mz" id="mz">
                                                 <app:dictselect dictType="JSXX_MZ" selectValue="${tbBizJzgjbxx.mz}"/>
                                            </select></td>
                                            <td>政治面貌：</td>
                                            <td><select class="easyui-combobox" required="true" name="zzmm" id="zzmm" data-options="multiple:true,multiline:true">
                                                 <app:dictselect dictType="JSXX_ZZMM" />
                                                 <input type="hidden" value="${tbBizJzgjbxx.zzmm}" id="zzmmhx"/>
                                            </select></td>
                                        </tr>
                                        <tr>
                                            <td>参加工作年月：</td>
                                            <td>
                                            <input id="cjgzny" name="cjgzny" class="Wdate" type="text" value="${tbBizJzgjbxx.cjgzny}" onfocus="WdatePicker({dateFmt:'yyyy-MM'})"/>
                                            </td>
                                            <td>
                                            <c:choose>
                                              <c:when test="${tbBizJzgjbxx.tbXxjgb.ssxd==60}">
                                                                                              进本园年月：
                                              </c:when>
                                              <c:otherwise>
                                                                                             进本校年月：
                                              </c:otherwise>
                                            </c:choose>
                                             </td>
                                            <td>
                                             <input id="jbxny" name="jbxny" class="Wdate" type="text" value="${tbBizJzgjbxx.jbxny}" onfocus="WdatePicker({dateFmt:'yyyy-MM'})"/>
                                            </td>
                                        </tr>
                                       <tr>
                                            <td>教职工来源：</td>
                                            <td>
                                          <%--   <select class="easyui-combobox" required="true" name="jzgly" id="">
                                                 <app:dictselect dictType="JSXX_JZGLY" selectValue="${tbBizJzgjbxx.jzgly}"/>
                                            </select> --%>
                                             <input type="text" class="easyui-searchbox" name="jzgly1" data-options="searcher:doSearch" value="<app:dictname dictid='JSXX_JZGLY${tbBizJzgjbxx.jzgly}'/>" id="jzgly" required>
                                             <input type="hidden" name="jzgly"  value="${tbBizJzgjbxx.jzgly}"/>
                                            </td>
                                            <td>教职工类别：</td>
                                            <td><select class="easyui-combobox" required="true" name="jzglb" id="jzglb">
                                                <appadd:dictselectadd dictType="JSXX_JZGLB" schoolType="${tbBizJzgjbxx.tbXxjgb.ssxd}" selectValue="${tbBizJzgjbxx.jzglb}"/>
                                            </select></td>
                                        </tr>
                                            <tr>
                                            <td>是否在编：</td>
                                            <td><select class="easyui-combobox" required="true" name="sfzb" id="sfzb">
                                              <c:choose>
                                              <c:when test="${tbBizJzgjbxx.sfzb==null||tbBizJzgjbxx.sfzb==''}">
                                              <app:dictselect dictType="JSXX_SFZB" selectValue="1"/>
                                              </c:when>
                                              <c:otherwise>
                                             <app:dictselect dictType="JSXX_SFZB" selectValue="${tbBizJzgjbxx.sfzb}"/>
                                              </c:otherwise>
                                            </c:choose>
                                            </select>
                                            <input type="hidden" value="${tbBizJzgjbxx.sfzb}" id="sfzbhx"/>
                                            </td>
                                           <td>用人形式：</td>
                                            <td><select class="easyui-combobox" required="true" name="yrxs" id="yrxs">
                                              <appadd:dictselectadd dictType="JSXX_YRXS" schoolType="${tbBizJzgjbxx.tbXxjgb.ssxd}" selectValue="${tbBizJzgjbxx.yrxs}"/>
                                            </select></td>
                                        </tr>
                                   
                                        
                                        <tr>
                                           <td>签订合同情况：</td>
                                            <td><select class="easyui-combobox" required="true" name="qdhtqk" id="qdhtqk">
                                                  <c:choose>
                                                 <c:when test="${tbBizJzgjbxx.sfzb==null||tbBizJzgjbxx.sfzb==''}">
                                                <app:dictselect dictType="JSXX_QDHTQK" selectValue="1"/>
                                                 </c:when>
                                                 <c:otherwise>
                                                <app:dictselect dictType="JSXX_QDHTQK" selectValue="${tbBizJzgjbxx.qdhtqk}"/>
                                                 </c:otherwise>
                                              </c:choose>
                                            </select></td>
                                            <td>是否全日制师范类专业毕业：</td>
                                            <td><select class="easyui-combobox" required="true" name="sfsflzyby" id="sfsflzyby">
                                                <app:dictselect dictType="JSXX_SFSFLZYBY" selectValue="${tbBizJzgjbxx.sfsflzyby}"/>
                                            </select></td>
                                        </tr>
                                        <c:if test="${tbBizJzgjbxx.tbXxjgb.ssxd==30||tbBizJzgjbxx.tbXxjgb.ssxd==50}">
                                        <tr>
                                            <td>是否受过特教专业培养培训：</td>
                                            <td><select class="easyui-combobox" required="true" name="sfsgtjzypypx" id="sfsgtjzypypx">
                                                <app:dictselect dictType="JSXX_SFSGTJZYPYPX" selectValue="${tbBizJzgjbxx.sfsgtjzypypx}"/>
                                            </select></td>
                                            <td>是否有特殊教育从业证书：</td>
                                            <td><select class="easyui-combobox" required="true" name="sfytsjycyzs" id="sfytsjycyzs">
                                                 <app:dictselect dictType="JSXX_SFYTSJYCYZS" selectValue="${tbBizJzgjbxx.sfytsjycyzs}"/>
                                            </select></td>
                                        </tr>
                                        </c:if>
                                        <tr>
                                            <td>信息技术应用能力：</td>
                                            <td><select class="easyui-combobox" required="true" name="xxjsyynl" id="xxjsyynl">
                                                <app:dictselect dictType="JSXX_XXJSYYNL" selectValue="${tbBizJzgjbxx.xxjsyynl}"/>
                                            </select></td>
                                            <td>是否属于免费（公费）师范生：</td>
                                            <td><select class="easyui-combobox" required="true" name="sfsymfsfs" id="sfsymfsfs">
                                            <c:choose>
                                                <c:when test="${tbBizJzgjbxx.sfsymfsfs==null||tbBizJzgjbxx.sfsymfsfs==''}">
                                                <app:dictselect dictType="JSXX_SFSTGJS" selectValue="0"/>
                                                </c:when>
                                                <c:otherwise>
                                               <app:dictselect dictType="JSXX_SFSYMFSFS" selectValue="${tbBizJzgjbxx.sfsymfsfs}"/>
                                                </c:otherwise>
                                               </c:choose> 
                                            </select></td>
                                        </tr>
                                        <c:choose>
                                          <c:when test="${tbBizJzgjbxx.tbXxjgb.ssxd==30}">
                                         <tr>
                                            <td>继续教育编号：</td>
                                            <td><input type="text" class="easyui-tooltip" title="凡有继续教育编号的人员请务必填写" name="jxjybh" value="${tbBizJzgjbxx.jxjybh}" id="jxjybh" ></td>
                                            <td>骨干教师类型：</td>
                                            <td><select class="easyui-combobox" required="true" name="ggjslx" id="ggjslx">
                                                     <c:choose>
		                                                <c:when test="${tbBizJzgjbxx.ggjslx==''||tbBizJzgjbxx.ggjslx==null}">
		                                                  <app:dictselect dictType="JSXX_GGJSLX" selectValue="0"/>
		                                                </c:when>
		                                                <c:otherwise>
		                                                  <app:dictselect dictType="JSXX_GGJSLX" selectValue="${tbBizJzgjbxx.ggjslx}"/>
		                                                </c:otherwise>
		                                              </c:choose>
                                            </select></td>
                                        </tr>
                                        <tr>
                                            <td>是否参加基层服务项目：</td>
                                            <td><select class="easyui-combobox" required="true" name="sfstgjs" id="sfstgjs">
                                              <c:choose>
                                                <c:when test="${tbBizJzgjbxx.sfstgjs==null||tbBizJzgjbxx.sfstgjs==''}">
                                                <app:dictselect dictType="JSXX_SFSTGJS" selectValue="0"/>
                                                </c:when>
                                                <c:otherwise>
                                                <app:dictselect dictType="JSXX_SFSTGJS" selectValue="${tbBizJzgjbxx.sfstgjs}"/>
                                                </c:otherwise>
                                               </c:choose> 
                                            </select></td>
                                            <td>是否心理健康教育教师：</td>
                                            <td><select class="easyui-combobox" required="true" name="sfxljkjyjs" id="sfxljkjyjs">
                                                  <app:dictselect dictType="JSXX_SFXLJKJYJS" selectValue="${tbBizJzgjbxx.sfxljkjyjs}"/>
                                            </select></td>
                                        </tr>
                                        <tr>
                                            <td>人员状态：</td>
                                            <td> <p id="text"><app:dictname dictid="JSXX_ZGQK${tbBizJzgjbxx.zgqk}"/></p></td>
                                        </tr>
                                        </c:when>
                                        <c:otherwise>
                                        <tr>
                                            <td>是否参加基层服务项目：</td>
                                            <td><select class="easyui-combobox" required="true" name="sfstgjs" id="sfstgjs">
                                                <c:choose>
                                                <c:when test="${tbBizJzgjbxx.sfstgjs==null||tbBizJzgjbxx.sfstgjs==''}">
                                                <app:dictselect dictType="JSXX_SFSTGJS" selectValue="0"/>
                                                </c:when>
                                                <c:otherwise>
                                                <app:dictselect dictType="JSXX_SFSTGJS" selectValue="${tbBizJzgjbxx.sfstgjs}"/>
                                                </c:otherwise>
                                               </c:choose> 
                                            </select></td>
                                            <td>是否心理健康教育教师：</td>
                                            <td><select class="easyui-combobox" required="true" name="sfxljkjyjs" id="sfxljkjyjs">
                                                  <app:dictselect dictType="JSXX_SFXLJKJYJS" selectValue="${tbBizJzgjbxx.sfxljkjyjs}"/>
                                            </select></td>
                                        </tr>
                                        <tr>
                                        	<td>骨干教师类型：</td>
                                            <td><select class="easyui-combobox" required="true" name="ggjslx" id="ggjslx">
                                                     <c:choose>
		                                                <c:when test="${tbBizJzgjbxx.ggjslx==''||tbBizJzgjbxx.ggjslx==null}">
		                                                  <app:dictselect dictType="JSXX_GGJSLX" selectValue="0"/>
		                                                </c:when>
		                                                <c:otherwise>
		                                                  <app:dictselect dictType="JSXX_GGJSLX" selectValue="${tbBizJzgjbxx.ggjslx}"/>
		                                                </c:otherwise>
		                                              </c:choose>
                                            </select></td>
                                            <td>人员状态：</td>
                                           <td> <p id="text"><app:dictname dictid="JSXX_ZGQK${tbBizJzgjbxx.zgqk}"/></p></td>
                                        </tr>
                                         <c:if test="${tbBizJzgjbxx.tbXxjgb.ssxd==50}">
                                        
                                            <tr>
                                            <td>从事特教起始年月：</td>

                                            <td>
                                            <input id="cstjqsny" name="cstjqsny" class="Wdate" type="text" value="${tbBizJzgjbxx.cstjqsny}" onfocus="WdatePicker({dateFmt:'yyyy-MM'})"/>
                                            </td>
                                            <td>是否全日制特殊教育专业毕业：</td>
                                            <td>
                                            <select class="easyui-combobox" required="true" name="sfqrztsyjzyby" id="sfqrztsyjzyby">
                                                  <app:dictselect dictType="JSXX_SFQRZTSYJZYBY" selectValue="${tbBizJzgjbxx.sfqrztsyjzyby}"/>
                                            </select>
                                            </td>

                                           </tr>
                                         </c:if> 
                                         <c:if test="${tbBizJzgjbxx.tbXxjgb.ssxd==60}">
                                           <td>是否受过学前教育专业培养培训：</td>
                                            <td><select class="easyui-combobox" required="true" name="sfsgxqjyzypypx" id="sfsgxqjyzypypx">
                                                  <app:dictselect dictType="JSXX_SFSGXQJYZYPYPX" selectValue="${tbBizJzgjbxx.sfsgxqjyzypypx}"/>
                                            </select></td>
                                              <td>是否全日制学前教育专业毕业：</td>
                                            <td><select class="easyui-combobox" required="true" name="sfxqjyzyby" id="sfxqjyzyby">
                                                  <app:dictselect dictType="JSXX_SFXQJYZYBY" selectValue="${tbBizJzgjbxx.sfxqjyzyby}"/>
                                            </select></td>
                                         </c:if>
                                        </c:otherwise>
                                       </c:choose> 
                                        
                                    </tbody>
                                             		</c:otherwise> 
                                       </c:choose>
    							</table>
    							</form>
    					<div id="updlg" class="easyui-dialog" title="上传照片" style="width:350px;height:200px; overflow:auto;padding:10px 20px" closed="true" buttons="#updlg-buttons">
					          <form id="fileform2" onsubmit="return false;">
					             	选择照片： <input style="margin-right:10px;" type="file" class="filetype" id="file" name="file" value="">
					           </form>
						</div>
						<div id="help" class="easyui-dialog" title="帮助" style="width:350px;padding:10px 20px" closed="true">
					          <p>操作步骤：</p>
					          <p>1.单击电脑左下角，单击所有程序，单击附件，单击画图，打开画图软件</p>
					          <p>2.单击软件左上角“画图”按钮，单击“打开”，选择要修改的照片</p>
					          <p>3.单击软件上方操作栏“重新调整大小”按钮，弹出窗口，单击“像素”，点掉“保持纵横比”前面的对号，调整水平154像素，垂直189像素，单击“确定”</p>
					          <p>4.单击左上角“保存”按钮</p>
					          <p>5.原图片即可上传使用</p>
						</div>
						<div id="updlg-buttons">
							<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="upload()">上传</a>
							<a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#updlg').dialog('close')">关闭</a>
						</div>
    					<!--easyUi树(jzgly) -->
                                    
                                         <div id="dd" class="easyui-dialog" title="教职工来源" closed="true" data-options="modal:true" style="width:50%;height:300px;margin:auto;overflow-y:auto;overflow-x:hidden;">
                                        	<input type="hidden" name="treeData" data="${jzglylist}"/>
                                        	<ul id="tt" class="easyui-tree">
											    
											</ul>
                                        </div>
    					
    					<!--easyUi树(csd) -->
                                    
                                         <div id="ddcsd" class="easyui-dialog" title="出生地" closed="true" data-options="modal:true" style="width:50%;height:300px;overflow-y:auto;overflow-x:hidden;">
                                        	<input type="hidden" name="treeData2" data="${csdlist}"/>
                                        	<ul id="ttcsd" class="easyui-tree">
											    
											</ul>
                                        </div>
                                         <div id="tips" class="easyui-dialog" title="温馨提示" style="padding:10px 20px;width:200px;height:100px;line-height:40px;text-align:center;overflow:auto;font-size:20px;" closed="true" data-options="modal:true">请选择一条记录</div>
                                
    						</div>
    </div>
    <c:if test="${SESSION_ISPUB == '1'}">
    	<div id="btn-wrap">
        <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="submitInfo()">报送</a>
        <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="cancelSubmit()">取消</a><span style="color:red">各项信息填写完整后，才能报送。报送后，不能再次修改教师信息。只有审核不通过的教师信息，才能再次编辑报送。</span>

    </div>
    </c:if>
    <script type="text/javascript">
  //获取浏览器显示区域的高度 
    $(window).height(); 
    //获取浏览器显示区域的宽度 
    $(window).width(); 

    //获取页面的文档高度 
    $(document.body).height(); 
    //获取页面的文档宽度 
    $(document.body).width(); 

    //获取滚动条到顶部的垂直高度 
    $(document).scrollTop(); 
    //获取滚动条到左边的垂直宽度 
    $(document).scrollLeft(); 


    
    
	$('#jxjybh').tooltip({
		position:'right',
		trackMouse:true,
		onShow:function(){
			$(this).tooltip('tip').css({
				borderColor:'red',
				color:'red'
			});
		}
	})

    $('#gjdq').combobox({ editable:false }); // 国籍/地区
    $('#csd').searchbox({ editable:false }); //  出生地
    $('#mz').combobox({ editable:false });//民族
    $('#zzmm').combobox({ editable:false });//政治面貌
    $('#cjgzny').attr('disabled',false);//参加工作年月
    $('#jbxny').attr('disabled',false);  //进本校年月
    $('#jzgly').searchbox({ editable:false }); //教职工来源
    $('#jzglb').combobox({ editable:false }); //教职工类别
    $('#sfzb').combobox({ editable:false }); //是否在编
    $('#yrxs').combobox({ editable:false }); //用人形式
    $('#qdhtqk').combobox({ editable:false }); //签订合同情况
    $('#xyjg').combobox({ editable:false }); //签订合同情况
    $('#sfqrztsyjzyby').combobox({ editable:false }); 
    $('#xxjsyynl').combobox({ editable:false }); //信息技术应用能力
    $('#sfsflzyby').combobox({ editable:false }); //是否全日制师范类专业毕业
    $('#sfsgtjzypypx').combobox({ editable:false }); //是否有特殊教育从业证书 
    $('#sfytsjycyzs').combobox({ editable:false }); //是否有特殊教育从业证书
    $('#sfsymfsfs').combobox({ editable:false }); //是否属于免费（公费）师范生
    $('#sfjbzyjndjzs').combobox({ editable:false }); 
    $('#sfss').combobox({ editable:false }); // 是否“双师型”教师
    $('#ggjslx').combobox({ editable:false }); //骨干教师类型
    $('#sfstgjs').combobox({ editable:false }); //是否参加基层服务项目
    $('#sfxljkjyjs').combobox({ editable:false }); //是否心理健康教育教师
    $('#sfsgxqjyzypypx').combobox({ editable:false }); //是否受过学前教育专业培养培训
    $('#sfxqjyzyby').combobox({ editable:false }); //是否全日制学前教育专业毕业
    $('#cstjqsny').attr('disabled',false);  //特教起始年月
    $('#xb').combobox({ editable:false });  //性别

    function tips(dlgNmae,msg){
		$(dlgNmae).html(msg).dialog('open');
	};

     	//关闭窗口
          function save_close(dlgName){
        	  $(dlgName).dialog('close');
     	 }
     	
    		 //保存
    		   function saveUser(){
    			   $.ajax({
    	 				url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
    	 				data:{},
    	 				dataType : "json",
    	 				success:function(data){
    	 					if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
    	 							 var cjgzny = $('#cjgzny').val();
    	 			                 var jbxny = $('#jbxny').val();
    	 			                 var ssxd = $('#ssxd').val();
    	 			                 var photo = $('#grzp').val();
    	 			                 var csrq = $('#csrq').val();
    	 			                 var csd = $("input[type='hidden'][name='csd']").val();
    	 			                 var jzgly = $("input[type='hidden'][name='jzgly']").val();
    	 			               
    	 			                 if(csrq==''||cjgzny==''||jbxny==''||csd==''||csd==null||jzgly==''||jzgly==null){
    	 			                	 $.messager.alert('温馨提示','信息不完善，不可保存！','warning');
    	 			                	 return;
    	 			                 }else{
    	 			                	 if(cjgzny>jbxny){
    	 			                		 if(ssxd==60){
    	 			                		 $.messager.alert('温馨提示', '进本园年月不能早于参加工作年月！','warning');
    	 			                		 }else{
    	 			                		 $.messager.alert('温馨提示', '进本校年月不能早于参加工作年月！','warning'); 
    	 			                		 }
    	 			                    	 return; 
    	 			                	 }
    	 			                	 if(cjgzny<=csrq){
    	 			                		 $.messager.alert('温馨提示', '参加工作年月必须晚于出生年月！','warning');
    	 			                    	 return;  
    	 			                	 }
    	 			                 }
    	 			                 if(ssxd==50){
    	 			                	 var cstjqsny = $('#cstjqsny').val();
    	 			                	 if(cstjqsny==''){
    	 			                		 $.messager.alert('温馨提示', '信息不完善，不可保存','warning');
    	 			                    	 return; 
    	 			                	 }else{
    	 			                		 if(cstjqsny<cjgzny){
    	 			                			 $.messager.alert('温馨提示', '从事特教起始年月不能早于参加工作年月 ！','warning');
    	 			                        	 return;  
    	 			                		 }
    	 			                	 }
    	 			                 }
    	 			                 if(photo==null||photo==''){
    	 			                	 $.messager.alert('温馨提示', '请上传您的照片！','warning');
    	 			                	 return; 
    	 			                 }
    	 			    			 
    	 			     		       $('#fm').form('submit',{
    	 			     		    	   //zhanwei是为了解决文件上传问题，没有这个占位处理器处理会出现异常
    	 			    		           url: "${ctx}/basicInformation/TeacherBaseInfoAction.a?saveUser=zhanwei",
    	 			    		           onSubmit: function(){
    	 			    		               return $(this).form('validate');
    	 			    		           },
    	 			    		           success: function(){
    	 			    		        		   $.messager.alert('温馨提示','保存成功','info',function(){
    	 			    		                     	window.location.reload();
    	 			    		             	  	});
    	 			    		           }
    	 			    		       }); 
    	 					 }else{
    	 		    				$.messager.alert('温馨提示','信息审核中或已通过审核，不可修改信息','warning');
    	 		    			 } 
    	 				},
    	 				error:function(XMLHttpRequest, textStatus, errorThrown){
    	 					var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
    	 					sessionTimeout(sessionstatus);
    	 				}
    	 			})
    		 }
    		 
  		  setTimeout(function(){
  			var gjdq = $("#gjdq").val();
 		    if(gjdq!=156){
 			   $('#mz').combobox('setValue','');
 			   $('#zzmm').combobox('setValue','');
			   $('#mz').combobox('disable');
			   $('#zzmm').combobox('disable');
 		    }else{
 		       $('#mz').combobox('setValue',01);
 			   $('#zzmm').combobox('setValue',13);
			   $('#mz').combobox('enable');
			   $('#zzmm').combobox('enable');	
 		    }
 		   var sfzb = $('#sfzbhx').val();
 		   if(sfzb=='1'){
 			   $('#yrxs').combobox('setValue','');
 			   $('#yrxs').combobox('disable');
 		   }
 		   var zzmms = $("#zzmmhx").val();
 		   if(zzmms!=null&&zzmms!=''){
 			  var zzmm = zzmms.split(",");
			   $('#zzmm').combobox('setValues',zzmm); 
 		   }
			var ssxd = $('#ssxd').val();
			if(ssxd==10){
				 var xyjgs = $("#xyjghx").val();
			     var xyjg = xyjgs.split(",");
				   $('#xyjg').combobox('setValues',xyjg);
			}
  		  },1000)
  		  
  		  
    	 //验证是否需要填写民族\政治面貌
    	   $('#gjdq').combobox({
    		   onSelect: function(param){
    			   var gjdq = $("input[type='hidden'][name='gjdq']").val();
    			   if(gjdq!=156){
    				   $('#mz').combobox('setValue',' ');
    				   $('#zzmm').combobox('setValue',' ');
    				   $('#mz').combobox('disable');
    				   $('#zzmm').combobox('disable');
    			   }
    			   if(gjdq==156){
    				   $('#mz').combobox('setValue',01);
    				   $('#zzmm').combobox('setValue',13);
    				   $('#mz').combobox("enable");
    				   $('#zzmm').combobox("enable");
    			   }
				}
			});
  		  
  		  /**
  		  *是否在编验证
  		  */
  		  
  		  $('#sfzb').combobox({
  			  onSelect: function(param){
   			   var sfzb = $("input[type='hidden'][name='sfzb']").val();
   			   if(sfzb==0){
   				   $('#qdhtqk').combobox('setValue',2);
   				$('#yrxs').combobox('setValue',1);
   				   $('#yrxs').combobox('enable');
   			   }
   			   if(sfzb==1){
   				   $('#yrxs').combobox('setValue',' ');
   				   $('#qdhtqk').combobox('setValue',1);
   				   $('#yrxs').combobox("disable");
   			   }
				}
  		  });
    	 
    	 //删除照片
    	 $('#deletePhoto').click(function(){
    			if($("#img").attr("src") == ""){
		 			return;
		 		}
    		    	  var fileName = $('#fileName').val();
    		    	  if(fileName==null||fileName==''){
    		    		  $.messager.confirm('温馨提示', '是否要删除照片？', function(r){
    		    			if(r){ 
    		    	    	$.ajax({
    		    	    		type : "post",
    		    	    		url : "${ctx}/sysuserManager/FileAction.a?delphoto=aaa&time="+new Date().getTime(),
    		    	    		dataType : "json",
    		    	    		data : {},
    		    	    		async : false,
    		    	    		cache: false,
    		    	            contentType: false,
    		    	            processData: false,
    		    	    		success : function(data) {
    		    	   	        	tishi(data.result.msg)
    		    	   	            if(data.result.status == "1"){
    		    	   	            	$("#img").attr("src","");
    		    	   	            	$("#file").val("");
    		    	   	            	$("#grzp").val("");
    		    	   	            }
    		    	    		},
    		    	    		error:function(XMLHttpRequest, textStatus, errorThrown){
    		    					var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
    		    					sessionTimeout(sessionstatus);
    		    				}
    		    	    	});
    		    			} 
    		    		  });
    		    	  }else{
    		    		$.ajax({
    		 				url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
    		 				data:{},
    		 				dataType : "json",
    		 				success:function(data){
    		 					    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
    		 					  	 $.messager.confirm('温馨提示', '是否要删除照片？', function(r){
    		 			    		      if(r){
    		 			    		    	  var jsid = $("input[type='hidden'][name='jsid']").val();
    				 			    		    		$.ajax({
    				 			    						url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?deletePhoto&time='+new Date().getTime(),
    				 			    						data:{jsid:jsid},
    				 			    						success:function(data){
    				 			    							$.messager.alert('温馨提示','删除成功！','info',function(){
    				 			    				            	window.location.reload();
    				 			    				    	  	});
    				 			    						},
    				 			    						error:function(data){
    				 			    							$.messager.alert('温馨提示','删除失败！','warning');
    				 			    						}
    				 			    						
    				 			    					}) 
    		 			    			}
    		 			    		      });  
    		 					    	}else{
    		 		    				$.messager.alert('温馨提示','信息审核中或已通过审核，不可删除照片','warning');
    		 		    			} 
    		 				},
    		 				error:function(XMLHttpRequest, textStatus, errorThrown){
    		 					var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
    		 					sessionTimeout(sessionstatus);
    		 				}
    		 				
    		 			})
    	     }		
    			
    	 })
    	 
     	/**
    	* 树查询教职工来源
    	*/
    	  function doSearch(){
			$('#dd').dialog('open');
			$("#dd").panel("move",{top:$(document).scrollTop() + ($(window).height()-260) * 0.5});
			$('.window-shadow').css('top',$(document).scrollTop() + ($(window).height()-260) * 0.5);
			 var dataStrKey = $('input[name="treeData"][type="hidden"]').attr('data').replace(/\'/g,'\"');
			 var data = JSON.parse(dataStrKey);
			 $('#tt').tree({
				 data:data
			 });
			 $('#tt').tree('collapseAll');
		}
    	 
     	/**
    	* 树查询csd
    	*/
    	  function doSearch2(){
			$('#ddcsd').dialog('open');
			$("#ddcsd").panel("move",{top:$(document).scrollTop() + ($(window).height()-260) * 0.5});
			$('.window-shadow').css('top',$(document).scrollTop() + ($(window).height()-260) * 0.5);
			 var dataStrKey = $('input[name="treeData2"][type="hidden"]').attr('data').replace(/\'/g,'\"').replace(/(\"id\":)(.*?)(\,)/g,'$1\"$2\"$3');
			 var data = JSON.parse(dataStrKey);
			 $('#ttcsd').tree({
				 data:data
			 });
			 $('#ttcsd').tree('collapseAll');
		}
    	 
    	 
    	 //树选择
		 $(function(){
			$('#tt').tree({
				onClick:function(node){
					$("#jzgly").searchbox('setValue', node.text);
					save_close("#dd");
					$("input[name='jzgly'][type='hidden']").val(node.id);
				}
			})
			$('#ttcsd').tree({
				onClick:function(node){
					$("#csd").searchbox('setValue', node.text);
					save_close("#ddcsd");
					$("input[name='csd'][type='hidden']").val(node.id);
				}
			})
		})
		
    	 
    /**
    *报送
	*/
	function submitInfo(){
    		 
				$.ajax({
					url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
					data:{},
					dataType : "json",
					success:function(data){
						    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
						    	$.ajax({
									url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?submitted&time='+new Date().getTime(),
									data:{},
									success:function(data){
										$.messager.alert('温馨提示','报送成功','info',function(){
						                       window.parent.location.reload();
							    	  	});
									},
									error:function(data){
										$.messager.alert('温馨提示','报送失败,请检查各项是否都已填写！','warning');
									}
									
								});   
							}else{
								$.messager.alert('温馨提示','信息已报送，不可重复报送！','warning');
							}  
					},
					error:function(XMLHttpRequest, textStatus, errorThrown){
						var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
						sessionTimeout(sessionstatus);
					}
					
				})
    		 
    	 }
    	 
    	 
    /**
    *取消报送
	*/
	function cancelSubmit(){
		 
		$.ajax({
			url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
			data:{},
			dataType : "json",
			success:function(data){
				    if(data.tbJzgxxsh.shzt==02){
				    	$.ajax({
							url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?cancelSubmit&time='+new Date().getTime(),
							data:{},
							success:function(data){
								$.messager.alert('温馨提示','取消成功','info',function(){
					            	
					    	  	});
							},
							error:function(data){
							
							}
							
						});  
					}else{
						$.messager.alert('温馨提示','信息审核中(审核通过)或未报送，不可取消保送！','warning');
					}  
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
			
		})
			
    	 }
	
	
		function addTab(title, url){
			if ($('#tb').tabs('exists', title)){
				$('#tb').tabs('select', title);
			} else {
				var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
				$('#tb').tabs('add',{
					title:title,
					content:content,
					closable:true
				});
			}
		}
		//关闭所有的tab  
		    function closeAll(){  
		        var tiles = new Array();  
		          var tabs = $('#tb').tabs('tabs');      
		          var len =  tabs.length;           
		          if(len>0){  
		            for(var j=0;j<len;j++){  
		                var a = tabs[j].panel('options').title;               
		                tiles.push(a);  
		            }  
		            for(var i=1;i<tiles.length;i++){               
		                $('#tb').tabs('close', tiles[i]);  
		            }
		          }
		    } 
		//关闭当前的tab 
			function removePanel(){
				var tab = $('#tb').tabs('getSelected');
				    if (tab){
					var index = $('#tb').tabs('getTabIndex', tab);
					$('#tb').tabs('close', index);
					}
				}
		
		
			$('#zzmm').combobox({
				onSelect:function(record){
				var v = $('#zzmm').combobox("getValue");
				
				if(v == '12' || v == '13'){
					$('#zzmm').combobox("setValue",record.value);
				}
				
				if(record.value == '12' || record.value == '13'){
					$('#zzmm').combobox("clear");
					$('#zzmm').combobox("setValue",record.value);
				}
			}
			});
			$('#xyjg').combobox({
				onSelect:function(record){
					var v = $('#xyjg').combobox("getValue");
					if(v == '4'){
						$('#xyjg').combobox("setValue",record.value);
					}
				if(record.value == '4'){
					$('#xyjg').combobox("clear");
					$('#xyjg').combobox("setValue",record.value);
				}
			}
			});
			
		window.onload=function(){
			var jzgly = '${tbBizJzgjbxx.jzgly}';
			if(jzgly != null){
				var dataStrKey = $('input[name="treeData"][type="hidden"]').attr('data').replace(/\'/g,'\"');
				 var data = JSON.parse(dataStrKey);
				 console.log(data);
				 $('#tt').tree({
					 data:data
				 });
				 
				 $("#jzgly").searchbox('setValue', $('#tt').tree("find",jzgly).text);
				 $("input[name='jzgly'][type='hidden']").val(jzgly);
				 
			} 
			
			
			var csd = '${tbBizJzgjbxx.csd}';
			
			if(csd != null){
				 var dataStrKey = $('input[name="treeData2"][type="hidden"]').attr('data').replace(/\'/g,'\"').replace(/(\"id\":)(.*?)(\,)/g,'$1\"$2\"$3');
				 var data = JSON.parse(dataStrKey);
				 $('#ttcsd').tree({
					 data:data
				 });
				 
				// $("#csd").searchbox('setValue', $('#ttcsd').tree("find",csd).text);
				 $("input[name='csd'][type='hidden']").val(csd);
				 
			}
			 
		}
   </script>					
</body>
</html>
