<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>北京市教师管理服务平台</title>
    <link rel="stylesheet" type="text/css" href="../css/newcss.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
	<link rel="stylesheet" type="text/css" href="../My97DatePicker/skin/WdatePicker.css">
    <script type="text/javascript" src="../easyui/jquery.min.js"></script>
    <script src="../js/echarts/build/dist/echarts-all.js"></script>
    <script src="../js/echarts/build/dist/echarts.js"></script>
    <script type="text/javascript" src="../easyui/easyloader.js"></script>
    <script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx}/js/refresh.js"></script>
    <script type="text/javascript" src="${ctx}/js/upload.js"></script>
    <script type="text/javascript" src="../My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript" src="${ctx}/basicInformation/ValidateUtils.js"></script>
    <script type="text/javascript">
    
    function isNull(name){
    	if($("#"+name).length == 0){
    		return false;
    	}
    	var obj = $("#"+name).val();
    	if(obj == null || obj == ""){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    function isNullTextBox(name){
    	if($("#"+name).length == 0){
    		return false;
    	}
    	var obj = $("#"+name).textbox("getValue");
    	if(obj == null || obj == ""){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    function isNullCombo(name){
    	if($("#"+name).length == 0){
    		return false;
    	}
    	var obj = $("#"+name).combobox("getValue");
    	if(obj == null || obj == ""){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    function isNullSearch(name){
    	if($("#"+name).length == 0){
    		return false;
    	}
    	var obj = $("#"+name).searchbox("getValue");
    	if(obj == null || obj == ""){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    function verifyCommon(){
    	if(isNull("name")){
    		tishi("姓名" + "必填！");
    		return false;
    	}
    	
    	if(isNull("csrq")){
    		tishi("出生日期" + "必填！");
    		return false;
    	}
    	
    	if(isNull("cjgzny")){
    		tishi("参加工作年月" + "必填！");
    		return false;
    	}
    	
    	if(isNull("jbxny")){
    		tishi("进本校年月" + "必填！");
    		return false;
    	}
    	
    	if(isNull("xxx")){
    		tishi("" + "必填！");
    		return false;
    	}
    	
    	return true;
    }
    
    function verifyTextbox(){
    	if(!verifyCommon()){
    		return false;
    	}
    	
    	if(isNullTextBox("sfzjh")){
    		tishi("证件号码" + "必填！");
    		return false;
    	}
    	
    	if(!isCardNo($("#sfzjh").textbox("getValue"))){
    		tishi("请输入正确的证件号码！");
    		return false;
    	}
    	
    	return true;
    }
    
 // 验证身份证 
    function isCardNo(card) { 
     var pattern = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/; 
     return pattern.test(card); 
    }
    
	function verifyCombo(){
		if(!verifyTextbox()){
    		return false;
    	}
		
		if(isNullCombo("xb")){
			tishi("性别" + "必填！");
    		return false;
    	}
		
		if(isNullCombo("sfzjlx")){
			tishi("身份证件类型" + "必填！");
    		return false;
    	}
		if(isNullCombo("gjdq")){
			tishi("国籍/地区" + "必填！");
    		return false;
    	}
		if(isNullCombo("mz")){
			tishi("民族" + "必填！");
    		return false;
    	}
		if(isNullCombo("zzmm")){
			tishi("政治面貌" + "必填！");
    		return false;
    	}
		if(isNullCombo("jzglb")){
			tishi("教职工类别" + "必填！");
    		return false;
    	}	
		
		return true;
    }
	
	function verifySearch(){
		if(!verifyCombo()){
    		return false;
    	}
		
		if(isNullSearch("csd")){
			tishi("出生地" + "必填！");
    		return false;
    	}
		if(isNullSearch("jzgly")){
			tishi("教职工来源" + "必填！");
    		return false;
    	}
		
		return true;
    }
	
	function verify(){
		if(!verifySearch()){
			return false;
		}else{
			return true;
		}
	}
	
	function tishi(value){
		$.messager.alert('系统提示',value,'warning');
	}
    </script>
    <script type="text/javascript">
    var serverPath = '${ctx}';
    
    function upload(){
    	var formData = new FormData($("#fileform2")[0]);
    	$.ajax({
    		type : "post",
    		url : "${ctx}/sysuserManager/FileAction.a?upload=aaa&time="+new Date().getTime(),
    		dataType : "json",
    		data : formData,
    		async : false,
    		cache: false,
            contentType: false,
            processData: false,
    		success : function(data) {
   	        	tishi(data.result.msg);
   	            if(data.result.status == "1"){
   	            	$("#grzp").val(data.result.filepath);
   	            	$("#img").attr("src",serverPath + data.result.filepath);
   	            	$('#updlg').dialog('close');
   	            }
    		},
    		error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
    	});
    	return false;;
    }
    
    function deleteImage(){
    	var formData = new FormData($("#fileform2")[0]);
    	$.ajax({
    		type : "post",
    		url : "${ctx}/sysuserManager/FileAction.a?delphoto=aaa&time="+new Date().getTime(),
    		dataType : "json",
    		data : formData,
    		async : false,
    		cache: false,
            contentType: false,
            processData: false,
    		success : function(data) {
   	        	tishi(data.result.msg)
   	            if(data.result.status == "1"){
   	            	$("#img").attr("src","");
   	            }
    		},
    		error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
    	});
    	return false;;
    }
    
    </script>
 </head>   
<body>
 <div id="anchor1">
	<div class="easyui-panel" data-options="region:'center',border:false,collapsible:true" iconCls="icon-dingdan" title="基本信息"  style="margin-bottom:20px; width:100%">
                            	<div id="toolbarxd" style=" border-bottom:1px dotted #ccc; ">
								<a href="#" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="saveUser()">保 存</a>
							    </div>
                               <form id="fm" method="post" encType="multipart/form-data" novalidate>

								<input type="hidden" value="${tbXxjgb.ssxd}" name="ssxd" id="ssxd"/>
								<input type="hidden" id="idCardVal" />
                                <table id="dg" class=" fitem" data-options="idField:'id',rownumbers:false,fitColumns:true,pagination:true,singleSelect:false,toolbar:'#toolbar',url:'',method:'get',checkOnSelect:'true',selectOnCheck:'true'" style="width:800px;height:300px;" >
									
                                	<thead></thead>
                                	  <c:choose>
                                               <c:when test="${tbXxjgb.ssxd==10||tbXxjgb.ssxd==20||tbXxjgb.ssxd==40}">
                                      <tbody class="fitem ">
        								<tr>
                                            <td>学校名称：</td>
                                            <td><p id="text">${tbXxjgb.xxjgmc}</p></td>
                                            <td>地址：</td>
                                            <td><p id="text">${tbXxjgb.dz}</p></td>
                                        </tr>
                                        <tr>
                                            <td>所在区：</td>
                                            <td><p id="text">${tbCfgXzqhdm.xzqhjc}</p></td>
                                            <td>办学类型：</td>
                                            <td><p id="text"><app:dictname dictid="XXJG_BXLX${tbXxjgb.bxlx}"/></p></td>
                                        </tr>
                                        <tr>
                                            <td>举办单位：</td>
                                            <td><p id="text">${tbXxjgb.jbdw}</p></td>
                                            <td>举办者类型：</td>
                                            <td><p id="text"><app:dictname dictid="XXJG_JBZLX${tbXxjgb.jbzlx}"/></p></td>
                                        </tr>
                                           <c:if test="${tbXxjgb.ssxd==40}">
	                                        <tr>
	                                        <td>驻地城乡类型：</td>
	                                            <td>
	                                               <p id="text"><app:dictname dictid="XXJG_ZDCXLX${tbXxjgb.zdcxlx}"/></p>
	                                            </td>
	                                        </tr>  
                                        </c:if> 
                                        <tr>
                                            <td><label>姓名：<span style="color:red;">*</span></label></td>
                                           <td><input type="text" name="xm" class="easyui-textbox" id="name" /></td>
                                            <td rowspan="5">照片预览：<br><span style="color:#19aa8d; line-height:20px;">注：近期免冠半身照片，<br>照片规格：宽154像素，<br>高189像素，<br>分辨率150dpi以上，<br>照片要求为jpg格式，<br>文件大小应小于60K。</span></td>
                                            <td rowspan="5" >
                                            <input type="hidden" id="fileName" name="fileName" value=""/>
                                            <img id="img" style="width:100px; margin-right:35px" src="">
                                            <br/>
                                            <input style="background:#ccc; color:white; border:1px solid #999; width:100px; margin-right:35px" type="button" id="deletePhoto" value="删除照片">
                                          </td>
                                        </tr>
                                        
                                        <tr>
                                            <td>性别：<span style="color:red;">*</span></td>
                                            <td>
                                             <select class="easyui-combobox"  name="xb" id="xb" >
                                            <app:dictselect dictType="JSXX_XB" />
                                            </select>
                                           </td> 
                                        </tr>
                                        <tr>
                                        	<td>教育ID：</td>
                                            <td><p id="text"></p></td>
                                        </tr>
                                        <tr>
                                        	<td>身份证件类型：<span style="color:red;">*</span></td>
                                            <td>
                                            <select class="easyui-combobox" name="sfzjlx" id="sfzjlx" >
                                            <app:dictselect dictType="JSXX_SFZJLX" selectValue="156"/>
                                            </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>证件号码：<span style="color:red;">*</span></td>
                                            <td><input type="text" name="sfzjh" class="easyui-textbox" id="sfzjh" /></td>                                     
                                        </tr>
                                        <tr>
                                        	<td>出生日期：<span style="color:red;">*</span></td>
                                            <td><input type="text" name="csrq" id="csrq" class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',maxDate:'#F{$dp.$D(\'cjgzny\')}'})"/></td> 
                                            <td>个人照片：</td>
                                            <td>
                                            <input type="hidden" name="grzp" id="grzp" value=""/>
	                                            <a href="#" style="width:100px" class="easyui-linkbutton" iconCls="icon-add" onclick="javascript:$('#updlg').dialog('open')">上传照片</a>
                                            	 <a href="#" style="width:68px" class="easyui-linkbutton" iconCls="icon-help" onclick="javascript:$('#help').dialog('open')">帮助</a>
                                            	</td>
                                        </tr>
                                        <tr>
                                            <td>国籍/地区：<span style="color:red;">*</span></td>
                                            <td>
                                            <select class="easyui-combobox" name="gjdq" id="gjdq" >
                                             		 <app:dictselect dictType="JSXX_GJDQ" selectValue="156"/>
                                            </select>
                                            </td>
                                            <td>出生地：<span style="color:red;">*</span></td>
                                            <td>
                                            <input type="text" class="easyui-searchbox" name="csd1" data-options="searcher:doSearch2"  id="csd" >
                                           <input type="hidden" name="csd"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>民族：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox"  name="mz" id="mz">
                                                 <app:dictselect dictType="JSXX_MZ" selectValue="01"/>
                                            </select></td>
                                            <td>政治面貌：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox"  name="zzmm" id="zzmm" data-options="multiple:true,multiline:true">
                                                 <app:dictselect dictType="JSXX_ZZMM" selectValue="13" />
                                            </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>参加工作年月：<span style="color:red;">*</span></td>
                                            <td><input type="text" name="cjgzny" id="cjgzny" class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM',minDate:'#F{$dp.$D(\'csrq\')}',maxDate:'#F{$dp.$D(\'jbxny\')}'})"></td>
                                            <td>进本校年月：<span style="color:red;">*</span></td>
                                            <td><input type="text" name="jbxny"  id="jbxny" class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM',minDate:'#F{$dp.$D(\'cjgzny\')}'})" ></td>
                                        </tr>
                                        <tr>
                                            <td>教职工来源：<span style="color:red;">*</span></td>
                                            <td>
                                          <%--   <select class="easyui-combobox" required="true" name="jzgly" id="">
                                                 <app:dictselect dictType="JSXX_JZGLY" selectValue="${tbBizJzgjbxx.jzgly}"/>
                                            </select> --%>
                                             <input type="text" class="easyui-searchbox" name="jzgly" data-options="searcher:doSearch"  id="jzgly" >
                                            	<input type="hidden" name="jzgly"/>
                                            </td>
                                            <td>教职工类别：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox" name="jzglb" id="jzglb">
                                               <appadd:dictselectadd dictType="JSXX_JZGLB" schoolType="${tbXxjgb.ssxd}"/>
                                            </select></td>
                                        </tr>
                                        
                                        <c:choose>
                                       
                                          <c:when test="${tbXxjgb.ssxd==10}">
                                        <tr>
                                            <td>是否在编：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox" required="true" name="sfzb" id="sfzb">
                                             <app:dictselect dictType="JSXX_SFZB" selectValue="1"/>
                                            </select></td>
                                            <td>学缘结构：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox" required="true" name="xyjg" id="xyjg" data-options="multiple:true,multiline:true">
                                                <app:dictselect dictType="JSXX_XYJG" />
                                            </select></td>
                                        </tr>
                                        <tr>
                                            <td>签订合同情况：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox" required="true" name="qdhtqk" id="qdhtqk">
                                                <app:dictselect dictType="JSXX_QDHTQK" selectValue="1"/>
                                            </select></td>
                                            <td>用人形式：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox" required="true" name="yrxs" id="yrxs">
                                              <appadd:dictselectadd dictType="JSXX_YRXS" schoolType="${tbXxjgb.ssxd}"/>
                                            </select></td>
                                        </tr>
                                        <tr style="text-align:right">
                                            <td>人员状态：<span style="color:red;">*</span></td>
                                             <td><select class="easyui-combobox" required="true" name="zgqk" id="zgqk">
                                                <app:dictselect dictType="JSXX_ZGQK" />
                                            </select></td>
                                        </tr>
                                      </c:when> 
                                      
                                      <c:otherwise>
	                                            <tr>
	                                            <td>是否在编：<span style="color:red;">*</span></td>
	                                            <td><select class="easyui-combobox" name="sfzb" id="sfzb">
	                                             <app:dictselect dictType="JSXX_SFZB" selectValue="1"/>
	                                            </select></td>
	                                           <td>企业工作(实践)时长：<span style="color:red;">*</span></td>
	                                            <td>
	                                            <select class="easyui-combobox" required="true" name="qygzsjsc" id="qygzsjsc">
	                                             <appadd:dictselectadd dictType="JSXX_QYGZSJSC" schoolType="${tbXxjgb.ssxd}" selectValue="0"/>
	                                            </select>
	                                             </td>   
	                                        </tr>
	                                            <tr>
	                                            <td>是否“双师型”教师：<span style="color:red;">*</span></td>
	                                            <td><select class="easyui-combobox" required="true" name="sfss" id="sfss">
	                                             <app:dictselect dictType="JSXX_SFSS" />
	                                            </select></td>
	                                            <td>是否具备职业技能等级证书：<span style="color:red;">*</span></td>
	                                            <td><select class="easyui-combobox" required="true" name="sfjbzyjndjzs" id="sfjbzyjndjzs">
	                                                <app:dictselect dictType="JSXX_SFJBZYJNDJZS" />
	                                            </select></td>
	                                        </tr>
	                                        <tr>
	                                            <td>签订合同情况：<span style="color:red;">*</span></td>
	                                            <td><select class="easyui-combobox" required="true" name="qdhtqk" id="qdhtqk">
	                                                <app:dictselect dictType="JSXX_QDHTQK" selectValue="1"/>
	                                            </select></td>
	                                            <td>用人形式：<span style="color:red;">*</span></td>
	                                            <td><select class="easyui-combobox" required="true" name="yrxs" id="yrxs">
	                                              <appadd:dictselectadd dictType="JSXX_YRXS" schoolType="${tbXxjgb.ssxd}"/>
	                                            </select></td>
	                                        </tr>
	                                        
	                                        
	                                         <c:if test="${tbXxjgb.ssxd==20}">
		                                        <tr style="text-align:right">
		                                            <td>人员状态：<span style="color:red;">*</span></td>
	                                             <td><select class="easyui-combobox" required="true" name="zgqk" id="zgqk">
			                                                <app:dictselect dictType="JSXX_ZGQK" />
			                                            </select></td>
		                                        </tr>
	                                        </c:if>
	                                        <c:if test="${tbXxjgb.ssxd==40}">
		                                        <tr style="text-align:right">
		                                            <td>骨干教师类型：</td>
		                                            <td><select class="easyui-combobox" required="true" name="ggjslx" id="ggjslx">
		                                                <app:dictselect dictType="JSXX_GGJSLX" selectValue="0"/>
		                                            </select></td>
		                                            <td>人员状态：</td>
		                                            <td><select class="easyui-combobox" required="true" name="zgqk" id="zgqk">
			                                                <app:dictselect dictType="JSXX_ZGQK" />
			                                            </select></td>
		                                        </tr>
	                                        </c:if>
	                                        
	                                        <tr style="text-align:right">
	                                            
	                                        </tr>
                                      </c:otherwise> 
                                     </c:choose> 
                                    </tbody>
                             </c:when>
                             
                            
                             <c:otherwise>
                      			<tbody class="fitem">
        								<tr>
                                            <td>学校名称：</td>
                                            <td><p id="text">${tbXxjgb.xxjgmc}</p></td>
                                            <td>地址：</td>
                                            <td><p id="text">${tbXxjgb.dz}</p></td>
                                        </tr>
                                        <tr>
                                            <td>所在区：</td>
                                            <td><p id="text">${tbCfgXzqhdm.xzqhjc}</p></td>
                                            <td>办学类型：</td>
                                            <td><p id="text"><app:dictname dictid="XXJG_BXLX${tbXxjgb.bxlx}"/></p></td>
                                        </tr>
                                        <tr>
                                            <td>举办单位：</td>
                                            <td><p id="text">${tbXxjgb.jbdw}</p></td>
                                            <td>举办者类型：</td>
                                            <td><p id="text"><app:dictname dictid="XXJG_JBZLX${tbXxjgb.jbzlx}"/></p></td>
                                        </tr>
	                                        <tr>
	                                        <td>驻地城乡类型：</td>
	                                            <td>
	                                               <p id="text"><app:dictname dictid="XXJG_ZDCXLX${tbXxjgb.zdcxlx}"/></p>
	                                            </td>
	                                        </tr>  
                                        <tr>
                                            <tr>
                                            <td><label>姓名：<span style="color:red;">*</span></label></td>
                                            <td><input type="text" name="xm" class="easyui-textbox" id="name"/></td>
                                            <td rowspan="5">照片预览：<br><span style="color:#19aa8d; line-height:20px;">注：近期免冠半身照片，<br>照片规格：宽154像素，<br>高189像素，<br>分辨率150dpi以上，<br>照片要求为jpg格式，<br>文件大小应小于60K。</span></td>
                                            <td rowspan="5" >
                                            <input type="hidden" id="fileName" name="fileName" value=""/>
                                            <img id="img" style="width:100px; margin-right:35px" src=""><br><input style="background:#ccc; color:white; border:1px solid #999; width:100px; margin-right:35px" type="button" id="deletePhoto" value="删除照片">
                                          </td>
                                        </tr>
                                        
                                        <tr>
                                            <td>性别：<span style="color:red;">*</span></td>
                                            <td>
                                             <select class="easyui-combobox" name="xb" id="xb" >
                                            <app:dictselect dictType="JSXX_XB" />
                                            </select>
                                           </td> 
                                        </tr>
                                        <tr>
                                        	<td>教育ID：</td>
                                            <td><p id="text"></p></td>
                                        </tr>
                                        <tr>
                                        	<td>身份证件类型：<span style="color:red;">*</span></td>
                                            <td>
                                            <select class="easyui-combobox" required="true" name="sfzjlx" id="sfzjlx" >
                                            <app:dictselect dictType="JSXX_SFZJLX" selectValue="156"/>
                                            </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>证件号码：<span style="color:red;">*</span></td>
                                              <td><input type="text" name="sfzjh" class="easyui-textbox" id="sfzjh" /></td>                                         
                                          </tr>
                                      <tr>
                                        	<td>出生日期：<span style="color:red;">*</span></td>
                                            <td><input type="text" name="csrq"  id="csrq" class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',maxDate:'#F{$dp.$D(\'cjgzny\')}'})"/></td> 
                                            <td>个人照片：<span style="color:red;">*</span></td>
                                            <td>
                                            	<input type="hidden" name="grzp" id="grzp" value=""/>
	                                            <a href="#" style="width:100px" class="easyui-linkbutton" iconCls="icon-add" onclick="javascript:$('#updlg').dialog('open')">上传照片</a>
                                            	 <a href="#" style="width:68px" class="easyui-linkbutton" iconCls="icon-help" onclick="javascript:$('#help').dialog('open')">帮助</a>
                                            </td>
                                        </tr>
                                            <tr>
                                            <td>国籍/地区：<span style="color:red;">*</span></td>
                                            <td>
                                            <select class="easyui-combobox"  name="gjdq" id="gjdq" >
                                             		 <app:dictselect dictType="JSXX_GJDQ" selectValue="156"/>
                                            </select>
                                            </td>
                                            <td>出生地：<span style="color:red;">*</span></td>
                                            <td>
                                            <input type="text" class="easyui-searchbox" name="csd1" data-options="searcher:doSearch2"  id="csd" >
                                           <input type="hidden" name="csd"/>
                                            </td>
                                        </tr>
                                          <tr>
                                            <td>民族：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox" name="mz" id="mz">
                                                 <app:dictselect dictType="JSXX_MZ" selectValue="01"/>
                                            </select></td>
                                            <td>政治面貌：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox"  name="zzmm" id="zzmm" data-options="multiple:true,multiline:true" >
                                                 <app:dictselect dictType="JSXX_ZZMM" selectValue="13" />
                                            </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>参加工作年月：<span style="color:red;">*</span></td>
                                            <td><input type="text" name="cjgzny"   id="cjgzny" class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM',minDate:'#F{$dp.$D(\'csrq\')}',maxDate:'#F{$dp.$D(\'jbxny\')}'})"></td>
                                            <td>
                                            <c:choose>
                                                <c:when test="${tbXxjgb.ssxd==60}">
                                                                                              进本园年月：
                                              </c:when>
                                              <c:otherwise>
                                                                                             进本校年月：
                                              </c:otherwise>
                                            </c:choose>
                                            <span style="color:red;">*</span></td>
                                            <td><input type="text" name="jbxny"  id="jbxny" class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM',minDate:'#F{$dp.$D(\'cjgzny\')}'})"></td>
                                        </tr>
                                       <tr>
                                            <td>教职工来源：<span style="color:red;">*</span></td>
                                            <td>
                                          <%--   <select class="easyui-combobox" required="true" name="jzgly" id="">
                                                 <app:dictselect dictType="JSXX_JZGLY" selectValue="${tbBizJzgjbxx.jzgly}"/>
                                            </select> --%>
                                             <input type="text" class="easyui-searchbox" name="jzgly1" data-options="searcher:doSearch"  id="jzgly" >
                                            	<input type="hidden" name="jzgly"/>
                                            </td>
                                            <td>教职工类别：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox" name="jzglb" id="jzglb">
                                                <appadd:dictselectadd dictType="JSXX_JZGLB" schoolType="${tbXxjgb.ssxd}"/>
                                            </select></td>
                                        </tr>
                                            <tr>
                                            <td>是否在编：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox"  name="sfzb" id="sfzb">
                                             <app:dictselect dictType="JSXX_SFZB" selectValue="1"/>
                                            </select></td>
                                           <td>用人形式：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox"  name="yrxs" id="yrxs">
                                              <appadd:dictselectadd dictType="JSXX_YRXS" schoolType="${tbXxjgb.ssxd}"/>
                                            </select></td>
                                        </tr>
                                   
                                        
                                        <tr>
                                           <td>签订合同情况：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox"  name="qdhtqk" id="qdhtqk">
                                                <app:dictselect dictType="JSXX_QDHTQK" selectValue="1"/>
                                            </select></td>
                                            <td>是否全日制师范类专业毕业：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox"  name="sfsflzyby" id="sfsflzyby">
                                                <app:dictselect dictType="JSXX_SFSFLZYBY" />
                                            </select></td>
                                        </tr>
                                        <c:if test="${tbXxjgb.ssxd==30||tbXxjgb.ssxd==50}">
                                        <tr>
                                            <td>是否受过特教专业培养培训：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox"  name="sfsgtjzypypx" id="sfsgtjzypypx">
                                                <app:dictselect dictType="JSXX_SFSGTJZYPYPX" />
                                            </select></td>
                                            <td>是否有特殊教育从业证书：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox"  name="sfytsjycyzs" id="sfytsjycyzs">
                                                 <app:dictselect dictType="JSXX_SFYTSJYCYZS" />
                                            </select></td>
                                        </tr>
                                       </c:if> 
                                        <tr>
                                            <td>信息技术应用能力：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox"  name="xxjsyynl" id="xxjsyynl">
                                                <app:dictselect dictType="JSXX_XXJSYYNL" />
                                            </select></td>
                                            <td>是否属于免费（公费）师范生：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox"  name="sfsymfsfs" id="sfsymfsfs">
                                                <app:dictselect dictType="JSXX_SFSYMFSFS" selectValue="0"/>
                                            </select></td>
                                        </tr>
                                        <c:choose>
                                          <c:when test="${tbXxjgb.ssxd==30}">
                                         <tr>
                                            <td>继续教育编号：</td>
                                            <td><input type="text" class="easyui-tooltip" title="凡有继续教育编号的人员请务必填写" name="jxjybh"  id="jxjybh" ></td>
                                            <td>骨干教师类型：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox"  name="ggjslx" id="ggjslx">
                                                <app:dictselect dictType="JSXX_GGJSLX" selectValue="0"/>
                                            </select></td>
                                        </tr>
                                        <tr>
                                            <td>是否参加基层服务项目：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox"  name="sfstgjs" id="sfstgjs">
                                                <app:dictselect dictType="JSXX_SFSTGJS" selectValue="0"/>
                                            </select></td>
                                            <td>是否心理健康教育教师：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox" name="sfxljkjyjs" id="sfxljkjyjs">
                                                  <app:dictselect dictType="JSXX_SFXLJKJYJS"/>
                                            </select></td>
                                        </tr>
                                        <tr>
                                            <td>人员状态：<span style="color:red;">*</span></td>
                                            <td>
                                            <select class="easyui-combobox" name="zgqk" id="zgqk">
                                                <app:dictselect dictType="JSXX_ZGQK"/>
                                            </select>
                                            </td>
                                        </tr>
                                        </c:when>
                                        <c:otherwise>
                                        <tr>
                                            <td>是否参加基层服务项目：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox"  name="sfstgjs" id="sfstgjs">
                                                <app:dictselect dictType="JSXX_SFSTGJS" selectValue="0" />
                                            </select></td>
                                            <td>是否心理健康教育教师：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox"  name="sfxljkjyjs" id="sfxljkjyjs">
                                                  <app:dictselect dictType="JSXX_SFXLJKJYJS" />
                                            </select></td>
                                        </tr>
                                        <tr>
                                        	<td>骨干教师类型：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox"  name="ggjslx" id="ggjslx">
                                                <app:dictselect dictType="JSXX_GGJSLX" selectValue="0"/>
                                            </select></td>
                                            <td>人员状态：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox"  name="zgqk" id="zgqk">
                                                <app:dictselect dictType="JSXX_ZGQK" />
                                            </select></td>
                                        </tr>
                                         <c:if test="${tbXxjgb.ssxd==50}">
                                            <tr>
                                            <td>从事特教起始年月：<span style="color:red;">*</span></td>
                                            <td>
                                            <input id="cstjqsny" name="cstjqsny" class="Wdate" type="text"  onfocus="WdatePicker({dateFmt:'yyyy-MM'})"/>
                                            </td>
                                           </tr>
                                         </c:if> 
                                          <c:if test="${tbBizJzgjbxx.tbXxjgb.ssxd==60}">
                                           <td>是否受过学前教育专业培养培训：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox"  name="sfsgxqjyzypypx" id="sfsgxqjyzypypx">
                                                  <app:dictselect dictType="JSXX_SFSGXQJYZYPYPX" selectValue="${tbBizJzgjbxx.sfsgxqjyzypypx}"/>
                                            </select></td>
                                              <td>是否全日制学前教育专业毕业：<span style="color:red;">*</span></td>
                                            <td><select class="easyui-combobox"  name="sfxqjyzyby" id="sfxqjyzyby">
                                                  <app:dictselect dictType="JSXX_SFXQJYZYBY" selectValue="${tbBizJzgjbxx.sfxqjyzyby}"/>
                                            </select></td>
                                         </c:if>
                                        </c:otherwise>
                                       </c:choose> 
                                        
                                    </tbody>
                                             		</c:otherwise> 
                                       </c:choose>
    							</table>
    							</form>
    					<div id="updlg" class="easyui-dialog" title="上传照片" style="width:350px;height:200px; overflow:auto;padding:10px 20px" closed="true" buttons="#updlg-buttons">
					          <form id="fileform2" onsubmit="return false;">
					             	选择照片： <input style="margin-right:10px;" type="file" class="filetype" id="file" name="file" value="">
					           </form>
						</div>
						<div id="help" class="easyui-dialog" title="帮助" style="width:350px;padding:10px 20px" closed="true">
					          <p>操作步骤：</p>
					          <p>1.单击电脑左下角，单击所有程序，单击附件，单击画图，打开画图软件</p>
					          <p>2.单击软件左上角“画图”按钮，单击“打开”，选择要修改的照片</p>
					          <p>3.单击软件上方操作栏“重新调整大小”按钮，弹出窗口，单击“像素”，点掉“保持纵横比”前面的对号，调整水平154像素，垂直189像素，单击“确定”</p>
					          <p>4.单击左上角“保存”按钮</p>
					          <p>5.原图片即可上传使用</p>
						</div>
						<div id="updlg-buttons">
							<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="upload()">上传</a>
							<a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#updlg').dialog('close')">关闭</a>
						</div>
    					<!--easyUi树(jzgly) -->
                                    
                                         <div id="dd" class="easyui-dialog" title="教职工来源" closed="true" data-options="modal:true" style="width:50%;height:300px;overflow-y:auto;overflow-x:hidden;">
                                        	<input type="hidden" name="treeData" data="${jzglylist}"/>
                                        	<ul id="tt" class="easyui-tree">
											    
											</ul>
                                        </div>
    					
    					<!--easyUi树(csd) -->
                                    
                                         <div id="ddcsd" class="easyui-dialog" title="出生地" closed="true" data-options="modal:true" style="width:50%;height:300px;overflow-y:auto;overflow-x:hidden;">
                                        	<input type="hidden" name="treeData2" data="${csdlist}"/>
                                        	<ul id="ttcsd" class="easyui-tree">
											    
											</ul>
                                        </div>
                                         <div id="tips" class="easyui-dialog" title="温馨提示" style="padding:10px 20px;width:200px;height:100px;line-height:40px;text-align:center;overflow:auto;font-size:20px;" closed="true" data-options="modal:true">请选择一条记录</div>
                                
    						</div>
    </div>
    <script type="text/javascript">
    $('#jxjybh').tooltip({
		position:'right',
		trackMouse:true,
		onShow:function(){
			$(this).tooltip('tip').css({
				borderColor:'red',
				color:'red'
			});
		}
	})
    
    $('#zzmm').combobox({ editable:false });//民族
    $('#jzgly').searchbox({ editable:false }); //教职工来源
    $('#jzglb').combobox({ editable:false }); //教职工类别
    $('#sfzb').combobox({ editable:false }); //是否在编
    $('#yrxs').combobox({ editable:false }); //用人形式
    $('#qdhtqk').combobox({ editable:false }); //签订合同情况
    $('#zgqk').combobox({ editable:false });//在岗情况（人员状态）
    $('#sfzjlx').combobox({ editable:false });//身份证件类型
    
    $('#gjdq').combobox({ editable:false }); //下拉框禁止输入 国籍/地区
    $('#csd').searchbox({ editable:false }); //搜索框禁止输入  出生地
    $('#mz').combobox({ editable:false });//民族
    $('#sfsymfsfs').combobox({ editable:false }); //信息技术应用能力
    $('#qygzsjsc').combobox({ editable:false }); //工作实践时长


    $('#sfstgjs').combobox({ editable:false }); //是否参加基层服务项目
    $('#sfxljkjyjs').combobox({ editable:false }); //是否心理健康教育教师
    $('#ggjslx').combobox({ editable:false }); //骨干教师类型
    $('#cstjqsny').datebox({ editable:false }); //从事特教起始年月
    
    $('#sfjbzyjndjzs').combobox({ editable:false }); //是否在编
    $('#sfxqjyzyby').combobox({ editable:false }); //是否全日制学前教育专业毕业
    $('#sfsgxqjyzypypx').combobox({ editable:false }); //是否受过学前教育专业培养培训
    $('#xb').combobox({ editable:false }); //性别
    $('#sfsflzyby').combobox({ editable:false }); //是否全日制师范类专业毕业
    $('#sfytsjycyzs').combobox({ editable:false }); //是否有特殊教育从业证书
    $('#sfsgtjzypypx').combobox({ editable:false }); //是否受过特教专业培养培训
    $('#xxjsyynl').combobox({ editable:false }); //信息应用技术
    $('#xyjg').combobox({ editable:false }); //学缘结构
    
     	//关闭窗口
          function save_close(dlgName){
        	  $(dlgName).dialog('close');
     	 }
    		 //保存
    		   function saveUser(){
    			 
     		       $('#fm').form('submit',{
     		    	   //zhanwei是为了解决文件上传问题，没有这个占位处理器处理会出现异常
    		           url: "${ctx}/basicInformation/TeacherBaseInfoAction.a?add=zhanwei",
    		           onSubmit: function(){
    		        	   //$(this).form('validate')
    		               return verify();
    		           },
    		           success: function(){
    		        	   $.messager.confirm('温馨提示', '保存完成',function(){
    		        		   window.location.reload();
    		        	   });
    		           }
    		       }); 
    		   }
    		 
  		  
    	 //验证是否需要填写民族
    	   $('#gjdq').combobox({
    		   onSelect: function(param){
    			   var gjdq = $("input[type='hidden'][name='gjdq']").val();
    			   if(gjdq!=156){
    				   $('#mz').combobox('setValue',' ');
    				   $('#mz').combobox('disable');
    			   }
    			   if(gjdq==156){
    				   $('#mz').combobox('setValue',01);
    				   $('#mz').combobox("enable");
    			   }
				}
			});
    	
    	 
    	 //删除照片
    	 $('#deletePhoto').click(function(){
    		 		if($("#img").attr("src") == ""){
    		 			return;
    		 		}
    		 		$.messager.confirm('温馨提示', '是否要删除照片？', function(r){
	    		      if(r){
	    		    	  deleteImage() 
	    				}
    		      });
    		
    	 });
    	 
     	/**
    	* 树查询教职工来源
    	*/
    	  function doSearch(){
			$('#dd').dialog('open');
			$("#dd").panel("move",{top:$(document).scrollTop() + ($(window).height()-260) * 0.5});
			$('.window-shadow').css('top',$(document).scrollTop() + ($(window).height()-260) * 0.5);
			 var dataStrKey = $('input[name="treeData"][type="hidden"]').attr('data').replace(/\'/g,'\"');
			 var data = JSON.parse(dataStrKey);
			 $('#tt').tree({
				 data:data
			 });
			 $('#tt').tree('collapseAll');
		}
    	 
    	 	/**
      	* 树查询csd
      	*/
      	  function doSearch2(){
  			$('#ddcsd').dialog('open');
			$("#ddcsd").panel("move",{top:$(document).scrollTop() + ($(window).height()-260) * 0.5});
			$('.window-shadow').css('top',$(document).scrollTop() + ($(window).height()-260) * 0.5);
  			 var dataStrKey = $('input[name="treeData2"][type="hidden"]').attr('data').replace(/\'/g,'\"').replace(/(\"id\":)(.*?)(\,)/g,'$1\"$2\"$3');
  			 var data = JSON.parse(dataStrKey);
  			 $('#ttcsd').tree({
  				 data:data
  			 });
  			$('#ttcsd').tree('collapseAll');
  		}
    	 //树选择
		 $(function(){
			$('#tt').tree({
				onClick:function(node){
					$("#jzgly").searchbox('setValue', node.text);
					save_close("#dd");
					$("input[name='jzgly'][type='hidden']").val(node.id);
				}
			})
			$('#ttcsd').tree({
				onClick:function(node){
					$("#csd").searchbox('setValue', node.text);
					save_close("#ddcsd");
					$("input[name='csd'][type='hidden']").val(node.id);
				}
			})
		})
		
    	 
	
		function addTab(title, url){
			if ($('#tb').tabs('exists', title)){
				$('#tb').tabs('select', title);
			} else {
				var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
				$('#tb').tabs('add',{
					title:title,
					content:content,
					closable:true
				});
			}
		}
		//关闭所有的tab  
		    function closeAll(){  
		        var tiles = new Array();  
		          var tabs = $('#tb').tabs('tabs');      
		          var len =  tabs.length;           
		          if(len>0){  
		            for(var j=0;j<len;j++){  
		                var a = tabs[j].panel('options').title;               
		                tiles.push(a);  
		            }  
		            for(var i=1;i<tiles.length;i++){               
		                $('#tb').tabs('close', tiles[i]);  
		            }
		          }
		    } 
		//关闭当前的tab 
			function removePanel(){
				var tab = $('#tb').tabs('getSelected');
				    if (tab){
					var index = $('#tb').tabs('getTabIndex', tab);
					$('#tb').tabs('close', index);
					}
				}
		
			 //验证是否需要填写民族\政治面貌
	    	   $('#gjdq').combobox({
	    		   onSelect: function(param){
	    			   var gjdq = $("input[type='hidden'][name='gjdq']").val();
	    			   if(gjdq!=156){
	    				   $('#mz').combobox('setValue','');
	    				   $('#zzmm').combobox('setValue','');
	    				   $('#mz').combobox('disable');
	    				   $('#zzmm').combobox('disable');
	    			   }
	    			   if(gjdq==156){
	    				   $('#mz').combobox('setValue',01);
	    				   $('#zzmm').combobox('setValue',13);
	    				   $('#mz').combobox("enable");
	    				   $('#zzmm').combobox("enable");
	    			   }
					}
				});
		
			 
	    	   /**
	    		  *是否在编验证
	    		  */
	    		  
	    		  $('#sfzb').combobox({
	    			  onSelect: function(param){
	     			   var sfzb = $("input[type='hidden'][name='sfzb']").val();
	     			   if(sfzb==0){
	     				   $('#qdhtqk').combobox('setValue',2);
	     				$('#yrxs').combobox('setValue',1);
	     				   $('#yrxs').combobox('enable');
	     			   }
	     			   if(sfzb==1){
	     				   $('#yrxs').combobox('setValue',' ');
	     				   $('#qdhtqk').combobox('setValue',1);
	     				   $('#yrxs').combobox("disable");
	     			   }
	  				}
	    		  });
			 
			 
			$('#zzmm').combobox({
				onSelect:function(record){
				var v = $('#zzmm').combobox("getValue");
				
				if(v == '12' || v == '13'){
					$('#zzmm').combobox("setValue",record.value);
				}
				
				if(record.value == '12' || record.value == '13'){
					$('#zzmm').combobox("clear");
					$('#zzmm').combobox("setValue",record.value);
				}
			}
			});
			$('#xyjg').combobox({
				onSelect:function(record){
					var v = $('#xyjg').combobox("getValue");
					if(v == '4'){
						$('#xyjg').combobox("setValue",record.value);
					}
				if(record.value == '4'){
					$('#xyjg').combobox("clear");
					$('#xyjg').combobox("setValue",record.value);
				}
			}
			});

	    	
	    	window.onload = function() { 
	    		$('#sfzjlx').combobox({
		        	onSelect:function(record){
		        		$('#idCardVal').attr('val',record.value);
		        	}
		        });
		    	
	    		$('#idCardVal').attr('val',$('#sfzjlx').combobox("getValue"));
		    	$("#sfzjh").next("span").find('input:first').blur(function(){
		        	if($('#idCardVal').attr('val')=='1'){
		    			if(ValidateUtils.validIdCard($('#sfzjh').textbox("getValue"))==false){
		    				$.messager.alert('温馨提示','请输入正确的身份证号','info');
		    				return false;
		    			}else{
		    				var birthday = $('#sfzjh').textbox("getValue").substring(6,14).replace(/(\d{4})(\d{2})(\d{2})/g,'$1-$2-$3');
		    				$('#csrq').val(birthday);
		    			}
		    		}
		        	
		        	var cardType=$('#idCardVal').attr('val');
		        	var cardId = $('#sfzjh').textbox("getValue");
		        	if(cardType == ""){
		        		cardType="1";
		        	}
		        	 $.ajax({
		    				url:'${ctx}/schoolManager/TeacherAccountManageAction.a?checkAddHm&time='+new Date().getTime(),
		    				data:{shenfen:cardType,tCNo:cardId},
		    				dataType : "json",
		    				success:function(data){
		    					if(data.isValid==1){
		    						
		    					}else{
		    						$.messager.alert('温馨提示','输入的身份证号已经存在，请执行调入操作！','info');
		    					}
		    				},
		    				error:function(XMLHttpRequest, textStatus, errorThrown){
		    					var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
		    					sessionTimeout(sessionstatus);
		    				}
		    		 });
		    	});
	    	};
   </script>
</body>
</html>
