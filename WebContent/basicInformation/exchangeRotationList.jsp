<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>北京市教师管理服务平台</title>
    <link rel="stylesheet" type="text/css" href="../css/newcss.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
    <script type="text/javascript" src="../easyui/jquery.min.js"></script>
    <script src="../js/echarts/build/dist/echarts-all.js"></script>
    <script src="../js/echarts/build/dist/echarts.js"></script>
    <script type="text/javascript" src="../easyui/easyloader.js"></script>
    <script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx}/js/refresh.js"></script>
    <script type="text/javascript" src="../My97DatePicker/WdatePicker.js"></script>
    <link rel="stylesheet" type="text/css" href="../My97DatePicker/skin/WdatePicker.css">
    
	
</head>
<body class="easyui-layout" style="margin:0 1px;background-color:#f5f5f5 !important;">  
   <!--  <div data-options="region:'south',split:true" style="height:40px;text-align:center;line-height:30px;background:#19aa8d;color:#f5f5f5;">北京市教师管理服务平台（版本：1.2.2.16082516）</div> -->
    
    <div data-options="region:'center'" style="background:#fff;border:0;">
	    <div id="tb" class="" data-options="tools:'#tab-tools',fit:true">
			<!-- <div title="个人信息维护" iconCls="icon-home" style="padding:15px;"> -->
		        <div class="easyui-layout" data-options="fit:true">
                        <div id="anchor130">
                            <div data-options="region:'center',border:false,collapsible:true" style="margin-bottom:20px;">
                            <form id="frm" method="post" action="${ctx}/basicInformation/ExchangeRotationAciton.a" novalidate >
                                <div id="toolbarJL">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUserJL()">新 增</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">编 辑</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a>
                                </div>
                                <input type="hidden" value="${tbJzgxxsh.shzt}" id="xxshzt"/>
                                <table id="dg" class="easyui-datagrid" title="交流轮岗" iconCls="icon-dingdan"
                                    data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,collapsible:true,toolbar:'#toolbarJL',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
                                <thead>
                                    <tr>
                                        <th field="ck" checkbox="true"></th>
                                        <th data-options="field:'id',width:100">交流轮岗类型</th>
                                        <th data-options="field:'sheewrncha',width:100">是否调动人事关系</th>
                                        <th data-options="field:'shewencdsha',width:100">开始年月</th>
                                        <th data-options="field:'shensdfcha',width:100">原单位名称</th>
                                        <th data-options="field:'shengfgcha',width:100">交流轮岗单位名称</th>
                                        <th data-options="field:'chacerhong',width:100">创建时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test="${not empty pageObj.pageElements}">
									<c:forEach items="${pageObj.pageElements}" var="item" varStatus="status">
										<tr>
											<td>${item.exchangeRotationId}</td>
											<td>${item.exchangeRotationTypeName}</td>
											<td><app:dictname dictid="JSXX_SFDDRSGX${item.perMobilization}" />
												<%-- <c:choose>
													<c:when test="${item.perMobilization=='0'}">
														否
													</c:when>
													<c:when test="${item.perMobilization=='1'}">
														是
													</c:when>
													<c:otherwise>
														是否人事调动状态有误
													</c:otherwise>
												</c:choose> --%>
											</td>
											<td>${item.beginYearM}</td>
											<td>${item.beforeCompany}</td>
											<td>${item.nowCompany}</td>
											<td>${item.createTime}</td>
										</tr>
									</c:forEach>
								</c:if>
								<c:if test="${empty pageObj.pageElements}">
									<tr>
										<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
									</tr>
								</c:if>
                                </tbody>
                                </table>
                                <!-- 分页所必须的 -->
								<div class="page tar mb15">
									<input name="pageNo" value="${pageNo }" type="hidden" />
									<c:if test="${not empty pageObj.pageElements}">
										<jsp:include page="../common/pager-nest.jsp">
											<jsp:param name="toPage" value="1" />
											<jsp:param name="showCount" value="1" />
											<jsp:param name="action" value="doSearch" />
										</jsp:include>
									</c:if>
								</div>
								</form>
                                <!-- 新增或者编辑交流轮岗页面   开始 -->
                                <div id="dlgJL" class="easyui-dialog" title="新增交流轮岗" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        <div id="table-content">
                                        <form id="addfm" method="post" action="" novalidate>
                                        <input type="hidden" name="exRotInfoDto.exchangeRotationId" id="exchangeRotationIdEdit"/>
                                        <table class="fitem clean">
                                            <tbody>
                                                <tr>
                                                    <td>交流轮岗类型：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="exRotInfoDto.exchangeRotationType" id="exchangeRotationType">
                                                            <option value="">请选择</option>
                                                            <app:dictselect dictType="JSXX_JLLGLX" />
                                                        </select>
                                                    </td>
                                                    <td>是否调动人事关系：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="exRotInfoDto.perMobilization" id="perMobilization">
<!--                                                             <option value="">请选择</option> -->
                                                            <app:dictselect dictType="JSXX_SFDDRSGX" />
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>开始年月：</td>
                                                    <td>
                                                  		  <input id="beginYearM" name="exRotInfoDto.beginYearM" class="Wdate" required="true" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM',minDate:'2014-01-01'})"/>
                                                        <!-- <input type="text" class="easyui-datebox"  name="exRotInfoDto.beginYearM" id="beginYearM" required /> -->
                                                    </td>
                                                    <td>原单位名称：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="exRotInfoDto.beforeCompany" id="beforeCompany" required />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>交流轮岗单位名称：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox"  name="exRotInfoDto.nowCompany" id="nowCompany" required />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
											

										</form>
                                        </div>
                                    </div>
                                </div>
                                <!-- 新增或者编辑交流轮岗页面   结束-->
                                <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgJL').dialog('close')">取消</a>
                                </div>
                            </div>
                        </div>
					</div>
				<!-- </div> -->
			</div>
		</div>
	<div >
		
	</div>
	</div> 
	<c:if test="${SESSION_ISPUB == '1'}">
    	<div id="btn-wrap">
        <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="submitInfo()">报送</a>
        <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="cancelSubmit()">取消</a><span style="color:red">各项信息填写完整后，才能报送。报送后，不能再次修改教师信息。只有审核不通过的教师信息，才能再次编辑报送。</span>
    </div>
    </c:if>
		<div id="tips" class="easyui-dialog" title="温馨提示"
			style="padding: 10px 20px; width: 200px; height: 100px; line-height: 40px; text-align: center; overflow: auto; font-size: 20px;"
			closed="true" data-options="modal:true">请选择一条记录</div> 
<script type="text/javascript">

//$('#beginYearM').datebox({ editable:false });  //日期框禁止输入
$('#exchangeRotationType').combobox({ editable:false }); //下拉框禁止输入
$('#perMobilization').combobox({ editable:false });

$('#dg').datagrid({
	onLoadSuccess:function(data){
		if(data.rows[0].id==undefined){ 
			$('.datagrid-body td[field="id"]').html(data.rows[0].ck);
		}
	}
});
$('#dg').datagrid({
	onLoadSuccess:function(data){
		if(data.rows[0].id==undefined){
			$('#datagrid-row-r3-2-0').html(data.rows[0].ck);
			$('.datagrid-btable').css({'text-align':'center','width':'100%','padding':'30px 0'});
		}
	}
});
var url;
function save_user(dlgNmae){
    $(dlgNmae).dialog('open');
    $('#fm').form('clear');
    initStatus();////打开新增初始化页面
}

function initStatus(){
	 var gjdq = $('#exchangeRotationType').combobox("getValue");
	  
	 if(gjdq == 0){
	//是否调动人事关系
	 	$('#perMobilization').combobox('setValue','');
			$('#perMobilization').combobox('disable');
		//开始年月
			$('#beginYearM').val('');
			$('#beginYearM').attr('disabled','disabled');
		//原单位名称	
			$('#beforeCompany').textbox('setValue','');
			$('#beforeCompany').textbox('disable');
		//交流轮岗单位名称
			$('#nowCompany').textbox('setValue','');
			$('#nowCompany').textbox('disable');
	 }else{
	//是否调动人事关系
	 	$('#perMobilization').combobox('setValue','1');
			$('#perMobilization').combobox('enable');
			$('#beginYearM').removeAttr('disabled');
			$('#beforeCompany').textbox('enable');
			$('#nowCompany').textbox('enable');	
	 }
}

//新增
function newUserXX(){
    save_user('#dlgXX');
}
function newUserGZ(){
    save_user('#dlgGZ');
}
function newUserDY(){
    save_user('#dlgDY');
}
function newUserGW(){
    save_user('#dlgGW');
}
function newUserZY(){
    save_user('#dlgZY');
}
function newUserND(){
    save_user('#dlgND');
}
function newUserJS(){
    save_user('#dlgJS');
}
function newUserJY(){
    save_user('#dlgJY');
}
function newUserJX(){
    save_user('#dlgJX');
}
function newUserRX(){
    save_user('#dlgRX');
}
function newUserGN(){
    save_user('#dlgGN');
}
function newUserHW(){
    save_user('#dlgHW');
}
function newUserJN(){
    save_user('#dlgJN');
}
function newUserLX(){
    save_user('#dlgLX');
}
function newUserSD(){
    save_user('#dlgSD');
}
function newUserJL(){
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
    				save_user('#dlgJL');
			    }else{
			    	$.messager.alert('温馨提示','信息审核中或已通过审核，不可添加','warning');
			    } 
			    },
			    error:function(XMLHttpRequest, textStatus, errorThrown){
					var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
					sessionTimeout(sessionstatus);
				}

			    }) 
}
//弹出编辑页面
function editUser(){
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
	 var selected=$("input[type='checkbox'][name='ck']:checked");
	 if(selected.length<=0){
		$.messager.alert('温馨提示','请选择一条记录','warning');
		return ;
	}else if(selected.length>1){
		$.messager.alert('温馨提示','只能编辑一条记录','warning');
		return ;
	}else{	
	
		var jllgId=selected.val();
		$('#dlgJL').dialog('open').dialog('setTitle','编辑交流轮岗信息');
		$.ajax({
		    url:"${ctx}/basicInformation/ExchangeRotationAciton.a?toEdit&time="+new Date().getTime(),    //请求的url地址
		    dataType:"json",   //返回格式为json
		    //async:true,//请求是否异步，默认为异步，这也是ajax重要特性
		    data:{"jllgId":jllgId},    //参数值
		    type:"post",   //请求方式
		    success:function(data){
		    	//console.log(data.ex);
		    	console.log(JSON.stringify(data));
		        $("#exchangeRotationIdEdit").val(data.ex.exchangeRotationId);//交流轮岗id
		        $("#nowCompany").textbox('setValue',data.ex.nowCompany);//现在
		        $("#beforeCompany").textbox('setValue',data.ex.beforeCompany);//原来
		        $("#exchangeRotationType").combobox('setValue',data.ex.exchangeRotationType);//交流轮岗类型
		        $("#perMobilization").combobox('setValue',data.ex.perMobilization);//是否调动人事关系
		    	$("#beginYearM").val(data.ex.beginYearM);//开始年月
		    	
		    	initStatus();////打开打开编辑初始化页面
		    },
		    error:function(){
		        //请求出错处理
		    }
		});
		
		save_user('#dlgJL');
	}
}else{
	$.messager.alert('温馨提示','信息审核中或已通过审核，不可编辑','warning');
} 
},
error:function(XMLHttpRequest, textStatus, errorThrown){
	var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
	sessionTimeout(sessionstatus);
}

}) 
}
//删除
function removeUser(){
	var selected=$("input[type='checkbox'][name='ck']:checked");
	if(selected.length<=0){
		$.messager.alert('温馨提示','请选择一条记录','warning');
		return ;
	}
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){ 
	$.messager.confirm('温馨提示', '是否要删除记录？', function(r){
		if(r){
			var frmObj=$("#frm");
			frmObj.attr("action","${ctx}/basicInformation/ExchangeRotationAciton.a?delete");
			frmObj.submit();
			$.messager.alert('温馨提示','删除成功','info',function(){
	          	window.location.reload();
	  	  	});
		} 
	});
	 }else{
		$.messager.alert('温馨提示','信息审核中或已通过审核，不可删除','warning');
	} 
	},
	error:function(XMLHttpRequest, textStatus, errorThrown){
		var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
		sessionTimeout(sessionstatus);
	}

	}) 
}

function yanzheng(){
    var company = $("#nowCompany").textbox('getValue');//现在
    var bcompany =  $("#beforeCompany").textbox('getValue');//原来
    var lization =  $("#perMobilization").combobox('getValue');//是否调动人事关系
    var beginYea =  $("#beginYearM").val();//开始年月
    
    if(company == ""){
    	$.messager.alert('温馨提示','请填写现单位名称！','warning');
    	return false;
    }
    
    if(bcompany == ""){
    	$.messager.alert('温馨提示','请填写原单位名称！','warning');
    	return false;
    }
    
    if(lization == ""){
    	$.messager.alert('温馨提示','请选择人事关系！','warning');
    	return false;
    }
    
    if(beginYea == ""){
    	$.messager.alert('温馨提示','请填写开始年月！','warning');
    	return false;
    }
    
    if($('#beginYearM').val()==''){
		$.messager.alert('温馨提示','请选择开始年月！','warning');
		return false;
	}
	if($('#beforeCompany').val()==''){
		$.messager.alert('温馨提示','请输入原单位名称！','warning');
		return false;
	}
	if($('#nowCompany').val()==''){
		$.messager.alert('温馨提示','请输入交流轮岗单位名称！','warning');
		return false;
	}
	var jllglxEdit = $('#exchangeRotationType').combobox("getValue");
	var exchangeRotationIdEdit=$("#exchangeRotationIdEdit").val();
	var beforeCompany=$("#beforeCompany").val();
	var nowCompany=$("#nowCompany").val();
	
	if(jllglxEdit!=0 && beforeCompany==nowCompany ){
		$.messager.alert('温馨提示','原单位名称与交流轮岗单位名称不能相同！','warning');
		return false;
	}
    
    return true;
}

//保存
function saveUser(){
	var jllglxEdit = $('#exchangeRotationType').combobox("getValue");

	if(jllglxEdit == ""){
		$.messager.alert('温馨提示','交流轮岗类型不能为空！','warning');
		return false;
	}
	if(jllglxEdit != 0){
		if(!yanzheng()){
			return;
		}
	}
	
	var exchangeRotationIdEdit=$("#exchangeRotationIdEdit").val();
	if(exchangeRotationIdEdit!=null&&exchangeRotationIdEdit>0){
		$.messager.alert('温馨提示','编辑成功','info',function(){
			var frmObj=$("#addfm");
			frmObj.attr("action","${ctx}/basicInformation/ExchangeRotationAciton.a?edit");
			frmObj.submit();
  	  	});
	}else{
		$.messager.alert('温馨提示','保存成功','info',function(){
			var frmObj=$("#addfm");
			frmObj.attr("action","${ctx}/basicInformation/ExchangeRotationAciton.a?add");
			frmObj.submit();
  	  	});
	}
}

//验证交流轮岗类型
$('#exchangeRotationType').combobox({
	   onSelect: function(param){
// 		   var gjdq = $("input[type='hidden'][name='exRotInfoDto.exchangeRotationType']").val();
			var gjdq = param.value;
		   if(gjdq == 0){
			 //是否调动人事关系
			  	$('#perMobilization').combobox('setValue','');
		  		$('#perMobilization').combobox('disable');
		  	//开始年月
		  		$('#beginYearM').val('');
		  		$('#beginYearM').attr('disabled','disabled');
		  	//原单位名称	
		  		$('#beforeCompany').textbox('setValue','');
		  		$('#beforeCompany').textbox('disable');
		  	//交流轮岗单位名称
		  		$('#nowCompany').textbox('setValue','');
		  		$('#nowCompany').textbox('disable');
		   }
		   if(gjdq!=0){
			 //是否调动人事关系
			  	$('#perMobilization').combobox('setValue','1');
		  		$('#perMobilization').combobox('enable');
		  		$('#beginYearM').removeAttr('disabled');
		  		$('#beforeCompany').textbox('enable');
		  		$('#nowCompany').textbox('enable');	
		   }
		}
	});

	//为easy-ui准备缓存时间加载
 	setTimeout(function(){
		var jllglx = $("input[type='hidden'][name='exRotInfoDto.exchangeRotationType']").val();
	    if(jllglx==0){
	    }
	  },2000)

//搜索
function doSearch(){
    $('#search').dialog('open');
}

function addTab(title, url){
	if ($('#tb').tabs('exists', title)){
		$('#tb').tabs('select', title);
	} else {
		var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
		$('#tb').tabs('add',{
			title:title,
			content:content,
			closable:true
		});
	}
}
//关闭所有的tab  
    function closeAll(){  
        var tiles = new Array();  
          var tabs = $('#tb').tabs('tabs');      
          var len =  tabs.length;           
          if(len>0){  
            for(var j=0;j<len;j++){  
                var a = tabs[j].panel('options').title;               
                tiles.push(a);  
            }  
            for(var i=1;i<tiles.length;i++){               
                $('#tb').tabs('close', tiles[i]);  
            }
          }
    } 
//关闭当前的tab 
	function removePanel(){
		var tab = $('#tb').tabs('getSelected');
		    if (tab){
			var index = $('#tb').tabs('getTabIndex', tab);
			$('#tb').tabs('close', index);
			}
		}
		
    /**
	    *报送
		*/
		function submitInfo(){
	    		 
					$.ajax({
						url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
						data:{},
						dataType : "json",
						success:function(data){
							    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
							    	$.ajax({
										url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?submitted&time='+new Date().getTime(),
										data:{},
										success:function(data){
											$.messager.alert('温馨提示','报送成功','info',function(){
												window.parent.location.reload();
								    	  	});
										},
										error:function(data){
											$.messager.alert('温馨提示','报送失败,请检查各项是否都已填写！','warning');
										}
										
									});   
								}else{
									$.messager.alert('温馨提示','信息已报送，不可重复报送！','warning');
								}  
						},
						error:function(XMLHttpRequest, textStatus, errorThrown){
							var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
							sessionTimeout(sessionstatus);
						}
						
					})
	    		 
	    	 }
	    /**
	    *取消报送
		*/
		function cancelSubmit(){
			 
			$.ajax({
				url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
				data:{},
				dataType : "json",
				success:function(data){
					    if(data.tbJzgxxsh.shzt==02){
					    	$.ajax({
								url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?cancelSubmit&time='+new Date().getTime(),
								data:{},
								success:function(data){
									$.messager.alert('温馨提示','取消成功','info',function(){
						            	
						    	  	});
								},
								error:function(data){
								
								}
								
							});  
						}else{
							$.messager.alert('温馨提示','信息审核中(审核通过)或未报送，不可取消保送！','warning');
						}  
				},
				error:function(XMLHttpRequest, textStatus, errorThrown){
					var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
					sessionTimeout(sessionstatus);
				}
				
			})
				
	    	 }

//表格分页	
/* var pager = $('#dg').datagrid('getPager');    // get the pager of datagrid
	pager.pagination({
		showPageList:false,
		buttons:[{
			iconCls:'icon-search',
			handler:function(){
				alert('search');
			}
		},{
			iconCls:'icon-add',
			handler:function(){
				alert('add');
			}
		},{
			iconCls:'icon-edit',
			handler:function(){
				alert('edit');
			}
		}],
		onBeforeRefresh:function(){
			alert('before refresh');
			return true;
		}
	}); */	
</script>
</body>
</html>
