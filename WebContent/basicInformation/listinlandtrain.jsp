<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>北京市教师管理服务平台</title>
<link rel="stylesheet" type="text/css" href="../css/newcss.css">
<link rel="stylesheet" type="text/css"
	href="../easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
<script type="text/javascript" src="../easyui/jquery.min.js"></script>
<script src="../js/echarts/build/dist/echarts-all.js"></script>
<script src="../js/echarts/build/dist/echarts.js"></script>
<script type="text/javascript" src="../easyui/easyloader.js"></script>
<script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="../basicInformation/ValidateUtils.js"></script>
<script type="text/javascript"
	src="../easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${ctx}/js/refresh.js"></script>
<script type="text/javascript" src="../My97DatePicker/WdatePicker.js"></script>
<link rel="stylesheet" type="text/css" href="../My97DatePicker/skin/WdatePicker.css">
<script type="text/javascript">
var myDate = new Date();
var defaultYear = myDate.getFullYear() - 1;///业务上的默认年
function setDefaultYear(){
	$("#defaultYear").val(defaultYear);
}
</script>
</head>
<body>
	
	<div id="anchor100">
		<div data-options="region:'center',border:false,collapsible:true"
			style="margin-bottom: 20px;">
<%-- 		<c:if test="${userType==1}">	
			<div id="toolbarGN">
				<a href="#" class="easyui-linkbutton" iconCls="icon-add"
					plain="true" onclick="newUserGN()">新 增</a> 
				<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true"
					onclick="editUser()">编 辑</a> 
				<a href="#" class="easyui-linkbutton"
					iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a>
			</div>
		</c:if>	 --%>
			<form action="${ctx}/basicInformation/InlandTrainAction.a" id="formInland" method="post">
			<input type="hidden" value="${tbJzgxxsh.shzt}" id="xxshzt"/>
			<input type="hidden" id="defaultYear"/>
			<table id="dg" class="easyui-datagrid" title="国内培训"
				iconCls="icon-dingdan"
				data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,collapsible:true,toolbar:'#toolbarGN',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
<!-- pagination:true, -->
				<thead>
					<tr>
						<th field="ck" checkbox="true"></th>
						<th data-options="field:'id',width:100">培训年度</th>
						<th data-options="field:'shesdxncha',width:100">培训类别</th>
						<th data-options="field:'shefnch',width:100">培训项目名称</th>
						<th data-options="field:'shedfdnha',width:100">培训机构名称</th>
						<th data-options="field:'sncfdha',width:100">培训方式</th>
						<th data-options="field:'ncsha',width:100">培训获得学时</th>
						<th data-options="field:'schca',width:100">培训获得学分</th>
						<th data-options="field:'ccchong',width:100">创建时间</th>
					</tr>
				</thead>
				<tbody>
					<c:if test="${not empty pageObj.pageElements }">
						<c:forEach items="${pageObj.pageElements}" var="item" varStatus="status">
							<tr>
								<td>${item.gnpxId}</td>
								<td>${item.pxnd }</td>
								<td><app:dictname dictid="JSXX_PXLB${item.pxlb}" />
								</td>
								<td>${item.pxxmmc }</td>
								<td>${item.pxjgmc }</td>
								<td><app:dictname dictid="JSXX_PXFS${item.pxfs}" />
								</td>
								<td>${item.pxhdxs }</td>
								<td>${item.pxhdxf }</td>
								<td>${item.cjsj }</td> 
							</tr>
						</c:forEach>
					</c:if>
					<c:if test="${empty pageObj.pageElements}">
						<tr>
							<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
						</tr>
					</c:if>
				</tbody>
			</table>
		
			
			<!-- 分页 -->
			<div class="page tar mb15">
				<input name="pageNo" value="${pageNo }" type="hidden" />
				<c:if test="${not empty pageObj.pageElements}">
					<jsp:include page="../common/pager-nest.jsp">
						<jsp:param name="toPage" value="1" />
						<jsp:param name="showCount" value="1" />
						<jsp:param name="action" value="doSearch" />
					</jsp:include>
				</c:if>
			</div>
		</form>
			<!-- 新增 -->
			<div id="dlgGN" class="easyui-dialog" title="新增国内培训"
				style="width: 700px; height: 300px; overflow: auto; padding: 10px 20px"
				closed="true" buttons="#dlg-buttons" data-options="modal:true">
				<div class="right_table">

					<div id="table-content">
						<form id="fm" method="post" novalidate>
							<table class="fitem">

										<tbody>
											<tr>
												<td>培训年度：</td>
												<td>
													<input id="pxndid" name="pxnd" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy',maxDate:'#F{$dp.$D(\'defaultYear\')}'})"/>
												</td>	
												
												<td>培训类别：</td>
												<td><select class="easyui-combobox" 
													name="pxlb" id="pxlbid">
														<app:dictselect dictType="JSXX_PXLB" selectValue=""/>
													</select></td>
											</tr>
											<tr>
												<td>培训项目名称：</td>
												<td><input type="text" class="easyui-textbox"
													 name="pxxmmc" id="pxxmmcid"></td>
												<td>培训机构名称：</td>
												<td><input type="text" class="easyui-textbox"
													 name="pxjgmc" id="pxjgmcid"></td>
											</tr>
											<tr>
												<td>培训方式：</td>
												<td><select class="easyui-combobox" 
													name="pxfs" id="pxfsid">
														<app:dictselect dictType="JSXX_PXFS" />
												</select></td>
												<td>培训获得学时：</td>
												<td><input type="text" max="99999"  min="0"  precision="0"  class="easyui-numberbox"
													 name="pxhdxs" id="pxhdxsInteger"><span
													id="pxhdxTip" style="color: red"></span></td>
											</tr>
											<tr>
												<td>培训获得学分：</td>
												<td><input type="text" max="99999"  min="0"  precision="0"  class="easyui-numberbox"
													 name="pxhdxf" id="pxhdxfInteger"><span
													id="pxhdxfTip" style="color: red"></span></td>
											</tr>
										</tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
			
			<!-- submit -->
			<div id="dlg-buttons">
				<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()">保存</a> 
				<a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel"
					onclick="javascript:$('#dlgGN').dialog('close')">取消</a>
			</div> 
			<!-- 编辑 -->
			<div id="dlgGN-BJ" class="easyui-dialog" title="编辑国内培训"
				style="width: 700px; height: 300px; overflow: auto; padding: 10px 20px"
				closed="true" buttons="#dlg-buttons" data-options="modal:true">
				<div class="right_table">
					<div id="table-content">
						<form id="fm" method="post" novalidate>
						<input type="hidden"  name="gnpxIdEdit" id="gnpxIdEdit" />
							<table class="fitem clean">
										<tbody>
											<tr>
												<td>培训年度：</td>
												<td>
												<input id="pxndEdit" name="pxnd" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy',maxDate:'#F{$dp.$D(\'defaultYear\')}'})"/>
												</td>
												<td>培训类别：</td>
												<td><select class="easyui-combobox" required="true"
													name="pxlb" id="pxlbEdit">
														<app:dictselect dictType="JSXX_PXLB" />
												</select></td>
											</tr>
											<tr>
												<td>培训项目名称：</td>
												<td><input type="text" class="easyui-textbox"
													required="true" name="pxxmmc" id="pxxmmcEdit"></td>
												<td>培训机构名称：</td>
												<td><input type="text" class="easyui-textbox"
													required="true" name="pxjgmc" id="pxjgmcEdit"></td>
											</tr>
											<tr>
												<td>培训方式：</td>
												<td><select class="easyui-combobox" required="true"
													name="pxfs" id="pxfsEdit">
														<app:dictselect dictType="JSXX_PXFS" />
												</select></td>
												<td>培训获得学时：</td>
												<td><input type="text" class="easyui-numberbox"
													required="true" name="pxhdxs" id="pxhdxsEdit"  max="99999"  min="0"  precision="0"><span
													id="pxhdxsTip" style="color: red"></span></td>
											</tr>
											<tr>
												<td>培训获得学分：</td>
												<td><input type="text" class="easyui-numberbox"
													required="true" name="pxhdxf" id="pxhdxfEdit" max="99999"  min="0"  precision="0" ><span
													id="pxhdxfTips" style="color: red"></span></td>
											</tr>
										</tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
			
			<!-- submit -->
			<div id="dlg-buttons">
				<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="etitInlang()">保存</a> 
				<a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel"
					onclick="javascript:$('#dlgGN-BJ').dialog('close')">取消</a>
			</div> 
		</div>
	</div>
	<div id="tips" class="easyui-dialog" title="温馨提示"
						style="padding: 10px 20px; width: 200px; height: 100px; line-height: 40px; text-align: center; overflow: auto; font-size: 20px;"
						closed="true" data-options="modal:true">请选择一条记录</div>
	<c:if test="${SESSION_ISPUB == '1'}">
    	<div id="btn-wrap">
        <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="submitInfo()">报送</a>
        <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="cancelSubmit()">取消</a><span style="color:red">各项信息填写完整后，才能报送。报送后，不能再次修改教师信息。只有审核不通过的教师信息，才能再次编辑报送。</span>
    </div>
    </c:if>
</body>
</html>

<script type="text/javascript">

$('#pxlbid').combobox({ editable:false }); //下拉框禁止输入
$('#pxfsid').combobox({ editable:false });
$('#pxlbEdit').combobox({ editable:false });
$('#pxfsEdit').combobox({ editable:false });
//页面为空显示提示
$('#dg').datagrid({
	onLoadSuccess:function(data){
		if(data.rows[0].id==undefined){
			$('#datagrid-row-r2-2-0').html(data.rows[0].ck);
			$('.datagrid-btable').css({'text-align':'center','width':'100%','padding':'30px 0'});
		}
	}
});
var url;
function save_user(dlgNmae){
    $(dlgNmae).dialog('open');
    $('#fm').form('clear');
    setDefaultYear();
    $("#pxndid").val(defaultYear);
}
//新增
function newUserGN(){
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
    save_user('#dlgGN');
			    }else{
					$.messager.alert('温馨提示','信息审核中或已通过审核，不可添加','warning');
				} 
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
		
	})
}


//编辑--回显
function editUser(){
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
    var selected=$("input[type='checkbox'][name='ck']:checked");
    
    if(selected.length<=0){
		 //save_user('#tipsInland');
		 $.messager.alert('温馨提示','请选择一条记录','warning');
		return ;
	}else if(selected.length>1){
		//save_user('#tipsInlands');
		$.messager.alert('温馨提示','只能编辑一条记录','warning');
		return ;
	}else{	
		var gnpxId=selected.val();
		$.ajax({
		    url:"${ctx}/basicInformation/InlandTrainAction.a?toUpdateInlandTrain&time="+new Date().getTime(),    //请求的url地址
		    dataType:"json",   //返回格式为json
		    //async:true,//请求是否异步，默认为异步，这也是ajax重要特性
		    data:{"gnpxId":gnpxId},    //参数值
		    type:"post",   //请求方式
		    success:function(data){
		    	console.log(data); 
		        $("#gnpxIdEdit").val(data.gnpxxx.gnpxId);
		       // $("#pxndEdit").datebox('setValue',data.gnpxxx.pxnd);
		        $("#pxndEdit").val(data.gnpxxx.pxnd);
		        $("#pxlbEdit").combobox('setValue',data.gnpxxx.pxlb);
		        $("#pxxmmcEdit").textbox('setValue',data.gnpxxx.pxxmmc);
		        $("#pxjgmcEdit").textbox('setValue',data.gnpxxx.pxjgmc);
		      	$("#pxfsEdit").combobox('setValue',data.gnpxxx.pxfs); 
		        $("#pxhdxsEdit").numberbox('setValue',data.gnpxxx.pxhdxs);
		        $("#pxhdxfEdit").numberbox('setValue',data.gnpxxx.pxhdxf);
		    },
		    error:function(){
		        //请求出错处理
		    }
		});
    save_user('#dlgGN-BJ');
	}
			    }else{
					$.messager.alert('温馨提示','信息审核中或已通过审核，不可编辑','warning');
				} 
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
		
	})
}
//编辑--修改  保存功能
function etitInlang() {
	var gnpxIdEdit=$("input[type='hidden'][name='gnpxIdEdit']").val();
	
	//日期修改
	var pxndEdit=$("#pxndEdit").val();
	//文本框修改
	var pxlbEdit=$("input[type='hidden'][name='pxlb']:last").val();
	var pxxmmcEdit=$("#pxxmmcEdit").val();
	var pxjgmcEdit=$("#pxjgmcEdit").val();
	var pxfsEdit=$("input[type='hidden'][name='pxfs']:last").val();
	var pxhdxsEdit=$("#pxhdxsEdit").val();
	var pxhdxfEdit=$("#pxhdxfEdit").val();
	
	console.info("!!!!!!!!!!"+gnpxIdEdit);
	console.info("!!!!!!!!!!"+pxndEdit);
	console.info("!!!!!!!!!!"+pxlbEdit);
	console.info("!!!!!!!!!!"+pxxmmcEdit);
	console.info("!!!!!!!!!!"+pxjgmcEdit); 
	console.info("!!!!!!!!!!"+pxfsEdit); 
	console.info("!!!!!!!!!!"+pxhdxsEdit);
	console.info("!!!!!!!!!!"+pxhdxfEdit); 
	
	if(pxndEdit==""){
		$.messager.alert('温馨提示','请选择培训年度','warning');
		return;
	}
	if(pxxmmcEdit==""||pxjgmcEdit==""){
		$.messager.alert('温馨提示','培训项目/机构名称不为空!','warning');
		return;
	}
	if(pxhdxsEdit!=null){
		if(ValidateUtils.validIntegerss(pxhdxsEdit)==false){
			$('#pxhdxsTip').text("请输入正整数");
			return;
			}
	}
	if(pxhdxfEdit!=null){
		if(ValidateUtils.validIntegerss(pxhdxfEdit)==false){
			$('#pxhdxfTips').text("请输入正整数");
			return;
			}
	}
	window.location.href = "${ctx}/basicInformation/InlandTrainAction.a?updateInlandTrain&gnpxId="+gnpxIdEdit
							+"&pxnd="+pxndEdit+"&pxlb="+pxlbEdit
							+"&pxxmmc="+pxxmmcEdit+"&pxjgmc="+pxjgmcEdit
							+"&pxfs="+pxfsEdit+"&pxhdxs="+pxhdxsEdit+"&pxhdxf="+pxhdxfEdit;
		$.messager.alert('温馨提示','编辑成功','info',function(){
          	window.location.reload();
  	  	});
}

//删除
function removeUser(){
	var selected=$("input[type='checkbox'][name='ck']:checked");
	if(selected.length<=0){
		$.messager.alert('温馨提示','请选择一条记录','warning');
		return ;
	}
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
	$.messager.confirm('温馨提示', '请确认是否删除信息？', function(r){
		if(r){
			var frmObj=$("#formInland");
			frmObj.attr("action","${ctx}/basicInformation/InlandTrainAction.a?delete");
			frmObj.submit();
			$.messager.alert('温馨提示','删除成功','info',function(){
              	window.location.reload();
      	  	});
		} 
	});
			    }else{
					$.messager.alert('温馨提示','信息审核中或已通过审核，不可删除','warning');
				} 
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
		
	})
}

//关闭dialog
function save_close(dlgNmae){
    $(dlgNmae).dialog('close');
}

//保存
function saveUser(){
	
	var pxndid =$('#pxndid').val();
	var pxlbid=$("input[type='hidden'][name='pxlb']").val();
	var pxxmmcid =$('#pxxmmcid').val();
	var pxjgmcid =$('#pxjgmcid').val();
	var pxfsid=$("input[type='hidden'][name='pxfs']").val();
	var pxhdxsInteger =$('#pxhdxsInteger').val();
	var pxhdxfInteger =$('#pxhdxfInteger').val();
	
	if(pxndid==""){
		$.messager.alert('温馨提示','请选择培训年度!','warning');
		return;
	}
	if(pxlbid==""){
		$.messager.alert('温馨提示','请选择培训类别!','warning');
		return;
	}
	if(pxxmmcid==""||pxjgmcid==""){
		$.messager.alert('温馨提示','培训项目/机构名称不为空!','warning');
		return;
	}
	if(pxfsid==""){
		$.messager.alert('温馨提示','请选择培训方式!','warning');
		return;
	}
	if(pxhdxsInteger==""){
		$.messager.alert('温馨提示','请填写培训获得学时且为正整数!','warning');
		return;
	}
	if(pxhdxsInteger!=""){
		if(ValidateUtils.validIntegerss(pxhdxsInteger)==false){
			$('#pxhdxTip').text("请输入正整数");
			return;
		}
	}
	if(pxhdxfInteger==""){
		$.messager.alert('温馨提示','请填写培训获得学分且为正整数!','warning');
		return;
	}
	if(pxhdxfInteger!=""){
		if(ValidateUtils.validIntegerss(pxhdxfInteger)==false){
			$('#pxhdxfTip').text("请输入正整数");
			return;
		}
	}
	
	$.messager.confirm('温馨提示', '确认保存？', function(r){
		 if(r){
    $('#fm').form('submit',{
        url: "${ctx}/basicInformation/InlandTrainAction.a?addInlandTrain",
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function (){
        	
        	save_close('#dlgGN');
        	$.messager.alert('温馨提示','保存成功','info',function(){
              	window.location.reload();
      	  	});
        	//window.location.reload();
        }
        
    });
    
    
		 }
	 });
}
//搜索
function doSearch(){
    $('#search').dialog('open');
}

function addTab(title, url){
	if ($('#tb').tabs('exists', title)){
		$('#tb').tabs('select', title);
	} else {
		var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
		$('#tb').tabs('add',{
			title:title,
			content:content,
			closable:true
		});
	}
}
//关闭所有的tab  
    function closeAll(){  
        var tiles = new Array();  
          var tabs = $('#tb').tabs('tabs');      
          var len =  tabs.length;           
          if(len>0){  
            for(var j=0;j<len;j++){  
                var a = tabs[j].panel('options').title;               
                tiles.push(a);  
            }  
            for(var i=1;i<tiles.length;i++){               
                $('#tb').tabs('close', tiles[i]);  
            }
          }
    } 
//关闭当前的tab 
	function removePanel(){
		var tab = $('#tb').tabs('getSelected');
		    if (tab){
			var index = $('#tb').tabs('getTabIndex', tab);
			$('#tb').tabs('close', index);
			}
		}	

		
		
    /**
	    *报送
		*/
		function submitInfo(){
	    		 
					$.ajax({
						url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
						data:{},
						dataType : "json",
						success:function(data){
							    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
							    	$.ajax({
										url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?submitted&time='+new Date().getTime(),
										data:{},
										success:function(data){
											$.messager.alert('温馨提示','报送成功','info',function(){
												window.parent.location.reload();
								    	  	});
										},
										error:function(data){
											$.messager.alert('温馨提示','报送失败,请检查各项是否都已填写！','warning');
										}
										
									});   
								}else{
									$.messager.alert('温馨提示','信息已报送，不可重复报送！','warning');
								}  
						},
						error:function(XMLHttpRequest, textStatus, errorThrown){
							var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
							sessionTimeout(sessionstatus);
						}
						
					})
	    		 
	    	 }
	    /**
	    *取消报送
		*/
		function cancelSubmit(){
			 
			$.ajax({
				url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
				data:{},
				dataType : "json",
				success:function(data){
					    if(data.tbJzgxxsh.shzt==02){
					    	$.ajax({
								url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?cancelSubmit&time='+new Date().getTime(),
								data:{},
								success:function(data){
									$.messager.alert('温馨提示','取消成功','info',function(){
						            	
						    	  	});
								},
								error:function(data){
								
								}
								
							});  
						}else{
							$.messager.alert('温馨提示','信息审核中(审核通过)或未报送，不可取消保送！','warning');
						}  
				},
				error:function(XMLHttpRequest, textStatus, errorThrown){
					var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
					sessionTimeout(sessionstatus);
				}
				
			})
				
	    	 }

</script>

