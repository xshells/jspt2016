<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="${ctx}/css/newcss.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/easyui/themes/xyy.css">
    <script type="text/javascript" src="${ctx}/easyui/jquery.min.js"></script>
    <script src="${ctx}/js/echarts/build/dist/echarts-all.js"></script>
    <script src="${ctx}/js/echarts/build/dist/echarts.js"></script>
    <script type="text/javascript" src="${ctx}/easyui/easyloader.js"></script>
    <script type="text/javascript" src="${ctx}/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="${ctx}/easyui/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="${ctx}/js/refresh.js"></script>
	<script type="text/javascript" src="../My97DatePicker/WdatePicker.js"></script>
    <link rel="stylesheet" type="text/css" href="../My97DatePicker/skin/WdatePicker.css">
</head>
<body>
         <%-- <form action="${ctx}/basicInformation/TeacherMoralityAction.a" id="fm" method="post" novalidate> --%>
		<div class="content" id="userList">
 			<div class="easyui-panel">
 				<div id="anchor70">
 				 <form action="${ctx}/basicInformation/TeacherMoralityAction.a" id="fm" method="post" novalidate>
                            <div data-options="region:'center',border:false,collapsible:true" style="margin-bottom:20px;">
                                <div id="toolbarSDOne">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUserSD()">新 增</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">编 辑</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a>
                                    
                                </div>
                                <table id="dg" class="easyui-datagrid" title="师德考核信息" iconCls="icon-dingdan"
                                    data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,collapsible:true,toolbar:'#toolbarSDOne',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
                                <input type="hidden" value="${tbJzgxxsh.shzt}" id="xxshzt"/>
                                <input type="hidden" name="xxjgmc" id="xxjgmc" val="${xxjgmc}"/>
                                <thead>
                                    <tr>
                                        <th field="ck" checkbox="true"></th>
                                        <th data-options="field:'id',width:100">师德考核时间</th>
                                        <th data-options="field:'shencha',width:100">师德考核结论</th>
                                        <th data-options="field:'chachong',width:100">师德考核单位名称</th>
                                      <!--   <th data-options="field:'name',width:100">处分原因</th>
                                        <th data-options="field:'named',width:100">处分发生日期</th>
 -->                                        <th data-options="field:'sex',width:100">创建时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                <c:if test="${empty pageObj.pageElements}">
									<tr>
										<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
									</tr>
								</c:if>
								<div class="page tar mb15">
									<c:if test="${not empty pageObj.pageElements}">
									<c:forEach items="${pageObj.pageElements}" var="item"
										varStatus="status">
										<tr>
										<%-- <td field="ck" checkbox="true">k${item.khid}j${item.jsid }</td> --%>
										<td field="ck" checkbox="true">${item.khid}</td>
											<%-- <td field="ck" checkbox="true">k${item.khid}j${item.jsid }</td> --%>
											<input type="hidden"  name="jsid" id="jsid" value="${item.jsid }">
											<input type="hidden"  name="khid" id="khid" value="${item.khid}">
											<input type="hidden"  name="cfid" id="cfid" value="${item.cfid}">
											<td>${item.khny}</td>
											<td><appadd:dictnameadd dictid="JSXX_SDKHJL" dictbm="${item.khjl}" schoolType="${ssxd}"/></td>
											<td>${item.sdkhdwmc}</td>
											<td>${item.cjsj}</td>
										</tr>
									</c:forEach>
									
									</c:if>
								</div>
                                </tbody>
                                </table>
                            </div>
                        <div class="page tar mb15">
							<input name="pageNo" value="${pageNo }" type="hidden" />
							<c:if test="${not empty pageObj.pageElements}">
								<jsp:include page="../common/pager-nest.jsp">
									<jsp:param name="toPage" value="1" />
									<jsp:param name="showCount" value="1" />
									<jsp:param name="action" value="doSearch" />
								</jsp:include>
							</c:if>
						</div>
						</form>
						<div id="dlgSD" class="easyui-dialog" title="新增师德信息" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        <div id="table-content">
                                        <form id="addform" method="post" novalidate>
                                         <input type="hidden"  name="khidd" id="khidd">
                                        <table class="fitem clean">
                                            <tbody>
                                               
                                                <tr>
                                                    <td>师德考核时间：</td>
                                                    <td>
                                                         <input id="khny" name="khny" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM'})" value="${dto.rxny}"/>
                                                    </td>
                                                    <td>师德考核结论：</td>
                                                    <td>
                                                        <select  class="easyui-combobox" required="true" name="khjl" id="khjl" >
                                                       			 <appadd:dictselectadd dictType="JSXX_SDKHJL" schoolType="${ssxd}"/>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>师德考核单位名称：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" required="true" name="sdkhdwmc" id="sdkhdwmc" value="${dto.sdkhdwmc}">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgSD').dialog('close')">取消</a>
                                </div>
                </div>
 			</div>

		</div>
		<iframe 
			src="${ctx}/basicInformation/PunishmentInfoAction.a"
			frameborder="0" width="100%" height="500"  marginwidth="0" scrolling="no"></iframe>
	<div id="tips" class="easyui-dialog" title="温馨提示" style="padding:10px 20px;width:200px;height:100px;line-height:40px;text-align:center;overflow:auto;font-size:20px;" closed="true" data-options="modal:true">请选择一条记录</div>

		<!-- 新增 -->
		<!-- <div class="content" style="display: none;" id="addUser">
			<div class="modelbox modelbox3">
				<div class="rowbox mb10 wper15">
					<input type="submit" value="保存" class="btnyellow fl"
						onclick="return saveUser();" /> <input type="button" value="返回"
						class="btnyellow fr" onclick="return hideAdd();" />
				</div>
			</div>
		</div> -->
  <!-- </form> -->
  <c:if test="${SESSION_ISPUB == '1'}">
    	<div id="btn-wrap">
        <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="submitInfo()">报送</a>
        <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="cancelSubmit()">取消</a><span style="color:red">各项信息填写完整后，才能报送。报送后，不能再次修改教师信息。只有审核不通过的教师信息，才能再次编辑报送。</span>
    </div>
    </c:if>
<script type="text/javascript">
$('#khjl').combobox({ editable:false }); //下拉框禁止输入
$(function(){
	$('#tt').tabs({
		onSelect:function(title,index){
			if(index==0){
				$.ajax({
					url:'${ctx}/basicInformation/TeacherMoralityAction.a&time='+new Date().getTime(),
					success:function(data){
						console.log("success");
					},
					error:function(XMLHttpRequest, textStatus, errorThrown){
						var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
						sessionTimeout(sessionstatus);
					}
				});
			}
			if(index==1){
				$.ajax({
					url:'${ctx}/basicInformation/PunishmentInfoAction.a&time='+new Date().getTime(),
					success:function(){
						console.log("success");
					},
					error:function(XMLHttpRequest, textStatus, errorThrown){
						var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
						sessionTimeout(sessionstatus);
					}
				})
			}
		}
	})
	
	
})


var url;
function save_user(dlgNmae){
    $(dlgNmae).dialog('open');
    $('#fm').form('clear');
}
//新增
function newUserSD(){
	var dnny="${year}-${mouth}";
	$('#khjl').combobox({ editable:false }); //下拉框禁止输入
	//$('#sdkhdwmc').textbox({ editable:false }); //下拉框禁止输入
$.ajax({
			url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
			data:{},
			dataType : "json",
			success:function(data){
				    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22||data.tbJzgxxsh.shzt==02){
    save_user('#dlgSD');
    $("#khjl").combobox('setValue',2);
    var xxjgmc = $("#xxjgmc").attr('val');
	$("#sdkhdwmc").textbox('setValue',xxjgmc);
	$("#khny").val(dnny);
	/* $("#khny").textbox('setValue','');
    var d = new Date()
	var vYear = d.getFullYear();
	var vMon = d.getMonth() + 1;
	var vDay = d.getDate();
    $("#khny").val(vYear+"-"+vMon); */
    }else{
		$.messager.alert('温馨提示','信息审核中或已通过审核，不可添加','warning');
	} 
},
error:function(XMLHttpRequest, textStatus, errorThrown){
	var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
	sessionTimeout(sessionstatus);
}

})
}
function newUserCF(){
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22||data.tbJzgxxsh.shzt==02){
     save_user('#dlgCF'); 
    }else{
		$.messager.alert('温馨提示','信息审核中或已通过审核，不可添加','warning');
	} 
},
error:function(XMLHttpRequest, textStatus, errorThrown){
	var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
	sessionTimeout(sessionstatus);
}

})
}
//编辑
function editUser(){
	$('#khjl').combobox({ editable:false }); //下拉框禁止输入
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22||data.tbJzgxxsh.shzt==02){
var selected=$("input[type='checkbox'][name='ck']:checked");
	
	if(selected.length<=0){
		$.messager.alert('温馨提示','请选择一条记录','warning');
		return ;
	}else if(selected.length>1){
		$.messager.alert('温馨提示','只能编辑一条记录','warning');
		return ;
	}
      var khid = selected.val();
	  var row = $('#dg').datagrid('getSelected');
      if (row){
          $('#dlgSD').dialog('open').dialog('setTitle','编辑用户');
          $('#fm').form('load',row);
          $.ajax({
	      	url : '${ctx}/basicInformation/TeacherMoralityAction.a?doQuery&khid='+khid+'&time='+new Date().getTime(),
	      	dataType: 'json',
	      	success:function(data){
	      		$("#khidd").val(data.dto.khid);
	      		$("#khny").val(data.dto.khny);
				$("#khjl").combobox('setValue',data.dto.khjl);//师德考核结论
				$("#sdkhdwmc").textbox('setValue',data.dto.sdkhdwmc); 
	      	}
	      });
      }
    }else{
		$.messager.alert('温馨提示','信息审核中或已通过审核，不可编辑','warning');
	} 
},
error:function(XMLHttpRequest, textStatus, errorThrown){
	var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
	sessionTimeout(sessionstatus);
}

})
}

/**
 *报送
	*/
	function submitInfo(){
 		 
				$.ajax({
					url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
					data:{},
					dataType : "json",
					success:function(data){
						    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
						    	$.ajax({
									url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?submitted&time='+new Date().getTime(),
									data:{},
									success:function(data){
										$.messager.alert('温馨提示','报送成功','info',function(){
											window.parent.location.reload();
							    	  	});
									},
									error:function(data){
										$.messager.alert('温馨提示','报送失败,请检查各项是否都已填写！','warning');
									}
									
								});   
							}else{
								$.messager.alert('温馨提示','信息已报送，不可重复报送！','warning');
							}  
					},
					error:function(XMLHttpRequest, textStatus, errorThrown){
						var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
						sessionTimeout(sessionstatus);
					}
					
				})
 		 
 	 }
 /**
 *取消报送
	*/
	function cancelSubmit(){
		 
		$.ajax({
			url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
			data:{},
			dataType : "json",
			success:function(data){
				    if(data.tbJzgxxsh.shzt==02){
				    	$.ajax({
							url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?cancelSubmit&time='+new Date().getTime(),
							data:{},
							success:function(data){
								$.messager.alert('温馨提示','取消成功','info',function(){
					            	
					    	  	});
							},
							error:function(data){
							
							}
							
						});  
					}else{
						$.messager.alert('温馨提示','信息审核中(审核通过)或未报送，不可取消保送！','warning');
					}  
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
			
		})
			
 	 }

function editUserCF(){
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22||data.tbJzgxxsh.shzt==02){
	var selected=$("input[type='checkbox'][name='ck']:checked");
		
		if(selected.length<=0){
			$.messager.alert('温馨提示','请选择一条记录','warning');
			return ;
		}else if(selected.length>1){
			$.messager.alert('温馨提示','只能编辑一条记录','warning');
			return ;
		}
	      var cfid = selected.val();
		  var row = $('#dg1').datagrid('getSelected');
	      if (row){
	          $('#dlgCF').dialog('open').dialog('setTitle','编辑用户');
	          $('#fm').form('load',row);
	          $.ajax({
		      	url : '${ctx}/basicInformation/PunishmentInfoAction.a?doQuery&cfid='+cfid+'&time='+new Date().getTime(),
		      	dataType: 'json',
		      	success:function(data){
					$("#cflb").combobox('setValue',data.dto.cflb);//处分类别
					$("#cfyy").combobox('setValue',data.dto.cfyy);//处分原因
					$("#ccfsrq").datebox('setValue',data.dto.ccfsrq);
		      	}
		      });
	      }
    }else{
		$.messager.alert('温馨提示','信息审核中或已通过审核，不可编辑','warning');
	} 
},
error:function(XMLHttpRequest, textStatus, errorThrown){
	var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
	sessionTimeout(sessionstatus);
}

})  
	}
//关闭
function save_close(dlgNmae){
	$(dlgNmae).dialog('close');
}
//删除
function removeUser(){
	var selected=$("input[type='checkbox'][name='ck']:checked");
	
	if(selected.length<=0){
		return $.messager.alert('温馨提示','请选择一条记录','warning');
	}
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22||data.tbJzgxxsh.shzt==02){
 	 var strSel = '';
     $("input[name='ck']:checked").each(function(index, element) {
          strSel += $(this).val()+",";
      }); 
      var arrCk = strSel.substring(0,strSel.length-1); 
	$.messager.confirm('温馨提示', '请确认是否删除信息？', function(r){
		if (r){
			$.messager.alert('温馨提示','删除成功','warning',function(){
				window.location.href ="${ctx}/basicInformation/TeacherMoralityAction.a?delTeacherMorality&ck="+arrCk;
			  	});
		}
	});
    }else{
		$.messager.alert('温馨提示','信息审核中或已通过审核，不可删除','warning');
	} 
},
error:function(XMLHttpRequest, textStatus, errorThrown){
	var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
	sessionTimeout(sessionstatus);
}

}) 
	
}
function removeUserCF(){
	var selected=$("input[type='checkbox'][name='ck']:checked");
	
	if(selected.length<=0){
		return $.messager.alert('温馨提示','请选择一条记录','warning');
	}
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22||data.tbJzgxxsh.shzt==02){
 	 var strSel = '';
     $("input[name='ck']:checked").each(function(index, element) {
          strSel += $(this).val()+",";
      }); 
      var arrCk = strSel.substring(0,strSel.length-1); 
	$.messager.confirm('温馨提示', '请确认是否删除信息？', function(r){
		if (r){
			$.messager.alert('温馨提示','删除成功','info',function(){
				window.location.href ="${ctx}/basicInformation/PunishmentInfoAction.a?delPunishmentInfo&ck="+arrCk;
			  	});
		}
	});
    }else{
		$.messager.alert('温馨提示','信息审核中或已通过审核，不可删除','warning');
	} 
},
error:function(XMLHttpRequest, textStatus, errorThrown){
	var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
	sessionTimeout(sessionstatus);
}

})
}
//保存
function saveUser(){
	var id="";
	var ckData = $('#dg').datagrid('getSelected');
/* 	var id = ckData.ck.match(/\d/g); */
if(ckData){
	id = ckData.ck;	
}else{
	id="";
}
    var cfid=$("input[type='hidden'][name='cfid']").val();
   // var khny=$("input[name='khny']").val();
    var khny=$("#khny").val();
    if(!khny){
		$.messager.alert('温馨提示','师德考核时间不能为空！','warning');
		return;
	}
    var khjl=$("input[type='hidden'][name='khjl']").val();
    var sdkhdwmc=$("input[type='hidden'][name='sdkhdwmc']").val();
	var cflb=$("input[type='hidden'][name='cflb']").val();
	var cfyy=$("input[type='hidden'][name='cfyy']").val();
	var ccfsrq=$("input[name='ccfsrq']").val();
	if(!sdkhdwmc){
		$.messager.alert('温馨提示','师德考核单位名称不能为空！','warning');
		return;
	}
	$.messager.confirm('温馨提示', '请确认是否保存信息？', function(r){
		if (r){
			if(id&&id!=""){
				$('#addform').form('submit',{
			       // url: "${ctx}/basicInformation/TeacherMoralityAction.a?editTeacherMorality&khid="+id+"&cfid="+id+"&khny="+khny+"&khjl="+khjl+"&cflb="+cflb+"&cfyy="+cfyy+"&ccfsrq="+ccfsrq+"&cfid="+cfid+"&sdkhdwmc="+sdkhdwmc,
			        url: "${ctx}/basicInformation/TeacherMoralityAction.a?editTeacherMorality",
			        onSubmit: function(){
			            return $(this).form('validate');
			        },
			        success: function(){
			        	save_close('#dlgSD');     // close the dialog
			        	$.messager.alert('温馨提示','编辑成功','info',function(){
			    	      	window.location.reload();
			    		  	});
			                /* $.messager.show({
			                    title: 'Error',
			                }); */
			        }
			    }); 
				
			}else{
		     $('#addform').form('submit',{
		        //url: "${ctx}/basicInformation/TeacherMoralityAction.a?addTeacherMorality&khid="+id+"&cfid="+id+"&khny="+khny+"&khjl="+khjl+"&cflb="+cflb+"&cfyy="+cfyy+"&ccfsrq="+ccfsrq+"&sdkhdwmc="+sdkhdwmc,
		        url: "${ctx}/basicInformation/TeacherMoralityAction.a?addTeacherMorality",
		        onSubmit: function(){
		            return $(this).form('validate');
		        },
		        success: function(){
		        	save_close('#dlgSD');     // close the dialog
		        	$.messager.alert('温馨提示','保存成功','info',function(){
		    	      	window.location.reload();
		    		  	}); 
		                /* $.messager.show({
		                    title: 'Error',
		                }); */
		        }
		    });  
		     
		}  
		}
	});
	
    
}
function saveUserCF(){
	var id="";
	var ckData = $('#cf').datagrid('getSelected');
/* 	var id = ckData.ck.match(/\d/g); */
if(ckData){
	id = ckData.ck;	
}else{
	id="";
}
    var cfid=$("input[type='hidden'][name='cfid']").val();
    var khny=$("input[name='khny']").val();
    var khjl=$("input[type='hidden'][name='khjl']").val();
    var sdkhdwmc=$("input[type='hidden'][name='sdkhdwmc']").val();
	var cflb=$("input[type='hidden'][name='cflb']").val();
	var cfyy=$("input[type='hidden'][name='cfyy']").val();
	var ccfsrq=$("input[name='ccfsrq']").val();
	if(id&&id!=""){
		
		$('#fm1').form('submit',{
	        url: "${ctx}/basicInformation/PunishmentInfoAction.a?editPunishmentInfo&khid="+id+"&cfid="+id+"&khny="+khny+"&khjl="+khjl+"&cflb="+cflb+"&cfyy="+cfyy+"&ccfsrq="+ccfsrq+"&cfid="+cfid+"&sdkhdwmc="+sdkhdwmc,
	        onSubmit: function(){
	            return $(this).form('validate');
	        },
	        success: function(){
	        	save_close('#dlgCF');     // close the dialog
	        	 window.location.reload();     // reload the user data
	                /* $.messager.show({
	                    title: 'Error',
	                }); */
	        }
	    }); 
	}else{
     $('#fm1').form('submit',{
        url: "${ctx}/basicInformation/PunishmentInfoAction.a?addPunishmentInfo&khid="+id+"&cfid="+id+"&khny="+khny+"&khjl="+khjl+"&cflb="+cflb+"&cfyy="+cfyy+"&ccfsrq="+ccfsrq+"&sdkhdwmc="+sdkhdwmc,
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(){
        	save_close('#dlgCF');     // close the dialog
        	 window.location.reload();     // reload the user data
                /* $.messager.show({
                    title: 'Error',
                }); */
        }
    });  
     
}  
    
}
//提示信息
function tips(dlgNmae,msg){
	$(dlgNmae).html(msg).dialog('open');
}
$('#dg').datagrid({
	onLoadSuccess:function(data){
		if(data.rows[0].id==undefined){  //id    是field的值
			$('.datagrid-body td[field="id"]').html(data.rows[0].ck);
		}
	}
});
$('#dg').datagrid({
	onLoadSuccess:function(data){
		if(data.rows[0].id==undefined){
			$('#datagrid-row-r3-2-0').html(data.rows[0].ck);
			$('.datagrid-btable').css({'text-align':'center','width':'100%','padding':'30px 0'});
		}
	}
});

</script>
   
</body>
</html>
