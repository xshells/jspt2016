<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>北京市教师管理服务平台</title>
<link rel="stylesheet" type="text/css" href="../css/newcss.css">
<link rel="stylesheet" type="text/css"
	href="../easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
<script type="text/javascript" src="../easyui/jquery.min.js"></script>
<script src="../js/echarts/build/dist/echarts-all.js"></script>
<script src="../js/echarts/build/dist/echarts.js"></script>
<script type="text/javascript" src="../easyui/easyloader.js"></script>
<script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="../basicInformation/ValidateUtils.js"></script>
<script type="text/javascript"
	src="../easyui/locale/easyui-lang-zh_CN.js"></script>


</head>
<body>

	<div id="anchor14">
		<div data-options="region:'center',border:false,collapsible:true"
			style="margin-bottom: 20px; width: 100%" class="easyui-panel"
			title="联系方式" iconCls="icon-dingdan">
			<div id="toolbarxd" style="border-bottom: 1px dotted #ccc">
				<a href="#" class="easyui-linkbutton" iconCls="icon-save"
					plain="true" onclick="saveContactWay()">保 存</a>
			</div>
			<form action="${ctx}/basicInformation/ContactWayAction.a?addContactWay"
				method="post" id="formcontactWay"  novalidate>
				<input type="hidden" name="jsid" value="${contactWay.tbBizJzgjbxx.jsid}" id="jsidEdit" />
				<input type="hidden" value="${tbJzgxxsh.shzt}" id="xxshzt"/>
					 <input
					type="hidden" name="lxfsid" value="${contactWay.lxfsId}"
					id="lxfsIdEdit" />
				<table id="dg" class="fitem"
					data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,collapsible:true,toolbar:'#toolbar',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
					<tbody>
						<tr>
							<td>手机：</td>
							<td><input type="text" class="easyui-textbox" id="sjEdit"
								required="true" name="sj" value="${contactWay.sj}"></td>
						</tr>
						<tr>
							<td>Email：</td>
							<td><input type="text" class="easyui-textbox" id="emailEdit"
								required="true" name="email" value="${contactWay.email}"></td>
						</tr>
					</tbody>
				</table>
			</form>
	</div>
	</div>
	<div id="tips" class="easyui-dialog" title="温馨提示"
		style="padding: 10px 20px; width: 200px; height: 100px; line-height: 40px; text-align: center; overflow: auto; font-size: 20px;"
		closed="true" data-options="modal:true">请选择一条记录</div>
	<c:if test="${SESSION_ISPUB == '1'}">
    	<div id="btn-wrap">
        <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="submitInfo()">报送</a>
        <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="cancelSubmit()">取消</a><span style="color:red">各项信息填写完整后，才能报送。报送后，不能再次修改教师信息。只有审核不通过的教师信息，才能再次编辑报送。</span>
    </div>
    </c:if> 

</body>
</html>

<script type="text/javascript">
    

	function saveContactWay(){
		$.ajax({
			url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
			data:{},
			dataType : "json",
			success:function(data){
				    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
		var lxfsIdEdit =$('#lxfsIdEdit').val();
		var sjEdit =$('#sjEdit').val();
		var emailEdit =$('#emailEdit').val();
		if(sjEdit=="" || emailEdit==""){
			$.messager.alert('温馨提示','请填写手机号及电子邮箱！','warning');
			return ;
		} 
		if(sjEdit!=""){
			if(ValidateUtils.validCellPhoneNumber(sjEdit.trim())==false){
				$.messager.alert('温馨提示','请填写正确的手机号！','warning');
				return;
			}
		}
		if(emailEdit!=""){
			if(ValidateUtils.validEmailAddress(emailEdit.trim())==false){
				$.messager.alert('温馨提示','请填写正确的电子邮箱','warning');
				return;
			}
		}
		if( lxfsIdEdit == ""){
			$.messager.alert('温馨提示','保存成功','info',function(){
			$('#formcontactWay').submit();
			});
		}
		if( lxfsIdEdit!=0 && lxfsIdEdit != ""){
			$.messager.alert('温馨提示','保存成功','info',function(){
				window.location.href = "${ctx}/basicInformation/ContactWayAction.a?updateContactWay&lxfsId="+ lxfsIdEdit+"&sj="+sjEdit+"&email="+emailEdit;
			});
			
		}
	    }else{
			$.messager.alert('温馨提示','信息审核中或已通过审核，不可修改！','warning');
		} 
},
error:function(XMLHttpRequest, textStatus, errorThrown){
	var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
	sessionTimeout(sessionstatus);
}

})
	}

	var url;
	function save_user(dlgNmae) {
		$(dlgNmae).dialog('open');
		$('#fm').form('clear');
		url = 'save_user.php';
	}
	//新增
	function newXX() {
		save_user('#dlgGN');
	}
	
	//搜索
	function doSearch() {
		$('#search').dialog('open');
	}

	function addTab(title, url) {
		if ($('#tb').tabs('exists', title)) {
			$('#tb').tabs('select', title);
		} else {
			var content = '<iframe scrolling="auto" frameborder="0"  src="'
					+ url + '" style="width:100%;height:100%;"></iframe>';
			$('#tb').tabs('add', {
				title : title,
				content : content,
				closable : true
			});
		}
	}
	//关闭所有的tab  
	function closeAll() {
		var tiles = new Array();
		var tabs = $('#tb').tabs('tabs');
		var len = tabs.length;
		if (len > 0) {
			for (var j = 0; j < len; j++) {
				var a = tabs[j].panel('options').title;
				tiles.push(a);
			}
			for (var i = 1; i < tiles.length; i++) {
				$('#tb').tabs('close', tiles[i]);
			}
		}
	}
	//关闭当前的tab 
	function removePanel() {
		var tab = $('#tb').tabs('getSelected');
		if (tab) {
			var index = $('#tb').tabs('getTabIndex', tab);
			$('#tb').tabs('close', index);
		}
	}
    /**
	    *报送
		*/
		function submitInfo(){
					$.ajax({
						url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
						data:{},
						dataType : "json",
						success:function(data){
							    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
							    	$.ajax({
										url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?submitted&time='+new Date().getTime(),
										data:{},
										success:function(data){
											$.messager.alert('温馨提示','报送成功','info',function(){
												window.parent.location.reload();
								    	  	});
										},
										error:function(data){
											$.messager.alert('温馨提示','报送失败,请检查各项是否都已填写！','warning');
										}
										
									});   
								}else{
									$.messager.alert('温馨提示','信息已报送，不可重复报送！','warning');
								}  
						},
						error:function(XMLHttpRequest, textStatus, errorThrown){
							var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
							sessionTimeout(sessionstatus);
						}
						
					})
	    		 
	    	 }
	    /**
	    *取消报送
		*/
		function cancelSubmit(){
			 
			$.ajax({
				url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
				data:{},
				dataType : "json",
				success:function(data){
					    if(data.tbJzgxxsh.shzt==02){
					    	$.ajax({
								url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?cancelSubmit&time='+new Date().getTime(),
								data:{},
								success:function(data){
									$.messager.alert('温馨提示','取消成功','info',function(){
						            	
						    	  	});
								},
								error:function(data){
								
								}
								
							});  
						}else{
							$.messager.alert('温馨提示','信息审核中(审核通过)或未报送，不可取消保送！','warning');
						}  
				},
				error:function(XMLHttpRequest, textStatus, errorThrown){
					var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
					sessionTimeout(sessionstatus);
				}
				
			})
				
	    	 }


</script>
