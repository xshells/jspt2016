String.prototype.trim=function() {
    return this.replace(/(^\s*)|(\s*$)/g,'');
}
var ValidateUtils = {
			
			//电子邮箱
			validEmailAddress : function(str) {
				var exp = new RegExp("^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$");
				return exp.test(str);
			},
			
			/**
			 * 检测字符串是否为合法的中国（大陆）手机号码
			 */
			validCellPhoneNumber : function(str) {
				// 校验规则：数字1开头，位数为11位
				var exp = new RegExp("^0?(13|15|18|17)[0-9]{9}$");
				return exp.test(str);
			},
			validIntegerss : function(str){
				// 校验正整数
				//var exp = new RegExp("^[1-9]\d*$");
				var exp = new RegExp("^[0-9]*[1-9][0-9]*$");
				return exp.test(str);
			},
			validIdCard:function(str){
				//身份证
				var idcardLength = str.trim().length;
				if(idcardLength==15){
					return /^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$/.test(str);
				}
				if(idcardLength==18){
					return /^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/.test(str);
				}
				return false;
			}
	} 