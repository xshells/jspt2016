<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>北京市教师管理服务平台</title>
    <link rel="stylesheet" type="text/css" href="../css/newcss.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
    <script type="text/javascript" src="../easyui/jquery.min.js"></script>
    <script src="../js/echarts/build/dist/echarts-all.js"></script>
    <script src="../js/echarts/build/dist/echarts.js"></script>
    <script type="text/javascript" src="../easyui/easyloader.js"></script>
    <script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx}/js/refresh.js"></script>
 </head>   
<body>
<div id="anchor7">
                            <div data-options="region:'center',border:false,collapsible:true" style="margin-bottom:20px;">
                                <div id="toolbarJS">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUserJSZG()">新 增</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUserJSZG()">编 辑</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUserJSZG()">删 除</a>
                                </div>
                                <form id="fmjszg" method="post" novalidate>
                                <input type="hidden" value="${tbJzgxxsh.shzt}" id="xxshzt"/>
                                 <input type="hidden" value="${tbBizJzgjbxx.jsid}" name="jsid" id="jsid"/>
    							 <input type="hidden" value="${tbBizJzgjbxx.tbXxjgb.ssxd}" name="ssxd" id="ssxd"/>
    							 <input type="hidden" value="${tbBizJzgjbxx.tbXxjgb.ssxd}" name="shzt" id="shzt"/>
                                <table id="dg" class="easyui-datagrid" title="教师资格" iconCls="icon-dingdan"
                                    data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,collapsible:true,toolbar:'#toolbarJS',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
                                
                                <thead>
                                    <tr>
                                        <th field="ck" checkbox="true"></th>
                                        <th data-options="field:'id',width:100">教师资格证种类</th>
                                        <th data-options="field:'shencha',width:100">教师资格证号码</th>
                                        <th data-options="field:'chachong',width:100">任教学科</th>
                                        <th data-options="field:'name',width:100">证书颁发日期</th>
                                        <th data-options="field:'named',width:100">证书认定机构</th>
                                        <th data-options="field:'sex',width:100">创建时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test="${not empty pageObj.pageElements}">
										<c:forEach items="${pageObj.pageElements}" var="item"
											varStatus="status">
											<tr>
                                        		 <td>${item.jszgId}</td>
                                        		 <td><app:dictname dictid="JSXX_JSZGZZL${item.jszgzzl}"/></td>
                                        		 <td>${item.jszgzhm}</td>
                                        		 <td><app:dictname dictid="JSXX_RKXKLB${item.rjxk}"/></td>
                                        		 <td>${item.zsbfrq}</td>
                                        		 <td>${item.zsrdjg}</td>
                                        		 <td>${item.cjsj}</td>
                                    		</tr>
										</c:forEach>
									</c:if>
									<c:if test="${empty pageObj.pageElements}">
											<tr>
												<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
											</tr>
									</c:if>
                                </tbody>
                                </table>
                                         <div class="page tar mb15">
											<input name="pageNo" value="${pageNo }" type="hidden" />
									<c:if test="${not empty pageObj.pageElements}">
										<jsp:include page="/common/pager-nest.jsp">
											<jsp:param name="toPage" value="1" />
											<jsp:param name="showCount" value="1" />
											<jsp:param name="action" value="doSearch" />
										</jsp:include>
									</c:if>
								 </div> 
								 </form>
								 
								  <!-- 添加 -->
                                <div id="dlgJSZG" class="easyui-dialog" title="新增教师资格" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        
                                        <div id="table-content">
                                        <form id="fm" method="post" novalidate>
                                        <input type="hidden" name="jsid" value="${tbBizJzgjbxx.jsid}" id="jsidadd"/>
                                        <input type="hidden" value="${tbBizJzgjbxx.tbXxjgb.ssxd}" name="ssxd" id="ssxdadd"/>
                                        <table class="fitem clean">
                                            <tbody>
                                                <tr>
                                                    <td>教师资格证种类：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="jszgzzl" id="jszgzzl">
                                               				<app:dictselect dictType="JSXX_JSZGZZL" selectValue="0"/>
                                                        </select>
                                                    </td>
                                                    <td>教师资格证号码：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="jszgzhm" disabled="disabled" id="jszgzhm">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>任教学科：</td>
                                                    <td>
                                                          <select class="easyui-combobox"  name="rjxk" id="rjxk" disabled ="disabled">
                                               				<app:dictselect dictType="JSXX_RKXKLB" />
                                                        </select>
                                                    </td>
                                                    <td>证书颁发日期：</td>
                                                    <td>
                                                        <input type="text" class="easyui-datebox" name="zsbfrq" id="zsbfrq" disabled ="disabled">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>证书认定机构：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="zsrdjg" id="zsrdjg" disabled ="disabled">
                                                    </td>
                                                </tr>
                                              
                                            </tbody>
                                        </table>
                                  
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUserJSZG()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgJSZG').dialog('close')">取消</a>
                                </div>
                            </div>
                            
                            <!-- 编辑 -->
                              <div id="editJSZG" class="easyui-dialog" title="编辑教师资格" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlgEdit-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        
                                        <div id="table-content">
                                        <form id="fmEdit" method="post" novalidate>
                                         <input type="hidden" value="${tbBizJzgjbxx.tbXxjgb.ssxd}" name="ssxd" id="ssxdedit"/>
                                        <input type="hidden" name="jszgid" id="jszgidEdit"/>
                                         <input type="hidden" name="jsid" value="${tbBizJzgjbxx.jsid}" id="jsidedit"/>
                                        <table class="fitem clean">
                                            <tbody>
                                                <tr>
                                                    <td>教师资格证种类：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="jszgzzl" id="jszgzzlEdit">
                                               				<app:dictselect dictType="JSXX_JSZGZZL" />
                                                        </select>
                                                    </td>
                                                    <td>教师资格证号码：</td>
                                                    <td>
                                                         <input type="text" class="easyui-textbox" name="jszgzhm" id="jszgzhmEdit">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>任教学科：</td>
                                                    <td>
                                                          <select class="easyui-combobox"  name="rjxk" id="rjxkEdit">
                                               				<app:dictselect dictType="JSXX_RKXKLB"/>
                                                        </select>
                                                    </td>
                                                    <td>证书颁发日期：</td>
                                                    <td>
                                                         <input type="text" class="easyui-datebox" name="zsbfrq" id="zsbfrqEdit">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>证书认定机构：</td>
                                                    <td>
                                                         <input type="text" class="easyui-textbox" name="zsrdjg" id="zsrdjgEdit">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                  
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                <div id="dlgEdit-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="edit()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#editJSZG').dialog('close')">取消</a>
                                </div>
                            </div>
                            <div id="tips" class="easyui-dialog" title="温馨提示" style="padding:10px 20px;width:200px;height:100px;line-height:40px;text-align:center;overflow:auto;font-size:20px;" closed="true" data-options="modal:true">请选择一条记录</div>
                        </div> 
                        <c:if test="${SESSION_ISPUB == '1'}">
    	<div id="btn-wrap">
        <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="submitInfo()">报送</a>
        <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="cancelSubmit()">取消</a><span style="color:red">各项信息填写完整后，才能报送。报送后，不能再次修改教师信息。只有审核不通过的教师信息，才能再次编辑报送。</span>
    </div>
    </c:if>   
<script type="text/javascript">

$('#zsbfrq,#zsbfrqEdit').datebox({ editable:false });  //日期框禁止输入
$('#jszgzzl,#rjxk,#jszgzzlEdit,#rjxkEdit').combobox({ editable:false }); //下拉框禁止输入


$('#dg').datagrid({
	onLoadSuccess:function(data){
		if(data.rows[0].id==undefined){ 
			$('.datagrid-body td[field="id"]').html(data.rows[0].ck);
		}
	}
});

$('#dg').datagrid({
	onLoadSuccess:function(data){
		if(data.rows[0].id==undefined){
			$('#datagrid-row-r3-2-0').html(data.rows[0].ck);
			$('.datagrid-btable').css({'text-align':'center','width':'100%','padding':'30px 0'});
		}
	}
});

var url;
function save_user(dlgNmae){
    $(dlgNmae).dialog('open');
    //$('#dlgJSZG').form('clear');
     $('#jszgzhm').textbox('setValue','');
     $('#rjxk').combobox('setValue','');
	 $('#zsbfrq').textbox('setValue','');
     $('#zsrdjg').textbox('setValue','');
    //解决打开新增页面时，即使资格证为0，学科不可编辑但是有值的情况
    if($('#jszgzzl').combobox("getValue")=="0"){
    	$("#rjxk").combobox("setValue","");
    }
}

//关闭dialog
function save_close(dlgNmae){
    $(dlgNmae).dialog('close');
}

//新增
function newUserJSZG(){
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
        			save_user('#dlgJSZG');
			    }else{
					$.messager.alert('温馨提示','信息审核中或已通过审核，不可添加','warning');
				} 
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
		
	})
}

//弹出编辑框 
  function editUserJSZG(){
		$.ajax({
			url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
			data:{},
			dataType : "json",
			success:function(data){
				    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
	var selected=$("input[type='checkbox'][name='ck']:checked");
	
	if(selected.length<=0){
		$.messager.alert('温馨提示','请选择一条记录','warning');
		return ;
	}else if(selected.length>1){
		$.messager.alert('温馨提示','只能编辑一条记录','warning');
		return ;
	}else{	
		var jszgid=selected.val();
		var jsid = $('#jsid').val();
		$.ajax({
		    url:"${ctx}/basicInformation/TeacherQualityAction.a?toEdit&time="+new Date().getTime(),    //请求的url地址
		    dataType:"json",   //返回格式为json
		    //async:true,//请求是否异步，默认为异步，这也是ajax重要特性
		    data:{"jszgid":jszgid,"jsid":jsid},    //参数值
		    type:"post",   //请求方式
		    success:function(data){
	    	console.log(data);
	        var jszgzzl = data.tbBizJszgxx.jszgzzl;
	   	 if(jszgzzl!=0){
	   		 $("#jszgidEdit").val(data.tbBizJszgxx.jszgid);
		        $("#jszgzzlEdit").combobox('setValue',data.tbBizJszgxx.jszgzzl);
		        $("#jszgzhmEdit").textbox('setValue',data.tbBizJszgxx.jszgzhm);
		        $("#rjxkEdit").combobox('setValue',data.tbBizJszgxx.rjxk);
		        $("#zsbfrqEdit").textbox('setValue',data.tbBizJszgxx.zsbfrq);
		        $("#zsrdjgEdit").textbox('setValue',data.tbBizJszgxx.zsrdjg);
			   $('#jszgzhmEdit').textbox('enable');
		    	$('#rjxkEdit').combobox('enable');
			    $('#zsbfrqEdit').textbox('enable');
				$('#zsrdjgEdit').textbox('enable');
		 }
		 if(jszgzzl==0){
			 $("#jszgidEdit").val(data.tbBizJszgxx.jszgid);
			 $("#jszgzzlEdit").combobox('setValue',data.tbBizJszgxx.jszgzzl);
					 $('#jszgzhmEdit').textbox('setValue',' ');
					   $('#rjxkEdit').combobox('setValue',' ');
					   $('#zsbfrqEdit').textbox('setValue',' ');
					   $('#zsrdjgEdit').textbox('setValue',' ');
					   $('#jszgzhmEdit').textbox('disable');
					   $('#rjxkEdit').combobox('disable');
					   $('#zsbfrqEdit').textbox('disable');
					   $('#zsrdjgEdit').textbox('disable');
				 }
			
	 	    },
		    error:function(){
		    	$.messager.alert('温馨提示','非本人操作!','warning');
		    }
		});
		save_user('#editJSZG');
	}  
				    }else{
						$.messager.alert('温馨提示','信息审核中或已通过审核，不可编辑','warning');
					} 
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
			
		})
}	

//编辑
  function edit(){
	  	var jszgzhm = $("input[type='hidden'][name='jszgzhm']:last").val();
    	if(jszgzhm.trim()==""){
    		  /*  $('#rjxkEdit').combobox({required:false}); */
			   $('#zsbfrqEdit').textbox({required:false});
			   $('#zsrdjgEdit').textbox({required:false});
    	}else{
    		/*  $('#rjxkEdit').combobox({required:true}); */
			 $('#zsbfrqEdit').textbox({required:true});
			 $('#zsrdjgEdit').textbox({required:true});
			 var rjxk = $("input[type='hidden'][name='rjxk']:last").val();
			 var zsbfrq = $("input[type='hidden'][name='zsbfrq']:last").val();
			 var zsrdjg = $("input[type='hidden'][name='zsrdjg']:last").val();
			 if(rjxk.trim()==""){
				 $.messager.alert('温馨提示','任教学科必填','warning');
				 return;
			 }
			 if(zsbfrq.trim()==""){
				 $.messager.alert('温馨提示','证书颁发日期必填','warning'); 
				 return;
			 }
			 if(zsrdjg.trim()==""){
				 $.messager.alert('温馨提示','证书认定单位必填','warning'); 
				 return;
			 }
			 if(zsrdjg.length>50){
				 $.messager.alert('温馨提示','证书认定单位过长','warning'); 
				 return;
			 }
			 if(jszgzhm.length>25){
				 $.messager.alert('温馨提示','教师资格证号码过长','warning'); 
				 return;
			 }
    	}
  	$('#fmEdit').form('submit',{
          url: '${ctx}/basicInformation/TeacherQualityAction.a?edit',
          onSubmit: function(){
              return $(this).form('validate');
          },
          success: function(){
              save_close('#editJSZG');      // close the dialog
              $.messager.alert('温馨提示','保存成功','info',function(){
      	      	window.location.reload();
      		  	}); 
          },
          error:function(){
        	  $.messager.alert('温馨提示','非本人操作!','warning');
          }
      });
  }
	

function removeUserJSZG(){
	var selected=$("input[type='checkbox'][name='ck']:checked");
	if(selected.length<=0){
		$.messager.alert('温馨提示','请选择记录','warning');
		return;
	}
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){ 
 	 var strSel = '';
     $("input[name='ck']:checked").each(function(index, element) {
          strSel += $(this).val()+",";
      }); 
      var arrCk = strSel.substring(0,strSel.length-1); 
      var jsid = $('#jsid').val();
      var ssxd = $('#ssxd').val();
      $.messager.confirm('温馨提示', '请确认是否删除信息？', function(r){
      if(r){
    	  $.messager.alert('温馨提示','删除成功','info',function(){
		window.location.href ="${ctx}/basicInformation/TeacherQualityAction.a?delete&ck="+arrCk+"&jsid="+jsid+"&ssxd="+ssxd;
			  	});
	  }
      });
			    }else{
					$.messager.alert('温馨提示','信息审核中或已通过审核，不可删除','warning');
				} 
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
		
	}) 
}

//保存
function saveUserJSZG(){
	var jszgzhm = $("input[type='hidden'][name='jszgzhm']:first").val();
	if(jszgzhm.trim()==""){
	/* 	   $('#rjxk').combobox({required:false}); */
		   $('#zsbfrq').textbox({required:false});
		   $('#zsrdjg').textbox({required:false});
	}else{
	/* 	 $('#rjxk').combobox({required:true}); */
		 $('#zsbfrq').textbox({required:true});
		 $('#zsrdjg').textbox({required:true});
		 var rjxk = $("input[type='hidden'][name='rjxk']:first").val();
		 var zsbfrq = $("input[type='hidden'][name='zsbfrq']:first").val();
		 var zsrdjg = $("input[type='hidden'][name='zsrdjg']:first").val();
		 if(rjxk.trim()==""){
			 $.messager.alert('温馨提示','任教学科必填','warning');
			 return;
		 }
		 if(zsbfrq.trim()==""){
			 $.messager.alert('温馨提示','证书颁发日期必填','warning'); 
			 return;
		 }
		 if(zsrdjg.trim()==""){
			 $.messager.alert('温馨提示','证书认定单位必填','warning'); 
			 return;
		 }
		 if(zsrdjg.length>50){
			 $.messager.alert('温馨提示','证书认定机构名称过长','warning'); 
			 return;
		 }
		 if(jszgzhm.length>25){
			 $.messager.alert('温馨提示','教师资格证号码过长','warning'); 
			 return;
		 }
	} 
	
	
	$.messager.confirm('温馨提示', '确认保存？', function(r){
		 if(r){
	$('#jsidadd').val($('#jsidedit').val());
	$('#ssxdadd').val($('#ssxdedit').val());
	$('#fm').form('submit',{
        url: '${ctx}/basicInformation/TeacherQualityAction.a?add',
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(){
            save_close("#dlgJSZG");      // close the dialog
            $.messager.alert('温馨提示','保存成功','info',function(){
    	      	window.location.reload();
    		  	});
        },
        error:function(){
        	$.messager.alert('温馨提示','请选择记录','warning');
        }
    });
	
		 }
	 });
} 
 
/**
 * 验证能否添加时选择其它选项
 */
 $('#jszgzzl').combobox({
	   onSelect: function(param){
		   var jszgzzl = $("input[type='hidden'][name='jszgzzl']:first").val();
		   if(jszgzzl==0){
			   $('#jszgzhm').textbox('setValue',' ');
			   $('#rjxk').combobox('setValue',' ');
			   $('#zsbfrq').textbox('setValue',' ');
			   $('#zsrdjg').textbox('setValue',' ');
			   $('#jszgzhm').textbox('disable');
			   $('#rjxk').combobox('disable');
			   $('#zsbfrq').textbox('disable');
			   $('#zsrdjg').textbox('disable');
		   }
		   if(jszgzzl!=0){
			   $('#rjxk').combobox('setValue',1);
			   $('#jszgzhm').textbox('enable');
			   $('#rjxk').combobox('enable');
			   $('#zsbfrq').textbox('enable');
			   $('#zsrdjg').textbox('enable');
		   }
		}
	});
	
 
/**
 * 验证能否修改时选择其它选项
 */
 $('#jszgzzlEdit').combobox({
	   onSelect: function(param){
		   var jszgzzl = $("input[type='hidden'][name='jszgzzl']:last").val();
		   if(jszgzzl==0){
			   $('#jszgzhmEdit').textbox('setValue',' ');
			   $('#rjxkEdit').combobox('setValue',' ');
			   $('#zsbfrqEdit').textbox('setValue',' ');
			   $('#zsrdjgEdit').textbox('setValue',' ');
			   $('#jszgzhmEdit').textbox('disable');
			   $('#rjxkEdit').combobox('disable');
			   $('#zsbfrqEdit').textbox('disable');
			   $('#zsrdjgEdit').textbox('disable');
		   }
		   if(jszgzzl!=0){
			   $('#rjxkEdit').combobox('setValue',1);
			   $('#jszgzhmEdit').textbox('enable');
			   $('#rjxkEdit').combobox('enable');
			   $('#zsbfrqEdit').textbox('enable');
			   $('#zsrdjgEdit').textbox('enable');
		   }
		}
	});
  
 /**
  *报送
	*/
	function submitInfo(){
  		 
				$.ajax({
					url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
					data:{},
					dataType : "json",
					success:function(data){
						    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
						    	$.ajax({
									url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?submitted&time='+new Date().getTime(),
									data:{},
									success:function(data){
										$.messager.alert('温馨提示','报送成功','info',function(){
											window.parent.location.reload();
							    	  	});
									},
									error:function(data){
										$.messager.alert('温馨提示','报送失败,请检查各项是否都已填写！','warning');
									}
									
								});   
							}else{
								$.messager.alert('温馨提示','信息已报送，不可重复报送！','warning');
							}  
					},
					error:function(XMLHttpRequest, textStatus, errorThrown){
						var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
						sessionTimeout(sessionstatus);
					}
					
				})
  		 
  	 }
  /**
  *取消报送
	*/
	function cancelSubmit(){
		 
		$.ajax({
			url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
			data:{},
			dataType : "json",
			success:function(data){
				    if(data.tbJzgxxsh.shzt==02){
				    	$.ajax({
							url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?cancelSubmit&time='+new Date().getTime(),
							data:{},
							success:function(data){
								$.messager.alert('温馨提示','取消成功','info',function(){
					            	
					    	  	});
							},
							error:function(data){
							
							}
							
						});  
					}else{
						$.messager.alert('温馨提示','信息审核中(审核通过)或未报送，不可取消保送！','warning');
					}  
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
			
		})
			
  	 }

function addTab(title, url){
	if ($('#tb').tabs('exists', title)){
		$('#tb').tabs('select', title);
	} else {
		var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
		$('#tb').tabs('add',{
			title:title,
			content:content,
			closable:true
		});
	}
}
//关闭所有的tab  
    function closeAll(){  
        var tiles = new Array();  
          var tabs = $('#tb').tabs('tabs');      
          var len =  tabs.length;           
          if(len>0){  
            for(var j=0;j<len;j++){  
                var a = tabs[j].panel('options').title;               
                tiles.push(a);  
            }  
            for(var i=1;i<tiles.length;i++){               
                $('#tb').tabs('close', tiles[i]);  
            }
          }
    } 
//关闭当前的tab 
	function removePanel(){
		var tab = $('#tb').tabs('getSelected');
		    if (tab){
			var index = $('#tb').tabs('getTabIndex', tab);
			$('#tb').tabs('close', index);
			}
		}

</script>			
</body>
</html>
