<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>北京市教师管理服务平台</title>
<link rel="stylesheet" type="text/css" href="../css/newcss.css">
<link rel="stylesheet" type="text/css"
	href="../easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
<script type="text/javascript" src="../easyui/jquery.min.js"></script>
<script src="../js/echarts/build/dist/echarts-all.js"></script>
<script src="../js/echarts/build/dist/echarts.js"></script>
<script type="text/javascript" src="../easyui/easyloader.js"></script>
<script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${ctx}/js/refresh.js"></script>
<script type="text/javascript" src="../My97DatePicker/WdatePicker.js"></script>
<link rel="stylesheet" type="text/css" href="../My97DatePicker/skin/WdatePicker.css">
<script type="text/javascript">
var myDate = new Date();
var defaultYear = myDate.getFullYear() - 1;///业务上的默认年
function setDefaultYear(){
	$("#defaultYear").val(defaultYear);
	$("#khnd").val(defaultYear);
}
</script>
</head>
<form action="${ctx}/basicInformation/YearTestManagerAction.a" id="fm" method="post" novalidate>
<div id="anchor4">
	<div data-options="region:'center',border:false,collapsible:true">
		<div id="toolbarGW">
			<a class="easyui-linkbutton" iconCls="icon-add" plain="true"
				onclick="return newUserSD();">新 增</a> <a href="#"
				class="easyui-linkbutton" iconCls="icon-edit" plain="true"
				onclick="editUser()">编 辑</a> <a href="#" class="easyui-linkbutton"
				iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a>
                <span style="color:red">注意：从本年度开始填写</span>
		</div>
		<input type="hidden" value="${tbJzgxxsh.shzt}" id="xxshzt"/>
		<table id="dg" class="easyui-datagrid" title="年度考核"
			iconCls="icon-dingdan" 
			data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,collapsible:true,toolbar:'#toolbarGW',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
			<thead>
				 <tr>
                     <th field="ck" checkbox="true"></th>
                     <th data-options="field:'khnd',width:1">考核年度</th>
                     <th data-options="field:'khdwmc',width:5">考核单位名称</th>
                     <th data-options="field:'khjg',width:1">考核结果</th>
                     <th data-options="field:'cjsj',width:1">创建时间</th>
                 </tr>
			</thead>
			<tbody>
				<c:if test="${empty pageObj.pageElements}">
					<tr>
						<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
					</tr>
				</c:if>
				<c:forEach items="${pageObj.pageElements}" var="item"
					varStatus="status">
					<tr>
						<td>${item.ndkhId}</td>
						<%-- <td>${item.ndkhId}</td>  --%>
						<td>${item.khnd}</td>
						<td>${item.khdwmc}</td>
						<td><app:dictname dictid="JSXX_KHJG${item.khjg}"/></td>
						<td>${item.cjsj}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
	<div class="page tar mb15">
		<input name="pageNo" value="${pageNo }" type="hidden" />
		<c:if test="${not empty pageObj.pageElements}">
			<jsp:include page="../common/pager-nest.jsp">
				<jsp:param name="toPage" value="1" />
				<jsp:param name="showCount" value="1" />
				<jsp:param name="action" value="doSearch" />
			</jsp:include>
		</c:if>
	</div>
</form>
	<div id="dlg-buttons">
		<a  class="easyui-linkbutton" iconCls="icon-ok"
			onclick="saveUser()" id="save">保存</a> <a href="#"
			class="easyui-linkbutton mr20" iconCls="icon-cancel"
			onclick="javascript:$('#dlgND').dialog('close')">取消</a>
	</div>
</div>
</div>

<div id="dlgND" class="easyui-dialog" title="新增年度考核" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
    <input type="hidden" name="xxjgmc" id="xxjgmc" value="${xxjgmc}">
    <form id="fom1" method="post" novalidate>
    <div class="right_table">
        <div id="table-content">
        <table class="fitem clean">
            <tbody>
                <tr>
                    <td>考核年度：</td>
                    <td>
                        <input class="Wdate" type="text" name="khnd"  id="khnd" onfocus="WdatePicker({dateFmt:'yyyy',maxDate:'#F{$dp.$D(\'defaultYear\')}'})"/>
                        <input type="hidden" id="defaultYear"/>
                    </td>
                    <td>考核单位名称：</td>
                    <td>
                        <input type="text" class="easyui-textbox"  name="khdwmc" id="khdwmc" required>
                    </td>
                </tr>
                <tr>
                    <td>考核结果：</td>
                    <td>
                        <select class="easyui-combobox" required="true" name="khjg" id="khjg">
                			<app:dictselect dictType="JSXX_KHJG" />
                        </select>
                    </td>
                </tr>
                <input type="hidden" name="ndkhId" id="ndkhId">
            </tbody>
        </table>
        </div>
    </div>
    <div id="tips" class="easyui-dialog" title="温馨提示" style="padding:10px 20px;width:200px;height:100px;line-height:40px;text-align:center;overflow:auto;font-size:20px;" closed="true" data-options="modal:true">请选择一条记录</div>
    </form>
</div>

<c:if test="${SESSION_ISPUB == '1'}">
    	<div id="btn-wrap">
        <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="submitInfo()">报送</a>
        <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="cancelSubmit()">取消</a><span style="color:red">各项信息填写完整后，才能报送。报送后，不能再次修改教师信息。只有审核不通过的教师信息，才能再次编辑报送。</span>
    </div>
    </c:if>

<script type="text/javascript">

	$('#dg').datagrid({
		onLoadSuccess:function(data){
			if(data.rows[0].khnd==undefined){
				$('#datagrid-row-r2-2-0').html(data.rows[0].ck);
				$('.datagrid-btable').css({'text-align':'center','width':'100%','padding':'30px 0'});
			}
		}
	});
	//新增
	function newUserSD() {
		$.ajax({
			url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
			data:{},
			dataType : "json",
			success:function(data){
				    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22||data.tbJzgxxsh.shzt==02){
			$("#khjg").combobox('setValue',1);
			var xxjgmc = $("#xxjgmc").val();
			$("#khdwmc").textbox('setValue',xxjgmc);
			save_user('#dlgND');
				    }else{
						$.messager.alert('温馨提示','信息审核中或已通过审核，不可添加','warning');
					} 
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
			
		})
	}
	function save_user(dlgNmae) {
		$(dlgNmae).dialog('open');
		$('#fom1').clear();
		$('#khjg').combobox({ editable:false }); //考核结果
		setDefaultYear();
	}

	function saveUser() {
		var ndkhId = $("#ndkhId").val();
		var khnd = $("#khnd").val();
		var khdwmc = $("#khdwmc").val();
		if(!khdwmc||khdwmc==""){
			return $.messager.alert('温馨提示','请输入考核单位名称','warnning');
		}
		if(!khnd){
			return $.messager.alert('温馨提示','请选择考核年度','warnning');
		}
		
		 $.messager.confirm('温馨提示', '确认保存？', function(r){
			 if(r){
		if (ndkhId) {
			$('#fom1').form('submit',{
								url : "${ctx}/basicInformation/YearTestManagerAction.a?toUpdateYearTest&time="+new Date().getTime(),
								onSubmit : function() {
									return $(this).form('validate');
								},
								success : function(result) {
 
									$.messager.alert('温馨提示','修改成功','info',function(){
										window.location.reload();
								  	});
 
								}
							});
		} else {
			$('#fom1').form(
							'submit',
							{
								url : "${ctx}/basicInformation/YearTestManagerAction.a?SubAddYearTest&time="+new Date().getTime(),
								onSubmit : function() {
									return $(this).form('validate');
								},
								success : function(result) {
 
									$.messager.alert('温馨提示','添加成功','info',function(){
										window.location.reload();
								  	});
 
								}
							});
		} 
		save_close('#dlgND');
			 }
		 });
	}
	//回显
	 function editUser() {
			$.ajax({
				url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
				data:{},
				dataType : "json",
				success:function(data){
					    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22||data.tbJzgxxsh.shzt==02){
		var selected = $("input[type='checkbox'][name='ck']:checked");
		if (selected.length <= 0) {
			$.messager.alert('温馨提示', '请选择一条记录', 'warning');
			return;
		} else if (selected.length > 1) {
			$.messager.alert('温馨提示', '只能编辑一条记录', 'warning');
			return;
		}
		var strSel = selected.val();
		var row = $('#dg').datagrid('getSelected');
		if (row) {
			$('#dlgND').dialog('open').dialog('setTitle', '编辑年度考核');
			$('#khjg').combobox({ editable:false }); //考核结果
			
			$('#form1').form('load', row);
			$.ajax({
						url : '${ctx}/basicInformation/YearTestManagerAction.a?editbasicYearTest&ndkhId='+strSel+'&time='+new Date().getTime(),
						dataType : 'json',
						success : function(data) {
							console.log(JSON.stringify(data));
							$("#ndkhId").val(data.dto.ndkhId);
							$("#khdwmc").textbox('setValue',data.dto.khdwmc);
							$("#khnd").val(data.dto.khnd); 
							$("#khjg").combobox('setValue',data.dto.khjg); 
							
						}
					});
		}
					    }else{
							$.messager.alert('温馨提示','信息审核中或已通过审核，不可编辑','warning');
						} 
				},
				error:function(XMLHttpRequest, textStatus, errorThrown){
					var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
					sessionTimeout(sessionstatus);
				}
				
			})
	} 
	//删除
	 function removeUser() {
		var selected = $("input[type='checkbox'][name='ck']:checked");
 
		if(selected.length<=0){
			return $.messager.alert('温馨提示','请选择一条记录','warning');
 
		}
		$.ajax({
			url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt',
			data:{},
			dataType : "json",
			success:function(data){
				    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22||data.tbJzgxxsh.shzt==02){
								var strSel = '';
								$("input[name='ck']:checked").each(function(index, element) {
									strSel += $(this).val() + ",";
								});
								var cks = strSel.substring(0, strSel.length - 1)
								
								$.messager.confirm('温馨提示', '请确认是否删除信息？', function(r){
							    	 		if (r){
							     			$.messager.alert('温馨提示','删除成功','info',function(){
									window.location.href = '${ctx}/basicInformation/YearTestManagerAction.a?delBasicYearTest&ck='+cks+'&time='+new Date().getTime();
							     		  	});
							     		}
							     	});
				    }else{
						$.messager.alert('温馨提示','信息审核中或已通过审核，不可删除','warning');
					} 
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
			
		}) 
	} 
	 
	
	    /**
	    *报送
		*/
		function submitInfo(){
	    		 
					$.ajax({
						url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt',
						data:{},
						dataType : "json",
						success:function(data){
							    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
							    	$.ajax({
										url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?submitted&time='+new Date().getTime(),
										data:{},
										success:function(data){
											$.messager.alert('温馨提示','报送成功','info',function(){
												window.parent.location.reload();
								    	  	});
										},
										error:function(data){
											$.messager.alert('温馨提示','报送失败,请检查各项是否都已填写！','warning');
										}
										
									});   
								}else{
									$.messager.alert('温馨提示','信息已报送，不可重复报送！','warning');
								}  
						},
						error:function(XMLHttpRequest, textStatus, errorThrown){
							var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
							sessionTimeout(sessionstatus);
						}
						
					})
	    		 
	    	 }
	    /**
	    *取消报送
		*/
		function cancelSubmit(){
			 
			$.ajax({
				url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
				data:{},
				dataType : "json",
				success:function(data){
					    if(data.tbJzgxxsh.shzt==02){
					    	$.ajax({
								url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?cancelSubmit&time='+new Date().getTime(),
								data:{},
								success:function(data){
									$.messager.alert('温馨提示','取消成功','info',function(){
						            	
						    	  	});
								},
								error:function(data){
								
								}
								
							});  
						}else{
							$.messager.alert('温馨提示','信息审核中(审核通过)或未报送，不可取消保送！','warning');
						}  
				},
				error:function(XMLHttpRequest, textStatus, errorThrown){
					var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
					sessionTimeout(sessionstatus);
				}
				
			})
				
	    	 }
	 
	//提示信息
	 function tips(dlgNmae,msg){
	 	$(dlgNmae).html(msg).dialog('open');
	 }
	//关闭dialog
	 function save_close(dlgNmae){
	     $(dlgNmae).dialog('close');
	 }

</script>
