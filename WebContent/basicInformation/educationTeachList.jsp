<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="${ctx}/css/newcss.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/easyui/themes/xyy.css">
    <script type="text/javascript" src="${ctx}/easyui/jquery.min.js"></script>
    <script src="${ctx}/js/echarts/build/dist/echarts-all.js"></script>
    <script src="${ctx}/js/echarts/build/dist/echarts.js"></script>
    <script type="text/javascript" src="${ctx}/easyui/easyloader.js"></script>
    <script type="text/javascript" src="${ctx}/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="${ctx}/easyui/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="${ctx}/js/refresh.js"></script>
	<script type="text/javascript" src="../My97DatePicker/WdatePicker.js"></script>
    <link rel="stylesheet" type="text/css" href="../My97DatePicker/skin/WdatePicker.css">
    <script type="text/javascript">
	var myDate = new Date();
	var defaultYear = '${year}';  ///业务上的默认年
	
	$(function(){
		$("#defaultYear").val(defaultYear);
	});
	function setDefaultYear(){
		$("#defaultYear").val(defaultYear);
	}
	</script>
</head>
<body>
      <form action="${ctx}/basicInformation/EducationTeachAction.a" id="fm" method="post" novalidate>
		<div class="content" id="userList">
			<div class="location">
			</div>
			<div class="modelbox modelbox3">

			</div>
 					<div id="anchor70">
                            <div data-options="region:'center',border:false,collapsible:true" style="margin-bottom:20px;">
                                <div id="toolbarSD">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUserSD()">新 增</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">编 辑</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a>
                                    
                                </div>
                                <input type="hidden" value="${tbJzgxxsh.shzt}" id="xxshzt"/>
                                <input type="hidden" id="defaultYear"/>
                                <table id="dg" class="easyui-datagrid" title="新增教育教学" iconCls="icon-dingdan" 
                                    data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,collapsible:true,toolbar:'#toolbarSD',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
                                
                                <thead>
                                    <tr>
                                    <th field="ck" checkbox="true"></th>
                                    <c:if test="${ssxd=='10'}">
                                        <th data-options="field:'id',width:60">年 度</th>
                                        <th data-options="field:'shencha',width:60">学期</th>
                                        <th data-options="field:'chachong',width:100">导师类别</th>
                                        <th data-options="field:'name',width:130">现主要从事学科领域</th>
                                        <th data-options="field:'named',width:130">是否为本科生上课</th>
                                        <th data-options="field:'na',width:100">任课状况</th>
                                        <th data-options="field:'sex',width:120">课程教学课时数</th>
                                        <th data-options="field:'num',width:100">创建时间</th>
                                    </c:if>
                                    <c:if test="${ssxd=='20'}">
                                        <th data-options="field:'id',width:60">年 度</th>
                                        <th data-options="field:'shencha',width:60">学期</th>
                                      <!--   <th data-options="field:'chachong',width:100">导师类别</th> -->
                                        <th data-options="field:'name',width:130">现主要从事学科领域</th>
                                      <!--   <th data-options="field:'named',width:130">是否为本科生上课</th> -->
                                        <th data-options="field:'na',width:100">任课状况</th>
                                        <th data-options="field:'sex',width:120">课程教学课时数</th>
                                        <th data-options="field:'rklb',width:120">任课课程类别</th>
                                        <th data-options="field:'num',width:100">创建时间</th>
                                    </c:if>
                                    <c:if test="${ssxd=='30'||ssxd=='50'}">
                                    	<th data-options="field:'id',width:60">年 度</th>
                                        <th data-options="field:'shencha',width:60">学期</th>
                                        <th data-options="field:'chachong',width:100">任教学段</th>
                                        <th data-options="field:'na',width:60">任课状况</th>
                                        <th data-options="field:'rjkc',width:60">任教课程</th>
                                        <th data-options="field:'kss',width:60">平均每周课堂教学课时数</th>
                                        <th data-options="field:'sex',width:60">兼任工作</th>
                                        <th data-options="field:'num',width:100">创建时间</th>
                                    </c:if>
                                    <c:if test="${ssxd=='40'}">
                                    	<th data-options="field:'id',width:60">年 度</th>
                                        <th data-options="field:'na',width:100">任课状况</th>
                                        <th data-options="field:'kclb',width:120">任课课程类别</th>
                                        <th data-options="field:'xklb',width:120">任课学科类别</th>
                                        <th data-options="field:'sex',width:120">课程教学课时数</th>
                                        <th data-options="field:'jrgz',width:60">兼任工作</th>
                                        <th data-options="field:'num',width:100">创建时间</th>
                                    </c:if>
                                       <!--  <th field="ck" checkbox="true"></th>
                                        <th data-options="field:'id',width:60">年 度</th>
                                        <th data-options="field:'shencha',width:60">学期</th>
                                        <th data-options="field:'chachong',width:100">导师类别</th>
                                        <th data-options="field:'name',width:130">现主要从事学科领域</th>
                                        <th data-options="field:'named',width:130">是否为本科生上课</th>
                                        <th data-options="field:'na',width:100">任课状况</th>
                                        <th data-options="field:'sex',width:120">教程教学课时数</th>
                                        <th data-options="field:'num',width:100">创建时间</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                <c:if test="${empty pageObj.pageElements}">
									<tr>
										<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
									</tr>
								</c:if>
								<div class="page tar mb15">
									<c:if test="${not empty pageObj.pageElements}">
									<c:forEach items="${pageObj.pageElements}" var="item"
										varStatus="status">
										<tr>
										<td field="ck" checkbox="true">${item.jyid}</td>
											<input type="hidden"  name="jyid" id="jyid" value="${item.jyid }">
											<%-- <input type="hidden"  name="xq1" id="xq1" value="${item.xq }">
											<input type="hidden"  name="dslb1" id="dslb1" value="${item.dslb }">
											<input type="hidden"  name="sfwbkssk1" id="sfwbkssk1" value="${item.sfwbkssk }">
											<input type="hidden"  name="rkzk1" id="rkzk1" value="${item.rkzk }"> --%>
											 <c:if test="${ssxd=='10'}">
                                    			<td>${item.xn}</td>
												<%-- <td>
													<c:if test="${item.xq eq 1}">上半年</c:if>
													<c:if test="${item.xq eq 2}">下半年</c:if>
													<c:if test="${item.xq eq 9}">其他</c:if>
												</td> --%>
												<td><appadd:dictnameadd dictid="JSXX_XQ" dictbm="${item.xq}" schoolType="${ssxd}"/></td>
												<td><app:dictname dictid="JSXX_DSLB${item.dslb}" /></td>
												<td><app:dictname dictid="JSXX_XKML${item.xzycsxkly}"/></td>
												<%-- <td>${item.xzycsxkly}</td> --%>
												<td><app:dictname dictid="JSXX_SFWBKSSK${item.sfwbkssk}" /></td>
												<td><app:dictname dictid="JSXX_RKZK${item.rkzk}"/></td>
												<td>${item.xnbzkkcjxkss}</td>
												<td>${item.cjsj}</td>
                                    		</c:if>
                                    		<c:if test="${ssxd=='20'}">
                                    			<td>${item.xn}</td>
												<%-- <td>
													<c:if test="${item.xq eq 1}">上半年</c:if>
													<c:if test="${item.xq eq 2}">上半年</c:if>
													<c:if test="${item.xq eq 9}">其他</c:if>
												</td> --%>
												<td><appadd:dictnameadd dictid="JSXX_XQ" dictbm="${item.xq}" schoolType="${ssxd}"/></td>
												<%-- <td><app:dictname dictid="JSXX_DSLB${item.dslb}" /></td> --%>
												<td><app:dictname dictid="JSXX_XKML${item.xzycsxkly}"/></td>
												<%-- <td><app:dictname dictid="JSXX_SFWBKSSK${item.sfwbkssk}" /></td> --%>
												<td><app:dictname dictid="JSXX_RKZK${item.rkzk}"/></td>
												<td>${item.xnbzkkcjxkss}</td>
												<td>
													<c:forEach items="${item.rkkclblist}" var="next" varStatus="indexNo">
													 <app:dictname dictid="JSXX_RKKCLB${next}" />
													</c:forEach>
												</td>
												
												<td>${item.cjsj}</td>
                                    		</c:if>
                                    		<c:if test="${ssxd=='30'||ssxd=='50'}">
                                    			<td>${item.xn}</td>
												<%-- <td>
													<c:if test="${item.xq eq 1}">春季</c:if>
													<c:if test="${item.xq eq 2}">秋季</c:if>
												</td> --%>
												<td><appadd:dictnameadd dictid="JSXX_XQ" dictbm="${item.xq}" schoolType="${ssxd}"/></td>
												<td><app:dictname dictid="JSXX_RJXD${item.rjxd}" /></td>
												<td><app:dictname dictid="JSXX_RKZK${item.rkzk}"/></td>
												<%-- <td>${item.rjkc}</td> --%>
												<td>
												<c:if test="${item.rjkc != null }">
													<c:forEach items="${item.rjkc }" var="rj">
														<appadd:dictnameadd dictid="JSXX_RJKC" dictbm="${rj}" schoolType="${ssxd}"/>
													</c:forEach>
												</c:if>
												<c:if test="${item.rjkc == null }">
												<appadd:dictnameadd dictid="JSXX_RJKC" dictbm="${rj}" schoolType="${ssxd}"/>
												</c:if>
												</td>
												<td>${item.pjzks }</td>
												<td><app:dictname dictid="JSXX_JRGZ${item.jrgz}"/></td>
												<td>${item.cjsj}</td>
                                    		</c:if>
                                    		<c:if test="${ssxd=='40'}">
                                    			<td>${item.xn}</td>
                                    			<td><app:dictname dictid="JSXX_RKZK${item.rkzk}"/></td>
												<%-- <td>${item.rkxklb}</td> --%>
													<td>
														<c:forEach items="${item.rkkclblist}" var="next" varStatus="indexNo">
														 <app:dictname dictid="JSXX_RKKCLB${next}" />
														</c:forEach>
													</td>
													<td>${item.rkxklb}</td>
												<td>${item.pjzks}</td>
												<td><app:dictname dictid="JSXX_JRGZ${item.jrgz}"/></td>
												<td>${item.cjsj}</td>
                                    		</c:if>
										</tr>
									</c:forEach>
									
									</c:if>
								</div>
                                </tbody>
                                </table>
                                
                                 <div id="dlgJY" class="easyui-dialog" title="新增教育教学" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        
                                        <div id="table-content">
                                         <table class="fitem clean">
											<c:if test="${ssxd=='10'}">
   												<tr>
                                                    <td>年度：</td>
                                                    <td>
                                                        <%-- <input type="text" class="easyui-datebox" required="true" name="xn" id="xn" value="${dto.xn}"> --%>
                                                         <input id="xn" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy',maxDate:'#F{$dp.$D(\'defaultYear\')}'})" value="${dto.xn}"/>
                                                    	<span style="color:red;display:block">请填本年度</span>
                                                    </td>
                                                    <td>学期：</td>
                                                    <td>
                                                        <select  class="easyui-combobox" required="true" name="xq" id="xq" value="${dto.xq}" >
                                                          <%--  <c:forEach items ="${conclusion }" var="con">
	                                                          	<option value="${con.zdxbm}" <c:if test="${dto.xq eq con.zdxbm}">selected</c:if>>${con.zdxmc}</option> 
                                                           </c:forEach> --%>
                                                           <appadd:dictselectadd dictType="JSXX_XQ" schoolType="${ssxd}"/>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>导师类别：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="dslb" id="dslb" value="${dto.dslb}">
                                                            <app:dictselect dictType="JSXX_DSLB" />
                                                        </select>
                                                    </td>
                                                    <td>现主要从事学科领域：</td><!--data-options="searcher:doSearch"  -->
                                                    <td>
                                                        <input type="text" class="easyui-searchbox"  data-options="searcher:doSearch" required="true" name="xzycsxkly1" id="xzycsxkly" value="${dto.xzycsxkly}">
                                                    	<input type="hidden"  name="xzycsxkly" >
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>是否为本科生上课：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="sfwbkssk" id="sfwbkssk" value="${dto.sfwbkssk}">
                                                            <app:dictselect dictType="JSXX_SFWBKSSK" />
                                                        </select>
                                                    </td>
                                                   
                                                    <td>任课状况</td>
                                                    <td>
                                                         <select class="easyui-combobox" required="true" name="rkzk" id="rkzk" value="${dto.rkzk}">
                                                            <app:dictselect dictType="JSXX_RKZK" />
                                                        </select>
                                                    </td>
                                                    
                                                </tr>
                                                <tr>
                                                	<td>课程教学课时数：</td>
                                                    <td>
                                                        <input type="text"  class="easyui-numberbox" required="true" name="xnbzkkcjxkss" id="xnbzkkcjxkss" min="0" max="99999" precision="0" value="${dto.xnbzkkcjxkss}" >
                                                    </td>
                                                </tr>
											</c:if>
											
											<c:if test="${ssxd=='20'}">
   												<tr>
                                                    <td>年度：</td>
                                                    <td>
                                                       <%--  <input type="text" class="easyui-datebox" required="true" name="xn" id="xn" value="${dto.xn}"> --%>
                                                        <input id="xn" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy',maxDate:'#F{$dp.$D(\'defaultYear\')}'})" value="${dto.xn}"/>
                                                    	<span style="color:red;display:block">请填本年度</span>
                                                    </td>
                                                    <td>学期：</td>
                                                    <td>
                                                        <select  class="easyui-combobox" required="true" name="xq" id="xq" value="${dto.xq}" >
                                                         <%--   <c:forEach items ="${conclusion }" var="con">
	                                                          	<option value="${con.zdxbm}" <c:if test="${dto.xq eq con.zdxbm}">selected</c:if>>${con.zdxmc}</option> 
                                                           </c:forEach> --%>
                                                            <appadd:dictselectadd dictType="JSXX_XQ" schoolType="${ssxd}"/>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>任课状况</td>
                                                    <td>
                                                         <select class="easyui-combobox" required="true" name="rkzk" id="rkzk" value="${dto.rkzk}">
                                                            <app:dictselect dictType="JSXX_RKZK" />
                                                        </select>
                                                    </td>
                                                    <td>现主要从事学科领域：</td>
                                                    <td>
                                                        <input type="text" class="easyui-searchbox"  data-options="searcher:doSearch" required="true" name="xzycsxkly1" id="xzycsxkly" value="${dto.xzycsxkly}" >
                                                    	<input type="hidden" name="xzycsxkly"  >
                                                    </td>
                                                </tr>
                                                <%-- <tr>
                                                    <td>是否为本科生上课：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="sfwbkssk" id="sfwbkssk" value="${dto.sfwbkssk}">
                                                            <app:dictselect dictType="JSXX_SFWBKSSK" />
                                                        </select>
                                                    </td>
                                                    <td>任课状况</td>
                                                    <td>
                                                         <select class="easyui-combobox" required="true" name="rkzk" id="rkzk" value="${dto.rkzk}">
                                                            <app:dictselect dictType="JSXX_RKZK" />
                                                        </select>
                                                    </td>
                                                    
                                                </tr> --%>
                                                <tr>
                                                	<td>课程教学课时数：</td>
                                                    <td>
                                                        <input type="text"  class="easyui-numberbox" required="true" name="xnbzkkcjxkss" id="xnbzkkcjxkss" min="0" max="99999" precision="0" value="${dto.xnbzkkcjxkss}" min="0" max="9999" precision="0">
                                                    </td>
                                                    <td>任课课程类别：</td>
                                                    <td>
                                                         <select class="easyui-combobox" required="true" name="rkkclb" id="rkkclb" value="${dto.rkkclb}" data-options="multiple:true,multiline:true" >
                                                            <app:dictselect dictType="JSXX_RKKCLB" />
                                                        </select>
                                                    </td>
                                                </tr>
												</c:if>
												
												<c:if test="${ssxd=='30'||ssxd=='50'}">
													<tr>
                                                    <td>年度：</td>
                                                    <td>
                                                        <%-- <input type="text" class="easyui-datebox" required="true" name="xn" id="xn" value="${dto.xn}"> --%>
                                                         <input id="xn" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy',maxDate:'#F{$dp.$D(\'defaultYear\')}'})" value="${dto.xn}"/>
                                                    </td>
                                                    <td>学期：</td>
                                                    <td>
                                                        <select  class="easyui-combobox" required="true" name="xq" id="xq" value="${dto.xq}" >
                                                            <appadd:dictselectadd dictType="JSXX_XQ" schoolType="${ssxd}"/>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>任教学段：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="rjxd" id="rjxd" value="${dto.rjxd}">
                                                            <app:dictselect dictType="JSXX_RJXD" />
                                                        </select>
                                                    </td>
                                                    <td>任课状况：</td>
                                                    <td>
                                                         <select class="easyui-combobox" required="true" name="rkzk" id="rkzk" value="${dto.rkzk}">
                                                            <app:dictselect dictType="JSXX_RKZK" />
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>任教课程：</td>
                                                    <td>
                                                        <input type="text" class="easyui-searchbox" data-options="searcher:doSearch"  required="true" name="rjkc1" id="rjkc" value="${dto.rjkc}">
                                                    	<input type="hidden" name="rjkc"  >
                                                    </td>
                                                    <td>平均每周课堂教学课时数：</td>
                                                    <td>
                                                         <input type="text"  class="easyui-numberbox" min="0" max="99999" precision="0"  name="pjzks" required="true" id="pjzks" value="${dto.pjzks}">
                                                    </td>
                                                    
                                                </tr>
                                                <tr>
                                                	<td>兼任工作：</td>
                                                    <td>
                                                    <select class="easyui-combobox" required="true" name="jrgz" id="jrgz" value="${dto.jrgz}">
                                                            <app:dictselect dictType="JSXX_JRGZ" />
                                                        </select>
                                                    </td>
                                                </tr>
												</c:if>
												<c:if test="${ssxd=='40'}">
												<tr>
                                                    <td>学年：</td>
                                                    <td>
                                                        <%-- <input type="text" class="easyui-datebox" required="true" name="xn" id="xn" value="${dto.xn}"> --%>
                                                          <input id="xn" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy',maxDate:'#F{$dp.$D(\'defaultYear\')}'})" value="${dto.xn}"/>
                                                    </td>
                                                    <td>任课状况：</td>
                                                    <td>
                                                         <select class="easyui-combobox" required="true" name="rkzk" id="rkzk" value="${dto.rkzk}">
                                                            <app:dictselect dictType="JSXX_RKZK" />
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>任课课程类别：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="rkkclb" id="rkkclb" value="${dto.rkkclb}" data-options="multiple:true,multiline:true">
                                                            <app:dictselect dictType="JSXX_RKKCLB" />
                                                        </select>
                                                    </td>
                                                    <td>任课学科类别：</td>
                                                    <td>
                                                        <input type="text" class="easyui-searchbox"  data-options="searcher:doSearch" required name="rkxklb1" id="rkxklb" value="${dto.rkxklb}">
                                                    	<input type="hidden"  name="rkxklb" >
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>平均每周课堂教学课时数：</td>
                                                    <td>
                                                         <input type="text"  class="easyui-numberbox" min="0" max="9999" precision="0" required="true" name="pjzks" id="pjzks" value="${dto.pjzks}">
                                                    </td>
                                                    <td>兼任工作：</td>
                                                    <td>
                                                    <select class="easyui-combobox" required="true" name="jrgz" id="jrgz" value="${dto.jrgz}">
                                                            <app:dictselect dictType="JSXX_JRGZ" />
                                                        </select>
                                                    </td>
                                                </tr>
												</c:if>
                                            </tbody>
                                        </table>
                                        <div id="dd" class="easyui-dialog" closed="true" buttons="#dlg-dd" data-options="modal:true"   style="width:40%;max-height:300px;overflow-y:auto;overflow-x:hidden">
                                        <c:if test="${ssxd=='10'}">
                                        	<input type="hidden" name="treeData"  data="${dto1}"/>
                                        	<ul id="tt" class="easyui-tree"></ul>
                                        	</c:if>
                                        	<c:if test="${ssxd=='20'}">
											<input type="hidden" name="treeData1" data="${dto2}"/>
                                        	<ul id="tt1" class="easyui-tree"></ul>
                                        	</c:if>
                                        	<c:if test="${ssxd=='40'}">
											<input type="hidden" name="treeData2" data="${rkxkdto}"/>
                                        	<ul id="tt2" class="easyui-tree" checkbox="true" ></ul>
                                        	</c:if>
                                        	<c:if test="${ssxd=='30'}">
											<input type="hidden" name="treeData3" data="${rjkczxxdto}"/>
                                        	<ul id="tt3" class="easyui-tree"></ul>
                                        	</c:if>
                                        	<c:if test="${ssxd=='50'}">
											<input type="hidden" name="treeData4" data="${rjkctjdto}"/>
                                        	<ul id="tt4" class="easyui-tree"></ul>
                                        	</c:if>
                                        </div>
                                        <div id="dlg-dd">
		                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveTree()">保存</a>
		                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dd').dialog('close')">取消</a>
		                                </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgJY').dialog('close')">取消</a>
                                </div>
                            </div>
                        </div>

			<div class="page tar mb15">
				<input name="pageNo" value="${pageNo }" type="hidden" />
				<c:if test="${not empty pageObj.pageElements}">
					<jsp:include page="../common/pager-nest.jsp">
						<jsp:param name="toPage" value="1" />
						<jsp:param name="showCount" value="1" />
						<jsp:param name="action" value="doSearch" />
					</jsp:include>
				</c:if>
			</div>
		</div>
		<div id="tips" class="easyui-dialog" title="温馨提示" style="padding:10px 20px;width:200px;height:100px;line-height:40px;text-align:center;overflow:auto;font-size:20px;" closed="true" data-options="modal:true">请选择一条记录</div>
		
		<!-- 新增 -->
		<!-- <div class="content" style="display: none;" id="addUser">
			<div class="modelbox modelbox3">
				<div class="rowbox mb10 wper15">
					<input type="submit" value="保存" class="btnyellow fl"
						onclick="return saveUser();" /> <input type="button" value="返回"
						class="btnyellow fr" onclick="return hideAdd();" />
				</div>
			</div>
		</div> -->
</form>
<c:if test="${SESSION_ISPUB == '1'}">
    	<div id="btn-wrap">
        <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="submitInfo()">报送</a>
        <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="cancelSubmit()">取消</a><span style="color:red">各项信息填写完整后，才能报送。报送后，不能再次修改教师信息。只有审核不通过的教师信息，才能再次编辑报送。</span>
    </div>
    </c:if>
<script type="text/javascript">

Array.prototype.indexOf = function(val) {
	for (var i = 0; i < this.length; i++) {
		if (this[i] == val) return i;
	}
	return -1;
};
Array.prototype.remove = function(val) {
	var index = this.indexOf(val);
	if (index > -1) {
		this.splice(index, 1);
	}
};

//fmx  新增加载树
function loadtree(){
	var dataStrKey = $('input[name="treeData"][type="hidden"],input[name="treeData1"][type="hidden"],input[name="treeData2"][type="hidden"],input[name="treeData3"][type="hidden"],input[name="treeData4"][type="hidden"]').attr('data').replace(/\'/g,'\"');
	 var dataStrKey = dataStrKey.replace(/(\d+)/g,'\"$1\"');
	 var data = JSON.parse(dataStrKey);
	 console.log(data);
	 $('#tt,#tt1,#tt2,#tt3,#tt4').tree({
		 data:data
	 });
}


var treeVal = [];
var treeId2 = [];
$('#tt2').tree({
	onCheck:function(node, checked){
		if(node.checked==false){
			treeVal.push(node.id+node.text);
			treeId2.push(node.id);
		}else{
			treeVal.remove(node.id+node.text);
			treeId2.remove(node.id);
		}
		
	}
})
//任教课程复选
var treeText = [];
var treeId = [];
$('#tt3,#tt4').tree({
	onCheck:function(node, checked){
		if(node.checked==false){
			treeText.push(node.text);
			treeId.push(node.id);
		}else{
			treeText.remove(node.text);
			treeId.remove(node.id);
		}
		console.log(treeText);
	}
});
function saveTree(){
	$('#dd').dialog('close');
	/* var treeValArr = treeVal + '';
	var treeValStr = treeValArr.replace(/\d+/g,''); */
	$('#rkxklb').searchbox('setValue',treeVal);
	$('input[name="rkxklb"][type="hidden"]').val(treeId2);
	$('#rjkc').searchbox('setValue',treeText);
	$('input[name="rjkc"][type="hidden"]').val(treeId);
	treeVal.splice(0,treeVal.length);
	treeText.splice(0,treeText.length);
	treeId.splice(0,treeId.length);
	treeId2.splice(0,treeId2.length);
}
$('#xq,#rkzk,#rkkclb').combobox({ editable:false }); //下拉框禁止输入
$('#xzycsxkly').searchbox({ editable:false }); //搜索框禁止输入

formatterDate = function(date) {
	var day = date.getDate() > 9 ? date.getDate() : "0" + date.getDate();
	var month = (date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0"
	+ (date.getMonth() + 1);
	return date.getFullYear() + '-' + month + '-' + day;
	};

function init(){
	
	$("#rkzk").combobox({
		onChange: function (n,o) {
				if(n == 20){
					$("#pjzks").numberbox("enable");
					$("#rjkc").searchbox("enable");
					$('#tt3').tree({checkbox:true});
					$('#tt4').tree({checkbox:true});
				}else{
					$("#pjzks").numberbox("setValue","");
					$("#pjzks").numberbox("disable");
					$("#rjkc").searchbox("disable");
				}
			}
		});
	
	$("#rjxd").combobox({
		onChange: function (n,o) {
				if(n == 0){
					$("#rkzk").combobox("setValue",10);
				}
			}
		});
}


 $(function(){
	init();
	
/* 	$.ajax({
		url:'${ctx}/basicInformation/EducationTeachAction.a&time='+new Date().getTime(),
		success:function(){
			console.log("success");
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
	}) */
})
function doSearch(){
	 $('#dd').dialog('open');
	 if(${ssxd}=='10'||${ssxd}=='20'){
		 $('#dd').prev().find('.panel-title').html('现主要从事学科领域');
	 }
	 if(${ssxd}=='30'){
		 $('#dd').prev().find('.panel-title').html('任教课程');
	 }
	 if(${ssxd}=='40'){
		 $('#dd').prev().find('.panel-title').html('任课学科类别');
	 }
	 if(${ssxd}=='50'){
		 $('#dd').prev().find('.panel-title').html('任教课程');
	 }
	 var dataStrKey = $('input[name="treeData"][type="hidden"],input[name="treeData1"][type="hidden"],input[name="treeData2"][type="hidden"],input[name="treeData3"][type="hidden"],input[name="treeData4"][type="hidden"]').attr('data').replace(/\'/g,'\"');
	 var dataStrKey = dataStrKey.replace(/(\d+)/g,'\"$1\"');
	 var data = JSON.parse(dataStrKey);
	 $('#tt,#tt1,#tt2,#tt3,#tt4').tree({
		 data:data
	 });
	 $('#tt,#tt1,#tt2,#tt3,#tt4').tree('collapseAll');
}

$('#tt,#tt1,#tt2,#tt3,#tt4').tree({
	onClick:function(node){
		$("#xzycsxkly,#rkxklb,#rjkc").searchbox('setValue', node.text);
		$('#dd').dialog('close');
		
		if(${ssxd}=='10'||${ssxd}=='20'){
			$('input[name="xzycsxkly"][type="hidden"]').val(node.id);
		 }
		 if(${ssxd}=='30'){
			 $('input[name="rjkc"][type="hidden"]').val(node.id);
		 }
		 if(${ssxd}=='40'){
			 $('input[name="rkxklb"][type="hidden"]').val(node.id);
		 }
		 if(${ssxd}=='50'){
			 $('input[name="rjkc"][type="hidden"]').val(node.id);
		 }
		
	}
})
//增加用户
function addUser() {
	window.location.href="${ctx}/EducationTeachAction.a?toAdd"
}
var url;
function save_user(dlgNmae){
    $(dlgNmae).dialog('open');
    $('#fm').form('clear');
    setDefaultYear();
    $("#xn").val(defaultYear);
	if($("#rkzk").combobox("getValue") == 20){
		$("#pjzks").numberbox("setValue",0);
	}
}




//提示信息
function tips(dlgNmae,msg){
	$(dlgNmae).html(msg).dialog('open');
}

//关闭dialog

function save_close(dlgNmae){
    $(dlgNmae).dialog('close');
}
//新增
function newUserSD(){
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
	     if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
	$('#rjxd').combobox({ editable:false }); //下拉框禁止输入
	$('#jrgz').combobox({ editable:false }); //下拉框禁止输入
	$('#rjkc').searchbox({ editable:false });
	$('#rkxklb').searchbox({ editable:false }); //搜索框禁止输入
	$('#dslb').combobox({ editable:false });
	$('#sfwbkssk').combobox({ editable:false });
	var d = new Date()
	var vYear = d.getFullYear();
	var vMon = d.getMonth() + 1;
	var vDay = d.getDate();
    $("#xn").val(vYear);
    $("#dslb").combobox('setValue',0);
    $("#rkzk").combobox('setValue',20); 
    save_user('#dlgJY');
		    }else{
				$.messager.alert('温馨提示','信息审核中或已通过审核，不可添加','warning');
			} 
	},
	error:function(XMLHttpRequest, textStatus, errorThrown){
		var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
		sessionTimeout(sessionstatus);
	}
	
})
}
//编辑
function editUser(){
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
	     if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
	if($("#xnbzkkcjxkss").length >0){
		var xnbzkkcjxkss=$("#xnbzkkcjxkss").numberbox("getValue");
	}
	var rkkclb=$("input[type='hidden'][name='rkkclb']").val();
	$('#rjxd').combobox({ editable:false }); //下拉框禁止输入
	$('#jrgz').combobox({ editable:false }); //下拉框禁止输入
	$('#rjkc').searchbox({ editable:false });
	$('#rkxklb').searchbox({ editable:false }); //搜索框禁止输入
	$('#dslb').combobox({ editable:false });
	$('#sfwbkssk').combobox({ editable:false });
	
// 	var xzycsxkly=$("#xzycsxkly").val();
// 	if (rkzk==20) {
// 		if(!xzycsxkly){
// 			$.messager.alert('温馨提示','现主要从事学科领域不能为空！','warning');
// 			return;
// 		}
// 		if(!xnbzkkcjxkss){
// 			$.messager.alert('温馨提示','课程教学课时数不能为空！','warning');
// 			return;
// 		}
// 		if (${ssxd}=='20'||${ssxd}=='40') {
// 			if(!rkkclb){
// 				$.messager.alert('温馨提示','任课课程类别不能为空！','warning');
// 				return;
// 			}
// 		}
// 	}
var selected=$("input[type='checkbox'][name='ck']:checked");
	if(selected.length<=0){
		$.messager.alert('温馨提示','请选择一条记录','warning');
		return ;
	}else if(selected.length>1){
		$.messager.alert('温馨提示','只能编辑一条记录','warning');
		return ;
	}
    /* var ckData = $('#dg').datagrid('getSelected');
   	$("#xn").datebox('setValue',ckData.id);
    $("#xq").combobox('setValue',ckData.shencha);
    $("#dslb").combobox('setValue',ckData.chachong);
    $("#xzycsxkly").searchbox('setValue',ckData.name);
    $("#sfwbkssk").combobox('setValue',ckData.named); 
    $("#rkzk").combobox('setValue',ckData.na); 
    $("#xnbzkkcjxkss").textbox('setValue',ckData.sex); 
    $('#dlgJY').dialog('open').dialog('setTitle','编辑用户'); */
	   var jyid = selected.val();
		  var row = $('#dg').datagrid('getSelected');
	      if (row){
	          
	          $('#fm').form('load',row);
	          $.ajax({
		      	url : '${ctx}/basicInformation/EducationTeachAction.a?doQuery&jyid='+jyid+'&time='+new Date().getTime(),
		      	dataType: 'json',
		      	success:function(data){
		      		$("#xn").val(data.dto.xn);
					$("#xq").combobox('setValue',data.dto.xq);//学期
					$("#dslb").combobox('setValue',data.dto.dslb);//导师类别
					//$("#xzycsxkly").searchbox('setValue',data.dto.xzycsxkly);//领域
					
					if(data.dto.xzycsxkly != ''){
						loadtree();
						if(${ssxd}=='10'){
							$("#xzycsxkly").searchbox('setValue',$('#tt').tree("find",data.dto.xzycsxkly).text);
					        $('input[name="xzycsxkly"][type="hidden"]').val(data.dto.xzycsxkly);
						 }
						if(${ssxd}=='20'){
							$("#xzycsxkly").searchbox('setValue',$('#tt1').tree("find",data.dto.xzycsxkly).text);
					        $('input[name="xzycsxkly"][type="hidden"]').val(data.dto.xzycsxkly);
						 }
					}
					$("#sfwbkssk").combobox('setValue',data.dto.sfwbkssk); 
					$("#rkzk").combobox('setValue',data.dto.rkzk); 
				    $("#xnbzkkcjxkss").numberbox('setValue',data.dto.xnbzkkcjxkss);
				    $("#rkkclb").combobox('setValues',data.dto.rkkclblist);
				  //  $('#cc').combobox('setValues', ['001','002']);
				    $("#rjxd").combobox('setValue',data.dto.rjxd); 
				    var rkxk=data.dto.rkxklb.split(",");
				    $("#rkxklb").searchbox('setValue',rkxk);
				    $('input[name="rkxklb"][type="hidden"]').val(data.dto.rkxklb.replace(/[\u4E00-\u9FA5]+/g,''));
				    /**
				    if(data.dto.rkxklb != ''){
						loadtree();
						if(${ssxd}=='40'){
							var treeTexts = [];
							 var rjvalue = data.dto.rkxklb;
							 if(rjvalue.indexOf(",")>=0){
								 var arr = rjvalue.split(",");
								 for(var i=0;i<arr.length;i++){
									 treeTexts.push($('#tt2').tree("find",arr[i]).text);
								 }
								 $("#rkxklb").searchbox('setValue',treeTexts);
								 $('input[name="rkxklb"][type="hidden"]').val(rjvalue);
								 treeTexts.splice(0,treeTexts.length);
							 }else{
								 $("#rkxklb").searchbox('setValue',$('#tt2').tree("find",data.dto.rkxklb).text);
							       $('input[name="rkxklb"][type="hidden"]').val(data.dto.rkxklb);
							 }
							
						 }
					}
				    */
				  //  $("#rjkc").searchbox('setValue',data.dto.rjkc); 
				    if(data.dto.rjkc != ''){
						loadtree();
						if(${ssxd}=='30'){
							var treeTexts = [];
							 var rjvalue = data.dto.rjkc;
							 if(rjvalue.indexOf(",")>=0){
								 var arr = rjvalue.split(",");
								 for(var i=0;i<arr.length;i++){
									 treeTexts.push($('#tt3').tree("find",arr[i]).text);
								 }
								 $("#rjkc").searchbox('setValue',treeTexts);
								 $('input[name="rjkc"][type="hidden"]').val(rjvalue);
								 treeTexts.splice(0,treeTexts.length);
							 }else{
								 $("#rjkc").searchbox('setValue',$('#tt3').tree("find",data.dto.rjkc).text);
						        $('input[name="rjkc"][type="hidden"]').val(data.dto.rjkc);
							 }
							
						 }
						 if(${ssxd}=='50'){
							 var treeTexts = [];
							 var rjvalue = data.dto.rjkc;
							 if(rjvalue.indexOf(",")>=0){
								 var arr = rjvalue.split(",");
								 for(var i=0;i<arr.length;i++){
									 treeTexts.push($('#tt4').tree("find",arr[i]).text);
								 }
								 $("#rjkc").searchbox('setValue',treeTexts);
								 $('input[name="rjkc"][type="hidden"]').val(rjvalue);
								 treeTexts.splice(0,treeTexts.length);
							 }else{
								 $("#rjkc").searchbox('setValue',$('#tt4').tree("find",data.dto.rjkc).text);
							     $('input[name="rjkc"][type="hidden"]').val(data.dto.rjkc);
							 }
							  
						 }
						
					}
				    $("#pjzks").numberbox('setValue',data.dto.pjzks); 
				    $("#jrgz").combobox('setValue',data.dto.jrgz); 
		      	}
		      });
	          
	          $('#dlgJY').dialog('open').dialog('setTitle','编辑用户');
	      }
    }else{
		$.messager.alert('温馨提示','信息审核中或已通过审核，不可编辑','warning');
	} 
},
error:function(XMLHttpRequest, textStatus, errorThrown){
	var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
	sessionTimeout(sessionstatus);
}

}) 
}
//删除
function removeUser(){
	var selected=$("input[type='checkbox'][name='ck']:checked");
	if(selected.length<=0){
		return $.messager.alert('温馨提示','请选择一条记录','warning');
	}
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		dataType : "json",
		success:function(data){
	     if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
 	 var strSel = '';
     $("input[name='ck']:checked").each(function(index, element) {
          strSel += $(this).val()+",";
      }); 
      var arrCk = strSel.substring(0,strSel.length-1); 
	$.messager.confirm('温馨提示', '请确认是否删除信息？', function(r){
		if (r){
			 $.messager.alert('温馨提示','删除成功','info',function(){
				window.location.href ="${ctx}/basicInformation/EducationTeachAction.a?delEducationTeach&ck="+arrCk;
				console.log('111');
			  	});
		}
	});
	     }else{
	 		$.messager.alert('温馨提示','信息审核中或已通过审核，不可删除','warning');
	 	} 
	 },
	 error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}

	 }) 
}
//获取选中行的索引
var rowIndexs = {};
$('#dg').datagrid({
	onSelect:function(rowIndex){
		rowIndexs.index=rowIndex;
	}
});
//保存
function saveUser(){
	
	var ckData = $('#dg').datagrid('getSelected');
/* 	var id = ckData.ck.match(/\d/g); */
if(ckData){
	var id = ckData.ck;	
}else{
	id="";
}
//var id = ckData.ck;
	/* var jsid = id[1];
    var khid = id[0]; */
    var selected=$("input[type='checkbox'][name='ck']:checked");
    var jyid = selected.val();
    xn=$("#xn").val();
	var xq="";
	var dslb="";
	var xzycsxkly="";
	var sfwbkssk="";
	var rkzk="";
	var xnbzkkcjxkss="";
	var cjsj="";
	var rkxklb="";
	var jrgz="";
	var rjkc="";
	var rjxd="";
	var rkkclb="";
	var pjzks="";
	var ssxd=${ssxd};
	if(!xn){
		$.messager.alert('温馨提示','年度不能为空！','warning');
		return;
	}
    if(${ssxd}=='10'){
    	    var selected=$("input[type='checkbox'][name='ck']:checked");
    	    var selecIndex = selected.parent().parent().parent().index();
    		xq=$("input[type='hidden'][name='xq']").val();
    	    dslb=$("input[type='hidden'][name='dslb']").val();
    		xzycsxkly=$("input[type='hidden'][name='xzycsxkly']").val();
    		sfwbkssk=$("input[type='hidden'][name='sfwbkssk']").val();
    		rkzk=$("input[type='hidden'][name='rkzk']").val();
    		xnbzkkcjxkss=$("#xnbzkkcjxkss").numberbox("getValue");
    		var rkzk=$('#rkzk').combobox('getValue');
    		if (rkzk==20) {
    			if(!xzycsxkly){
        			$.messager.alert('温馨提示','现主要从事学科领域不能为空！','warning');
        			return;
        		}
        		if(!xnbzkkcjxkss){
        			$.messager.alert('温馨提示','课程教学课时数不能为空！','warning');
        			return;
        		}
			}
    		cjsj=$("#cjsj").val();
    }
	if(${ssxd}=='20'){
		var selected=$("input[type='checkbox'][name='ck']:checked");
	    var selecIndex = selected.parent().parent().parent().index();
		xq=$("input[type='hidden'][name='xq']").val();
	    dslb=$("input[type='hidden'][name='dslb']").val();
		xzycsxkly=$("input[type='hidden'][name='xzycsxkly']").val();
		sfwbkssk=$("input[type='hidden'][name='sfwbkssk']").val();
		rkzk=$("input[type='hidden'][name='rkzk']").val();
		xnbzkkcjxkss=$("#xnbzkkcjxkss").numberbox("getValue");
		rkkclb=$("input[type='hidden'][name='rkkclb']").val();
		if (rkzk==20) {
			if(!xzycsxkly){
    			$.messager.alert('温馨提示','现主要从事学科领域不能为空！','warning');
    			return;
    		}
    		if(!xnbzkkcjxkss){
    			$.messager.alert('温馨提示','课程教学课时数不能为空！','warning');
    			return;
    		}
    		if (${ssxd}=='20'||${ssxd}=='40') {
    			if(!rkkclb){
    				$.messager.alert('温馨提示','任课课程类别不能为空！','warning');
    				return;
    			}
    		}
		}
		cjsj=$("#cjsj").val();
		
		//rkxklb=$("input[type='hidden'][name='rkxklb']").val();
		var dzzwlh = $("input[name='rkkclb']").length;
		var rkkclb="";
		 for(var k=0;k<dzzwlh;k++){
			 rkkclb+=$("input[name='rkkclb']").eq(k).val()+",";
		 }

		$('#cc').combobox('setValues', ['001','002']);
	    }
	if(${ssxd}=='30'||${ssxd}=='50'){
		var selected=$("input[type='checkbox'][name='ck']:checked");
	    var selecIndex = selected.parent().parent().parent().index();
		xq=$("input[type='hidden'][name='xq']").val();
		rkzk=$("input[type='hidden'][name='rkzk']").val();
		rjxd=$("input[type='hidden'][name='rjxd']").val();
		rjkc=$("input[name='rjkc'][type='hidden']").val();
		pjzks=$("input[type='hidden'][name='pjzks']").val();
		jrgz=$("input[type='hidden'][name='jrgz']").val();
		
		if(rjxd == 0 && rkzk==20){
			$.messager.alert('温馨提示','任课状况应为未任课！','warning');
			return;
		}
		
		if (rkzk==20) {
			if(!rjkc){
				$.messager.alert('温馨提示','任教课程不能为空！','warning');
				return;
			}
			if(!pjzks){
				$.messager.alert('温馨提示','平均每周课堂教学课时数不能为空！','warning');
				return;
			}
		}
	}
	if(${ssxd}=='40'){
		rkzk=$("input[type='hidden'][name='rkzk']").val();
		rkkclb=$("input[type='hidden'][name='rkkclb']").val();
		rkxklbRe = $("input[type='hidden'][name='rkxklb']").val();
		
		rkxklb = rkxklbRe;
// 		rkxklb = rkxklbRe.replace(/[\u4E00-\u9FA5]+/g,'');
		
		var rkzk=$('#rkzk').combobox('getValue');
		if (rkzk==20) {
			if(!rkkclb){
				$.messager.alert('温馨提示','任课课程类别不能为空！','warning');
				return;
			}
			if(!rkxklb){
				$.messager.alert('温馨提示','任课学科类别不能为空！','warning');
				return;
			}
			
		}
		pjzks=$("input[type='hidden'][name='pjzks']").val();
		jrgz=$("input[type='hidden'][name='jrgz']").val();
		rkkclb=$("input[type='hidden'][name='rkkclb']").val();
		//rkxklb=$("input[type='hidden'][name='rkxklb']").val();
		if(!pjzks){
			$.messager.alert('温馨提示','平均每周课堂教学课时数不能为空！','warning');
			return;
		}
		var dzzwlh = $("input[name='rkkclb']").length;
		var rkkclb="";
		 for(var k=0;k<dzzwlh;k++){
			 rkkclb+=$("input[name='rkkclb']").eq(k).val()+",";
		 }
	}
	$.messager.confirm('温馨提示', '请确认是否要保存信息？', function(r){
		if (r){
			if(id&&id!=""){
				$('#fm').form('submit',{
			        url: "${ctx}/basicInformation/EducationTeachAction.a?editEducationTeach&jyid="+jyid+"&xn="+xn+"&xq="+xq+"&dslb="+dslb+"&xzycsxkly="+xzycsxkly+"&sfwbkssk="+sfwbkssk+"&rkzk="+rkzk+"&xnbzkkcjxkss="+xnbzkkcjxkss+"&cjsj="+cjsj+"&rkxklb="+rkxklb+"&jrgz="+jrgz+"&rjkc="+rjkc+"&rjxd="+rjxd+"&rkkclb="+rkkclb+"&pjzks="+pjzks,
			        onSubmit: function(){
			            return $(this).form('validate');
			        },
			        success: function(result){
		            	save_close('#dlgJY');      // close the dialog
		            	$.messager.alert('温馨提示','编辑成功','info',function(){
		        	      	window.location.reload();
		        		  	});
			        },
			        onLoadSuccess:function(){

			            var result = eval('('+result+')');
			            $('#dlg').dialog('close');   
			            if (result.success){
			                $('#dlg').dialog('close');      // close the dialog
			                $('#dg').datagrid('reload');    // reload the user data
			            } else {
			                $.messager.show({
			                    title: 'Error',
			                    msg: result.msg
			                });
			            }
			        }
			    }); 
				
			}else{
		     $('#fm').form('submit',{
		        url: "${ctx}/basicInformation/EducationTeachAction.a?addEducationTeach&jyid="+jyid+"&xn="+xn+"&xq="+xq+"&dslb="+dslb+"&xzycsxkly="+xzycsxkly+"&sfwbkssk="+sfwbkssk+"&rkzk="+rkzk+"&xnbzkkcjxkss="+xnbzkkcjxkss+"&cjsj="+cjsj+"&rkxklb="+rkxklb+"&jrgz="+jrgz+"&rjkc="+rjkc+"&rjxd="+rjxd+"&rkkclb="+rkkclb+"&pjzks="+pjzks,
		        onSubmit: function(){
		            return $(this).form('validate');
		        },
		        success: function(result){
		        	save_close('#dlgJY');      // close the dialog
		        	$.messager.alert('温馨提示','保存成功','info',function(){
		    	      	window.location.reload();
		    		  	}); 
		        },
		        onLoadSuccess:function(){
		        }
		    }); 
		     
		}
		}
	});

	
}
function tips(dlgNmae,msg){
	$(dlgNmae).html(msg).dialog('open');
}

/**
 *报送
	*/
	function submitInfo(){
 		 
				$.ajax({
					url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
					data:{},
					dataType : "json",
					success:function(data){
						    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
						    	$.ajax({
									url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?submitted&time='+new Date().getTime(),
									data:{},
									success:function(data){
										$.messager.alert('温馨提示','报送成功','info',function(){
											window.parent.location.reload();
							    	  	});
									},
									error:function(data){
										$.messager.alert('温馨提示','报送失败,请检查各项是否都已填写！','warning');
									}
									
								});   
							}else{
								$.messager.alert('温馨提示','信息已报送，不可重复报送！','warning');
							}  
					},
					error:function(XMLHttpRequest, textStatus, errorThrown){
						var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
						sessionTimeout(sessionstatus);
					}
					
				})
 		 
 	 }
 /**
 *取消报送
	*/
	function cancelSubmit(){
		 
		$.ajax({
			url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
			data:{},
			dataType : "json",
			success:function(data){
				    if(data.tbJzgxxsh.shzt==02){
				    	$.ajax({
							url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?cancelSubmit&time='+new Date().getTime(),
							data:{},
							success:function(data){
								$.messager.alert('温馨提示','取消成功','info',function(){
					            	
					    	  	});
							},
							error:function(data){
							
							}
							
						});  
					}else{
						$.messager.alert('温馨提示','信息审核中(审核通过)或未报送，不可取消保送！','warning');
					}  
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
			
		})
			
 	 }
	$('#dg').datagrid({
		onLoadSuccess:function(data){
			if(data.rows[0].id==undefined){
				$('#datagrid-row-r3-2-0').html(data.rows[0].ck);
				$('.datagrid-btable').css({'text-align':'center','width':'100%','padding':'30px 0'});
			}
		}
	});
	//是否双肩挑事件
	$('#rkzk').combobox({
	    	onSelect:function(record){
	    	var rkzk=$('#rkzk').combobox('getValue');
	    	if(rkzk=='20'){
	    		$('#rkkclb').combobox("enable");
	    		$("#xzycsxkly").searchbox("enable");
	    		$("#xnbzkkcjxkss").numberbox("enable"); 
	    		$("#xnbzkkcjxkss").numberbox('setValue',0); 
	    		$("#rjkc").searchbox("enable");
	    		$("#rkxklb").searchbox("enable");
	    	}else{
	    		$('#rkkclb').combobox("clear");
	    		$('#rkkclb').combobox("disable");
	    		$('#xzycsxkly').searchbox("clear");
	    		$("#xzycsxkly").searchbox("disable");
	    		$("#xnbzkkcjxkss").numberbox("clear"); 
	    		$("#xnbzkkcjxkss").numberbox("disable"); 
	    		$('#rjkc').searchbox("clear");
	    		$("#rjkc").searchbox("disable");
	    		$("input[name='rjkc'][type='hidden']").val('');
	    		$('#rkxklb').searchbox("clear");
	    		$("#rkxklb").searchbox("disable");
	    	}
	    }
	});
	

</script>
     
</body>
</html>
