<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>北京市教师管理服务平台</title>
<link rel="stylesheet" type="text/css" href="../css/newcss.css">
<link rel="stylesheet" type="text/css"
	href="../easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
<script type="text/javascript" src="../easyui/jquery.min.js"></script>
<script src="../js/echarts/build/dist/echarts-all.js"></script>
<script src="../js/echarts/build/dist/echarts.js"></script>
<script type="text/javascript" src="../easyui/easyloader.js"></script>
<script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${ctx}/js/refresh.js"></script>
<script type="text/javascript" src="../My97DatePicker/WdatePicker.js"></script>
<link rel="stylesheet" type="text/css" href="../My97DatePicker/skin/WdatePicker.css">
<script type="text/javascript">
var myDate = new Date();
var defaultYear = '${preYear}';  ///业务上的默认年

$(function(){
	$("#defaultYear").val('${preYear}');
});
</script>
</head>

<div id="anchor4">
	<div data-options="region:'center',border:false,collapsible:true"
		style="margin-bottom: 20px;">
		<form action="${ctx}/basicInformation/BasicTreatManagerAction.a" id="fm" method="post" novalidate>
		<div id="toolbarDY">
			<a class="easyui-linkbutton" iconCls="icon-add" plain="true"
				onclick="newUserSD()">新 增</a> <a href="#"
				class="easyui-linkbutton" iconCls="icon-edit" plain="true"
				onclick="editUser()">编 辑</a> <a href="#" class="easyui-linkbutton"
				iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a>

		</div>
		 <input type="hidden" value="${tbJzgxxsh.shzt}" id="xxshzt"/>
		 <input type="hidden" id="defaultYear"/>
		<table id="dg" class="easyui-datagrid" title="基本待遇"
			iconCls="icon-dingdan"
			data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,collapsible:true,toolbar:'#toolbarDY',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
			<thead>
				<tr>
					<th field="ck" checkbox="true"></th>
					<th data-options="field:'nd',width:8">年度</th>
					<th data-options="field:'id',width:8">年工资收入（元）</th>
					<th data-options="field:'shencha',width:8">基本工资(元/月)</th>
					<th data-options="field:'namrtred',width:9">绩效工资(元/月)</th>
					<c:choose>
						<c:when test="${ssxd==30}">
							<th data-options="field:'xcjtbt',width:10">乡村教师生活补助(元/月)</th>
							<th data-options="field:'qitabt',width:10">其他津贴补贴(元/月)</th>
						</c:when>
						<c:when test="${ssxd==40||ssxd==60}">
							<th data-options="field:'jtbt',width:10">津贴补贴(元/月)</th>
						</c:when>
						<c:when test="${ssxd==50}">
							<th data-options="field:'tjjtbt',width:10">特教津贴补贴(元/月)</th>
							<th data-options="field:'qtjtbt',width:10">其他津贴补贴(元/月)</th>
						</c:when>
					</c:choose>
					<th data-options="field:'qt',width:10">其他(元/月)</th>
					<th data-options="field:'wxyj',width:10">五险一金</th>
					<th data-options="field:'cjsj',width:10">创建时间</th>
				</tr>
			</thead>
			<tbody>
				<c:if test="${empty pageObj.pageElements}">
					<tr>
						<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
					</tr>
				</c:if>
				<c:forEach items="${pageObj.pageElements}" var="item"
					varStatus="status">
					<tr>
						<%-- <th field="ck" checkbox="true" name="ck" value="${item.jbdyId}">${item.jbdyId}</th> --%>
						<td>${item.jbdyId}</td>
						<td>${item.nd}</td>
						<td>${item.ngzsr}</td>
						<td>${item.jbgz}</td>
						<td>${item.jxgz}</td>
						<c:choose>
							<c:when test="${ssxd==30}">
								<td>${item.xcjsshbz}</td>
								<td>${item.jtbt}</td>
							</c:when>
							<c:when test="${ssxd==40||ssxd==60}">
								<td>${item.jtbt }</td>
							</c:when>
							<c:when test="${ssxd==50}">
							<td>${item.tjjtbt}</td>
							<td>${item.jtbt}</td>
							</c:when>
						</c:choose>
						<td>${item.qt}</td>
						<td><c:forEach items="${item.wxyjList}" var="next" varStatus="indexNo">
							 <app:dictname dictid="JSXX_WXYJ${next}" />
							</c:forEach></td>
						<td>${item.cjsj}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
	<div class="page tar mb15">
		<input name="pageNo" value="${pageNo }" type="hidden" />
		<c:if test="${not empty pageObj.pageElements}">
			<jsp:include page="../common/pager-nest.jsp">
				<jsp:param name="toPage" value="1" />
				<jsp:param name="showCount" value="1" />
				<jsp:param name="action" value="doSearch" />
			</jsp:include>
		</c:if>
	</div>
</form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok"
			onclick="saveUser()" id="save">保存</a> <a href="#"
			class="easyui-linkbutton mr20" iconCls="icon-cancel"
			onclick="javascript:$('#dlgDY').dialog('close')">取消</a>
	</div>
</div>
</div>

<div id="dlgDY" class="easyui-dialog" title="新增基本待遇" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
<form id="fom1" method="post" novalidate>
		<div class="right_table">
			<div id="table-content">
					<table class="fitem clean">
					 <c:choose>
						<c:when test="${ssxd==30}">
							<tbody>
								<tr>
									<td>年度：</td>
									<td><input  class="Wdate" type="text" name="nd"  id="nd" onfocus="WdatePicker({dateFmt:'yyyy',maxDate:'#F{$dp.$D(\'defaultYear\')}'})"/></td>
									<td>年工资收入（元）：</td>
									<td><input type="text" class="easyui-numberbox" name="ngzsr" id="ngzsr" min="0" max="999999999999" precision="2"  required ></td>
								</tr>
								<tr>
									<td>基本工资(元/月)：</td>
									<td><input type="text" class="easyui-numberbox" name="jbgz" id="jbgz" min="0" max="999999999999" precision="2"  required ></td>
									<td>绩效工资(元/月)：</td>
									<td><input type="text" class="easyui-numberbox" name="jxgz"
										id="jxgz" min="0" max="999999999999" precision="2"  required ></td>
								</tr>
								<tr>
									<td>乡村教师生活补助(元/月)：</td>
									<td><input type="text" class="easyui-numberbox"
										name="xcjsshbz" id="xcjsshbz" min="0" max="999999999999" precision="2"  required ></td>
									<td>其他津贴补贴(元/月)：</td>
									<td><input type="text" class="easyui-numberbox" name="jtbt"
										id="jtbt" min="0" max="999999999999" precision="2"  required ></td>
								</tr>
								<tr>
									<td>其他(元/月)：</td>
									<td><input type="text" class="easyui-numberbox" name="qt"
										id="qt" min="0" max="999999999999" precision="2"  required ></td>
									<td>五险一金：</td>
									<td>
										<select class="easyui-combobox" required="true" name="wxyj" id="wxyj" data-options="multiple:true,multiline:true">
												<app:dictselect dictType="JSXX_WXYJ" />
										</select>
									</td>
								</tr>
								<input type="hidden" name="jbdyId" id="jbdyId">
							</tbody>
					 	</c:when>
					 	
					 	<c:when test="${ssxd==40||ssxd==60}">
							<tbody>
								<tr>
									<td>年度：</td>
									<td><input  class="Wdate" type="text" name="nd"  id="nd" onfocus="WdatePicker({dateFmt:'yyyy',maxDate:'#F{$dp.$D(\'defaultYear\')}'})"/></td>
									<td>年工资收入（元）：</td>
									<td><input type="text" class="easyui-numberbox" name="ngzsr" id="ngzsr" min="0" max="999999999999" precision="2"  required ></td>
								</tr>
								<tr>
									<td>基本工资(元/月)：</td>
									<td><input type="text" class="easyui-numberbox" name="jbgz" id="jbgz" min="0" max="999999999999" precision="2"  required ></td>
									<td>绩效工资(元/月)：</td>
									<td><input type="text" class="easyui-numberbox" name="jxgz" id="jxgz" min="0" max="999999999999" precision="2"  required ></td>
								</tr>
								<tr>
									<td>津贴补贴(元/月)：</td>
									<td><input type="text" class="easyui-numberbox" name="jtbt" id="jtbt" min="0" max="999999999999" precision="2"  required ></td>
										
									<td>其他(元/月)：</td>
									<td><input type="text" class="easyui-numberbox" name="qt" id="qt" min="0" max="999999999999" precision="2"  required ></td>
								</tr>
								<tr>
									<td>五险一金：</td>
									<td>
										<select class="easyui-combobox" required="true" name="wxyj" id="wxyj" data-options="multiple:true,multiline:true">
												<app:dictselect dictType="JSXX_WXYJ" />
										</select>
									</td>
								</tr>
								<input type="hidden" name="jbdyId" id="jbdyId">
							</tbody>
					 	</c:when>
					 	
					 	<c:when test="${ssxd==50}">
							<tbody>
								<tr>
									<td>年度：</td>
									<td><input  class="Wdate" type="text" name="nd"  id="nd" onfocus="WdatePicker({dateFmt:'yyyy',maxDate:'#F{$dp.$D(\'defaultYear\')}'})"/></td>
									<td>年工资收入（元）：</td>
									<td><input type="text" class="easyui-numberbox" name="ngzsr" id="ngzsr" min="0" max="999999999999" precision="2"  required ></td>
								</tr>
								<tr>
									<td>基本工资(元/月)：</td>
									
									<td><input type="text" class="easyui-numberbox" name="jbgz" id="jbgz" min="0" max="999999999999" precision="2"  required ></td>
									<td>绩效工资(元/月)：</td>
									<td><input type="text" class="easyui-numberbox" name="jxg" id="jxgz" min="0" max="999999999999" precision="2"  required ></td>
								</tr>
								<tr>
									<td>特教津贴补贴(元/月)：</td>
									<td><input type="text" class="easyui-numberbox" name="tjjtbt" id="tjjtbt" min="0" max="999999999999" precision="2"  required ></td>
									
									<td>其他津贴补贴(元/月)：</td>
									<td><input type="text" class="easyui-numberbox" name="jtbt" id="jtbt" min="0" max="999999999999" precision="2"  required ></td>
								</tr>	
								<tr>
									<td>其他(元/月)：</td>
									<td><input type="text" class="easyui-numberbox" name="qt" id="qt" min="0" max="999999999999" precision="2"  required ></td>
									<td>五险一金：</td>
									<td>
										<select class="easyui-combobox" required="true" name="wxyj" id="wxyj" data-options="multiple:true,multiline:true">
												<app:dictselect dictType="JSXX_WXYJ" />
										</select>
									</td>
								</tr>
								<input type="hidden" name="jbdyId" id="jbdyId">
							</tbody>
					 	</c:when>
					</c:choose>
					</table>
			</div>
		</div>
</form>
<div id="tips" class="easyui-dialog" title="温馨提示" style="padding:10px 20px;width:200px;height:100px;line-height:40px;text-align:center;overflow:auto;font-size:20px;" closed="true" data-options="modal:true">请选择一条记录</div>
	</div>

<c:if test="${SESSION_ISPUB == '1'}">
    	<div id="btn-wrap">
        <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="submitInfo()">报送</a>
        <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="cancelSubmit()">取消</a><span style="color:red">各项信息填写完整后，才能报送。报送后，不能再次修改教师信息。只有审核不通过的教师信息，才能再次编辑报送。</span>
    </div>
    </c:if>
<script type="text/javascript">
$('#dg').datagrid({
	onLoadSuccess:function(data){
		if(data.rows[0].id==undefined){
			$('#datagrid-row-r2-2-0').html(data.rows[0].ck);
			$('.datagrid-btable').css({'text-align':'center','width':'100%','padding':'30px 0'});
		}
	}
});

	$(function() {
		$.ajax({
			url : '${ctx}/basicInformation/BasicTreatManagerAction.a?doSearch&time='+new Date().getTime(),
			success : function() {
				console.log("success");
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
		})
	})

	//新增
	function newUserSD() {
		$.ajax({
			url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
			data:{},
			dataType : "json",
			success:function(data){
		     if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22||data.tbJzgxxsh.shzt==02){
		   save_user('#dlgDY');
		     }else{
					$.messager.alert('温馨提示','信息审核中或已通过审核，不可添加','warning');
			}  
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
		
	}) 
	}

	function save_user(dlgNmae) {
		$(dlgNmae).dialog('open');
		$('#fom1').form('clear');
		
		$("#nd").val(defaultYear);
		$('#wxyj').combobox({ editable:false }); //下拉框禁止输入
		$('#ngzsr,#jtbt,#jxgz,#tjjtbt,#jbgz,#qt,#xcjsshbz').numberbox('setValue',0);  
	}

	function saveUser() {
		var jbdyId = $("#jbdyId").val();
		var wxyj = $("input[name='wxyj']").val();
		var wxyjVal="";
		
		 var wxyjlh = $("input[name='wxyj']").length;
		 var wxyjVal="";
		 for(var k=0;k<wxyjlh;k++){
			 wxyjVal+=$("input[name='wxyj']").eq(k).val()+",";
		 }
		 wxyjVal=wxyjVal.substring(0,wxyjVal.length-1);
		 $("input[name=wxyj]").val(wxyjVal); 
		 
		 var nd = $("#nd").val();
			if(!nd){
				return $.messager.alert('温馨提示','请选择年度','warnning');
			}
			
		if (jbdyId) {
			$('#fom1').form('submit',{
								url : "${ctx}/basicInformation/BasicTreatManagerAction.a?toUpdateTreat",
								onSubmit : function() {
									return $(this).form('validate');
								},
								success : function(result) {
									$.messager.alert('温馨提示','修改成功','info',function(){
										window.location.reload();
								  	});
								}
							});
		} else {$('#fom1').form(
							'submit',
							{
								url : "${ctx}/basicInformation/BasicTreatManagerAction.a?SubAddTreat",
								onSubmit : function() {
									return $(this).form('validate');
								},
								success : function(result) {
									/* $.messager.alert('温馨提示','新增成功','warning'); */
									$.messager.alert('温馨提示','添加成功','info',function(){
										window.location.reload();
								  	});
								}
							});
		}
		save_close('#dlgDY');
	}
	//回显
	function editUser() {
		$.ajax({
			url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
			data:{},
			dataType : "json",
			success:function(data){
		     if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22||data.tbJzgxxsh.shzt==02){
		var selected = $("input[type='checkbox'][name='ck']:checked");
		if (selected.length <= 0) {
			$.messager.alert('温馨提示', '请选择一条记录', 'warning');
			//tips('#tips','请选择一条记录');
			return;
		} else if (selected.length > 1) {
			$.messager.alert('温馨提示', '只能编辑一条记录', 'warning');
			//tips('#tips','只能编辑一条记录');
			return;
		}
		var strSel = selected.val();
		var row = $('#dg').datagrid('getSelected');
		if (row) {
			$('#dlgDY').dialog('open').dialog('setTitle', '编辑基本待遇');
			
			$('#wxyj').combobox({ editable:false }); //下拉框禁止输入
			
			
			$('#form1').form('load', row);
			$.ajax({
					url : '${ctx}/basicInformation/BasicTreatManagerAction.a?editbasicTreat&jbdyId='+strSel+'&time='+new Date().getTime(),
					dataType : 'json',
					success : function(data) {
						$("#jbdyId").val(data.dto.jbdyId);
						$("#nd").val(data.dto.nd);
						$("#ngzsr").textbox('setValue',data.dto.ngzsr);
						$("#jbgz").textbox('setValue',data.dto.jbgz); 
						$("#jxgz").textbox('setValue',data.dto.jxgz);
						$("#xcjsshbz").textbox('setValue',data.dto.xcjsshbz);
						$("#jtbt").textbox('setValue',data.dto.jtbt);
						$("#qt").textbox('setValue',data.dto.qt);
						$("#tjjtbt").textbox('setValue',data.dto.tjjtbt);
						$("#wxyj").combobox('setValues',data.dto.wxyj);//五险一金   
					}
				});
		}
		     }else{
					$.messager.alert('温馨提示','信息审核中或已通过审核，不可编辑','warning');
			}  
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
		
	}) 
	}
	//删除
	function removeUser() {
		var selected = $("input[type='checkbox'][name='ck']:checked");
		if (selected.length <= 0) {
			return  $.messager.alert('温馨提示', '请选择一条记录', 'warning');
		}
		$.ajax({
			url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
			data:{},
			dataType : "json",
			success:function(data){
		     if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22||data.tbJzgxxsh.shzt==02){
		var strSel = '';
		$("input[name='ck']:checked").each(function(index, element) {
			strSel += $(this).val() + ",";
		});
		var cks = strSel.substring(0, strSel.length - 1)
		$.messager.confirm('温馨提示', '是否删除？', function(r){
	    	 		if (r){
	     			$.messager.alert('温馨提示','删除成功','info',function(){
						window.location.href = '${ctx}/basicInformation/BasicTreatManagerAction.a?delBasicTreat&ck='+cks;
	     		  	});
	     		}
	     	});
	    }else{
			$.messager.alert('温馨提示','信息审核中或已通过审核，不可删除','warning');
	}  
},
error:function(XMLHttpRequest, textStatus, errorThrown){
	var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
	sessionTimeout(sessionstatus);
}

})  
	}
	
    /**
	    *报送
		*/
		function submitInfo(){
	    		 
					$.ajax({
						url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
						data:{},
						dataType : "json",
						success:function(data){
							    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
							    	$.ajax({
										url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?submitted&time='+new Date().getTime(),
										data:{},
										success:function(data){
											$.messager.alert('温馨提示','报送成功','info',function(){
												window.parent.location.reload();
								    	  	});
										},
										error:function(data){
											$.messager.alert('温馨提示','报送失败,请检查各项是否都已填写！','warning');
										}
										
									});   
								}else{
									$.messager.alert('温馨提示','信息已报送，不可重复报送！','warning');
								}  
						},
						error:function(XMLHttpRequest, textStatus, errorThrown){
							var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
							sessionTimeout(sessionstatus);
						}
						
					})
	    		 
	    	 }
	    /**
	    *取消报送
		*/
		function cancelSubmit(){
			 
			$.ajax({
				url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
				data:{},
				dataType : "json",
				success:function(data){
					    if(data.tbJzgxxsh.shzt==02){
					    	$.ajax({
								url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?cancelSubmit&time='+new Date().getTime(),
								data:{},
								success:function(data){
									$.messager.alert('温馨提示','取消成功','info',function(){
						            	
						    	  	});
								},
								error:function(data){
								
								}
								
							});  
						}else{
							$.messager.alert('温馨提示','信息审核中(审核通过)或未报送，不可取消保送！','warning');
						}  
				},
				error:function(XMLHttpRequest, textStatus, errorThrown){
					var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
					sessionTimeout(sessionstatus);
				}
				
			})
				
	    	 }
	
	//提示信息
	function tips(dlgNmae,msg){
		$(dlgNmae).html(msg).dialog('open');
	}
	//关闭dialog
	 function save_close(dlgNmae){
	     $(dlgNmae).dialog('close');
	 }
	 $('#wxyj').combobox({
			onSelect:function(record){
				var v = $('#wxyj').combobox("getValue");
				if(v == '0'){
					$('#wxyj').combobox("setValue",record.value);
				}
			if(record.value == '0'){
				$('#wxyj').combobox("clear");
				$('#wxyj').combobox("setValue",record.value);
			}
		}
		});
</script>
