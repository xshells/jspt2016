<div id="anchor130">
                            <div data-options="region:'center',border:false,collapsible:true" style="margin-bottom:20px;">
                            <form id="frm" method="post" action="" novalidate >
                                <div id="toolbarJL">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUserJL()">新 增</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">编 辑</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a>
                                </div>
                                <table id="dg" class="easyui-datagrid" title="交流轮岗" iconCls="icon-dingdan" style="width:100%;height:370px;"
                                    data-options="idField:'id',rownumbers:false,fitColumns:true,pagination:true,singleSelect:false,collapsible:true,toolbar:'#toolbarJL',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
                                <thead>
                                    <tr>
                                        <th field="ck" checkbox="true"></th>
                                        <th data-options="field:'id',width:100">交流轮岗类型</th>
                                        <th data-options="field:'sheewrncha',width:100">是否调动人事关系</th>
                                        <th data-options="field:'shewencdsha',width:100">开始年月</th>
                                        <th data-options="field:'shensdfcha',width:100">原单位名称</th>
                                        <th data-options="field:'shengfgcha',width:100">交流轮岗单位名称</th>
                                        <th data-options="field:'chacerhong',width:100">创建时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test="${not empty pageObj.pageElements}">
									<c:forEach items="${pageObj.pageElements}" var="item" varStatus="status">
										<tr>
											<td>${item.exchangeRotationId}</td>
											<td><app:dictname dictid="${item.exchangeRotationType}"/></td>
											<td>
												<c:choose>
													<c:when test="${item.perMobilization=='0'}">
														否
													</c:when>
													<c:when test="${item.perMobilization=='1'}">
														是
													</c:when>
													<c:otherwise>
														是否人事调动状态有误
													</c:otherwise>
												</c:choose>
											</td>
											<td>${item.beginYearM}</td>
											<td>${item.beforeCompany}</td>
											<td>${item.nowCompany}</td>
											<td>${item.createTime}</td>
										</tr>
									</c:forEach>
								</c:if>
								<c:if test="${empty pageObj.pageElements}">
									<tr>
										<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
									</tr>
								</c:if>
                                </tbody>
                                </table>
                                <!-- 分页所必须的 -->
								<%-- <div class="page tar mb15">
									<input name="pageNo" value="${pageNo }" type="hidden" />
									<c:if test="${not empty pageObj.pageElements}">
										<jsp:include page="../../common/pager-nest.jsp">
											<jsp:param name="toPage" value="1" />
											<jsp:param name="showCount" value="1" />
											<jsp:param name="action" value="doSearch" />
										</jsp:include>
									</c:if>
								</div> --%>
								</form>
                                <!-- 新增或者编辑交流轮岗页面   开始 -->
                                <div id="dlgJL" class="easyui-dialog" title="新增交流轮岗" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        <div id="table-content">
                                        <form id="addfm" method="post" action="" novalidate>
                                        <input type="hidden" name="exRotInfoDto.exchangeRotationId" class="easyui-textbox" id="exchangeRotationIdEdit"/>
                                        <table class="fitem clean">
                                            <tbody>
                                                <tr>
                                                    <td>交流轮岗类型：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="exRotInfoDto.exchangeRotationType" id="exchangeRotationType">
                                                            <option value="">请选择</option>
                                                            <app:dictselect dictType="JSXX_JLLGLX" />
                                                        </select>
                                                    </td>
                                                    <td>是否调动人事关系：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="exRotInfoDto.perMobilization" id="perMobilization">
                                                            <option value="">请选择</option>
                                                            <option value="0">否</option>
                                                            <option value="1">是</option>
                                                            
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>开始年月：</td>
                                                    <td>
                                                        <input type="text" class="easyui-datebox"  name="exRotInfoDto.beginYearM" id="beginYearM" required />
                                                    </td>
                                                    <td>原单位名称：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="exRotInfoDto.beforeCompany" id="beforeCompany" required />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>交流轮岗单位名称：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox"  name="exRotInfoDto.nowCompany" id="nowCompany" required />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- 新增或者编辑交流轮岗页面   结束-->
                                <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgJL').dialog('close')">取消</a>
                                </div>
                            </div>
                        </div>