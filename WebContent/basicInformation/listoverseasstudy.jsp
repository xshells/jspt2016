<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>北京市教师管理服务平台</title>
<link rel="stylesheet" type="text/css" href="../css/newcss.css">
<link rel="stylesheet" type="text/css"
	href="../easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
<script type="text/javascript" src="../easyui/jquery.min.js"></script>
<script src="../js/echarts/build/dist/echarts-all.js"></script>
<script src="../js/echarts/build/dist/echarts.js"></script>
<script type="text/javascript" src="../easyui/easyloader.js"></script>
<script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="../easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${ctx}/js/refresh.js"></script>

</head>
<body>
	
	<div id="anchor12">
		<div data-options="region:'center',border:false,collapsible:true"
			style="margin-bottom: 20px;">
			<div id="toolbarGN">
				<a href="#" class="easyui-linkbutton" iconCls="icon-add"
					plain="true" onclick="newUserGN()">新 增</a> 
				<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true"
					onclick="editOverseas()">编 辑</a> 
				<a href="#" class="easyui-linkbutton"
					iconCls="icon-remove" plain="true" onclick="removeOverseas()">删 除</a>
					<span style="color:red">注意：海外学历不在此填写，时长三个月以上的。</span>
			</div>
			<form action="${ctx}/basicInformation/OverseasStudyAction.a"
				id="formOverseas" method="post">
				  <input type="hidden" value="${tbJzgxxsh.shzt}" id="xxshzt"/>
				<table id="dg" class="easyui-datagrid" title="海外研修(访学)"
					iconCls="icon-dingdan"
					data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,collapsible:true,toolbar:'#toolbarGN',method:'get',checkOnSelect:'true',selectOnCheck:'true'">

					<thead>
						<tr>
							<th field="ck" checkbox="true"></th>
							<th data-options="field:'id',width:100">是否有海外研修经历</th>
							<th data-options="field:'cjsj',width:100">创建时间</th>
						</tr>
					</thead>
					<tbody>
						<c:if test="${not empty pageObj.pageElements }">
							<c:forEach items="${pageObj.pageElements}" var="item"
								varStatus="status">
								<tr>
									<td>${item.hwyxId}</td>
									<td><app:dictname dictid="JSXX_SFJYHWYXJL${item.sfjyhwyxjl}" />
									</td>
									<td>${item.cjsj }</td>
								</tr>
							</c:forEach>
						</c:if>
						<c:if test="${empty pageObj.pageElements}">
							<tr>
								<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
							</tr>
						</c:if>
					</tbody>
				</table>

				<!-- 分页 -->
				<div class="page tar mb15">
					<input name="pageNo" value="${pageNo }" type="hidden" />
					<c:if test="${not empty pageObj.pageElements}">
						<jsp:include page="../common/pager-nest.jsp">
							<jsp:param name="toPage" value="1" />
							<jsp:param name="showCount" value="1" />
							<jsp:param name="action" value="doSearch" />
						</jsp:include>
					</c:if>
				</div>
			</form>
			
			<!-- 新增 -->
			<div id="dlgOverseasStudy" class="easyui-dialog" title="新增海外研修(访学)经历"
				style="width: 700px; height: 300px; overflow: auto; padding: 10px 20px"
				closed="true" buttons="#dlg-buttons" data-options="modal:true">
				<div class="right_table">

					<div id="table-content">
						<form id="fmOverseasStudy" method="post" novalidate>
							<table class="fitem clean">
								<tbody>
								<tr>
									<td>是否有海外研修(访学)经历:</td>
									<td><select type="text" class="easyui-combobox" required="true" name="sfjyhwyxjl" id="sfjyhwyxjlid">
										<app:dictselect dictType="JSXX_SFJYHWYXJL" selectValue="1"/>
										</select>
									</td>
								</tr>
								</tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
			
			<!-- submit -->
			<div id="dlg-buttons">
				<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveOverseasStudy()">保存</a> 
				<a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel"
					onclick="javascript:$('#dlgOverseasStudy').dialog('close')">取消</a>
			</div> 
			<!-- 编辑 -->
			<div id="dlgHW-BJ" class="easyui-dialog" title="编辑海外研修(访学)经历"
				style="width: 700px; height: 300px; overflow: auto; padding: 10px 20px"
				closed="true" buttons="#dlg-buttons" data-options="modal:true">
				<div class="right_table">
					<div id="table-content">
						<form id="fm" method="post" novalidate>
						<input type="hidden"  name="hwyxId" id="hwyxIdEdit" />
							<table class="fitem clean">
								<tbody>
									<tr>
										<td>是否有海外研修(访学)经历:</td>
										<td><select class="easyui-combobox" required="true" name="sfjyhwyxjl" id="sfjyhwyxjlEdit">
											<app:dictselect dictType="JSXX_SFJYHWYXJL"/> 
											</select>
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
			
			<!-- submit -->
			<div id="dlg-buttons">
				<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="etitOverseasStudy()">保存</a> 
				<a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel"
					onclick="javascript:$('#dlgHW-BJ').dialog('close')">取消</a>
			</div> 
		</div>
	</div>
	<div id="tips" class="easyui-dialog" title="温馨提示"
						style="padding: 10px 20px; width: 200px; height: 100px; line-height: 40px; text-align: center; overflow: auto; font-size: 20px;"
						closed="true" data-options="modal:true">请选择一条记录</div>
	<c:if test="${SESSION_ISPUB == '1'}">
    	<div id="btn-wrap">
        <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="submitInfo()">报送</a>
        <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="cancelSubmit()">取消</a><span style="color:red">各项信息填写完整后，才能报送。报送后，不能再次修改教师信息。只有审核不通过的教师信息，才能再次编辑报送。</span>
    </div>
    </c:if>
</body>
</html>


<script type="text/javascript">

$('#sfjyhwyxjlid').combobox({ editable:false }); //下拉框禁止输入
$('#sfjyhwyxjlEdit').combobox({ editable:false }); 
$('#dg').datagrid({
	onLoadSuccess:function(data){
		if(data.rows[0].id==undefined){
			$('#datagrid-row-r2-2-0').html(data.rows[0].ck);
			$('.datagrid-btable').css({'text-align':'center','width':'100%','padding':'30px 0'});
		}
	}
});

var url;
function save_user(dlgNmae){
    $(dlgNmae).dialog('open');
    $('#fm').form('clear');
    url = 'save_user.php';
}
//新增
function newUserGN(){
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
	if(${empty pageObj.pageElements}){
		url:url;
		 save_user('#dlgOverseasStudy');
	}
    if(${not empty pageObj.pageElements}){
		//save_user('#tipsaddOverseas');
		$.messager.alert('温馨提示','只能添加一条记录！','warning');
		return ;
	}
    }else{
		$.messager.alert('温馨提示','信息审核中或已通过审核，不可添加','warning');
	} 
},
error:function(XMLHttpRequest, textStatus, errorThrown){
	var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
	sessionTimeout(sessionstatus);
}

})
}

//关闭dialog
function save_close(dlgNmae){
    $(dlgNmae).dialog('close');
}
//保存-海外研修
function saveOverseasStudy(){
	$.messager.confirm('温馨提示', '确认保存？', function(r){
		 if(r){
    	$('#fmOverseasStudy').form('submit',{
        	url: "${ctx}/basicInformation/OverseasStudyAction.a?addOverseasStudy",
        	onSubmit: function(){
          	  return $(this).form('validate');
      	  },
        	success: function(result){
        		save_close('#dlgOverseasStudy');
        		$.messager.alert('温馨提示','保存成功','info',function(){
                  	window.location.reload();
          	  	});
       		 }
    	});
    	
		 }
	 });
}
//删除
function removeOverseas(){
	var selected=$("input[type='checkbox'][name='ck']:checked");
	if(selected.length<=0){
		$.messager.alert('温馨提示','请选择一条记录','warning');
		return ;
	}
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
	$.messager.confirm('温馨提示', '请确认是否删除信息？', function(r){
		if(r){
			var frmObj=$("#formOverseas");
			frmObj.attr("action","${ctx}/basicInformation/OverseasStudyAction.a?deleteUpdateOverseasStudy");
			frmObj.submit();
			$.messager.alert('温馨提示','删除成功','info',function(){
	          	window.location.reload();
	  	  	});
		} 
	});
   }else{
	  $.messager.alert('温馨提示','信息审核中或已通过审核，不可删除','warning');
	} 
},
error:function(XMLHttpRequest, textStatus, errorThrown){
	var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
	sessionTimeout(sessionstatus);
}

})
}

//编辑--回显
function editOverseas(){
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
    var selected=$("input[type='checkbox'][name='ck']:checked");
    if(selected.length<=0){
    	$.messager.alert('温馨提示','请选择一条记录','warning');
		return ;
	}else if(selected.length>1){
		$.messager.alert('温馨提示','只能编辑一条记录','warning');
		return ;
	}else{	
		var hwyxId=selected.val();
		$.ajax({
		    url:"${ctx}/basicInformation/OverseasStudyAction.a?toUpdateOverseasStudy&time="+new Date().getTime(),    //请求的url地址
		    dataType:"json",   //返回格式为json
		    //async:true,//请求是否异步，默认为异步，这也是ajax重要特性
		    data:{"hwyxId":hwyxId},    //参数值
		    type:"post",   //请求方式
		    success:function(data){
		    	console.log(data); 
		        $("#hwyxIdEdit").val(data.hwyxxx.hwyxId);
		        $("#sfjyhwyxjlEdit").combobox('setValue',data.hwyxxx.sfjyhwyxjl);
		    },
		    error:function(){
		        //请求出错处理
		    }
		});
    save_user('#dlgHW-BJ');
	}
    }else{
		$.messager.alert('温馨提示','信息审核中或已通过审核，不可编辑','warning');
	} 
},
error:function(XMLHttpRequest, textStatus, errorThrown){
	var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
	sessionTimeout(sessionstatus);
}

})
}

//编辑--修改  保存功能
function etitOverseasStudy() {
	var hwyxIdEdit=$("input[type='hidden'][name='hwyxId']").val();
	var sfjyhwyxjlEdit=$("input[type='hidden'][name='sfjyhwyxjl']:last").val();
	
	console.info("!!!!!!!!!!"+hwyxIdEdit);
	console.info("!!!!!!!!!!"+sfjyhwyxjlEdit);
	
		$.messager.alert('温馨提示','编辑成功','info',function(){
			window.location.href = "${ctx}/basicInformation/OverseasStudyAction.a?updateOverseasStudy&hwyxId="+hwyxIdEdit+"&sfjyhwyxjl="+sfjyhwyxjlEdit;
  	  	});
}




/**
 *报送
	*/
	function submitInfo(){
 		 
				$.ajax({
					url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
					data:{},
					dataType : "json",
					success:function(data){
						    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
						    	$.ajax({
									url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?submitted',
									data:{},
									success:function(data){
										$.messager.alert('温馨提示','报送成功','info',function(){
											window.parent.location.reload();
							    	  	});
									},
									error:function(data){
										$.messager.alert('温馨提示','报送失败,请检查各项是否都已填写！','warning');
									}
									
								});   
							}else{
								$.messager.alert('温馨提示','信息已报送，不可重复报送！','warning');
							}  
					},
					error:function(XMLHttpRequest, textStatus, errorThrown){
						var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
						sessionTimeout(sessionstatus);
					}
					
				})
 		 
 	 }
 /**
 *取消报送
	*/
	function cancelSubmit(){
		 
		$.ajax({
			url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
			data:{},
			dataType : "json",
			success:function(data){
				    if(data.tbJzgxxsh.shzt==02){
				    	$.ajax({
							url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?cancelSubmit&time='+new Date().getTime(),
							data:{},
							success:function(data){
								$.messager.alert('温馨提示','取消成功','info',function(){
					            	
					    	  	});
							},
							error:function(data){
							
							}
							
						});  
					}else{
						$.messager.alert('温馨提示','信息审核中(审核通过)或未报送，不可取消保送！','warning');
					}  
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
			
		})
			
 	 }

</script>

