<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>北京市教师管理服务平台</title>
    <link rel="stylesheet" type="text/css" href="../css/newcss.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
    <script type="text/javascript" src="../easyui/jquery.min.js"></script>
    <script src="../js/echarts/build/dist/echarts-all.js"></script>
    <script src="../js/echarts/build/dist/echarts.js"></script>
    <script type="text/javascript" src="../easyui/easyloader.js"></script>
    <script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx}/js/refresh.js"></script>
    <script type="text/javascript" src="../My97DatePicker/WdatePicker.js"></script>
    <link rel="stylesheet" type="text/css" href="../My97DatePicker/skin/WdatePicker.css">
    
	
</head>
<form action="${ctx}/basicInformation/ProtTechAppoinAction.a" id="fm" method="post" novalidate>
		<div id="anchor4">
        <div data-options="region:'center',border:false,collapsible:true" style="margin-bottom:20px;">
        <div id="toolbarGW">
            <a  class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="return newUserSD();">新 增</a>
            <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">编 辑</a>
            <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a>
            
        </div>
         <input type="hidden" value="${tbJzgxxsh.shzt}" id="xxshzt"/>
        <table id="dg" class="easyui-datagrid" title="专业技术职务聘任" iconCls="icon-dingdan"
           data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,collapsible:true,toolbar:'#toolbarGW',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
        
        <thead>
            <tr>
                <th field="ck" checkbox="true" data-options="field:'id',width:50"></th>
                <th data-options="field:'prdwmc',width:100">聘任单位名称</th>
                <th data-options="field:'przyjszw',width:100">聘任专业技术职务</th>
                <th data-options="field:'prksny',width:100">聘任开始年月</th>
                <th data-options="field:'prjsny',width:100">聘任结束年月</th>
                <th data-options="field:'cjsj',width:100">创建时间</th>
            </tr>
        </thead>
        <tbody>
        <c:if test="${empty pageObj.pageElements}">
			<tr>
				<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
			</tr>
		</c:if>
        <c:forEach items="${pageObj.pageElements}" var="item" varStatus="status">
            <tr>
           	 <%--   <th field="ck" checkbox="true" name="ck" value="${item.zyjszwId}">${item.zyjszwId}</th> --%>
               <td>${item.zyjszwId}</td>
               <td>${item.prdwmc}</td>
             <%--   <td>${item.przyjszw}</td> --%>
               <td><app:dictname dictid="JSXX_ZRJSZW${item.przyjszw}"/></td> 
               <td>${item.prksny}</td>
               <td>${item.prjsny}</td>
               <td>${item.cjsj}</td>
            </tr>
        </c:forEach>
        </tbody>
        </table>
        
        <div id="dlg-buttons">
            <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()" id="save">保存</a>
            <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgZY').dialog('close')">取消</a>
        </div>
        <div class="page tar mb15">
			<input name="pageNo" value="${pageNo }" type="hidden" />
			<c:if test="${not empty pageObj.pageElements}">
				<jsp:include page="../common/pager-nest.jsp">
					<jsp:param name="toPage" value="1" />
					<jsp:param name="showCount" value="1" />
					<jsp:param name="action" value="doSearch" />
				</jsp:include>
			</c:if>
		</div>
    </div>
</div>
</form>


<div id="dlgZY" class="easyui-dialog" title="新增专业技术职务聘任信息" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
<form id="form1" method="post">
    <div class="right_table">
        <div id="table-content">
         <table class="fitem clean">
            <tbody>
                <tr>
               		 <td>聘任专业技术职务：</td>
                    <td>
                       <input type="text" class="easyui-searchbox" data-options="searcher:doSearch"   required="true" name="PRZYJSZW1" id="PRZYJSZW">
                       <input type="hidden" name="PRZYJSZW">
                    </td>
                    <td>聘任单位名称：</td>
                    <td>
                        <input type="text"  class="easyui-textbox" name="PRDWMC" id="PRDWMC" required >
                    </td>
                </tr>
                <tr>
                    <td>聘任开始年月：</td>
                    <td>
                        <input type="text" name="PRKSNY" class="Wdate" id="PRKSNY" onfocus="WdatePicker({dateFmt:'yyyy-MM',maxDate:'#F{$dp.$D(\'PRJSNY\')}'})"/>
                    </td>
                    <td>聘任结束年月：</td>
                    <td>
                        <input  class="Wdate" type="text" name="PRJSNY"  id="PRJSNY" onfocus="WdatePicker({dateFmt:'yyyy-MM',minDate:'#F{$dp.$D(\'PRKSNY\')}'})"/>
                    </td>
                </tr>
                <input type="hidden" name="zyjszwId" id="zyjszwId">
            </tbody>
        </table>
        <div id="dd" class="easyui-dialog" title="专业技术职务" closed="true" data-options="modal:true" style="width:50%;max-height:300px;overflow-y:auto;overflow-x:hidden">
	         <input type="hidden" name="treeData" data="${dto1}"/>
	         <ul id="tt" class="easyui-tree">
			 </ul>
         </div>
        </div>
    </div>
    <div id="tips" class="easyui-dialog" title="温馨提示" style="padding:10px 20px;width:200px;height:100px;line-height:40px;text-align:center;overflow:auto;font-size:20px;" closed="true" data-options="modal:true">请选择一条记录</div>
        </form>
</div>

<c:if test="${SESSION_ISPUB == '1'}">
    	<div id="btn-wrap">
        <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="submitInfo()">报送</a>
        <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="cancelSubmit()">取消</a><span style="color:red">各项信息填写完整后，才能报送。报送后，不能再次修改教师信息。只有审核不通过的教师信息，才能再次编辑报送。</span>
    </div>
    </c:if>

<script type="text/javascript">

$('#dg').datagrid({
	onLoadSuccess:function(data){
		if(data.rows[0].prdwmc==undefined){
			$('#datagrid-row-r2-2-0').html(data.rows[0].ck);
			$('.datagrid-btable').css({'text-align':'center','width':'100%','padding':'30px 0'});
		}
	}
});

//fmx  新增加载树
function loadtree(){
	 var dataStrKey = $('input[name="treeData"][type="hidden"]').attr('data').replace(/\'/g,'\"');
	 var dataStrKey = dataStrKey.replace(/(\d+)/g,'\"$1\"');
	 var data = JSON.parse(dataStrKey);
	 console.log(data);
	 $('#tt').tree({
		 data:data
	 });
}
function tips(dlgNmae,msg){
	$(dlgNmae).html(msg).dialog('open');
};
$(function(){
	$('#tt').tree({
		onClick:function(node){
			$("#PRZYJSZW").searchbox('setValue', node.text);
			$('#dd').dialog('close');
			$('input[name="PRZYJSZW"][type="hidden"]').val(node.id);
		}
	});
});


$('#dd').dialog({
    onClose:function(){
    	var zwValue = $("#PRZYJSZW").searchbox('getValue');
    	if(zwValue == null || zwValue == ""||zwValue == "无"){
    		$("#PRDWMC").textbox("setValue"," ");
    		$("#PRDWMC").textbox("disable");
    		
    		$("#PRKSNY").val("");
    		$("#PRKSNY").attr("disabled","disabled");
    		
    		$("#PRJSNY").val("");
    		$("#PRJSNY").attr("disabled","disabled");
    	}else{
    		$("#PRDWMC").textbox("enable");
    		$("#PRKSNY").removeAttr("disabled");
    		$("#PRJSNY").removeAttr("disabled");
    	}
    }
});

function doSearch(){
	//debugger;
	 $('#dd').dialog('open');
	 var dataStrKey = $('input[name="treeData"][type="hidden"]').attr('data').replace(/\'/g,'\"');
	 var dataStrKey = dataStrKey.replace(/(\d+)/g,'\"$1\"');
	 var data = JSON.parse(dataStrKey);
	 $('#tt').tree({
		 data:data
	 });
	 
	//聘任专业技术职务事件
	$('#tt').tree({
		onSelect:function(node){
			if(node.text){
				$('#PRDWMC').textbox("enable");
				$('#PRKSNY').removeAttr("disabled");
		  		$('#PRJSNY').removeAttr("disabled");
			}else{
				$('#PRDWMC').combobox("disable");
		  		
		  		$('#PRKSNY').attr("disabled","disabled");
		  		$('#PRJSNY').attr("disabled","disabled");
			}
// 			$('#dd').dialog('close');
// 			$('input[name="PRZYJSZW"][type="hidden"]').val(node.id);
		}
	});
	$('#tt').tree('collapseAll');
}

//新增
function newUserSD(){
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
    save_user('#dlgZY');
			    }else{
					$.messager.alert('温馨提示','信息审核中或已通过审核，不可添加','warning');
				} 
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
		
	})
}
/**
 *报送
	*/
	function submitInfo(){
 		 
				$.ajax({
					url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
					data:{},
					dataType : "json",
					success:function(data){
						    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
						    	$.ajax({
									url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?submitted&time='+new Date().getTime(),
									data:{},
									success:function(data){
										$.messager.alert('温馨提示','报送成功','info',function(){
											window.parent.location.reload();
							    	  	});
									},
									error:function(data){
										$.messager.alert('温馨提示','报送失败,请检查各项是否都已填写！','warning');
									}
									
								});   
							}else{
								$.messager.alert('温馨提示','信息已报送，不可重复报送！','warning');
							}  
					},
					error:function(XMLHttpRequest, textStatus, errorThrown){
						var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
						sessionTimeout(sessionstatus);
					}
					
				})
 		 
 	 }
 /**
 *取消报送
	*/
	function cancelSubmit(){
		 
		$.ajax({
			url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
			data:{},
			dataType : "json",
			success:function(data){
				    if(data.tbJzgxxsh.shzt==02){
				    	$.ajax({
							url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?cancelSubmit&time='+new Date().getTime(),
							data:{},
							success:function(data){
								$.messager.alert('温馨提示','取消成功','info',function(){
					            	
					    	  	});
							},
							error:function(data){
							
							}
							
						});  
					}else{
						$.messager.alert('温馨提示','信息审核中(审核通过)或未报送，不可取消保送！','warning');
					}  
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
			
		})
			
 	 }

function save_user(dlgNmae){
    $(dlgNmae).dialog('open');
    $('#form1').form('clear');
  	
    $('#PRZYJSZW').searchbox("setValue",""); 
  	$('#PRKSNY').attr("disabled",true); //聘任开始年月禁止输入
	$('#PRJSNY').attr("disabled",true); //聘任结束年月禁止输入
	$('#PRZYJSZW').searchbox({ editable:false }); //聘任专业技术职务禁止输入
  	$('#PRDWMC').textbox("disable");
	
}

 function saveUser(){
	 var zyjszwId = $("#zyjszwId").val();
	 var prksny = $("input[name='PRKSNY']").val();//聘任开始年月
	 var prjsny = $("input[name='PRJSNY']").val();//聘任结束年月
	 var dzzw = $("input[name='dzzw']").val();
	 var zwValue = $("#PRZYJSZW").searchbox('getValue');
	 if(zwValue!="无"){
	
		 if($("#PRDWMC").val() == "" || $("#PRZYJSZW").val()==""||prksny==""){
			 $.messager.confirm('温馨提示', '请填写必填项！', 'warning');
			 return
		 }
		 if($("#PRDWMC").val().length>50){
			 $.messager.confirm('温馨提示', '聘任单位名称过长！', 'warning');
			 return
		 }
		 if(prjsny){
			 if(prjsny<prksny){
				 $.messager.alert('温馨提示', '聘任结束年月必须大于聘任开始年月！', 'warning');
				 return
			 }
		 }
		
	 }
	 $.messager.confirm('温馨提示', '确认保存？', function(r){
	 
		 if(r){
			 if(zyjszwId){
				 $('#form1').form('submit',{
				        url: "${ctx}/basicInformation/ProtTechAppoinAction.a?toUpAppoint",
				        onSubmit: function(){
				            return $(this).form('validate');
				        },
				        success: function(result){
				        	$.messager.alert('温馨提示','修改成功','info',function(){
				        		save_close('#dlgZY');
								window.location.reload();
						  	});
				        }
				    });  
				
			 }else{
				 $('#form1').form('submit',{
			        url: "${ctx}/basicInformation/ProtTechAppoinAction.a?SubAddtech",
			        onSubmit: function(){
			            return $(this).form('validate');
			        },
			        success: function(result){
		
			        	$.messager.alert('温馨提示','添加成功','info',function(){
			        		save_close('#dlgZY');
			     			window.location.reload();
			     	  	});
		
			        }
			    }); 
			 }
		 }
		 
	 });
	 
 }
 //回显
 function editUser(){
	 $.ajax({
			url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
			data:{},
			dataType : "json",
			success:function(data){
				    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
	 var selected=$("input[type='checkbox'][name='ck']:checked");
		
		if(selected.length<=0){
			 return $.messager.alert('温馨提示','请选择一条记录','warning');
		}else if(selected.length>1){
			return $.messager.alert('温馨提示','只能编辑一条记录','warning');
		}
	
	var strSel = selected.val();
    var row = $('#dg').datagrid('getSelected');
      if (row){
          
          $('#PRKSNY').attr("disabled",false); //聘任开始年月禁止输入
      	  $('#PRJSNY').attr("disabled",false); //聘任结束年月禁止输入
      	
      	  $('#PRZYJSZW').searchbox({ editable:false }); //聘任专业技术职务禁止输入
          $('#form1').form('load',row);
          $.ajax({
	      	url : '${ctx}/basicInformation/ProtTechAppoinAction.a?editPostAppoint&zyjszwId='+strSel+'&time='+new Date().getTime(),
	      	dataType: 'json',
	      	success:function(data){
				$("#zyjszwId").val(data.dto.zyjszwId);
	      		$("#PRKSNY").val(data.dto.prksny);
	      		$("#PRJSNY").val(data.dto.prjsny);
				$("#PRDWMC").textbox('setValue',data.dto.prdwmc);
			   // $("#PRZYJSZW").searchbox('setValue',data.dto.przyjszw);
				if(data.dto.przyjszw != ''){
					loadtree();
					$("#PRZYJSZW").searchbox('setValue',$('#tt').tree("find",data.dto.przyjszw).text);
					
			        $('input[name="PRZYJSZW"][type="hidden"]').val(data.dto.przyjszw);
				}
	      	}
	      });
          
          $('#dlgZY').dialog('open').dialog('setTitle','编辑专业技术职务聘任信息');
      }
				    }else{
						$.messager.alert('温馨提示','信息审核中或已通过审核，不可编辑','warning');
					} 
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
			
		})
 }
 //删除
 function removeUser(){
	 var selected=$("input[type='checkbox'][name='ck']:checked");
	 if(selected.length<=0){
			return $.messager.alert('温馨提示','请选择一条记录','warning');
		}
	 $.ajax({
			url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
			data:{},
			dataType : "json",
			success:function(data){
				    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
	  var strSel = '';
     $("input[name='ck']:checked").each(function(index, element) {
          strSel += $(this).val()+",";
      }); 
     var cks = strSel.substring(0, strSel.length-1);
     $.messager.confirm('温馨提示', '请确认是否删除信息？', function(r){
    	 		if (r){
     			$.messager.alert('温馨提示','删除成功','info',function(){
    				window.location.href='${ctx}/basicInformation/ProtTechAppoinAction.a?delPostAppoint&ck='+cks; 
     		  	});
     		}
     	});
				    }else{
						$.messager.alert('温馨提示','信息审核中或已通过审核，不可删除','warning');
					} 
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
			
		})
 }
 

//提示信息
 function tips(dlgNmae,msg){
 	$(dlgNmae).html(msg).dialog('open');
 }

//关闭dialog
 function save_close(dlgNmae){
     $(dlgNmae).dialog('close');
 }
 
 </script>
