<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>北京市教师管理服务平台</title>
    <link rel="stylesheet" type="text/css" href="../css/newcss.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
    <script type="text/javascript" src="../easyui/jquery.min.js"></script>
    <script src="../js/echarts/build/dist/echarts-all.js"></script>
    <script src="../js/echarts/build/dist/echarts.js"></script>
    <script type="text/javascript" src="../easyui/easyloader.js"></script>
    <script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx}/js/refresh.js"></script>
   	<script type="text/javascript" src="../My97DatePicker/WdatePicker.js"></script>
    <link rel="stylesheet" type="text/css" href="../My97DatePicker/skin/WdatePicker.css">
 </head>   
<body>
<div id="anchor2">
	  <div data-options="region:'center',border:false,collapsible:true" style="margin-bottom:20px">
                                <div id="toolbarXX">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUserXX()">新 增</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUserXx()">编 辑</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUserXx()">删 除</a>
                                    <c:choose>
                                    <c:when test="${tbBizJzgjbxx.tbXxjgb.ssxd==10||tbBizJzgjbxx.tbXxjgb.ssxd==20}">
                                         <span style="color:red">注意:从第一学历(本科或专科)填写至当前学历，学历低于本科或专科学历的，只填写最高学历</span>
                               		</c:when>
                               		<c:otherwise>
                               			 <span style="color:red">注意:从第一学历(高中或中专)填写至当前学历，学历低于高中或中专学历的，只填写最高学历</span>
                               		</c:otherwise>
                                   </c:choose>
                                </div>
                                <form action="${ctx}/basicInformation/TeacherLearnAction.a" id="frm" method="post">
                                <input type="hidden" value="${tbJzgxxsh.shzt}" id="xxshzt"/>
                                       <input type="hidden" value="${tbBizJzgjbxx.jsid}" name="jsid" id="jsid"/>
    							       <input type="hidden" value="${tbBizJzgjbxx.tbXxjgb.ssxd}" name="ssxd" id="ssxd"/>
                                <table id="dg" class="easyui-datagrid" title="学习经历" iconCls="icon-dingdan" style="width:100%"
                                    data-options="idField:'id',rownumbers:false,fitColumns:false,singleSelect:false,collapsible:true,toolbar:'#toolbarXX',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
                                
                                <thead>
                                    <tr>
                                        <th field="ck" checkbox="true"></th>
                                        <th data-options="field:'id',width:150">获得学历</th>
                                        <th data-options="field:'shencha',width:100">获得学历的国家（地区）</th>
                                        <th data-options="field:'chachong',width:200">获得学历的院校或机构</th>
                                        <th data-options="field:'name',width:100">所学专业</th>
                                        <c:if test="${tbBizJzgjbxx.tbXxjgb.ssxd!=10&&tbBizJzgjbxx.tbXxjgb.ssxd!=20}">
                                        <th data-options="field:'nakjge',width:80">是否师范类专业</th>
                                        </c:if>
                                        <th data-options="field:'named',width:100">入学年月</th>
                                        <th data-options="field:'sex',width:100">毕业年月</th>
                                        <th data-options="field:'num',width:100">学位层次</th>
                                        <th data-options="field:'beizhu',width:200">学位名称</th>
                                        <th data-options="field:'timef',width:150">获得学位的国家（地区）</th>
                                        <th data-options="field:'tim',width:150">获得学位的院校或机构</th>
                                        <th data-options="field:'nam',width:100">学位授予年月</th>
                                        <th data-options="field:'n',width:100">学习方式</th>
                                        <th data-options="field:'nme',width:150">在学单位类别</th>
                                        <th data-options="field:'nae',width:100">创建时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<c:if test="${not empty pageObj.pageElements}">
										<c:forEach items="${pageObj.pageElements}" var="item"
											varStatus="status">
											<tr>
                                        		<td>${item.xlId}</td>
                                        		<td><app:dictname dictid="JSXX_XL${item.hdxl}"/></td>
                                        	 	<td><app:dictname dictid="JSXX_GJDQ${item.hdxldgjdq}"/></td>
                                        		<td>${item.hdxldyxhjg}</td>
                                        		<td>${item.sxzy}</td>
                                                <c:if test="${tbBizJzgjbxx.tbXxjgb.ssxd!=10&&tbBizJzgjbxx.tbXxjgb.ssxd!=20}">
                                                    <td><app:dictname dictid="JSXX_SFSFLZY${item.sfsflzy}"/></td>
                                               </c:if>
                                        		<td>${item.rxny}</td>
                                        		<td>${item.byny}</td>
                                        		<td><app:dictname dictid="JSXX_XWCC${item.xwcc}"/></td>
                                        		<td><app:dictname dictid="JSXX_XW${item.xwmc}"/></td>
                                        		<td><app:dictname dictid="JSXX_GJDQ${item.hdxwdgjdq}"/></td>
                                        		<td>${item.hdxwdyxhjg}</td>
                                        		<td>${item.xwsyny}</td>
                                        		<td><app:dictname dictid="JSXX_XXFS${item.xxfs}"/></td>
                                        		<td><app:dictname dictid="JSXX_ZXDWLB${item.zxdwlb}"/></td>
                                        		<td>${item.cjsj}</td> 
                                    		</tr>
										</c:forEach>
									</c:if>
									<c:if test="${empty pageObj.pageElements}">
											<tr>
												<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
											</tr>
									</c:if>
                             
                                 </tbody>
                                </table>
                             
                                    <div class="page tar mb15">
											<input name="pageNo" value="${pageNo }" type="hidden" />
									<c:if test="${not empty pageObj.pageElements}">
										<jsp:include page="/common/pager-nest.jsp">
											<jsp:param name="toPage" value="1" />
											<jsp:param name="showCount" value="1" />
											<jsp:param name="action" value="doSearch" />
										</jsp:include>
									</c:if>
								 </div> 
							   </form>	 
								 <!-- 新增 -->
                                <div id="dlgXX" class="easyui-dialog" title="新增学习经历" style="width:700px;height:460px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        <div id="table-content">
                                        <form id="fm" method="post" novalidate>
                                        <input type="hidden" name="jsid" value="${tbBizJzgjbxx.jsid}"/>
                                         <input type="hidden" value="${tbBizJzgjbxx.tbXxjgb.ssxd}" name="ssxd"/>
                                        <table class="fitem clean">
                                         <c:choose>
                                                    <c:when test="${tbBizJzgjbxx.tbXxjgb.ssxd==10||tbBizJzgjbxx.tbXxjgb.ssxd==20}">
                                             			 <tbody>
                                                <tr>
                                                    <td>获得学历：</td>
                                                    <td>
                                                        <select class="easyui-combobox hdxl" required="true" name="hdxl" id="hdxl">
                                                          <app:dictselect dictType="JSXX_XL"/>
                                                        </select>
                                                    </td>
                                                    <td>获得学历的国家（地区）：</td>
                                                    <td>
                                                    <select class="easyui-combobox hdxldgjdq" required="true" name="hdxldgjdq" id="hdxldgjdq">
                                                     <app:dictselect dictType="JSXX_GJDQ" selectValue="156"/>
                                                     </select> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>获得学历的学院或机构：</td>
                                                    <td>
                                                        <input type="text" name="hdxldyxhjg" class="easyui-textbox  hdxldxyhjg" id="hdxldyxhjg" required>
                                                    </td>
                                                    <td>所学专业：</td>
                                                    <td>
                                                        <input type="text" name="sxzy" id="sxzy" class="easyui-textbox sxzy" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>入学年月：</td>
                                                    <td>
                                                        <input type="text" name="rxny" id="rxny" class="rxny Wdate" required onfocus="WdatePicker({dateFmt:'yyyy-MM',maxDate:'#F{$dp.$D(\'byny\')}'})"/>
                                                    </td>
                                                    <td>毕业年月：</td>
                                                    <td>

                                                        <input type="text" name="byny" id="byny" class="byny Wdate" required onfocus="WdatePicker({dateFmt:'yyyy-MM',minDate:'#F{$dp.$D(\'rxny\')}'})"/>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>学位层次：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="xwcc" id="xwcc">
                                                			<app:dictselect dictType="JSXX_XWCC"/>
                                                        </select>
                                                    </td>
                                                    <td>学位名称：</td>
                                                    <td>
                                                        <%-- <select class="easyui-combobox" required="true" name="xwmc" id="xwmc">
                                             				<app:dictselect dictType="JSXX_XW"/>
                                                        </select> --%>
                                                        <%-- <select class="easyui-combobox" required="true" name="xwmc" id="xwmc" >                                                    
      														 <c:forEach items ="${conclusion }" var="con">
	                                                          	<option value="${con.zdxbm}" <c:if test="${dto.khjl eq con.zdxbm}">selected</c:if>>${con.zdxmc}</option> 
                                                           </c:forEach>
      													</select> --%>
      													<select name="xwmc" id="xwmcc" >
      														<option value="">请选择</option>   
      													 <c:forEach items ="${conclusion }" var="con">
	                                                          	<option value="${con.zdxbm}" <c:if test="${dto.xwmc eq con.zdxbm}">selected</c:if>>${con.zdxmc}</option> 
                                                           </c:forEach>
      													</select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>获得学位的国家(地区)：</td>
                                                    <td>
                                                     <select class="easyui-combobox" required="true" name="hdxwdgjdq" id="hdxwdgjdq">
                                                     <app:dictselect dictType="JSXX_GJDQ" selectValue="156"/>
                                                     </select> 
                                                    </td>
                                                    <td>获得学位的院校或机构：</td>
                                                    <td>
                                                        <input type="text" name="hdxwdyxhjg" class="easyui-textbox" id="hdxwdyxhjg" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>学位授予年月：</td>
                                                    <td>
                                                        <input type="text" name="xwsyny" id="xwsyny" class="Wdate byny"  onfocus="WdatePicker({dateFmt:'yyyy-MM'})">
                                                    </td>
                                                    <td>学习方式：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="xxfs" id="xxfs">
                                                			<app:dictselect dictType="JSXX_XXFS"/>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>在学单位类别：</td>
                                                    <td>
                                                        <input type="text" name="zxdwlb" class="easyui-searchbox" id="zxdwlb" data-options="searcher:doSearch" required>
                                                    </td>
                                                </tr>
                                            </tbody>
                                                    </c:when>
                                                    <c:otherwise>
                                             		 <tbody>
                                                <tr>
                                                    <td>获得学历：</td>
                                                    <td>
                                                        <select class="easyui-combobox hdxl" required="true" name="hdxl" id="hdxl">
                                                          <app:dictselect dictType="JSXX_XL"/>
                                                        </select>
                                                    </td>
                                                    <td>获得学历的国家（地区）：</td>
                                                    <td>
                                                    <select class="easyui-combobox hdxldgjdq" required="true" name="hdxldgjdq" id="hdxldgjdq">
                                                     <app:dictselect dictType="JSXX_GJDQ" selectValue="156"/>
                                                     </select> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>获得学历的学院或机构：</td>
                                                    <td>
                                                        <input type="text" name="hdxldyxhjg" class="easyui-textbox  hdxldxyhjg" id="hdxldyxhjg" required>
                                                    </td>
                                                    <td>所学专业：</td>
                                                    <td>
                                                        <input type="text" name="sxzy" id="sxzy" class="easyui-textbox sxzy" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>入学年月：</td>
                                                    <td>
                                                        <input type="text" name="rxny" id="rxny"  class="Wdate rxny" required onfocus="WdatePicker({dateFmt:'yyyy-MM',maxDate:'#F{$dp.$D(\'byny\')}'})">
                                                    </td>
                                                    <td>是否师范类专业：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="sfsflzy" id="sfsflzy">
                                                			<app:dictselect dictType="JSXX_SFSFLZY"/>
                                                        </select>
                                                    </td>
                                                </tr>    
                                                <tr>
                                                <td>毕业年月：</td>
                                                    <td>
                                                        <input type="text" name="byny" id="byny" class="Wdate byny" required onfocus="WdatePicker({dateFmt:'yyyy-MM',minDate:'#F{$dp.$D(\'rxny\')}'})">
                                                    </td>
                                                    <td>学位层次：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="xwcc" id="xwcc">
                                                			<app:dictselect dictType="JSXX_XWCC"/>
                                                        </select>
                                                    </td>
                                                   
                                                </tr>
                                                <tr>
                                                 <td>获得学位的国家(地区)：</td>
                                                    <td>
                                                     <select class="easyui-combobox" required="true" name="hdxwdgjdq" id="hdxwdgjdq">
                                                     <app:dictselect dictType="JSXX_GJDQ" selectValue="156"/>
                                                     </select> 
                                                    </td>
                                                 <td>学位名称：</td>
                                                    <td>
<!--                                                         <select class="easyui-combobox" required="true" name="xwmc" id="xwmcc" > -->
<!--                                                             <option value="">请选择</option>  -->
<%--                                              				<app:dictselect dictType="JSXX_XW"/> --%>
<!--                                                         </select> -->
                                                        <select name="xwmc" id="xwmcc" >
      														<option value="">请选择</option>   
      													 <c:forEach items ="${conclusion }" var="con">
	                                                          	<option value="${con.zdxbm}" <c:if test="${dto.xwmc eq con.zdxbm}">selected</c:if>>${con.zdxmc}</option> 
                                                           </c:forEach>
      													</select>
                                                        <!-- <select name="xwmc" id="xwmc"  >   </select> -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                 <td>获得学位的院校或机构：</td>
                                                    <td>
                                                        <input type="text" name="hdxwdyxhjg" class="easyui-textbox" id="hdxwdyxhjg" required>
                                                    </td>
                                                    <td>学位授予年月：</td>
                                                    <td>
                                                        <!-- <input type="text" name="xwsyny" class="easyui-datebox" id="xwsyny" required> -->
                                                         <input type="text" name="xwsyny" id="xwsyny" class="Wdate byny"  onfocus="WdatePicker({dateFmt:'yyyy-MM'})">
                                                    </td>
                                                   
                                                </tr>
                                                <tr>
                                                   <td>在学单位类别：</td>
                                                    <td>
                                                        <input type="text" name="zxdwlb1" class="easyui-searchbox" id="zxdwlb" data-options="searcher:doSearch" required>
                                                    	<input type="hidden" name="zxdwlb"  >
                                                    </td>
                                                 <td>学习方式：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="xxfs" id="xxfs">
                                                			<app:dictselect dictType="JSXX_XXFS"/>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </tbody>
                                             		</c:otherwise> 
                                                </c:choose>
                                           
                                        </table>
                                        </form>
                                        </div>
                                    </div>
                            
                                  <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUserXx()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgXX').dialog('close')">取消</a>
                                </div>
                                
                               </div> 
                                <!-- 编辑 -->
                            <div id="editXX" class="easyui-dialog" title="编辑学习经历" style="width:700px;height:460px; overflow:auto;padding:10px 20px" closed="true" buttons="#edit-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        <div id="table-content">
                                        <form id="fmEdit" method="post" novalidate>
                                        <input type="hidden" name="xlId" id="xlIdEdit" />
                                         <input type="hidden" name="jsid" value="${tbBizJzgjbxx.jsid}"/>
                                         <input type="hidden" value="${tbBizJzgjbxx.tbXxjgb.ssxd}" name="ssxd"/>
                                        <table class="fitem clean">
                                        
                                         <c:choose>
                                                    <c:when test="${tbBizJzgjbxx.tbXxjgb.ssxd==10||tbBizJzgjbxx.tbXxjgb.ssxd==20}">
                                             	    <tbody>
                                                <tr>
                                                    <td>获得学历${tbBizJzgjbxx.jsid}：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="hdxl" id="hdxlEdit">
                                              				<app:dictselect dictType="JSXX_XL"/>
                                                        </select>
                                                    </td>
                                                    <td>获得学历的国家（地区）：</td>
                                                    <td>
                                                   		 <select class="easyui-combobox" required="true" name="hdxldgjdq" id="hdxldgjdqEdit">
                                                          <app:dictselect dictType="JSXX_GJDQ" selectValue="156"/>
                                                       </select> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>获得学历的学院或机构：</td>
                                                    <td>
                                                        <input type="text" name="hdxldyxhjg" class="easyui-textbox" required id="hdxldyxhjgEdit">
                                                    </td>
                                                    <td>所学专业：</td>
                                                    <td>
                                                        <input type="text" name="sxzy" class="easyui-textbox" required id="sxzyEdit">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>入学年月：</td>
                                                    <td>
                                                        <input type="text" name="rxny" class="Wdate" required id="rxnyEdit" onfocus="WdatePicker({dateFmt:'yyyy-MM',maxDate:'#F{$dp.$D(\'bynyEdit\')}'})">
                                                    </td>
                                                    <td>毕业年月：</td>
                                                    <td>
                                                        <input type="text" name="byny" class="Wdate" required id="bynyEdit" onfocus="WdatePicker({dateFmt:'yyyy-MM',minDate:'#F{$dp.$D(\'rxnyEdit\')}'})">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>学位层次：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="xwcc" id="xwccEdit">
                                                			<app:dictselect dictType="JSXX_XWCC"/>
                                                        </select>
                                                    </td>
                                                    <td>学位名称：</td>
                                                    <td>
                                                         <select class="easyui-combobox" required="true" name="xwmc" id="xwmccEdit">
                                                			<app:dictselect dictType="JSXX_XW"/>
                                                        </select> 
                                                       <%-- <select name="xwmc" id="xwmccEdit">   
      													 <c:forEach items ="${conclusion }" var="con">
	                                                          	<option value="${con.zdxbm}" <c:if test="${dto.xwmc eq con.zdxbm}">selected</c:if>>${con.zdxmc}</option> 
                                                           </c:forEach>
      													</select>--%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>获得学位的国家(地区)：</td>
                                                    <td>
                                                       <select class="easyui-combobox" required="true" name="hdxwdgjdq" id="hdxwdgjdqEdit">
                                                           <app:dictselect dictType="JSXX_GJDQ" selectValue="156"/>
                                                       </select> 
                                                    </td>
                                                    <td>获得学位的院校或机构：</td>
                                                    <td>
                                                        <input type="text" name="hdxwdyxhjg" class="easyui-textbox" required id="hdxwdyxhjgEdit">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>学位授予年月：</td>
                                                    <td>
                                                    <input type="text" name="xwsyny" id="xwsynyEdit" class="Wdate byny" required onfocus="WdatePicker({dateFmt:'yyyy-MM'})">
                                                        <!-- <input type="text" name="xwsyny" class="easyui-datebox" required id="xwsynyEdit"> -->
                                                    </td>
                                                    <td>学习方式：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="xxfs" id="xxfsEdit" >
                                                			<app:dictselect dictType="JSXX_XXFS"/>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>在学单位类别：</td>
                                                    <td>
                                                        <input type="text" name="zxdwlb1" class="easyui-searchbox" data-options="searcher:doSearch2" required id="zxdwlbEdit">
                                                    	<input type="hidden" name="zxdwlb"  >
                                                    </td>
                                                </tr>
                                            </tbody>
                                                    </c:when>
                                                    <c:otherwise>
                                             		     <tbody>
                                                <tr>
                                                    <td>获得学历：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="hdxl" id="hdxlEdit">
                                              				<app:dictselect dictType="JSXX_XL"/>
                                                        </select>
                                                    </td>
                                                    <td>获得学历的国家（地区）：</td>
                                                    <td>
                                                   		 <select class="easyui-combobox" required="true" name="hdxldgjdq" id="hdxldgjdqEdit">
                                                          <app:dictselect dictType="JSXX_GJDQ" selectValue="156"/>
                                                       </select> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>获得学历的学院或机构：</td>
                                                    <td>
                                                        <input type="text" name="hdxldyxhjg" class="easyui-textbox" required id="hdxldyxhjgEdit">
                                                    </td>
                                                    <td>所学专业：</td>
                                                    <td>
                                                        <input type="text" name="sxzy" class="easyui-textbox" required id="sxzyEdit">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>入学年月：</td>
                                                    <td>
                                                        <input type="text" name="rxny" class="Wdate" required id="rxnyEdit" onfocus="WdatePicker({dateFmt:'yyyy-MM',maxDate:'#F{$dp.$D(\'bynyEdit\')}'})">
                                                    </td>
                                                    <td>是否师范类专业：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="sfsflzy" id="sfsflzyEdit">
                                                			<app:dictselect dictType="JSXX_SFSFLZY"/>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                 <td>毕业年月：</td>
                                                    <td>
                                                        <input type="text" name="byny" class="Wdate" required id="bynyEdit" onfocus="WdatePicker({dateFmt:'yyyy-MM',minDate:'#F{$dp.$D(\'rxnyEdit\')}'})">
                                                    </td>
                                                    <td>学位层次：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="xwcc" id="xwccEdit">
                                                			<app:dictselect dictType="JSXX_XWCC"/>
                                                        </select>
                                                    </td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td>获得学位的国家(地区)：</td>
                                                    <td>
                                                       <select class="easyui-combobox" required="true" name="hdxwdgjdq" id="hdxwdgjdqEdit">
                                                           <app:dictselect dictType="JSXX_GJDQ" selectValue="156"/>
                                                       </select> 
                                                    </td>
                                                    <td>学位名称：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="xwmc" id="xwmccEdit">
                                           			     <app:dictselect dictType="JSXX_XW"/>
                                                        </select>
                                                    </td>
                                                   
                                                </tr>
                                                <tr>
                                                 <td>获得学位的院校或机构：</td>
                                                    <td>
                                                        <input type="text" name="hdxwdyxhjg" class="easyui-textbox" required id="hdxwdyxhjgEdit">
                                                    </td>
                                                    <td>学位授予年月：</td>
                                                    <td>
                                                     <input type="text" name="xwsyny" class="Wdate" required id="xwsynyEdit" onfocus="WdatePicker({dateFmt:'yyyy-MM'})">
                                                        <!-- <input type="text" name="xwsyny" class="easyui-datebox" required id="xwsynyEdit"> -->
                                                    </td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td>在学单位类别：</td>
                                                    <td>
                                                        <input type="text" name="zxdwlb1" class="easyui-searchbox" data-options="searcher:doSearch2" required id="zxdwlbEdit">
                                                    	<input type="hidden" name="zxdwlb"  >
                                                    </td>
                                                    <td>学习方式：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="xxfs" id="xxfsEdit">
                                                			<app:dictselect dictType="JSXX_XXFS"/>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </tbody>
                                             		</c:otherwise> 
                                                </c:choose>
                                        
                                        </table>
                                        </form>
                                        </div>
                                    </div>  
                                  
                           	<!--easyUi树(jzgly) -->
                                    
                                         <div id="dd" class="easyui-dialog" title="在学单位类别" closed="true" data-options="modal:true" style="width:50%;height:300px;overflow-y:auto;overflow-x:hidden;">
                                        	<input type="hidden" name="treeData" data="${zxdwlblist}"/>
                                        	<ul id="tt" class="easyui-tree">
											    
											</ul>
                                        </div>
                                         <div id="ddEdit" class="easyui-dialog" title="在学单位类别" closed="true" data-options="modal:true" style="width:50%;height:300px;overflow-y:auto;overflow-x:hidden;">
                                        	<ul id="ttEdit" class="easyui-tree">
											    
											</ul>
                                        </div>
                                   
                                <div id="edit-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="edit()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#editXX').dialog('close')">取消</a>
                                </div>  
                                
                                <div id="tips" class="easyui-dialog" title="温馨提示" style="padding:10px 20px;width:200px;height:100px;line-height:40px;text-align:center;overflow:auto;font-size:20px;" closed="true" data-options="modal:true">请选择一条记录</div>
                           
                            </div>
   </div>  
   <c:if test="${SESSION_ISPUB == '1'}">
    	<div id="btn-wrap">
        <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="submitInfo()">报送</a>
        <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="cancelSubmit()">取消</a><span style="color:red">各项信息填写完整后，才能报送。报送后，不能再次修改教师信息。只有审核不通过的教师信息，才能再次编辑报送。</span>
    </div>
    </c:if>                       
<script type="text/javascript">

//日期范围限制
/* $('#rxny').datebox({
	onSelect:function(date){
		var now = new Date();
		var d1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
		var d2 = new Date(now.getFullYear(), now.getMonth(), now.getDate()+10);
		$('#byny').datebox('setValued')1<=date && date<=d2;
	}
}); */


//日期范围限制
/* $('#rxny').datebox({
	onSelect:function(date){
		var now = new Date();
		var d1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
		var d2 = new Date(now.getFullYear(), now.getMonth(), now.getDate()+10);
		$('#byny').datebox('setValued')1<=date && date<=d2;
	}
}); */


$('#sfsflzyEdit,#sfsflzy,#xwmcc,#xwmc,#xwmccEdit,#hdxl,#hdxldgjdq,#xwcc,#xwccEdit,#hdxwdgjdq,#xxfs').combobox({ editable:false }); //下拉框禁止输入
$('#khjl,#zxdwlb').searchbox({ editable:false }); //搜索框禁止输入


// $('#xwcc,#xwccEdit').combobox({
// 	onSelect: function(record){
// 		$('#xwmcEdit').combobox('select',record.value);
// 	}
// });

 $('#dg').datagrid({
	onLoadSuccess:function(data){
		if(data.rows[0].id==undefined){ 
			$('.datagrid-body td[field="id"]').html(data.rows[0].ck);
		}
	}
}); 

$('#dg').datagrid({
	onLoadSuccess:function(data){
		if(data.rows[0].id==undefined){
			$('#datagrid-row-r3-2-0').html(data.rows[0].ck);
			$('.datagrid-btable').css({'text-align':'center','width':'100%','padding':'30px 0'});
		}
	}
});

var url;
function save_user(dlgNmae){
    $(dlgNmae).dialog('open');
    setNull();///置空

//     $('#xwcc').combobox({
//     	onSelect:function(record){
//     		if(record.value==0){
//     			alert(3)
//     			$('#hdxwdgjdq').combo({required:false}).combobox('clear').combobox('disable');
//     			$('#hdxwdyxhjg').combo({required:false}).textbox('disable');
//     			$('#xwsyny').combo({required:false}).datebox('disable');
//     		}
//     	}
//     })

}

//关闭dialog
function save_close(dlgNmae){
    $(dlgNmae).dialog('close');
}

//新增
function newUserXX(){
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
        save_user('#dlgXX');
			    }else{
					$.messager.alert('温馨提示','信息审核中或已通过审核，不可添加','warning');
				} 
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
		
	})
}

//fmx  新增加载树
function loadtree(){
	 var dataStrKey = $('input[name="treeData"][type="hidden"]').attr('data').replace(/\'/g,'\"');
	 var data = JSON.parse(dataStrKey);
	 console.log(data);
	 $('#ttEdit').tree({
		 data:data
	 });
	 $('#ttEdit').tree('collapseAll');
}

//弹出编辑框 
  function editUserXx(){
		$.ajax({
			url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
			data:{},
			dataType : "json",
			success:function(data){
				    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
	var selected=$("input[type='checkbox'][name='ck']:checked");
	
	if(selected.length<=0){
		$.messager.alert('温馨提示','请选择一条记录','warning');
		return ;
	}else if(selected.length>1){
		$.messager.alert('温馨提示','只能编辑一条记录','warning');
		return ;
	}else{	
		var xlId=selected.val();
		var jsid = $('#jsid').val(); 
		$.ajax({
		    url:"${ctx}/basicInformation/TeacherLearnAction.a?toEdit&time="+new Date().getTime(),    //请求的url地址
		    dataType:"json",   //返回格式为json
		    //async:true,//请求是否异步，默认为异步，这也是ajax重要特性
		    data:{"xlId":xlId,"jsid":jsid},    //参数值
		    type:"post",   //请求方式
		    success:function(data){
	    	console.log(data);
	           $("#xlIdEdit").val(data.tbBizJzgxxjlxx.xlId);
		        $("#hdxlEdit").combobox('setValue',data.tbBizJzgxxjlxx.hdxl);
		        $("#hdxldgjdqEdit").combobox('setValue',data.tbBizJzgxxjlxx.hdxldgjdq);
		        $("#hdxldyxhjgEdit").textbox('setValue',data.tbBizJzgxxjlxx.hdxldyxhjg);
		        $("#sxzyEdit").textbox('setValue',data.tbBizJzgxxjlxx.sxzy);
		        $("#sfsflzyEdit").combobox('setValue',data.tbBizJzgxxjlxx.sfsflzy);
		        //$("#rxnyEdit").datebox('setValue',data.tbBizJzgxxjlxx.rxny); 
		        $("#rxnyEdit").val(data.tbBizJzgxxjlxx.rxny); 
		        $("#bynyEdit").val(data.tbBizJzgxxjlxx.byny); 
		        //$("#bynyEdit").datebox('setValue',data.tbBizJzgxxjlxx.byny); 
		        $("#xwccEdit").combobox('setValue',data.tbBizJzgxxjlxx.xwcc); 
		        
		        $("#xwmccEdit").combobox('setValue',data.tbBizJzgxxjlxx.xwmc); 
		        $("#hdxwdgjdqEdit").combobox('setValue',data.tbBizJzgxxjlxx.hdxwdgjdq); 
		        $("#hdxwdyxhjgEdit").textbox('setValue',data.tbBizJzgxxjlxx.hdxwdyxhjg); 
		        $("#xwsynyEdit").val(data.tbBizJzgxxjlxx.xwsyny); 
		        //$("#xwsynyEdit").datebox('setValue',data.tbBizJzgxxjlxx.xwsyny); 
		        $("#xxfsEdit").combobox('setValue',data.tbBizJzgxxjlxx.xxfs); 

		        //fmx 回显树字段   注意：注释掉原来的方法
		        //$("#zxdwlbEdit").searchbox('setValue',data.tbBizJzgxxjlxx.zxdwlb);
				if(data.tbBizJzgxxjlxx.zxdwlb != null){
					loadtree();
					$("#zxdwlbEdit").searchbox('setValue',$('#ttEdit').tree("find",data.tbBizJzgxxjlxx.zxdwlb).text);
			        $('input[name="zxdwlb"][type="hidden"]').val(data.tbBizJzgxxjlxx.zxdwlb);
				}
	 	    },
		    error:function(){
		        //请求出错处理
		    }
		});
		save_user('#editXX');
	}  
				    }else{
						$.messager.alert('温馨提示','信息审核中或已通过审核，不可编辑','warning');
					} 
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
			
		})
}	


  function saveValidateEdit(){
		var hdxl=$('#hdxlEdit').combobox('getValue');
	 	if(hdxl !="0"){
	 		if($('#hdxldgjdqEdit').combobox("getValue") == ""){
	 			$.messager.alert('温馨提示','获得学历的国家不能为空!','warning');
	 			return false;
	 		}
	 		
	 		if($('#hdxldyxhjgEdit').textbox("getValue") == ""){
	 			$.messager.alert('温馨提示','获得学历的学院和机构不能为空!','warning');
	 			return false;
	 		}
	 		if($('#hdxldyxhjgEdit').textbox("getValue").length>50){
	 			$.messager.alert('温馨提示','获得学历的学院和机构名称过长!','warning');
	 			return false;
	 		}
	 		
	 		if($('#hdxldyxhjgEdit').textbox("getValue").length > 30){
	 			$.messager.alert('温馨提示','获得学历的学院和机构超出长度!','warning');
	 			return false;
	 		}
	 		
	 		if($('#sxzyEdit').textbox("getValue") == ""){
	 			$.messager.alert('温馨提示','所学专业不能为空!','warning');
	 			return false;
	 		}
	 		
	 		if($('#sxzyEdit').textbox("getValue").length > 30){
	 			$.messager.alert('温馨提示','所学专业超出长度!','warning');
	 			return false;
	 		}
	 		if($('#sxzyEdit').textbox("getValue").length>50){
	 			$.messager.alert('温馨提示','所学专业名称过长!','warning');
	 			return false;
	 		}
	 		/* if($('#sfsflzyEdit').combobox("getValue") == ""){
	 			$.messager.alert('温馨提示','是否师范专业不能为空!','warning');
	 			return false;
	 		} */
	 		
	 		if($('#rxnyEdit').val() == ""){
	 			$.messager.alert('温馨提示','任职年月不能为空!','warning');
	 			return false;
	 		}
	 		
	 		if($('#bynyEdit').val() == ""){
	 			$.messager.alert('温馨提示','毕业年月不能为空!','warning');
	 			return false;
	 		}
	 	}
	 	
	 	
	 	var xwccv=$('#xwccEdit').combobox('getValue');
	 	if(xwccv !="0"){
	 		if($('#xwmccEdit').combobox("getValue") == ""){
	 			$.messager.alert('温馨提示','学位名称不能为空!','warning');
	 			return false;
	 		}
	 		
	 		if($('#hdxwdyxhjgEdit').textbox("getValue") == ""){
	 			$.messager.alert('温馨提示','获得学位的学院和机构不能为空!','warning');
	 			return false;
	 		}
	 		
	 		if($('#hdxwdyxhjgEdit').textbox("getValue").length > 20){
	 			$.messager.alert('温馨提示','获得学位的学院和机构超出长度!','warning');
	 			return false;
	 		}
	 		
	 		if($('#hdxwdyxhjgEdit').textbox("getValue").length>50){
	 			$.messager.alert('温馨提示','获得学位的学院和机构过长!','warning');
	 			return false;
	 		}
	 		if($('#hdxwdgjdqEdit').combobox("getValue") == ""){
	 			$.messager.alert('温馨提示','获得学位的国家不能为空!','warning');
	 			return false;
	 		}
	 		
	 		if($('#xwsynyEdit').val() == ""){
	 			$.messager.alert('温馨提示','学位授予年月不能为空!','warning');
	 			return false;
	 		}
	 	}
	 	if($('#zxdwlbEdit').searchbox('getValue') == ""){
 			$.messager.alert('温馨提示','在学单位类别不能为空!','warning');
 			return false;
 		}
	 	return true;
	 }


//编辑学习经历
  function edit(){
  	$('#fmEdit').form('submit',{
          url: '${ctx}/basicInformation/TeacherLearnAction.a?edit',
          onSubmit: function(){
//               return $(this).form('validate');
				return saveValidateEdit();
          },
          success: function(){
              save_close('#editXX');      // close the dialog
              $.messager.alert('温馨提示','编辑成功','info',function(){
                	window.location.reload();
        	  	});
       
 
          }
      });
  }

/**
 * 删除
 */
function removeUserXx(){
	var selected=$("input[type='checkbox'][name='ck']:checked");
	if(selected.length<=0){
		$.messager.alert('温馨提示','请选择记录','warning');
		return;
	}
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
 	 var strSel = '';
     $("input[name='ck']:checked").each(function(index, element) {
          strSel += $(this).val()+",";
      }); 
      var arrCk = strSel.substring(0,strSel.length-1); 
      var jsid = $('#jsid').val();
      var ssxd = $('#ssxd').val();
      $.messager.confirm('温馨提示', '请确认是否删除信息？', function(r){
 
      	if(r){
			window.location.href ="${ctx}/basicInformation/TeacherLearnAction.a?delete&ck="+arrCk+"&jsid="+jsid+"&ssxd="+ssxd;
			$.messager.alert('温馨提示','删除成功','info',function(){
	          	window.location.reload();
	  	  	});
			}
      	});
			    }else{
					$.messager.alert('温馨提示','信息审核中或已通过审核，不可删除','warning');
				} 
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
		
	}) 
}

/**
 *报送
	*/
	function submitInfo(){
 		 
				$.ajax({
					url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
					data:{},
					dataType : "json",
					success:function(data){
						    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
						    	$.ajax({
									url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?submitted&time='+new Date().getTime(),
									data:{},
									success:function(data){
										$.messager.alert('温馨提示','报送成功','info',function(){
											window.parent.location.reload();
							    	  	});
									},
									error:function(data){
										$.messager.alert('温馨提示','报送失败,请检查各项是否都已填写！','warning');
									}
									
								});   
							}else{
								$.messager.alert('温馨提示','信息已报送，不可重复报送！','warning');
							}  
					},
					error:function(XMLHttpRequest, textStatus, errorThrown){
						var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
						sessionTimeout(sessionstatus);
					}
					
				})
 		 
 	 }
 /**
 *取消报送
	*/
	function cancelSubmit(){
		 
		$.ajax({
			url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
			data:{},
			dataType : "json",
			success:function(data){
				    if(data.tbJzgxxsh.shzt==02){
				    	$.ajax({
							url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?cancelSubmit&time='+new Date().getTime(),
							data:{},
							success:function(data){
								$.messager.alert('温馨提示','取消成功','info',function(){
					            	
					    	  	});
							},
							error:function(data){
							
							}
							
						});  
					}else{
						$.messager.alert('温馨提示','信息审核中(审核通过)或未报送，不可取消保送！','warning');
					}  
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
			
		})
			
 	 }

 function setNull(){
	 $('#hdxl').combobox('setValue',"11");
	 $('#hdxldgjdq').combobox("setValue","156");
	 $('#hdxldyxhjg').textbox("setValue","");
	 $('#sxzy').textbox("setValue","");
	 $('#sfsflzy').combobox("setValue","0");
	 $('#rxny').val("");
	 $('#byny').val("");
	 $('#xwcc').combobox('setValue',"2");
	 $('#xwmcc').combobox("setValue","");
	 $('#hdxwdyxhjg').textbox("setValue","");
	 $('#hdxwdgjdq').combobox("setValue","156");
	 $('#xwsyny').val("");
	 
	 $('#zxdwlb').searchbox("setValue","");
	 $('#xxfs').combobox("setValue","1");
 }
 
 
 function saveValidate(){
	var hdxl=$('#hdxl').combobox('getValue');
 	if(hdxl !="0"){
 		if($('#hdxldgjdq').combobox("getValue") == ""){
 			$.messager.alert('温馨提示','获得学历的国家不能为空!','warning');
 			return false;
 		}
 		
 		if($('#hdxldyxhjg').textbox("getValue") == ""){
 			$.messager.alert('温馨提示','获得学历的学院和机构不能为空!','warning');
 			return false;
 		}
 		

 		if($('#hdxldyxhjg').textbox("getValue").length>40){
 			$.messager.alert('温馨提示','获得学历的学院和机构超出长度!','warning');
 			return false;
 		}
 		
 		if($('#sxzy').textbox("getValue") == ""){
 			$.messager.alert('温馨提示','所学专业不能为空!','warning');
 			return false;
 		}
 		
 		if($('#sxzy').textbox("getValue").length>40){
 			$.messager.alert('温馨提示','所学专业超出长度!','warning');
 			return false;
 		}
 		
 		if($('#sxzy').textbox("getValue").length>50){
 			$.messager.alert('温馨提示','所学专业名称过长!','warning');
 			return false;
 		}
 		/* if($('#sfsflzy').combobox("getValue") == ""){
 			$.messager.alert('温馨提示','是否师范专业不能为空!','warning');
 			return false;
 		} */
 		
 		if($('#rxny').val() == ""){
 			$.messager.alert('温馨提示','入学年月不能为空!','warning');
 			return false;
 		}
 		
 		if($('#byny').val() == ""){
 			$.messager.alert('温馨提示','毕业年月不能为空!','warning');
 			return false;
 		}
 	}
 	
 	
 	var xwccv=$('#xwcc').combobox('getValue');
 	if(xwccv !="0"){
 		if($('#xwmcc').combobox("getValue") == ""){
 			$.messager.alert('温馨提示','学位名称不能为空!','warning');
 			return false;
 		}
 		
 		if($('#hdxwdyxhjg').textbox("getValue") == ""){
 			$.messager.alert('温馨提示','获得学位的学院和机构不能为空!','warning');
 			return false;
 		}
 		if($('#hdxwdyxhjg').textbox("getValue").length>50){
 			$.messager.alert('温馨提示','获得学位的学院和机构名称过长!','warning');
 			return false;
 		}
 		
 		if($('#hdxwdyxhjg').textbox("getValue").length > 20){
 			$.messager.alert('温馨提示','获得学位的学院和机构超出长度!','warning');
 			return false;
 		}
 		
 		if($('#hdxwdgjdq').combobox("getValue") == ""){
 			$.messager.alert('温馨提示','获得学位的国家不能为空!','warning');
 			return false;
 		}
 		
 		if($('#xwsyny').val() == ""){
 			$.messager.alert('温馨提示','学位授予年月不能为空!','warning');
 			return false;
 		}
 		
 	}
 	if($('#zxdwlb').searchbox('getValue') == ""){
			$.messager.alert('温馨提示','在学单位类别不能为空!','warning');
			return false;
		}
 	return true;
 }
 
//保存
function saveUserXx(){

	var rxny=$('#rxny').val();
	var byny=$('#byny').val();
	if(rxny>byny){
		$.messager.alert('温馨提示','入学年月不能大于毕业年份!','warning');
		return;
	}

	 $.messager.confirm('温馨提示', '确认保存？', function(r){
		 if(r){
	
			$('#fm').form('submit',{
		        url: '${ctx}/basicInformation/TeacherLearnAction.a?add',
		        onSubmit: function(){
		//         	return $(this).form('validate');
		            return saveValidate();
		        },
		        success: function(){
		            save_close("#dlgXX");      // close the dialog
		            $.messager.alert('温馨提示','保存成功','info',function(){
		 
		              	window.location.reload();
		      	  	});
		        }
		    });
		 }
	 });

}
 
//改变学历验证
/*    $('#hdxl').combobox({
    		   onSelect: function(param){
    			   var hdxl = $("input[type='hidden'][name='hdxl']").val();
    			   if(hdxl==0){
    				   $('#hdxldgjdq').combobox('setValue',' ');
    				   $('#hdxldyxhjg').combobox('setValue',' ');
    				   $('#sxzy').combobox('setValue',' ');
    				   $('#rxny').combobox('setValue',' ');
    				   $('#byny').combobox('setValue',' ');
    				   $('#hdxldgjdq').combobox('disable');
    				   $('#hdxldyxhjg').combobox('disable');
    				   $('#sxzy').combobox('disable');
    				   $('#rxny').combobox('disable');
    				   $('#byny').combobox('disable');
    			   }
    			   if(gjdq!=0){
    				   $('#mz').combobox('setValue',);
    				   $('#mz').combobox("enable");
    			   }
				}
  }); */


//搜索
 function doSearch(){
			$('#dd').dialog('open');
			 var dataStrKey = $('input[name="treeData"][type="hidden"]').attr('data').replace(/\'/g,'\"');
			 console.log(dataStrKey);
			 var data = JSON.parse(dataStrKey);
			 $('#tt').tree({
				 data:data
			 });
			 $('#tt').tree('collapseAll');
		}
 function doSearch2(){
			$('#ddEdit').dialog('open');
			 var dataStrKey = $('input[name="treeData"][type="hidden"]').attr('data').replace(/\'/g,'\"');
			 var data = JSON.parse(dataStrKey);
			 console.log(data);
			 $('#ttEdit').tree({
				 data:data
			 });
			 $('#ttEdit').tree('collapseAll');
	}
  
  //树选择
	 $(function(){
		$('#tt').tree({
			onClick:function(node){
				$("#zxdwlb").searchbox('setValue', node.text);
				save_close("#dd");
				
				//fmx  选中树时添加id
				$('input[name="zxdwlb"][type="hidden"]').val(node.id);
			}
		})
	 })
	 $(function(){
		$('#ttEdit').tree({
			onClick:function(node){
				$("#zxdwlbEdit").searchbox('setValue', node.text);
				save_close("#ddEdit");
				
				//fmx  选中树时添加id 注:编辑树使用，如果没有编辑树忽略
				
				$('input[name="zxdwlb"][type="hidden"]').val(node.id);
			}
		})
	 })
function addTab(title, url){
	if ($('#tb').tabs('exists', title)){
		$('#tb').tabs('select', title);
	} else {
		var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
		$('#tb').tabs('add',{
			title:title,
			content:content,
			closable:true
		});
	}
}
//关闭所有的tab  
    function closeAll(){  
        var tiles = new Array();  
          var tabs = $('#tb').tabs('tabs');      
          var len =  tabs.length;           
          if(len>0){  
            for(var j=0;j<len;j++){  
                var a = tabs[j].panel('options').title;               
                tiles.push(a);  
            }  
            for(var i=1;i<tiles.length;i++){               
                $('#tb').tabs('close', tiles[i]);  
            }
          }
    } 
//关闭当前的tab 
	function removePanel(){
		var tab = $('#tb').tabs('getSelected');
		    if (tab){
			var index = $('#tb').tabs('getTabIndex', tab);
			$('#tb').tabs('close', index);
			}
		}
		
	//是否双肩挑事件 获取学历的控制
	$('#hdxlEdit').combobox({
	    	onSelect:function(record){
	    	var hdxl=$('#hdxlEdit').combobox('getValue');
	    	if(hdxl=="0"){
	    		$('#hdxldgjdqEdit').combobox("clear");
	    		$('#hdxldgjdqEdit').combobox("disable");
	    		$('#hdxldyxhjgEdit').textbox("clear");
	    		$('#hdxldyxhjgEdit').textbox("disable");  
	    		$('#sxzyEdit').textbox("clear");
	    		$('#sxzyEdit').textbox("disable");
	    		$('#sfsflzyEdit').combobox("clear");
	    		$('#sfsflzyEdit').combobox("disable");
	    		$('#rxnyEdit').val(" ");
	    		$('#rxnyEdit').attr("disabled","disabled");
 	    		$('#bynyEdit').val(" ");
 	    		$('#bynyEdit').attr("disabled","disabled"); 
	    		
	    		///////////////////////////////////
// 	    		 $('#xwccEdit').combobox("clear");
// 	 	    	$('#xwccEdit').combobox("disable");
// 	 	    	 $('#xwmccEdit').combobox("clear");
// 		 	    $('#xwmccEdit').combobox("disable");

// 	    		$('#xwsynyEdit').val("");
// 	    		$('#xwsynyEdit').attr("disabled","disabled");
	    	}else{
	    		$('#hdxldgjdqEdit').combobox("enable");
	    		$('#hdxldyxhjgEdit').textbox("enable");
	    		$('#sxzyEdit').textbox("enable");
	    		$('#sfsflzyEdit').combobox("enable");
	    		$('#rxnyEdit').removeAttr("disabled");

	    		$("#hdxwdgjdqEdit").combobox("setValue","156");
	    		
// 	    		$('#xwsynyEdit').removeAttr("disabled");
                $('#bynyEdit').removeAttr("disabled");
// 	    		$('#xwccEdit').combobox("enable");
// 		 	    $('#xwmccEdit').combobox("enable");
	    	}
	    }
	});
		

	//学位层次的联动事件
	$('#xwccEdit').combobox({
        onSelect:function(record){
        	var xwcc=$('#xwccEdit').combobox('getValue');
        	if(xwcc=='0'){
        		$("#xwmccEdit").combobox("clear");
        		$("#xwmccEdit").combobox("disable");
        		$('#hdxwdgjdqEdit').combobox("clear");
 	    		$('#hdxwdgjdqEdit').combobox("disable");
 	    		$('#hdxwdyxhjgEdit').textbox("clear");
 	    		$('#hdxwdyxhjgEdit').textbox("disable");
 	    		$('#xwsynyEdit').val("");
 	    		$('#xwsynyEdit').attr("disabled","disabled");
        	}else{
        		$("#xwmccEdit").combobox("enable");
        		 $('#hdxwdgjdqEdit').combobox("enable");
 	    		$('#hdxwdyxhjgEdit').textbox("enable");
 	    		$('#xwsynyEdit').removeAttr("disabled"); 
 	    		$("#hdxwdgjdqEdit").combobox("setValue","156");
        		loadgwgjEdit(xwcc);
        	}
        }
	});
		
	
	function loadgwgjEdit(xwcc) {
		$.ajax({  
			url : '${ctx}/basicInformation/TeacherLearnAction.a?doQuery&zdxbm='+xwcc+'&time='+new Date().getTime(),
			data: null,
			dataType : 'json',
			type : 'POST',
			success : function(data) {
				$("#xwmccEdit").combobox({
			        url: null,
			        valueField: 'zdxbm',
			        textField: 'zdxmc',
			        editable: false,
			        data: data.dto
			    });
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
		});
	}

	//是否双肩挑事件 获取学历的控制
	$('#hdxl').combobox({
	    	onSelect:function(record){
	    	var hdxl=$('#hdxl').combobox('getValue');
	    	if(hdxl=="0"){
	    		$('#hdxldgjdq').combobox("clear");
	    		$('#hdxldgjdq').combobox("disable");
	    		$('#hdxldyxhjg').textbox("clear");
	    		$('#hdxldyxhjg').textbox("disable");  
	    		$('#sxzy').textbox("clear");
	    		$('#sxzy').textbox("disable");
	    		$('#sfsflzy').combobox("clear");
	    		$('#sfsflzy').combobox("disable");
	    		$('#rxny').val(" ");
	    		$('#rxny').attr("disabled","disabled");
	    		
 	    		$('#byny').val(" ");
	    		$('#byny').attr("disabled","disabled"); 
	    		
	    		///////////////////////////////////
// 	    		 $('#xwcc').combobox("clear");
// 	 	    	$('#xwcc').combobox("disable");
// 	 	    	 $('#xwmcc').combobox("clear");
// 		 	    $('#xwmcc').combobox("disable");

// 	    		$('#xwsyny').val("");
// 	    		$('#xwsyny').attr("disabled","disabled");
	    	}else{
	    		$('#hdxldgjdq').combobox("enable");
	    		$('#hdxldyxhjg').textbox("enable");
	    		$('#sxzy').textbox("enable");
	    		$('#sfsflzy').combobox("enable");
	    		$('#rxny').removeAttr("disabled");
	    		$("#hdxwdgjdq").combobox("setValue","156");
	    		
 	    		$('#byny').removeAttr("disabled");
// 	    		$('#xwsyny').removeAttr("disabled"); 
// 	    		$('#xwcc').combobox("enable");
// 		 	    $('#xwmcc').combobox("enable");
	    	}
	    }
	});


	function loadgwgj(xwcc) {
		$.ajax({  
			url : '${ctx}/basicInformation/TeacherLearnAction.a?doQuery&zdxbm='+xwcc+'&time='+new Date().getTime(),
			data: null,
			dataType : 'json',
			type : 'POST',
			success : function(data) {
				$("#xwmcc").combobox({
			        url: null,
			        valueField: 'zdxbm',
			        textField: 'zdxmc',
			        editable: false,
			        data: data.dto
			    });
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
		});
	}
	//学位层次的联动事件
	$('#xwcc').combobox({
        onSelect:function(record){
        	var xwcc=$('#xwcc').combobox('getValue');
        	if(xwcc=='0'){
        		$("#xwmcc").combobox("clear");
        		$("#xwmcc").combobox("disable");
        		 $('#hdxwdgjdq').combobox("clear");
 	    		$('#hdxwdgjdq').combobox("disable");
 	    		$('#hdxwdyxhjg').textbox("clear");
 	    		$('#hdxwdyxhjg').textbox("disable");
 	    		$('#xwsyny').val("");
 	    		$('#xwsyny').attr("disabled","disabled");
        	}else{
        		$("#xwmcc").combobox("clear");
        		$("#xwmcc").combobox("enable");
        		 $('#hdxwdgjdq').combobox("enable");
 	    		$('#hdxwdyxhjg').textbox("enable");
 	    		$('#xwsyny').removeAttr("disabled"); 
 	    		$("#hdxwdgjdq").combobox("setValue","156");
        		loadgwgj(xwcc);
        	}
        }
});

loadgwgj($('#xwcc').combobox('getValue'));  ///初始化学位列表
</script>			
</body>
</html>
