<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>北京市教师管理服务平台</title>
    <link rel="stylesheet" type="text/css" href="../css/newcss.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
    <script type="text/javascript" src="../easyui/jquery.min.js"></script>
    <script src="../js/echarts/build/dist/echarts-all.js"></script>
    <script src="../js/echarts/build/dist/echarts.js"></script>
    <script type="text/javascript" src="../easyui/easyloader.js"></script>
    <script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx}/js/refresh.js"></script>
    <script type="text/javascript" src="../My97DatePicker/WdatePicker.js"></script>
    <link rel="stylesheet" type="text/css" href="../My97DatePicker/skin/WdatePicker.css">
    
	
</head>
		<form action="${ctx}/basicInformation/PostAppointAction.a" id="fm" method="post" novalidate>
		<div id="anchor4">
        <div data-options="region:'center',border:false,collapsible:true" style="margin-bottom:20px;">
        <div id="toolbarGW">
            <a  class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="return newUserSD();">新 增</a>
            <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">编 辑</a>
            <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a>
            <span style="color:red">注意：从当前状态开始录入</span>
        </div>
        	<input type="hidden" value="${tbJzgxxsh.shzt}" id="xxshzt"/>
        <table id="dg" class="easyui-datagrid" title="岗位聘任" iconCls="icon-dingdan" 
            data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,collapsible:true,toolbar:'#toolbarGW',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
        
        <thead>
            <tr>
                <th field="ck" checkbox="true"></th>
                <th data-options="field:'id',width:30">岗位类别</th>
                <th data-options="field:'gwdj',width:30">岗位等级</th>
                <th data-options="field:'prksny',width:20">聘任开始年月</th>
                <c:choose>
                <c:when test="${ssxd==10||ssxd==20}">
                <th data-options="field:'sfsjt',width:20">是否双肩挑</th>
                <th data-options="field:'sfzzfdy',width:50">是否为辅导员</th>
                <th data-options="field:'dzjb',width:50">党政级别</th>
                <th data-options="field:'dzzw',width:50">党政职务</th>
                </c:when>
                <c:otherwise>
                <th data-options="field:'dzzw',width:50">校级职务</th>
                </c:otherwise>
                </c:choose>
                <th data-options="field:'rzksny',width:50">任职开始年月</th>
                <th data-options="field:'cjsj',width:50">创建时间</th>
            </tr>
        </thead>
        <tbody>
        <c:if test="${empty pageObj.pageElements}">
			<tr>
				<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
			</tr>
		</c:if>
        <c:forEach items="${pageObj.pageElements}" var="item" varStatus="status">
            <tr>
           	   <th field="ck" checkbox="true" name="ck" value="${item.gwprId}">${item.gwprId}</th>
               <td>${item.gwprId}</td>
               <td><appadd:dictnameadd dictid="JSXX_GWLB" dictbm="${item.gwlb}" schoolType="${ssxd}"/></td>
               <td><app:dictname dictid="JSXX_GWDJ${item.gwgj}"/></td>
               <td>${item.prksny}</td>
               <c:if test="${ssxd==10||ssxd==20}">
               <td><app:dictname dictid="JSXX_SFSJT${item.sfsjt}"/></td>  
               <td><app:dictname dictid="JSXX_SFZZFDY${item.sfzzfdy}"/></td>
               <td><app:dictname dictid="JSXX_DZJB${item.dzjb}"/></td>  
               </c:if>
               <td>
	              <c:forEach items="${item.dzzwList}" var="dzIndx">
<%-- 		              <app:dictname dictid="JSXX_DZZW${dzIndx}"/> --%>
		              <appadd:dictnameadd dictid="JSXX_DZZW" dictbm="${dzIndx}" schoolType="${ssxd}"/>
	               </c:forEach>
<%-- 	               <appadd:dictnameadd dictid="JSXX_DZZW" dictbm="${item.dzzw}" schoolType="${ssxd}"/> --%>
               </td>
               <td>${item.rzksny}</td>
               <td>${item.cjsj}</td>
            </tr>
        </c:forEach>
        </tbody>
        </table>
        
        <div id="dlg-buttons">
            <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()" id="save">保存</a>
            <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgGW').dialog('close')">取消</a>
        </div>
    </div>
    </div>
   		<div class="page tar mb15">
			<input name="pageNo" value="${pageNo }" type="hidden" />
			<c:if test="${not empty pageObj.pageElements}">
				<jsp:include page="../common/pager-nest.jsp">
					<jsp:param name="toPage" value="1" />
					<jsp:param name="showCount" value="1" />
					<jsp:param name="action" value="doSearch" />
				</jsp:include>
			</c:if>
		</div>
	</form>
	
	<div id="dlgGW" class="easyui-dialog" title="新增岗位聘任" style="width:800px;height:400px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
            <form id="form1" method="post" novalidate>
            <div class="right_table">
                <div id="table-content">
                <table class="fitem ">
               <c:choose>
               	<c:when test="${ssxd==10||ssxd==20}">
                    <tbody>
                        <tr>
                            <td>岗位类别：</td>
                            <td>
                                <select class="easyui-combobox" required="true" name="gwlb" id="gwlb">
                                <appadd:dictselectadd dictType="JSXX_GWLB" schoolType="${ssxd}" />
                                </select>
                            </td>
                            <td>岗位等级：</td>
                            <td>
                                <select class="easyui-combobox" name="gwgj" id="gwgj" >  
                              <%--   <app:dictselect dictType="JSXX_GWDJ" />  --%>                                                   
     							</select>
                            </td>
                        </tr>
                        <tr>
                            <td>聘任开始年月：</td>
                            <td>
                                <input  class="Wdate" type="text" name="prksny" id="prksny" onfocus="WdatePicker({dateFmt:'yyyy-MM',maxDate:'#F{$dp.$D(\'rzksny\')}'})"/>
                            </td>
                            <td>党政职务：</td>
                             <td>
                                 <select  class="easyui-combobox" required="true" name="dzzw" id="dzzwOne" value="${dto}" >
                                    <%-- <c:forEach items ="${dzzwList}" var="con">
                                    	<option value="${con.zdxbm}" <c:if test="${dto.dzzw eq con.zdxbm}">selected</c:if>>${con.zdxbm}-${con.zdxmc}</option> 
                                    </c:forEach> --%>
                                    <appadd:dictselectadd dictType="JSXX_DZZW" schoolType="${ssxd}" />
                                 </select>
                             </td>
                            
                        </tr>
                        <tr>
                            <td>任职开始年月：</td>
                            <td>
                                <input class="Wdate" type="text" name="rzksny"  id="rzksny" onfocus="WdatePicker({dateFmt:'yyyy-MM',minDate:'#F{$dp.$D(\'prksny\')}'})"/><span style="color:red" id="addrzjsnydlgs"></span>
                            </td>
                            <td>是否双肩挑：</td>
                            <td>
                                <select class="easyui-combobox" required="true" name="sfsjt" id="sfsjt">
                        			  <app:dictselect dictType="JSXX_SFSJT" />
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>是否为辅导员：</td>
                            <td>
                                <select class="easyui-combobox" required="true" name="sfzzfdy" id="sfzzfdy">
                        			  <app:dictselect dictType="JSXX_SFZZFDY" />
                                </select>
                            </td>
                            <td>党政级别：</td>
                            <td>
                                <select class="easyui-combobox" name="dzjb" id="dzjb">
                        			  <app:dictselect dictType="JSXX_DZJB" />
                                </select>
                                <!-- <input type="text" name="dzjb" id="dzjb"> -->
                            </td>
                        </tr>
                            <input type="hidden" name="gwprId" id="gwprId">
                    </tbody>
                 </c:when>
                 <c:when test="${ssxd==30||ssxd==40||ssxd==50||ssxd==60}">
                    <tbody>
                        <tr>
                            <td>岗位类别：</td>
                            <td>
                                <select class="easyui-combobox" required="true" name="gwlb" id="gwlb">
                               <%--  <app:dictselect dictType="JSXX_GWLB" /> --%>
                                <appadd:dictselectadd dictType="JSXX_GWLB" schoolType="${ssxd}" />
                                </select>
                            </td>
                            <td>岗位等级：</td>
                            <td>
                                <select class="easyui-combobox" name="gwgj" id="gwgj" >  
                                <app:dictselect dictType="JSXX_GWDJ" />                                                    
     							</select>
                            </td>
                            
                        </tr>
                        <tr>
                            <td>聘任开始年月：</td>
                            <td>
                                <input  class="Wdate" type="text" name="prksny" id="prksny" onfocus="WdatePicker({dateFmt:'yyyy-MM',maxDate:'#F{$dp.$D(\'rzksny\')}'})"/>
                            </td>
                            <td>校级职务：</td>
                            <td>
                                 <select  class="easyui-combobox" required="true" name="dzzw" id="dzzw" value="${dto}" data-options="multiple:true,multiline:true">
                                    <c:forEach items ="${dzzwMiddle}" var="con">
                                    	<option value="${con.zdxbm}" <c:if test="${dto.dzzw eq con.zdxbm}">selected</c:if>>${con.zdxmc}</option> 
                                    </c:forEach>
                                 </select>
                            </td>
                        </tr>
                        <tr>
                            <td>任职开始年月：</td>
                            <td>
                                <input class="Wdate" type="text" name="rzksny"  id="rzksny" onfocus="WdatePicker({dateFmt:'yyyy-MM',minDate:'#F{$dp.$D(\'prksny\')}'})"/><span style="color:red" id="addrzjsnydlgs"></span>
                            </td>
                        </tr>
                            <input type="hidden" name="gwprId" id="gwprId">
                    </tbody>
                 </c:when>
                 
                 <c:otherwise>
                 	<tbody>
                        <tr>
                            <td>岗位类别：</td>
                            <td>
                                <select  class="easyui-combobox" required="true" name="gwlb" id="gwlb" value="${dto.gwlb}" >
                                    <c:forEach items ="${conclusion }" var="con">
                                    	<option value="${con.zdxbm}" <c:if test="${dto.gwlb eq con.zdxbm}">selected</c:if>>${con.zdxmc}</option> 
                                    </c:forEach>
                                 </select>
                            </td>
                            <td>岗位等级：</td>
                            <td>
                                <select name="gwgj" id="gwgj" class="easyui-combobox"> 
                                 	<app:dictselect dictType="JSXX_GWDJ" />                                                   
     							</select>
                            </td>
                        </tr>
                        <tr>
                            <td>聘任开始年月：</td>
                            <td>
                                <input  class="Wdate" type="text" name="prksny"  id="prksny" onfocus="WdatePicker({dateFmt:'yyyy-MM',maxDate:'#F{$dp.$D(\'rzksny\')}'})"/>
                            </td>
                            
                            <td>任职开始年月：</td>
                            <td>
                                 <input class="Wdate" type="text" name="rzksny"  id="rzksny" onfocus="WdatePicker({dateFmt:'yyyy-MM',minDate:'#F{$dp.$D(\'prksny\')}'})"/><span style="color:red" id="addrzjsnydlgs"></span>
                            </td>
                         </tr>
                         <tr>   
                       		<td>是否兼任其他岗位：</td>
                            <td>
                                <select class="easyui-combobox" required="true" name="sfjrqtgw" id="sfjrqtgw">
                       				<app:dictselect dictType="JSXX_SFJRQTGW" />
                                </select>
                            </td>
                            
                            <td>兼任岗位类别：</td>
                            <td>
                                <select class="easyui-combobox"  name="jrgwlb" id="jrgwlb">
                       				<app:dictselect dictType="JSXX_JRGWLB" />
                                </select>
                            </td>
                          </tr> 
                          <tr>  
                         	<td>兼任岗位等级：</td>
                            <td>
                                <select class="easyui-combobox"  name="jrgwdj" id="jrgwdj">
                       				<app:dictselect dictType="JSXX_GWDJ" />
                                </select>
                            </td>                                             
                            <td>校级职务：</td>
                            <td>
                                 <select  class="easyui-combobox" required="true" name="dzzw" id="dzzw" value="${dto}" >
                                    <c:forEach items ="${dzzwMiddle}" var="con">
                                    	<option value="${con.zdxbm}" <c:if test="${dto.dzzw eq con.zdxbm}">selected</c:if>>${con.zdxmc}</option> 
                                    </c:forEach>
                                 </select>
                            </td>
                          </tr>
                            <input type="hidden" name="gwprId" id="gwprId">
                    	</tbody>
                 </c:otherwise>
               </c:choose>
                </table>
                </div>
            </div>
       </form>
                           
  <div id="tips" class="easyui-dialog" title="温馨提示" style="padding:10px 20px;width:200px;height:100px;line-height:40px;text-align:center;overflow:auto;font-size:20px;" closed="true" data-options="modal:true">请选择一条记录</div>
        </div>
        
<c:if test="${SESSION_ISPUB == '1'}">
    	<div id="btn-wrap">
        <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="submitInfo()">报送</a>
        <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="cancelSubmit()">取消</a><span style="color:red">各项信息填写完整后，才能报送。报送后，不能再次修改教师信息。只有审核不通过的教师信息，才能再次编辑报送。</span>
    </div>
</c:if>
<script type="text/javascript">


$('#dg').datagrid({
	onLoadSuccess:function(data){
		if(data.rows[0].id==undefined){
			$('#datagrid-row-r2-2-0').html(data.rows[0].ck);
			$('.datagrid-btable').css({'text-align':'center','width':'100%','padding':'30px 0'});
		}
	}
});

$('#sfjrqtgw,#jrgwlb,#jrgwdj').combobox({ editable:false }); //下拉框禁止输入

//新增
function newUserSD(){
	var xxshzt = $('#xxshzt').val();
	if(xxshzt==null||xxshzt==""||xxshzt==01||xxshzt==12||xxshzt==22){
        save_user('#dlgGW');
	}else{
		$.messager.alert('温馨提示','信息审核中或已通过审核，不可添加','warning');
	} 
}

function save_user(dlgNmae){
    $(dlgNmae).dialog('open');
    $('#form1').form('clear');
    
	$('#gwlb').combobox({ editable:false }); //下拉框禁止输入
	
	$('#sfsjt').combobox({ editable:false }); //下拉框禁止输入
	$('#sfzzfdy').combobox({ editable:false }); //下拉框禁止输入
	$('#dzjb').combobox({ editable:false }); //下拉框禁止输入
	
	var ssxd = ${ssxd};
	if(ssxd=='10'||ssxd=='20'){
		$('#dzzwOne').combobox({ editable:false }); //下拉框禁止输入
    	$("#dzzwOne").combobox('setValue','0');
	}else{
		$('#dzzw').combobox({ editable:false }); //下拉框禁止输入
    	$("#dzzw").combobox('setValue','00');
	}
	$('#rzksny').attr("disabled",true);
	$('#dzjb').combobox("clear");
	$('#dzjb').combobox("disable");
	
	loadgwgj($('#gwlb').combobox("getValue"));//岗位类别级联
}

//保存
 function saveUser(){
	 var gwprId = $("#gwprId").val();
	 var prksny = $("input[name='prksny']").val();//聘任开始年月
	 var rzksny = $("input[name='rzksny']").val();//任职开始年月
	 var gwgj = $("#gwgj").combobox('getValue');//岗位等级
	
	 var dzzw = $("input[name='dzzw']").val();//党政职务
	 var dzzwlh = $("input[name='dzzw']").length;
	 var dzzwVal="";
	 for(var k=0;k<dzzwlh;k++){
		 dzzwVal+=$("input[name='dzzw']").eq(k).val()+",";
	 }
	 dzzwVal=dzzwVal.substring(0,dzzwVal.length-1);
	 $("input[name=dzzw]").val(dzzwVal);
	 
	 if(!gwgj||gwgj=="0"){
		 $.messager.alert('温馨提示', '请选择岗位等级！', 'warning');
		 return
	 }
	 
	 if(!prksny||prksny==""){
		 $.messager.alert('温馨提示', '请选择聘任开始年月！', 'warning');
		 return
	 }
	/*  if(rzksny||rzksny!=""){
		 if(rzksny<prksny){
			 $.messager.alert('温馨提示', '任职开始年月必须大于聘任开始年月！', 'warning');
			 return
		 }
	 } */
	 
	
		 if(dzzw!="0"&&dzzw!="00"){
			 if(!rzksny||rzksny==""){
				 $.messager.alert('温馨提示', '请选择任职开始年月！', 'warning');
				 return
			 }
			 if(rzksny||rzksny!=""){
				 if(prksny>rzksny){
						$('#addrzjsnydlgs').text("任职开始年月必须大于聘任开始年月!");
						return;
					}
			 }
		 }
	
	
	 var ssxd = ${ssxd};
	 if(ssxd==10||ssxd==20){
	 var dzjb = $("#dzjb").combobox('getValue');//党政级别
		 if(dzzw!="0"&&dzzw!=""){
			 if(dzjb==""||!dzjb){
				 $.messager.alert('温馨提示', '请选择党政级别！', 'warning');
				 return
			 }
		 }
	 }
	 
	 $.messager.confirm('温馨提示', '确认保存？', function(r){
		 if(r){
				 if(gwprId&&gwprId!=""){
					 $('#form1').form('submit',{
					        url: "${ctx}/basicInformation/PostAppointAction.a?toSubAppoint",
					        onSubmit: function(){
					            return $(this).form('validate');
					        },
					        success: function(result){
							 $.messager.alert('温馨提示','修改成功','info',function(){
								save_close('#dlgGW');
								window.location.reload();
						  	 });
					        }
					    }); 
				 }else{
					 $('#form1').form('submit',{
				        url: "${ctx}/basicInformation/PostAppointAction.a?SubAddpoint",
				        onSubmit: function(){
				            return $(this).form('validate');
				        },
				        success: function(result){
				        	 $.messager.alert('温馨提示','添加成功','info',function(){
				        		save_close('#dlgGW');
				     			window.location.reload();
				     	  	});
				        }
				    }); 
				 }
	 	 }
	 });
	 
 }
 //回显
 function editUser(){

	 var xxshzt = $('#xxshzt').val();
		if(xxshzt==null||xxshzt==""||xxshzt==01||xxshzt==12||xxshzt==22){
		$('#gwlb').combobox({ editable:false }); //下拉框禁止输入
		//$('#gwgj').combobox({ editable:false }); //下拉框禁止输入
		$('#dzzw').combobox({ editable:false }); //下拉框禁止输入
		$('#sfsjt').combobox({ editable:false }); //下拉框禁止输入
// 		$('#sjtgwlb').combobox({ editable:false }); //下拉框禁止输入
// 		$('#sjtgwdj').combobox({ editable:false }); //下拉框禁止输入
		/* $('#sfzzcsxlzxgz').combobox({ editable:false }); //下拉框禁止输入
		$('#sfcyxlzxzgzs').combobox({ editable:false }); //下拉框禁止输入 */
		$('#sfzzfdy').combobox({ editable:false }); //下拉框禁止输入
		$('#dzjb').combobox({ editable:false }); //下拉框禁止输入
		
		
		
	    var selected=$("input[type='checkbox'][name='ck']:checked");
		
		if(selected.length<=0){
			return $.messager.alert('温馨提示','请选择一条记录','warning');
		}else if(selected.length>1){
			return $.messager.alert('温馨提示','只能编辑一条记录','warning');
		}
	  var strSel = selected.val();
      var row = $('#dg').datagrid('getSelected');
      if (row){
          $('#dlgGW').dialog('open').dialog('setTitle','编辑用户');
       		
          $('#form1').form('load',row);
          $.ajax({
	      	url : '${ctx}/basicInformation/PostAppointAction.a?editPostAppoint&gwprId='+strSel+'&time='+new Date().getTime(),
	      	dataType: 'json',
	      	success:function(data){
	      		
	      		 //加载数据方法 loadgwgjHX
	    		loadgwgjHX(data.dto.gwlb,data.dto.gwgj);
	      		$("#prksny").val(data.dto.prksny);//聘任开始年月
				$("#gwprId").val(data.dto.gwprId);
				$("#dzjb").combobox('setValue',data.dto.dzjb);//党政级别
				$("#sfzzfdy").combobox('setValue',data.dto.sfzzfdy);//是否为辅导员
				$("#sfsjt").combobox('setValue',data.dto.sfsjt);//是否双肩挑
				$("#gwlb").combobox('setValue',data.dto.gwlb);//岗位类别
				
				$("#gwgj").combobox("setValues",data.dto.gwgj);//岗位等级
				if($("#dzzw").length > 0){
					$("#dzzw").combobox('setValues',data.dto.dzzw);	
				}
				if($("#dzzwOne").length > 0){
					$("#dzzwOne").combobox('setValues',data.dto.dzzw);	
				}
				
				$("#rzksny").val(data.dto.rzksny);//任职开始年月
				
				//党政职务为0时 任职开始年月不可选
				var dzzw=$("input[name='dzzw']").val();
				if(dzzw=="00"){
					$('#rzksny').attr("disabled",true);
					$('#rzksny').val("");
					$('#dzjb').combobox("clear");
					$('#dzjb').combobox("disable");
				}else if(dzzw=='0'){
					$('#rzksny').attr("disabled",true);
					$('#rzksny').val("");
					$('#dzjb').combobox("clear");
					$('#dzjb').combobox("disable");
				}else{
					$('#dzjb').combobox("enable");
					$('#rzksny').attr("disabled",false);
				}
				
				$("#sfjrqtgw").combobox('setValue',data.dto.sfjrqtgw);//是否兼任其他岗位--(中等职业学校用户)
				$("#jrgwlb").combobox('setValue',data.dto.jrgwlb);//是否兼任其他岗位--(中等职业学校用户)
				$("#jrgwdj").combobox('setValue',data.dto.jrgwdj);//是否兼任其他岗位--(中等职业学校用户)
				
				//如果不是双肩挑
	            var sfsjt=$('#sfsjt').combobox('getValue');
	      	},
	      	error:function(XMLHttpRequest, textStatus, errorThrown){
	      		var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
	      		sessionTimeout(sessionstatus);
	      	}
	      });
        	
      }
	}else{
		$.messager.alert('温馨提示','信息审核中或已通过审核，不可编辑','warning');
	} 
 }
 //删除
 function removeUser(){
	 var selected=$("input[type='checkbox'][name='ck']:checked");
	 if(selected.length<=0){
			return $.messager.alert('温馨提示','请选择一条记录','warning');
		}
	 var xxshzt = $('#xxshzt').val();
		if(xxshzt==null||xxshzt==""||xxshzt==01||xxshzt==12||xxshzt==22){
	  var strSel = '';
     $("input[name='ck']:checked").each(function(index, element) {
          strSel += $(this).val()+",";
      }); 
     var cks = strSel.substring(0, strSel.length-1)
     $.messager.confirm('温馨提示', '请确认是否删除信息？', function(r){
 		if (r){
 			$.messager.alert('温馨提示','删除成功','info',function(){
 				window.location.href='${ctx}/basicInformation/PostAppointAction.a?delPostAppoint&ck='+cks;
 		  	});
 		}
 	});
		}else{
			$.messager.alert('温馨提示','信息审核中或已通过审核，不可删除','warning');
		} 
 }
 
//是否双肩挑事件
$('#sfsjt').combobox({
    	onSelect:function(record){
    	var sfsjt=$('#sfsjt').combobox('getValue');
//     	if(sfsjt=='0'){
//     		$('#sjtgwlb').combobox("disable");$('#sjtgwlb').combobox('setValue',"");
//     		$('#sjtgwdj').combobox("disable");$('#sjtgwdj').combobox('setValue',"");
//     		//$('#sjtgwlb').validatebox('disableValidation');
//     	}else{
//     		$('#sjtgwlb').combobox("enable");
//     		$('#sjtgwdj').combobox("enable");
//     	}
    }
});




//是否兼任其他岗位
$('#sfjrqtgw').combobox({
    	onSelect:function(record){
    	var sfjrqtgw=$('#sfjrqtgw').combobox('getValue');
    	if(sfjrqtgw=='0'){
    		$('#jrgwlb').combobox("disable");$('#jrgwlb').combobox('setValue',"");
    		$('#jrgwdj').combobox("disable");$('#jrgwdj').combobox('setValue',"");
    	}else{
    		$('#jrgwlb').combobox("enable");
    		$('#jrgwdj').combobox("enable");
    	}
    }
});

//党政职务
$('#dzzw').combobox({
	onSelect:function(record){
	var dzzw=$('#dzzw').combobox('getValue');
	
	if(dzzw == '00' && record.value != '00'){
		$('#dzzw').combobox("clear");
		$('#dzzw').combobox("setValue",record.value);
	}
	
	if(record.value == '00'){
		$('#dzzw').combobox("clear");
		$('#dzzw').combobox("setValue",record.value);
		
		$('#rzksny').attr("disabled",true);
		$('#rzksny').val("");
		$('#dzjb').combobox("clear");
		$('#dzjb').combobox("disable");
	}else{
		$('#rzksny').attr("disabled",false);
		$('#dzjb').combobox("enable");
		$('#dzjb').combobox('setValue',"");
	}
}
});


//党政职务
$('#dzzwOne').combobox({
	onSelect:function(record){
	var dzzw=$('#dzzwOne').combobox('getValue');
	
	if(record.value == '0'){
		
		$('#rzksny').attr("disabled",true);
		$('#rzksny').val("");
		$('#dzjb').combobox("clear");
		$('#dzjb').combobox("disable");
	}else{
		$('#rzksny').attr("disabled",false);
		$('#dzjb').combobox("enable");
		$('#dzjb').combobox('setValue',"");
	}
}
});


$('#gwlb').combobox({
	onSelect:function(record){
	var gwlb=record.value;
	//$("#gwgj").combobox("clear");
	if(gwlb != 0){	
		loadgwgj(gwlb);
	}
}
});

function loadgwgjHX(gwlb,value) {
$("input[name='gwgj']").val('');
	$.ajax({  
		url : '${ctx}/basicInformation/PostAppointAction.a?SelectPostGrade&zdxbm='+gwlb+'&time='+new Date().getTime(),
		data: null,
		dataType : 'json',
		type : 'POST',
		success : function(data) {
			$("#gwgj").combobox({
		        url: null,
		        valueField: 'zdxbm',
		        textField: 'zdxmc',
		        editable: false,
		        data: data.dto
		    });
			$("#gwgj").combobox("setValue",value);
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
	});
}

function loadgwgj(gwlb) {
	var ssxdd = '${ssxd}';
//$("input[name='gwgj']").val('');
	$.ajax({  
		url : '${ctx}/basicInformation/PostAppointAction.a?SelectPostGrade&zdxbm='+gwlb+'&ssxd='+ ssxdd +'&time='+new Date().getTime(),
		data: null,
		dataType : 'json',
		type : 'POST',
		success : function(data) {
			
			$("#gwgj").combobox({
		        url: null,
		        valueField: 'zdxbm',
		        textField: 'zdxmc',
		        editable: false,
		        data: data.dto
		    });
			$("#gwgj").combobox("setValue",data.dto[0].zdxbm);
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
	});
}

function loadPositionLevelByCategory(lb, dj){
	var ssxdd = '${ssxd}';
	$.ajax({  
		url : '${ctx}/basicInformation/PostAppointAction.a?SelectPostGrade&zdxbm='+lb+'&ssxd='+ ssxdd +'&time='+new Date().getTime(),
		data: null,
		dataType : 'json',
		type : 'POST',
		success : function(data) {
			
			$("#gwgj").combobox("clear");
			$("#gwgj").combobox({
		        url: null,
		        valueField: 'zdxbm',
		        textField: 'zdxmc',
		        editable: false,
		        data: data.dto
		    });
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
	});
}


/**
 *报送
	*/
	function submitInfo(){
 		 
				$.ajax({
					url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
					data:{},
					dataType : "json",
					success:function(data){
						    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
						    	$.ajax({
									url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?submitted&time='+new Date().getTime(),
									data:{},
									success:function(data){
										$.messager.alert('温馨提示','报送成功','info',function(){
											window.parent.location.reload();
							    	  	});
									},
									error:function(data){
										$.messager.alert('温馨提示','报送失败,请检查各项是否都已填写！','warning');
									}
									
								});   
							}else{
								$.messager.alert('温馨提示','信息已报送，不可重复报送！','warning');
							}  
					},
					error:function(XMLHttpRequest, textStatus, errorThrown){
						var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
						sessionTimeout(sessionstatus);
					}
					
				})
 		 
 	 }
 /**
 *取消报送
	*/
	function cancelSubmit(){
		 
		$.ajax({
			url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
			data:{},
			dataType : "json",
			success:function(data){
				    if(data.tbJzgxxsh.shzt==02){
				    	$.ajax({
							url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?cancelSubmit&time='+new Date().getTime(),
							data:{},
							success:function(data){
								$.messager.alert('温馨提示','取消成功','info',function(){
					            	
					    	  	});
							},
							error:function(data){
							
							}
							
						});  
					}else{
						$.messager.alert('温馨提示','信息审核中(审核通过)或未报送，不可取消保送！','warning');
					}  
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
			
		})
			
 	 }
 
 //提示信息
 function tips(dlgNmae,msg){
		$(dlgNmae).html(msg).dialog('open');
 }
 //关闭dialog
 function save_close(dlgNmae){
     $(dlgNmae).dialog('close');
 }
 </script>
