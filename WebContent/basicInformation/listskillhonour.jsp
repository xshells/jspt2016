<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>北京市教师管理服务平台</title>
<link rel="stylesheet" type="text/css" href="../css/newcss.css">
<link rel="stylesheet" type="text/css"
	href="../easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
<script type="text/javascript" src="../easyui/jquery.min.js"></script>
<script src="../js/echarts/build/dist/echarts-all.js"></script>
<script src="../js/echarts/build/dist/echarts.js"></script>
<script type="text/javascript" src="../easyui/easyloader.js"></script>
<script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="../basicInformation/ValidateUtils.js"></script>
<script type="text/javascript"
	src="../easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${ctx}/js/refresh.js"></script>

</head>
<body>
	
	<div id="anchor13">
		<div data-options="region:'center',border:false,collapsible:true"
			style="margin-bottom: 20px;">
			<div id="toolbarGN">
				<a href="#" class="easyui-linkbutton" iconCls="icon-add"
					plain="true" onclick="newSkill()">新 增</a> 
				<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true"
					onclick="editSkill()">编 辑</a> 
				<a href="#" class="easyui-linkbutton"
					iconCls="icon-remove" plain="true" onclick="removeSkill()">删 除</a>

			</div>
			<form action="${ctx}/basicInformation/SkillHonourAction.a" id="formSkill" method="post">
			<input type="hidden" value="${tbJzgxxsh.shzt}" id="xxshzt"/>
				<table id="dg" class="easyui-datagrid" title="技能及证书"
					iconCls="icon-dingdan"
					data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,collapsible:true,toolbar:'#toolbarGN',method:'get',checkOnSelect:'true',selectOnCheck:'true'">

					<thead>
						<tr>
							<th field="ck" checkbox="true"></th>
							<th data-options="field:'id',width:100">语种</th>
							<th data-options="field:'shesdxncha',width:100">掌握程度</th>
							<th data-options="field:'ccchong',width:100">创建时间</th>
						</tr>
					</thead>
					<tbody>
						<c:if test="${not empty pageObj.pageElements }">
							<c:forEach items="${pageObj.pageElements}" var="item"
								varStatus="status">
								<tr>
									<td>${item.jnzsId }</td>
									<td><app:dictname dictid="JSXX_YZ${item.yz}" />
									<td><app:dictname dictid="JSXX_ZWCD${item.zwcd}" />
									<td>${item.cjsj }</td>
								</tr>
							</c:forEach>
						</c:if>
						<c:if test="${empty pageObj.pageElements}">
							<tr>
								<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
							</tr>
						</c:if>
					</tbody>
				</table>


				<!-- 分页 -->
				<div class="page tar mb15">
					<input name="pageNo" value="${pageNo }" type="hidden" />
					<c:if test="${not empty pageObj.pageElements}">
						<jsp:include page="../common/pager-nest.jsp">
							<jsp:param name="toPage" value="1" />
							<jsp:param name="showCount" value="1" />
							<jsp:param name="action" value="doSearch" />
						</jsp:include>
					</c:if>
				</div>
			</form>
			<div id="dlgJNZS" class="easyui-dialog" title="新增技能证书"  
				style="width: 700px; height: 300px; overflow: auto; padding: 10px 20px"
				closed="true" buttons="#dlg-buttons" data-options="modal:true">
				<div class="right_table">
					
					<div id="table-content">
						<form id="fm" method="post" novalidate>
							<table class="fitem clean">
								<tbody>
									<tr>
										<td>语种：
											<select class="easyui-combobox" required="true" name="yz" id="yzid">
												<app:dictselect dictType="JSXX_YZ" selectValue="zh"/>
											</select>
										</td>
										
										<td>掌握程度：</td>
										<td><select class="easyui-combobox" required="true" name="zwcd" id="zwcdid">
												<app:dictselect dictType="JSXX_ZWCD"/>
											</select>
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
			<div id="dlg-buttons">
				<a href="#" class="easyui-linkbutton" iconCls="icon-ok"
					onclick="saveSkillHonour()">保存</a> <a href="#"
					class="easyui-linkbutton mr20" iconCls="icon-cancel"
					onclick="javascript:$('#dlgJNZS').dialog('close')">取消</a>
			</div>

			<!-- 编辑 -->
			<div id="dlgJNBJ" class="easyui-dialog" title="编辑技能及证书"
				style="width: 700px; height: 300px; overflow: auto; padding: 10px 20px"
				closed="true" buttons="#dlg-buttons" data-options="modal:true">
				<div class="right_table">
					<div id="table-content">
						<form id="fm" method="post" novalidate>
						<input type="hidden"  name="jnzsId" id="jnzsIdEdit" />
							<table class="fitem clean">
								<tbody>
									<tr>
										<td>语种：</td>
										<td><select class="easyui-combobox" required="true" name="yz" id="yzEdit">
												<app:dictselect dictType="JSXX_YZ"/>
											</select>
										</td>
										<td>掌握程度：</td>
										<td><select class="easyui-combobox" required="true" name="zwcd" id="zwcdEdit">
											<app:dictselect dictType="JSXX_ZWCD"/> 
											</select>
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
			
			<!-- submit -->
			<div id="dlg-buttons">
				<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="etitSkill()">保存</a> 
				<a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel"
					onclick="javascript:$('#dlgJNBJ').dialog('close')">取消</a>
			</div> 
		</div>
	</div>
	<div id="tips" class="easyui-dialog" title="温馨提示"
						style="padding: 10px 20px; width: 200px; height: 100px; line-height: 40px; text-align: center; overflow: auto; font-size: 20px;"
						closed="true" data-options="modal:true">请选择一条记录</div>
	<c:if test="${SESSION_ISPUB == '1'}">
    	<div id="btn-wrap">
        <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="submitInfo()">报送</a>
        <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="cancelSubmit()">取消</a><span style="color:red">各项信息填写完整后，才能报送。报送后，不能再次修改教师信息。只有审核不通过的教师信息，才能再次编辑报送。</span>
    </div>
    </c:if>
</body>
</html>

<script type="text/javascript">

$('#yzid').combobox({ editable:false }); //下拉框禁止输入
$('#zwcdid').combobox({ editable:false });
$('#yzEdit').combobox({ editable:false });
$('#zwcdEdit').combobox({ editable:false });

$('#dg').datagrid({
	onLoadSuccess:function(data){
		if(data.rows[0].id==undefined){
			$('#datagrid-row-r2-2-0').html(data.rows[0].ck);
			$('.datagrid-btable').css({'text-align':'center','width':'100%','padding':'30px 0'});
		}
	}
});


var url;
function save_user(dlgNmae){
    $(dlgNmae).dialog('open');
    $('#fm').form('clear');
    url = 'save_user.php';
}
//新增
function newSkill(){
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
    save_user('#dlgJNZS');
			    }else{
					$.messager.alert('温馨提示','信息审核中或已通过审核，不可添加','warning');
				} 
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
		
	})
}


//删除
function removeSkill(){
	var selected=$("input[type='checkbox'][name='ck']:checked");
	if(selected.length<=0){
		$.messager.alert('温馨提示','请选择一条记录','warning');
		return ;
	}
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
	$.messager.confirm('温馨提示', '请确认是否删除信息？', function(r){
		if(r){
			var frmObj=$("#formSkill");
			frmObj.attr("action","${ctx}/basicInformation/SkillHonourAction.a?deleteSkillHonour");
			frmObj.submit();
			$.messager.alert('温馨提示','删除成功','info',function(){
		      	window.location.reload();
			  	});
		} 
	});
			    }else{
					$.messager.alert('温馨提示','信息审核中或已通过审核，不可删除','warning');
				} 
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
		
	})
}

//关闭dialog
function save_close(dlgNmae){
    $(dlgNmae).dialog('close');
}
//保存
function saveSkillHonour(){
	var yz=$("input[type='hidden'][name='yz']").val();
	var zwcd=$("input[type='hidden'][name='zwcd']").val();
	if(yz==""||zwcd==""){
		$.messager.alert('温馨提示','请选择语种/掌握程度!','warning');
		return ;
	}
		$.ajax({
		    url:"${ctx}/basicInformation/SkillHonourAction.a?search&time="+new Date().getTime(),    //请求的url地址
		    dataType:"json",   //返回格式为json
		    //async:true,//请求是否异步，默认为异步，这也是ajax重要特性
		    data:{"yz":yz},    //参数值
		    type:"post",   //请求方式
		    success:function(data){
		    	console.log(data.result); 
		    	if(data.result==1){
		    	$('#fm').form('submit',{
			        url: "${ctx}/basicInformation/SkillHonourAction.a?addSkillHonour",
			        onSubmit: function(){
			            return $(this).form('validate');
			        },
			        success: function(result){
			        	save_close('#dlgJNZS');
			        	$.messager.alert('温馨提示','保存成功','info',function(){
			              	window.location.reload();
			      	  	});
			        }
			    });
		    	}else{
		    		$.messager.alert('温馨提示','此语种已添加!','warning');
		    	}
		    },
		    error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
		});

}


//编辑--回显
function editSkill(){
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
    var selected=$("input[type='checkbox'][name='ck']:checked");
    if(selected.length<=0){
		 $.messager.alert('温馨提示','请选择一条记录','warning');
		return ;
	}else if(selected.length>1){
		$.messager.alert('温馨提示','只能编辑一条记录','warning');
		return ;
	}else{	
		var jnzsId=selected.val();
		$.ajax({
		    url:"${ctx}/basicInformation/SkillHonourAction.a?toUpdateSkillHonour&time="+new Date().getTime(),    //请求的url地址
		    dataType:"json",   //返回格式为json
		    //async:true,//请求是否异步，默认为异步，这也是ajax重要特性
		    data:{"jnzsId":jnzsId},    //参数值
		    type:"post",   //请求方式
		    success:function(data){
		    	console.log(data); 
				
		        $("#jnzsIdEdit").val(data.skillHonour.jnzsId);
		        $("#yzEdit").combobox('setValue',data.skillHonour.yz);
		        $("#zwcdEdit").combobox('setValue',data.skillHonour.zwcd);
		    },
		    error:function(){
		        //请求出错处理
		    }
		});
    save_user('#dlgJNBJ');
	}
			    }else{
					$.messager.alert('温馨提示','信息审核中或已通过审核，不可编辑','warning');
				} 
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
		
	})
}
//编辑--修改  保存功能
function etitSkill() {
	var jnzsIdEdit=$("input[type='hidden'][name='jnzsId']").val();
	var yzEdit=$("input[type='hidden'][name='yz']:last").val();
	var zwcdEdit=$("input[type='hidden'][name='zwcd']:last").val();
	console.info("!!!!!!!!!!"+jnzsIdEdit);
	console.info("!!!!!!!!!!"+yzEdit);
	console.info("!!!!!!!!!!"+zwcdEdit);
	
	$.ajax({
	    url:"${ctx}/basicInformation/SkillHonourAction.a?searchUpdate&time="+new Date().getTime(),    //请求的url地址
	    dataType:"json",   //返回格式为json
	    //async:true,//请求是否异步，默认为异步，这也是ajax重要特性
	    data:{"jnzsId":jnzsIdEdit,"yz":yzEdit},    //参数值
	    type:"post",   //请求方式
	    success:function(data){
	    	console.log(data.result); 
	    	if(data.result==1){
				$.messager.alert('温馨提示','编辑成功','info',function(){
					window.location.href = "${ctx}/basicInformation/SkillHonourAction.a?updateSkillHonour&jnzsId="+jnzsIdEdit+"&yz="+yzEdit+"&zwcd="+zwcdEdit;
			  	});
	    	}else{
	    		$.messager.alert('温馨提示','此语种已存在!','warning');
	    	}
	    },
	    error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
	});
	
	
	
}


//搜索
function doSearch(){
    $('#search').dialog('open');
}
function addTab(title, url){
	if ($('#tb').tabs('exists', title)){
		$('#tb').tabs('select', title);
	} else {
		var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
		$('#tb').tabs('add',{
			title:title,
			content:content,
			closable:true
		});
	}
}
//关闭所有的tab  
    function closeAll(){  
        var tiles = new Array();  
          var tabs = $('#tb').tabs('tabs');      
          var len =  tabs.length;           
          if(len>0){  
            for(var j=0;j<len;j++){  
                var a = tabs[j].panel('options').title;               
                tiles.push(a);  
            }  
            for(var i=1;i<tiles.length;i++){               
                $('#tb').tabs('close', tiles[i]);  
            }
          }
    } 
//关闭当前的tab 
	function removePanel(){
		var tab = $('#tb').tabs('getSelected');
		    if (tab){
			var index = $('#tb').tabs('getTabIndex', tab);
			$('#tb').tabs('close', index);
			}
		}
    /**
	    *报送
		*/
		function submitInfo(){
	    		 
					$.ajax({
						url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
						data:{},
						dataType : "json",
						success:function(data){
							    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
							    	$.ajax({
										url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?submitted&time='+new Date().getTime(),
										data:{},
										success:function(data){
											$.messager.alert('温馨提示','报送成功','info',function(){
												window.parent.location.reload();
								    	  	});
										},
										error:function(data){
											$.messager.alert('温馨提示','报送失败,请检查各项是否都已填写！','warning');
										}
										
									});   
								}else{
									$.messager.alert('温馨提示','信息已报送，不可重复报送！','warning');
								}  
						},
						error:function(XMLHttpRequest, textStatus, errorThrown){
							var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
							sessionTimeout(sessionstatus);
						}
						
					})
	    		 
	    	 }
	    /**
	    *取消报送
		*/
		function cancelSubmit(){
			 
			$.ajax({
				url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
				data:{},
				dataType : "json",
				success:function(data){
					    if(data.tbJzgxxsh.shzt==02){
					    	$.ajax({
								url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?cancelSubmit&time='+new Date().getTime(),
								data:{},
								success:function(data){
									$.messager.alert('温馨提示','取消成功','info',function(){
						            	
						    	  	});
								},
								error:function(data){
								
								}
								
							});  
						}else{
							$.messager.alert('温馨提示','信息审核中(审核通过)或未报送，不可取消保送！','warning');
						}  
				},
				error:function(XMLHttpRequest, textStatus, errorThrown){
					var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
					sessionTimeout(sessionstatus);
				}
				
			})
				
	    	 }

</script>
