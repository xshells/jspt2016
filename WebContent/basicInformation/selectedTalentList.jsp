<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="${ctx}/css/newcss.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/easyui/themes/xyy.css">
    <script type="text/javascript" src="${ctx}/easyui/jquery.min.js"></script>
    <script src="${ctx}/js/echarts/build/dist/echarts-all.js"></script>
    <script src="${ctx}/js/echarts/build/dist/echarts.js"></script>
    <script type="text/javascript" src="${ctx}/easyui/easyloader.js"></script>
    <script type="text/javascript" src="${ctx}/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="${ctx}/easyui/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="${ctx}/js/refresh.js"></script>
	<script type="text/javascript" src="../My97DatePicker/WdatePicker.js"></script>
    <link rel="stylesheet" type="text/css" href="../My97DatePicker/skin/WdatePicker.css">
</head>
<body>
         <form action="${ctx}/basicInformation/SelectedTalentAction.a" id="fm" method="post" novalidate>
		<div class="content" id="userList">
			<div class="location">
			</div>
			<div class="modelbox modelbox3">

			</div>
			<%-- <div id="menu" class="easyui-accordion" fit="true" border="false">
             <input type="text" name="treeData" data="${dto1}"/>
		     <ul id="tt" class="easyui-tree"></ul>
        	</div> --%>
 					<div id="anchor70">
                            <div data-options="region:'center',border:false,collapsible:true" style="margin-bottom:20px;">
                                <div id="toolbarSD">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUserSD()">新 增</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">编 辑</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a>
                                    
                                </div>
                                <input type="hidden" value="${tbJzgxxsh.shzt}" id="xxshzt"/>
                                 <table id="dg" class="easyui-datagrid" title="入选人才项目" iconCls="icon-dingdan"
                                    data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,collapsible:true,toolbar:'#toolbarSD',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
                                
                                <thead>
                                    <tr>
                                        <th field="ck" checkbox="true"></th>
                                        <th data-options="field:'id',width:100">入选人才项目名称</th>
                                        <th data-options="field:'shencha',width:100">入选年份</th>
                                        <th data-options="field:'chachong',width:100">创建时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                <c:if test="${empty pageObj.pageElements}">
									<tr>
										<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
									</tr>
								</c:if>
								<div class="page tar mb15">
									<c:if test="${not empty pageObj.pageElements}">
									<c:forEach items="${pageObj.pageElements}" var="item"
										varStatus="status">
										<tr>
										<td field="ck" checkbox="true">${item.xmid}</td>
											<input type="hidden"  name="jsid" id="jsid" value="${item.jsid }">
											<input type="hidden"  name="xmid" id="xmid" value="${item.xmid }">
											<input type="hidden"  name="shzt" id="shzt" value="${item.shzt }">
											<td><app:dictname dictid="JSXX_RXRCXMMC${item.rxrcxmmc}"/></td>
											<td>${item.rxny}</td>
											<td>${item.cjsj}</td>
										</tr>
									</c:forEach>
									
									</c:if>
								</div>
                                </tbody>
                                </table>
                                <div id="dlgRX" class="easyui-dialog" title="新增入选人才项目" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        
                                        <div id="table-content">
                                        <form id="fm" method="post" novalidate>
                                        <input type="hidden" name="rxid"  value="">
                                        <table class="fitem clean">
                                            <tbody>
                                                <tr>
                                                    <td>入选人才项目名称：</td><!-- data-options="searcher:doSearch" -->
                                                    <td>
                                                        <input type="text" class="easyui-searchbox" data-options="searcher:doSearch" required="true" name="rxrcxmmc1" id="rxrcxmmc" value="${dto.rxrcxmmc}" >
                                                    	<input type="hidden" name="rxrcxmmc"  value="${dto.rxrcxmmc}">
                                                    </td>
	                                                    <td>入选年份：</td>
	                                                    <td>
	                                                       <%--  <input type="text" class="easyui-datebox" required="true" name="rxny" id="rxny" value="${dto.rxny}"> --%>
	                                                        <input id="rxny" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy'})" value="${dto.rxny}"/>
	                                                        <input type="hidden" name="rxny1" id="rxny1" value="${dto.rxny}">
	                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div id="dd" class="easyui-dialog" closed="true" title="入选人才项目名称" data-options="modal:true" style="width:50%;max-height:300px;overflow-y:auto;overflow-x:hidden">
                                        	<c:if test="${ssxd=='10'||ssxd=='20'}">
	                                        	<input type="hidden" name="treeData" data="${dto1}"/>
	                                        	<ul id="tt" class="easyui-tree">
												    
												</ul>
											</c:if>
											 <c:if test="${ssxd=='30'||ssxd=='40'||ssxd=='50'||ssxd=='60'}">
	                                        	<input type="hidden" name="treeData1" data="${dtozxx}"/>
	                                        	<ul id="tt1" class="easyui-tree"></ul>
                                        	</c:if>
                                        </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgRX').dialog('close')">取消</a>
                                </div>
                            </div>
                        </div>

			<div class="page tar mb15">
				<input name="pageNo" value="${pageNo }" type="hidden" />
				<c:if test="${not empty pageObj.pageElements}">
					<jsp:include page="../common/pager-nest.jsp">
						<jsp:param name="toPage" value="1" />
						<jsp:param name="showCount" value="1" />
						<jsp:param name="action" value="doSearch" />
					</jsp:include>
				</c:if>
			</div>
		</div>
		
<c:if test="${SESSION_ISPUB == '1'}">
    <div id="btn-wrap">
        <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="submitInfo()">报送</a>
        <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="cancelSubmit()">取消</a><span style="color:red">各项信息填写完整后，才能报送。报送后，不能再次修改教师信息。只有审核不通过的教师信息，才能再次编辑报送。</span>
    </div>
</c:if>
    
	<div id="tips" class="easyui-dialog" title="温馨提示" style="padding:10px 20px;width:200px;height:100px;line-height:40px;text-align:center;overflow:auto;font-size:20px;" closed="true" data-options="modal:true">请选择一条记录</div>
	
		<!-- 新增 -->
		<!-- <div class="content" style="display: none;" id="addUser">
			<div class="modelbox modelbox3">
				<div class="rowbox mb10 wper15">
					<input type="submit" value="保存" class="btnyellow fl"
						onclick="return saveUser();" /> <input type="button" value="返回"
						class="btnyellow fr" onclick="return hideAdd();" />
				</div>
			</div>
		</div> -->
  </form>
 
<script type="text/javascript">
var sxd = '${ssxd}';
$('#rxrcxmmc').searchbox({ editable:false }); //搜索框禁止输入

$('#dg').datagrid({
	onLoadSuccess:function(data){
		if(data.rows[0].id==undefined){
			$('#datagrid-row-r2-2-0').html(data.rows[0].ck);
			$('.datagrid-btable').css({'text-align':'center','width':'100%','padding':'30px 0'});
		}
	}
});
/*  $(function(){
	$.ajax({
		url:'${ctx}/basicInformation/SelectedTalentAction.a&time='+new Date().getTime(),
		success:function(){
			console.log("success");
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}
	})
})  */


//增加用户
function addUser() {
	window.location.href="${ctx}/TeacherAction.a?toAdd"
}
var url;
function save_user(dlgNmae){
    $(dlgNmae).dialog('open');
    $('#fm').form('clear');
}
//新增
function newUserSD(){
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
					if($('input[name="rxrcxmmc"][type="hidden"]').val()!="00"){
						$("#rxny").val('${year}');
					}
					$("#rxrcxmmc").searchbox('setValue','');
				    save_user('#dlgRX');
			    }else{
					$.messager.alert('温馨提示','信息审核中或已通过审核，不可添加','warning');
				} 
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
			sessionTimeout(sessionstatus);
		}

	});
}

//fmx  新增加载树
function loadtree(){
	 var dataStrKey = $('input[name="treeData"][type="hidden"],input[name="treeData1"][type="hidden"]').attr('data').replace(/\'/g,'\"');
	 var dataStrKey = dataStrKey.replace(/(\d+)/g,'\"$1\"');
	 var data = JSON.parse(dataStrKey);
	 $('#tt,#tt1').tree({	
		 data:data
	 });
}

//编辑
function editUser(){
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
var selected=$("input[type='checkbox'][name='ck']:checked");
	
	if(selected.length<=0){
		$.messager.alert('温馨提示','请选择一条记录','warning');
		return ;
	}else if(selected.length>1){
		$.messager.alert('温馨提示','只能编辑一条记录','warning');
		return ;
	}
	
	
	 var xmid = selected.val();
	 
	  var row = $('#dg').datagrid('getSelected');
     if (row){
         $('#dlgJY').dialog('open').dialog('setTitle','编辑用户');
         $('#fm').form('load',row);
         $.ajax({
	      	url : '${ctx}/basicInformation/SelectedTalentAction.a?doQuery&xmid='+xmid + '&time='+new Date().getTime(),
	      	dataType: 'json',
	      	success:function(data){
// 			    $("#rxrcxmmc").searchbox('setValue', data.dto.rxrcxmmc);
	      		if(data.dto.rxrcxmmc != ''){
					loadtree();
					if(sxd == "10" || sxd == "20"){
						$("#rxrcxmmc").searchbox('setValue',$('#tt').tree("find",data.dto.rxrcxmmc).text);
					}else{
						$("#rxrcxmmc").searchbox('setValue',$('#tt1').tree("find",data.dto.rxrcxmmc).text);
					}
					
			        $('input[name="rxrcxmmc"][type="hidden"]').val(data.dto.rxrcxmmc);
			        $('input[name="rxid"][type="hidden"]').val(xmid);
				}
			    
			    $("#rxny").val(data.dto.rxny); 
	      	}
	      });
     }
    
     $('#dlgRX').dialog('open').dialog('setTitle','编辑入选人才');
	
	
//     var ckData = $('#dg').datagrid('getSelected');
//     $("#rxrcxmmc").searchbox('setValue', ckData.id);
//     $("#rxny").val(ckData.shencha); 
//     $('#dlgRX').dialog('open').dialog('setTitle','编辑入选人才');
    console.log(ckData);
    }else{
		$.messager.alert('温馨提示','信息审核中或已通过审核，不可编辑','warning');
	} 
},
error:function(XMLHttpRequest, textStatus, errorThrown){
	var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
	sessionTimeout(sessionstatus);
}

}) 
}

function doSearch(){
	 $('#dd').dialog('open');
	 var dataStrKey = $('input[name="treeData"][type="hidden"],input[name="treeData1"][type="hidden"]').attr('data').replace(/\'/g,'\"');
	 var dataStrKey = dataStrKey.replace(/(\d+)/g,'\"$1\"');
	 var data = JSON.parse(dataStrKey);
	 $('#tt,#tt1').tree({	
		 data:data
	 })
	 $('#tt,#tt1').tree({
	onClick:function(node){
		$("#rxrcxmmc").searchbox('setValue', node.text);
		if(node.id=="00"){
			$("#rxny").val("");
			$('#rxny').attr("disabled","disabled");
		}else{
			 $('#rxny').removeAttr('disabled');
			 rxny1=$("input[type='hidden'][name='rxny1']").val();
		}
		$('#dd').dialog('close');
		$('input[name="rxrcxmmc"][type="hidden"]').val(node.id);
	}
 });
	 $('#tt,#tt1').tree('collapseAll');
	/*  $('#tt').tree({
			onClick:function(node){
				$("#rxrcxmmc").searchbox('setValue', node.text);
				$('#dd').dialog('close');
			}
		}) */
 }

/**
 *报送
	*/
	function submitInfo(){
 		 
				$.ajax({
					url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
					data:{},
					dataType : "json",
					success:function(data){
						    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
						    	$.ajax({
									url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?submitted&time='+new Date().getTime(),
									data:{},
									success:function(data){
										$.messager.alert('温馨提示','报送成功','info',function(){
											window.parent.location.reload();
							    	  	});
									},
									error:function(data){
										$.messager.alert('温馨提示','报送失败,请检查各项是否都已填写！','warning');
									}
									
								});   
							}else{
								$.messager.alert('温馨提示','信息已报送，不可重复报送！','warning');
							}  
					},
					error:function(XMLHttpRequest, textStatus, errorThrown){
						var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
						sessionTimeout(sessionstatus);
					}
					
				})
 		 
 	 }
 /**
 *取消报送
	*/
	function cancelSubmit(){
		 
		$.ajax({
			url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
			data:{},
			dataType : "json",
			success:function(data){
				    if(data.tbJzgxxsh.shzt==02){
				    	$.ajax({
							url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?cancelSubmit&time='+new Date().getTime(),
							data:{},
							success:function(data){
								$.messager.alert('温馨提示','取消成功','info',function(){
					            	
					    	  	});
							},
							error:function(data){
							
							}
							
						});  
					}else{
						$.messager.alert('温馨提示','信息审核中(审核通过)或未报送，不可取消保送！','warning');
					}  
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
			
		})
			
 	 }

$(function(){
	$('#tt').tree({
		onClick:function(node){
			$("#rxrcxmmc").searchbox('setValue', node.text);
			$('#dd').dialog('close');
			$('input[name="rxrcxmmc"][type="hidden"]').val(node.id);
		}
	})
});
//删除
function removeUser(){
	var selected=$("input[type='checkbox'][name='ck']:checked");
	
	if(selected.length<=0){
		return $.messager.alert('温馨提示','请选择一条记录','warning');
	}
	$.ajax({
		url:'${ctx}/basicInformation/TeacherBaseInfoAction.a?getShzt&time='+new Date().getTime(),
		data:{},
		dataType : "json",
		success:function(data){
			    if(data.tbJzgxxsh.shzt==null||data.tbJzgxxsh.shzt==""||data.tbJzgxxsh.shzt==01||data.tbJzgxxsh.shzt==12||data.tbJzgxxsh.shzt==22){
 	 var strSel = '';
     $("input[name='ck']:checked").each(function(index, element) {
          strSel += $(this).val()+",";
      }); 
      var arrCk = strSel.substring(0,strSel.length-1); 
	$.messager.confirm('温馨提示', '请确认是否删除信息？', function(r){
		if (r){
		$.messager.alert('温馨提示','删除成功','info',function(){
		window.location.href ="${ctx}/basicInformation/SelectedTalentAction.a?delSelectTalent&ck="+arrCk;
		  	});
		}
	});
    }else{
		$.messager.alert('温馨提示','信息审核中或已通过审核，不可删除','warning');
	} 
},
error:function(XMLHttpRequest, textStatus, errorThrown){
	var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
	sessionTimeout(sessionstatus);
}

})

}
//保存
function saveUser(){
	var id = $('input[name="rxid"][type="hidden"]').val();
   var repat = 0;
	var rxrcxmmc=$("input[name='rxrcxmmc']").val();
	if(!rxrcxmmc){
		$.messager.alert('温馨提示','入选人才项目名称不能为空！','warning');
		return;
	}
	var rxny=$("#rxny").val();
	if(rxrcxmmc!=null && rxrcxmmc !="00" && (!rxny)){
		$.messager.alert('温馨提示','入选年份不能为空！','warning');
		return;
	}
	/* $.messager.confirm('温馨提示', '请确认是否要保存信息？', function(r){ */
		 if (repat==0){
		 	 repat = 1;
			if(id&&id!=""){
				$('#fm').form('submit',{
			        url: "${ctx}/basicInformation/SelectedTalentAction.a?editSelectedTalent&rxrcxmmc="+rxrcxmmc+"&rxny="+rxny+"&xmid="+id,
			        onSubmit: function(){
			            return $(this).form('validate');
			        },
			        success: function(result){
						
		            	save_close('#dlgRX');      // close the dialog
		            	/* $('#dg').datagrid('reload'); */
		            	$.messager.alert('温馨提示','编辑成功','info',function(){
		        	      	window.location.reload();
		        		  	});
			        }
			    }); 
				
			}else if(repat=1){
				repat=2;
				$.messager.confirm('温馨提示', '请确认是否要保存信息？', function(r){
					if(r){
						 $('#fm').form('submit',{
						        url: "${ctx}/basicInformation/SelectedTalentAction.a?addSeclectTalent&rxrcxmmc="+rxrcxmmc+"&rxny="+rxny+"&xmid="+id,
						        onSubmit: function(){
						            return $(this).form('validate');
						        },
						        success: function(result){
						        	save_close('#dlgRX');      // close the dialog
						        	$.messager.alert('温馨提示','保存成功','info',function(){
						    	      	window.location.reload();
						    		  	});  
						        }
						    });
					}
					      
				});
		}  
		 } 
	/* }); */
	
    
}
function updateTeacher(tid){
	//var uName = $("#uName"+tname).val();
	//$("#tname").val(uName);
	window.location.href="${ctx}/TeacherAction.a?updateTeacher";
	$("#addUser").show();
	$("#userList").hide();
}
//提示信息
function tips(dlgNmae,msg){
	$(dlgNmae).html(msg).dialog('open');
}
//关闭dialog
function save_close(dlgNmae){
    $(dlgNmae).dialog('close');
}

</script>
   
</body>
</html>
