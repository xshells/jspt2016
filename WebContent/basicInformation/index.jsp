<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>北京市教师管理服务平台</title>
    <link rel="stylesheet" type="text/css" href="../css/newcss.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
    <script type="text/javascript" src="../easyui/jquery.min.js"></script>
    <script src="../js/echarts/build/dist/echarts-all.js"></script>
    <script src="../js/echarts/build/dist/echarts.js"></script>
    <script type="text/javascript" src="../easyui/easyloader.js"></script>
    <script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
    
	
</head>
<body class="easyui-layout" style="margin:0 1px;background-color:#f5f5f5 !important;">  
    <div data-options="region:'north',split:true" style="height:80px;" id="xyy_header">
	    <div class="easyui-layout" data-options="fit:true">   
            <div style="float:left;width:40%;padding:18px 0 0 15px;">
			    <img style="width:30px; margin-bottom:-12px" src="../img/logo-white.png">
				<a href="#" style="font-size:20px;color:#fff;text-decoration:none;">北京市教师管理服务平台</a>
			</div>
            <div style="float:right;width:56%;">
				<div class="xyy_tool">	
					<ul style="float:right;position: relative;right:0;margin:0;">
						<li><a href="index.html"><i class="xyy-home" style="position: absolute;display:block;width: 16px;height: 16px;top: 50%;margin: -8px 12px;"></i>主 页</a></li>						
						<li><a href="#"><i class="shezhi" style="position: absolute;display:block;width: 16px;height: 16px;top: 50%;margin: -8px 12px;"></i>设 置</a></li>						
						<li><a href="#"><i class="xyy-mima" style="position: absolute;display:block;width: 16px;height: 16px;top: 50%;margin: -8px 12px;"></i>修改密码</a></li>				
						<li><a target="_self" href="../login.html"><i class="xyy-tuichu" style="position: absolute;display:block;width: 16px;height: 16px;top: 50%;margin: -8px 12px;"></i>退出登录</a></li>
					</ul>
				</div>				
			</div>			
        </div>
	</div>   
    <div data-options="region:'south',split:true" style="height:40px;text-align:center;line-height:30px;background:#19aa8d;color:#f5f5f5;">北京市教师管理服务平台（版本：1.2.2.16082516）</div>
    <div data-options="region:'west',title:'高校教师主菜单',split:true" style="width:200px;">
	    <div id="menu" class="easyui-accordion" fit="true" border="false">
        	<div title="教师个人信息" data-options="iconCls:'icon-shezhi',selected:true" style="overflow:auto;padding:10px;">
                <ul id="tt" class="easyui-tree" data-options="animate:true,lines:true">                
                    <li><span><a href="#anchor1">基本信息*</a></span></li>
                    <li><span><a href="#anchor2">学习经历*</a></span></li>
                    <li><span><a href="#anchor3">工作经历*</a></span></li>  
                    <li><span><a href="#anchor4">岗位聘任*</a></span></li>
                    <li><span><a href="#anchor5">专业技术职务聘任*</a></span></li>
                    <!--<li><span><a href="#anchor15">基本待遇</a></span></li>-->
                    <li><span><a href="#anchor6">年度考核*</a></span></li>
                    <li><span><a href="#anchor7">教师资格*</a></span></li>
                    
                    <!--<li><span><a href="#anchor16">师德信息</a></span></li>-->
                    <li><span><a href="#anchor8">教育教学*</a></span></li>  
                    <!--<li><span><a href="#anchor9">教学科研成果及获奖信息</a></span></li>-->
                    <li><span><a href="#anchor10">入选人才项目*</a></span></li>
                    <!--<li><span><a href="#anchor11">国内培训</a></span></li> -->
                    <li><span><a href="#anchor12">海外研修（访学）*</a></span></li>
                    <li><span><a href="#anchor13">技能及证书*</a></span></li>
                    <li><span><a href="#anchor14">联系方式*</a></span></li>
                </ul>
            </div>
        </div>
	
	</div>  
    <div data-options="region:'center'" style="background:#fff;border:0;">
	    <div id="tb" class="easyui-tabs" data-options="tools:'#tab-tools',fit:true">
			<div title="个人信息维护" iconCls="icon-home" style="padding:15px;">
		        <div class="easyui-layout" data-options="fit:true">
                    
                    
                    <div data-options="region:'center',border:false">
                        <div id="toolbarxd" style=" border:1px dotted #19aa8d; background:white;margin-bottom:4px; border-radius:4px; width:60px">
                        <a href="#" class="easyui-linkbutton" iconCls="icon-ok" plain="true" onclick="newUser()">报 送</a>
                        </div>
                        
                        <div id="anchor1">
    						<div class="easyui-panel" data-options="region:'center',border:false,collapsible:true" iconCls="icon-dingdan" title="基本信息"  style="margin-bottom:20px; width:100%">
                            	<div id="toolbarxd" style=" border-bottom:1px dotted #ccc; ">
								<a href="#" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="newUser()">保 存</a>
							    </div>
                                <table id="dg" class=" fitem" data-options="idField:'id',rownumbers:false,fitColumns:true,pagination:true,singleSelect:false,toolbar:'#toolbar',method:'get',checkOnSelect:'true',selectOnCheck:'true'" style="width:800px;height:300px;" >
    							
                                	<thead></thead>
                                	<tbody class="fitem ">
        								<tr>
                                            <td>学校名称：</td>
                                            <td><p id="text">海淀一中</p></td>
                                            <td>地址：</td>
                                            <td><p id="text">海淀区</p></td>
                                        </tr>
                                        <tr>
                                            <td>所在区：</td>
                                            <td><p id="text">海淀区</p></td>
                                            <td>办学类型：</td>
                                            <td><p id="text">高中</p></td>
                                        </tr>
                                        <tr>
                                            <td>举办单位：</td>
                                            <td><p id="text">海淀区教育部</p></td>
                                            <td>举办者类型：</td>
                                            <td><p id="text">政府机构</p></td>
                                        </tr>
                                        <tr>
                                            <td><label>姓名：</label></td>
                                            <td><p id="text">李梅</p></td>
                                            <td rowspan="5">照片预览：<br><span style="color:#19aa8d; line-height:20px;">注：近期免冠半身照片，<br>照片规格：宽26mm，<br>高32mm，<br>分辨率150dpi以上，<br>照片要求为jpg格式，<br>文件大小应小于60K。</span></td>
                                            <td rowspan="5" ><img style="width:100px; margin-right:35px" src="../img/pic.jpg"><br><input style="background:#ccc; color:white; border:1px solid #999; width:100px; margin-right:35px" type="button" value="删除照片"></td>
                                        </tr>
                                        
                                        <tr>
                                            <td>性别：</td>
                                            <td><p id="text">女</p></td>
                                        </tr>
                                        <tr>
                                        	<td>教育ID</td>
                                            <td><p id="text">123554</p></td>
                                        </tr>
                                        <tr>
                                        	<td>身份证件类型：	</td>
                                            <td><select class="easyui-combobox" required="true" name="" id="">
                                                <option>请选择</option>
                                            <app:dictselect dictType="JSXX_SFZJLX"/>
                                               <!--  <option></option>
                                                <option>1-居民身份证</option> -->
                                            </select></td>
                                        </tr>
                                        <tr>
                                            <td>身份证件号：</td>
                                            <td><p id="text">110321198901012321</p></td>                                        </tr>
                                        <tr>
                                        	<td>出生日期：</td>
                                            <td><p id="text">1989-01-01</p></td>
                                            <td>个人照片：</td>
                                            <td><input style="margin-right:10px; width:112px" type="button" required value="选择文件">尚未选择</td>
                                        </tr>
                                        <tr>
                                            <td>国籍/地区：</td>
                                            <td><select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                <option>请选择</option>
                                                <option>156-中国</option>
                                            </select></td>
                                            <td>出生地：</td>
                                            <td><input type="text" class="easyui-textbox" required></td>
                                        </tr>
                                        <tr>
                                            <td>民族：</td>
                                            <td><select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                <option>请选择</option>
                                                <option>01-汉族</option>
                                            </select></td>
                                            <td>政治面貌：</td>
                                            <td><select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                <option>请选择</option>
                                                <option>请选择</option>
                                            </select></td>
                                        </tr>
                                        <tr>
                                            <td>参加工作年月：</td>
                                            <td><input type="text" class="easyui-datebox" required ></td>
                                            <td>进本校年月：</td>
                                            <td><input type="text" class="easyui-datebox" required ></td>
                                        </tr>
                                        <tr>
                                            <td>教职工来源：</td>
                                            <td><input type="text" class="easyui-datebox" required ></td>
                                            <td>教职工类别：</td>
                                            <td><select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                <option>请选择</option>
                                                <option>请选择</option>
                                            </select></td>
                                        </tr>
                                        <tr>
                                            <td>是否在编：</td>
                                            <td><select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                <option>请选择</option>
                                                <option>请选择</option>
                                            </select></td>
                                            <td>学缘结构：</td>
                                            <td><select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                <option>请选择</option>
                                                <option>请选择</option>
                                            </select></td>
                                        </tr>
                                        <tr>
                                            <td>签订合同情况：</td>
                                            <td><select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                <option>请选择</option>
                                                <option>请选择</option>
                                            </select></td>
                                            <td>用人形式：</td>
                                            <td><select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                <option>请选择</option>
                                                <option>请选择</option>
                                            </select></td>
                                        </tr>
                                        <tr style="text-align:right">
                                            <td>人员状态：</td>
                                            <td><p id="text">在本单位任职</p></td>
                                        </tr>
                                    </tbody>
    							</table>
                                
    						</div>
					    </div>
                        <div id="anchor2">
                            <div data-options="region:'center',border:false,collapsible:true" style="margin-bottom:20px">
                                <div id="toolbarXX">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUserXX()">新 增</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">编 辑</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a>
                                    
                                </div>
                                <table id="dg" class="easyui-datagrid" title="学习经历" iconCls="icon-dingdan" style="width:100%;height:370px; overflow:scroll"
                                    data-options="idField:'id',rownumbers:false,fitColumns:true,pagination:true,singleSelect:false,collapsible:true,toolbar:'#toolbarXX',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
                                
                                <thead>
                                    <tr>
                                        <th field="ck" checkbox="true"></th>
                                        <th data-options="field:'id',width:50">获得学历</th>
                                        <th data-options="field:'shencha',width:50">获得学历的国籍（地区）</th>
                                        <th data-options="field:'chachong',width:50">获得学历的院校或机构</th>
                                        <th data-options="field:'name',width:50">所学专业</th>
                                        <th data-options="field:'named',width:50">入学年月</th>
                                        <th data-options="field:'sex',width:50">毕业年月</th>
                                        <th data-options="field:'num',width:50">学位层次</th>
                                        <th data-options="field:'beizhu',width:50">学位名称</th>
                                        <th data-options="field:'timef',width:50">获得学位的国家（地区）</th>
                                        <th data-options="field:'tim',width:50">获得学位的院校或机构</th>
                                        <th data-options="field:'nam',width:50">学位授予年月</th>
                                        <th data-options="field:'n',width:50">学习方式</th>
                                        <th data-options="field:'nme',width:50">在学单位类别</th>
                                        <th data-options="field:'nae',width:60">创建时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td><td>001</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>002</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>003</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>004</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>005</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>006</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>007</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>008</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>009</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>010</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                </tbody>
                                </table>
                                <div id="dlgXX" class="easyui-dialog" title="新增学习经历" style="width:700px;height:460px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        <div id="table-content">
                                        <form id="fm" method="post" novalidate>
                                        <table class="fitem clean">
                                            <tbody>
                                                <tr>
                                                    <td>获得学历：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                            <option value="请选择">请选择</option>
                                                            <option value="1">1</option>
                                                        </select>
                                                    </td>
                                                    <td>获得学历的国籍（地区）：</td>
                                                    <td>
                                                        <input type="text" class="easyui-searchbox" data-options="searcher:doSearch" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>获得学历的学院或机构：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" required>
                                                    </td>
                                                    <td>所学专业：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>入学年月：</td>
                                                    <td>
                                                        <input type="text" class="easyui-datebox" required>
                                                    </td>
                                                    <td>毕业年月：</td>
                                                    <td>
                                                        <input type="text" class="easyui-datebox" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>学位层次：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                            <option value="请选择">请选择</option>
                                                            <option value="1">1</option>
                                                        </select>
                                                    </td>
                                                    <td>学位名称：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                            <option value="请选择">请选择</option>
                                                            <option value="1">1</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>获得学位的国家(地区)：</td>
                                                    <td>
                                                        <input type="text" class="easyui-searchbox" data-options="searcher:doSearch" required>
                                                    </td>
                                                    <td>获得学位的院校或机构：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>学位授予年月：</td>
                                                    <td>
                                                        <input type="text" class="easyui-datebox" required>
                                                    </td>
                                                    <td>学习方式：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                            <option value="请选择">请选择</option>
                                                            <option value="1">1</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>在学单位类别：</td>
                                                    <td>
                                                        <input type="text" class="easyui-searchbox" data-options="searcher:doSearch" required>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </form>
                                        </div>
                                    </div>
                                    <div id="search" class="easyui-dialog" title="选择在学单位类别" closed="true">
                                        <div class="easyui-panel">
                                            <ul class="easyui-tree">
                                                <li>
                                                    <span>1-全日制学校</span>
                                                    <ul>
                                                        <li>11-全日制小学</li>
                                                        <li>11-全日制中学</li>
                                                        <li>11-全日制中专</li>
                                                        <li>11-全日制高等学校</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgXX').dialog('close')">取消</a>
                                </div>
                            </div>
                        </div>
                        <div id="anchor3">
    						<div data-options="region:'center',border:false,collapsible:true" style="margin-bottom:20px;">
                                <div id="toolbarGZ">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUserGZ()">新 增</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">编 辑</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a>
                                    
                                </div>
                                <table id="dg" title="工作经历" iconCls="icon-dingdan" class="easyui-datagrid" style="width:100%;height:370px;"
                                    data-options="idField:'id',rownumbers:false,fitColumns:true,pagination:true,singleSelect:false,collapsible:true,toolbar:'#toolbarGZ',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
                                
                                <thead>
                                    <tr>
                                        <th field="ck" checkbox="true"></th>
                                        <th data-options="field:'id',width:100">任职单位名称</th>
                                        <th data-options="field:'shencha',width:100">任职开始年月</th>
                                        <th data-options="field:'chachong',width:100">任职结束年月</th>
                                        <th data-options="field:'name',width:100">单位性质类别</th>
                                        <th data-options="field:'named',width:100">任职岗位</th>
                                        <th data-options="field:'sex',width:100">创建时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td><td>001</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>002</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>003</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>004</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>005</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>006</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>007</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>008</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>009</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>010</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                </tbody>
                                </table>
                                <div id="dlgGZ" class="easyui-dialog" title="新增工作经历" style="width:700px;height:300px; overflow:auto;padding:0px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        
                                        <div id="table-content">
                                        <form id="fm" method="post" novalidate>
                                        <table class="fitem">
                                            <tbody>
                                                <tr>
                                                    <td>任职单位名称：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>任职开始年月：</td>
                                                    <td>
                                                        <input type="text" class="easyui-datebox" required>
                                                    </td>
                                                    <td>任职结束年月：</td>
                                                    <td>
                                                        <input type="text" class="easyui-datebox" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>单位性质类别：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                            <option value="请选择">请选择</option>
                                                            <option value="1">1</option>
                                                        </select>
                                                    </td>
                                                    <td>任职岗位：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" required>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgGZ').dialog('close')">取消</a>
                                </div>
                            </div>
                        </div>

                        <div id="anchor4">
                            <div data-options="region:'center',border:false,collapsible:true" style="margin-bottom:20px;">
                                <div id="toolbarGW">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUserGW()">新 增</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">编 辑</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a>
                                    
                                </div>
                                <table id="dg" class="easyui-datagrid" title="岗位聘任" iconCls="icon-dingdan" style="width:100%;height:370px;"
                                    data-options="idField:'id',rownumbers:false,fitColumns:true,pagination:true,singleSelect:false,collapsible:true,toolbar:'#toolbarGW',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
                                
                                <thead>
                                    <tr>
                                        <th field="ck" checkbox="true"></th>
                                        <th data-options="field:'id',width:100">岗位类别</th>
                                        <th data-options="field:'shencha',width:100">岗位等级</th>
                                        <th data-options="field:'chachong',width:100">聘任开始年月</th>
                                        <th data-options="field:'name',width:100">是否双肩挑</th>
                                        <th data-options="field:'named',width:100">是否为辅导员</th>
                                        <th data-options="field:'sex',width:100">党政职务</th>
                                        <th data-options="field:'num',width:100">任职开始年月</th>
                                        <th data-options="field:'beizhu',width:100">党政级别</th>
                                        <th data-options="field:'timef',width:100">创建时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td><td>001</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>002</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>003</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>004</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>005</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>006</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>007</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>008</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>009</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>010</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                </tbody>
                                </table>
                                <div id="dlgGW" class="easyui-dialog" title="新增岗位聘任" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        <div id="table-content">
                                        <form id="fm" method="post" novalidate>
                                        <table class="fitem ">
                                            <tbody>
                                                <tr>
                                                    <td>岗位类别：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                            <option value="请选择">请选择</option>
                                                            <option value="1">1-教师岗位</option>
                                                            <option value="1">2-其他专业技术岗位</option>
                                                            <option value="1">3-管理岗位</option>
                                                            <option value="1">4-工勤技能岗位</option>
                                                            <option value="1">5-特设岗位</option>
                                                        </select>
                                                    </td>
                                                    <td>岗位等级：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                            <option value="请选择">请选择</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>聘任开始年月：</td>
                                                    <td>
                                                        <input type="text" class="easyui-datebox" required>
                                                    </td>
                                                    <td>是否双肩挑：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                            <option value="请选择">0-否</option>
                                                            <option value="1">请选择</option>
                                                            <option value="1">0-否</option>
                                                            <option value="1">1-是</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>是否为辅导员：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                            <option value="请选择">请选择</option>
                                                        </select>
                                                    </td>
                                                    <td>党政职务：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                            <option value="请选择">请选择</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>任职开始年月：</td>
                                                    <td>
                                                        <input type="text" class="easyui-datebox" required>
                                                    </td>
                                                    <td>党政级别：</td>
                                                    <td>
                                                        <input type="text" class="easyui-searchbox" data-options="searcher:doSearch" required>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgGW').dialog('close')">取消</a>
                                </div>
                            </div>
                        </div>

                        <div id="anchor5">
                            <div data-options="region:'center',border:false,collapsible:true" style="margin-bottom:20px;">
                                <div id="toolbarZY">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUserZY()">新 增</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">编 辑</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a>
                                    
                                </div>
                                <table id="dg" class="easyui-datagrid" title="专业技术职务聘任" iconCls="icon-dingdan" style="width:100%;height:370px;"
                                    data-options="idField:'id',rownumbers:false,fitColumns:true,pagination:true,singleSelect:false,collapsible:true,toolbar:'#toolbarZY',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
                                
                                <thead>
                                    <tr>
                                        <th field="ck" checkbox="true"></th>
                                        <th data-options="field:'id',width:100">聘任专业技术职务</th>
                                        <th data-options="field:'shencha',width:100">聘任开始年月</th>
                                        <th data-options="field:'name',width:100">聘任单位名称</th>
                                        <th data-options="field:'named',width:100">创建时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td><td>001</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>002</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>003</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>004</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>005</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>006</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>007</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>008</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>009</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>010</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                </tbody>
                                </table>
                                <div id="dlgZY" class="easyui-dialog" title="新增专业技术职务聘任" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        
                                        <div id="table-content">
                                        <form id="fm" method="post" novalidate>
                                        <table class="fitem clean">
                                            <tbody>
                                                <tr>
                                                    <td>聘任专业技术职务：</td>
                                                    <td>
                                                        <input type="text" class="easyui-searchbox" data-options="searcher:doSearch" required>
                                                    </td>
                                                    <tr>
                                                    <td>聘任单位名称：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" required>
                                                    </td>
                                                </tr>
                                                </tr>
                                                <tr>
                                                	<td>聘任开始年月：</td>
                                                    <td>
                                                        <input type="text" class="easyui-datebox" required>
                                                    </td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgZY').dialog('close')">取消</a>
                                </div>
                            </div>
                        </div>

                       
                        <div id="anchor6">
                            <div data-options="region:'center',border:false,collapsible:true" style="margin-bottom:20px;">
                                <div id="toolbarND">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUserND()">新 增</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">编 辑</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a>
                                    
                                </div>
                                <table id="dg" class="easyui-datagrid" title="年度考核" iconCls="icon-dingdan" style="width:100%;height:370px;"
                                    data-options="idField:'id',rownumbers:false,fitColumns:true,pagination:true,singleSelect:false,collapsible:true,toolbar:'#toolbarND',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
                                
                                <thead>
                                    <tr>
                                        <th field="ck" checkbox="true"></th>
                                        <th data-options="field:'id',width:100">考核年度</th>
                                        <th data-options="field:'scha',width:100">考核单位名称</th>
                                        <th data-options="field:'shencha',width:100">考核结果</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td><td>001</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>002</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>003</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>004</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>005</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>006</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>007</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>008</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>009</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>010</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                </tbody>
                                </table>
                                <div id="dlgND" class="easyui-dialog" title="新增年度考核" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        
                                        <div id="table-content">
                                        <form id="fm" method="post" novalidate>
                                        <table class="fitem clean">
                                            <tbody>
                                                <tr>
                                                    <td>考核年度：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                            <option value="请选择">请选择</option>
                                                        </select>
                                                    </td>
                                                    <td>考核结果：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                            <option value="请选择">请选择</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>考核单位名称：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" required>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgND').dialog('close')">取消</a>
                                </div>
                            </div>
                        </div>

                        <div id="anchor7">
                            <div data-options="region:'center',border:false,collapsible:true" style="margin-bottom:20px;">
                                <div id="toolbarJS">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUserJS()">新 增</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">编 辑</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a>
                                    
                                </div>
                                <table id="dg" class="easyui-datagrid" title="教师资格" iconCls="icon-dingdan" style="width:100%;height:370px;"
                                    data-options="idField:'id',rownumbers:false,fitColumns:true,pagination:true,singleSelect:false,collapsible:true,toolbar:'#toolbarJS',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
                                
                                <thead>
                                    <tr>
                                        <th field="ck" checkbox="true"></th>
                                        <th data-options="field:'id',width:100">教师资格证种类</th>
                                        <th data-options="field:'shencha',width:100">教师资格证号码</th>
                                        <th data-options="field:'chachong',width:100">任教学科</th>
                                        <th data-options="field:'name',width:100">证书颁发日期</th>
                                        <th data-options="field:'named',width:100">证书认定机构</th>
                                        <th data-options="field:'sex',width:100">创建时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td><td>001</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>002</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>003</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>004</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>005</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>006</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>007</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>008</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>009</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>010</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                </tbody>
                                </table>
                                <div id="dlgJS" class="easyui-dialog" title="新增教师资格" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        
                                        <div id="table-content">
                                        <form id="fm" method="post" novalidate>
                                        <table class="fitem clean">
                                            <tbody>
                                                <tr>
                                                    <td>教师资格证种类：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                            <option value="请选择">请选择</option>
                                                        </select>
                                                    </td>
                                                    <td>教师资格证号码：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>任教学科：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" required>
                                                    </td>
                                                    <td>证书颁发日期：</td>
                                                    <td>
                                                        <input type="text" class="easyui-datebox" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>认定机构：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" required>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgJS').dialog('close')">取消</a>
                                </div>
                            </div>
                        </div>

                      
                        <div id="anchor8">
                            <div data-options="region:'center',border:false,collapsible:true" style="margin-bottom:20px;">
                                <div id="toolbarJY">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUserJY()">新 增</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">编 辑</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a>
                                    
                                </div>
                                <table id="dg" class="easyui-datagrid" title="教育教学" iconCls="icon-dingdan" style="width:100%;height:370px;"
                                    data-options="idField:'id',rownumbers:false,fitColumns:true,pagination:true,singleSelect:false,collapsible:true,toolbar:'#toolbarJY',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
                                
                                <thead>
                                    <tr>
                                        <th field="ck" checkbox="true"></th>
                                        <th data-options="field:'id',width:60">年 度</th>
                                        <th data-options="field:'shencha',width:60">学期</th>
                                        <th data-options="field:'chachong',width:100">导师类别</th>
                                        <th data-options="field:'name',width:130">现主要从事学科领域</th>
                                        <th data-options="field:'named',width:130">是否为本科生上课</th>
                                        <th data-options="field:'na',width:100">任课状况</th>
                                        <th data-options="field:'sex',width:120">教程教学课时数</th>
                                        <th data-options="field:'num',width:100">创建时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td><td>001</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>002</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>003</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>004</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>005</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>006</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>007</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>008</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>009</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>010</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                </tbody>
                                </table>
                                <div id="dlgJY" class="easyui-dialog" title="新增教育教学" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        
                                        <div id="table-content">
                                        <form id="fm" method="post" novalidate>
                                        <table class="fitem clean">
                                            <tbody>
                                                <tr>
                                                    <td>年度：</td>
                                                    <td>
                                                        <input type="text" class="easyui-datebox" required>
                                                    </td>
                                                    <td>学期：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                            <option value="请选择">请选择</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>导师类别：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                            <option value="请选择">请选择</option>
                                                        </select>
                                                    </td>
                                                    <td>现主要从事学科领域：</td>
                                                    <td>
                                                        <input type="text" class="easyui-searchbox" data-options="searcher:doSearch" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>是否为本科生上课：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                            <option value="请选择">请选择</option>
                                                        </select>
                                                    </td>
                                                    <td>任课状况</td>
                                                    <td>
                                                        <input type="text" class="easyui-searchbox" data-options="searcher:doSearch" required>
                                                    </td>
                                                    
                                                </tr>
                                                <tr>
                                                	<td>课程教学课时数：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" required>
                                                    </td>
                                                    <td>其他工作折合课时数：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" required>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgJY').dialog('close')">取消</a>
                                </div>
                            </div>
                        </div>

                        <div id="anchor10">
                            <div data-options="region:'center',border:false,collapsible:true" style="margin-bottom:20px;">
                                <div id="toolbarRX">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUserRX()">新 增</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">编 辑</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a>
                                    
                                </div>
                                <table id="dg" class="easyui-datagrid" title="入选人才项目" iconCls="icon-dingdan" style="width:100%;height:370px;"
                                    data-options="idField:'id',rownumbers:false,fitColumns:true,pagination:true,singleSelect:false,collapsible:true,toolbar:'#toolbarRX',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
                                
                                <thead>
                                    <tr>
                                        <th field="ck" checkbox="true"></th>
                                        <th data-options="field:'id',width:100">入选人才项目名称</th>
                                        <th data-options="field:'shencha',width:100">入选年份</th>
                                        <th data-options="field:'chachong',width:100">创建时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td><td>001</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>002</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>003</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>004</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>005</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>006</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>007</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>008</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>009</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>010</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                </tbody>
                                </table>
                                <div id="dlgRX" class="easyui-dialog" title="新增入选人才项目" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        
                                        <div id="table-content">
                                        <form id="fm" method="post" novalidate>
                                        <table class="fitem clean">
                                            <tbody>
                                                <tr>
                                                    <td>入选人才项目名称：</td>
                                                    <td>
                                                        <input type="text" class="easyui-searchbox" data-options="searcher:doSearch" required>
                                                    </td>
                                                    <td>入选年份：</td>
                                                    <td>
                                                        <input type="text" class="easyui-datebox" required>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgRX').dialog('close')">取消</a>
                                </div>
                            </div>
                        </div>

                       
                        <div id="anchor12">
                            <div data-options="region:'center',border:false,collapsible:true" style="margin-bottom:20px;">
                                <div id="toolbarHW">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUserHW()">新 增</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">编 辑</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a>
                                    
                                </div>
                                <table id="dg" class="easyui-datagrid" title="海外研修（访学）" iconCls="icon-dingdan" style="width:100%;height:370px;"
                                    data-options="idField:'id',rownumbers:false,fitColumns:true,pagination:true,singleSelect:false,collapsible:true,toolbar:'#toolbarHW',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
                                
                                <thead>
                                    <tr>
                                        <th field="ck" checkbox="true"></th>
                                        <th data-options="field:'id',width:150">是否有海外研修(访学)经历</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td><td>001</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>002</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>003</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>004</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>005</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>006</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>007</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>008</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>009</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>010</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                </tbody>
                                </table>
                                <div id="dlgHW" class="easyui-dialog" title="新增海外研修（访学）" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        
                                        <div id="table-content">
                                        <form id="fm" method="post" novalidate>
                                        <table class="fitem clean">
                                            <tbody>
                                                <tr>
                                                    <td>是否有海外研修(访学)经历：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                            <option value="请选择">请选择</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgHW').dialog('close')">取消</a>
                                </div>
                            </div>
                        </div>

                        <div id="anchor13">
                            <div data-options="region:'center',border:false,collapsible:true" style="margin-bottom:20px;">
                                
                                <div id="toolbarJN">
                                	<h3 style="text-indent:10px">语言能力</h3>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUserJN()">新 增</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">编 辑</a>
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a>
                                    
                                </div>
                                <table id="dg" class="easyui-datagrid" title="技能及证书" iconCls="icon-dingdan" style="width:100%;height:420px;"
                                    data-options="idField:'id',rownumbers:false,fitColumns:true,pagination:true,singleSelect:false,collapsible:true,toolbar:'#toolbarJN',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
                                
                                <thead>
                                    <tr>
                                        <th field="ck" checkbox="true"></th>
                                        <th data-options="field:'id',width:100">语种</th>
                                        <th data-options="field:'shencha',width:100">掌握程度</th>
                                        <th data-options="field:'chachong',width:100">创建时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td><td>001</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>002</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>003</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>004</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>005</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>006</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>007</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>008</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>009</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>010</td><td>审核通过</td><td>查重通过</td><td>李梅</td><td>无</td><td>女</td><td>110111000</td><td>无</td><td>2016-9-6</td>
                                    </tr>
                                </tbody>
                                </table>
                                <div id="dlgJN" class="easyui-dialog" title="新增语言能力" style="width:700px;height:300px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                                    <div class="right_table">
                                        
                                        <div id="table-content">
                                        <form id="fm" method="post" novalidate>
                                        <table class="fitem clean">
                                            <tbody>
                                                <tr>
                                                    <td>语种：</td>
                                                    <td>
                                                        <input type="text" class="easyui-searchbox" data-options="searcher:doSearch" required>
                                                    </td>
                                                    <td>掌握程度：</td>
                                                    <td>
                                                        <select class="easyui-combobox" required="true" name="" id="">
                                                <option></option>
                                                            <option value="请选择">请选择</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                <div id="dlg-buttons">
                                    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()">保存</a>
                                    <a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlgJN').dialog('close')">取消</a>
                                </div>
                            </div>
                        </div>

                        <div id="anchor14">
                            <div data-options="region:'center',border:false,collapsible:true" style="margin-bottom:20px;width:100%" class="easyui-panel" title="联系方式" iconCls="icon-dingdan">
                            	<div id="toolbarzx" style=" border-bottom:1px dotted #ccc">
								<a href="#" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="newUser()">保 存</a>
							    </div>
                                <table id="dg" class="fitem" style="width:600px;height:120px;"
                                    data-options="idField:'id',rownumbers:false,fitColumns:true,pagination:true,singleSelect:false,collapsible:true,toolbar:'#toolbar',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
                                    <tbody>
                                        <tr>
                                            <td>手机：</td>
                                            <td>
                                                <input type="text" class="easyui-textbox" required>
                                            </td>
                                            <td>Email：</td>
                                            <td>
                                                <input type="text" class="easyui-textbox" required>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>  
<script type="text/javascript">


var url;
function save_user(dlgNmae){
    $(dlgNmae).dialog('open');
    $('#fm').form('clear');
    url = 'save_user.php';
}
//新增
function newUserXX(){
    save_user('#dlgXX');
}
function newUserGZ(){
    save_user('#dlgGZ');
}
function newUserGW(){
    save_user('#dlgGW');
}
function newUserZY(){
    save_user('#dlgZY');
}
function newUserND(){
    save_user('#dlgND');
}
function newUserJS(){
    save_user('#dlgJS');
}
function newUserJY(){
    save_user('#dlgJY');
}
function newUserJX(){
    save_user('#dlgJX');
}
function newUserRX(){
    save_user('#dlgRX');
}
function newUserGN(){
    save_user('#dlgGN');
}
function newUserHW(){
    save_user('#dlgHW');
}
function newUserJN(){
    save_user('#dlgJN');
}
function newUserLX(){
    save_user('#dlgLX');
}
//编辑
function editUser(){
    var row = $('#dg').datagrid('getSelected');
    if (row){
        $('#dlg').dialog('open').dialog('setTitle','编辑用户');
        $('#fm').form('load',row);
        url = 'update_user.php?id='+row.id;
    }
}
//删除
function removeUser(){
    var row = $('#dg').datagrid('getSelected');
    if (row){
        $.messager.confirm('Confirm','Are you sure you want to remove this user?',function(r){
            if (r){
                $.post('remove_user.php',{id:row.id},function(result){
                    if (result.success){
                        $('#dg').datagrid('reload');    // reload the user data
                    } else {
                        $.messager.show({   // show error message
                            title: 'Error',
                            msg: result.msg
                        });
                    }
                },'json');
            }
        });
    }
}
//保存
function saveUser(){
    $('#fm').form('submit',{
        url: url,
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            var result = eval('('+result+')');
            if (result.success){
                $('#dlg').dialog('close');      // close the dialog
                $('#dg').datagrid('reload');    // reload the user data
            } else {
                $.messager.show({
                    title: 'Error',
                    msg: result.msg
                });
            }
        }
    });
}
//搜索
function doSearch(){
    $('#search').dialog('open');
}

function addTab(title, url){
	if ($('#tb').tabs('exists', title)){
		$('#tb').tabs('select', title);
	} else {
		var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
		$('#tb').tabs('add',{
			title:title,
			content:content,
			closable:true
		});
	}
}
//关闭所有的tab  
    function closeAll(){  
        var tiles = new Array();  
          var tabs = $('#tb').tabs('tabs');      
          var len =  tabs.length;           
          if(len>0){  
            for(var j=0;j<len;j++){  
                var a = tabs[j].panel('options').title;               
                tiles.push(a);  
            }  
            for(var i=1;i<tiles.length;i++){               
                $('#tb').tabs('close', tiles[i]);  
            }
          }
    } 
//关闭当前的tab 
	function removePanel(){
		var tab = $('#tb').tabs('getSelected');
		    if (tab){
			var index = $('#tb').tabs('getTabIndex', tab);
			$('#tb').tabs('close', index);
			}
		}
//表格分页	
var pager = $('#dg').datagrid('getPager');    // get the pager of datagrid
	pager.pagination({
		showPageList:false,
		buttons:[{
			iconCls:'icon-search',
			handler:function(){
				alert('search');
			}
		},{
			iconCls:'icon-add',
			handler:function(){
				alert('add');
			}
		},{
			iconCls:'icon-edit',
			handler:function(){
				alert('edit');
			}
		}],
		onBeforeRefresh:function(){
			alert('before refresh');
			return true;
		}
	});	
</script>
</body>
</html>