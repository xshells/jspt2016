<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>北京市教师管理服务平台</title>
    <link rel="stylesheet" type="text/css" href="../css/newcss.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="../easyui/themes/xyy.css">
    <script type="text/javascript" src="../easyui/jquery.min.js"></script>
    <script type="text/javascript" src="../easyui/easyloader.js"></script>
    <script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../easyui/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx}/js/refresh.js"></script>
<script type="text/javascript">
		var url;
		function newUser(){
			$('#dlg').dialog('open').dialog('setTitle','编辑单位信息');
			$('#fm').form('clear');
			url = 'save_user.php';
		}
		function editUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg').dialog('open').dialog('setTitle','编辑用户');
				$('#fm').form('load',row);
				url = 'update_user.php?id='+row.id;
			}
		}
		function saveUser(){
			$('#fm').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.success){
						$('#dlg').dialog('close');		// close the dialog
						$('#dg').datagrid('reload');	// reload the user data
					} else {
						$.messager.show({
							title: 'Error',
							msg: result.msg
						});
					}
				}
			});
		}
		function removeUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$.messager.confirm('Confirm','Are you sure you want to remove this user?',function(r){
					if (r){
						$.post('remove_user.php',{id:row.id},function(result){
							if (result.success){
								$('#dg').datagrid('reload');	// reload the user data
							} else {
								$.messager.show({	// show error message
									title: 'Error',
									msg: result.msg
								});
							}
						},'json');
					}
				});
			}
		}
	</script>
</head>
<body class="easyui-layout" style="margin:0 1px;background-color:#f5f5f5 !important; margin-top:20px">
		        <div class="easyui-layout" data-options="fit:true">
					<div data-options="region:'center',border:false">
					<form action="${ctx}/sysuserManager/SysuserManageAction.a" id="frm" method="post">
                    	<div>
    						<div class="easyui-panel" data-options="region:'center',border:false,collapsible:true" iconCls="icon-dingdan" title="查询条件"  style="margin-bottom:0px; width:100%">
                            	<div id="toolbarxx" style=" border-bottom:1px dotted #ccc">
								<a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="submit()">查 询</a>
								<a href="#" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="resetQuery()">重 置</a>
							    
                                </div>
                                <table id="dg1" class="fitem" data-options="idField:'id',rownumbers:false,fitColumns:true,pagination:true,singleSelect:false,toolbar:'#toolbar',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
        								<tr>
                                        	<td>用户账号</td>
                                            <th><input type="text" class="easyui-textbox" name="accountQuery" value="${accountQuery }"></th>
<!--                                             <td>用户姓名</td> -->
<%--                                             <th><input type="text" class="easyui-textbox" name="nameQuery" value="${nameQuery }"></th> --%>
                                            <td>用户范围</td>
                                            <th>
<!--                                             	<select class="easyui-combobox"  name="yhfwQuery"> -->
<!-- 		                                            <option value=" ">请选择</option> -->
<%-- 		                                            <app:dictselect dictType="YHLX" selectValue="${yhfwQuery }"/> --%>
<!-- 		                                        </select> -->
												<c:if test="${userType ==9}">
													<select class="easyui-combobox"  name="yhfwQuery" id="yhfwQuery">
		                                            	  <option value="">全部</option>
		                                                  <c:forEach items="${fwList }" var="list">
		                                                  		<c:if test="${list.id != 9}">
		                                                  		<option value="${list.id }"
		                                                  		<c:if test="${list.id == yhfwQuery }">selected</c:if>
		                                                  		>${list.id }-${list.name }</option>
		                                                  		</c:if>
		                                                  </c:forEach>
		                                               </select>
												</c:if>
												<c:if test="${userType ==1 }">
													<select class="easyui-combobox"  name="yhfwQuery" id="yhfwQuery">
		                                            	  <option value="">全部</option>
		                                                  <c:forEach items="${fwList }" var="list">
		                                                  		<c:if test="${list.id >= userType && list.id != 9}">
		                                                  		<option value="${list.id }"
		                                                  		<c:if test="${list.id == yhfwQuery }">selected</c:if>
		                                                  		>${list.id }-${list.name }</option>
		                                                  		</c:if>
		                                                  </c:forEach>
		                                               </select>
												</c:if>
												<c:if test="${userType == 2}">
													<select class="easyui-combobox"  name="yhfwQuery" id="yhfwQuery">
														<option value="">全部</option>
	                                                  <c:forEach items="${fwList }" var="list">
	                                                  	<c:if test="${list.id >= userType && list.id != 9}">
	                                                  		<option value="${list.id }"
	                                                  		<c:if test="${list.id == yhfwQuery }">selected</c:if>
	                                                  		>${list.id }-${list.name }</option>
	                                                  	</c:if>
	                                                  </c:forEach>
	                                               </select>
												</c:if>
                                            </th>
                                        </tr>
                                        <tr>
                                        	<td>用户角色</td>
                                            <th>
                                            	<select class="easyui-combobox"  name="roleIdQuery" id="roleIdQuery">
                                            	  <option value="">全部</option>
                                                  <c:forEach items="${roleList }" var="list">
                                                  		<option value="${list.roleId }"
                                                  		<c:if test="${list.roleId == roleIdQuery }">selected</c:if>
                                                  		>${list.roleName }</option>
                                                  </c:forEach>
                                               </select>
                                            </th>
                                            <td>账号状态</td>
                                            <th>
                                            	<select class="easyui-combobox"  name="statusQuery" id="statusQuery">
                                            		<option value="2"
                                            		<c:if test="${statusQuery==2 }">selected</c:if>
                                            		>全部</option>
                                                  	<option value="1"
                                                  		<c:if test="${statusQuery==1 }">selected</c:if>
                                                  		>启用</option>
                                                  	<option value="0"
                                                  		<c:if test="${statusQuery==0 }">selected</c:if>
                                                  		>禁用</option>
                                               </select>
                                            </th>
                                            <td>所属学段</td>
                                            <th>
	                                            <select class="easyui-combobox"  name="affiliatedSchoolQuery" id="affiliatedSchoolQuery" value="${affiliatedSchoolQuery }" >
		                                            <option value=" ">请选择</option>

		                                        <c:choose>
		                                         	<c:when test="${userType ==2 }">
		                                         	   <option value="30" <c:if test="${affiliatedSchoolQuery==30}">selected=true</c:if>>30-中小学校</option>
		                                         	   <option value="40" <c:if test="${affiliatedSchoolQuery==40}">selected=true</c:if>>40-中等职业学校</option>
		                                         	   <option value="50" <c:if test="${affiliatedSchoolQuery==50}">selected=true</c:if>>50-特殊教育学校</option>
		                                         	   <option value="60" <c:if test="${affiliatedSchoolQuery==60}">selected=true</c:if>>60-幼儿园</option>
		                                         	</c:when>
		                                         	<c:otherwise> 
		                                            <app:dictselect dictType="XXJG_SSXD" selectValue="${affiliatedSchoolQuery }"/>
		                                         	</c:otherwise>
		                                         </c:choose> 

		                                        </select>
                                       	 	</th>
                                        </tr>
    							</table>
                            
    						</div>
					    </div>
                        
                        
                        
                        
                        
						<div  data-options="region:'center',border:false" style="margin-top:20px;">
                            <table id="dg" title="用户列表" iconCls="icon-dingdan" class="easyui-datagrid" style="width:100%;"
								data-options="idField:'id',rownumbers:false,fitColumns:true,singleSelect:false,collapsible:true,toolbar:'#toolbar',method:'get',checkOnSelect:'true',selectOnCheck:'true'">
							<!---获取数据--->
							<thead>
								<tr>
								<th field="ck" checkbox="true"></th>
                                <th data-options="field:'id',width:50">序号</th>
                                <th data-options="field:'ndsfme',width:100">所属单位</th>
<!--                                 <th data-options="field:'nadfge',width:100">用户姓名</th> -->
                                <th data-options="field:'ndfme',width:100">用户账号</th>
                                <th data-options="field:'nbme',width:100">用户范围</th>
                                <th data-options="field:'nbhme',width:100">用户角色</th>
                                <th data-options="field:'nbmghge',width:100">所属学段</th>
                                <th data-options="field:'najjh',width:100">账号状态</th>
								</tr>
							</thead>
							<tbody>
							<c:if test="${not empty pageObj.pageElements}">
									<c:forEach items="${pageObj.pageElements}" var="item" varStatus="status">
										<tr>
											<td>${item.userId}</td>
											<td>${item.userId}</td>
											<td>${item.jgname}</td>
<%-- 											<td>${item.name}</td> --%>
											<td>${item.account}</td>
											<td><app:dictname dictid="YHLX${item.yhfw}"/></td>
											<td>${item.roleName}</td>
											<td><app:dictname dictid="XXJG_SSXD${item.xd}"/></td>
											<td>
												<c:choose>
													<c:when test="${item.status ==1}">
														启用
													</c:when>
													<c:otherwise>
														禁用
													</c:otherwise>
												</c:choose>
											</td>
										</tr>
									</c:forEach>
								</c:if>
								<c:if test="${empty pageObj.pageElements}">
									<tr>
										<td colspan="12" class="prompt">未查询到符合条件的记录。</td>
									</tr>
								</c:if>
								
							</tbody>
							</table>
							<div id="toolbar">
<!-- 								<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUser()">新 增</a> -->
<!-- 								<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">编 辑</a> -->
<!-- 								<a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()">删 除</a> -->
								<a href="#" class="easyui-linkbutton" iconCls="icon-ok" plain="true" onclick="setStatus(1)">启 用</a>
                                <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="true" onclick="setStatus(0)">禁用</a>
                                <a href="#" class="easyui-linkbutton" iconCls="icon-lock" plain="true" onclick="reset()">密码重置</a>
							</div>
							<!-- 分页所必须的 -->
							<div class="page tar mb15">
								<input id="pageNo" name="pageNo" value="${pageNo }" type="hidden" />
								<c:if test="${not empty pageObj.pageElements}">
									<jsp:include page="../common/pager-nest.jsp">
										<jsp:param name="toPage" value="1" />
										<jsp:param name="showCount" value="1" />
										<jsp:param name="action" value="doSearch" />
									</jsp:include>
								</c:if>
							</div>
						</div>
                        </form>
                        <!-- 添加页面   开始 -->
						<div id="dlg" class="easyui-dialog" style="width:700px;height:400px; overflow:auto;padding:10px 20px" closed="true" buttons="#dlg-buttons" data-options="modal:true">
                               <form action="${ctx}/districtManager/InstitutionsManageAction.a?add" id="addfrm" method="post">
                                <div class="right_table">
                                    <div id="table-content">
                                    <!-- <form id="fm" method="post" novalidate> -->
                                    <table class="fitem clean">
                                            <tbody>
                                                <tr>
                                                    <td>用户帐号：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="sysuserDto.account" id="account" required />
                                                    </td>
                                                    <td>用户姓名：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="sysuserDto.name" id="name"  required />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>所属单位：</td>
                                                    <td>
                                                        <input type="text" class="easyui-textbox" name="" />
                                                    </td>
                                                    <td>用户范围：</td>
                                                    <td>
	                                                    <select class="easyui-combobox"  name="sysuserDto.yhfw" id="yhfw">
				                                            <option value="">请选择</option>
				                                            <app:dictselect dictType="YHLX"/>
				                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                	<td>用户角色：</td>
                                                    <td colspan="1">
                                                        <select class="easyui-combobox" required="true" name="sysuserDto.roleId" id="role" >
		                                                  <c:forEach items="${roleList }" var="list">
		                                                  		<option value="${list.roleId }">${list.roleName }</option>
		                                                  </c:forEach>
		                                               </select>
                                                    </td>
                                                	<td>密码：</td>
                                                    <td>
                                                         <input type="password" class="easyui-textbox" name="sysuserDto.password" id="pwd"  required />
                                                    </td>
                                                </tr>
                                                <tr>
                                                	<td>再次输入密码：</td>
                                                    <td>
                                                        <input type="password" class="easyui-textbox" id="pwd2" required />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    <!-- </form> -->
                                    </div>
                                </div>
									<input type="hidden" name="sysuserDto.userId" id="userId" class="easyui-textbox"/>
                                 </form>
							</div>
							<div id="dlg-buttons">
								<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()">保存</a>
								<a href="#" class="easyui-linkbutton mr20" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">关闭</a>
							</div>
							<!-- 添加页面   结束 -->
					</div>
				</div>
				<div id="tips" class="easyui-dialog" title="温馨提示" style="padding:10px 20px;width:220px;height:120px;line-height:40px;text-align:center;overflow:auto;font-size:20px;" closed="true" data-options="modal:true">请选择一条记录</div>
<script type="text/javascript">

$('#affiliatedSchoolQuery').combobox({ editable:false }); //所属学段
$('#yhfwQuery').combobox({ editable:false }); //用户范围
/* $('#yhfwQuery2').combobox({ editable:false });
$('#yhfwQuery9').combobox({ editable:false }); */
$('#statusQuery').combobox({ editable:false });
$('#roleIdQuery').combobox({ editable:false });
$('#role').combobox({ editable:false });



function addTab(title, url){
	if ($('#tb').tabs('exists', title)){
		$('#tb').tabs('select', title);
	} else {
		var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
		$('#tb').tabs('add',{
			title:title,
			content:content,
			closable:true
		});
	}
}
//关闭所有的tab  
    function closeAll(){  
        var tiles = new Array();  
          var tabs = $('#tb').tabs('tabs');      
          var len =  tabs.length;           
          if(len>0){  
            for(var j=0;j<len;j++){  
                var a = tabs[j].panel('options').title;               
                tiles.push(a);  
            }  
            for(var i=1;i<tiles.length;i++){               
                $('#tb').tabs('close', tiles[i]);  
            }  
          }  
    } 
//关闭当前的tab 
	function removePanel(){
		var tab = $('#tb').tabs('getSelected');
		    if (tab){
			var index = $('#tb').tabs('getTabIndex', tab);
			$('#tb').tabs('close', index);
			}
		}
/////////////////////////////////
	function tips(dlgNmae,msg){
		$(dlgNmae).html(msg).dialog('open');
	};
	
	function setStatus(s){
		var selected=$("input[type='checkbox'][name='ck']:checked");
		if(selected.length<=0){
			$.messager.alert('温馨提示','请选择一条记录');
			return ;
		}
		
		var frmObj=$("#frm");
		frmObj.attr("action","${ctx}/sysuserManager/SysuserManageAction.a?setStatus&status=" + s);
		frmObj.submit();
	}
	
	function reset(){
		var selected=$("input[type='checkbox'][name='ck']:checked");
		if(selected.length<=0){

			$.messager.alert("温馨提示","请选择一条记录",'info');

			return ;
		}

		$.messager.confirm('温馨提示','确认重置密码?',function(r){
			if(r){
				$('#frm').form('submit',{
		           url: "${ctx}/sysuserManager/SysuserManageAction.a?reset",
		           onSubmit: function(){
		               return true;
		           },
		           success: function(){
		        	   $.messager.alert('温馨提示', '重置完成',function(){
		        		   window.location.reload();
		        	   });
		           }
		       });
			}
		})

	}
	
	function submit(){
		$("#frm").submit();
	}
	
	/* 重置 */
	function resetQuery(){
		$("input[name='accountQuery']").val("");
		$("input[name='nameQuery']").val("");
		$("select[name='yhfwQuery']").combobox('setValue',"");
		$("select[name='roleIdQuery']").combobox('setValue',"");
		$("select[name='statusQuery']").combobox('setValue',"");
		$("select[name='affiliatedSchoolQuery']").combobox('setValue',"");
		window.location.href = "${ctx}/sysuserManager/SysuserManageAction.a";
	}
//////暂时废弃功能
///////////////////////////////////////////////////	
	var url;
	//表单提交
	
	/* 弹出新增框 */
	function newUser(){
		$('#dlg').dialog('open').dialog('setTitle','新增用户');
		$('#fm').form('clear');
	}
	
	/* 弹出编辑框 */
	function editUser(){
		var selected=$("input[type='checkbox'][name='ck']:checked");
		
		if(selected.length<=0){
			tips("#tips","请选择一条记录");
			return ;
		}else if(selected.length>1){
			tips("#tips","只能编辑一条记录");
			return ;
		}else{	
			var userId=selected.val();
			$.ajax({
			    url:"${ctx}/sysuserManager/SysuserManageAction.a?toEdit&time="+new Date().getTime(),    //请求的url地址
			    dataType:"json",   //返回格式为json
			    data:{"userId":userId},    //参数值
			    type:"post",   //请求方式
			    success:function(data){
			    	console.log(data);
			        $("#account").textbox('setValue',data.ins.account);
			        $("#name").textbox('setValue',data.ins.name);
			        $("#role").combobox('setValue',data.ins.roleId);
			        $("#yhfw").combobox('setValue',data.ins.yhfw);
			        $("#jbzlx").combobox('setValue',data.ins.jbzlx);
			        $("#userId").textbox('setValue',data.ins.userId);
			        $("#pwd").textbox('setValue',data.ins.password);
			        $("#pwd2").textbox('setValue',data.ins.password);
			    },
			    error:function(XMLHttpRequest, textStatus, errorThrown){
					var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
					sessionTimeout(sessionstatus);
				}
			});
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg').dialog('open').dialog('setTitle','编辑用户');
				$('#fm').form('load',row);
				url = 'update_user.php?id='+row.id;
			}
		}
		
	}
	/* 新增或者编辑的保存方法 */
	function saveUser(){
		var account=$("#account").val();
		$.ajax({
		    url:"${ctx}/sysuserManager/SysuserManageAction.a?isExist&time="+new Date().getTime(),    //请求的url地址
		    dataType:"json",   //返回格式为json
		    data:{"account":account},    //参数值
		    type:"post",   //请求方式
		    success:function(data){
		    	console.log(data);
			    var userId=$("#userId").val();
		    	if(data.is == "0" || data.is == 0){
					if(userId!=null&&userId>0){
						var frmObj=$("#addfrm");
						frmObj.attr("action","${ctx}/sysuserManager/SysuserManageAction.a?edit");
						frmObj.submit();
					}else{
						var frmObj=$("#addfrm");
						frmObj.attr("action","${ctx}/sysuserManager/SysuserManageAction.a?add");
						frmObj.submit();
					} 
		    	}else{
		    		tips("#tips","单位代码已存在，请重新填写！");
		    	}
		    },
		    error:function(XMLHttpRequest, textStatus, errorThrown){
				var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
				sessionTimeout(sessionstatus);
			}
		});
	}
	
	/* 删除 */
	function removeUser(){
		var selected=$("input[type='checkbox'][name='ck']:checked");
		if(selected.length<=0){
			tips("#tips","请选择一条记录");
			return ;
		}
		var node = $('#dg').datagrid('getSelections')
		for(var i=0;i<node.length;i++){
			var isTrueStr = node[i].nacxgme;
			var isTrue = isTrueStr.slice(-3,-2);
			if(!isTrue){
				$('#dg').datagrid('unselectRow',node[i].index);
			}
		}
		
		$.messager.confirm('温馨提示', '是否要删除', function(r){
			if(r){
				var frmObj=$("#frm");
				frmObj.attr("action","${ctx}/sysuserManager/SysuserManageAction.a?delete");
				frmObj.submit();
			}
		});
	}
	
</script>
</body>
</html>
