/**
 * TbBizJzgjbxxDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

import java.io.Serializable;
import java.util.Date;

import com.becom.jspt2016.model.TbXxjgb;

/**
 * 教职工基本信息
 *
 * @author   zhangchunming
 * @Date	 2016年10月22日 	 
 */
public class TbBizJzgjbxxDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private long jsid;
	private TbXxjgb tbXxjgb;
	private Long personalId;
	private String eduId;
	private String jxjybh;
	private String jzgh;
	private String sfzz;
	private String xm;
	private String cym;
	private String xb;
	private String gjdq;
	private String sfzjlx;
	private String sfzjh;
	private String csrq;
	private String jg;
	private String csd;
	private String mz;
	private String zzmm;
	private String hyzk;
	private String jkzk;
	private String zgxl;
	private String hdzgxldyxhjg;
	private String zgxwcc;
	private String zgxxmc;
	private String hdzgxwdyxhjg;
	private String cjgzny;
	private String cstjqsny;
	private String jbxny;
	private String jzgly;
	private String xyjg;
	private String jzglb;
	private String sfzb;
	private String yrxs;
	private String qdhtqk;
	private String szejdw;
	private String xrgwlb;
	private String xrgwdj;
	private String xrzyjszw;
	private String sfsflzyby;
	private String sfqrztsyjzyby;
	private String sfsgtjzypypx;
	private String sfytsjycyzs;
	private String sfxqjyzyby;
	private String sfsgxqjyzypypx;
	private String xxjsyynl;
	private String sfsymfsfs;
	private String sfstgjs;
	private String cjjcfwxmqsny;
	private String cjjcfwxmjsny;
	private String ggjslx;
	private String sfstjjs;
	private String sfxjjysggjs;
	private String sfxljkjyjs;
	private String sfss;
	private String sfjbzyjndjzs;
	private String qygzsjsc;
	private String zgqk;
	private String grzp;
	private String bsrlb;
	private String shzt;
	private Date shsj;
	private String shjg;
	private String sfsc;
	private String cjr;
	private Date cjsj;
	private String xgr;
	private Date xgsj;
	private String jjspsj;
	private String brsf;
	private String zjjl;
	private Short glN;
	private Byte glY;
	private Short jlN;
	private Byte jlY;
	private Short lxjlN;
	private Byte lxjlY;
	private String jg1;
	private String hkszdxxdz;
	private String zgybdysj;
	private String cjdpsj;
	private String dedp;
	private String cjdedpsj;
	private String xrzgdnzw;
	private String xrxzzw;
	private String rzsj;
	private String ltxqk;
	private String ltxsj;

	public long getJsid() {
		return jsid;
	}

	public void setJsid(long jsid) {
		this.jsid = jsid;
	}

	public TbXxjgb getTbXxjgb() {
		return tbXxjgb;
	}

	public void setTbXxjgb(TbXxjgb tbXxjgb) {
		this.tbXxjgb = tbXxjgb;
	}

	public Long getPersonalId() {
		return personalId;
	}

	public void setPersonalId(Long personalId) {
		this.personalId = personalId;
	}

	public String getEduId() {
		return eduId;
	}

	public void setEduId(String eduId) {
		this.eduId = eduId;
	}

	public String getJxjybh() {
		return jxjybh;
	}

	public void setJxjybh(String jxjybh) {
		this.jxjybh = jxjybh;
	}

	public String getJzgh() {
		return jzgh;
	}

	public void setJzgh(String jzgh) {
		this.jzgh = jzgh;
	}

	public String getSfzz() {
		return sfzz;
	}

	public void setSfzz(String sfzz) {
		this.sfzz = sfzz;
	}

	public String getXm() {
		return xm;
	}

	public void setXm(String xm) {
		this.xm = xm;
	}

	public String getCym() {
		return cym;
	}

	public void setCym(String cym) {
		this.cym = cym;
	}

	public String getXb() {
		return xb;
	}

	public void setXb(String xb) {
		this.xb = xb;
	}

	public String getGjdq() {
		return gjdq;
	}

	public void setGjdq(String gjdq) {
		this.gjdq = gjdq;
	}

	public String getSfzjlx() {
		return sfzjlx;
	}

	public void setSfzjlx(String sfzjlx) {
		this.sfzjlx = sfzjlx;
	}

	public String getSfzjh() {
		return sfzjh;
	}

	public void setSfzjh(String sfzjh) {
		this.sfzjh = sfzjh;
	}

	public String getCsrq() {
		return csrq;
	}

	public void setCsrq(String csrq) {
		this.csrq = csrq;
	}

	public String getJg() {
		return jg;
	}

	public void setJg(String jg) {
		this.jg = jg;
	}

	public String getCsd() {
		return csd;
	}

	public void setCsd(String csd) {
		this.csd = csd;
	}

	public String getMz() {
		return mz;
	}

	public void setMz(String mz) {
		this.mz = mz;
	}

	public String getZzmm() {
		return zzmm;
	}

	public void setZzmm(String zzmm) {
		this.zzmm = zzmm;
	}

	public String getHyzk() {
		return hyzk;
	}

	public void setHyzk(String hyzk) {
		this.hyzk = hyzk;
	}

	public String getJkzk() {
		return jkzk;
	}

	public void setJkzk(String jkzk) {
		this.jkzk = jkzk;
	}

	public String getZgxl() {
		return zgxl;
	}

	public void setZgxl(String zgxl) {
		this.zgxl = zgxl;
	}

	public String getHdzgxldyxhjg() {
		return hdzgxldyxhjg;
	}

	public void setHdzgxldyxhjg(String hdzgxldyxhjg) {
		this.hdzgxldyxhjg = hdzgxldyxhjg;
	}

	public String getZgxwcc() {
		return zgxwcc;
	}

	public void setZgxwcc(String zgxwcc) {
		this.zgxwcc = zgxwcc;
	}

	public String getZgxxmc() {
		return zgxxmc;
	}

	public void setZgxxmc(String zgxxmc) {
		this.zgxxmc = zgxxmc;
	}

	public String getHdzgxwdyxhjg() {
		return hdzgxwdyxhjg;
	}

	public void setHdzgxwdyxhjg(String hdzgxwdyxhjg) {
		this.hdzgxwdyxhjg = hdzgxwdyxhjg;
	}

	public String getCjgzny() {
		return cjgzny;
	}

	public void setCjgzny(String cjgzny) {
		this.cjgzny = cjgzny;
	}

	public String getCstjqsny() {
		return cstjqsny;
	}

	public void setCstjqsny(String cstjqsny) {
		this.cstjqsny = cstjqsny;
	}

	public String getJbxny() {
		return jbxny;
	}

	public void setJbxny(String jbxny) {
		this.jbxny = jbxny;
	}

	public String getJzgly() {
		return jzgly;
	}

	public void setJzgly(String jzgly) {
		this.jzgly = jzgly;
	}

	public String getXyjg() {
		return xyjg;
	}

	public void setXyjg(String xyjg) {
		this.xyjg = xyjg;
	}

	public String getJzglb() {
		return jzglb;
	}

	public void setJzglb(String jzglb) {
		this.jzglb = jzglb;
	}

	public String getSfzb() {
		return sfzb;
	}

	public void setSfzb(String sfzb) {
		this.sfzb = sfzb;
	}

	public String getYrxs() {
		return yrxs;
	}

	public void setYrxs(String yrxs) {
		this.yrxs = yrxs;
	}

	public String getQdhtqk() {
		return qdhtqk;
	}

	public void setQdhtqk(String qdhtqk) {
		this.qdhtqk = qdhtqk;
	}

	public String getSzejdw() {
		return szejdw;
	}

	public void setSzejdw(String szejdw) {
		this.szejdw = szejdw;
	}

	public String getXrgwlb() {
		return xrgwlb;
	}

	public void setXrgwlb(String xrgwlb) {
		this.xrgwlb = xrgwlb;
	}

	public String getXrgwdj() {
		return xrgwdj;
	}

	public void setXrgwdj(String xrgwdj) {
		this.xrgwdj = xrgwdj;
	}

	public String getXrzyjszw() {
		return xrzyjszw;
	}

	public void setXrzyjszw(String xrzyjszw) {
		this.xrzyjszw = xrzyjszw;
	}

	public String getSfsflzyby() {
		return sfsflzyby;
	}

	public void setSfsflzyby(String sfsflzyby) {
		this.sfsflzyby = sfsflzyby;
	}

	public String getSfqrztsyjzyby() {
		return sfqrztsyjzyby;
	}

	public void setSfqrztsyjzyby(String sfqrztsyjzyby) {
		this.sfqrztsyjzyby = sfqrztsyjzyby;
	}

	public String getSfsgtjzypypx() {
		return sfsgtjzypypx;
	}

	public void setSfsgtjzypypx(String sfsgtjzypypx) {
		this.sfsgtjzypypx = sfsgtjzypypx;
	}

	public String getSfytsjycyzs() {
		return sfytsjycyzs;
	}

	public void setSfytsjycyzs(String sfytsjycyzs) {
		this.sfytsjycyzs = sfytsjycyzs;
	}

	public String getSfxqjyzyby() {
		return sfxqjyzyby;
	}

	public void setSfxqjyzyby(String sfxqjyzyby) {
		this.sfxqjyzyby = sfxqjyzyby;
	}

	public String getSfsgxqjyzypypx() {
		return sfsgxqjyzypypx;
	}

	public void setSfsgxqjyzypypx(String sfsgxqjyzypypx) {
		this.sfsgxqjyzypypx = sfsgxqjyzypypx;
	}

	public String getXxjsyynl() {
		return xxjsyynl;
	}

	public void setXxjsyynl(String xxjsyynl) {
		this.xxjsyynl = xxjsyynl;
	}

	public String getSfsymfsfs() {
		return sfsymfsfs;
	}

	public void setSfsymfsfs(String sfsymfsfs) {
		this.sfsymfsfs = sfsymfsfs;
	}

	public String getSfstgjs() {
		return sfstgjs;
	}

	public void setSfstgjs(String sfstgjs) {
		this.sfstgjs = sfstgjs;
	}

	public String getCjjcfwxmqsny() {
		return cjjcfwxmqsny;
	}

	public void setCjjcfwxmqsny(String cjjcfwxmqsny) {
		this.cjjcfwxmqsny = cjjcfwxmqsny;
	}

	public String getCjjcfwxmjsny() {
		return cjjcfwxmjsny;
	}

	public void setCjjcfwxmjsny(String cjjcfwxmjsny) {
		this.cjjcfwxmjsny = cjjcfwxmjsny;
	}

	public String getGgjslx() {
		return ggjslx;
	}

	public void setGgjslx(String ggjslx) {
		this.ggjslx = ggjslx;
	}

	public String getSfstjjs() {
		return sfstjjs;
	}

	public void setSfstjjs(String sfstjjs) {
		this.sfstjjs = sfstjjs;
	}

	public String getSfxjjysggjs() {
		return sfxjjysggjs;
	}

	public void setSfxjjysggjs(String sfxjjysggjs) {
		this.sfxjjysggjs = sfxjjysggjs;
	}

	public String getSfxljkjyjs() {
		return sfxljkjyjs;
	}

	public void setSfxljkjyjs(String sfxljkjyjs) {
		this.sfxljkjyjs = sfxljkjyjs;
	}

	public String getSfss() {
		return sfss;
	}

	public void setSfss(String sfss) {
		this.sfss = sfss;
	}

	public String getSfjbzyjndjzs() {
		return sfjbzyjndjzs;
	}

	public void setSfjbzyjndjzs(String sfjbzyjndjzs) {
		this.sfjbzyjndjzs = sfjbzyjndjzs;
	}

	public String getQygzsjsc() {
		return qygzsjsc;
	}

	public void setQygzsjsc(String qygzsjsc) {
		this.qygzsjsc = qygzsjsc;
	}

	public String getZgqk() {
		return zgqk;
	}

	public void setZgqk(String zgqk) {
		this.zgqk = zgqk;
	}

	public String getGrzp() {
		return grzp;
	}

	public void setGrzp(String grzp) {
		this.grzp = grzp;
	}

	public String getBsrlb() {
		return bsrlb;
	}

	public void setBsrlb(String bsrlb) {
		this.bsrlb = bsrlb;
	}

	public String getShzt() {
		return shzt;
	}

	public void setShzt(String shzt) {
		this.shzt = shzt;
	}

	public Date getShsj() {
		return shsj;
	}

	public void setShsj(Date shsj) {
		this.shsj = shsj;
	}

	public String getShjg() {
		return shjg;
	}

	public void setShjg(String shjg) {
		this.shjg = shjg;
	}

	public String getSfsc() {
		return sfsc;
	}

	public void setSfsc(String sfsc) {
		this.sfsc = sfsc;
	}

	public String getCjr() {
		return cjr;
	}

	public void setCjr(String cjr) {
		this.cjr = cjr;
	}

	public Date getCjsj() {
		return cjsj;
	}

	public void setCjsj(Date cjsj) {
		this.cjsj = cjsj;
	}

	public String getXgr() {
		return xgr;
	}

	public void setXgr(String xgr) {
		this.xgr = xgr;
	}

	public Date getXgsj() {
		return xgsj;
	}

	public void setXgsj(Date xgsj) {
		this.xgsj = xgsj;
	}

	public String getJjspsj() {
		return jjspsj;
	}

	public void setJjspsj(String jjspsj) {
		this.jjspsj = jjspsj;
	}

	public String getBrsf() {
		return brsf;
	}

	public void setBrsf(String brsf) {
		this.brsf = brsf;
	}

	public String getZjjl() {
		return zjjl;
	}

	public void setZjjl(String zjjl) {
		this.zjjl = zjjl;
	}

	public Short getGlN() {
		return glN;
	}

	public void setGlN(Short glN) {
		this.glN = glN;
	}

	public Byte getGlY() {
		return glY;
	}

	public void setGlY(Byte glY) {
		this.glY = glY;
	}

	public Short getJlN() {
		return jlN;
	}

	public void setJlN(Short jlN) {
		this.jlN = jlN;
	}

	public Byte getJlY() {
		return jlY;
	}

	public void setJlY(Byte jlY) {
		this.jlY = jlY;
	}

	public Short getLxjlN() {
		return lxjlN;
	}

	public void setLxjlN(Short lxjlN) {
		this.lxjlN = lxjlN;
	}

	public Byte getLxjlY() {
		return lxjlY;
	}

	public void setLxjlY(Byte lxjlY) {
		this.lxjlY = lxjlY;
	}

	public String getJg1() {
		return jg1;
	}

	public void setJg1(String jg1) {
		this.jg1 = jg1;
	}

	public String getHkszdxxdz() {
		return hkszdxxdz;
	}

	public void setHkszdxxdz(String hkszdxxdz) {
		this.hkszdxxdz = hkszdxxdz;
	}

	public String getZgybdysj() {
		return zgybdysj;
	}

	public void setZgybdysj(String zgybdysj) {
		this.zgybdysj = zgybdysj;
	}

	public String getCjdpsj() {
		return cjdpsj;
	}

	public void setCjdpsj(String cjdpsj) {
		this.cjdpsj = cjdpsj;
	}

	public String getDedp() {
		return dedp;
	}

	public void setDedp(String dedp) {
		this.dedp = dedp;
	}

	public String getCjdedpsj() {
		return cjdedpsj;
	}

	public void setCjdedpsj(String cjdedpsj) {
		this.cjdedpsj = cjdedpsj;
	}

	public String getXrzgdnzw() {
		return xrzgdnzw;
	}

	public void setXrzgdnzw(String xrzgdnzw) {
		this.xrzgdnzw = xrzgdnzw;
	}

	public String getXrxzzw() {
		return xrxzzw;
	}

	public void setXrxzzw(String xrxzzw) {
		this.xrxzzw = xrxzzw;
	}

	public String getRzsj() {
		return rzsj;
	}

	public void setRzsj(String rzsj) {
		this.rzsj = rzsj;
	}

	public String getLtxqk() {
		return ltxqk;
	}

	public void setLtxqk(String ltxqk) {
		this.ltxqk = ltxqk;
	}

	public String getLtxsj() {
		return ltxsj;
	}

	public void setLtxsj(String ltxsj) {
		this.ltxsj = ltxsj;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
