/**
 * TbCfgZdxbRxrcInfoDbo.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

/**
 * 字典表项入选人才
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   shanjigang
 * @Date	 2016年10月21日 	 
 */
public class TbCfgZdxbRxrcInfoDbo implements java.io.Serializable {

	private static final long serialVersionUID = 298207568205605502L;
	/**
	 * 字典标识
	 */
	private String zdbs;
	/**
	 * 字典编码
	 */
	private String zdxbm;
	/**
	 * 字典名称
	 */
	private String zdxmc;
	/**
	 * 父字典项
	 */
	private String fzdx;

	public String getZdbs() {
		return zdbs;
	}

	public void setZdbs(String zdbs) {
		this.zdbs = zdbs;
	}

	public String getZdxbm() {
		return zdxbm;
	}

	public void setZdxbm(String zdxbm) {
		this.zdxbm = zdxbm;
	}

	public String getZdxmc() {
		return zdxmc;
	}

	public void setZdxmc(String zdxmc) {
		this.zdxmc = zdxmc;
	}

	public String getFzdx() {
		return fzdx;
	}

	public void setFzdx(String fzdx) {
		this.fzdx = fzdx;
	}

}
