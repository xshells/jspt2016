/**
 * TbBizJzgxxjlxxDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

import java.io.Serializable;

/**
 * TODO(这里用一句话描述这个类的作用)
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   ZhuanJunxiang
 * @Date	 2016年10月19日 	 
 */
public class TbBizJzgxxjlxxDto implements Serializable {

	private static final long serialVersionUID = 1L;

	public String hdxl;
	public String hdxldgjdq;
	public String hdxldyxhjg;
	public String sxzy;
	public String rxny;
	public String byny;
	public String xwcc;
	public String xwmc;
	public String hdxwdgjdq;
	public String hdxwdyxhjg;
	public String xwsyny;
	public String xxfs;
	public String zxdwlb;
	public long xlId;
	public String sfsflzy;

	public String getSfsflzy() {
		return sfsflzy;
	}

	public void setSfsflzy(String sfsflzy) {
		this.sfsflzy = sfsflzy;
	}

	public String getHdxl() {
		return hdxl;
	}

	public void setHdxl(String hdxl) {
		this.hdxl = hdxl;
	}

	public String getHdxldgjdq() {
		return hdxldgjdq;
	}

	public void setHdxldgjdq(String hdxldgjdq) {
		this.hdxldgjdq = hdxldgjdq;
	}

	public String getHdxldyxhjg() {
		return hdxldyxhjg;
	}

	public void setHdxldyxhjg(String hdxldyxhjg) {
		this.hdxldyxhjg = hdxldyxhjg;
	}

	public String getSxzy() {
		return sxzy;
	}

	public void setSxzy(String sxzy) {
		this.sxzy = sxzy;
	}

	public String getRxny() {
		return rxny;
	}

	public void setRxny(String rxny) {
		this.rxny = rxny;
	}

	public String getByny() {
		return byny;
	}

	public void setByny(String byny) {
		this.byny = byny;
	}

	public String getXwcc() {
		return xwcc;
	}

	public void setXwcc(String xwcc) {
		this.xwcc = xwcc;
	}

	public String getXwmc() {
		return xwmc;
	}

	public void setXwmc(String xwmc) {
		this.xwmc = xwmc;
	}

	public String getHdxwdgjdq() {
		return hdxwdgjdq;
	}

	public void setHdxwdgjdq(String hdxwdgjdq) {
		this.hdxwdgjdq = hdxwdgjdq;
	}

	public String getHdxwdyxhjg() {
		return hdxwdyxhjg;
	}

	public void setHdxwdyxhjg(String hdxwdyxhjg) {
		this.hdxwdyxhjg = hdxwdyxhjg;
	}

	public String getXwsyny() {
		return xwsyny;
	}

	public void setXwsyny(String xwsyny) {
		this.xwsyny = xwsyny;
	}

	public String getXxfs() {
		return xxfs;
	}

	public void setXxfs(String xxfs) {
		this.xxfs = xxfs;
	}

	public String getZxdwlb() {
		return zxdwlb;
	}

	public void setZxdwlb(String zxdwlb) {
		this.zxdwlb = zxdwlb;
	}

	public long getXlId() {
		return xlId;
	}

	public void setXlId(long xlId) {
		this.xlId = xlId;
	}

}
