/**
 * TeacherAccountInfoDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

import java.util.Date;

/**
 * 入选人才项目Dto
 * <p>
 * @author   shanjigang
 * @Date	 2016年10月17日
 */

public class TbBizJzgrxrcxmxxInfoDto implements java.io.Serializable {

	private static final long serialVersionUID = -8363807423435252234L;
	/**
	 * 入选项目名称
	 */
	private String rxrcxmmc;
	/**
	 * 入选年月
	 */
	private String rxny;
	/**
	 * 创建时间
	 */
	private Date cjsj;
	/**
	 * 教师id
	 */
	private long jsid;
	/**
	 * 项目id
	 */
	private long xmid;
	private String shzt;

	public String getRxrcxmmc() {
		return rxrcxmmc;
	}

	public void setRxrcxmmc(String rxrcxmmc) {
		this.rxrcxmmc = rxrcxmmc;
	}

	public String getRxny() {
		return rxny;
	}

	public void setRxny(String rxny) {
		this.rxny = rxny;
	}

	public Date getCjsj() {
		return cjsj;
	}

	public void setCjsj(Date cjsj) {
		this.cjsj = cjsj;
	}

	public long getJsid() {
		return jsid;
	}

	public void setJsid(long jsid) {
		this.jsid = jsid;
	}

	public long getXmid() {
		return xmid;
	}

	public void setXmid(long xmid) {
		this.xmid = xmid;
	}

	public String getShzt() {
		return shzt;
	}

	public void setShzt(String shzt) {
		this.shzt = shzt;
	}

}
