/**
 * PostAppointDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

import java.io.Serializable;

/**
 * TODO岗位聘任Dto
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   董锡福
 * @Date	 2016年10月16日 	 
 */
public class PostAppointDto implements Serializable {
	private long GWPR_ID; //岗位聘任ID

	/*private int JSID; //教职工ID
	private String GWLB; //岗位类别
	private String GWGJ; //岗位等级
	private String PRKSNY; //聘任开始年月
	private String SFJRQTGW; //是否兼任其他岗位
	private String JRGWLB; //兼任岗位类别
	private String JRGWDJ; //兼任岗位等级
	private String SFSJT; //是否双肩挑
	private String SJTGWLB; //双肩挑岗位类别
	private String SJTGWDJ; //双肩挑岗位等级
	private String SFZZFDY; //是否为辅导员
	private String DZZW; //党政职务（或校级职务）
	private String RZKSNY; //任职开始年月
	private String DZJB; //党政级别
	private String SFZZCSXLZXGZ; //是否专职从事心理咨询工作
	private String SFCYXLZXZGZS; //是否持有心理咨询资格证书
	private String GZ; //工种
	private String QZSJ; //取证时间
	private String BSRLB; //报送人用户类别
	private String SHZT; //审核状态
	private Date SHSJ; //审核时间
	private String SHJG; //审核结果
	private String SFSC; //是否删除
	private String CJR; //创建人
	private Date CJSJ; //创建时间
	private String XGR; //修改人
	private Date XGSJ; //修改时间
	*/
	public long getGWPR_ID() {
		return GWPR_ID;
	}

	public void setGWPR_ID(long gWPR_ID) {
		GWPR_ID = gWPR_ID;
	}

	/*public void setGWPR_ID(int gWPR_ID) {
		GWPR_ID = gWPR_ID;
	}

	public int getJSID() {
		return JSID;
	}

	public void setJSID(int jSID) {
		JSID = jSID;
	}

	public String getGWLB() {
		return GWLB;
	}

	public void setGWLB(String gWLB) {
		GWLB = gWLB;
	}

	public String getGWGJ() {
		return GWGJ;
	}

	public void setGWGJ(String gWGJ) {
		GWGJ = gWGJ;
	}

	public String getPRKSNY() {
		return PRKSNY;
	}

	public void setPRKSNY(String pRKSNY) {
		PRKSNY = pRKSNY;
	}

	public String getSFJRQTGW() {
		return SFJRQTGW;
	}

	public void setSFJRQTGW(String sFJRQTGW) {
		SFJRQTGW = sFJRQTGW;
	}

	public String getJRGWLB() {
		return JRGWLB;
	}

	public void setJRGWLB(String jRGWLB) {
		JRGWLB = jRGWLB;
	}

	public String getJRGWDJ() {
		return JRGWDJ;
	}

	public void setJRGWDJ(String jRGWDJ) {
		JRGWDJ = jRGWDJ;
	}

	public String getSFSJT() {
		return SFSJT;
	}

	public void setSFSJT(String sFSJT) {
		SFSJT = sFSJT;
	}

	public String getSJTGWLB() {
		return SJTGWLB;
	}

	public void setSJTGWLB(String sJTGWLB) {
		SJTGWLB = sJTGWLB;
	}

	public String getSJTGWDJ() {
		return SJTGWDJ;
	}

	public void setSJTGWDJ(String sJTGWDJ) {
		SJTGWDJ = sJTGWDJ;
	}

	public String getSFZZFDY() {
		return SFZZFDY;
	}

	public void setSFZZFDY(String sFZZFDY) {
		SFZZFDY = sFZZFDY;
	}

	public String getDZZW() {
		return DZZW;
	}

	public void setDZZW(String dZZW) {
		DZZW = dZZW;
	}

	public String getRZKSNY() {
		return RZKSNY;
	}

	public void setRZKSNY(String rZKSNY) {
		RZKSNY = rZKSNY;
	}

	public String getDZJB() {
		return DZJB;
	}

	public void setDZJB(String dZJB) {
		DZJB = dZJB;
	}

	public String getSFZZCSXLZXGZ() {
		return SFZZCSXLZXGZ;
	}

	public void setSFZZCSXLZXGZ(String sFZZCSXLZXGZ) {
		SFZZCSXLZXGZ = sFZZCSXLZXGZ;
	}

	public String getSFCYXLZXZGZS() {
		return SFCYXLZXZGZS;
	}

	public void setSFCYXLZXZGZS(String sFCYXLZXZGZS) {
		SFCYXLZXZGZS = sFCYXLZXZGZS;
	}

	public String getGZ() {
		return GZ;
	}

	public void setGZ(String gZ) {
		GZ = gZ;
	}

	public String getQZSJ() {
		return QZSJ;
	}

	public void setQZSJ(String qZSJ) {
		QZSJ = qZSJ;
	}

	public String getBSRLB() {
		return BSRLB;
	}

	public void setBSRLB(String bSRLB) {
		BSRLB = bSRLB;
	}

	public String getSHZT() {
		return SHZT;
	}

	public void setSHZT(String sHZT) {
		SHZT = sHZT;
	}

	public Date getSHSJ() {
		return SHSJ;
	}

	public void setSHSJ(Date sHSJ) {
		SHSJ = sHSJ;
	}

	public String getSHJG() {
		return SHJG;
	}

	public void setSHJG(String sHJG) {
		SHJG = sHJG;
	}

	public String getSFSC() {
		return SFSC;
	}

	public void setSFSC(String sFSC) {
		SFSC = sFSC;
	}

	public String getCJR() {
		return CJR;
	}

	public void setCJR(String cJR) {
		CJR = cJR;
	}

	public Date getCJSJ() {
		return CJSJ;
	}

	public void setCJSJ(Date cJSJ) {
		CJSJ = cJSJ;
	}

	public String getXGR() {
		return XGR;
	}

	public void setXGR(String xGR) {
		XGR = xGR;
	}

	public Date getXGSJ() {
		return XGSJ;
	}

	public void setXGSJ(Date xGSJ) {
		XGSJ = xGSJ;
	}*/
}
