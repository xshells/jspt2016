/**
 * InstitutionsInfoDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

/**
 * 机构信息Dto
 * <p>
 * @author   kuangxiang
 * @Date	 2016年10月18日
 */

public class InstitutionsInfoDto implements java.io.Serializable {
	/**
	 * TODO（用一句话描述这个变量表示什么）
	 */

	private static final long serialVersionUID = -8779150676408252455L;

	/**
	 * TODO（用一句话描述这个变量表示什么）
	 */

	public InstitutionsInfoDto() {

	}

	public InstitutionsInfoDto(Integer institutionId, String institutionCode, String institutionName,
			String districtCode, String institutionAddress, String schoolType, String residentType, String unitType,
			String unitCode, String disName, Integer count) {
		this.institutionId = institutionId;
		this.institutionCode = institutionCode;
		this.institutionName = institutionName;

		this.districtCode = districtCode;
		this.institutionAddress = institutionAddress;
		this.schoolType = schoolType;
		this.residentType = residentType;
		this.unitType = unitType;
		this.unitCode = unitCode;
		this.disName = disName;
		this.count = count;

	}

	/**
	 * 机构Id
	 */
	private Integer institutionId;
	/**
	 * 机构代码
	 */
	private String institutionCode;
	/**
	 * 机构名称
	 */
	private String institutionName;
	/**
	 * 区县代码
	 */
	private String districtCode;
	/**
	 * 机构地址
	 */
	private String institutionAddress;
	/**
	 * 办学类型
	 */
	private String schoolType;
	/**
	 * 驻地城乡类型
	 */
	private String residentType;
	/**
	 * 举办单位
	 */
	private String jbdw;
	/**
	 * 举办者类型
	 */
	private String jbzlx;
	/**
	 * 联系人
	 */
	private String contactPerson;
	/**
	 * 联系电话
	 */
	private String contactTel;
	/**
	 * 邮箱
	 */
	private String email;
	/**
	 * 所属学段
	 */
	private String affiliatedSchool;

	/**
	 * 全国机构代码
	 */
	private String countryInstitutionCode;
	/**
	 * 单位类别
	 */
	private String unitType;
	/**
	 * 组织机构代码（单位组织代码）
	 */
	private String unitCode;

	private String disName;
	/**
	 * 机构下可用老师账户的数量
	 */
	private Integer count;

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getDisName() {
		return disName;
	}

	public void setDisName(String disName) {
		this.disName = disName;
	}

	public String getUnitCode() {
		return unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	public String getCountryInstitutionCode() {
		return countryInstitutionCode;
	}

	public void setCountryInstitutionCode(String countryInstitutionCode) {
		this.countryInstitutionCode = countryInstitutionCode;
	}

	public Integer getInstitutionId() {
		return institutionId;
	}

	public void setInstitutionId(Integer institutionId) {
		this.institutionId = institutionId;
	}

	public String getInstitutionCode() {
		return institutionCode;
	}

	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}

	public String getInstitutionName() {
		return institutionName;
	}

	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}

	public String getDistrictCode() {
		return districtCode;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	public String getInstitutionAddress() {
		return institutionAddress;
	}

	public void setInstitutionAddress(String institutionAddress) {
		this.institutionAddress = institutionAddress;
	}

	public String getSchoolType() {
		return schoolType;
	}

	public void setSchoolType(String schoolType) {
		this.schoolType = schoolType;
	}

	public String getResidentType() {
		return residentType;
	}

	public void setResidentType(String residentType) {
		this.residentType = residentType;
	}

	public String getJbdw() {
		return jbdw;
	}

	public void setJbdw(String jbdw) {
		this.jbdw = jbdw;
	}

	public String getJbzlx() {
		return jbzlx;
	}

	public void setJbzlx(String jbzlx) {
		this.jbzlx = jbzlx;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getContactTel() {
		return contactTel;
	}

	public void setContactTel(String contactTel) {
		this.contactTel = contactTel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAffiliatedSchool() {
		return affiliatedSchool;
	}

	public void setAffiliatedSchool(String affiliatedSchool) {
		this.affiliatedSchool = affiliatedSchool;
	}

}
