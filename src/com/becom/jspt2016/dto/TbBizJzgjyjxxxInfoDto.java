/**
 * TbBizJzgsdxxInfoDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

import java.util.Date;

/**
 * 教育教学信息dbo
 *
 * @author   shanjigang
 * @Date	 2016年10月16日 	 
 */
public class TbBizJzgjyjxxxInfoDto implements java.io.Serializable {

	private static final long serialVersionUID = 6308923400390383321L;

	private int jyid;
	private int jsid;
	private String xn;
	private String xq;
	private String rkzk;
	private String sfwbkssk;
	private String dslb;
	private String xzycsxkly;
	private String xnbzkkcjxkss;
	private Date cjsj;
	/**
	 * 任课课程类别
	 */
	private String rkkclb;
	private String shzt;
	/**
	 * 任教学段
	 */
	private String rjxd;
	/**
	 * 任教课程
	 */
	private String rjkc;
	private String[] rjkcArr;
	/**
	 * 平均每周课堂教学课时数
	 */
	private String pjzks;
	/**
	 * 兼任工作
	 */
	private String jrgz;
	/**
	 * 任课学科类别 
	 */
	private String rkxklb;
	private String[] rkkclblist;

	public int getJyid() {
		return jyid;
	}

	public void setJyid(int jyid) {
		this.jyid = jyid;
	}

	public int getJsid() {
		return jsid;
	}

	public void setJsid(int jsid) {
		this.jsid = jsid;
	}

	public String getXn() {
		return xn;
	}

	public void setXn(String xn) {
		this.xn = xn;
	}

	public String getXq() {
		return xq;
	}

	public void setXq(String xq) {
		this.xq = xq;
	}

	public String getRkzk() {
		return rkzk;
	}

	public void setRkzk(String rkzk) {
		this.rkzk = rkzk;
	}

	public String getSfwbkssk() {
		return sfwbkssk;
	}

	public void setSfwbkssk(String sfwbkssk) {
		this.sfwbkssk = sfwbkssk;
	}

	public String getDslb() {
		return dslb;
	}

	public void setDslb(String dslb) {
		this.dslb = dslb;
	}

	public String getXzycsxkly() {
		return xzycsxkly;
	}

	public void setXzycsxkly(String xzycsxkly) {
		this.xzycsxkly = xzycsxkly;
	}

	public String getXnbzkkcjxkss() {
		return xnbzkkcjxkss;
	}

	public void setXnbzkkcjxkss(String xnbzkkcjxkss) {
		this.xnbzkkcjxkss = xnbzkkcjxkss;
	}

	public Date getCjsj() {
		return cjsj;
	}

	public void setCjsj(Date cjsj) {
		this.cjsj = cjsj;
	}

	public String getSfsc() {
		return sfsc;
	}

	public void setSfsc(String sfsc) {
		this.sfsc = sfsc;
	}

	private String sfsc;

	public String getRkkclb() {
		return rkkclb;
	}

	public void setRkkclb(String rkkclb) {
		this.rkkclb = rkkclb;
	}

	public String getShzt() {
		return shzt;
	}

	public void setShzt(String shzt) {
		this.shzt = shzt;
	}

	public String getRjxd() {
		return rjxd;
	}

	public void setRjxd(String rjxd) {
		this.rjxd = rjxd;
	}

	public String getRjkc() {
		return rjkc;
	}

	public void setRjkc(String rjkc) {
		this.rjkc = rjkc;
	}

	public String getPjzks() {
		return pjzks;
	}

	public void setPjzks(String pjzks) {
		this.pjzks = pjzks;
	}

	public String getJrgz() {
		return jrgz;
	}

	public void setJrgz(String jrgz) {
		this.jrgz = jrgz;
	}

	public String getRkxklb() {
		return rkxklb;
	}

	public void setRkxklb(String rkxklb) {
		this.rkxklb = rkxklb;
	}

	public String[] getRkkclblist() {
		return rkkclblist;
	}

	public void setRkkclblist(String[] rkkclblist) {
		this.rkkclblist = rkkclblist;
	}

	public String[] getRjkcArr() {
		if (this.rjkc != null && rjkc.indexOf(",") > 0) {
			rjkcArr = this.rjkc.split(",");
		}
		return rjkcArr;
	}

}
