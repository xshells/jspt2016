/**
 * SubmitCommentDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

import java.io.Serializable;

/**
 * 报送内容dto
 *
 * @author   zhangchunming
 * @Date	 2016年10月26日 	 
 */
public class SubmitCommentDto implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 教职工Id
	 */
	private long jsId;

	/**
	 * 所属学段
	 */
	private String ssxd;

	/**
	 * 审核状态
	 */
	private String aduitStatus;

	/**
	 * 报送人id
	 */
	private long submitPersonId;

	/**
	 * 信息类别
	 */
	private String xxlb;

	public String getShjg() {
		return shjg;
	}

	public void setShjg(String shjg) {
		this.shjg = shjg;
	}

	/**
	 * 报送人类别
	 */
	private String submitPersonType;

	/**
	 * 是否删除
	 */
	private String sfsc;

	/**
	 * 审核结果
	 */
	private String shjg;

	public String getSfsc() {
		return sfsc;
	}

	public void setSfsc(String sfsc) {
		this.sfsc = sfsc;
	}

	public String getXxlb() {
		return xxlb;
	}

	public void setXxlb(String xxlb) {
		this.xxlb = xxlb;
	}

	public long getJsId() {
		return jsId;
	}

	public void setJsId(long jsId) {
		this.jsId = jsId;
	}

	public String getSsxd() {
		return ssxd;
	}

	public void setSsxd(String ssxd) {
		this.ssxd = ssxd;
	}

	public String getAduitStatus() {
		return aduitStatus;
	}

	public void setAduitStatus(String aduitStatus) {
		this.aduitStatus = aduitStatus;
	}

	public long getSubmitPersonId() {
		return submitPersonId;
	}

	public void setSubmitPersonId(long submitPersonId) {
		this.submitPersonId = submitPersonId;
	}

	public String getSubmitPersonType() {
		return submitPersonType;
	}

	public void setSubmitPersonType(String submitPersonType) {
		this.submitPersonType = submitPersonType;
	}

}
