/**
 * HwyxxxInfoDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *     		 海外研修dto
 * @author   王小雷
 * @Date	 2016年10月20日 	 
 */
public class HwyxxxInfoDto implements Serializable {

	private static final long serialVersionUID = 777458364311554497L;
	/**
	 * 海外研修id
	 */
	private long hwyxId;
	/**
	 * 教职工Id
	 */
	private long jsid;
	/**
	 * 是否有海外(访问)经历
	 */
	private String sfjyhwyxjl;
	/**
	 * 创建时间
	 */
	private Date cjsj;

	public long getHwyxId() {
		return hwyxId;
	}

	public void setHwyxId(long hwyxId) {
		this.hwyxId = hwyxId;
	}

	public long getJsid() {
		return jsid;
	}

	public void setJsid(long jsid) {
		this.jsid = jsid;
	}

	public String getSfjyhwyxjl() {
		return sfjyhwyxjl;
	}

	public void setSfjyhwyxjl(String sfjyhwyxjl) {
		this.sfjyhwyxjl = sfjyhwyxjl;
	}

	public Date getCjsj() {
		return cjsj;
	}

	public void setCjsj(Date cjsj) {
		this.cjsj = cjsj;
	}

}
