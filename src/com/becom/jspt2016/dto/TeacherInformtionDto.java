/**
 * TeacherInformtionDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

import java.util.Date;

/**
 * TODO(这里用一句话描述这个类的作用)
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   dongxifu
 * @Date	 2016年10月24日 	 
 */
public class TeacherInformtionDto implements java.io.Serializable {
	/**
	 * TODO（用一句话描述这个变量表示什么）
	 */

	private static final long serialVersionUID = 4394302661772345861L;
	private long jsid;
	private String eduId;
	private String xm;
	private String xb;
	private String sfzjh;
	private String csrq;
	private String qxmc;
	private String shzt;
	private String shjg;

	public String getQxmc() {
		return qxmc;
	}

	public String getShzt() {
		return shzt;
	}

	public void setShzt(String shzt) {
		this.shzt = shzt;
	}

	public String getShjg() {
		return shjg;
	}

	public void setShjg(String shjg) {
		this.shjg = shjg;
	}

	public void setQxmc(String qxmc) {
		this.qxmc = qxmc;
	}

	public long getJsid() {
		return jsid;
	}

	public void setJsid(long jsid) {
		this.jsid = jsid;
	}

	public String getEduId() {
		return eduId;
	}

	public void setEduId(String eduId) {
		this.eduId = eduId;
	}

	public String getXm() {
		return xm;
	}

	public void setXm(String xm) {
		this.xm = xm;
	}

	public String getXb() {
		return xb;
	}

	public void setXb(String xb) {
		this.xb = xb;
	}

	public String getSfzjh() {
		return sfzjh;
	}

	public void setSfzjh(String sfzjh) {
		this.sfzjh = sfzjh;
	}

	public String getCsrq() {
		return csrq;
	}

	public void setCsrq(String csrq) {
		this.csrq = csrq;
	}

	public Date getShsj() {
		return shsj;
	}

	public void setShsj(Date shsj) {
		this.shsj = shsj;
	}

	public String getQxdm() {
		return qxdm;
	}

	public void setQxdm(String qxdm) {
		this.qxdm = qxdm;
	}

	private Date shsj;
	private String qxdm;
}
