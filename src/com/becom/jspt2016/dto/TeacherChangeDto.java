/**
 * TeacherChangeDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.dto;

import java.io.Serializable;

/**
 * 教师调动Dto
 * <p>
 * @author   fmx
 * @Date	 2016年10月26日
 */
public class TeacherChangeDto implements Serializable{

	private static final long serialVersionUID = 8715351486013400762L;

	/**
	 * 调动ID
	 */
	private Integer id;
	/**
	 * 教师ID
	 */
	private Integer tid;
	/**
	 * 审核状态
	 */
	private String status;
	/**
	 * 教育ID
	 */
	private Integer eduId;
	/**
	 * 姓名
	 */
	private String name;
	/**
	 * 性别
	 */
	private String sex;
	/**
	 * 身份证号
	 */
	private String cardNo;
	/**
	 * 调出机构ID
	 */
	private Integer outId;
	private String outName;
	/**
	 * 调入机构ID
	 */
	private Integer inId;
	private String inName;
	/**
	 * 调出时间
	 */
	private String outTime;
	/**
	 * 调入时间
	 */
	private String inTime;
	/**
	 * 申请时间
	 */
	private String applyTime;
	/**
	 * 原因
	 */
	private String causeStr;
	
	private Long sqrid;
	private String sqrname;
	
	public String getSqrname() {
		return sqrname;
	}
	public void setSqrname(String sqrname) {
		this.sqrname = sqrname;
	}
	private String signNo;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getEduId() {
		return eduId;
	}
	public void setEduId(Integer eduId) {
		this.eduId = eduId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public Integer getOutId() {
		return outId;
	}
	public void setOutId(Integer outId) {
		this.outId = outId;
	}
	public Integer getInId() {
		return inId;
	}
	public void setInId(Integer inId) {
		this.inId = inId;
	}
	public String getOutTime() {
		return outTime;
	}
	public void setOutTime(String outTime) {
		this.outTime = outTime;
	}
	public String getInTime() {
		return inTime;
	}
	public void setInTime(String inTime) {
		this.inTime = inTime;
	}
	public String getApplyTime() {
		return applyTime;
	}
	public void setApplyTime(String applyTime) {
		this.applyTime = applyTime;
	}
	public String getOutName() {
		return outName;
	}
	public void setOutName(String outName) {
		this.outName = outName;
	}
	public String getInName() {
		return inName;
	}
	public void setInName(String inName) {
		this.inName = inName;
	}
	public String getCauseStr() {
		return causeStr;
	}
	public void setCauseStr(String causeStr) {
		this.causeStr = causeStr;
	}
	public String getSignNo() {
		return signNo;
	}
	public void setSignNo(String signNo) {
		this.signNo = signNo;
	}
	public Integer getTid() {
		return tid;
	}
	public void setTid(Integer tid) {
		this.tid = tid;
	}
	public Long getSqrid() {
		return sqrid;
	}
	public void setSqrid(Long sqrid) {
		this.sqrid = sqrid;
	}
	
}
