/**
 * TbBizJszgxxDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

import java.io.Serializable;

/**
 *教师资格dto
 *
 * @author   zhangchunming
 * @Date	 2016年10月24日 	 
 */
public class TbBizJszgxxDto implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 教师资格id
	 */
	private long jszgid;

	/**
	 * 教师资格证种类
	 */
	private String jszgzzl;

	/**
	 * 教师资格证号码
	 */
	private String jszgzhm;

	/**
	 * 任教学科
	 */
	private String rjxk;

	/**
	 * 证书颁发日期
	 */
	private String zsbfrq;

	/**
	 * 证书认定机构
	 */
	private String zsrdjg;

	public long getJszgid() {
		return jszgid;
	}

	public void setJszgid(long jszgid) {
		this.jszgid = jszgid;
	}

	public String getJszgzzl() {
		return jszgzzl;
	}

	public void setJszgzzl(String jszgzzl) {
		this.jszgzzl = jszgzzl;
	}

	public String getJszgzhm() {
		return jszgzhm;
	}

	public void setJszgzhm(String jszgzhm) {
		this.jszgzhm = jszgzhm;
	}

	public String getRjxk() {
		return rjxk;
	}

	public void setRjxk(String rjxk) {
		this.rjxk = rjxk;
	}

	public String getZsbfrq() {
		return zsbfrq;
	}

	public void setZsbfrq(String zsbfrq) {
		this.zsbfrq = zsbfrq;
	}

	public String getZsrdjg() {
		return zsrdjg;
	}

	public void setZsrdjg(String zsrdjg) {
		this.zsrdjg = zsrdjg;
	}

}
