/**
 * TbBizJzgsdxxInfoDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

import java.util.Date;

/**
 * TODO(这里用一句话描述这个类的作用)
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   shanjigang
 * @Date	 2016年10月16日 	 
 */
public class TbBizJzgsdxxInfoDto implements java.io.Serializable {
	/**
	 * TODO（用一句话描述这个变量表示什么）
	 */

	private static final long serialVersionUID = 7711179391892823374L;
	private long jsid;
	private long khid;
	private long cfid;
	private String khny;
	private String khjl;
	private String cflb;
	private String cfyy;
	private String ccfsrq;
	private Date cjsj;
	private String shzt;
	private String sdkhdwmc;

	public long getJsid() {
		return jsid;
	}

	public void setJsid(long jsid) {
		this.jsid = jsid;
	}

	public long getKhid() {
		return khid;
	}

	public void setKhid(long khid) {
		this.khid = khid;
	}

	public long getCfid() {
		return cfid;
	}

	public void setCfid(long cfid) {
		this.cfid = cfid;
	}

	public String getKhny() {
		return khny;
	}

	public void setKhny(String khny) {
		this.khny = khny;
	}

	public String getKhjl() {
		return khjl;
	}

	public void setKhjl(String khjl) {
		this.khjl = khjl;
	}

	public String getCflb() {
		return cflb;
	}

	public void setCflb(String cflb) {
		this.cflb = cflb;
	}

	public String getCfyy() {
		return cfyy;
	}

	public void setCfyy(String cfyy) {
		this.cfyy = cfyy;
	}

	public String getCcfsrq() {
		return ccfsrq;
	}

	public void setCcfsrq(String ccfsrq) {
		this.ccfsrq = ccfsrq;
	}

	public Date getCjsj() {
		return cjsj;
	}

	public void setCjsj(Date cjsj) {
		this.cjsj = cjsj;
	}

	public String getShzt() {
		return shzt;
	}

	public void setShzt(String shzt) {
		this.shzt = shzt;
	}

	public String getSdkhdwmc() {
		return sdkhdwmc;
	}

	public void setSdkhdwmc(String sdkhdwmc) {
		this.sdkhdwmc = sdkhdwmc;
	}

}
