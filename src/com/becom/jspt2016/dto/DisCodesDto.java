/**
 * DisCodesDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

/**
 * 区县代码Dto
 * <p>
 * @author   kuangxiang
 * @Date	 2016年10月22日
 */

public class DisCodesDto implements java.io.Serializable {

	/**
	 * TODO（用一句话描述这个变量表示什么）
	 */

	private static final long serialVersionUID = 2187056583882462104L;
	/**
	 * 区县代码
	 */
	private String disCode;
	/**
	 * 区县名字
	 */
	private String disName;

	public String getDisCode() {
		return disCode;
	}

	public void setDisCode(String disCode) {
		this.disCode = disCode;
	}

	public String getDisName() {
		return disName;
	}

	public void setDisName(String disName) {
		this.disName = disName;
	}

}
