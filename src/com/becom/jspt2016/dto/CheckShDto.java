/**
 * CheckSh.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

import java.io.Serializable;

/**
 * 检查审核Dto
 * 
 * @author   zhangchunming
 * @Date	 2016年10月26日 	 
 */
public class CheckShDto implements Serializable {

	/**
	 * TODO（用一句话描述这个变量表示什么）
	 */

	private static final long serialVersionUID = 1L;
	private int count;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
