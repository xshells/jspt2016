/**
 * TbJzgxxshLogDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

import java.io.Serializable;
import java.sql.Date;

/**
 * 教职工信息审核日志dto
 *
 * @author   zhangchunming
 * @Date	 2016年10月31日 	 
 */
public class TbJzgxxshLogDto implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 审核时间
	 */
	private Date shsj;

	/**
	 * 审核意见
	 */
	private String shyj;

	/**
	 * 审核人
	 */
	private String shr;

	/**
	 * 审核结果
	 */
	private String shjg;

	public String getShr() {
		return shr;
	}

	public void setShr(String shr) {
		this.shr = shr;
	}

	public String getShjg() {
		return shjg;
	}

	public void setShjg(String shjg) {
		this.shjg = shjg;
	}

	public Date getShsj() {
		return shsj;
	}

	public void setShsj(Date shsj) {
		this.shsj = shsj;
	}

	public String getShyj() {
		return shyj;
	}

	public void setShyj(String shyj) {
		this.shyj = shyj;
	}

}
