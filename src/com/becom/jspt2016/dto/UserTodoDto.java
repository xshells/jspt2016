/**
 * UserTodoDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

import java.io.Serializable;
import java.sql.Date;

/**
 * 系统用户待办事项dto
 *
 * @author   zhagnchunming
 * @Date	 2016年10月31日 	 
 */
public class UserTodoDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String xm;

	private Date sj;

	public String getXm() {
		return xm;
	}

	public void setXm(String xm) {
		this.xm = xm;
	}

	public Date getSj() {
		return sj;
	}

	public void setSj(Date sj) {
		this.sj = sj;
	}

}
