/**
 * RoleInfoDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

/**
 * 角色Dto
 * <p>
 * @author   kuangxiang
 * @Date	 2016年10月24日
 */

public class RoleInfoDto implements java.io.Serializable {

	/**
	 * TODO（用一句话描述这个变量表示什么）
	 */

	private static final long serialVersionUID = 2201357929627584834L;
	private String roleId;

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

}
