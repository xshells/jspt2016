/**
 * UserDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

/**
 * 用户Dto
 * <p>
 * @author   kuangxiang
 * @Date	 2016年10月21日
 */

public class UserDto implements java.io.Serializable {

	/**
	 * TODO（用一句话描述这个变量表示什么）
	 */

	private static final long serialVersionUID = 4225604259273647718L;
	private Integer userId;
	/**
	 * 用户类型
	 */
	private String userType;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

}
