/**
 * TeacherNumDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

import java.io.Serializable;

/**
 * 教师数量dto
 *
 * @author   zhangchunming
 * @Date	 2016年11月8日 	 
 */
public class TeacherNumDto implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 所属学段
	 */
	private String ssxd;

	/**
	 * 教师数量 
	 */
	private int num;

	/**
	 * 审核状态
	 */
	private String shzt;

	public String getSsxd() {
		return ssxd;
	}

	public void setSsxd(String ssxd) {
		this.ssxd = ssxd;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String getShzt() {
		return shzt;
	}

	public void setShzt(String shzt) {
		this.shzt = shzt;
	}

}
