/**
 * ZxdwlbZdxbDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

import java.io.Serializable;

/**
 * 在学单位类别dto
 *
 * @author   zhangchunming
 * @Date	 2016年10月25日 	 
 */
public class ZxdwlbZdxbDto implements Serializable {

	/**
	 * TODO（用一句话描述这个变量表示什么）
	 */
	private static final long serialVersionUID = 1L;

	private String zdxbm;
	private String zdxmc;
	private String fzdx;
	private String sfmrz;

	public String getZdxbm() {
		return zdxbm;
	}

	public void setZdxbm(String zdxbm) {
		this.zdxbm = zdxbm;
	}

	public String getZdxmc() {
		return zdxmc;
	}

	public void setZdxmc(String zdxmc) {
		this.zdxmc = zdxmc;
	}

	public String getFzdx() {
		return fzdx;
	}

	public void setFzdx(String fzdx) {
		this.fzdx = fzdx;
	}

	public String getSfmrz() {
		return sfmrz;
	}

	public void setSfmrz(String sfmrz) {
		this.sfmrz = sfmrz;
	}

}
