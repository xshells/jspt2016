/**
 * InputPInfoDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

/**
 * 个人信息录入Dto
 * <p>
 * @author   kuangxiang
 * @Date	 2016年10月24日
 */

public class InputPInfoDto implements java.io.Serializable {

	/**
	 * TODO（用一句话描述这个变量表示什么）
	 */

	private static final long serialVersionUID = 4886678110829217176L;
	/**
	 * 教职工Id
	 */
	private Integer jsId;

	/**
	 * 审核状态
	 */
	private String reviewStatus;
	/**
	 * 教育Id
	 */
	private String eduId;
	/**
	 * 老师姓名
	 */
	private String teacherName;
	/**
	 * 性别
	 */
	private String sex;
	/**
	 * 证件号码
	 */
	private String idNum;

	/**
	 * 国籍
	 */
	private String country;
	/**
	 * 身份类别
	 */
	private String shenFenType;
	/**
	 * 身份证号
	 */
	private String cardNO;
	/**
	 * 出生年月日
	 */
	private String birthday;
	/**
	 * 出生地
	 */
	private String birthdayAdress;
	/**
	 * 民族
	 */
	private String nation;
	/**
	 * 政治面貌
	 */
	private String politicalLandscape;
	/**
	 * 参加工作年月
	 */
	private String beginWorkDay;
	/**
	 * 进本校年月
	 */
	private String comeSchoolDay;
	/**
	 * 教职工来源
	 */
	private String teacherSource;
	/**
	 * 教职工类别
	 */
	private String teacherType;
	/**
	 * 是否在编
	 */
	private String isSeries;
	/**
	 * 用人形式
	 */
	private String usePersonType;
	/**
	 * 签订合同
	 */
	private String signContract;
	/**
	 * 人员转态
	 */
	private String personStatus;
	/**
	 * 创建时间
	 */
	private String createTime;

	public String getReviewStatus() {
		return reviewStatus;
	}

	public void setReviewStatus(String reviewStatus) {
		this.reviewStatus = reviewStatus;
	}

	public String getEduId() {
		return eduId;
	}

	public void setEduId(String eduId) {
		this.eduId = eduId;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getIdNum() {
		return idNum;
	}

	public void setIdNum(String idNum) {
		this.idNum = idNum;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getShenFenType() {
		return shenFenType;
	}

	public void setShenFenType(String shenFenType) {
		this.shenFenType = shenFenType;
	}

	public String getCardNO() {
		return cardNO;
	}

	public void setCardNO(String cardNO) {
		this.cardNO = cardNO;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getBirthdayAdress() {
		return birthdayAdress;
	}

	public void setBirthdayAdress(String birthdayAdress) {
		this.birthdayAdress = birthdayAdress;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getPoliticalLandscape() {
		return politicalLandscape;
	}

	public void setPoliticalLandscape(String politicalLandscape) {
		this.politicalLandscape = politicalLandscape;
	}

	public String getBeginWorkDay() {
		return beginWorkDay;
	}

	public void setBeginWorkDay(String beginWorkDay) {
		this.beginWorkDay = beginWorkDay;
	}

	public String getComeSchoolDay() {
		return comeSchoolDay;
	}

	public void setComeSchoolDay(String comeSchoolDay) {
		this.comeSchoolDay = comeSchoolDay;
	}

	public String getTeacherSource() {
		return teacherSource;
	}

	public void setTeacherSource(String teacherSource) {
		this.teacherSource = teacherSource;
	}

	public String getTeacherType() {
		return teacherType;
	}

	public void setTeacherType(String teacherType) {
		this.teacherType = teacherType;
	}

	public String getIsSeries() {
		return isSeries;
	}

	public void setIsSeries(String isSeries) {
		this.isSeries = isSeries;
	}

	public String getUsePersonType() {
		return usePersonType;
	}

	public void setUsePersonType(String usePersonType) {
		this.usePersonType = usePersonType;
	}

	public String getSignContract() {
		return signContract;
	}

	public void setSignContract(String signContract) {
		this.signContract = signContract;
	}

	public String getPersonStatus() {
		return personStatus;
	}

	public void setPersonStatus(String personStatus) {
		this.personStatus = personStatus;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public Integer getJsId() {
		return jsId;
	}

	public void setJsId(Integer jsId) {
		this.jsId = jsId;
	}

}
