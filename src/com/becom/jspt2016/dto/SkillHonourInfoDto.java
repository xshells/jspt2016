/**
 * GnpxxxInfoDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

import java.util.Date;

/**
 
 *			技能及证书dto
 * @author   wxl
 * @Date	 2016年10月18日 	 
 */
public class SkillHonourInfoDto implements java.io.Serializable {

	private static final long serialVersionUID = -3419813743429213268L;
	/**
	 * 技能证书id
	 */
	private long jnzsId;
	/**
	 * 教职工Id
	 */
	private long jsid;
	/**
	 * 语种
	 */
	private String yz;
	/**
	 * 掌握程度
	 */
	private String zwcd;

	public long getJnzsId() {
		return jnzsId;
	}

	public void setJnzsId(long jnzsId) {
		this.jnzsId = jnzsId;
	}

	public long getJsid() {
		return jsid;
	}

	public void setJsid(long jsid) {
		this.jsid = jsid;
	}

	public String getYz() {
		return yz;
	}

	public void setYz(String yz) {
		this.yz = yz;
	}

	public String getZwcd() {
		return zwcd;
	}

	public void setZwcd(String zwcd) {
		this.zwcd = zwcd;
	}

	public Date getCjsj() {
		return cjsj;
	}

	public void setCjsj(Date cjsj) {
		this.cjsj = cjsj;
	}

	/**
	 * 创建时间
	 */
	private Date cjsj;

}
