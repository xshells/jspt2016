/**
 * AduitLogDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

/**
 * 审核日志Dto
 * <p>
 * @author   kuangxiang
 * @Date	 2016年10月26日
 */

public class AduitLogDto implements java.io.Serializable {

	/**
	 * TODO（用一句话描述这个变量表示什么）
	 */

	private static final long serialVersionUID = 4363586630794478102L;

	private long jsId;
	private String aduitOpinion;

	public long getJsId() {
		return jsId;
	}

	public void setJsId(long jsId) {
		this.jsId = jsId;
	}

	public String getAduitOpinion() {
		return aduitOpinion;
	}

	public void setAduitOpinion(String aduitOpinion) {
		this.aduitOpinion = aduitOpinion;
	}

}
