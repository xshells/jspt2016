/**
 * ExchangeRotationInfoDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

/**
 * 交流轮岗Dto
 * <p>
 * @author   kuangxiang
 * @Date	 2016年10月20日
 */

public class ExchangeRotationInfoDto implements java.io.Serializable {

	private static final long serialVersionUID = 4972478224355690703L;
	/**
	 * 交流轮岗id
	 */
	private Integer exchangeRotationId;
	/**
	 * 交流轮岗类型
	 */
	private String exchangeRotationType;
	/**
	 * 是否有人事变动
	 */
	private String perMobilization;
	/**
	 * 开始年月
	 */
	private String beginYearM;
	/**
	 * 原单位
	 */
	private String beforeCompany;
	/**
	 * 交流轮岗单位名称
	 */
	private String nowCompany;
	/**
	 * 创建时间
	 */
	private String createTime;
	/**
	 * 教职工id
	 */
	private long jsId;

	public long getJsId() {
		return jsId;
	}

	public void setJsId(long jsId) {
		this.jsId = jsId;
	}

	/**
	 * 交流轮岗类型名称
	 */
	private String exchangeRotationTypeName;

	public String getExchangeRotationTypeName() {
		return exchangeRotationTypeName;
	}

	public void setExchangeRotationTypeName(String exchangeRotationTypeName) {
		this.exchangeRotationTypeName = exchangeRotationTypeName;
	}

	public Integer getExchangeRotationId() {
		return exchangeRotationId;
	}

	public void setExchangeRotationId(Integer exchangeRotationId) {
		this.exchangeRotationId = exchangeRotationId;
	}

	public String getExchangeRotationType() {
		return exchangeRotationType;
	}

	public void setExchangeRotationType(String exchangeRotationType) {
		this.exchangeRotationType = exchangeRotationType;
	}

	public String getPerMobilization() {
		return perMobilization;
	}

	public void setPerMobilization(String perMobilization) {
		this.perMobilization = perMobilization;
	}

	public String getBeginYearM() {
		return beginYearM;
	}

	public void setBeginYearM(String beginYearM) {
		this.beginYearM = beginYearM;
	}

	public String getBeforeCompany() {
		return beforeCompany;
	}

	public void setBeforeCompany(String beforeCompany) {
		this.beforeCompany = beforeCompany;
	}

	public String getNowCompany() {
		return nowCompany;
	}

	public void setNowCompany(String nowCompany) {
		this.nowCompany = nowCompany;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

}
