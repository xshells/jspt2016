/**
 * TbCfgZdxbInfoDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

/**
 * 字典项表Dto
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   shanjigang
 * @Date	 2016年10月24日 	 
 */
public class TbCfgZdxbInfoDto implements java.io.Serializable {

	private static final long serialVersionUID = 725516141823366478L;
	/**
	 * 字典项编码
	 */
	private String zdxbm;
	/**
	 * 字典项名称
	 */
	private String zdxmc;
	/**
	 * 父字典项
	 */
	private String fzdx;

	public String getZdxbm() {
		return zdxbm;
	}

	public void setZdxbm(String zdxbm) {
		this.zdxbm = zdxbm;
	}

	public String getZdxmc() {
		return zdxmc;
	}

	public void setZdxmc(String zdxmc) {
		this.zdxmc = zdxmc;
	}

	public String getFzdx() {
		return fzdx;
	}

	public void setFzdx(String fzdx) {
		this.fzdx = fzdx;
	}

}
