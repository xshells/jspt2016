/**
 * TeacherInfoAuditDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

/**
 * 教师信息审核Dto
 * <p>
 * @author   kuangxiang
 * @Date	 2016年10月25日
 */

public class TeacherInfoAuditDto implements java.io.Serializable {

	/**
	 * TODO（用一句话描述这个变量表示什么）
	 */

	private static final long serialVersionUID = 6184973376892108273L;
	/**
	 * 教职工Id
	 */
	private Integer jsId;
	/**
	 * 审核装态
	 */
	private String aduitStutas;
	/**
	 * 审核内容
	 */
	private String aduitComment;
	/**
	 * 审核Id
	 */
	private String eduId;
	/**
	 * 老师名字
	 */
	private String teacherName;
	/**
	 * 性别
	 */
	private String sex;
	/**
	 * 身份证号
	 */
	private String cardNo;
	/**
	 * 出生日期
	 */
	private String birthDay;
	/**
	 * 申请日期
	 */
	private String aduitDay;
	/**
	 * 区县名称
	 */
	private String disName;
	
	private String xd;
	private String unitType;
	private String jgName;
	
	public String getXd() {
		return xd;
	}

	public void setXd(String xd) {
		this.xd = xd;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	public String getJgName() {
		return jgName;
	}

	public void setJgName(String jgName) {
		this.jgName = jgName;
	}

	public String getDisName() {
		return disName;
	}

	public void setDisName(String disName) {
		this.disName = disName;
	}

	public Integer getJsId() {
		return jsId;
	}

	public void setJsId(Integer jsId) {
		this.jsId = jsId;
	}

	public String getAduitStutas() {
		return aduitStutas;
	}

	public void setAduitStutas(String aduitStutas) {
		this.aduitStutas = aduitStutas;
	}

	public String getAduitComment() {
		return aduitComment;
	}

	public void setAduitComment(String aduitComment) {
		this.aduitComment = aduitComment;
	}

	public String getEduId() {
		return eduId;
	}

	public void setEduId(String eduId) {
		this.eduId = eduId;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}

	public String getAduitDay() {
		return aduitDay;
	}

	public void setAduitDay(String aduitDay) {
		this.aduitDay = aduitDay;
	}

}
