package com.becom.jspt2016.dto;

import java.io.Serializable;

/**
 * 系统用户dto
 * @author fmx
 *
 */
public class SysuserDto implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer userId;
	private String account;
	private String name;
	private String yhfw;//用户范围
	private Integer roleId;
	private String roleName;
	private Integer status;
	
	private String password;
	
	private String jgname;
	private String xd;
	
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getYhfw() {
		return yhfw;
	}
	public void setYhfw(String yhfw) {
		this.yhfw = yhfw;
	}
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getJgname() {
		return jgname;
	}
	public void setJgname(String jgname) {
		this.jgname = jgname;
	}
	public String getXd() {
		return xd;
	}
	public void setXd(String xd) {
		this.xd = xd;
	}
	
}
