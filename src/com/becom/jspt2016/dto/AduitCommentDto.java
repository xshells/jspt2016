/**
 * AduitCommentDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

import java.io.Serializable;

/**
 *审核内容Dto
 * <p>
 * @author   kuangxiang
 * @Date	 2016年10月25日
 */

public class AduitCommentDto implements Serializable {
	/**
	 * TODO（用一句话描述这个变量表示什么）
	 */

	private static final long serialVersionUID = -2526738677270295712L;
	/**
	 * 教职工Id
	 */
	private long jdId;
	/**
	 * 所属学段
	 */
	private String ssxd;
	/**
	 * 审核状态
	 */
	private String aduitStatus;
	/**
	 * 审核结果
	 */
	private String aduitResult;
	/**
	 * 审核意见
	 */
	private String aduitOpinion;
	/**
	 * 审核人
	 */
	private String aduitPerson;
	/**
	 * 审核人Id
	 */
	private long aduitPersonId;
	/**
	 * 审核人Ip
	 */
	private String aduitPersonIp;
	/**
	 * 信息类型
	 */
	private String xxlx;
	/**
	 * 审核Id
	 */
	private long aduitId;

	public long getAduitId() {
		return aduitId;
	}

	public void setAduitId(long aduitId) {
		this.aduitId = aduitId;
	}

	public String getXxlx() {
		return xxlx;
	}

	public void setXxlx(String xxlx) {
		this.xxlx = xxlx;
	}

	public long getJdId() {
		return jdId;
	}

	public void setJdId(long jdId) {
		this.jdId = jdId;
	}

	public String getSsxd() {
		return ssxd;
	}

	public void setSsxd(String ssxd) {
		this.ssxd = ssxd;
	}

	public String getAduitStatus() {
		return aduitStatus;
	}

	public void setAduitStatus(String aduitStatus) {
		this.aduitStatus = aduitStatus;
	}

	public String getAduitResult() {
		return aduitResult;
	}

	public void setAduitResult(String aduitResult) {
		this.aduitResult = aduitResult;
	}

	public String getAduitOpinion() {
		return aduitOpinion;
	}

	public void setAduitOpinion(String aduitOpinion) {
		this.aduitOpinion = aduitOpinion;
	}

	public String getAduitPerson() {
		return aduitPerson;
	}

	public void setAduitPerson(String aduitPerson) {
		this.aduitPerson = aduitPerson;
	}

	public long getAduitPersonId() {
		return aduitPersonId;
	}

	public void setAduitPersonId(long aduitPersonId) {
		this.aduitPersonId = aduitPersonId;
	}

	public String getAduitPersonIp() {
		return aduitPersonIp;
	}

	public void setAduitPersonIp(String aduitPersonIp) {
		this.aduitPersonIp = aduitPersonIp;
	}

}
