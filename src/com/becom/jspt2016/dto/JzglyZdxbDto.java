/**
 * TbCfgZdxbDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

import java.io.Serializable;

/**
 * TODO(这里用一句话描述这个类的作用)
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   ZhuanJunxiang
 * @Date	 2016年10月24日 	 
 */
public class JzglyZdxbDto implements Serializable {

	/**
	 * TODO（用一句话描述这个变量表示什么）
	 */

	private static final long serialVersionUID = 1L;

	private String zdxbm;
	private String zdxmc;
	private String fzdx;
	private String sfmrz;

	public String getSfmrz() {
		return sfmrz;
	}

	public void setSfmrz(String sfmrz) {
		this.sfmrz = sfmrz;
	}

	public String getZdxbm() {
		return zdxbm;
	}

	public void setZdxbm(String zdxbm) {
		this.zdxbm = zdxbm;
	}

	public String getZdxmc() {
		return zdxmc;
	}

	public void setZdxmc(String zdxmc) {
		this.zdxmc = zdxmc;
	}

	public String getFzdx() {
		return fzdx;
	}

	public void setFzdx(String fzdx) {
		this.fzdx = fzdx;
	}

}
