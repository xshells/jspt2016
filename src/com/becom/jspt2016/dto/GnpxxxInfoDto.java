/**
 * GnpxxxInfoDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

import java.util.Date;

/**
 
 *	国内培训dto
 * @author   wxl
 * @Date	 2016年10月18日 	 
 */
public class GnpxxxInfoDto implements java.io.Serializable {

	private static final long serialVersionUID = -3419813743429213268L;
	/**
	 * 国内培训Id
	 */
	private long gnpxId;
	/**
	 * 教职工Id
	 */
	private long jsid;
	/**
	 * 培训年度
	 */
	private String pxnd;
	/**
	 * 培训类别
	 */
	private String pxlb;
	/**
	 * 培训项目名称
	 */
	private String pxxmmc;
	/**
	 * 培训机构名称
	 */
	private String pxjgmc;
	/**
	 * 培训方式
	 */
	private String pxfs;
	/**
	 * 培训获得学时
	 */
	private String pxhdxs;
	/**
	 * 培训获得学分
	 */
	private String pxhdxf;
	/**
	 * 创建时间
	 */
	private Date cjsj;

	public long getGnpxId() {
		return gnpxId;
	}

	public void setGnpxId(long gnpxId) {
		this.gnpxId = gnpxId;
	}

	public long getJsid() {
		return jsid;
	}

	public void setJsid(long jsid) {
		this.jsid = jsid;
	}

	public String getPxnd() {
		return pxnd;
	}

	public void setPxnd(String pxnd) {
		this.pxnd = pxnd;
	}

	public String getPxlb() {
		return pxlb;
	}

	public void setPxlb(String pxlb) {
		this.pxlb = pxlb;
	}

	public String getPxxmmc() {
		return pxxmmc;
	}

	public void setPxxmmc(String pxxmmc) {
		this.pxxmmc = pxxmmc;
	}

	public String getPxjgmc() {
		return pxjgmc;
	}

	public void setPxjgmc(String pxjgmc) {
		this.pxjgmc = pxjgmc;
	}

	public String getPxfs() {
		return pxfs;
	}

	public void setPxfs(String pxfs) {
		this.pxfs = pxfs;
	}

	public String getPxhdxs() {
		return pxhdxs;
	}

	public void setPxhdxs(String pxhdxs) {
		this.pxhdxs = pxhdxs;
	}

	public String getPxhdxf() {
		return pxhdxf;
	}

	public void setPxhdxf(String pxhdxf) {
		this.pxhdxf = pxhdxf;
	}

	public Date getCjsj() {
		return cjsj;
	}

	public void setCjsj(Date cjsj) {
		this.cjsj = cjsj;
	}

}
