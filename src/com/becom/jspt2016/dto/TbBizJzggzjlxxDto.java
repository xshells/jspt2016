/**
 * TbBizJzggzjlxxDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * TODO(这里用一句话描述这个类的作用)
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   ZhuanJunxiang
 * @Date	 2016年10月20日 	 
 */
public class TbBizJzggzjlxxDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private long gzjlId;
	private long jsid;
	private String rzdwmc;
	private String rzksny;
	private String rzjsny;
	private String dwxzlb;
	private String rzgw;
	private String sfwxdj;
	private String bsrlb;
	private String shzt;
	private Date shsj;
	private String shjg;
	private String sfsc;
	private String cjr;
	private Date cjsj;
	private String xgr;
	private Date xgsj;

	public long getGzjlId() {
		return gzjlId;
	}

	public void setGzjlId(long gzjlId) {
		this.gzjlId = gzjlId;
	}

	public long getJsid() {
		return jsid;
	}

	public void setJsid(long jsid) {
		this.jsid = jsid;
	}

	public String getRzdwmc() {
		return rzdwmc;
	}

	public void setRzdwmc(String rzdwmc) {
		this.rzdwmc = rzdwmc;
	}

	public String getRzksny() {
		return rzksny;
	}

	public void setRzksny(String rzksny) {
		this.rzksny = rzksny;
	}

	public String getRzjsny() {
		return rzjsny;
	}

	public void setRzjsny(String rzjsny) {
		this.rzjsny = rzjsny;
	}

	public String getDwxzlb() {
		return dwxzlb;
	}

	public void setDwxzlb(String dwxzlb) {
		this.dwxzlb = dwxzlb;
	}

	public String getRzgw() {
		return rzgw;
	}

	public void setRzgw(String rzgw) {
		this.rzgw = rzgw;
	}

	public String getSfwxdj() {
		return sfwxdj;
	}

	public void setSfwxdj(String sfwxdj) {
		this.sfwxdj = sfwxdj;
	}

	public String getBsrlb() {
		return bsrlb;
	}

	public void setBsrlb(String bsrlb) {
		this.bsrlb = bsrlb;
	}

	public String getShzt() {
		return shzt;
	}

	public void setShzt(String shzt) {
		this.shzt = shzt;
	}

	public Date getShsj() {
		return shsj;
	}

	public void setShsj(Date shsj) {
		this.shsj = shsj;
	}

	public String getShjg() {
		return shjg;
	}

	public void setShjg(String shjg) {
		this.shjg = shjg;
	}

	public String getSfsc() {
		return sfsc;
	}

	public void setSfsc(String sfsc) {
		this.sfsc = sfsc;
	}

	public String getCjr() {
		return cjr;
	}

	public void setCjr(String cjr) {
		this.cjr = cjr;
	}

	public Date getCjsj() {
		return cjsj;
	}

	public void setCjsj(Date cjsj) {
		this.cjsj = cjsj;
	}

	public String getXgr() {
		return xgr;
	}

	public void setXgr(String xgr) {
		this.xgr = xgr;
	}

	public Date getXgsj() {
		return xgsj;
	}

	public void setXgsj(Date xgsj) {
		this.xgsj = xgsj;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
