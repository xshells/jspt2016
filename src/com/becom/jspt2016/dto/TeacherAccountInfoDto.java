/**
 * TeacherAccountInfoDto.java
 * com.becom.jspt2016.dto
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.dto;

/**
 * 教师用户查询Dto
 * <p>
 * @author   kuangxiang
 * @Date	 2016年10月17日
 */

public class TeacherAccountInfoDto implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 教职工Id
	 */
	private Integer jsId;

	/**
	 * 教育Id
	 */
	private String educationId;

	/**
	 * 身份证号码
	 */
	private String cardNo;

	/**
	 * 教师姓名
	 */
	private String teacherName;

	/**
	 * 账号状态
	 */
	private String accountStatus;

	/**
	 * 出生日期
	 */
	private String birthday;
	/**
	 * 身份类型
	 */
	private String shenfen;
	private String sex;

	public Integer getJsId() {
		return jsId;
	}

	public void setJsId(Integer jsId) {
		this.jsId = jsId;
	}

	public String getEducationId() {
		return educationId;
	}

	public void setEducationId(String educationId) {
		this.educationId = educationId;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getShenfen() {
		return shenfen;
	}

	public void setShenfen(String shenfen) {
		this.shenfen = shenfen;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

}
