/**
 * IInputPInfoManagerExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.List;
import java.util.Map;

import org.nestframework.commons.hibernate.IPage;

import com.becom.jspt2016.model.TbBizJzgjbxx;

/**
 * 个人信息录入
 * <p>
 * 学校机构
 * @author   kuangxiang
 * @Date	 2016年10月24日
 */

public interface IInputPInfoManagerExt {

	/**
	 * 搜索
	 * <p>
	 *
	 * @param param
	 * @param pageNo
	 * @param pageSize
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	*/

	public IPage doSearch(Map<String, Object> param, int pageNo, int pageSize);

	/**
	 * 删除
	 * <p>
	 * 机构删除个人信息
	 *
	 * @param ck
	*/

	public int delete(String[] ck);

	public Map<String, Object> shStatus(String jsId);

	public List<TbBizJzgjbxx> findAll(Map<String, Object> param);

}
