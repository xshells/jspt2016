/**
 * ITbBizJzggzjlxxManagerExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.Map;

import org.nestframework.commons.hibernate.IPage;

import com.becom.jspt2016.dto.TbBizJzggzjlxxDto;
import com.becom.jspt2016.model.TbBizJzggzjlxx;

/**
 * 教职工工作经历服务
 *
 * @author  zhangchunming
 * @Date	 2016年10月19日 	 
 */
public interface ITbBizJzggzjlxxManagerExt {

	/**
	 * 工作经历 分页查询
	 * <p>
	 * @param map
	 * @param pageNo
	 * @param pageSize
	 */

	IPage<TbBizJzggzjlxx> doSearch(Map<String, Object> map, int pageNo, int pageSize);

	/**
	 * 添加
	 * <p>
	 * @param tbBizJzggzjlxx
	 */
	void add(TbBizJzggzjlxx tbBizJzggzjlxx);

	/**
	 * 查询
	 * <p>
	 * @param 工作经历id
	 */
	TbBizJzggzjlxxDto get(long gzjlId);

	/**
	 * 更新
	 * <p>
	 * @param 工作经历id
	 */
	void edit(Map<String, Object> params);

	/**
	 * 删除
	 * <p>
	 * @param 工作经历id组成的字符串
	 */
	int deleteGz(String[] ck);

}
