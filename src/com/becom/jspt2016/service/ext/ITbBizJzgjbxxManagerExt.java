/**
 * TbBizJzgjbxxManagerExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.Map;

import org.nestframework.action.FileItem;

import com.becom.jspt2016.dto.CheckShDto;
import com.becom.jspt2016.model.TbBizJzgjbxx;

/**
 * 教职工基本信息管理
 *
 * @author   zhangchunming
 * @Date	 2016年10月17日 	 
 */
public interface ITbBizJzgjbxxManagerExt {

	public void saveUser(Map<String, Object> params, String ssxd);

	public void deletePhoto(Map<String, Object> params);

	public Map<String, String> checkPhoto(FileItem photo);

	public CheckShDto submitted(TbBizJzgjbxx jbxx);

	public void submitInfo(TbBizJzgjbxx jzgjbxx);

	public void cancelSubmit(TbBizJzgjbxx jzgjbxx);

	public String add(TbBizJzgjbxx jbxx, long jgid);

	public void yxsubmitInfo(TbBizJzgjbxx jzgjbxx, long bsrid);

	public CheckShDto syssubmitted(TbBizJzgjbxx jzgjbxx);

}
