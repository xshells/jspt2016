/**
 * TbBizJzgsdxxManager.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.commons.hibernate.IPage;
import org.nestframework.commons.hibernate.IRowHandler;
import org.nestframework.commons.hibernate.ISqlElement;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.TbBizJzgrxrcxmxxInfoDto;
import com.becom.jspt2016.dto.TbCfgZdxbInfoDto;
import com.becom.jspt2016.model.TbBizJzgrxrcxmxx;
import com.becom.jspt2016.service.ext.ITbBizJzgrxrcxmxxManagerExt;

/**
 * 入选人才项目实现类
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   shanjigang
 * @Date	 2016年10月20日 	 
 */
public class TbBizJzgrxrcxmxxManagerExt extends JdbcRootManager implements ITbBizJzgrxrcxmxxManagerExt {
	protected Logger logger = Logger.getLogger(this.getClass());
	/**
	 * 入选项目名称
	 */
	public String rxrcxmmc;
	/**
	 * 入选年月
	 */
	public String rxny;
	/**
	 * 创建时间
	 */
	public Date cjsj;
	/**
	 * 教师id
	 */
	public long jsid;
	/**
	 * 项目id
	 */
	public long xmid;

	public IPage<TbBizJzgrxrcxmxxInfoDto> doQuery(Map<String, Object> params, int pageNo, int pageSize) {
		try {

			IPage<TbBizJzgrxrcxmxxInfoDto> dtos = this.findPage("TbBizJzgrxrcxmxxManagerExt.doQuery.query",
					"TbBizJzgrxrcxmxxManagerExt.doQuery.count", params, pageNo, pageSize,
					new IRowHandler<TbBizJzgrxrcxmxxInfoDto>() {
						public TbBizJzgrxrcxmxxInfoDto handleRow(ResultSet rs) {
							TbBizJzgrxrcxmxxInfoDto dto = new TbBizJzgrxrcxmxxInfoDto();
							try {
								dto.setXmid(rs.getLong("xmid"));
								dto.setJsid(rs.getLong("jsid"));
								dto.setCjsj(rs.getDate("cjsj"));
								dto.setRxny(rs.getString("rxny"));
								dto.setRxrcxmmc(rs.getString("rxrcxmmc"));
								dto.setShzt(rs.getString("shzt"));
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			return dtos;
		} catch (Exception e) {
			logger.error("TeacherMoralityManagerExt doSearch() is error!", e);
		}
		return null;

	}

	public void addSeclectTeach(final TbBizJzgrxrcxmxx jyjx, final String jsid) {
		StringBuffer sql = new StringBuffer();
		sql.append(" insert into tb_biz_jzgrxrcxmxx (RXRCXM_ID ,JSID,BSRLB,SHZT,SHSJ,SHJG,SFSC,CJSJ,rxrcxmmc,rxny,cjr)")
				.append("values (SEQ_TB_BIZ_JZGRXRCXMXX.nextval,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {

				Date currentTime = new Date();
				ps.setString(1, jsid);
				ps.setString(2, "4");//jyjx.getBsrlb()
				ps.setString(3, "01");//jyjx.getShzt()
				ps.setTimestamp(4, new java.sql.Timestamp(jyjx.getShsj().getTime()));
				ps.setString(5, "");
				ps.setString(6, jyjx.getSfsc());
				ps.setTimestamp(7, new java.sql.Timestamp(jyjx.getCjsj().getTime()));
				ps.setString(8, jyjx.getRxrcxmmc());
				ps.setString(9, jyjx.getRxny());
				ps.setString(10, jyjx.getCjr());
			}
		});
	}

	public void editSeclectTeach(String rxrcxmmc, String rxny, String xmid) {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("rxrcxmmc", rxrcxmmc);
			params.put("rxny", rxny);
			params.put("xmid", xmid);
			ISqlElement se = processSql(params, "TbBizJzgrxrcxmxxManagerExt.editSeclectTeach.update");
			getJdbcTemplate().update(se.getSql(), se.getParams());

		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
	}

	public void delSeclectTeach(String[] ids) {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("ids", ids);
			params.put("sfsc", 1);
			ISqlElement se = processSql(params, "TbBizJzgrxrcxmxxManagerExt.delSeclectTeach.del");
			getJdbcTemplate().update(se.getSql(), se.getParams());

		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
	}

	/**
	 * 
	 * 查询高校入选人才项目树的信息集合
	 * 
	 * @return list
	 */
	public List<TbCfgZdxbInfoDto> searchData() {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			List<TbCfgZdxbInfoDto> dtos = this.findList("TbBizJzgrxrcxmxxManagerExt.searchData.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TbCfgZdxbInfoDto dto = new TbCfgZdxbInfoDto();
							dto.setFzdx(rs.getString("fzdx"));
							dto.setZdxbm(rs.getString("zdxbm"));
							dto.setZdxmc(rs.getString("zdxmc"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

	/**
	 * 
	 * 查询中小学入选人才项目树信息列表
	 *
	 * @return list
	 */
	public List<TbCfgZdxbInfoDto> searchDatazxx() {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			List<TbCfgZdxbInfoDto> dtos = this.findList("TbBizJzgrxrcxmxxManagerExt.searchDatazxx.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TbCfgZdxbInfoDto dto = new TbCfgZdxbInfoDto();
							dto.setFzdx(rs.getString("fzdx"));
							dto.setZdxbm(rs.getString("zdxbm"));
							dto.setZdxmc(rs.getString("zdxmc"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

	public TbBizJzgrxrcxmxxInfoDto editSelectedTalent(long jsid, long xmid) {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsid", jsid);
			params.put("xmid", xmid);
			List<TbBizJzgrxrcxmxxInfoDto> dtos = this.findList("TbBizJzgrxrcxmxxManagerExt.editSelectedTalent.query",
					params, new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TbBizJzgrxrcxmxxInfoDto dto = new TbBizJzgrxrcxmxxInfoDto();
							dto.setRxrcxmmc(rs.getString("rxrcxmmc"));
							dto.setRxny(rs.getString("rxny"));
							//							dto.setCjsj(rs.getDate("cjsj"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

}
