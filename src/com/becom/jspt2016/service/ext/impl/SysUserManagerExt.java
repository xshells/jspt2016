/**
 * SysUserManagerExt.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.InstitutionsInfoDto;
import com.becom.jspt2016.dto.RoleInfoDto;
import com.becom.jspt2016.service.ext.ISysUserManagerExt;

/**
 * 用户业务层
 * <p>
 * @author   kuangxiang
 * @Date	 2016年10月21日
 */

public class SysUserManagerExt extends JdbcRootManager implements ISysUserManagerExt {
	protected Logger logger = Logger.getLogger(this.getClass());
	/**
	 * 校级管理员代码
	 */
	private static final String XJADMINISTRATOR = "3";
	/**
	 * 学校审核员	
	 */
	private static final String ORDERtYPE = "311";
	/**
	 *  默认为1；	1：启用0：禁用 
	 */
	private static final String SFZY = "1";

	/**
	 * 添加用户
	 * 
	 */
	@Override
	public void add(final InstitutionsInfoDto institutionDto) {
		StringBuffer sql = new StringBuffer();
		sql.append("insert into SYS_USER(USER_ID,USER_NAME,USER_PWD,USER_TYPE ,ROLE_ID,SFZY,CJSJ,ID)").append(
				"values (SEQ_SYS_USER.nextval,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				//获取当前管理员信息
				ps.setString(1, institutionDto.getInstitutionCode());
				ps.setString(2, DigestUtils.md5Hex("123456"));
				ps.setString(3, XJADMINISTRATOR);//用户类型
				ps.setString(4, fetchRoleId(XJADMINISTRATOR, institutionDto.getAffiliatedSchool()));//角色Id
				ps.setString(5, SFZY);
				ps.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
				ps.setString(7, institutionDto.getDistrictCode());//设置ID
			}
		});

	}

	/**
	 * 获取角色id
	 * <p>
	 *
	 * @param userType 用户类型
	 * @param ssxd 所属学段
	 */
	public String fetchRoleId(String userType, String ssxd) {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("userType", userType);
			params.put("ssxd", ssxd);
			List<RoleInfoDto> dtos = this.findList("SysUserManagerExt.fetchRoleId.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					RoleInfoDto dto = new RoleInfoDto();
					dto.setRoleId(rs.getString("roleId"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0).getRoleId();
			}
		} catch (Exception e) {
			logger.error("SysUserManagerExt fetchRoleId() is error!", e);
		}
		return null;
	}
}
