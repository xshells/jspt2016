/**
 * SkillHonourManagerExt.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.commons.hibernate.IPage;
import org.nestframework.commons.hibernate.IRowHandler;
import org.nestframework.commons.hibernate.ISqlElement;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.SkillHonourInfoDto;
import com.becom.jspt2016.model.TbBizJzgyynlxx;
import com.becom.jspt2016.service.ext.ISkillHonourManagerExt;

/**
 *  技能及证书业务层
 * <p>
 * @author   wxl
 * @Date	 2016年10月20日 	 
 */
public class SkillHonourManagerExt extends JdbcRootManager implements ISkillHonourManagerExt {

	protected Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 技能及证书分页列表
	 */
	@Override
	public IPage<TbBizJzgyynlxx> pageSearch(Map<String, Object> params, int pageNo, int pageSize) {
		try {
			IPage<TbBizJzgyynlxx> dtos = this.findPage("SkillHonourManagerExt.pageSearch.query",
					"SkillHonourManagerExt.pageSearch.count", params, pageNo, pageSize,
					new IRowHandler<TbBizJzgyynlxx>() {
						public TbBizJzgyynlxx handleRow(ResultSet rs) {
							TbBizJzgyynlxx dto = new TbBizJzgyynlxx();
							try {
								dto.setJnzsId(rs.getLong("jnzs_Id"));
								dto.setYz(rs.getString("yz"));
								dto.setZwcd(rs.getString("zwcd"));
								dto.setCjsj(rs.getDate("cjsj"));
							} catch (Exception e) {
								logger.error("SkillHonourManagerExt pageSearch dto is error!", e);
							}
							return dto;
						}
					});
			return dtos;
		} catch (Exception e) {
			logger.error("SkillHonourManagerExt pageSearch dtos is error!", e);
		}
		return null;
	}

	/**
	 * 删除
	 * 技能及证书 
	 */
	@Override
	public int deleteSkillHonour(String[] ids) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ids", ids);
		try {
			ISqlElement processSql = processSql(params, "SkillHonourManagerExt.updateDeleteSkillHonour.delete");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = -1;
			logger.error("SkillHonourManagerExt deleteSkillHonour is error!", e);
		}
		return result;
	}

	/**
	 * 新增
	 * 技能及证书
	 */
	@Override
	public void insertSkillHonour(final TbBizJzgyynlxx yynlxx) {

		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO TB_BIZ_JZGYYNLXX(jnzs_id,jsid,yz,zwcd,cjsj,sfsc,bsrlb,shzt,shsj,shjg,cjr) ").append(
				"values (SEQ_TB_BIZ_JZGYYNLXX.nextval,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, yynlxx.getTbBizJzgjbxx().getJsid());
				ps.setString(2, yynlxx.getYz());
				ps.setString(3, yynlxx.getZwcd());
				ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
				ps.setString(5, "0");
				ps.setString(6, "4");
				ps.setString(7, "01");
				ps.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
				ps.setString(9, null);
				ps.setLong(10, yynlxx.getTbBizJzgjbxx().getJsid());
			}
		});

	}

	/**
	 * 根据技能及证书id
	 * 获取技能及证书信息
	 */
	@Override
	public SkillHonourInfoDto getSkillHonour(long jnzsId) {

		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jnzsId", jnzsId);
			List<SkillHonourInfoDto> dtos = this.findList("SkillHonourManagerExt.toUpdateSkillHonour.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							SkillHonourInfoDto dto = new SkillHonourInfoDto();
							dto.setJnzsId(rs.getLong("jnzsId"));
							dto.setYz(rs.getString("yz"));
							dto.setZwcd(rs.getString("zwcd"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("SkillHonourManagerExt getSkillHonour is error!", e);
		}
		return null;

	}

	/**
	 * 根据技能及证书id
	 * 更新技能及证书信息
	 */
	@Override
	public int updateSkillHonour(TbBizJzgyynlxx tbBizJzgyynlInfo) {

		int iOk = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jnzsId", tbBizJzgyynlInfo.getJnzsId());
		params.put("yz", tbBizJzgyynlInfo.getYz());
		params.put("zwcd", tbBizJzgyynlInfo.getZwcd());
		try {
			ISqlElement se = processSql(params, "SkillHonourManagerExt.updateSkillHonour.update");
			getJdbcTemplate().update(se.getSql(), se.getParams());
		} catch (Exception e) {
			logger.error("SkillHonourManagerExt updateSkillHonour is error!", e);
			iOk = -1;
		}
		return iOk;

	}

	@Override
	public TbBizJzgyynlxx search(Long jsid, String yz) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("yz", yz);
		params.put("jsid", jsid);
		try {
			List<TbBizJzgyynlxx> dtos = this.findList("SkillHonourManagerExt.search.query", params, new RowMapper() {
				@Override
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbBizJzgyynlxx dto = new TbBizJzgyynlxx();
					dto.setJnzsId(rs.getLong("jnzs_Id"));
					dto.setYz(rs.getString("yz"));
					dto.setZwcd(rs.getString("zwcd"));
					dto.setCjsj(rs.getDate("cjsj"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("SkillHonourManagerExt search is error!", e);
		}
		return null;

	}

	@Override
	public TbBizJzgyynlxx searchUpdate(Long jsid, Long jnzsId, String yz) {

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jnzsId", jnzsId);
		params.put("jsid", jsid);
		params.put("yz", yz);
		try {
			List<TbBizJzgyynlxx> dtos = this.findList("SkillHonourManagerExt.searchUpdate.query", params,
					new RowMapper() {
						@Override
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TbBizJzgyynlxx dto = new TbBizJzgyynlxx();
							dto.setJnzsId(rs.getLong("jnzs_Id"));
							dto.setYz(rs.getString("yz"));
							dto.setZwcd(rs.getString("zwcd"));
							dto.setCjsj(rs.getDate("cjsj"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("SkillHonourManagerExt search is error!", e);
		}
		return null;

	}

}
