/**
 * STeacherInfoAduitExt.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.commons.hibernate.IPage;
import org.nestframework.commons.hibernate.IRowHandler;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.AduitCommentDto;
import com.becom.jspt2016.dto.InstitutionsInfoDto;
import com.becom.jspt2016.dto.TeacherInfoAuditDto;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.service.ext.ISTeacherInfoAduitExt;
import com.becom.jspt2016.service.ext.ITeacherInfoAuditExt;

/**
 *教师信息审核
 * <p>
 * 学校机构审核
 * @author   kuangxiang
 * @Date	 2016年10月27日
 */

public class STeacherInfoAduitExt extends JdbcRootManager implements ISTeacherInfoAduitExt {
	protected Logger logger = Logger.getLogger(this.getClass());
	/**
	 * 是否删除 0-否 1-是 默认0
	 */
	private static final String SFZY = "0";
	/**
	 * 11-学校（机构）审核通过
	 */
	private static final String SADUITYES = "11";
	/**
	 * 12-学校（机构）审核不通过
	 */
	private static final String SADUITNO = "12";
	/**
	 * 未报送
	 */
	private static final String NOSUBMIT = "01";
	/**
	 * 审核结果不通过 0-审核中 1-通过 2-不通过 
	 */
	private static final String NOADOPT = "2";
	/**
	 * 审核结果通过
	 */
	private static final String ADOPT = "1";
	/**
	 * 审核结果  审核中
	 */
	private static final String WAITADOPT = "0";
	/**
	 * 信息类型  01 是基本信息
	 */
	private static final String XXLX = "01";

	@Spring
	private ITeacherInfoAuditExt teacherInfoAuditExt;

	/**
	 * 搜索
	 * 
	 */
	@Override
	public IPage doSearch(Map<String, Object> param, int pageNo, int pageSize) {
		try {
			param.put("sfsc", SFZY);//是否删除 默认是0不删除
			IPage<TeacherInfoAuditDto> dtos = this.findPage("STeacherInfoAduitExt.doSearch.query",
					"STeacherInfoAduitExt.doSearch.count", param, pageNo, pageSize,
					new IRowHandler<TeacherInfoAuditDto>() {
						public TeacherInfoAuditDto handleRow(ResultSet rs) {
							TeacherInfoAuditDto dto = new TeacherInfoAuditDto();
							try {
								dto.setJsId(Integer.valueOf(rs.getString("jsId")));
								SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
								if (rs.getTimestamp("aduitDay") != null) {
									dto.setAduitDay(sf.format(rs.getTimestamp("aduitDay")));
								}
								dto.setAduitStutas(rs.getString("aduitStutas"));
								dto.setBirthDay(rs.getString("birthDay"));
								dto.setCardNo(rs.getString("cardNo"));
								dto.setEduId(rs.getString("eduId"));
								dto.setSex(rs.getString("sex"));
								dto.setTeacherName(rs.getString("teacherName"));
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			return dtos;
		} catch (Exception e) {
			logger.error("STeacherInfoAduitExt doSearch() is error!", e);
		}
		return null;
	}

	/**
	 * 审核
	 * 1.审核基本信息表
	 * 2.审核表
	 * 3.添加审核日志表
	 * 4,一堆的子表
	 * 
	 */
	@Override
	public void aduit(String[] ck, String isAdopt, String aduitOpinion, InstitutionsInfoDto institution, SysUser sUser,
			String ip) {
		for (String c : ck) {
			long jsId = Long.valueOf(c);
			String aduitStatus = "";
			String aduitResult = "";
			if ("1".equals(isAdopt) || isAdopt == null) {
				//审核通过
				aduitStatus = SADUITYES;
				aduitResult = WAITADOPT;
			} else if ("2".equals(isAdopt)) {
				//审核不通过
				aduitStatus = SADUITNO;
				aduitResult = NOADOPT;
			}
			AduitCommentDto adDto = new AduitCommentDto();
			adDto.setJdId(jsId);
			adDto.setAduitStatus(aduitStatus);
			adDto.setAduitResult(aduitResult);
			adDto.setAduitOpinion(aduitOpinion);
			adDto.setSsxd(institution.getAffiliatedSchool());
			adDto.setAduitPerson(institution.getInstitutionCode());
			adDto.setAduitPersonId(sUser.getUserId());
			adDto.setAduitPersonIp(ip);

			teacherInfoAuditExt.aduitBasicInfo(adDto);
			if (queryAduitTable(jsId).size() > 0) {
				teacherInfoAuditExt.updateAduit(adDto);//学校审核时需要判断有没有这个表有的话就更新，没有就添加
			} else {
				addAduit(adDto);
			}
			long aduitId = teacherInfoAuditExt.fetchAduitBInfo(jsId);
			adDto.setAduitId(aduitId);
			teacherInfoAuditExt.addAduitLog(adDto);

			teacherInfoAuditExt.childrenTableAduit(adDto);
		}
	}

	/**
	 * 查询审核表
	 * <p>
	 * @param jsId 教职工Id
	 */
	public List<AduitCommentDto> queryAduitTable(long jsId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jsId", jsId);
		try {
			List<AduitCommentDto> list = this.findList("STeacherInfoAduitExt.queryAduitTable.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							AduitCommentDto dto = new AduitCommentDto();
							dto.setJdId(rs.getLong("jsId"));
							return dto;
						}
					});
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 添加审核表
	 * <p>
	 *添加教师基本信息审核表
	 * @param adDto 审核内容Dto
	 */
	public void addAduit(final AduitCommentDto adDto) {
		StringBuffer sql = new StringBuffer();
		sql.append("insert into TB_JZGXXSH(SH_ID,JSID,SSXD,XXLB,SHZT,SHJG,SHYJ,SHSJ,SHR,SHR_USER_ID,SHR_IP)").append(
				"values (SEQ_TB_JZGXXSH.nextval,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				//获取当前管理员信息
				ps.setLong(1, adDto.getJdId());//机构代码
				ps.setString(2, adDto.getSsxd());
				ps.setString(3, XXLX);
				ps.setString(4, adDto.getAduitStatus());
				ps.setString(5, adDto.getAduitResult());
				ps.setString(6, adDto.getAduitOpinion());
				ps.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
				ps.setString(8, adDto.getAduitPerson());
				ps.setLong(9, adDto.getAduitPersonId());
				ps.setString(10, adDto.getAduitPersonIp());
			}
		});
	}

	public ITeacherInfoAuditExt getTeacherInfoAuditExt() {
		return teacherInfoAuditExt;
	}

	public void setTeacherInfoAuditExt(ITeacherInfoAuditExt teacherInfoAuditExt) {
		this.teacherInfoAuditExt = teacherInfoAuditExt;
	}

}
