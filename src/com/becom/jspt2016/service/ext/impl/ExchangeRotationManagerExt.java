/**
 * ExchangeRotationManagerExt.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.commons.hibernate.IPage;
import org.nestframework.commons.hibernate.IRowHandler;
import org.nestframework.commons.hibernate.ISqlElement;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.ExchangeRotationInfoDto;
import com.becom.jspt2016.service.ext.IExchangeRotationManagerExt;

/**
 * 交流轮岗业务层
 * <p>
 * @author   kuangxiang
 * @Date	 2016年10月20日
 */

public class ExchangeRotationManagerExt extends JdbcRootManager implements IExchangeRotationManagerExt {
	protected Logger logger = Logger.getLogger(this.getClass());
	/**
	 * 0-否 1-是 
	 */
	private static final String SFSC = "0";
	/**
	 * 是否删除 是：1
	 */
	private static final String SFSCYES = "1";
	/**
	 * 审核结果默认0
	 */
	private static final String SHJG = "0";
	/**
	 * 审核状态 01-未报送02-待审核11-学校（机构）审核通过12-学校（机构）审核不通过21-区县审核通过22-区县审核不通过 
	 */
	private static final String SHZT = "01";

	/**
	 * 查询
	 */
	@Override
	public IPage doSearch(Map<String, Object> param, int pageNo, int pageSize) {
		try {
			IPage<ExchangeRotationInfoDto> dtos = this.findPage("ExchangeRotationManagerExt.doSearch.query",
					"ExchangeRotationManagerExt.doSearch.count", param, pageNo, pageSize,
					new IRowHandler<ExchangeRotationInfoDto>() {
						public ExchangeRotationInfoDto handleRow(ResultSet rs) {
							ExchangeRotationInfoDto dto = new ExchangeRotationInfoDto();
							try {
								dto.setExchangeRotationId(Integer.valueOf(rs.getString("exchangeRotationId")));
								dto.setExchangeRotationType(rs.getString("exchangeRotationType"));
								dto.setPerMobilization(rs.getString("perMobilization"));
								//								String beginT = rs.getString("beginYearM");
								//								if (beginT != "") {
								//									dto.setBeginYearM(beginT.substring(0, beginT.lastIndexOf("-")));
								//								}
								dto.setBeginYearM(rs.getString("beginYearM"));
								dto.setBeforeCompany(rs.getString("beforeCompany"));
								dto.setNowCompany(rs.getString("nowCompany"));
								SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
								dto.setCreateTime(sf.format(rs.getTimestamp("createTime")));
								dto.setExchangeRotationTypeName(rs.getString("exchangeRotationTypeName"));
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			return dtos;
		} catch (Exception e) {
			logger.error("ExchangeRotationManagerExt doSearch() is error!", e);
		}
		return null;
	}

	/**
	 * 删除
	 */
	@Override
	public int delete(String[] ck) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ids", ck);
		params.put("sfsc", SFSCYES);
		try {
			ISqlElement processSql = processSql(params, "ExchangeRotationManagerExt.delete.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherAccountManageExt delete() is error!", e);
		}
		return result;
	}

	/**
	 * 添加
	 */
	@Override
	public void add(final ExchangeRotationInfoDto exchangeRotationInfoDto, final Long jsId) {

		//getSession().getAttribute(Constant.KEY_LOGIN_USER);
		StringBuffer sql = new StringBuffer();
		sql.append(
				"insert into TB_BIZ_JZGJLLGXX (JLLG_ID,JLLGLX,SFDDRSGX,KSNY,YDWMC,JLLGDWMC,CJSJ,JSID,BSRLB,SHZT,SHSJ,SHJG,SFSC,CJR)")
				.append("values (SEQ_TB_BIZ_JZGJLLGXX.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setString(1, exchangeRotationInfoDto.getExchangeRotationType());
				ps.setString(2, exchangeRotationInfoDto.getPerMobilization());
				//String time = exchangeRotationInfoDto.getBeginYearM();time.substring(0, time.lastIndexOf("-"))
				ps.setString(3, exchangeRotationInfoDto.getBeginYearM());
				ps.setString(4, exchangeRotationInfoDto.getBeforeCompany());
				ps.setString(5, exchangeRotationInfoDto.getNowCompany());
				ps.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
				ps.setLong(7, exchangeRotationInfoDto.getJsId());//教职工Id需要获取        -----------------------------
				ps.setString(8, "4");//报送者类别需要修改
				ps.setString(9, SHZT);
				ps.setTimestamp(10, new Timestamp(System.currentTimeMillis()));
				ps.setString(11, SHJG);
				ps.setString(12, SFSC);
				ps.setLong(13, jsId);
			}
		});
	}

	/**
	 * 通过交流轮岗Id查询交流轮岗信息
	 * 
	 */
	@Override
	public ExchangeRotationInfoDto get(long jllgId) {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("exchangeRotationId", jllgId);
			List<ExchangeRotationInfoDto> dtos = this.findList("ExchangeRotationManagerExt.get.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							ExchangeRotationInfoDto dto = new ExchangeRotationInfoDto();
							dto.setExchangeRotationId(rs.getInt("exchangeRotationId"));
							dto.setBeforeCompany(rs.getString("beforeCompany"));
							dto.setBeginYearM(rs.getString("beginYearM"));
							dto.setExchangeRotationType(rs.getString("exchangeRotationType"));
							dto.setNowCompany(rs.getString("nowCompany"));
							dto.setPerMobilization(rs.getString("perMobilization"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("ExchangeRotationManagerExt get() is error!", e);
		}
		return null;
	}

	/**
	 *编辑功能
	 * 
	 */
	@Override
	public int edit(ExchangeRotationInfoDto exchangeRotationInfoDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("exchangeRotationId", exchangeRotationInfoDto.getExchangeRotationId());
		params.put("exchangeRotationType", exchangeRotationInfoDto.getExchangeRotationType());
		params.put("perMobilization", exchangeRotationInfoDto.getPerMobilization());
		//String time = exchangeRotationInfoDto.getBeginYearM();time.subSequence(0, time.lastIndexOf("-"))
		params.put("beginYearM", exchangeRotationInfoDto.getBeginYearM());
		params.put("beforeCompany", exchangeRotationInfoDto.getBeforeCompany());
		params.put("nowCompany", exchangeRotationInfoDto.getNowCompany());
		params.put("editPerson", "");//修改人需要修改====================================================
		params.put("editTime", new Timestamp(System.currentTimeMillis()));
		try {
			ISqlElement processSql = processSql(params, "ExchangeRotationManagerExt.edit.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("ExchangeRotationManagerExt edit() is error!", e);
		}
		return result;
	}
}
