/**
 * InputPInfoManagerExt.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.commons.hibernate.IPage;
import org.nestframework.commons.hibernate.IRowHandler;
import org.nestframework.commons.hibernate.ISqlElement;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.InputPInfoDto;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.service.ext.IInputPInfoManagerExt;

/**
 *个人信息录入业务层
 * <p>
 * @author   kuangxiang
 * @author   zhangchunming
 * @Date	 2016年10月24日
 * @Date	 2016年10月28日 
 */

public class InputPInfoManagerExt extends JdbcRootManager implements IInputPInfoManagerExt {
	protected Logger logger = Logger.getLogger(this.getClass());
	/**
	 * 是否在用 默认0，0:否1：是
	 */
	private static final String SFZY = "0";
	/**
	 * 删除
	 */
	private static final String SFZYYES = "1";

	/**
	 *  未审核状态(未报送)
	 */
	private static final String WBSSHZT = "01";
	/**
	 *  学校不通过状态
	 */
	private static final String XXBTGSHZT = "12";
	/**
	 *  区县不通过状态
	 */
	private static final String QXBTGSHZT = "22";

	/**
	 * 搜索
	 * 
	 */
	@Override
	public IPage doSearch(Map<String, Object> param, int pageNo, int pageSize) {
		try {
			param.put("sfsc", SFZY);//是否在用
			IPage<InputPInfoDto> dtos = this.findPage("InputPInfoManagerExt.doSearch.query",
					"InputPInfoManagerExt.doSearch.count", param, pageNo, pageSize, new IRowHandler<InputPInfoDto>() {
						public InputPInfoDto handleRow(ResultSet rs) {
							InputPInfoDto dto = new InputPInfoDto();
							try {
								dto.setJsId(Integer.valueOf(rs.getString("jsId")));
								dto.setReviewStatus(rs.getString("reviewStatus"));
								dto.setEduId(rs.getString("eduId"));
								dto.setTeacherName(rs.getString("teacherName"));
								dto.setSex(rs.getString("sex"));
								dto.setCountry(rs.getString("country"));
								dto.setShenFenType(rs.getString("shenFenType"));
								dto.setCardNO(rs.getString("cardNO"));
								dto.setBirthday(rs.getString("birthday"));
								dto.setBirthdayAdress(rs.getString("birthdayAdress"));
								dto.setNation(rs.getString("nation"));
								dto.setPoliticalLandscape(rs.getString("politicalLandscape"));
								dto.setBeginWorkDay(rs.getString("beginWorkDay"));
								dto.setComeSchoolDay(rs.getString("comeSchoolDay"));
								dto.setTeacherSource(rs.getString("teacherSource"));
								dto.setTeacherType(rs.getString("teacherType"));
								dto.setIsSeries(rs.getString("isSeries"));
								dto.setUsePersonType(rs.getString("usePersonType"));
								dto.setSignContract(rs.getString("signContract"));
								dto.setPersonStatus(rs.getString("personStatus"));
								SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
								dto.setCreateTime(sf.format(rs.getTimestamp("createTime")));
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			return dtos;
		} catch (Exception e) {
			logger.error("InputPInfoManagerExt doSearch() is error!", e);
		}
		return null;
	}

	/**
	 * 删除
	 * 
	 */
	@Override
	public int delete(String[] ck) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ids", ck);
		params.put("sfsc", SFZYYES);
		try {
			ISqlElement processSql = processSql(params, "InputPInfoManagerExt.delete.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("InputPInfoManagerExt delete() is error!", e);
		}
		return result;
	}

	/**
	 * 获取审核状态
	 * 
	 */
	@Override
	public Map<String, Object> shStatus(String jsId) {

		Map<String, Object> param = new HashMap<String, Object>();
		param.put("jsId", jsId);
		try {
			List<Map<String, Object>> rolelist = this.findList("InputPInfoManagerExt.shStatus.query", param,
					new RowMapper() {

						@Override
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							Map<String, Object> map = new HashMap<String, Object>();
							map.put("jsId", rs.getString("jsId"));
							map.put("name", rs.getString("name"));
							map.put("shStatus", rs.getString("shStatus"));
							return map;
						}
					});
			return rolelist.size() > 0 ? rolelist.get(0) : null;
		} catch (Exception e) {
			logger.error("SysuserManageExt getQu() is error!", e);
		}
		return null;
	}

	/**
	 * 查询机构所有可以保送的教师
	 * (non-Javadoc)
	 * @see com.becom.jspt2016.service.ext.IInputPInfoManagerExt#findAll(java.util.Map)
	 */
	@Override
	public List<TbBizJzgjbxx> findAll(Map<String, Object> param) {

		try {
			param.put("sfzy", SFZY);
			StringBuffer sb = new StringBuffer();
			sb.append(WBSSHZT).append(",").append(XXBTGSHZT).append(",").append(QXBTGSHZT);
			param.put("shzts", sb.toString().split(","));
			List<TbBizJzgjbxx> dtos = this.findList("InputPInfoManagerExt.findAll.query", param, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbBizJzgjbxx dto = new TbBizJzgjbxx();
					dto.setJsid(rs.getLong("jsid"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("InputPInfoManagerExt findAll() is error!", e);
		}
		return null;

	}
}
