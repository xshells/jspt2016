/**
 * YearTestManagerExt.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.commons.hibernate.IPage;
import org.nestframework.commons.hibernate.IRowHandler;
import org.nestframework.commons.hibernate.ISqlElement;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.model.TbBizJzgndkhxx;
import com.becom.jspt2016.model.TbXxjgb;
import com.becom.jspt2016.service.ext.IYearTestManagerExt;

/**
 * (年度考核---ext实现类)
 * <p>
 * @author   董锡福
 * @Date	 2016年10月20日 	 
 */
public class YearTestManagerExt extends JdbcRootManager implements IYearTestManagerExt {

	protected Logger logger = Logger.getLogger(this.getClass());

	public IPage<TbBizJzgndkhxx> doSearch(Map<String, Object> params, int pageNo, int pageSize) {
		try {
			IPage<TbBizJzgndkhxx> dtos = this.findPage("YearTestManagerExt.findNdkh.query",
					"YearTestManagerExt.findNdkh.count", params, pageNo, pageSize, new IRowHandler<TbBizJzgndkhxx>() {
						public TbBizJzgndkhxx handleRow(ResultSet rs) {
							TbBizJzgndkhxx dto = new TbBizJzgndkhxx();
							try {
								dto.setNdkhId((rs.getLong("NDKH_ID")));
								dto.setKhnd((rs.getString("KHND")));
								dto.setKhdwmc((rs.getString("KHDWMC")));
								dto.setKhjg((rs.getString("KHJG")));
								dto.setCjsj(rs.getDate("CJSJ"));
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			if (dtos != null) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("YearTestManagerExt doSearch() is error!", e);
		}
		return null;
	}

	//添加
	public void SubAddYearTest(final TbBizJzgndkhxx kh) {
		StringBuffer sql = new StringBuffer();
		sql.append("insert into TB_BIZ_JZGNDKHXX(NDKH_ID,JSID,KHND,KHJG,KHDWMC,BSRLB,SHZT,SHSJ,SHJG,SFSC,CJR,CJSJ)")
				.append("values (SEQ_TB_BIZ_JZGNDKHXX.nextval,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, kh.getTbBizJzgjbxx().getJsid());
				ps.setString(2, kh.getKhnd());//考核年度
				ps.setLong(1, kh.getTbBizJzgjbxx().getJsid());
				ps.setString(3, kh.getKhjg());//考核结果
				ps.setString(4, kh.getKhdwmc());//考核单位名称
				ps.setString(5, "4");//报送人类别
				ps.setString(6, "01");//审核状态
				ps.setTimestamp(7, new java.sql.Timestamp(kh.getShsj().getTime()));//审核时间
				ps.setString(8, "");//审核结果
				ps.setString(9, "0");//是否删除
				ps.setLong(10, kh.getTbBizJzgjbxx().getJsid());//创建人
				ps.setTimestamp(11, new java.sql.Timestamp(kh.getCjsj().getTime()));//创建时间
			}
		});

	}

	//修改时回显
	public TbBizJzgndkhxx editYearTest(long ndkhId) {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("ndkhId", ndkhId);
			List<TbBizJzgndkhxx> dtos = this.findList("YearTestManagerExt.editNdkh.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbBizJzgndkhxx dto = new TbBizJzgndkhxx();
					dto.setNdkhId(rs.getLong("NDKH_ID"));
					dto.setKhnd(rs.getString("khnd"));//考核年度
					dto.setKhjg(rs.getString("khjg"));//考核结果
					dto.setKhdwmc(rs.getString("khdwmc"));//考核单位名称
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("YearTestManagerExt doSearch() is error!", e);
		}
		return null;
	}

	//执行修改
	public int updateYearTest(TbBizJzgndkhxx kh) {
		int result = 0;
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("ndkhId", kh.getNdkhId());
			params.put("khnd", kh.getKhnd()); //考核年度      
			params.put("khjg", kh.getKhjg()); //考核结果      
			params.put("khdwmc", kh.getKhdwmc()); //考核单位名称
			ISqlElement se = processSql(params, "YearTestManagerExt.doUpdateNdkh.update");
			getJdbcTemplate().update(se.getSql(), se.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("YearTestManagerExt doSearch() is error!", e);
		}
		return result;
	}

	//删除
	public int removeYearTest(String[] ck) {
		int result = 0;
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("ids", ck);
			ISqlElement se = processSql(params, "YearTestManagerExt.doDeleteNdkh.update");
			getJdbcTemplate().update(se.getSql(), se.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("YearTestManagerExt doSearch() is error!", e);
		}
		return result;
	}

	public String getOrgNm(Map<String, Object> params) {

		try {
			List<TbXxjgb> dtos = this.findList("YearTestManagerExt.query.orgName", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbXxjgb dto = new TbXxjgb();
					dto.setXxjgid(rs.getLong("XXJGID"));
					dto.setXxjgmc(rs.getString("XXJGMC"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0).getXxjgmc();
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}
}
