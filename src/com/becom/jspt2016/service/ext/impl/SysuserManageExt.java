package com.becom.jspt2016.service.ext.impl;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.commons.hibernate.IPage;
import org.nestframework.commons.hibernate.IRowHandler;
import org.nestframework.commons.hibernate.ISqlElement;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.SysuserDto;
import com.becom.jspt2016.service.ext.IExportExt;
import com.becom.jspt2016.service.ext.ISysuserManageExt;

public class SysuserManageExt extends JdbcRootManager implements ISysuserManageExt {
	protected Logger logger = Logger.getLogger(this.getClass());
	@Spring
	private IExportExt exportExt;
	/**
	 * 是否在用1-在用 0-禁用 
	 */
	private static final String SFZY = "1";
	/**
	 *禁用
	 */
	private static final String NOSFZY = "0";
	
	private static final String DEFAULTPWD = "123456";

	@Override
	public IPage doSearch(Map<String, Object> param, int pageNo, int pageSize) {
		try {
			IPage<SysuserDto> dtos = this.findPage("SysuserManageExt.doSearch.query",
					"SysuserManageExt.doSearch.count", param, pageNo, pageSize,
					new IRowHandler<SysuserDto>() {
						public SysuserDto handleRow(ResultSet rs) {
							SysuserDto dto = new SysuserDto();
							try {
								dto.setUserId(rs.getInt("userId"));
								dto.setAccount(rs.getString("account"));
								dto.setName(rs.getString("name"));
								dto.setYhfw(rs.getString("yhfw"));
								dto.setRoleId(rs.getInt("roleId"));
								dto.setRoleName(rs.getString("roleName"));
								dto.setStatus(rs.getInt("status"));
								String id = rs.getString("id");
								dto = getJgAndXd(rs.getString("yhfw"),rs.getString("id"),dto);
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			return dtos;
		} catch (Exception e) {
			logger.error("SysuserManageExt doSearch() is error!", e);
		}
		return null;
	}
	
	/**
	 * 
	 */
	public SysuserDto getJgAndXd(String yhfw,String idbm,SysuserDto dto){
		SysuserDto u = dto;
		if(yhfw != null){
			if("2".equals(yhfw)){
				u.setJgname(getQu(idbm));//区名放到什么地方？不知道啊！
			}else if("3".equals(yhfw)){
				Map<String,Object> map = getJg(idbm);
				if(map != null){
					u.setJgname(map.get("name").toString());
					u.setXd(map.get("xd").toString());
				}
			}
		}
		return u;
	}
	
	public String getQu(String quid){
		Map<String, Object> param = new HashMap<String,Object>();
		param.put("qxdm", quid);
		try {
			List<String> rolelist = this.findList("SysuserManageExt.getQu.query",param,new RowMapper(){

				@Override
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					return rs.getString("name");
				}
			});
			return rolelist.size()>0?rolelist.get(0):"";
		} catch (Exception e) {
			logger.error("SysuserManageExt getQu() is error!", e);
		}
		return null;
	}
	
	public List<Map<String,Object>> getFwList(){
		Map<String, Object> param = new HashMap<String,Object>();
		param.put("zdbs", "YHLX");
		try {
			List<Map<String,Object>> rolelist = this.findList("SysuserManageExt.getFw.query",param,new RowMapper(){

				@Override
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					Map<String,Object> map = new HashMap<String,Object>();
					map.put("id", rs.getInt("id"));
					map.put("name", rs.getString("name"));
					return map;
				}
			});
			return rolelist;
		} catch (Exception e) {
			logger.error("SysuserManageExt getRoleList() is error!", e);
		}
		return null;
	}
	
	public Map<String, Object> getJg(String jgId){
		Map<String, Object> param = new HashMap<String,Object>();
		param.put("jgId", jgId);
		try {
			List<Map<String, Object>> rolelist = this.findList("SysuserManageExt.getJg.query",param,new RowMapper(){

				@Override
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					Map<String, Object> map = new HashMap<String,Object>();
					map.put("name", rs.getString("name"));
					map.put("xd", rs.getString("xd"));
					return map;
				}
			});
			return rolelist.size()>0?rolelist.get(0):null;
		} catch (Exception e) {
			logger.error("SysuserManageExt getQu() is error!", e);
		}
		return null;
	}
	
	public int setStatus(String[] ck,String status) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ids", ck);
		params.put("sfzy", status);
		try {
			ISqlElement processSql = processSql(params, "SysuserManageExt.delete.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("SysuserManageExt delete() is error!", e);
		}
		return result;
	}
	
	public int reset(String[] ck) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ids", ck);
		params.put("password", DigestUtils.md5Hex(DEFAULTPWD));
		try {
			ISqlElement processSql = processSql(params, "SysuserManageExt.reset.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("SysuserManageExt delete() is error!", e);
		}
		return result;
	}
	////////////////////////////////////////////
	
	
	public SysuserDto getUser(String userId) {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("userId", userId);
			List<SysuserDto> dtos = this.findList("SysuserManageExt.getUser.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							SysuserDto dto = new SysuserDto();
							dto.setUserId(rs.getInt("userId"));
							dto.setAccount(rs.getString("account"));
							dto.setName(rs.getString("name"));
							dto.setYhfw(rs.getString("yhfw"));
							dto.setRoleId(rs.getInt("roleId"));
							dto.setRoleName(rs.getString("roleName"));
							dto.setStatus(rs.getInt("status"));
							dto.setPassword(rs.getString("password"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("InstitutionsManageExt getInstitution() is error!", e);
		}
		return null;
	}

	public List<Map<String,Object>> getRoleList(Map<String, Object> param){
		try {
			List<Map<String,Object>> rolelist = this.findList("SysuserManageExt.doSearch.role",param,new RowMapper(){

				@Override
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					Map<String,Object> map = new HashMap<String,Object>();
					map.put("roleId", rs.getInt("roleId"));
					map.put("roleName", rs.getString("roleName"));
					return map;
				}
			});
			return rolelist;
		} catch (Exception e) {
			logger.error("SysuserManageExt getRoleList() is error!", e);
		}
		return null;
	}
	
	public Boolean isExist(Map<String, Object> param){
		try {
			List<Integer> rolelist = this.findList("SysuserManageExt.doSearch.isExist", param,new RowMapper(){

				@Override
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					return rs.getInt(1);
				}
			});
			if(rolelist == null){
				return false;
			}
			return rolelist.size() > 0;
		} catch (Exception e) {
			logger.error("SysuserManageExt getRoleList() is error!", e);
		}
		return false;
	}
	
	@Override
	public void add(final SysuserDto userDto) {
		StringBuffer sql = new StringBuffer();
		sql.append(
				"insert into SYS_USER(USER_ID,USER_NAME,USER_PWD,USER_TYPE,ROLE_ID,ROLE_NAME,SFZY,CJSJ,ID)")
				.append("values (SEQ_SYS_USER.nextval,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				//获取当前管理员信息
				ps.setString(1, userDto.getAccount());//机构代码
				ps.setString(2, DigestUtils.md5Hex(userDto.getPassword()));
				ps.setString(3, userDto.getYhfw());
				ps.setInt(4, userDto.getRoleId());
				ps.setString(5, "");
				ps.setString(6, SFZY);
				ps.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
				ps.setString(8, "");
			}
		});
	}

	@Override
	public int edit(SysuserDto userDto) {
		int result = 0;
		Map<String, Object> pa = new HashMap<String, Object>();
		pa.put("roleId", userDto.getRoleId());
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", userDto.getUserId());
		params.put("account", userDto.getAccount());
		params.put("password", userDto.getPassword());
		params.put("name", userDto.getName());
		params.put("yhfw", userDto.getYhfw());
		params.put("roleId", userDto.getRoleId());
		params.put("roleName", getRoleList(pa).get(0).get("roleName"));
		params.put("xgsj", new Timestamp(System.currentTimeMillis()));
		
		try {
			ISqlElement processSql = processSql(params, "SysuserManageExt.edit.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("SysuserManageExt edit() is error!", e);
		}
		return result;
	}

	@Override
	public int delete(String[] ck) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ids", ck);
		params.put("sfzy", NOSFZY);
		try {
			ISqlElement processSql = processSql(params, "SysuserManageExt.delete.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("SysuserManageExt delete() is error!", e);
		}
		return result;
	}
	/////////////////////////////////////////////////////////////////
	
	public boolean isSd(String jsId){
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("jsId", jsId);
		try {
			List<Integer> dtos = this.findList("TeacherAudit.isSd.query",param,new RowMapper(){

				@Override
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					return rs.getInt(1);
				}
			});
			if(dtos != null && dtos.size() > 0){
				return dtos.get(0) > 0;
			}else{
				return false;
			}
		} catch (Exception e) {
			logger.error("SysuserManageExt isSd() is error!", e);
		}
		return false;
	}
	
	public boolean isCf(String jsId){
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("jsId", jsId);
		try {
			List<Integer> dtos = this.findList("TeacherAudit.isCf.query",param,new RowMapper(){

				@Override
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					return rs.getInt(1);
				}
			});
			if(dtos != null && dtos.size() > 0){
				return dtos.get(0) > 0;
			}else{
				return false;
			}
		} catch (Exception e) {
			logger.error("SysuserManageExt isCf() is error!", e);
		}
		return false;
	}
	
	/////////////////////
	
	
	public IExportExt getExportExt() {
		return exportExt;
	}

	public void setExportExt(IExportExt exportExt) {
		this.exportExt = exportExt;
	}

	
	public static void main(String[] args) throws NoSuchAlgorithmException{
		MessageDigest md5 = MessageDigest.getInstance("MD5");
		System.out.println();
	}
}
