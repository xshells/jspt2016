/**
 * ContactWayManagerExt.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.commons.hibernate.ISqlElement;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbBizJzglxfsxx;
import com.becom.jspt2016.service.ext.IContactWayManagerExt;

/**
 *   联系方式
 * <p>
 *
 * @author   wxl
 * @Date	 2016年10月21日 	 
 */
public class ContactWayManagerExt extends JdbcRootManager implements IContactWayManagerExt {

	protected Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 联系方式
	 * <p>
	 * @param 教职工id
	 */
	@Override
	public TbBizJzglxfsxx getContactWay(final long jsid) {

		try {
			Map<String, Object> params = new HashMap<String, Object>();

			params.put("jsid", jsid);
			List<TbBizJzglxfsxx> dtos = this.findList("ContactWayManagerExt.doSearch.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbBizJzglxfsxx dto = new TbBizJzglxfsxx();
					TbBizJzgjbxx jbxx = new TbBizJzgjbxx();
					jbxx.setJsid(jsid);
					dto.setTbBizJzgjbxx(jbxx);
					dto.setLxfsId(rs.getLong("lxfsId"));
					dto.setSj(rs.getString("sj"));
					dto.setEmail(rs.getString("email"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				TbBizJzglxfsxx a = dtos.get(0);
				return dtos.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 新增联系方式
	 */
	@Override
	public void insertContactWay(final TbBizJzglxfsxx lxfsxx) {
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO TB_BIZ_JZGLXFSXX(lxfs_id,jsid,sj,email,cjsj,sfsc,bsrlb,shzt,shsj,shjg,cjr) ").append(
				"values (SEQ_TB_BIZ_JZGLXFSXX.nextval,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, lxfsxx.getTbBizJzgjbxx().getJsid());
				ps.setString(2, lxfsxx.getSj());
				ps.setString(3, lxfsxx.getEmail());
				ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
				ps.setString(5, "0");
				ps.setString(6, "4");
				ps.setString(7, "01");
				ps.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
				ps.setString(9, null);
				ps.setLong(10, lxfsxx.getTbBizJzgjbxx().getJsid());
			}
		});
	}

	/**
	 * 新增联系方式
	 * <p> 
	 * @param 已有联系方式id
	 */
	@Override
	public int updateContactWay(TbBizJzglxfsxx contactWayInfo) {

		int iOk = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("lxfsId", contactWayInfo.getLxfsId());
		params.put("sj", contactWayInfo.getSj());
		params.put("email", contactWayInfo.getEmail());
		try {
			ISqlElement se = processSql(params, "ContactWayManagerExt.updateContactWay.update");
			getJdbcTemplate().update(se.getSql(), se.getParams());
		} catch (Exception e) {
			logger.error("ContactWayManagerExt updateContactWay is error!", e);
			iOk = -1;
		}
		return iOk;
	}
}
