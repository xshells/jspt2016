/**
 * ExportExt.java
 * com.becom.jspt2016.webapp.action.schoolManager
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.service.ext.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.InstitutionsInfoDto;
import com.becom.jspt2016.service.ext.IExportExt;

/**
 * 导入导出服务类
 * @author   fmx
 */

public class ExportExt extends JdbcRootManager implements IExportExt {
	protected Logger logger = Logger.getLogger(this.getClass());
	/**
	 * 是否在用1-在用 0-禁用 
	 */
	private static final String SFZY = "1";

	//////////////////////////////////////////////////////////////////

	/**
	 * 删除状态 0：否，
	 */
	private static final String NOSFSC = "0";
	/**
	 * 是否在职，统一为:1
	 */
	private static final String SFZZ = "1";
	/**
	 * 是否全日制师范类专业毕业   默认为0-否，1-是
	 */
	private static final String SFSFLZYBY = "0";
	/**
	 * 是否受过特教专业培养培训  默认为【0-否】0-否1-是
	 */
	private static final String SFSGTJZYPYPX = "0";
	/**
	 * 是否有特殊教育从业证书  	默认为【0-否】0-否1-是
	 */
	private static final String SFYTSJYCYZS = "0";
	/**
	 * 信息技术应用能力    1-精通2-熟练3-良好4-一般5-较弱
	 */
	private static final String XXJSYYNL = "1";

	/**
	 * 是否属于免费（公费）师范生     默认为【0-否】1-部属师大免费师范生 2-地方免费（公费）师范生 0-否
	 */
	private static final String SFSYMFSFS = "0";
	/**
	 * 报送人类别3-校级  4-教师 
	 */
	private static final String BSRLB = "3";
	/**
	 * 审核状态     01-未报送 02-待审核11-学校（机构）审核通过12-学校（机构）审核不通过21-区县审核通过22-区县审核不通过
	 */
	private static final String SHZT = "01";
	/**
	 * 审核结果0-审核中 1-通过 2-不通过 
	 */
	private static final String SHJG = "0";
	/**
	 * 是否参加基层服务项目   默认为【0-否】
	 *	1-农村义务教育阶段学校教师特设岗位计划
	 *	101-中央特岗教师
	 *	102-地方特岗教师
	 *	103-新疆双语特岗
	 *	2-大学生村官
	 *	3-高校毕业生三支一扶计划
	 *	4-大学生志愿服务西部计划 0-否
	 */
	private static final String SFSTGJS = "0";

	/**
	 * 在岗情况（人员状态）（在职）
	 */
	private static final String ZGQK = "100";

	///基础方法
	///////////////////////////////////////////////////
	@Override
	public String getZdxName(String zdbs, String zdxbm) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("zdbs", zdbs);
		param.put("zdxbm", zdxbm);
		try {
			List<String> rlist = this.findList("ExportInstitutionsManageExt.getZdxName.query", param, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					return rs.getString("name");
				}
			});
			return rlist.size() > 0 ? rlist.get(0) : "";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String getZDXBM(String zdbs, String name) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("zdbs", zdbs);
		param.put("name", name);
		try {
			List<String> rlist = this.findList("ExportInstitutionsManageExt.getZDXBM.query", param, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					return rs.getString("zdxbm");
				}
			});
			return rlist.size() > 0 ? rlist.get(0) : "";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String getQXDM(String name) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("name", name);
		try {
			List<String> rlist = this.findList("ExportInstitutionsManageExt.getQXDM.query", param, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					return rs.getString("qxdm");
				}
			});
			return rlist.size() > 0 ? rlist.get(0) : "";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取机构名称
	 * @param jgid
	 * @return
	 */
	public String getJgname(String jgid) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("jgId", jgid);
		try {
			List<String> rlist = this.findList("ExportInstitutionsManageExt.getJgname.query", param, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					return rs.getString("name");
				}
			});
			return rlist.size() > 0 ? rlist.get(0) : "";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	///////////////////根据身份证号和姓名判断教师是否已经存在
	public boolean isExistTea(String cardNo,String name) throws Exception {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("name", name);
		param.put("cardNo", cardNo);
		
		List<Integer> dtos = this.findList("ExportExt.isExistTea.query", param,
				new RowMapper() {
					public Object mapRow(ResultSet rs, int arg1) throws SQLException {
						return rs.getInt(1);
					}
				});
		if(dtos != null && dtos.size() > 0){
			return dtos.get(0) > 0;
		}else{
			return false;
		}
	}
	
	@Override
	public String getTeacherInfo(String cardNo,String name) throws Exception {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("name", name);
		param.put("cardNo", cardNo);
		
		List<String> dtos = this.findList("ExportExt.getTeacherInfo.query", param,
				new RowMapper() {
					public Object mapRow(ResultSet rs, int arg1) throws SQLException {
						return rs.getString(1);
					}
				});
		if(dtos != null && dtos.size() > 0){
			return dtos.get(0);
		}else{
			return null;
		}
	}
	
	public InstitutionsInfoDto getInstitution(String jgId) {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jgId", jgId);
			List<InstitutionsInfoDto> dtos = this.findList("ExportExt.getInstitution.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							InstitutionsInfoDto dto = new InstitutionsInfoDto();
							dto.setInstitutionId(Integer.valueOf(rs.getString("institutionId")));
							dto.setInstitutionCode(rs.getString("institutionCode"));
							dto.setInstitutionName(rs.getString("institutionName"));
							dto.setInstitutionAddress(rs.getString("institutionAddress"));
							dto.setContactPerson(rs.getString("contactPerson"));
							dto.setContactTel(rs.getString("contactTel"));
							dto.setEmail(rs.getString("email"));
							dto.setDistrictCode(rs.getString("districtCode"));
							dto.setJbdw(rs.getString("jbdw"));
							dto.setJbzlx(rs.getString("jbzlx"));
							dto.setResidentType(rs.getString("residentType"));
							dto.setSchoolType(rs.getString("schoolType"));
							dto.setAffiliatedSchool(rs.getString("affiliatedSchool"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("InstitutionsManageExt getInstitution() is error!", e);
		}
		return null;
	}
	
	///导入导出教师帐号信息方法
	///////////////////////////////////////////////////
	public List<List<Object>> getTeacher(Map<String, Object> param) {
		try {
			List<Map<String, Object>> dtos = this.findList("ExportInstitutionsManageExt.getTeacher.query", param,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							Map<String, Object> dto = new HashMap<String, Object>();
							try {
								dto.put("jsId", rs.getString("jsId"));
								dto.put("eduId", rs.getString("eduId"));
								dto.put("name", rs.getString("name"));
								dto.put("cardType", rs.getString("cardType"));
								dto.put("cardNo", rs.getString("cardNo"));
//								dto.put("jgId", rs.getString("jgId"));
//								dto.put("sex", rs.getString("sex"));
//								dto.put("birthday", rs.getString("birthday"));
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			List<List<Object>> result = new ArrayList<List<Object>>();
			for (int i = 0; i < dtos.size(); i++) {
				List<Object> clist = new ArrayList<Object>();
				Map<String, Object> map = dtos.get(i);
				clist.add(map.get("jsId"));
				clist.add(map.get("eduId"));
				clist.add(map.get("name"));
				clist.add(map.get("cardType").toString() + "-"+ getZdxName("JSXX_SFZJLX", map.get("cardType").toString()));
				clist.add(map.get("cardNo"));
				
//				clist.add(map.get("jgId"));
//				clist.add(getJgname(map.get("jgId").toString()));
//				clist.add(map.get("sex").toString() + "-" + getZdxName("JSXX_XB", map.get("sex").toString()));
//				clist.add(map.get("birthday"));//出生日期格式待处理
				result.add(clist);
			}

			return result;
		} catch (Exception e) {
			logger.error("TeacherAccountManageExt doSearch() is error!", e);
		}
		return null;
	}

	/**
	 * 导入教师帐号信息
	 */
	@Override
	public void addTeacher(final Map<String, Object> teacherInfo, final InstitutionsInfoDto institution) {
		StringBuffer sql = new StringBuffer();
		sql.append(
				"insert into TB_BIZ_JZGJBXX(JSID,XXJGID,EDU_ID,SFZZ,SFSFLZYBY,SFSGTJZYPYPX,SFYTSJYCYZS,XXJSYYNL,SFSYMFSFS,SFSTGJS,BSRLB,SHZT,SHSJ,SHJG,SFSC,CJSJ,XM,XB,SFZJLX,SFZJH,CSRQ,ZGQK) ")
				.append("values (SEQ_TB_BIZ_JZGJBXX.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				if (teacherInfo.get("jgId") != null && !"".equals(teacherInfo.get("jgId").toString())) {
					ps.setLong(1, Long.parseLong(teacherInfo.get("jgId").toString()));//机构Id
				} else {
					ps.setLong(1, institution.getInstitutionId());//机构Id
				}

				ps.setString(2, getEduId(institution.getAffiliatedSchool(), institution.getDistrictCode()));//教育Id需要重写
				ps.setString(3, SFZZ);
				ps.setString(4, SFSFLZYBY);
				ps.setString(5, SFSGTJZYPYPX);
				ps.setString(6, SFYTSJYCYZS);
				ps.setString(7, XXJSYYNL);
				ps.setString(8, SFSYMFSFS);
				ps.setString(9, SFSTGJS);
				ps.setString(10, BSRLB);
				ps.setString(11, SHZT);
				ps.setTimestamp(12, new Timestamp(System.currentTimeMillis()));
				ps.setString(13, SHJG);
				ps.setString(14, NOSFSC);
				ps.setTimestamp(15, new Timestamp(System.currentTimeMillis()));
				ps.setString(16, teacherInfo.get("name").toString());
				ps.setString(17, "0");
				ps.setString(18, teacherInfo.get("cardType").toString());
				ps.setString(19, teacherInfo.get("cardNo").toString());
				ps.setString(20, teacherInfo.get("birth").toString());
				ps.setString(21, ZGQK);
			}
		});
	}

	public String getEduId(String affiliatedSchool, String disCode) {
		//直接判断教师所在机构是哪个区或者市，是哪个就走哪个不用判断是哪个学段
		String eduId = "";
		if ("110000000000".equals(disCode)) {
			//市级
			eduId = "900";
			String s = getSequenceSJ(disCode);
			for (int i = 0; i < 5 - s.length(); i++) {
				eduId += "0";
			}
			eduId += getSequenceSJ(disCode);
		} else {
			//区级
			eduId = "9" + disCode.substring(4, 6);
			String s = getSequenceSJ(disCode);
			for (int i = 0; i < 5 - s.length(); i++) {
				eduId += "0";
			}
			eduId += getSequenceSJ(disCode);
		}
		return eduId;
	}

	/**
	 * 获取序列
	 * <p>
	 * 区级和区级
	 */
	public String getSequenceSJ(String disCode) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("110101000000", "TeacherAccountManageExt.getSequenceDC.query");//东城
		map.put("110102000000", "TeacherAccountManageExt.getSequenceXC.query");//西城
		map.put("110105000000", "TeacherAccountManageExt.getSequenceCY.query");//朝阳
		map.put("110108000000", "TeacherAccountManageExt.getSequenceHD.query");//海淀
		map.put("110106000000", "TeacherAccountManageExt.getSequenceFT.query");//丰台
		map.put("110107000000", "TeacherAccountManageExt.getSequenceSJS.query");//石景山
		map.put("110112000000", "TeacherAccountManageExt.getSequenceTZ.query");//通州
		map.put("110115000000", "TeacherAccountManageExt.getSequenceDX.query");//大兴
		map.put("110111000000", "TeacherAccountManageExt.getSequenceFS.query");//房山
		map.put("110113000000", "TeacherAccountManageExt.getSequenceSY.query");//顺义
		map.put("110228000000", "TeacherAccountManageExt.getSequenceMY.query");//密云
		map.put("110116000000", "TeacherAccountManageExt.getSequenceHR.query");//怀柔
		map.put("110229000000", "TeacherAccountManageExt.getSequenceYQ.query");//延庆
		map.put("110117000000", "TeacherAccountManageExt.getSequencePG.query");//平谷
		map.put("11A1A1000000", "TeacherAccountManageExt.getSequenceYS.query");//燕山
		map.put("110114000000", "TeacherAccountManageExt.getSequenceCP.query");//昌平
		map.put("110109000000", "TeacherAccountManageExt.getSequenceMTG.query");//门头沟
		map.put("110000000000", "InstitutionsManageExt.getSequenceSHIJI.query");//市级
		for (String key : map.keySet()) {
			if (key.equals(disCode)) {
				Map<String, Object> params = new HashMap<String, Object>();
				try {
					List<InstitutionsInfoDto> list = this.findList(map.get(key), params, new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							InstitutionsInfoDto dto = new InstitutionsInfoDto();
							dto.setInstitutionName(rs.getString("institutionName"));
							return dto;
						}
					});
					return list.get(0).getInstitutionName();
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			}
		}
		return null;
	}

	//机构导入导出代码
	////////////////////////////////////////////////////////////////////////////////
	@Override
	public List<InstitutionsInfoDto> getInstitutionsList(Map<String, Object> param) {
		try {
			param.put("sfzy", SFZY);//是否在用
			List<InstitutionsInfoDto> dtos = this.findList("ExportInstitutionsManageExt.export.query", param,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							InstitutionsInfoDto dto = new InstitutionsInfoDto();
							try {
								dto.setInstitutionId(Integer.valueOf(rs.getString("institutionId")));
								dto.setInstitutionCode(rs.getString("institutionCode"));
								dto.setInstitutionName(rs.getString("institutionName"));
								dto.setInstitutionAddress(rs.getString("institutionAddress"));
								dto.setContactPerson(rs.getString("contactPerson"));
								dto.setContactTel(rs.getString("contactTel"));
								dto.setEmail(rs.getString("email"));
								dto.setDistrictCode(rs.getString("districtCode"));
								dto.setJbdw(rs.getString("jbdw"));
								dto.setJbzlx(rs.getString("jbzlx"));
								dto.setResidentType(rs.getString("residentType"));
								dto.setSchoolType(rs.getString("schoolType"));
								dto.setAffiliatedSchool(rs.getString("affiliatedSchool"));
								dto.setUnitType(rs.getString("unitType"));
								dto.setDisName(rs.getString("disName"));
								dto.setUnitCode(rs.getString("unitCode"));
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			return dtos;
		} catch (Exception e) {
			logger.error("InstitutionsManageExt doSearch() is error!", e);
		}
		return null;
	}

	/**
	 * 导入机构信息
	 */
	@Override
	public void add(final InstitutionsInfoDto institutionDto, final String userType) {
		StringBuffer sql = new StringBuffer();
		sql.append(
				"insert into TB_XXJGB(XXJGID,XXJGDM,XXJGMC,QXDM,DZ,BXLX,ZDCXLX,JBDW,JBZLX,LXR,LXDH,EMAIL,CJSJ,SFZY,SSXD,ZXXFL,DWZZDM)")
				.append("values (SEQ_TB_XXJGB.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				if (institutionDto.getInstitutionCode() != null && !"".equals(institutionDto.getInstitutionCode())) {
					ps.setString(1, institutionDto.getInstitutionCode());//机构代码
				} else {
					ps.setString(1, generateInstitutionCode(institutionDto, userType));//机构代码
				}
				ps.setString(2, institutionDto.getInstitutionName());
				ps.setString(3, institutionDto.getDistrictCode());
				ps.setString(4, institutionDto.getInstitutionAddress());
				ps.setString(5, institutionDto.getSchoolType());
				ps.setString(6, institutionDto.getResidentType());
				ps.setString(7, institutionDto.getJbdw());
				ps.setString(8, institutionDto.getJbzlx());
				ps.setString(9, institutionDto.getContactPerson());
				ps.setString(10, institutionDto.getContactTel());
				ps.setString(11, institutionDto.getEmail());
				ps.setTimestamp(12, new Timestamp(System.currentTimeMillis()));
				ps.setString(13, SFZY);
				ps.setString(14, institutionDto.getAffiliatedSchool());
				ps.setString(15, institutionDto.getUnitType());
				ps.setString(16, institutionDto.getUnitCode());
			}
		});

		//生成机构管理员	新增机构时，系统自动创建机构用户，用户名为机构代码，并将用户的角色自动设置为机构管理员，密码默认为123456.
		//sysUserManagerExt.add(String institutionCode,String);
	}

	//////////////////////////////////////////
	private String generateInstitutionCode(final InstitutionsInfoDto institutionDto, String userType) {
		String institutionCode = "";
		if ("10".equals(institutionDto.getAffiliatedSchool())) {
			//本科院校
			institutionCode = "0100" + "D";
			String s = getSequenceBK();
			for (int i = 0; i < 5 - s.length(); i++) {
				institutionCode += "0";
			}
			institutionCode += getSequenceBK();
		}
		if ("20".equals(institutionDto.getAffiliatedSchool())) {
			//高等职业院校
			institutionCode = "0100" + "C";
			String s = getSequenceGZ();
			for (int i = 0; i < 5 - s.length(); i++) {
				institutionCode += "0";
			}
			institutionCode += getSequenceGZ();
		}
		if ("30".equals(institutionDto.getAffiliatedSchool())) {
			//中小学
			/*if("1".equals(institutionDto.getUnitType())){
				//中小学
				 institutionCode=institutionDto.getDistrictCode()+;
			}*/
			if ("2".equals(institutionDto.getUnitType())) {
				//直属机构
				if ("1".equals(userType)) {//市级
					institutionCode = "0100" + "9";
					String s = getSequenceZS();
					for (int i = 0; i < 5 - s.length(); i++) {
						institutionCode += "0";
					}
					institutionCode += getSequenceZS();
				} else if ("2".equals(userType)) {//区级
					institutionCode = institutionDto.getDistrictCode().substring(2, 6) + "9";
					String s = getSequenceZS();
					for (int i = 0; i < 5 - s.length(); i++) {
						institutionCode += "0";
					}
					institutionCode += getSequenceZS();
				}
			}
			if ("3".equals(institutionDto.getUnitType())) {
				//研修机构
				if ("1".equals(userType)) {//市级
					institutionCode = "0100" + "E";
					String s = getSequenceYX();
					for (int i = 0; i < 5 - s.length(); i++) {
						institutionCode += "0";
					}
					institutionCode += getSequenceYX();
				} else if ("2".equals(userType)) {//区级
					institutionCode = institutionDto.getDistrictCode().substring(2, 6) + "E";
					String s = getSequenceYX();
					for (int i = 0; i < 5 - s.length(); i++) {
						institutionCode += "0";
					}
					institutionCode += getSequenceYX();
				}
			}
			if ("4".equals(institutionDto.getUnitType())) {
				//校外机构
				if ("1".equals(userType)) {//市级
					institutionCode = "0100" + "F";
					String s = getSequenceXW();
					for (int i = 0; i < 5 - s.length(); i++) {
						institutionCode += "0";
					}
					institutionCode += getSequenceXW();
				} else if ("2".equals(userType)) {//区级
					institutionCode = institutionDto.getDistrictCode().substring(2, 6) + "F";
					String s = getSequenceXW();
					for (int i = 0; i < 5 - s.length(); i++) {
						institutionCode += "0";
					}
					institutionCode += getSequenceXW();
				}
			}
		}
		if ("40".equals(institutionDto.getAffiliatedSchool())) {
			//中等职业学校
			institutionCode = institutionDto.getDistrictCode().substring(2, 6) + "B";
			String s = getSequenceZD();
			for (int i = 0; i < 5 - s.length(); i++) {
				institutionCode += "0";
			}
			institutionCode += getSequenceZD();
		}
		/*if ("50".equals(institutionDto.getAffiliatedSchool())) {
			//特殊教育学校
			institutionCode=institutionDto.getDistrictCode()+;
		}*/
		if ("60".equals(institutionDto.getAffiliatedSchool())) {
			//幼儿园
			institutionCode = institutionDto.getDistrictCode().substring(2, 6) + "A";
			String s = getSequenceYEY();
			for (int i = 0; i < 5 - s.length(); i++) {
				institutionCode += "0";
			}
			institutionCode += getSequenceYEY();
		}
		return institutionCode;
	}

	/**
	 * 获取序列
	 * <p>
	 *获取本科序列
	 */
	public String getSequenceBK() {
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			List<InstitutionsInfoDto> list = this.findList("InstitutionsManageExt.getSequenceBK.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							InstitutionsInfoDto dto = new InstitutionsInfoDto();
							dto.setInstitutionName(rs.getString("institutionName"));
							return dto;
						}
					});
			return list.get(0).getInstitutionName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取序列
	 * <p>
	 *获取幼儿园序列
	 *
	 */
	public String getSequenceYEY() {
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			List<InstitutionsInfoDto> list = this.findList("InstitutionsManageExt.getSequenceYEY.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							InstitutionsInfoDto dto = new InstitutionsInfoDto();
							dto.setInstitutionName(rs.getString("institutionName"));
							return dto;
						}
					});
			return list.get(0).getInstitutionName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * 获取序列
	 * <p>
	 *中等职业学校
	 */
	public String getSequenceZD() {
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			List<InstitutionsInfoDto> list = this.findList("InstitutionsManageExt.getSequenceZD.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							InstitutionsInfoDto dto = new InstitutionsInfoDto();
							dto.setInstitutionName(rs.getString("institutionName"));
							return dto;
						}
					});
			return list.get(0).getInstitutionName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取序列
	 * <p>
	 *高等职业学校
	 */
	public String getSequenceGZ() {
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			List<InstitutionsInfoDto> list = this.findList("InstitutionsManageExt.getSequenceGZ.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							InstitutionsInfoDto dto = new InstitutionsInfoDto();
							dto.setInstitutionName(rs.getString("institutionName"));
							return dto;
						}
					});
			return list.get(0).getInstitutionName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取序列
	 * <p>
	 *研修机构
	 */
	public String getSequenceYX() {
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			List<InstitutionsInfoDto> list = this.findList("InstitutionsManageExt.getSequenceYX.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							InstitutionsInfoDto dto = new InstitutionsInfoDto();
							dto.setInstitutionName(rs.getString("institutionName"));
							return dto;
						}
					});
			return list.get(0).getInstitutionName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取序列
	 * <p>
	 *校外机构
	 */
	public String getSequenceXW() {
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			List<InstitutionsInfoDto> list = this.findList("InstitutionsManageExt.getSequenceXW.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							InstitutionsInfoDto dto = new InstitutionsInfoDto();
							dto.setInstitutionName(rs.getString("institutionName"));
							return dto;
						}
					});
			return list.get(0).getInstitutionName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取序列
	 * <p>
	 *直属机构
	 */
	public String getSequenceZS() {
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			List<InstitutionsInfoDto> list = this.findList("InstitutionsManageExt.getSequenceZS.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							InstitutionsInfoDto dto = new InstitutionsInfoDto();
							dto.setInstitutionName(rs.getString("institutionName"));
							return dto;
						}
					});
			return list.get(0).getInstitutionName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
