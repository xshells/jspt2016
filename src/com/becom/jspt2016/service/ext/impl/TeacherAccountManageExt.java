/**
 * TeacherAccountManageExt.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.commons.hibernate.IPage;
import org.nestframework.commons.hibernate.IRowHandler;
import org.nestframework.commons.hibernate.ISqlElement;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.InstitutionsInfoDto;
import com.becom.jspt2016.dto.TeacherAccountInfoDto;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.service.ITbBizJzgjbxxManager;
import com.becom.jspt2016.service.ITbXxjgbManager;
import com.becom.jspt2016.service.ext.ITeacherAccountManageExt;

/**
 *教师账号管理业务层
 * <p>
 * @author   kuangxiang
 * @author   zhangchunming
 * @Date	 2016年10月17日
 * @Date	 2016年11月1日 
 */

public class TeacherAccountManageExt extends JdbcRootManager implements ITeacherAccountManageExt {
	protected Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 删除状态 0：否，1：是
	 */
	private static final String SFSC = "1";
	/**
	 * 删除状态 0：否，
	 */
	private static final String NOSFSC = "0";
	
	/**
	 * 删除状态2：待调出
	 */
	private static final String RSFSCR= "2";
	/**
	 * 是否在职，统一为:1
	 */
	private static final String SFZZ = "1";
	/**
	 * 是否全日制师范类专业毕业   默认为0-否，1-是
	 */
	private static final String SFSFLZYBY = "0";
	/**
	 * 是否受过特教专业培养培训  默认为【0-否】0-否1-是
	 */
	private static final String SFSGTJZYPYPX = "0";
	/**
	 * 是否有特殊教育从业证书  	默认为【0-否】0-否1-是
	 */
	private static final String SFYTSJYCYZS = "0";
	/**
	 * 信息技术应用能力    1-精通2-熟练3-良好4-一般5-较弱
	 */
	private static final String XXJSYYNL = "1";

	/**
	 * 是否属于免费（公费）师范生     默认为【0-否】1-部属师大免费师范生 2-地方免费（公费）师范生 0-否
	 */
	private static final String SFSYMFSFS = "0";
	/**
	 * 报送人类别3-校级  4-教师 
	 */
	private static final String BSRLB = "3";
	/**
	 * 审核状态     01-未报送 02-待审核11-学校（机构）审核通过12-学校（机构）审核不通过21-区县审核通过22-区县审核不通过
	 */
	private static final String SHZT = "01";
	/**
	 * 审核结果0-审核中 1-通过 2-不通过 
	 */
	private static final String SHJG = "0";

	/**
	 * 在岗情况（人员状态）（在职）
	 */
	private static final String ZGQK = "100";
	/**
	 * 是否参加基层服务项目   默认为【0-否】
	 *	1-农村义务教育阶段学校教师特设岗位计划
	 *	101-中央特岗教师
	 *	102-地方特岗教师
	 *	103-新疆双语特岗
	 *	2-大学生村官
	 *	3-高校毕业生三支一扶计划
	 *	4-大学生志愿服务西部计划 0-否
	 */
	private static final String SFSTGJS = "0";
	/**
	 * 学校机构service
	 */
	@Spring
	private ITbXxjgbManager tbXxjgbManager;
	/**
	 * 教师基本信息service
	 */
	@Spring
	private ITbBizJzgjbxxManager tbBizJzgjbxxManager;

	/**
	 * 删除教师账号
	 */
	@Override
	public int delete(String[] ids) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ids", ids);
		params.put("sfsc", SFSC);
		try {
			ISqlElement processSql = processSql(params, "TeacherAccountManageExt.delete.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherAccountManageExt delete() is error!", e);
		}
		return result;
	}

	/**
	 * 删除教师账号
	 */
	@Override
	public int updateCallOut(String[] ids) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ids", ids);
		params.put("sfsc", RSFSCR);
		try {
			ISqlElement processSql = processSql(params, "TeacherAccountManageExt.delete.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherAccountManageExt delete() is error!", e);
		}
		return result;
	}
	
	/**
	 * 找回账号
	 */
	@Override
	public int retrieveAccount(String[] ids) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ids", ids);
		params.put("sfsc", NOSFSC);
		try {
			ISqlElement processSql = processSql(params, "TeacherAccountManageExt.delete.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherAccountManageExt retrieveAccount() is error!", e);
		}
		return result;
	}

	/**
	 * 教师账号管理列表
	 * <p>
	 *
	 * @param param查询参数
	 * @param pageNo 页码
	 * @param pageSize 每页显示个数
	*/
	@Override
	public IPage doSearch(Map<String, Object> param, int pageNo, int pageSize) {
		try {
			IPage<TeacherAccountInfoDto> dtos = this.findPage("TeacherAccountManageExt.doSearch.query",
					"TeacherAccountManageExt.doSearch.count", param, pageNo, pageSize,
					new IRowHandler<TeacherAccountInfoDto>() {
						public TeacherAccountInfoDto handleRow(ResultSet rs) {
							TeacherAccountInfoDto dto = new TeacherAccountInfoDto();
							try {
								dto.setJsId(Integer.valueOf(rs.getString("jsId")));
								dto.setEducationId(rs.getString("educationId"));
								dto.setTeacherName(rs.getString("teacherName"));
								dto.setCardNo(rs.getString("cardNo"));
								dto.setBirthday(rs.getString("birthday"));
								dto.setAccountStatus(rs.getString("accountStatus"));
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			return dtos;
		} catch (Exception e) {
			logger.error("TeacherAccountManageExt doSearch() is error!", e);
		}
		return null;
	}

	/**
	 * 教师账号管理列表
	 * <p>
	 *
	 * @param param查询参数
	 * @param pageNo 页码
	 * @param pageSize 每页显示个数
	*/
	@Override
	public IPage findTeacherInfo(Map<String, Object> param, int pageNo, int pageSize) {
		try {
			IPage<TeacherAccountInfoDto> dtos = this.findPage("TeacherAccountManageExt.findTeacherInfo.query",
					"TeacherAccountManageExt.findTeacherInfo.count", param, pageNo, pageSize,
					new IRowHandler<TeacherAccountInfoDto>() {
						public TeacherAccountInfoDto handleRow(ResultSet rs) {
							TeacherAccountInfoDto dto = new TeacherAccountInfoDto();
							try {
								dto.setJsId(Integer.valueOf(rs.getString("jsId")));
								dto.setEducationId(rs.getString("educationId"));
								dto.setTeacherName(rs.getString("teacherName"));
								dto.setCardNo(rs.getString("cardNo"));
								dto.setBirthday(rs.getString("birthday"));
								dto.setAccountStatus(rs.getString("accountStatus"));
								dto.setSex(rs.getString("sex"));
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			return dtos;
		} catch (Exception e) {
			logger.error("TeacherAccountManageExt findTeacherInfo() is error!", e);
		}
		return null;
	}
	
	/**
	 * 添加教师账号
	 */
	@Override
	public void add(final TbBizJzgjbxx teacherInfo, final InstitutionsInfoDto institution) {
		StringBuffer sql = new StringBuffer();
		sql.append(
				"insert into TB_BIZ_JZGJBXX(JSID,XXJGID,EDU_ID,SFZZ,SFSFLZYBY,SFSGTJZYPYPX,SFYTSJYCYZS,XXJSYYNL,SFSYMFSFS,SFSTGJS,BSRLB,SHZT,SHSJ,SHJG,SFSC,CJSJ,XM,XB,SFZJLX,SFZJH,CSRQ,ZGQK) ")
				.append("values (SEQ_TB_BIZ_JZGJBXX.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, institution.getInstitutionId());//机构Id
				ps.setString(2, getEduId(institution.getAffiliatedSchool(), institution.getDistrictCode()));//教育Id需要重写
				ps.setString(3, SFZZ);
				ps.setString(4, SFSFLZYBY);
				ps.setString(5, SFSGTJZYPYPX);
				ps.setString(6, SFYTSJYCYZS);
				ps.setString(7, XXJSYYNL);
				ps.setString(8, SFSYMFSFS);
				ps.setString(9, SFSTGJS);
				ps.setString(10, BSRLB);
				ps.setString(11, SHZT);
				ps.setTimestamp(12, new Timestamp(System.currentTimeMillis()));
				ps.setString(13, SHJG);
				ps.setString(14, NOSFSC);
				ps.setTimestamp(15, new Timestamp(System.currentTimeMillis()));
				ps.setString(16, teacherInfo.getXm().toString());
				ps.setString(17, teacherInfo.getXb());
				ps.setString(18, teacherInfo.getSfzjlx());
				ps.setString(19, teacherInfo.getSfzjh());
				ps.setString(20, teacherInfo.getCsrq());
				ps.setString(21, ZGQK);
			}
		});
	}

	/**
	 * 获取教育Id
	 * @param  affiliatedSchool 所属学段
	 * @param disCode 区市代码也就是所在地
	 */
	public String getEduId(String affiliatedSchool, String disCode) {
		//直接判断教师所在机构是哪个区或者市，是哪个就走哪个不用判断是哪个学段
		String eduId = "";
		if ("110000000000".equals(disCode)) {
			//市级
			eduId = "900";
			String s = getSequenceSJ(disCode);
			for (int i = 0; i < 5 - s.length(); i++) {
				eduId += "0";
			}
			eduId += getSequenceSJ(disCode);
		} else {
			//区级
			eduId = "9" + disCode.substring(4, 6);
			String s = getSequenceSJ(disCode);
			for (int i = 0; i < 5 - s.length(); i++) {
				eduId += "0";
			}
			eduId += getSequenceSJ(disCode);
		}
		return eduId;
	}

	/**
	 * 获取序列
	 * <p>
	 * 区级和区级
	 */
	public String getSequenceSJ(String disCode) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("110101000000", "TeacherAccountManageExt.getSequenceDC.query");//东城
		map.put("110102000000", "TeacherAccountManageExt.getSequenceXC.query");//西城
		map.put("110105000000", "TeacherAccountManageExt.getSequenceCY.query");//朝阳
		map.put("110108000000", "TeacherAccountManageExt.getSequenceHD.query");//海淀
		map.put("110106000000", "TeacherAccountManageExt.getSequenceFT.query");//丰台
		map.put("110107000000", "TeacherAccountManageExt.getSequenceSJS.query");//石景山
		map.put("110112000000", "TeacherAccountManageExt.getSequenceTZ.query");//通州
		map.put("110115000000", "TeacherAccountManageExt.getSequenceDX.query");//大兴
		map.put("110111000000", "TeacherAccountManageExt.getSequenceFS.query");//房山
		map.put("110113000000", "TeacherAccountManageExt.getSequenceSY.query");//顺义
		map.put("110228000000", "TeacherAccountManageExt.getSequenceMY.query");//密云
		map.put("110116000000", "TeacherAccountManageExt.getSequenceHR.query");//怀柔
		map.put("110229000000", "TeacherAccountManageExt.getSequenceYQ.query");//延庆
		map.put("110117000000", "TeacherAccountManageExt.getSequencePG.query");//平谷
		map.put("11A1A1000000", "TeacherAccountManageExt.getSequenceYS.query");//燕山
		map.put("110114000000", "TeacherAccountManageExt.getSequenceCP.query");//昌平
		map.put("110109000000", "TeacherAccountManageExt.getSequenceMTG.query");//门头沟
		map.put("110000000000", "InstitutionsManageExt.getSequenceSHIJI.query");//市级
		for (String key : map.keySet()) {
			if (key.equals(disCode)) {
				Map<String, Object> params = new HashMap<String, Object>();
				try {
					List<InstitutionsInfoDto> list = this.findList(map.get(key), params, new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							InstitutionsInfoDto dto = new InstitutionsInfoDto();
							dto.setInstitutionName(rs.getString("institutionName"));
							return dto;
						}
					});
					return list.get(0).getInstitutionName();
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			}
		}
		return null;
	}

	/**
	 * 编辑
	 */
	@Override
	public int edit(TbBizJzgjbxx teacherInfo, String editName) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jsId", teacherInfo.getJsid());
		params.put("sfzjlx", teacherInfo.getSfzjlx());
		params.put("sfzjh", teacherInfo.getSfzjh());
		params.put("csrq", teacherInfo.getCsrq());
		params.put("xm", teacherInfo.getXm());
		params.put("xb", teacherInfo.getXb());
		params.put("editName", editName);
		params.put("editTime", new Timestamp(System.currentTimeMillis()));
		try {
			ISqlElement processSql = processSql(params, "TeacherAccountManageExt.edit.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherAccountManageExt edit() is error!", e);
		}
		return result;

	}

	/**
	 * 获取教师账号信息通过教职工Id
	 */
	@Override
	public TeacherAccountInfoDto get(long jsId) {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsId", jsId);
			List<TeacherAccountInfoDto> dtos = this.findList("TeacherAccountManageExt.get.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TeacherAccountInfoDto dto = new TeacherAccountInfoDto();
							dto.setJsId(rs.getInt("jsId"));
							dto.setTeacherName(rs.getString("name"));
							dto.setSex(rs.getString("sex"));
							dto.setBirthday(rs.getString("birthday"));
							dto.setShenfen(rs.getString("shenfen"));
							dto.setCardNo(rs.getString("cardNo"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("TeacherAccountManageExt get() is error!", e);
		}

		return null;

	}

	public ITbXxjgbManager getTbXxjgbManager() {
		return tbXxjgbManager;
	}

	public void setTbXxjgbManager(ITbXxjgbManager tbXxjgbManager) {
		this.tbXxjgbManager = tbXxjgbManager;
	}

	public ITbBizJzgjbxxManager getTbBizJzgjbxxManager() {
		return tbBizJzgjbxxManager;
	}

	public void setTbBizJzgjbxxManager(ITbBizJzgjbxxManager tbBizJzgjbxxManager) {
		this.tbBizJzgjbxxManager = tbBizJzgjbxxManager;
	}

	/**
	 * 校验添加的证件号码
	 */
	@Override
	public TeacherAccountInfoDto checkAddHm(String shenfen, String tCNo) {

		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("shenfen", shenfen);
			params.put("tCNo", tCNo);
			params.put("sfsc", NOSFSC);
			List<TeacherAccountInfoDto> dtos = this.findList("TeacherAccountManageExt.checkAddHm.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TeacherAccountInfoDto dto = new TeacherAccountInfoDto();
							dto.setJsId(rs.getInt("jsId"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("TeacherAccountManageExt checkAddHm() is error!", e);
		}

		return null;
	}

	/**
	 * 校验编辑的证件号码
	 */
	@Override
	public TeacherAccountInfoDto checkEditHm(String shenfen, String tCNo, long jsId) {

		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsId", jsId);
			params.put("shenfen", shenfen);
			params.put("tCNo", tCNo);
			params.put("sfsc", NOSFSC);
			List<TeacherAccountInfoDto> dtos = this.findList("TeacherAccountManageExt.checkEditHm.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TeacherAccountInfoDto dto = new TeacherAccountInfoDto();
							dto.setJsId(rs.getInt("jsId"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("TeacherAccountManageExt checkEditHm() is error!", e);
		}

		return null;

	}
}
