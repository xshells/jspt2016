/**
 * ProtTechAppoinManagerExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.service.ext.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.commons.hibernate.IPage;
import org.nestframework.commons.hibernate.IRowHandler;
import org.nestframework.commons.hibernate.ISqlElement;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.TbCfgZdxbInfoDto;
import com.becom.jspt2016.model.TbBizJzggwprxx;
import com.becom.jspt2016.model.TbBizJzgzyjszwprxx;
import com.becom.jspt2016.service.ext.IProtTechAppoinManagerExt;

/**
 * TODO(岗位聘任专业技术---ext实现类)
 * @author   董锡福
 * @Date	 2016年10月20日 	 
 */
public class ProtTechAppoinManagerExt extends JdbcRootManager implements IProtTechAppoinManagerExt {

	protected Logger logger = Logger.getLogger(this.getClass());

	public IPage<TbBizJzgzyjszwprxx> doSearch(Map<String, Object> params, int pageNo, int pageSize) {
		try {
			IPage<TbBizJzgzyjszwprxx> dtos = this.findPage("ProtTechAppoinManagerExt.findZYJSZ.query",
					"ProtTechAppoinManagerExt.findZYJSZ.count", params, pageNo, pageSize,
					new IRowHandler<TbBizJzgzyjszwprxx>() {
						public TbBizJzgzyjszwprxx handleRow(ResultSet rs) {
							TbBizJzgzyjszwprxx dto = new TbBizJzgzyjszwprxx();
							try {
								dto.setZyjszwId(rs.getLong("ZYJSZW_ID"));
								dto.setPrdwmc(rs.getString("prdwmc"));
								dto.setPrzyjszw(rs.getString("przyjszw"));
								dto.setPrksny(rs.getString("prksny"));
								dto.setPrjsny(rs.getString("prjsny"));
								dto.setCjsj(rs.getDate("cjsj"));
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			if (dtos != null) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("SysTechnologyrManagerExt doSearch() is error!", e);
		}
		return null;
	}

	public void SubAddpoint(final TbBizJzgzyjszwprxx kh) {
		StringBuffer sql = new StringBuffer();
		sql.append(
				"insert into TB_BIZ_JZGZYJSZWPRXX(ZYJSZW_ID,JSID,PRZYJSZW,PRDWMC,PRKSNY,PRJSNY,BSRLB,SHZT,SHSJ,SHJG,SFSC,CJR,CJSJ)")
				.append("values (SEQ_TB_BIZ_JZGZYJSZWPRXX.nextval,?,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, kh.getTbBizJzgjbxx().getJsid());
				ps.setString(2, kh.getPrzyjszw());
				ps.setString(3, kh.getPrdwmc());
				ps.setString(4, kh.getPrksny());
				ps.setString(5, kh.getPrjsny());
				ps.setString(6, "4");
				ps.setString(7, "01");
				ps.setTimestamp(8, new java.sql.Timestamp(kh.getCjsj().getTime()));
				ps.setString(9, "");//
				ps.setString(10, "0");//是否删除
				ps.setLong(11, kh.getTbBizJzgjbxx().getJsid());//创建人
				ps.setTimestamp(12, new java.sql.Timestamp(kh.getCjsj().getTime()));//创建时间
			}
		});

	}

	public TbBizJzggwprxx editJzggwprxx(long gwprId) {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("GWPR_ID", gwprId);
			List<TbBizJzggwprxx> dtos = this.findList("JZGGWPRXXManagerExt.edit.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbBizJzggwprxx dto = new TbBizJzggwprxx();
					dto.setGwprId(rs.getLong("GWPR_ID"));
					dto.setGwlb(rs.getString("gwlb"));
					dto.setGwgj(rs.getString("gwgj"));
					dto.setPrksny(rs.getString("prksny"));
					dto.setDzzw(rs.getString("dzzw"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

	//执行修改
	public int editProtech(Long gwprId, String prksny) {
		int result = 0;
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("prksny", prksny);
			params.put("gwprId", gwprId);
			ISqlElement se = processSql(params, "JZGGWPRXXManagerExt.doUpdateAnswer.update");
			getJdbcTemplate().update(se.getSql(), se.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return result;
	}

	//删除
	public int removeProTech(String[] ck) {
		int result = 0;
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("ids", ck);
			ISqlElement se = processSql(params, "ProtTechAppoinManagerExt.doDelete.update");
			getJdbcTemplate().update(se.getSql(), se.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return result;
	}

	@Override
	public int updateProtech(TbBizJzgzyjszwprxx tbbizjzgzyjszwprxx) {
		int result = 0;
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("zyjszwId", tbbizjzgzyjszwprxx.getZyjszwId());
			params.put("prdwmc", tbbizjzgzyjszwprxx.getPrdwmc());
			params.put("przyjszw", tbbizjzgzyjszwprxx.getPrzyjszw());
			params.put("prksny", tbbizjzgzyjszwprxx.getPrksny());
			params.put("prjsny", tbbizjzgzyjszwprxx.getPrjsny());
			ISqlElement se = processSql(params, "ProtTechAppoinManagerExt.doUpdate.update");
			getJdbcTemplate().update(se.getSql(), se.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("updateProtech update() is error!", e);
		}
		return result;
	}

	@Override
	public TbBizJzgzyjszwprxx editProtech(long zyjszwId) {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("zyjszwId", zyjszwId);
			List<TbBizJzgzyjszwprxx> dtos = this.findList("prottechappoinmanagerExt.edit.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TbBizJzgzyjszwprxx dto = new TbBizJzgzyjszwprxx();
							dto.setZyjszwId((rs.getLong("ZYJSZW_ID")));
							dto.setPrdwmc(rs.getString("prdwmc"));
							dto.setPrzyjszw(rs.getString("przyjszw"));
							dto.setPrksny(rs.getString("prksny"));
							dto.setPrjsny(rs.getString("prjsny"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("prottechappoinmanagerExt doSearch() is error!", e);
		}
		return null;
	}

	//树结构
	public List<TbCfgZdxbInfoDto> searchData() {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			List<TbCfgZdxbInfoDto> dtos = this.findList("ProtTechAppoinManagerExt.searchData.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TbCfgZdxbInfoDto dto = new TbCfgZdxbInfoDto();
							dto.setFzdx(rs.getString("fzdx"));
							//							dto.setZdbs(rs.getString("zdbs"));
							dto.setZdxbm(rs.getString("zdxbm"));
							dto.setZdxmc(rs.getString("zdxmc"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

}
