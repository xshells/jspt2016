/**
 * InlandTrainManagerExt.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.commons.hibernate.IPage;
import org.nestframework.commons.hibernate.IRowHandler;
import org.nestframework.commons.hibernate.ISqlElement;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.GnpxxxInfoDto;
import com.becom.jspt2016.model.TbBizJzggnpxxx;
import com.becom.jspt2016.model.TbXxjgb;
import com.becom.jspt2016.service.ext.IInlandTrainManagerExt;

/**
 *    国内培训业务层
 * @author   wxl
 * @Date	 2016年10月17日 	 
 */
public class InlandTrainManagerExt extends JdbcRootManager implements IInlandTrainManagerExt {
	protected Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 国内培训分页列表
	 */
	public IPage<TbBizJzggnpxxx> pageSearch(Map<String, Object> params, int pageNo, int pageSize) {
		try {
			IPage<TbBizJzggnpxxx> dtos = this.findPage("InlandTrainManagerExt.pageSearch.query",
					"InlandTrainManagerExt.pageSearch.count", params, pageNo, pageSize,
					new IRowHandler<TbBizJzggnpxxx>() {
						public TbBizJzggnpxxx handleRow(ResultSet rs) {
							TbBizJzggnpxxx dto = new TbBizJzggnpxxx();
							try {
								dto.setPxnd(rs.getString("pxnd"));
								dto.setGnpxId(rs.getLong("gnpx_Id"));
								dto.setPxlb(rs.getString("pxlb"));
								dto.setPxxmmc(rs.getString("pxxmmc"));
								dto.setPxjgmc(rs.getString("pxjgmc"));
								dto.setPxfs(rs.getString("pxfs"));
								dto.setPxhdxf(rs.getString("pxhdxf"));
								dto.setPxhdxs(rs.getString("pxhdxs"));
								dto.setCjsj(rs.getDate("cjsj"));
							} catch (Exception e) {
								e.printStackTrace();
							}
							return dto;
						}
					});

			return dtos;
		} catch (Exception e) {
			logger.error("InlandTrainManagerExt pageSearch() is error!", e);
		}
		return null;
	}

	/**
	 * 更新
	 * <p>
	 * 根据国内培训id
	 * 更新国内培训信息
	 */
	@Override
	public int updateInlandTrain(TbBizJzggnpxxx tbBizJzggnpxInfo) {
		int iOk = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("gnpxId", tbBizJzggnpxInfo.getGnpxId());
		params.put("pxnd", tbBizJzggnpxInfo.getPxnd());
		params.put("pxlb", tbBizJzggnpxInfo.getPxlb());
		params.put("pxxmmc", tbBizJzggnpxInfo.getPxxmmc());
		params.put("pxjgmc", tbBizJzggnpxInfo.getPxjgmc());
		params.put("pxfs", tbBizJzggnpxInfo.getPxfs());
		params.put("pxhdxs", tbBizJzggnpxInfo.getPxhdxs());
		params.put("pxhdxf", tbBizJzggnpxInfo.getPxhdxf());
		try {
			ISqlElement se = processSql(params, "InlandTrainManagerExt.updateInlandTrain.update");
			getJdbcTemplate().update(se.getSql(), se.getParams());
		} catch (Exception e) {
			logger.error("InlandTrainManagerExt updateInlandTrain is error!", e);
			iOk = -1;
		}
		return iOk;

	}

	/**
	 * 新增国内培训信息
	 */
	@Override
	public void insertInlandTrain(final TbBizJzggnpxxx gnpxxx) {

		StringBuffer sql = new StringBuffer();
		sql.append(
				"INSERT INTO TB_BIZ_JZGGNPXXX(gnpx_id,jsid,pxnd,pxlb,pxxmmc,pxjgmc,pxfs,"
						+ "pxhdxs,pxhdxf,cjsj,sfsc,bsrlb,shzt,shsj,shjg,cjr) ").append(
				"values (SEQ_TB_BIZ_JZGGNPXXX.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, gnpxxx.getTbBizJzgjbxx().getJsid());
				ps.setString(2, gnpxxx.getPxnd());
				ps.setString(3, gnpxxx.getPxlb());
				ps.setString(4, gnpxxx.getPxxmmc());
				ps.setString(5, gnpxxx.getPxjgmc());
				ps.setString(6, gnpxxx.getPxfs());
				ps.setString(7, gnpxxx.getPxhdxs());
				ps.setString(8, gnpxxx.getPxhdxf());
				ps.setTimestamp(9, new Timestamp(System.currentTimeMillis()));
				ps.setString(10, "0");
				ps.setString(11, "4");
				ps.setString(12, "01");
				ps.setTimestamp(13, new Timestamp(System.currentTimeMillis()));
				ps.setString(14, null);
				ps.setLong(15, gnpxxx.getTbBizJzgjbxx().getJsid());
			}
		});

	}

	/**
	 * 根据国内培训id
	 * 获取国内培训信息
	 */
	@Override
	public GnpxxxInfoDto getInlandTrain(long gnpxId) {

		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("gnpxId", gnpxId);
			List<GnpxxxInfoDto> dtos = this.findList("InlandTrainManagerExt.toUpdateInlandTrain.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							GnpxxxInfoDto dto = new GnpxxxInfoDto();
							dto.setGnpxId(rs.getLong("gnpxId"));
							dto.setPxnd(rs.getString("pxnd"));
							dto.setPxlb(rs.getString("pxlb"));
							dto.setPxxmmc(rs.getString("pxxmmc"));
							dto.setPxjgmc(rs.getString("pxjgmc"));
							dto.setPxfs(rs.getString("pxfs"));
							dto.setPxhdxs(rs.getString("pxhdxs"));
							dto.setPxhdxf(rs.getString("pxhdxf"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("InlandTrainManagerExt getInlandTrain is error!", e);
		}
		return null;
	}

	/**
	 * 根据id字符串数组
	 * 删除(假删)国内培训信息
	 * 0-显示
	 * 1-已删除
	 */
	@Override
	public int delete(String[] ids) {

		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ids", ids);
		//		params.put("sfsc", '1');
		try {
			ISqlElement processSql = processSql(params, "InlandTrainManagerExt.updateDeleteInlandTrain.delete");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = -1;
			logger.error("InlandTrainManagerExt delete() is error!", e);
		}
		return result;

	}

	/**
	 * 查询角色
	 * <p>
	 * 根据session取id对应机构所属字段
	 */
	@Override
	public String getXd(Map<String, Object> params) {

		try {
			List<TbXxjgb> dtos = this.findList("InlandTrainManagerExt.query.role", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbXxjgb dto = new TbXxjgb();
					dto.setXxjgid(rs.getLong("XXJGID"));
					dto.setSsxd(rs.getString("SSXD"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0).getSsxd();
			}
		} catch (Exception e) {
			logger.error("InlandTrainManagerExt getXd is error!", e);
		}
		return null;

	}

}
