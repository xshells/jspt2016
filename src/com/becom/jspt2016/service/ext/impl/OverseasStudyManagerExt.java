/**
 * OverseasStudyManagerExt.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.commons.hibernate.IPage;
import org.nestframework.commons.hibernate.IRowHandler;
import org.nestframework.commons.hibernate.ISqlElement;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.HwyxxxInfoDto;
import com.becom.jspt2016.model.TbBizJzghwyxxx;
import com.becom.jspt2016.service.ext.IOverseasStudyManagerExt;

/**
 *   海外研修业务层
 * @author  wxl
 * @Date	 2016年10月19日 	 
 * 
 */

public class OverseasStudyManagerExt extends JdbcRootManager implements IOverseasStudyManagerExt {

	protected Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 海外研修(访学)列表
	 */
	@Override
	public IPage<TbBizJzghwyxxx> pageSearch(Map<String, Object> params, int pageNo, int pageSize) {

		try {
			IPage<TbBizJzghwyxxx> dtos = this.findPage("OverseasStudyManagerExt.pageSearch.query",
					"OverseasStudyManagerExt.pageSearch.count", params, pageNo, pageSize,
					new IRowHandler<TbBizJzghwyxxx>() {
						public TbBizJzghwyxxx handleRow(ResultSet rs) {
							TbBizJzghwyxxx dto = new TbBizJzghwyxxx();
							try {
								dto.setHwyxId(rs.getLong("hwyx_Id"));
								dto.setSfjyhwyxjl(rs.getString("sfjyhwyxjl"));
								dto.setKsrq(rs.getString("ksrq"));
								dto.setJsrq(rs.getString("jsrq"));
								dto.setGjdq(rs.getString("gjdq"));
								dto.setPxjgmc(rs.getString("pxjgmc"));
								dto.setPxxmmc(rs.getString("pxxmmc"));
								dto.setBsrlb(rs.getString("bsrlb"));
								dto.setShzt(rs.getString("shzt"));
								dto.setShsj(rs.getDate("shsj"));
								dto.setSfsc(rs.getString("sfsc"));
								dto.setCjsj(rs.getDate("cjsj"));
							} catch (Exception e) {
								logger.error("OverseasStudyManagerExt  findPage() dto is error!", e);
							}
							return dto;
						}
					});
			return dtos;
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() dtos is error!", e);
		}
		return null;
	}

	/**
	 * 添加海外研修(访学)信息
	 */
	@Override
	public void insertOverseasStudy(final TbBizJzghwyxxx hwyxxx) {

		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO TB_BIZ_JZGHWYXXX(hwyx_Id,jsid,sfjyhwyxjl,bsrlb,shzt,shsj,sfsc,cjsj,cjr) ").append(
				"values (SEQ_TB_BIZ_JZGHWYXXX.nextval,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, hwyxxx.getTbBizJzgjbxx().getJsid());//
				ps.setString(2, hwyxxx.getSfjyhwyxjl());
				ps.setString(3, "4");
				ps.setString(4, "01");
				ps.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				ps.setString(6, "0");
				ps.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
				ps.setLong(8, hwyxxx.getTbBizJzgjbxx().getJsid());
			}
		});
	}

	/**
	 * 根据id字符串数组
	 * 删除(假删)海外研修(访学)列表
	 * 0-显示
	 * 1-已删除
	 */
	@Override
	public int delete(String[] ids) {

		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ids", ids);
		try {
			ISqlElement processSql = processSql(params, "OverseasStudyManagerExt.updateDeleteOverseasStudy.delete");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = -1;
			logger.error("SysTeacherManagerExt delete() is error!", e);
		}
		return result;
	}

	/**
	 * 根据海外研修id
	 * 获取海外研修(访学)信息
	 */
	@Override
	public HwyxxxInfoDto getOverseasStudy(long hwyxId) {

		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("hwyxId", hwyxId);
			List<HwyxxxInfoDto> dtos = this.findList("OverseasStudyManagerExt.toUpdateOverseasStudy.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							HwyxxxInfoDto dto = new HwyxxxInfoDto();
							dto.setHwyxId(rs.getLong("hwyxId"));
							dto.setSfjyhwyxjl(rs.getString("sfjyhwyxjl"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt getOverseasStudy() is error!", e);
		}
		return null;
	}

	/**
	 * 根据海外研修id
	 * 更新海外研修(访学)信息
	 */
	@Override
	public int updateOverseasStudy(TbBizJzghwyxxx btBizJzghwyxInfo) {

		int iOk = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("hwyxId", btBizJzghwyxInfo.getHwyxId());
		params.put("sfjyhwyxjl", btBizJzghwyxInfo.getSfjyhwyxjl());
		try {
			ISqlElement se = processSql(params, "OverseasStudyManagerExt.updateOverseasStudy.update");
			getJdbcTemplate().update(se.getSql(), se.getParams());
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt updateOverseasStudy() is error!", e);
			iOk = -1;
		}
		return iOk;
	}

}
