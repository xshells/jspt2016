/**
 * TeacherChangeExt.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.commons.hibernate.IPage;
import org.nestframework.commons.hibernate.IRowHandler;
import org.nestframework.commons.hibernate.ISqlElement;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.TeacherAccountInfoDto;
import com.becom.jspt2016.dto.TeacherChangeDto;
import com.becom.jspt2016.service.ext.ITeacherChangeExt;

/**
 * TODO(教师变动---ext实现类)
 * <p>
 * TODO()
 *
 * @author   fmx
 * @Date	 2016年10月26日 	 
 */
public class TeacherChangeExt extends JdbcRootManager implements ITeacherChangeExt {
	protected Logger logger = Logger.getLogger(this.getClass());
	public final SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");

	@Override
	public IPage doSearch(Map<String, Object> param, int pageNo, int pageSize) {
		try {
			IPage<TeacherChangeDto> dtos = this.findPage("TeacherChangeExt.doSearch.query",
					"TeacherChangeExt.doSearch.count", param, pageNo, pageSize, new IRowHandler<TeacherChangeDto>() {
						public TeacherChangeDto handleRow(ResultSet rs) {
							TeacherChangeDto dto = new TeacherChangeDto();
							try {
								dto.setId(rs.getInt("id"));
								dto.setTid(rs.getInt("tId"));
								dto.setName(rs.getString("name"));
								dto.setEduId(rs.getInt("eduId"));
								dto.setCardNo(rs.getString("cardNo"));
								dto.setSex(rs.getString("sex"));
								dto.setOutId(rs.getInt("outId"));
								dto.setOutName(rs.getString("outName"));
								dto.setInTime(rs.getString("inTime").substring(0, 10));
								dto.setStatus(rs.getString("status"));
								dto.setApplyTime(rs.getString("applyTime").substring(0, 10));
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			return dtos;
		} catch (Exception e) {
			logger.error("TeacherChangeExt doSearch() is error!", e);
		}
		return null;
	}

	@Override
	public void add(final TeacherChangeDto changeDto) {

		StringBuffer sql = new StringBuffer();
		sql.append(
				"insert into TB_JSDDB(JSDD_ID,JSID,XM,XB,SFZJH,DR_XXJGID,DC_XXJGID,DRRQ,DCRQ,DDYY,SQSJ,SQR_USER_ID,SQR,SPZT)")
				.append("values (SEQ_TB_JSDDB.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setInt(1, changeDto.getTid());
				ps.setString(2, changeDto.getName());
				ps.setString(3, changeDto.getSex());
				ps.setString(4, changeDto.getCardNo());
				ps.setInt(5, changeDto.getInId() != null ? changeDto.getInId() : changeDto.getOutId());
				ps.setInt(6, changeDto.getOutId());
				System.out.println(changeDto.getInTime());
				try {
					ps.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
					ps.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
					ps.setTimestamp(10, new Timestamp(fmt.parse(changeDto.getApplyTime()).getTime()));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				ps.setString(9, changeDto.getCauseStr());

				ps.setLong(11, changeDto.getSqrid());
				ps.setString(12, changeDto.getSqrname());
				ps.setString(13, "1");
			}
		});
		//改变教师机构信息
		change(changeDto);
	}

	@Override
	public int edit(TeacherChangeDto changeDto) {
		int result = 0;

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", changeDto.getId());
		params.put("tid", changeDto.getTid());
		params.put("name", changeDto.getName());
		params.put("cardNo", changeDto.getCardNo());
		params.put("outId", changeDto.getOutId());
		params.put("sex", changeDto.getSex());
		try {
			params.put("inTime", new Timestamp(fmt.parse(changeDto.getInTime()).getTime()));
			params.put("applyTime", new Timestamp(fmt.parse(changeDto.getApplyTime()).getTime()));
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		params.put("causeStr", changeDto.getCauseStr());

		try {
			ISqlElement processSql = processSql(params, "TeacherChangeExt.edit.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherChangeExt edit() is error!", e);
		}
		return result;
	}

	public int change(TeacherChangeDto changeDto) {
		int result = 0;

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jsId", changeDto.getTid());
		params.put("jgId", changeDto.getInId());

		try {
			ISqlElement processSql = processSql(params, "TeacherChangeExt.change.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherChangeExt change() is error!", e);
		}
		return result;
	}

	@Override
	public int delete(String[] ck) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ids", ck);
		try {
			ISqlElement processSql = processSql(params, "TeacherChangeExt.delete.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherChangeExt delete() is error!", e);
		}
		return result;
	}

	@Override
	public TeacherChangeDto get(Map<String, Object> param) {
		try {
			List<TeacherChangeDto> rolelist = this.findList("TeacherChangeExt.get.query", param, new RowMapper() {

				@Override
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TeacherChangeDto dto = new TeacherChangeDto();
					dto.setId(rs.getInt("id"));
					dto.setTid(rs.getInt("tId"));
					dto.setName(rs.getString("name"));
					dto.setEduId(rs.getInt("eduId"));
					dto.setCardNo(rs.getString("cardNo"));
					dto.setSex(rs.getString("sex"));
					dto.setOutId(rs.getInt("outId"));
					dto.setOutName(rs.getString("outName"));
					dto.setInTime(rs.getString("inTime").substring(0, 10));
					dto.setStatus(rs.getString("status"));
					dto.setApplyTime(rs.getString("applyTime").substring(0, 10));
					dto.setCauseStr(rs.getString("causeStr"));

					return dto;
				}
			});
			return rolelist.size() > 0 ? rolelist.get(0) : null;
		} catch (Exception e) {
			logger.error("TeacherChangeExt getTeacher() is error!", e);
		}
		return null;
	}

	public Map<String, Object> getTeacher(String card) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("cardNo", card);
		try {
			List<Map<String, Object>> rolelist = this.findList("TeacherChangeExt.getTeacher.query", param,
					new RowMapper() {

						@Override
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							Map<String, Object> map = new HashMap<String, Object>();
							map.put("signNo", rs.getString("signNo"));
							map.put("name", rs.getString("name"));
							map.put("sex", rs.getString("sex"));
							map.put("cardType", rs.getString("cardType"));
							map.put("birth", rs.getString("birth"));
							map.put("jgId", rs.getString("jgId"));
							map.put("outName", rs.getString("outName"));
							map.put("tId", rs.getString("jsId"));
							return map;
						}
					});
			return rolelist.size() > 0 ? rolelist.get(0) : null;
		} catch (Exception e) {
			logger.error("TeacherChangeExt getTeacher() is error!", e);
		}
		return null;
	}

	public Map<String, Object> getJg(String jgId) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("jgId", jgId);
		try {
			List<Map<String, Object>> rolelist = this.findList("SysuserManageExt.getJg.query", param, new RowMapper() {

				@Override
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("name", rs.getString("name"));
					return map;
				}
			});
			return rolelist.size() > 0 ? rolelist.get(0) : null;
		} catch (Exception e) {
			logger.error("TeacherChangeExt getQu() is error!", e);
		}
		return null;
	}

	////////////////出动审核部分//////////////////////////////

	@Override
	public IPage doSearchC(Map<String, Object> param, int pageNo, int pageSize) {
		try {
			IPage<TeacherChangeDto> dtos = this.findPage("TeacherChangeExt.doSearchC.query",
					"TeacherChangeExt.doSearchC.count", param, pageNo, pageSize, new IRowHandler<TeacherChangeDto>() {
						public TeacherChangeDto handleRow(ResultSet rs) {
							TeacherChangeDto dto = new TeacherChangeDto();
							try {
								dto.setId(rs.getInt("id"));
								dto.setTid(rs.getInt("tId"));
								dto.setName(rs.getString("name"));
								dto.setEduId(rs.getInt("eduId"));
								dto.setCardNo(rs.getString("cardNo"));
								dto.setSex(rs.getString("sex"));
								dto.setInId(rs.getInt("inId"));
								dto.setInName(rs.getString("inName"));
								dto.setOutTime(rs.getString("outTime").substring(0, 10));
								dto.setStatus(rs.getString("status"));
								dto.setApplyTime(rs.getString("applyTime").substring(0, 10));
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			return dtos;
		} catch (Exception e) {
			logger.error("TeacherChangeExt doSearch() is error!", e);
		}
		return null;
	}

	public int sub(Map<String, Object> params) {
		int result = 0;

		try {
			ISqlElement processSql = processSql(params, "TeacherChangeExt.sub.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherChangeExt edit() is error!", e);
		}
		return result;
	}

	public int subAll(Map<String, Object> params) {
		int result = 0;
		try {
			ISqlElement processSql = processSql(params, "TeacherChangeExt.subAll.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherChangeExt delete() is error!", e);
		}
		return result;
	}

	@Override
	public IPage<TeacherAccountInfoDto> findTeacherMove(Map<String, Object> param, int pageNo, int pageSize) {
		try {
			IPage<TeacherAccountInfoDto> dtos = this.findPage("TeacherChangeExt.findTeacherMove.query",
					"TeacherChangeExt.findTeacherMove.count", param, pageNo, pageSize, new IRowHandler<TeacherAccountInfoDto>() {
						public TeacherAccountInfoDto handleRow(ResultSet rs) {
							TeacherAccountInfoDto dto = new TeacherAccountInfoDto();
							try {
								dto.setJsId(Integer.valueOf(rs.getString("jsId")));
								dto.setEducationId(rs.getString("educationId"));
								dto.setTeacherName(rs.getString("teacherName"));
								dto.setCardNo(rs.getString("cardNo"));
								dto.setBirthday(rs.getString("birthday"));
								dto.setAccountStatus(rs.getString("accountStatus"));
								dto.setSex(rs.getString("sex"));
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			return dtos;
		} catch (Exception e) {
			logger.error("TeacherChangeExt doSearch() is error!", e);
			
		}
		return null;
	}
}
