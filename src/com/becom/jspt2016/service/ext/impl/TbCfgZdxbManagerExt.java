/**
 * TbCfgZdxbManagerExt.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.JzglyZdxbDto;
import com.becom.jspt2016.dto.ZxdwlbZdxbDto;
import com.becom.jspt2016.model.TbCfgZdxbId;
import com.becom.jspt2016.service.ext.ITbCfgZdxbManagerExt;

/**
 * 查询字典表
 * <p>
 *
 * @author   YuChengCheng
 * @author   zhahgchunming
 * @Date	 2016年10月18日 	 
 * @Date	 2016年10月24日 	 
 */
public class TbCfgZdxbManagerExt extends JdbcRootManager implements ITbCfgZdxbManagerExt {

	protected Logger logger = Logger.getLogger(this.getClass());

	private String ISVALID = "1";

	/**
	 * 教职工来源字典大类
	 */
	private String JZGLY = "JSXX_JZGLY";

	/**
	 * 在学单位类别字典大类
	 */
	private String ZXDWLB = "JSXX_ZXDWLB";

	/**
	 * 在用
	 */
	private int SFZY = 1;

	/**
	 * (non-Javadoc)
	 * @see com.becom.jspt2016.service.ext.ITbCfgZdxbManagerExt#findAllInfo()
	 */
	public List<TbCfgZdxbId> findAllInfo() {
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			List<TbCfgZdxbId> dtos = this.findList("TbCfgZdxbManagerExt.findAllInfo.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbCfgZdxbId dto = new TbCfgZdxbId();
					dto.setZdbs(rs.getString("ZDBS"));
					dto.setZdmc(rs.getString("ZDMC"));
					dto.setZdxbm(rs.getString("ZDXBM"));
					dto.setZdxmc(rs.getString("ZDXMC"));

					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("TbCfgZdxbManagerExt findAllInfo() is error!", e);
		}
		return null;
	}

	/**
	 * 教职工来源
	 * <p>
	 * 根据教师的所在机构类别获取教职工来源列表
	 * 
	 * @param ssxd 所属学段
	 * @see com.becom.jspt2016.service.ext.ITbCfgZdxbManagerExt#findJzgly(java.lang.String)
	 */
	@Override
	public List<JzglyZdxbDto> findJzgly(String ssxd) {

		Map<String, Object> params = new HashMap<String, Object>();
		String sqlId = "";
		if (ssxd.equals("10")) {//本科院校
			sqlId = "TbCfgZdxbManagerExt.findJzgly.queryGX";
		}
		if (ssxd.equals("20")) {//高等职业学校
			sqlId = "TbCfgZdxbManagerExt.findJzgly.queryGZ";
		}
		if (ssxd.equals("30")) {//中小学校
			sqlId = "TbCfgZdxbManagerExt.findJzgly.queryZXX";
		}
		if (ssxd.equals("40")) {//中等职业学校
			sqlId = "TbCfgZdxbManagerExt.findJzgly.ZZGX";
		}
		if (ssxd.equals("50")) {//特殊教育学校
			sqlId = "TbCfgZdxbManagerExt.findJzgly.queryTJ";
		}
		if (ssxd.equals("60")) {//幼儿园
			sqlId = "TbCfgZdxbManagerExt.findJzgly.queryYEY";
		}
		params.put("zdbs", JZGLY);
		params.put("isvalid", ISVALID);
		params.put("sfzy", SFZY);
		try {
			List<JzglyZdxbDto> dtos = this.findList(sqlId, params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					JzglyZdxbDto dto = new JzglyZdxbDto();
					dto.setZdxbm(rs.getString("zdxbm"));
					dto.setZdxmc(rs.getString("zdxmc"));
					dto.setFzdx(rs.getString("fzdx"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("TbCfgZdxbManagerExt findJzgly() is error!", e);
		}
		return null;

	}

	/**
	 * 在学单位类别
	 * 
	 * @see com.becom.jspt2016.service.ext.ITbCfgZdxbManagerExt#findJzgly(java.lang.String)
	 */
	@Override
	public List<ZxdwlbZdxbDto> findZxdwlb() {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("zdbs", ZXDWLB);
		params.put("sfzy", SFZY);
		try {
			List<ZxdwlbZdxbDto> dtos = this.findList("TbCfgZdxbManagerExt.findZxdwlb.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					ZxdwlbZdxbDto dto = new ZxdwlbZdxbDto();
					dto.setZdxbm(rs.getString("zdxbm"));
					dto.setZdxmc(rs.getString("zdxmc"));
					dto.setFzdx(rs.getString("fzdx"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("TbCfgZdxbManagerExt findZxdwlb() is error!", e);
		}
		return null;

	}

	/**
	 * (non-Javadoc)
	 * @see com.becom.jspt2016.service.ext.ITbCfgZdxbManagerExt#findNameByTypeAndId(java.lang.String, java.lang.String)
	 */
	@Override
	public List<TbCfgZdxbId> findNameByType(String dictType, String schoolType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("dictType", dictType);
		params.put("schoolType", schoolType);
		try {
			List<TbCfgZdxbId> dtos = this.findList("TbCfgZdxbManagerExt.findNameByType.query", params, new RowMapper() {
				@Override
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbCfgZdxbId dto = new TbCfgZdxbId();
					dto.setZdbs(rs.getString("ZDBS"));
					dto.setZdmc(rs.getString("ZDMC"));
					dto.setZdxbm(rs.getString("ZDXBM"));
					dto.setZdxmc(rs.getString("ZDXMC"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("TbCfgZdxbManagerExt findNameByType() is error!", e);
		}
		return null;
	}

	/**
	 * (non-Javadoc)
	 * @see com.becom.jspt2016.service.ext.ITbCfgZdxbManagerExt#findNameBybsbmtype(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public String findNameBybsbmtype(String dictBs, String dictBm, String schoolType) {

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("dictType", dictBs);
		if (dictBm == null || dictBm.trim() == "") {
			params.put("dictBm", "-1");
		} else {
			params.put("dictBm", dictBm);
		}
		params.put("schoolType", schoolType);
		try {
			List<TbCfgZdxbId> dtos = this.findList("TbCfgZdxbManagerExt.findNameBybsbmtype.query", params,
					new RowMapper() {
						@Override
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TbCfgZdxbId dto = new TbCfgZdxbId();
							dto.setZdbs(rs.getString("ZDBS"));
							dto.setZdmc(rs.getString("ZDMC"));
							dto.setZdxbm(rs.getString("ZDXBM"));
							dto.setZdxmc(rs.getString("ZDXMC"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0).getZdxmc();
			}
		} catch (Exception e) {
			logger.error("TbCfgZdxbManagerExt findNameBybsbmtype() is error!", e);
		}
		return "";

	}

}
