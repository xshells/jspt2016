package com.becom.jspt2016.service.ext.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.commons.hibernate.IPage;
import org.nestframework.commons.hibernate.IRowHandler;
import org.nestframework.commons.hibernate.ISqlElement;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.model.TbBizJzgjbdyxx;
import com.becom.jspt2016.model.TbXxjgb;
import com.becom.jspt2016.service.ext.IBasicTreatManagerExt;

/**
 * (基本待遇---ext实现类)
 * <p>
 * @author   董锡福
 * @Date	 2016年10月20日 	 
 */
public class BasicTreatManagerExt extends JdbcRootManager implements IBasicTreatManagerExt {

	protected Logger logger = Logger.getLogger(this.getClass());

	public IPage<TbBizJzgjbdyxx> doSearch(Map<String, Object> params, int pageNo, int pageSize) {
		try {
			IPage<TbBizJzgjbdyxx> dtos = this.findPage("BasicTreatManagerExt.findJbdy.query",
					"BasicTreatManagerExt.findJbdy.count", params, pageNo, pageSize, new IRowHandler<TbBizJzgjbdyxx>() {
						public TbBizJzgjbdyxx handleRow(ResultSet rs) {
							TbBizJzgjbdyxx dto = new TbBizJzgjbdyxx();
							try {
								dto.setJbdyId(rs.getLong("JBDY_ID"));
								dto.setNd(rs.getString("nd"));
								dto.setNgzsr(rs.getString("ngzsr"));
								dto.setJbgz(rs.getString("jbgz"));
								dto.setJxgz(rs.getString("jxgz"));
								dto.setXcjsshbz(rs.getString("xcjsshbz"));
								dto.setJtbt(rs.getString("jtbt"));
								dto.setTjjtbt(rs.getString("tjjtbt"));
								dto.setQt(rs.getString("qt"));
								dto.setWxyj(rs.getString("wxyj"));
								dto.setCjsj(rs.getDate("cjsj"));
								String wxyj = rs.getString("wxyj");
								String[] arr = wxyj.split(",");
								dto.setWxyjList(arr);
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			if (dtos != null) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("BasicTreatManagerExt doSearch() is error!", e);
		}
		return null;
	}

	//添加
	public void SubAddpoint(final TbBizJzgjbdyxx dy) {
		StringBuffer sql = new StringBuffer();
		sql.append(
				"insert into TB_BIZ_JZGJBDYXX(JBDY_ID,JSID,ND,NGZSR,JBGZ,JXGZ,XCJSSHBZ,TJJTBT,JTBT,QT,WXYJ,BSRLB,SHZT,SHSJ,SHJG,SFSC,CJR,CJSJ)")
				.append("values (SEQ_TB_BIZ_JZGJBDYXX.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, dy.getTbBizJzgjbxx().getJsid());
				ps.setString(2, dy.getNd());
				ps.setString(3, dy.getNgzsr());
				ps.setString(4, dy.getJbgz());
				ps.setString(5, dy.getJxgz());
				ps.setString(6, dy.getXcjsshbz());
				ps.setString(7, dy.getTjjtbt());
				ps.setString(8, dy.getJtbt());
				ps.setString(9, dy.getQt());
				ps.setString(10, dy.getWxyj());
				ps.setString(11, "4");
				ps.setString(12, "01");
				ps.setTimestamp(13, new java.sql.Timestamp(dy.getShsj().getTime()));
				ps.setString(14, "");//审核结果
				ps.setString(15, "0");//是否删除
				ps.setLong(16, dy.getTbBizJzgjbxx().getJsid());//创建人
				ps.setTimestamp(17, new java.sql.Timestamp(dy.getCjsj().getTime()));//创建时间
			}
		});

	}

	//修改时回显
	public TbBizJzgjbdyxx editBasicTreat(long jbdyId) {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jbdyId", jbdyId);
			List<TbBizJzgjbdyxx> dtos = this.findList("BasicTreatManagerExt.editJbdy.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbBizJzgjbdyxx dto = new TbBizJzgjbdyxx();
					dto.setJbdyId(rs.getLong("JBDY_ID"));
					dto.setNd(rs.getString("nd"));//年度
					dto.setNgzsr(rs.getString("ngzsr"));//年工资收入
					dto.setJbgz(rs.getString("jbgz"));//基本工资
					dto.setJxgz(rs.getString("jxgz"));//绩效工资
					dto.setXcjsshbz(rs.getString("xcjsshbz"));//乡村教师生活补助
					dto.setTjjtbt(rs.getString("tjjtbt"));//特教津贴补贴
					dto.setJtbt(rs.getString("jtbt"));//其它津贴补贴
					dto.setQt(rs.getString("qt"));//其它
					dto.setWxyj(rs.getString("wxyj"));//五险一金
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("BasicTreatManagerExt doSearch() is error!", e);
		}
		return null;
	}

	//执行修改
	public int updateBasicTreat(TbBizJzgjbdyxx dy) {
		int result = 0;
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jbdyId", dy.getJbdyId());
			params.put("nd", dy.getNd());//年度
			params.put("ngzsr", dy.getNgzsr());//年工资收入 
			params.put("jbgz", dy.getJbgz()); //基本工资    
			params.put("jxgz", dy.getJxgz()); //绩效工资    
			params.put("xcjsshbz", dy.getXcjsshbz());//乡村教师生活补助 
			params.put("qt", dy.getQt()); //其他
			params.put("wxyj", dy.getWxyj());//五险一金  
			params.put("jtbt", dy.getJtbt());//其他津贴补贴
			ISqlElement se = processSql(params, "BasicTreatManagerExt.doUpdateJbdy.update");
			getJdbcTemplate().update(se.getSql(), se.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("BasicTreatManagerExt doSearch() is error!", e);
		}
		return result;
	}

	//删除
	public int removeBasicTreat(String[] ck) {
		int result = 0;
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("ids", ck);
			ISqlElement se = processSql(params, "BasicTreatManagerExt.doDeleteJbdy.update");
			getJdbcTemplate().update(se.getSql(), se.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("BasicTreatManagerExt doSearch() is error!", e);
		}
		return result;
	}

	@Override
	public String getXd(Map<String, Object> params) {

		try {
			List<TbXxjgb> dtos = this.findList("JZGGWPRXXManagerExt.query.role", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbXxjgb dto = new TbXxjgb();
					dto.setXxjgid(rs.getLong("XXJGID"));
					dto.setSsxd(rs.getString("SSXD"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0).getSsxd();
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;

	}
}
