/**
 * TbCfgXzqhdmManagerExt.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.TbCfgXzqhdmDto;
import com.becom.jspt2016.service.ext.ITbCfgXzqhdmManagerExt;

/**
 * 行政区划代码表服务
 *
 * @author   ZhuanJunxiang
 * @Date	 2016年10月22日 	 
 */
public class TbCfgXzqhdmManagerExt extends JdbcRootManager implements ITbCfgXzqhdmManagerExt {

	protected Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 未删除状态
	 */
	private final String SFSC = "0";

	/**
	 * 获得区划代码列表
	 * @see com.becom.jspt2016.service.ext.ITbCfgXzqhdmManagerExt#findAll()
	 */
	@Override
	public List<TbCfgXzqhdmDto> findAll() {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("sfsc", SFSC);
			List<TbCfgXzqhdmDto> dtos = this.findList("TbCfgXzqhdmManagerExt.findAll.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbCfgXzqhdmDto dto = new TbCfgXzqhdmDto();
					dto.setId(rs.getString("xzqhdm"));
					dto.setName(rs.getString("xzqhmc"));
					dto.setPid(rs.getString("fxzqhdm"));
					dto.setLevel(rs.getInt("jb"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("TbCfgXzqhdmManagerExt findAll() is error!", e);
		}
		return null;

	}

}
