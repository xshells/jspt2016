/**
 * TbJzgxxshManagerExt.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.model.TbJzgxxsh;
import com.becom.jspt2016.service.ext.ITbJzgxxshManagerExt;

/**
 * 教职工信息审核服务
 *
 * @author   zhagnchunming
 * @Date	 2016年10月28日 	 
 */
public class TbJzgxxshManagerExt extends JdbcRootManager implements ITbJzgxxshManagerExt {

	protected Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 查询
	 * <p>
	 * 根据教师id查询审核状况
	 *
	 * @param jsid
	 */
	@Override
	public TbJzgxxsh query(long jsid) {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsid", jsid);
			List<TbJzgxxsh> dtos = this.findList("TbJzgxxshManagerExt.query.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbJzgxxsh dto = new TbJzgxxsh();
					dto.setShzt(rs.getString("shzt"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("TbJzgxxshManagerExt query() is error!", e);
		}

		return null;
	}

}
