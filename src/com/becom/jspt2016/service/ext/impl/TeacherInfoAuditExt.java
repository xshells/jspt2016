/**
 * TeacherInfoAuditExt.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.commons.hibernate.IPage;
import org.nestframework.commons.hibernate.IRowHandler;
import org.nestframework.commons.hibernate.ISqlElement;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.AduitCommentDto;
import com.becom.jspt2016.dto.AduitLogDto;
import com.becom.jspt2016.dto.InstitutionsInfoDto;
import com.becom.jspt2016.dto.TeacherInfoAuditDto;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzggnpxxx;
import com.becom.jspt2016.model.TbBizJzgjbdyxx;
import com.becom.jspt2016.model.TbBizJzgsdcfxx;
import com.becom.jspt2016.model.TbBizJzgsdkhxx;
import com.becom.jspt2016.service.ext.ITeacherInfoAuditExt;

/**
 * 教师信息审核业务层
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 * @author   kuangxiang
 * @Date	 2016年10月25日
 */

public class TeacherInfoAuditExt extends JdbcRootManager implements ITeacherInfoAuditExt {
	protected Logger logger = Logger.getLogger(this.getClass());
	/**
	 * 是否删除 0-否 1-是 默认0
	 */
	private static final String SFZY = "0";
	/**
	 * 区审核不通过
	 */
	private static final String QUSHZTNO = "22";
	/**
	 * 区审核通过
	 */
	private static final String QUSHZTYES = "21";
	/**
	 * 未报送
	 */
	private static final String NOSUBMIT = "01";
	/**
	 * 审核结果不通过 0-审核中 1-通过 2-不通过 
	 */
	private static final String NOADOPT = "2";
	/**
	 * 审核结果通过
	 */
	private static final String ADOPT = "1";
	/**
	 * 信息类型  01 是基本信息
	 */
	private static final String XXLX = "01";
	/**
	 * 审核结果  审核中
	 */
	private static final String WAITADOPT = "0";

	/**
	 * 搜索
	 * 
	 */
	@Override
	public IPage doSearch(Map<String, Object> param, int pageNo, int pageSize) {
		try {
			param.put("sfsc", SFZY);//是否删除
			IPage<TeacherInfoAuditDto> dtos = this.findPage("TeacherInfoAuditExt.doSearch.query",
					"TeacherInfoAuditExt.doSearch.count", param, pageNo, pageSize,
					new IRowHandler<TeacherInfoAuditDto>() {
						public TeacherInfoAuditDto handleRow(ResultSet rs) {
							TeacherInfoAuditDto dto = new TeacherInfoAuditDto();
							try {
								dto.setJsId(Integer.valueOf(rs.getString("jsId")));
								SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
								if (rs.getTimestamp("aduitDay") != null) {
									dto.setAduitDay(sf.format(rs.getTimestamp("aduitDay")));
								}
								dto.setAduitStutas(rs.getString("aduitStutas"));
								dto.setBirthDay(rs.getString("birthDay"));
								dto.setCardNo(rs.getString("cardNo"));
								dto.setEduId(rs.getString("eduId"));
								dto.setSex(rs.getString("sex"));
								dto.setTeacherName(rs.getString("teacherName"));

								dto.setXd(rs.getString("xd"));
								dto.setUnitType(rs.getString("unitType"));
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			return dtos;
		} catch (Exception e) {
			logger.error("TeacherInfoAuditExt doSearch() is error!", e);
		}
		return null;

	}

	/**
	 * 审核
	 * 1.审核基本信息表
	 * 2.审核表
	 * 3.添加审核日志表
	 * 4,一堆的子表
	 * 
	 */
	@Override
	public void aduit(String[] ck, String isAdopt, String aduitOpinion, InstitutionsInfoDto institution, SysUser sUser,
			String ip) {
		for (String c : ck) {
			long jsId = Long.valueOf(c);
			String aduitStatus = "";
			String aduitResult = "";
			if ("1".equals(isAdopt)) {
				//审核通过
				aduitStatus = QUSHZTYES;
				aduitResult = ADOPT;
			} else if ("2".equals(isAdopt)) {
				//审核不通过
				aduitStatus = QUSHZTNO;
				aduitResult = WAITADOPT;
			}
			AduitCommentDto adDto = new AduitCommentDto();
			adDto.setJdId(jsId);
			adDto.setAduitStatus(aduitStatus);
			adDto.setAduitResult(aduitResult);
			adDto.setAduitOpinion(aduitOpinion);
			if (institution != null && institution.getAffiliatedSchool() != null) {
				adDto.setSsxd(institution.getAffiliatedSchool());

			}
			adDto.setAduitPerson(sUser.getUserName());
			adDto.setAduitPersonId(sUser.getUserId());
			adDto.setAduitPersonIp(ip);

			aduitBasicInfo(adDto);
			updateAduit(adDto);
			long aduitId = fetchAduitBInfo(jsId);
			adDto.setAduitId(aduitId);
			addAduitLog(adDto);

			childrenTableAduit(adDto);
		}
	}

	/**
	 * 审核基本信息表
	 * <p>
	 *
	 * @param jsId  教职工ID
	 * @param aduitStatus 审核状态
	 * @param aduitResult 审核结果
	 */
	public int aduitBasicInfo(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jsId", adDto.getJdId());
		params.put("shzt", adDto.getAduitStatus());
		params.put("shjg", adDto.getAduitResult());
		params.put("shsj", new Timestamp(System.currentTimeMillis()));
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.aduitBasicInfo.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt aduitBasicInfo() is error!", e);
		}
		return result;
	}

	/**
	 * 更新审核表
	 * 1.基本信息表关联机构变查询所属学段，
	 * <p>
	 *
	 */
	public int updateAduit(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		//		params.put("ssxd", adDto.getSsxd());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitResult", adDto.getAduitResult());
		params.put("aduitOpinion", adDto.getAduitOpinion());
		params.put("aduitPerson", adDto.getAduitPerson());
		params.put("aduitPersonId", adDto.getAduitPersonId());
		params.put("aduitPersonIp", adDto.getAduitPersonIp());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("xxlx", XXLX);
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateAduit.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateAduit() is error!", e);
		}
		return result;
	}

	/**
	 * 查询审核表信息
	 */
	public long fetchAduitBInfo(long jsId) {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsId", jsId);
			List<AduitCommentDto> dtos = this.findList("TeacherInfoAuditExt.fetchAduitBInfo.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							AduitCommentDto dto = new AduitCommentDto();
							dto.setAduitId(Long.valueOf(rs.getString("aduitId")));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0).getAduitId();
			}
		} catch (Exception e) {
			logger.error("TeacherInfoAuditExt fetchAduitBInfo() is error!", e);
		}
		return 0;
	}

	/**
	 * 添加审核日志表
	 * <p>
	 *
	 * @param AduitCommentDto 审核内容dto
	 */
	public void addAduitLog(final AduitCommentDto adDto) {
		StringBuffer sql = new StringBuffer();
		sql.append(
				"insert into TB_JZGXXSH_LOG(LOG_ID,SH_ID,JSID,SSXD,XXLB ,SHZT ,SHJG,SHYJ ,SHSJ,SHR,SHR_USER_ID,SHR_IP)")
				.append("values (SEQ_TB_JZGXXSH_LOG.nextval,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				//获取当前管理员信息
				ps.setLong(1, adDto.getAduitId());//机构代码
				ps.setLong(2, adDto.getJdId());
				ps.setString(3, adDto.getSsxd());
				ps.setString(4, XXLX);
				ps.setString(5, adDto.getAduitStatus());
				ps.setString(6, adDto.getAduitResult());
				ps.setString(7, adDto.getAduitOpinion());
				ps.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
				ps.setString(9, adDto.getAduitPerson());
				ps.setLong(10, adDto.getAduitPersonId());
				ps.setString(11, adDto.getAduitPersonIp());
			}
		});
	}

	/**
	 * 教职工基本信息子表审核
	 * <p>
	 * @param adDto 审核内容dto
	 */
	public void childrenTableAduit(AduitCommentDto adDto) {
		updateZiGeXXB(adDto);
		updateZYJSZWPRB(adDto);
		updateJLLGB(adDto);
		updateRXRCXM(adDto);
		if (queryGNPXB(adDto.getJdId()).size() > 0) {
			updateGNPX(adDto);
		}
		updateXXJL(adDto);
		updateGWPR(adDto);
		updateGZJL(adDto);

		if (querySDCFB(adDto.getJdId()).size() > 0) {
			updateSDCF(adDto);
		}
		if (querySDKHB(adDto.getJdId()).size() > 0) {
			updateSDKH(adDto);
		}
		updateSDRY(adDto);
		updateNDKH(adDto);
		updateJNJZSQ(adDto);
		updateYYJN(adDto);
		updateZSB(adDto);
		updateRJZZQ(adDto);
		updateGJYY(adDto);
		updateGJBZJHYBZ(adDto);
		updateCJBSHJ(adDto);
		updateWYZP(adDto);
		updateHJXX(adDto);
		updateLWXX(adDto);
		updateZZXX(adDto);
		updateXMKT(adDto);
		updateJYJX(adDto);
		updateYXHFX(adDto);
		updateLXXX(adDto);
		updateZXBgGHYJBG(adDto);
		if (queryJBDYB(adDto.getJdId()).size() > 0) {
			updateJBDYXX(adDto);
		}
	}

	/**
	 * 审核 资格信息表
	 * <p>
	 *
	 * @param adDto 审核内容dto
	 */
	public int updateZiGeXXB(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateZiGeXXB.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateZiGeXXB() is error!", e);
		}
		return result;
	}

	/**
	 * 审核    教职工专业技术职务聘任信息表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateZYJSZWPRB(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateZYJSZWPRB.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateZYJSZWPRB() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   交流轮岗表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateJLLGB(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateJLLGB.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateJLLGB() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工入选人才项目信息表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateRXRCXM(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateRXRCXM.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateRXRCXM() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   国内培训信息表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateGNPX(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateGNPX.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateGNPX() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工学习经历信息表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateXXJL(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateXXJL.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateXXJL() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工岗位聘任信息表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateGWPR(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateGWPR.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateGWPR() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工工作经历信息表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateGZJL(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateGZJL.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateGZJL() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工师德处分信息表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateSDCF(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateSDCF.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateSDCF() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工师德考核信息表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateSDKH(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateSDKH.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateSDKH() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工师德荣誉信息表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateSDRY(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateSDRY.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateSDRY() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工年度考核表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateNDKH(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateNDKH.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateNDKH() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工技能及证书 ---其他技能信息表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateJNJZSQ(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateJNJZSQ.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateJNJZSQ() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工技能及证书 ---语言技能信息表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateYYJN(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateYYJN.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateYYJN() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工技能及证书 ---证书表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateZSB(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateZSB.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateZSB() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工科研成果及获奖--专利或软件著作权信息表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateRJZZQ(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateRJZZQ.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateRJZZQ() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工科研成果及获奖--国家医药证书信息表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateGJYY(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateGJYY.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateGJYY() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工科研成果及获奖--国家标准及行业标准信息表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateGJBZJHYBZ(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateGJBZJHYBZ.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateGJBZJHYBZ() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工科研成果及获奖--指导学生参加比赛获奖信息表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateCJBSHJ(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateCJBSHJ.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateCJBSHJ() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工科研成果及获奖--文艺作品信息表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateWYZP(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateWYZP.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateWYZP() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工科研成果及获奖--获奖信息表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateHJXX(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateHJXX.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateHJXX() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工科研成果及获奖--论文信息表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateLWXX(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateLWXX.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateLWXX() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工科研成果及获奖--著作信息表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateZZXX(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateZZXX.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateZZXX() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工科研成果及获奖--项目课题表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateXMKT(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateXMKT.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateXMKT() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工科研成果及获奖--教育教学表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateJYJX(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateJYJX.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateJYJX() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工海外研修和访学表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateYXHFX(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateYXHFX.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateYXHFX() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工联系信息表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateLXXX(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateLXXX.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateLXXX() is error!", e);
		}
		return result;
	}

	/**
	 * 审核   教职工科研成果及获奖-咨询报告或研究报告信息表
	 * <p>
	 *
	 * @param adDto  审核内容dto
	 */
	public int updateZXBgGHYJBG(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateZXBgGHYJBG.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateZXBgGHYJBG() is error!", e);
		}
		return result;
	}

	/**
	 * 审核  教职工基本待遇信息表
	 * <p>
	 * @param adDto
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	public int updateJBDYXX(AduitCommentDto adDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", adDto.getJdId());
		params.put("aduitStatus", adDto.getAduitStatus());
		params.put("aduitTime", new Timestamp(System.currentTimeMillis()));
		params.put("aduitResult", adDto.getAduitResult());
		try {
			ISqlElement processSql = processSql(params, "TeacherInfoAuditExt.updateJBDYXX.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TeacherInfoAuditExt updateJBDYXX() is error!", e);
		}
		return result;
	}

	/**
	 * 查询审核意见
	 * 
	 */
	@Override
	public List<AduitLogDto> queryAduitComment(long jsId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jsId", jsId);
		try {
			List<AduitLogDto> list = this.findList("TeacherInfoAuditExt.queryAduitComment.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							AduitLogDto dto = new AduitLogDto();
							dto.setJsId(rs.getLong("jsId"));
							dto.setAduitOpinion(rs.getString("aduitOpinion"));
							return dto;
						}
					});
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * 查询国内培训信息
	 * <p>
	 *
	 * @param jsId 教职工Id
	 */
	public List<TbBizJzggnpxxx> queryGNPXB(long jsId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jsId", jsId);
		try {
			List<TbBizJzggnpxxx> list = this.findList("TeacherInfoAuditExt.queryGNPXB.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbBizJzggnpxxx dto = new TbBizJzggnpxxx();
					dto.setGnpxId(rs.getLong("gnpxId"));
					return dto;
				}
			});
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 查询基本待遇信息
	 * <p>
	 *
	 * @param jsId 教职工Id
	 */
	public List<TbBizJzgjbdyxx> queryJBDYB(long jsId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jsId", jsId);
		try {
			List<TbBizJzgjbdyxx> list = this.findList("TeacherInfoAuditExt.queryJBDYB.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbBizJzgjbdyxx dto = new TbBizJzgjbdyxx();
					dto.setJbdyId(rs.getLong("jbdyId"));
					return dto;
				}
			});
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 查询师德处分信息
	 * <p>
	 *
	 * @param jsId 教职工Id
	 */
	public List<TbBizJzgsdcfxx> querySDCFB(long jsId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jsId", jsId);
		try {
			List<TbBizJzgsdcfxx> list = this.findList("TeacherInfoAuditExt.querySDCFB.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbBizJzgsdcfxx dto = new TbBizJzgsdcfxx();
					dto.setSdcfId(rs.getLong("sdcfId"));
					return dto;
				}
			});
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 查询师德考核信息
	 * <p>
	 *
	 * @param jsId 教职工Id
	 */
	public List<TbBizJzgsdkhxx> querySDKHB(long jsId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jsId", jsId);
		try {
			List<TbBizJzgsdkhxx> list = this.findList("TeacherInfoAuditExt.querySDKHB.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbBizJzgsdkhxx dto = new TbBizJzgsdkhxx();
					dto.setSdkhId(rs.getLong("sdkhId"));
					return dto;
				}
			});
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
