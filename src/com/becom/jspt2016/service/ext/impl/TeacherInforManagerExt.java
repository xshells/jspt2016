/**
 * TeacherInforManagerExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.service.ext.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.commons.hibernate.IPage;
import org.nestframework.commons.hibernate.IRowHandler;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.TeacherInfoAuditDto;
import com.becom.jspt2016.dto.TeacherInformtionDto;
import com.becom.jspt2016.service.ext.ITeacherInforManagerExt;

/**
 *(教师基本信息---ext实现类)
 * @author   董锡福
 * @Date	 2016年10月24日 	 
 */
public class TeacherInforManagerExt extends JdbcRootManager implements ITeacherInforManagerExt {

	protected Logger logger = Logger.getLogger(this.getClass());
	/**
	 * 是否删除 0-否 1-是 默认0
	 */
	private static final String SFZY = "0";

	public IPage<TeacherInfoAuditDto> doSearch(Map<String, Object> params, int pageNo, int pageSize) {
		try {
			params.put("sfsc", SFZY);//是否删除
			IPage<TeacherInfoAuditDto> dtos = this.findPage("TeacherInforManagerExt.findjzgjb.query",
					"TeacherInforManagerExt.findjzgjb.count", params, pageNo, pageSize,
					new IRowHandler<TeacherInfoAuditDto>() {
						public TeacherInfoAuditDto handleRow(ResultSet rs) {
							TeacherInfoAuditDto dto = new TeacherInfoAuditDto();
							try {
								dto.setJsId(Integer.valueOf(rs.getString("jsId")));
								SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
								if (rs.getTimestamp("aduitDay") != null) {
									dto.setAduitDay(sf.format(rs.getTimestamp("aduitDay")));
								}
								dto.setAduitStutas(rs.getString("aduitStutas"));
								dto.setBirthDay(rs.getString("birthDay"));
								dto.setCardNo(rs.getString("cardNo"));
								dto.setEduId(rs.getString("eduId"));
								dto.setSex(rs.getString("sex"));
								dto.setTeacherName(rs.getString("teacherName"));
								dto.setDisName(rs.getString("disName"));
								
								dto.setXd(rs.getString("xd"));
								dto.setUnitType(rs.getString("unitType"));
								dto.setJgName(rs.getString("jgName"));
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			return dtos;
		} catch (Exception e) {
			logger.error("TeacherInforManagerExt doSearch() is error!", e);
		}
		return null;
	}

	public List<TeacherInformtionDto> queryAdress() {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			List<TeacherInformtionDto> Adress = this.findList("TeacherInforManagerExt.findSelect.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TeacherInformtionDto dto = new TeacherInformtionDto();
							dto.setQxdm((rs.getString("qxdm")));
							dto.setQxmc((rs.getString("qxmc")));
							return dto;
						}
					});
			if (Adress != null && Adress.size() > 0) {
				return Adress;
			}
		} catch (Exception e) {
			logger.error("TeacherInforManagerExt doSearch() is error!", e);
		}
		return null;
	}

}
