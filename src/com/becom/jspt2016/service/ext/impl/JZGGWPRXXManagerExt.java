/**
 * JZGGWPRXXManagerExt.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.service.ext.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.commons.hibernate.IPage;
import org.nestframework.commons.hibernate.IRowHandler;
import org.nestframework.commons.hibernate.ISqlElement;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.TbCfgZdxbInfoDto;
import com.becom.jspt2016.dto.ZxdwlbZdxbDto;
import com.becom.jspt2016.model.TbBizJzggwprxx;
import com.becom.jspt2016.model.TbXxjgb;
import com.becom.jspt2016.service.ext.IJZGGWPRXXManagerExt;

/**
 * 岗位聘任Ext实现类
 * @author   董锡福
 * @Date	 2016年10月16日 	 
 */
public class JZGGWPRXXManagerExt extends JdbcRootManager implements IJZGGWPRXXManagerExt {

	protected Logger logger = Logger.getLogger(this.getClass());

	public IPage<TbBizJzggwprxx> doSearch(Map<String, Object> params, int pageNo, int pageSize) {
		try {
			IPage<TbBizJzggwprxx> dtos = this.findPage("JZGGWPRXXManagerExt.findGWPRXX.query",
					"JZGGWPRXXManagerExt.findGWPRXX.count", params, pageNo, pageSize,
					new IRowHandler<TbBizJzggwprxx>() {
						public TbBizJzggwprxx handleRow(ResultSet rs) {
							TbBizJzggwprxx dto = new TbBizJzggwprxx();
							try {
								dto.setGwprId(rs.getLong("GWPR_ID"));
								dto.setGwlb(rs.getString("gwlb"));
								dto.setGwgj(rs.getString("gwgj"));
								dto.setPrksny(rs.getString("prksny"));
								dto.setSfsjt(rs.getString("sfsjt"));
								dto.setSfzzfdy(rs.getString("sfzzfdy"));
								//dto.setDzzw(rs.getString("dzzw"));
								dto.setRzksny(rs.getString("rzksny"));
								dto.setDzjb(rs.getString("dzjb"));
								dto.setCjsj(rs.getDate("cjsj"));
								if (rs.getString("dzzw") == null) {
									String arr[] = {};
									dto.setDzzwList(arr);
								} else {
									String arr[] = rs.getString("dzzw").split(",");
									dto.setDzzwList(arr);
								}
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			if (dtos != null) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

	public void SubAddpoint(final TbBizJzggwprxx gw) {
		StringBuffer sql = new StringBuffer();
		sql.append(
				"insert into TB_BIZ_JZGGWPRXX(GWPR_ID,JSID,DZZW,gwlb,gwgj,sfsjt,sjtgwlb,sjtgwdj,sfzzcsxlzxgz,sfcyxlzxzgzs,Sfjrqtgw,Jrgwlb,Jrgwdj,sfzzfdy,dzjb,prksny,rzksny,BSRLB,SHZT,SFSC,CJR,CJSJ)")
				.append("values (SEQ_TB_BIZ_JZGGWPRXX.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, gw.getTbBizJzgjbxx().getJsid());
				ps.setString(2, gw.getDzzw()); //党政职务（或校级职务）        
				ps.setString(3, gw.getGwlb()); //岗位类别               
				ps.setString(4, gw.getGwgj()); //岗位等级              
				ps.setString(5, gw.getSfsjt()); //是否双肩挑      ----
				ps.setString(6, gw.getSjtgwlb()); //双肩挑岗位类别     
				ps.setString(7, gw.getSjtgwdj()); //双肩挑岗位等级     
				ps.setString(8, gw.getSfzzcsxlzxgz()); //是否专职从事心理咨询工作     
				ps.setString(9, gw.getSfcyxlzxzgzs()); //是否持有心理咨询资格证书  --

				ps.setString(10, gw.getSfjrqtgw());//是否兼任其他岗位--(中等职业学校用户)
				ps.setString(11, gw.getJrgwlb());//兼任岗位类别
				ps.setString(12, gw.getJrgwdj());//兼任岗位等级

				ps.setString(13, gw.getSfzzfdy());//是否为辅导员 
				ps.setString(14, gw.getDzjb()); //党政级别   
				ps.setString(15, gw.getPrksny()); //聘任开始年月 
				ps.setString(16, gw.getRzksny()); //任职开始年月     
				ps.setString(17, "4");//报送人类别    
				ps.setString(18, "01");//审核状态
				ps.setString(19, "0");//是否删除
				ps.setLong(20, gw.getTbBizJzgjbxx().getJsid());//创建人id
				ps.setTimestamp(21, new java.sql.Timestamp(gw.getCjsj().getTime()));//创建时间
			}
		});

	}

	public TbBizJzggwprxx editJzggwprxx(long gwprId) {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("GWPR_ID", gwprId);
			List<TbBizJzggwprxx> dtos = this.findList("JZGGWPRXXManagerExt.edit.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbBizJzggwprxx dto = new TbBizJzggwprxx();
					dto.setGwprId(rs.getLong("GWPR_ID"));
					dto.setGwlb(rs.getString("gwlb"));
					dto.setGwgj(rs.getString("gwgj"));
					dto.setSfsjt(rs.getString("sfsjt"));//是否双肩挑----
					dto.setSjtgwlb(rs.getString("sjtgwlb")); //双肩挑岗位类别     
					dto.setSjtgwdj(rs.getString("sjtgwdj")); //双肩挑岗位等级     
					dto.setSfzzcsxlzxgz(rs.getString("sfzzcsxlzxgz")); //是否专职从事心理咨询工作     
					dto.setSfcyxlzxzgzs(rs.getString("sfcyxlzxzgzs")); //是否持有心理咨询资格证书    --

					dto.setSfjrqtgw(rs.getString("sfjrqtgw")); //是否兼任其他岗位--(中等职业学校用户)
					dto.setJrgwlb(rs.getString("jrgwlb")); //兼任岗位类别
					dto.setJrgwdj(rs.getString("jrgwdj")); //兼任岗位等级

					dto.setSfzzfdy(rs.getString("sfzzfdy"));//是否为辅导员
					dto.setDzjb(rs.getString("dzjb"));//党政级别
					dto.setPrksny(rs.getString("prksny"));//聘任开始年月
					dto.setDzzw(rs.getString("dzzw"));
					dto.setRzksny(rs.getString("rzksny"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

	//执行修改
	public int updateJzggwprxx(TbBizJzggwprxx gw) {
		int result = 0;
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("gwprId", gw.getGwprId());
			params.put("gwlb", gw.getGwlb());//岗位类别
			params.put("gwgj", gw.getGwgj());//岗位等级
			params.put("dzzw", gw.getDzzw());//党政职务                                                                                                                                    
			params.put("sfsjt", gw.getSfsjt());//是否双肩挑--                                 			
			params.put("sjtgwlb", gw.getSjtgwlb());//双肩挑岗位类别                                 			
			params.put("sjtgwdj", gw.getSjtgwdj());//双肩挑岗位等级
			params.put("sfzzcsxlzxgz", gw.getSfzzcsxlzxgz());//是否专职从事心理咨询工作
			params.put("sfcyxlzxzgzs", gw.getSfcyxlzxzgzs());//是否持有心理咨询资格证书--

			params.put("sfjrqtgw", gw.getSfjrqtgw());//是否兼任其他岗位--(中等职业学校用户)
			params.put("jrgwlb", gw.getJrgwlb());//兼任岗位类别
			params.put("jrgwdj", gw.getJrgwdj());//兼任岗位等级

			params.put("sfzzfdy", gw.getSfzzfdy());//是否为辅导员
			params.put("dzjb", gw.getDzjb());//党政级别
			params.put("prksny", gw.getPrksny());//聘任开始年月
			params.put("rzksny", gw.getRzksny());//任职开始年月
			ISqlElement se = processSql(params, "JZGGWPRXXManagerExt.doUpdateAnswer.update");
			getJdbcTemplate().update(se.getSql(), se.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return result;
	}

	//删除
	public int removeGwpr(String[] ck) {
		int result = 0;
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("ids", ck);
			ISqlElement se = processSql(params, "JZGGWPRXXManagerExt.doDelete.update");
			getJdbcTemplate().update(se.getSql(), se.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return result;
	}

	public String getXd(Map<String, Object> params) {

		try {
			List<TbXxjgb> dtos = this.findList("JZGGWPRXXManagerExt.query.role", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbXxjgb dto = new TbXxjgb();
					dto.setXxjgid(rs.getLong("XXJGID"));
					dto.setSsxd(rs.getString("SSXD"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0).getSsxd();
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

	public List SelectPostGrade(Map<String, Object> params) {
		try {
			List<ZxdwlbZdxbDto> dtos = this.findList("JZGGWPRXXManagerExt.query.gwlbSelct", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					ZxdwlbZdxbDto dto = new ZxdwlbZdxbDto();
					dto.setZdxmc(rs.getString("zdxbm") + "-" + rs.getString("zdxmc"));
					dto.setZdxbm(rs.getString("zdxbm"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("gwlbSelct doSearch() is error!", e);
		}
		return null;
	}

	public List<TbCfgZdxbInfoDto> queryConclusion() {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			List<TbCfgZdxbInfoDto> dtos = this.findList("JZGGWPRXXManagerExt.queryConclusion.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TbCfgZdxbInfoDto dto = new TbCfgZdxbInfoDto();
							dto.setZdxbm(rs.getString("zdxbm"));
							dto.setZdxmc(rs.getString("zdxmc"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

	public List<TbCfgZdxbInfoDto> queryDzzw() {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			List<TbCfgZdxbInfoDto> dtos = this.findList("TbBizJzgsdxxManagerExt.queryConclusion.queryDzzw", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TbCfgZdxbInfoDto dto = new TbCfgZdxbInfoDto();
							dto.setZdxbm(rs.getString("zdxbm"));
							dto.setZdxmc(rs.getString("zdxmc"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

	public List<TbCfgZdxbInfoDto> queryMiddle() {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			List<TbCfgZdxbInfoDto> dtos = this.findList("TbBizJzgsdxxManagerExt.queryConclusion.queryMiddle", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TbCfgZdxbInfoDto dto = new TbCfgZdxbInfoDto();
							dto.setZdxbm(rs.getString("zdxbm"));
							dto.setZdxmc(rs.getString("zdxmc"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}
}
