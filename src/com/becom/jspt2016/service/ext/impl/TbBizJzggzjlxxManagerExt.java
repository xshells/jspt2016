/**
 * TbBizJzggzjlxxManagerExt.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.commons.hibernate.IPage;
import org.nestframework.commons.hibernate.IRowHandler;
import org.nestframework.commons.hibernate.ISqlElement;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.TbBizJzggzjlxxDto;
import com.becom.jspt2016.model.TbBizJzggzjlxx;
import com.becom.jspt2016.service.ext.ITbBizJzggzjlxxManagerExt;

/**
 * 教职工工作经历服务
 *
 * @author   zhangchunming
 * @Date	 2016年10月19日 	 
 */
public class TbBizJzggzjlxxManagerExt extends JdbcRootManager implements ITbBizJzggzjlxxManagerExt {

	protected Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 是否删除(未删除)
	 */
	public String SFSC = "0";

	/**
	 * 是否删除(已删除)
	 */
	public String SC = "1";

	/**
	 * 未报送
	 */
	public String SHZT = "01";

	/**
	 * 工作经历 分页查询
	 * <p>
	 * @param map
	 * @param pageNo
	 * @param pageSize
	 */
	@Override
	public IPage<TbBizJzggzjlxx> doSearch(Map<String, Object> params, int pageNo, int pageSize) {

		try {
			params.put("sfsc", SFSC);
			IPage<TbBizJzggzjlxx> dtos = this.findPage("TbBizJzggzjlxxManagerExt.doSearch.query",
					"TbBizJzggzjlxxManagerExt.doSearch.count", params, pageNo, pageSize,
					new IRowHandler<TbBizJzggzjlxx>() {
						public TbBizJzggzjlxx handleRow(ResultSet rs) {
							TbBizJzggzjlxx dto = new TbBizJzggzjlxx();
							try {
								dto.setRzdwmc(rs.getString("rzdwmc"));
								dto.setRzksny(rs.getString("rzksny"));
								dto.setRzjsny(rs.getString("rzjsny"));
								dto.setDwxzlb(rs.getString("dwxzlb"));
								dto.setRzgw(rs.getString("rzgw"));
								dto.setCjsj(rs.getDate("cjsj"));
								dto.setGzjlId(rs.getLong("gzjl_Id"));
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			System.out.println(dtos.getPageElements());
			return dtos;
		} catch (Exception e) {
			logger.error("TbBizJzgxxjlxxManagerExt doSearch() is error!", e);
		}
		return null;
	}

	/**
	 * 添加工作经历
	 * @see com.becom.jspt2016.service.ext.ITbBizJzggzjlxxManagerExt#add(com.becom.jspt2016.model.TbBizJzggzjlxx)
	 	@param TbBizJzggzjlxx 对象
	 */
	@Override
	public void add(final TbBizJzggzjlxx tbBizJzggzjlxx) {
		StringBuffer sql = new StringBuffer();

		sql.append(
				"insert into TB_BIZ_JZGGZJLXX (GZJL_ID,JSID,RZDWMC,RZKSNY,RZJSNY,DWXZLB,RZGW,SFSC,CJSJ,BSRLB,SHZT,SHSJ,CJR) ")
				.append("values (SEQ_TB_BIZ_JZGGZJLXX.nextval,?,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, tbBizJzggzjlxx.getTbBizJzgjbxx().getJsid());
				ps.setString(2, tbBizJzggzjlxx.getRzdwmc());
				ps.setString(3, tbBizJzggzjlxx.getRzksny());
				ps.setString(4, tbBizJzggzjlxx.getRzjsny());
				ps.setString(5, tbBizJzggzjlxx.getDwxzlb());
				ps.setString(6, tbBizJzggzjlxx.getRzgw());
				ps.setString(7, SFSC);
				ps.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
				//必填项默认填什么？
				ps.setString(9, "4");
				ps.setString(10, SHZT);
				ps.setTimestamp(11, new Timestamp(System.currentTimeMillis()));
				ps.setLong(12, tbBizJzggzjlxx.getTbBizJzgjbxx().getJsid());
			}
		});
	}

	/**
	 * 获取工作经历对象信息
	 * <p>
	 * @param 工作经历id
	 */
	@Override
	public TbBizJzggzjlxxDto get(long gzjlId) {

		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("gzjlid", gzjlId);
			params.put("sfsc", SFSC);
			List<TbBizJzggzjlxxDto> dtos = this.findList("TbBizJzggzjlxxManagerExt.get.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbBizJzggzjlxxDto dto = new TbBizJzggzjlxxDto();
					dto.setGzjlId(rs.getLong("gzjlId"));
					dto.setJsid(rs.getLong("jsid"));
					dto.setRzdwmc(rs.getString("rzdwmc"));
					dto.setRzksny(rs.getString("rzksny"));
					dto.setRzjsny(rs.getString("rzjsny"));
					dto.setDwxzlb(rs.getString("dwxzlb"));
					dto.setRzgw(rs.getString("rzgw"));

					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("TbBizJzgxxjlxxManagerExt get() is error!", e);
		}

		return null;

	}

	/**
	 * 编辑
	 * @see com.becom.jspt2016.service.ext.ITbBizJzggzjlxxManagerExt#edit(java.util.Map)
	 */
	@Override
	public void edit(Map<String, Object> params) {
		try {
			ISqlElement se = processSql(params, "TbBizJzggzjlxxManagerExt.edit.update");
			getJdbcTemplate().update(se.getSql(), se.getParams());
		} catch (Exception e) {
			logger.error("TbBizJzggzjlxxManagerExt edit() is error!", e);
		}
	}

	/**
	 *删除 
	 * @see com.becom.jspt2016.service.ext.ITbBizJzggzjlxxManagerExt#delete(long[])
	 */
	@Override
	public int deleteGz(String[] ids) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ids", ids);
		params.put("sfsc", SC);
		try {
			ISqlElement processSql = processSql(params, "TbBizJzggzjlxxManagerExt.delete.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = -1;
			logger.error("TbBizJzggzjlxxManagerExt delete() is error!", e);
		}
		return result;

	}
}
