/**
 * TbBizJzgxxjlxxManagerExt.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.commons.hibernate.IPage;
import org.nestframework.commons.hibernate.IRowHandler;
import org.nestframework.commons.hibernate.ISqlElement;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.TbBizJzgxxjlxxDto;
import com.becom.jspt2016.dto.TbCfgZdxbInfoDto;
import com.becom.jspt2016.model.TbBizJzgxxjlxx;
import com.becom.jspt2016.service.ext.ITbBizJzgxxjlxxManagerExt;

/**
 * 教职工学习经历信息管理
 *
 * @author   zhangchunming
 * @Date	 2016年10月17日 	 
 */
public class TbBizJzgxxjlxxManagerExt extends JdbcRootManager implements ITbBizJzgxxjlxxManagerExt {

	protected Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 是否删除(未删除)
	 */
	public String SFSC = "0";

	/**
	 * 是否删除(已删除)
	 */
	public String SC = "1";

	/**
	 * 未报送
	 */
	public String SHZT = "01";

	/**
	 * 高校所属学段
	 */
	public String GXSSXD = "10";
	/**
	 * 高职所属学段
	 */
	public String GZSSXD = "20";

	/**
	 * 查询教师学习经历
	 * 
	 * @see com.becom.jspt2016.service.ext.ITbBizJzgxxjlxxManagerExt#doSearch(java.util.Map, int, int)
	 */
	@Override
	public IPage<TbBizJzgxxjlxx> doSearch(Map<String, Object> params, int pageNo, int pageSize) {

		try {
			params.put("sfsc", SFSC);
			IPage<TbBizJzgxxjlxx> dtos = this.findPage("TbBizJzgxxjlxxManagerExt.doSearch.query",
					"TbBizJzgxxjlxxManagerExt.doSearch.count", params, pageNo, pageSize,
					new IRowHandler<TbBizJzgxxjlxx>() {
						public TbBizJzgxxjlxx handleRow(ResultSet rs) {
							TbBizJzgxxjlxx dto = new TbBizJzgxxjlxx();
							try {
								dto.setHdxl(rs.getString("hdxl"));
								dto.setHdxldgjdq(rs.getString("hdxldgjdq"));
								dto.setHdxldyxhjg(rs.getString("hdxldyxhjg"));
								dto.setSfsflzy(rs.getString("sfsflzy"));
								dto.setSxzy(rs.getString("sxzy"));
								dto.setRxny(rs.getString("rxny"));
								dto.setByny(rs.getString("byny"));
								dto.setXwcc(rs.getString("xwcc"));
								dto.setXwmc(rs.getString("xwmc"));
								dto.setHdxwdgjdq(rs.getString("hdxwdgjdq"));
								dto.setHdxwdyxhjg(rs.getString("hdxwdyxhjg"));
								dto.setXwsyny(rs.getString("xwsyny"));
								dto.setXxfs(rs.getString("xxfs"));
								dto.setZxdwlb(rs.getString("zxdwlb"));
								dto.setCjsj(rs.getDate("cjsj"));
								dto.setShzt(rs.getString("shzt"));
								dto.setXlId(rs.getLong("xl_Id"));
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			System.out.println(dtos.getPageElements());
			return dtos;
		} catch (Exception e) {
			logger.error("TbBizJzgxxjlxxManagerExt doSearch() is error!", e);
		}
		return null;
	}

	/**
	 * 添加
	 * 
	 * @see com.becom.jspt2016.service.ext.ITbBizJzgxxjlxxManagerExt#add(com.becom.jspt2016.model.TbBizJzgxxjlxx)
	 */
	@Override
	public void add(final TbBizJzgxxjlxx tbBizJzgxxjlxx, final String ssxd) {
		StringBuffer sql = new StringBuffer();
		String sqlbegin = "";
		String sqlend = "";
		/*	if (ssxd.equals(GXSSXD) || ssxd.equals(GXSSXD)) {
				sqlbegin = "insert into TB_BIZ_JZGXXJLXX (XL_ID,JSID,HDXL,HDXLDGJDQ,HDXLDYXHJG,SXZY,RXNY,BYNY,XWCC,XWMC,HDXWDGJDQ,HDXWDYXHJG,XWSYNY,XXFS,ZXDWLB,CJSJ,SFSC,BSRLB,SHZT,SHSJ)";
				sqlend = "values (SEQ_TB_BIZ_JZGXXJLXX.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			} else {*/

		sqlbegin = "insert into TB_BIZ_JZGXXJLXX (XL_ID,JSID,HDXL,HDXLDGJDQ,HDXLDYXHJG,SXZY,RXNY,BYNY,XWCC,XWMC,HDXWDGJDQ,HDXWDYXHJG,XWSYNY,XXFS,ZXDWLB,CJSJ,SFSC,BSRLB,SHZT,SHSJ,SFSFLZY,CJR)";

		sqlend = "values (SEQ_TB_BIZ_JZGXXJLXX.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		//}
		sql.append(sqlbegin).append(sqlend);
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, tbBizJzgxxjlxx.getTbBizJzgjbxx().getJsid());
				ps.setString(2, tbBizJzgxxjlxx.getHdxl());
				ps.setString(3, tbBizJzgxxjlxx.getHdxldgjdq());
				ps.setString(4, tbBizJzgxxjlxx.getHdxldyxhjg());
				ps.setString(5, tbBizJzgxxjlxx.getSxzy());
				ps.setString(6, tbBizJzgxxjlxx.getRxny());
				ps.setString(7, tbBizJzgxxjlxx.getByny());
				ps.setString(8, tbBizJzgxxjlxx.getXwcc());
				ps.setString(9, tbBizJzgxxjlxx.getXwmc());
				ps.setString(10, tbBizJzgxxjlxx.getHdxwdgjdq());
				ps.setString(11, tbBizJzgxxjlxx.getHdxwdyxhjg());
				ps.setString(12, tbBizJzgxxjlxx.getXwsyny());
				ps.setString(13, tbBizJzgxxjlxx.getXxfs());
				ps.setString(14, tbBizJzgxxjlxx.getZxdwlb());
				ps.setTimestamp(15, new Timestamp(System.currentTimeMillis()));
				ps.setString(16, SFSC);
				//不可为空的。。。。
				ps.setString(17, "4");
				ps.setString(18, SHZT);
				ps.setTimestamp(19, new Timestamp(System.currentTimeMillis()));
				//if (!ssxd.equals(GXSSXD) && !ssxd.equals(GXSSXD)) {
				ps.setString(20, tbBizJzgxxjlxx.getSfsflzy());
				ps.setLong(21, tbBizJzgxxjlxx.getTbBizJzgjbxx().getJsid());
				//}
				ps.setString(21, String.valueOf(tbBizJzgxxjlxx.getTbBizJzgjbxx().getJsid()));
			}
		});

	}

	/**
	 * 删除
	 * @see com.becom.jspt2016.service.ext.ITbBizJzgxxjlxxManagerExt#delete(long[])
	 */
	@Override
	public int delete(String[] ids) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ids", ids);
		params.put("sfsc", SC);
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgxxjlxxManagerExt.delete.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgxxjlxxManagerExt delete() is error!", e);
		}
		return result;

	}

	/**
	 * 去编辑
	 * @see com.becom.jspt2016.service.ext.ITbBizJzgxxjlxxManagerExt#get(java.lang.String)
	 */
	@Override
	public TbBizJzgxxjlxxDto get(long xlId) {

		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("xlId", xlId);
			List<TbBizJzgxxjlxxDto> dtos = this.findList("TbBizJzgxxjlxxManagerExt.get.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbBizJzgxxjlxxDto dto = new TbBizJzgxxjlxxDto();
					dto.setXlId(rs.getLong("xlId"));
					dto.setHdxl(rs.getString("hdxl"));
					dto.setHdxldgjdq(rs.getString("hdxldgjdq"));
					dto.setHdxldyxhjg(rs.getString("hdxldyxhjg"));
					dto.setSxzy(rs.getString("sxzy"));
					dto.setRxny(rs.getString("rxny"));
					dto.setByny(rs.getString("byny"));
					dto.setXwcc(rs.getString("xwcc"));
					dto.setXwmc(rs.getString("xwmc"));
					dto.setHdxwdgjdq(rs.getString("hdxwdgjdq"));
					dto.setHdxwdyxhjg(rs.getString("hdxwdyxhjg"));
					dto.setXwsyny(rs.getString("xwsyny"));
					dto.setXxfs(rs.getString("xxfs"));
					dto.setZxdwlb(rs.getString("zxdwlb"));
					dto.setSfsflzy(rs.getString("sfsflzy"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("TbBizJzgxxjlxxManagerExt get() is error!", e);
		}

		return null;

	}

	/**
	 * 编辑
	 * @see com.becom.jspt2016.service.ext.ITbBizJzgxxjlxxManagerExt#edit(com.becom.jspt2016.model.TbBizJzgxxjlxx)
	 */
	@Override
	public void edit(Map<String, Object> params, String ssxd) {
		String sql = "";
		if (ssxd.equals(GXSSXD) || ssxd.equals(GZSSXD)) {
			sql = "TbBizJzgxxjlxxManagerExt.edit.updateGXGZ";
		} else {
			sql = "TbBizJzgxxjlxxManagerExt.edit.updateElse";
		}
		try {
			ISqlElement se = processSql(params, sql);
			getJdbcTemplate().update(se.getSql(), se.getParams());
		} catch (Exception e) {
			logger.error("TbBizJzgxxjlxxManagerExt edit() is error!", e);
		}

	}

	/**
	 * 查找最高学历
	 *
	 * @param jsid 教师id
	 * @return 学习经历dto
	 */
	public TbBizJzgxxjlxxDto findLastXl(long jsid) {
		return null;
	}

	/**
	 * 查找最高学位
	 *
	 * @param jsid 教师id
	 * @return 学习经历dto
	 */
	public TbBizJzgxxjlxxDto findLastXW(long jsid) {
		return null;
	}

	/**
	 * 
	 * 查询学位名称集合
	 *
	 * @return list
	 */
	public List<TbCfgZdxbInfoDto> queryConclusion(String zdxbm) {
		try {

			Map<String, Object> param = new HashMap<String, Object>();
			//params.put("zdxbm", "like " + zdxbm + "%");
			//			params.put("zdxbm", zdxbm + "%");
			if ("2".equals(zdxbm) || "2".equals(zdxbm)) {
				param.put("zdxbm", "2%");
			} else if ("3".equals(zdxbm)) {
				param.put("zdxbm", "3%");
			} else if ("4".equals(zdxbm)) {
				param.put("zdxbm", "4%");
			}
			param.put("nozdxbm", zdxbm);
			List<TbCfgZdxbInfoDto> dtos = this.findList("TbBizJzgxxjlxxManagerExt.queryConclusion.query", param,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TbCfgZdxbInfoDto dto = new TbCfgZdxbInfoDto();
							dto.setZdxbm(rs.getString("zdxbm"));
							dto.setZdxmc(rs.getString("zdxbm") + "-" + rs.getString("zdxmc"));
							dto.setFzdx(rs.getString("fzdx"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

	public List<TbCfgZdxbInfoDto> queryConclusion() {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			List<TbCfgZdxbInfoDto> dtos = this.findList("TbBizJzgxxjlxxManagerExt.queryConclusion.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TbCfgZdxbInfoDto dto = new TbCfgZdxbInfoDto();
							dto.setZdxbm(rs.getString("zdxbm"));
							dto.setZdxmc(rs.getString("zdxmc"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}
}
