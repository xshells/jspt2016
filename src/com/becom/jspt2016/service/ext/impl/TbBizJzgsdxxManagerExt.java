/**
 * TbBizJzgsdxxManager.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.commons.hibernate.IPage;
import org.nestframework.commons.hibernate.IRowHandler;
import org.nestframework.commons.hibernate.ISqlElement;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.TbBizJzgsdxxInfoDto;
import com.becom.jspt2016.dto.TbCfgZdxbInfoDto;
import com.becom.jspt2016.model.TbBizJzgsdcfxx;
import com.becom.jspt2016.model.TbBizJzgsdkhxx;
import com.becom.jspt2016.service.ext.ITbBizJzgsdxxManagerExt;
import com.becom.jspt2016.service.impl.TbBizJzgsdcfxxManager;
import com.becom.jspt2016.service.impl.TbBizJzgsdkhxxManager;

/**
 * 师德信息实现类
 *
 * @author   shanjigang
 * @Date	 2016年10月16日 	 
 */
public class TbBizJzgsdxxManagerExt extends JdbcRootManager implements ITbBizJzgsdxxManagerExt {
	protected Logger logger = Logger.getLogger(this.getClass());
	public TbBizJzgsdkhxxManager tbBizJzgsdkhxxManager;
	public TbBizJzgsdcfxxManager tbBizJzgsdcfxxManager;

	//查询教师考核信息
	public IPage<TbBizJzgsdxxInfoDto> doQuery(Map<String, Object> params, int pageNo, int pageSize) {
		try {

			IPage<TbBizJzgsdxxInfoDto> dtos = this.findPage("TbBizJzgsdxxManagerExt.doQuery.query",
					"TbBizJzgsdxxManagerExt.doQuery.count", params, pageNo, pageSize,
					new IRowHandler<TbBizJzgsdxxInfoDto>() {
						public TbBizJzgsdxxInfoDto handleRow(ResultSet rs) {
							TbBizJzgsdxxInfoDto dto = new TbBizJzgsdxxInfoDto();
							try {
								dto.setJsid(rs.getLong("jsid"));
								dto.setKhid(rs.getLong("khid"));
								dto.setKhny(rs.getString("khny"));
								dto.setKhjl(rs.getString("khjl"));
								dto.setShzt(rs.getString("shzt"));
								dto.setCjsj(rs.getDate("cjsj"));
								dto.setSdkhdwmc(rs.getString("sdkhdwmc"));
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			return dtos;
		} catch (Exception e) {
			logger.error("TeacherMoralityManagerExt doSearch() is error!", e);
		}
		return null;

	}

	//编辑教师考核信息
	public void editKaohe(long khid, String khny, String khjl, String sdkhdwmc) {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("khid", khid);
			params.put("khny", khny);
			params.put("khjl", khjl);
			params.put("sdkhdwmc", sdkhdwmc);
			ISqlElement se = processSql(params, "TbBizJzgsdxxManagerExt.editKaohe.update");
			getJdbcTemplate().update(se.getSql(), se.getParams());

		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
	}

	//编辑教师处分信息
	public void editChufen(long cfid, String cflb, String cfyy, String ccfsrq) {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("cfid", cfid);
			params.put("cflb", cflb);
			params.put("cfyy", cfyy);
			params.put("ccfsrq", ccfsrq);
			ISqlElement se = processSql(params, "TbBizJzgsdxxManagerExt.editChufen.update1");
			getJdbcTemplate().update(se.getSql(), se.getParams());

		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
	}

	//删除教师考核信息
	public void delKaohe(String[] ids) {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("ids", ids);
			params.put("sfsc", 1);
			ISqlElement se = processSql(params, "TbBizJzgsdxxManagerExt.delKaohe.del1");
			getJdbcTemplate().update(se.getSql(), se.getParams());

		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
	}

	//删除教师处分信息
	public void delChufen(String[] ids) {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("ids", ids);
			params.put("sfsc", 1);
			ISqlElement se = processSql(params, "TbBizJzgsdxxManagerExt.delChufen.del1");
			getJdbcTemplate().update(se.getSql(), se.getParams());

		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
	}

	//修改教师考核信息
	public TbBizJzgsdxxInfoDto editTeacherMorality(long jsid, long khid, long cfid) {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsid", jsid);
			params.put("khid", khid);
			List<TbBizJzgsdxxInfoDto> dtos = this.findList("TbBizJzgsdxxManagerExt.editTeacherMorality.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TbBizJzgsdxxInfoDto dto = new TbBizJzgsdxxInfoDto();
							dto.setJsid(rs.getLong("jsid"));
							dto.setKhid(rs.getLong("khid"));
							dto.setKhny(rs.getString("khny"));
							dto.setKhjl(rs.getString("khjl"));
							dto.setSdkhdwmc(rs.getString("sdkhdwmc"));
							//							dto.setCjsj(rs.getDate("cjsj"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

	//添加教师考核信息
	public void addTeacherMorality(final TbBizJzgsdkhxx kh, final TbBizJzgsdcfxx cf) {
		StringBuffer sql = new StringBuffer();
		sql.append("insert into tb_biz_jzgsdkhxx(SDKH_ID,JSID,BSRLB,SHZT,SHSJ,SFSC,CJSJ,SDKHNY,SDKHJL,sdkhdwmc,cjr) ")
				.append("values (SEQ_TB_BIZ_JZGSDKHXX.nextval,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {

				Date currentTime = new Date();
				ps.setString(1, String.valueOf(kh.getTbBizJzgjbxx().getJsid()));
				ps.setString(2, "4");
				ps.setString(3, "01");
				ps.setTimestamp(4, new java.sql.Timestamp(kh.getShsj().getTime()));
				ps.setString(5, "0");
				ps.setTimestamp(6, new java.sql.Timestamp(kh.getCjsj().getTime()));
				ps.setString(7, kh.getSdkhny());
				ps.setString(8, kh.getSdkhjl());
				ps.setString(9, kh.getSdkhdwmc());
				ps.setString(10, kh.getCjr());
			}
		});
	}

	// 删除教师考核信息

	public void delTeacherMorality(String[] ids) {
		delKaohe(ids);
	}

	// 查询师德考核记录下拉框
	public List<TbCfgZdxbInfoDto> queryConclusion() {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			List<TbCfgZdxbInfoDto> dtos = this.findList("TbBizJzgsdxxManagerExt.queryConclusion.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TbCfgZdxbInfoDto dto = new TbCfgZdxbInfoDto();
							dto.setZdxbm(rs.getString("zdxbm"));
							dto.setZdxmc(rs.getString("zdxmc"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

	//添加教师处分信息
	public void addPunishmentInfo(final TbBizJzgsdcfxx cf) {
		StringBuffer sql1 = new StringBuffer();
		sql1.append("insert into tb_biz_jzgsdcfxx (SDCF_ID,JSID,BSRLB,SHZT,SHSJ,SFSC,CJSJ,CFLB,CFYY,CCFSRQ,cjr)")
				.append("values (SEQ_TB_BIZ_JZGSDCFXX.nextval,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql1.toString(), new PreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				Date currentTime = new Date();
				ps.setString(1, String.valueOf(cf.getTbBizJzgjbxx().getJsid()));
				ps.setString(2, "4");
				ps.setString(3, "01");
				ps.setTimestamp(4, new java.sql.Timestamp(cf.getShsj().getTime()));
				ps.setString(5, "0");
				ps.setTimestamp(6, new java.sql.Timestamp(cf.getCjsj().getTime()));
				ps.setString(7, cf.getCflb());
				ps.setString(8, cf.getCfyy());
				ps.setString(9, cf.getCcfsrq());
				ps.setString(10, cf.getCjr());
			}
		});
	}

	//查询教师处分信息
	public TbBizJzgsdxxInfoDto editPunishmentInfo(long jsid, long cfid) {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsid", jsid);
			params.put("cfid", cfid);
			List<TbBizJzgsdxxInfoDto> dtos = this.findList("TbBizJzgsdxxManagerExt.editPunishmentInfo.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {

							TbBizJzgsdxxInfoDto dto = new TbBizJzgsdxxInfoDto();
							dto.setCfid(rs.getLong("cfid"));
							dto.setCflb(rs.getString("cflb"));
							dto.setCfyy(rs.getString("cfyy"));
							dto.setCcfsrq(rs.getString("ccfsrq"));
							//							dto.setCjsj(rs.getDate("cjsj"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

	//删除处分信息
	public void delPunishment(String[] ids) {
		delChufen(ids);
	}

	//查询教师考核信息
	public IPage<TbBizJzgsdxxInfoDto> doQuerycf(Map<String, Object> params, int pageNo, int pageSize) {
		try {

			IPage<TbBizJzgsdxxInfoDto> dtos = this.findPage("TbBizJzgsdxxManagerExt.doQuerycf.query",
					"TbBizJzgsdxxManagerExt.doQuerycf.count", params, pageNo, pageSize,
					new IRowHandler<TbBizJzgsdxxInfoDto>() {
						public TbBizJzgsdxxInfoDto handleRow(ResultSet rs) {
							TbBizJzgsdxxInfoDto dto = new TbBizJzgsdxxInfoDto();
							try {
								dto.setCfid(rs.getLong("cfid"));
								dto.setCflb(rs.getString("cflb"));
								dto.setCfyy(rs.getString("cfyy"));
								dto.setCcfsrq(rs.getString("ccfsrq"));
								dto.setCjsj(rs.getDate("cjsj"));
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			return dtos;
		} catch (Exception e) {
			logger.error("TeacherMoralityManagerExt doSearch() is error!", e);
		}
		return null;

	}

}
