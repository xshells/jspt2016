/**
 * TbBizJzgsdxxManager.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.commons.hibernate.IPage;
import org.nestframework.commons.hibernate.IRowHandler;
import org.nestframework.commons.hibernate.ISqlElement;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.TbBizJzgjyjxxxInfoDto;
import com.becom.jspt2016.dto.TbCfgZdxbInfoDto;
import com.becom.jspt2016.model.TbBizJzgjyjxxx;
import com.becom.jspt2016.model.TbXxjgb;
import com.becom.jspt2016.service.ext.ITbBizJzgjyjxxxManagerExt;

/**
 * 师德信息实现类
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   shanjigang
 * @Date	 2016年10月16日 	 
 */
public class TbBizJzgjyjxxxManagerExt extends JdbcRootManager implements ITbBizJzgjyjxxxManagerExt {
	protected Logger logger = Logger.getLogger(this.getClass());

	public IPage<TbBizJzgjyjxxxInfoDto> doQuery(Map<String, Object> params, int pageNo, int pageSize) {
		try {
			IPage<TbBizJzgjyjxxxInfoDto> dtos = this.findPage("TbBizJzgjyjxxxManagerExt.doQuery.query",
					"TbBizJzgjyjxxxManagerExt.doQuery.count", params, pageNo, pageSize,
					new IRowHandler<TbBizJzgjyjxxxInfoDto>() {
						public TbBizJzgjyjxxxInfoDto handleRow(ResultSet rs) {
							TbBizJzgjyjxxxInfoDto dto = new TbBizJzgjyjxxxInfoDto();
							try {
								dto.setJyid(rs.getInt("jyid"));
								dto.setJsid(rs.getInt("jsid"));
								dto.setXn(rs.getString("xn"));
								dto.setXq(rs.getString("xq"));
								dto.setRkzk(rs.getString("rkzk"));
								dto.setSfwbkssk(rs.getString("sfwbkssk"));
								dto.setDslb(rs.getString("dslb"));
								dto.setXzycsxkly(rs.getString("xzycsxkly"));
								dto.setXnbzkkcjxkss(rs.getString("xnbzkkcjxkss"));
								dto.setCjsj(rs.getDate("cjsj"));
								dto.setRkkclb(rs.getString("rkkclb"));
								dto.setShzt(rs.getString("shzt"));
								dto.setRjxd(rs.getString("rjxd"));
								dto.setRjkc(rs.getString("rjkc"));
								/*String rjkczxx = rs.getString("rjkc").trim();
								String rjck = getRjkc(rjkczxx);
								dto.setRjkc(rjck);*/
								dto.setPjzks(rs.getString("pjzks"));
								dto.setJrgz(rs.getString("xzycsxkly"));
								//dto.setRkxklb(rs.getString("rkxklb"));
								if (!"".equals(rs.getString("rkxklb")) && rs.getString("rkxklb") != null) {
									String rkxklb = rs.getString("rkxklb").trim();
									String[] lb = rkxklb.split(",");
									StringBuilder sb = new StringBuilder();
									for (int i = 0; i < lb.length; i++) {
										if (i != lb.length - 1) {
											sb.append(getRkxklb(lb[i]));
											sb.append(" ");
										} else {
											sb.append(getRkxklb(lb[i]));
										}
									}
									//String rklb = sb.toString();
									dto.setRkxklb(sb.toString());
								}

								dto.setJrgz(rs.getString("jrgz"));
								dto.setRjkc(rs.getString("rjkc"));
								dto.setRjxd(rs.getString("rjxd"));
								dto.setRkkclb(rs.getString("rkkclb"));
								dto.setPjzks(rs.getString("pjzks"));
								String rkkclb1 = rs.getString("rkkclb");
								if (!"".equals(rkkclb1) && rkkclb1 != null) {
									String[] arr = rkkclb1.split(",");
									dto.setRkkclblist(arr);
								}

							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			return dtos;
		} catch (Exception e) {
			logger.error("TeacherMoralityManagerExt doSearch() is error!", e);
		}
		return null;

	}

	public void addTeacherMorality(final TbBizJzgjyjxxx jyjx, final String jsid) {
		StringBuffer sql = new StringBuffer();
		sql.append(
				"insert into tb_biz_jzgjyjxxx (JYJX_ID ,JSID,BSRLB,SHZT,SHSJ,SHJG,SFSC,CJSJ,XN,XQ,SFWBKSSK,DSLB,RKZK,XZYCSXKLY,XNBZKKCJXKSS,rkxklb,jrgz,rjkc,rjxd,rkkclb,pjzks,cjr)")
				.append("values (SEQ_TB_BIZ_JZGJYJXXX.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {

				Date currentTime = new Date();
				ps.setString(1, jsid);
				ps.setString(2, "4");//jyjx.getBsrlb()
				ps.setString(3, "01");//jyjx.getShzt()
				ps.setTimestamp(4, new java.sql.Timestamp(jyjx.getShsj().getTime()));
				ps.setString(5, "");
				ps.setString(6, jyjx.getSfsc());
				ps.setTimestamp(7, new java.sql.Timestamp(jyjx.getCjsj().getTime()));
				ps.setString(8, jyjx.getXn());
				ps.setString(9, jyjx.getXq());
				ps.setString(10, jyjx.getSfwbkssk());
				ps.setString(11, jyjx.getDslb());
				ps.setString(12, jyjx.getRkzk());
				ps.setString(13, jyjx.getXzycsxkly());
				ps.setString(14, jyjx.getXnbzkkcjxkss());
				ps.setString(15, jyjx.getRkxklb());
				ps.setString(16, jyjx.getJrgz());
				ps.setString(17, jyjx.getRjkc());
				ps.setString(18, jyjx.getRjxd());
				ps.setString(19, jyjx.getRkkclb());
				ps.setString(20, jyjx.getPjzks());
				ps.setString(21, jyjx.getCjr());
			}
		});
	}

	public void editEducationTeach(String xn, String xq, String dslb, String xzycsxkly, String sfwbkssk, String rkzk,
			String xnbzkkcjxkss, String cjsj, int jyid, String rkxklb, String jrgz, String rjkc, String rjxd,
			String rkkclb, String pjzks) {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jyid", jyid);
			params.put("xn", xn);
			params.put("xq", xq);
			params.put("dslb", dslb);
			params.put("xzycsxkly", xzycsxkly);
			params.put("sfwbkssk", sfwbkssk);
			params.put("rkzk", rkzk);
			params.put("xnbzkkcjxkss", xnbzkkcjxkss);
			params.put("xq", xq);
			params.put("dslb", dslb);
			params.put("xzycsxkly", xzycsxkly);
			params.put("sfwbkssk", sfwbkssk);
			params.put("rkzk", rkzk);
			params.put("xnbzkkcjxkss", xnbzkkcjxkss);
			params.put("rkxklb", rkxklb);
			params.put("jrgz", jrgz);
			params.put("rjkc", rjkc);
			params.put("rkkclb", rkkclb);
			params.put("pjzks", pjzks);
			params.put("rjxd", rjxd);
			ISqlElement se = processSql(params, "TbBizJzgjyjxxxManagerExt.editEducationTeach.update");
			getJdbcTemplate().update(se.getSql(), se.getParams());

		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
	}

	public void delEducationTeach(String[] ids) {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("ids", ids);
			params.put("sfsc", 1);
			ISqlElement se = processSql(params, "TbBizJzgjyjxxxManagerExt.delEducationTeach.del");
			getJdbcTemplate().update(se.getSql(), se.getParams());

		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
	}

	public TbBizJzgjyjxxxInfoDto editEduction(int jyid) {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jyid", jyid);
			List<TbBizJzgjyjxxxInfoDto> dtos = this.findList("TbBizJzgjyjxxxManagerExt.editEduvationTeach.query",
					params, new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TbBizJzgjyjxxxInfoDto dto = new TbBizJzgjyjxxxInfoDto();
							dto.setJyid(rs.getInt("jyid"));
							dto.setJsid(rs.getInt("jsid"));
							dto.setXn(rs.getString("xn"));
							dto.setXq(rs.getString("xq"));
							dto.setRkzk(rs.getString("rkzk"));
							dto.setSfwbkssk(rs.getString("sfwbkssk"));
							dto.setDslb(rs.getString("dslb"));
							dto.setXzycsxkly(rs.getString("xzycsxkly"));
							dto.setXnbzkkcjxkss(rs.getString("xnbzkkcjxkss"));
							if (!"".equals(rs.getString("rkxklb")) && rs.getString("rkxklb") != null) {
								String rkxklb = rs.getString("rkxklb").trim();
								String[] lb = rkxklb.split(",");
								StringBuilder sb = new StringBuilder();
								for (int i = 0; i < lb.length; i++) {
									if (i != lb.length - 1) {
										sb.append(lb[i]);
										sb.append(getRkxklb(lb[i]));
										sb.append(",");
									} else {
										sb.append(lb[i]);
										sb.append(getRkxklb(lb[i]));
									}
								}
								//String rklb = sb.toString();
								dto.setRkxklb(sb.toString());
							}
							//							dto.setRkxklb(rs.getString("rkxklb"));
							dto.setJrgz(rs.getString("jrgz"));
							dto.setRjkc(rs.getString("rjkc"));
							dto.setRjxd(rs.getString("rjxd"));
							dto.setRkkclb(rs.getString("rkkclb"));
							dto.setPjzks(rs.getString("pjzks"));
							String rkkclb1 = rs.getString("rkkclb");
							if (!"".equals(rkkclb1) && rkkclb1 != null) {
								String[] arr = rkkclb1.split(",");
								dto.setRkkclblist(arr);
							}
							//							dto.setCjsj(rs.getDate("cjsj"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

	public List<TbCfgZdxbInfoDto> queryConclusion() {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			List<TbCfgZdxbInfoDto> dtos = this.findList("TbBizJzgsdxxManagerExt.queryConclusion.querygx", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TbCfgZdxbInfoDto dto = new TbCfgZdxbInfoDto();
							dto.setZdxbm(rs.getString("zdxbm"));
							dto.setZdxmc(rs.getString("zdxmc"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

	/**
	 * 
	 * 查询教育教学教学领域高校列表
	 *
	 * @return list
	 */
	public List<TbCfgZdxbInfoDto> searchData() {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			List<TbCfgZdxbInfoDto> dtos = this.findList("TbBizJzgjyjxxxManagerExt.searchData.querygx", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TbCfgZdxbInfoDto dto = new TbCfgZdxbInfoDto();
							dto.setFzdx(rs.getString("fzdx"));
							//							dto.setZdbs(rs.getString("zdbs"));
							dto.setZdxbm(rs.getString("zdxbm"));
							dto.setZdxmc(rs.getString("zdxmc"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

	/**
	 * 
	 * 查询教育教学教学领域高职列表
	 *
	 * @return list
	 */
	public List<TbCfgZdxbInfoDto> searchDatagz() {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			List<TbCfgZdxbInfoDto> dtos = this.findList("TbBizJzgjyjxxxManagerExt.searchData.querygz", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TbCfgZdxbInfoDto dto = new TbCfgZdxbInfoDto();
							dto.setFzdx(rs.getString("fzdx"));
							//							dto.setZdbs(rs.getString("zdbs"));
							dto.setZdxbm(rs.getString("zdxbm"));
							dto.setZdxmc(rs.getString("zdxmc"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

	public String getXd(Map<String, Object> params) {

		try {
			List<TbXxjgb> dtos = this.findList("TbBizJzgjyjxxxManagerExt.getXd.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbXxjgb dto = new TbXxjgb();
					dto.setXxjgid(rs.getLong("XXJGID"));//机构ID
					dto.setSsxd(rs.getString("SSXD"));//所属学段
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0).getSsxd();
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

	/**
	 * 
	 * 查询教育教学教学任课学科领域
	 *
	 * @return list
	 */
	public List<TbCfgZdxbInfoDto> searchDataxkly() {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			List<TbCfgZdxbInfoDto> dtos = this.findList("TbBizJzgjyjxxxManagerExt.searchDatarkxklb.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TbCfgZdxbInfoDto dto = new TbCfgZdxbInfoDto();
							dto.setFzdx(rs.getString("fzdx"));
							//							dto.setZdbs(rs.getString("zdbs"));
							dto.setZdxbm(rs.getString("zdxbm"));
							dto.setZdxmc(rs.getString("zdxmc"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

	/**
	 * 
	 * 查询教育教学任教课程中小学列表
	 *
	 * @return list
	 */
	public List<TbCfgZdxbInfoDto> searchDatarjkczxx() {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			List<TbCfgZdxbInfoDto> dtos = this.findList("TbBizJzgjyjxxxManagerExt.searchDatarjkczxx.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TbCfgZdxbInfoDto dto = new TbCfgZdxbInfoDto();
							dto.setFzdx(rs.getString("fzdx"));
							//							dto.setZdbs(rs.getString("zdbs"));
							dto.setZdxbm(rs.getString("zdxbm"));
							dto.setZdxmc(rs.getString("zdxmc"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

	/**
	 * 
	 * 查询教育教学任教课程特殊学校列表
	 *
	 * @return list
	 */
	public List<TbCfgZdxbInfoDto> searchDatarjkcts() {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			List<TbCfgZdxbInfoDto> dtos = this.findList("TbBizJzgjyjxxxManagerExt.searchDatarjkcts.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TbCfgZdxbInfoDto dto = new TbCfgZdxbInfoDto();
							dto.setFzdx(rs.getString("fzdx"));
							//							dto.setZdbs(rs.getString("zdbs"));
							dto.setZdxbm(rs.getString("zdxbm"));
							dto.setZdxmc(rs.getString("zdxmc"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

	//任课学科类别
	public String getRkxklb(String rkxklb) {

		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("rkxklb", rkxklb);
			List<TbXxjgb> dtos = this.findList("TbBizJzgjyjxxxManagerExt.getRkxklb.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbXxjgb dto = new TbXxjgb();
					dto.setSsxd(rs.getString("zdxmc"));//字典项名称
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0).getSsxd();
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

	//任教课程中小学
	public String getRjkc(String rjkc) {

		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("rjkc", rjkc);
			List<TbXxjgb> dtos = this.findList("TbBizJzgjyjxxxManagerExt.getRjkc.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbXxjgb dto = new TbXxjgb();
					dto.setSsxd(rs.getString("zdxmc"));//字典项名称
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0).getSsxd();
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}

	//任教课程特教
	public String getRjkctj(String rjkc) {

		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("rjkc", rjkc);
			List<TbXxjgb> dtos = this.findList("TbBizJzgjyjxxxManagerExt.getRjkctj.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbXxjgb dto = new TbXxjgb();
					dto.setSsxd(rs.getString("zdxmc"));//字典项名称
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0).getSsxd();
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return null;
	}
}
