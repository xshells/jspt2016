/**
 * LoginUserManagerExt.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.TbJzgxxshLogDto;
import com.becom.jspt2016.dto.TeacherNumDto;
import com.becom.jspt2016.dto.UserTodoDto;
import com.becom.jspt2016.model.SysFunc;
import com.becom.jspt2016.model.SysRole;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbJzgxxsh;
import com.becom.jspt2016.model.TbXxjgb;
import com.becom.jspt2016.service.ext.ILoginUserManagerExt;
import com.becom.jspt2016.service.ext.ISysuserManageExt;
import com.becom.jspt2016.service.ext.ITbJzgxxshManagerExt;

/**
 * 用户权限
 * <p>
 *
 * @author   YuChengCheng
 * @auther   Zhangchunming
 * @Date	 2016年10月16日
 * @Date	 2016年10月31日  	 
 */
public class LoginUserManagerExt extends JdbcRootManager implements ILoginUserManagerExt {
	protected Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 学校机构用户类型
	 */
	public static final String XXJGTYPE = "3";
	/**
	 * 区县用户类型
	 */
	public static final String QXTYPE = "2";
	/**
	 * 市用户类型
	 */
	public static final String STYPE = "1";
	/**
	 * 未删除状态
	 */
	public static final String SFSC = "0";
	/**
	 * 待审核状态
	 */
	public static final String DSHZT = "02";
	/**
	 * 校审核通过状态
	 */
	public static final String XSHTG = "11";

	@Spring
	public ISysuserManageExt sysuserManageExt;

	@Spring
	public ITbJzgxxshManagerExt tbJzgxxshManagerExt;

	/**
	 * 学校审核不通过状态
	 */
	public static final String XXBTG = "12";

	/**
	 * 区县审核不通过状态
	 */
	public static final String QXBTG = "22";

	/**
	 * 区县审核通过状态
	 */
	public static final String QXTG = "21";

	/**
	 * 高校
	 */
	private static final String GX = "10";
	/**
	 * 高职
	 */
	private static final String GZ = "20";
	/**
	 * 中小学
	 */
	private static final String ZXX = "30";
	/**
	 * 中职
	 */
	private static final String ZZ = "40";
	/**
	 * 特教
	 */
	private static final String TJ = "50";
	/**
	 * 幼儿园
	 */
	private static final String YEY = "60";

	public ITbJzgxxshManagerExt getTbJzgxxshManagerExt() {
		return tbJzgxxshManagerExt;
	}

	public void setTbJzgxxshManagerExt(ITbJzgxxshManagerExt tbJzgxxshManagerExt) {
		this.tbJzgxxshManagerExt = tbJzgxxshManagerExt;
	}

	public ISysuserManageExt getSysuserManageExt() {
		return sysuserManageExt;
	}

	public void setSysuserManageExt(ISysuserManageExt sysuserManageExt) {
		this.sysuserManageExt = sysuserManageExt;
	}

	/**
	 * (non-Javadoc)
	 * @see com.becom.jspt2016.service.ext.ILoginUserManagerExt#findOrgByUserNameAndPwd(java.lang.String, java.lang.String)
	 */
	@Override
	public SysUser findOrgByUserNameAndPwd(String userName, String pwd) {

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userName", userName.trim());
		params.put("pwd", pwd.trim());
		try {
			List<SysUser> dtos = this.findList("LoginUserManagerExt.findOrgByUserNameAndPwd.query", params,
					new RowMapper() {
						@Override
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							SysUser dto = new SysUser();
							SysRole sr = new SysRole();
							sr.setRoleId(rs.getLong("ROLE_ID"));
							dto.setUserId(rs.getLong("USER_ID"));//用户id
							dto.setUserName(rs.getString("USER_NAME"));//用户账号
							dto.setUserPwd(rs.getString("USER_PWD"));//用户密码
							dto.setUserXm(rs.getString("USER_XM"));//用户姓名
							dto.setUserType(rs.getString("USER_TYPE"));//用户类型
							dto.setId(rs.getString("ID"));//用户类型
							dto.setSysRole(sr);
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("LoginUserManagerExt findOrgByUserNameAndPwd(userName, pwd) is error!", e);
		}
		return null;

	}

	/**
	 * (non-Javadoc)
	 * @see com.becom.jspt2016.service.ext.ILoginUserManagerExt#findTeaByEduId(java.lang.String)
	 */
	@Override
	public TbBizJzgjbxx findTeaByEduId(String eduId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("eduId", eduId);
		try {
			List<TbBizJzgjbxx> dtos = this.findList("LoginUserManagerExt.findTeaByEduId.query", params,
					new RowMapper() {
						@Override
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TbBizJzgjbxx dto = new TbBizJzgjbxx();
							TbXxjgb jgb = new TbXxjgb();
							jgb.setXxjgid(rs.getLong("XXJGID"));//学校(机构)Id
							dto.setTbXxjgb(jgb);
							dto.setJsid(rs.getLong("JSID"));//教职工ID
							dto.setPersonalId(rs.getLong("PERSONAL_ID"));//旧平台教职工标识号
							dto.setEduId(rs.getString("EDU_ID"));
							dto.setXm(rs.getString("XM"));//教师姓名
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("LoginUserManagerExt findTeaByEduId(eduId) is error!", e);
		}
		return null;

	}

	/**
	 * (non-Javadoc)
	 * @see com.becom.jspt2016.service.ext.ILoginUserManagerExt#getMenuByRoleId(java.lang.String, java.lang.String)
	 */
	@Override
	public List<SysFunc> getMenuByEduId(String eduId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("eduId", eduId);

		try {
			List<SysFunc> dtos = this.findList("LoginUserManagerExt.getMenuByEduId.get", params, new RowMapper() {
				@Override
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					SysFunc dto = new SysFunc();
					dto.setFuncId(rs.getInt("funcId"));
					dto.setParentFuncId(rs.getLong("pFuncId"));
					dto.setFuncLevel(rs.getShort("funcLevel"));
					dto.setFuncName(rs.getString("funcName"));
					dto.setUrl(rs.getString("url"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * (non-Javadoc)
	 * @see com.becom.jspt2016.service.ext.ILoginUserManagerExt#getMenuByUserId(java.lang.String)
	 */
	@Override
	public List<SysFunc> getMenuByUserId(String userId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", userId);

		try {
			List<SysFunc> dtos = this.findList("LoginUserManagerExt.getMenuByUserId.get", params, new RowMapper() {
				@Override
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					SysFunc dto = new SysFunc();
					dto.setFuncId(rs.getInt("funcId"));
					dto.setParentFuncId(rs.getLong("pFuncId"));
					dto.setFuncLevel(rs.getShort("funcLevel"));
					dto.setFuncName(rs.getString("funcName"));
					dto.setUrl(rs.getString("url"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public List<SysFunc> getMenuByJgId(String jgId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jgId", jgId);

		try {
			List<SysFunc> dtos = this.findList("LoginUserManagerExt.getMenuByJgId.get", params, new RowMapper() {
				@Override
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					SysFunc dto = new SysFunc();
					dto.setFuncId(rs.getInt("funcId"));
					dto.setParentFuncId(rs.getLong("pFuncId"));
					dto.setFuncLevel(rs.getShort("funcLevel"));
					dto.setFuncName(rs.getString("funcName"));
					dto.setUrl(rs.getString("url"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<SysFunc> findFunAll() {
		try {
			List<SysFunc> dtos = this.findList("LoginUserManagerExt.getFunList.getAll", new RowMapper() {
				@Override
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					SysFunc dto = new SysFunc();
					dto.setFuncId(rs.getInt("funcId"));
					dto.setParentFuncId(rs.getLong("pFuncId"));
					dto.setFuncLevel(rs.getShort("funcLevel"));
					dto.setFuncName(rs.getString("funcName"));
					dto.setUrl(rs.getString("url"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public TbBizJzgjbxx findTeaByJsId(String tid) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jsId", tid);
		try {
			List<TbBizJzgjbxx> dtos = this.findList("LoginUserManagerExt.findTeaByJsId.query", params, new RowMapper() {
				@Override
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbBizJzgjbxx dto = new TbBizJzgjbxx();
					TbXxjgb jgb = new TbXxjgb();
					jgb.setXxjgid(rs.getLong("XXJGID"));//学校(机构)Id
					dto.setTbXxjgb(jgb);
					dto.setJsid(rs.getLong("JSID"));//教职工ID
					dto.setPersonalId(rs.getLong("PERSONAL_ID"));//旧平台教职工标识号
					dto.setEduId(rs.getString("EDU_ID"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("LoginUserManagerExt findTeaByEduId(eduId) is error!", e);
		}
		return null;
	}

	/////加载用户姓名
	public SysUser handleUser(SysUser user) {
		String setUserName = "";

		if (user != null) {
			String userType = user.getUserType();
			if ("3".equals(userType)) {
				Map<String, Object> jgmap = sysuserManageExt.getJg(user.getId());
				if (jgmap != null) {
					Object name = jgmap.get("name");
					setUserName = (name == null ? "" : name.toString());
				}
			} else if ("2".equals(userType)) {
				setUserName = sysuserManageExt.getQu(user.getId());
			} else if ("1".equals(userType)) {
				setUserName = "北京市";
			} else if ("9".equals(userType)) {
				setUserName = "超级管理员";
			}
			user.setUserXm(setUserName);
		}

		return user;
	}

	/**
	 * 教师待办事项
	 * (non-Javadoc)
	 * @see com.becom.jspt2016.service.ext.ILoginUserManagerExt#teacherTodo(long)
	 */
	@Override
	public TbJzgxxshLogDto teacherTodo(long jsid) {
		TbJzgxxsh tbJzgxxsh = tbJzgxxshManagerExt.query(jsid);
		if (tbJzgxxsh == null) {//没有报送过
			return null;
		}

		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsid", jsid);
			List<TbJzgxxshLogDto> dtos = this.findList("LoginUserManagerExt.teacherTodo.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TbJzgxxshLogDto dto = new TbJzgxxshLogDto();
							dto.setShsj(rs.getDate("shsj"));
							dto.setShyj(rs.getString("shyj"));
							dto.setShr(rs.getString("shr"));
							dto.setShjg(rs.getString("shjg"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("LoginUserManagerExt teacherTodo(jsid) is error!", e);
		}

		return null;

	}

	/**
	 * 用户待办事项
	 *
	 * @param userId 用户id
	 * @param managerType 用户类型
	 * @param id 校id或区县id
	 * @return 待办事项列表
	 */
	@Override
	public List<UserTodoDto> userTodo(long userId, String managerType, String id) {

		try {
			String sqlKey = "";
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("id", id);
			params.put("sfsc", SFSC);
			if (managerType.equals(XXJGTYPE)) {
				params.put("shzt", DSHZT);
				sqlKey = "LoginUserManagerExt.userTodo.queryXXJG";
			} else {
				params.put("shzt", XSHTG);
				sqlKey = "LoginUserManagerExt.userTodo.queryQX";
			}
			List<UserTodoDto> dtos = this.findList(sqlKey, params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					UserTodoDto dto = new UserTodoDto();
					dto.setXm(rs.getString("xm"));
					dto.setSj(rs.getDate("sj"));

					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("LoginUserManagerExt userTodo() is error!", e);
		}

		return null;

	}

	/**
	 * 获取教师数量
	 * 
	 * @param managerType 用户类型（区县：‘2’，市：‘1’）
	 * (non-Javadoc)
	 * @see com.becom.jspt2016.service.ext.ILoginUserManagerExt#getTeacherNum(java.lang.String)
	 */
	@Override
	public Map<String, Object> getTeacherNum(String managerType, String id, String ssxd) {
		List<TeacherNumDto> list = new ArrayList<TeacherNumDto>();
		list = queryTeacherNum(managerType, id);
		Map<String, Object> map = new HashMap<String, Object>();
		if (managerType.equals(QXTYPE) && list != null) {
			int num = 0;
			int dshrs = 0;
			int shtgrs = 0;
			for (TeacherNumDto teacherNumDto : list) {
				num += teacherNumDto.getNum();
				if (teacherNumDto.getShzt().equals("11")) {
					dshrs++;
				}
				if (teacherNumDto.getShzt().equals("21")) {
					shtgrs++;
				}
			}
			map.put("num", num);
			map.put("dsh", dshrs);
			map.put("shtg", shtgrs);
		} else if (managerType.equals(STYPE)) {
			map.put("tnums", list);
		} else {
			//TODO校
			int num = 0;
			int dshrs = 0;
			int shtgrs = 0;
			if (list != null) {
				for (TeacherNumDto teacherNumDto : list) {
					num += teacherNumDto.getNum();
					if (teacherNumDto.getShzt().equals("02")) {
						dshrs++;
					}
					if (ssxd.equals("10") || ssxd.equals("20")) {

						if (teacherNumDto.getShzt().equals("11")) {
							shtgrs++;
						}
					} else {
						if (teacherNumDto.getShzt().equals("21")) {
							shtgrs++;
						}
					}
				}
			}
			map.put("num", num);
			map.put("dsh", dshrs);
			map.put("shtg", shtgrs);
		}
		return map;
	}

	/**
	 * 查询教师数量
	 * 
	 * @param managerType 用户类型（区县：‘2’，市：‘1’）
	 * (non-Javadoc)
	 * @see com.becom.jspt2016.service.ext.ILoginUserManagerExt#getTeacherNum(java.lang.String)
	 */
	@Override
	public List<TeacherNumDto> queryTeacherNum(String managerType, String id) {

		try {
			Map<String, Object> params = new HashMap<String, Object>();

			String sql = "";
			if (managerType.equals(QXTYPE)) {
				sql = "LoginUserManagerExt.queryTeacherNum.queryQX";
				String[] ssxds = { "30", "40", "50", "60" };
				params.put("sfsc", SFSC);
				params.put("ssxds", ssxds);
				params.put("qxdm", id);
				List<TeacherNumDto> dtos = this.findList(sql, params, new RowMapper() {
					public Object mapRow(ResultSet rs, int arg1) throws SQLException {
						TeacherNumDto dto = new TeacherNumDto();
						dto.setNum(rs.getInt("num"));
						dto.setShzt(rs.getString("shzt"));
						return dto;
					}
				});
				return dtos;
			} else if (managerType.equals(STYPE)) {
				sql = "LoginUserManagerExt.queryTeacherNum.queryS";
				params.put("shjg", 1);
				params.put("sfsc", SFSC);
				List<TeacherNumDto> dtos = this.findList(sql, params, new RowMapper() {
					public Object mapRow(ResultSet rs, int arg1) throws SQLException {
						TeacherNumDto dto = new TeacherNumDto();
						dto.setSsxd(rs.getString("ssxd"));
						dto.setNum(rs.getInt("num"));
						return dto;
					}
				});
				return dtos;
			} else {
				sql = "LoginUserManagerExt.queryTeacherNum.queryXXJG";
				params.put("xxid", id);
				params.put("sfsc", SFSC);
				List<TeacherNumDto> dtos = this.findList(sql, params, new RowMapper() {
					public Object mapRow(ResultSet rs, int arg1) throws SQLException {
						TeacherNumDto dto = new TeacherNumDto();
						dto.setShzt(rs.getString("shzt"));
						dto.setNum(rs.getInt("num"));
						return dto;
					}
				});
				return dtos;
			}
		} catch (Exception e) {
			logger.error("LoginUserManagerExt queryTeacherNum(managerType) is error!", e);
		}

		return null;

	}
}
