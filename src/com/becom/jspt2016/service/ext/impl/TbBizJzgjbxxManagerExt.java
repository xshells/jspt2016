/**
 * TbBizJzgjbxxManagerExt.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.nestframework.action.FileItem;
import org.nestframework.commons.hibernate.ISqlElement;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.CheckShDto;
import com.becom.jspt2016.dto.SubmitCommentDto;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbJzgxxsh;
import com.becom.jspt2016.model.TbXxjgb;
import com.becom.jspt2016.service.ITbJzgxxshManager;
import com.becom.jspt2016.service.ITbXxjgbManager;
import com.becom.jspt2016.service.ext.ITbBizJzgjbxxManagerExt;
import com.becom.jspt2016.service.ext.ITeacherAccountManageExt;

/**
 * 教师基本信息
 *
 * @author   zhangchunming
 * @Date	 2016年10月17日 	 
 */
public class TbBizJzgjbxxManagerExt extends JdbcRootManager implements ITbBizJzgjbxxManagerExt {

	protected Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 相片格式
	 */
	private static final String ExtName = "jpg";

	/**
	 * 是否删除(未删除)
	 */
	private static final String SFSC = "0";

	/**
	 * 相片最大值
	 */
	private static final int MaxSize = 60;

	/**
	 * 高校
	 */
	private static final String GX = "10";
	/**
	 * 高职
	 */
	private static final String GZ = "20";
	/**
	 * 中小学
	 */
	private static final String ZXX = "30";
	/**
	 * 中职
	 */
	private static final String ZZ = "40";
	/**
	 * 特教
	 */
	private static final String TJ = "50";
	/**
	 * 幼儿园
	 */
	private static final String YEY = "60";
	/**
	 *  未审核状态(未报送)
	 */
	private static final String WBSSHZT = "01";
	/**
	 *  学校不通过状态
	 */
	private static final String XXBTGSHZT = "12";
	/**
	 *  区县不通过状态
	 */
	private static final String QXBTGSHZT = "22";
	/**
	 *  待审核状态
	 */
	private static final String DSHZT = "02";
	/**
	 *  审核状态院校通过状态
	 */
	private static final String YXTGSHZT = "11";
	/**
	 *  信息类别
	 */
	private static final String XXLB = "01";
	/**
	 *  报送人类别（教师）
	 */
	private static final String BSRLB = "4";
	/**
	 *  报送人类别（机构）
	 */
	private static final String JGBSRLB = "3";

	/**
	 * 审核结果0-审核中 1-通过 2-不通过 
	 */
	private static final String SHJG = "0";

	/**
	 * 是否在职，统一为:1
	 */
	private static final String SFZZ = "1";

	/**
	 * 审核中
	 */
	private static final String SHZ = "0";

	/**
	 * 审核通过
	 */
	private static final String SHTG = "1";

	/**
	 * 是否完善了基本信息（是）
	 */
	private static final long SFWCBTX = 1;

	public String eduid;

	private ITbJzgxxshManager tbJzgxxshManager;

	private ITbXxjgbManager tbXxjgbManager;

	private ITeacherAccountManageExt teacherAccountManageExt;

	/**
	 * 保存信息
	 * @see com.becom.jspt2016.service.ext.ITbBizJzgjbxxManagerExt#saveUser(java.util.Map)
	 */
	@Override
	public void saveUser(Map<String, Object> params, String ssxd) {

		String sqlKey = "";
		if (ssxd.equals(GX)) {
			sqlKey = "TbBizJzgjbxxManagerExt.saveUser.updateGX";
		}
		if (ssxd.equals(GZ)) {
			sqlKey = "TbBizJzgjbxxManagerExt.saveUser.updateGZ";
		}
		if (ssxd.equals(ZZ)) {
			sqlKey = "TbBizJzgjbxxManagerExt.saveUser.updateZZ";
		}
		if (ssxd.equals(ZXX)) {
			sqlKey = "TbBizJzgjbxxManagerExt.saveUser.updateZXX";
		}
		if (ssxd.equals(TJ)) {
			sqlKey = "TbBizJzgjbxxManagerExt.saveUser.updateTJ";
		}
		if (ssxd.equals(YEY)) {
			sqlKey = "TbBizJzgjbxxManagerExt.saveUser.updateYEY";
		}
		params.put("sfwcbtx", SFWCBTX);
		try {
			ISqlElement se = processSql(params, sqlKey);
			getJdbcTemplate().update(se.getSql(), se.getParams());
		} catch (Exception e) {
			logger.error("TbBizJzgjbxxManagerExt saveUser() is error!", e);
		}

	}

	/**
	 * 添加教职工
	 */
	@Override
	public String add(final TbBizJzgjbxx jbxx, final long jgid) {
		StringBuffer sql = new StringBuffer();
		TbXxjgb xxjgb = tbXxjgbManager.get(jgid);
		eduid = teacherAccountManageExt.getEduId(xxjgb.getSsxd(), xxjgb.getQxdm());//教育Id需要重写
		sql.append(
				"insert into TB_BIZ_JZGJBXX(JSID,XXJGID,EDU_ID,SFZZ,SFSFLZYBY,SFSGTJZYPYPX,SFYTSJYCYZS,XXJSYYNL,SFSYMFSFS,SFSTGJS,BSRLB,SHZT,SHSJ,SHJG,SFSC,CJSJ,XM,XB,SFZJLX,SFZJH,CSRQ,ZGQK,"
						+ "GJDQ,CSD,MZ,ZZMM,CJGZNY,JBXNY,JZGLY,JZGLB,SFZB,XYJG,QDHTQK,YRXS,GRZP,SFSS,SFJBZYJNDJZS,QYGZSJSC,JXJYBH,SFXLJKJYJS,CSTJQSNY,SFSGXQJYZYPYPX,SFXQJYZYBY,SFWCBTX,GGJSLX) ")
				.append("values (SEQ_TB_BIZ_JZGJBXX.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {

				ps.setLong(1, jgid);//机构Id
				ps.setString(2, eduid);
				ps.setString(3, SFZZ);
				ps.setString(4, jbxx.getSfqrztsyjzyby());
				ps.setString(5, jbxx.getSfsgtjzypypx());
				ps.setString(6, jbxx.getSfytsjycyzs());
				ps.setString(7, jbxx.getXxjsyynl());
				ps.setString(8, jbxx.getSfsymfsfs());
				ps.setString(9, jbxx.getSfstgjs());
				ps.setString(10, JGBSRLB);
				ps.setString(11, WBSSHZT);
				ps.setTimestamp(12, new Timestamp(System.currentTimeMillis()));
				ps.setString(13, SHJG);
				ps.setString(14, SFSC);
				ps.setTimestamp(15, new Timestamp(System.currentTimeMillis()));
				ps.setString(16, jbxx.getXm().toString());
				ps.setString(17, jbxx.getXb());
				ps.setString(18, jbxx.getSfzjlx());
				ps.setString(19, jbxx.getSfzjh());
				ps.setString(20, jbxx.getCsrq());
				ps.setString(21, jbxx.getZgqk());
				ps.setString(22, jbxx.getGjdq());
				ps.setString(23, jbxx.getCsd());
				ps.setString(24, jbxx.getMz());
				ps.setString(25, jbxx.getZzmm());
				ps.setString(26, jbxx.getCjgzny());
				ps.setString(27, jbxx.getJbxny());
				ps.setString(28, jbxx.getJzgly());
				ps.setString(29, jbxx.getJzglb());
				ps.setString(30, jbxx.getSfzb());
				ps.setString(31, jbxx.getXyjg());
				ps.setString(32, jbxx.getQdhtqk());
				ps.setString(33, jbxx.getYrxs());
				ps.setString(34, jbxx.getGrzp());
				ps.setString(35, jbxx.getSfss());
				ps.setString(36, jbxx.getSfjbzyjndjzs());
				ps.setString(37, jbxx.getQygzsjsc());
				ps.setString(38, jbxx.getJxjybh());
				ps.setString(39, jbxx.getSfxljkjyjs());
				ps.setString(40, jbxx.getCstjqsny());
				ps.setString(41, jbxx.getSfsgxqjyzypypx());
				ps.setString(42, jbxx.getSfxqjyzyby());
				ps.setLong(43, SFWCBTX);
				ps.setString(44, jbxx.getGgjslx());
			}
		});
		return eduid;
	}

	/**
	* 删除照片
	* @see com.becom.jspt2016.service.ext.ITbBizJzgjbxxManagerExt#deletePhoto(java.util.Map)
	*/

	@Override
	public void deletePhoto(Map<String, Object> params) {

		try {
			ISqlElement se = processSql(params, "TbBizJzgjbxxManagerExt.deletePhoto.update");
			getJdbcTemplate().update(se.getSql(), se.getParams());
		} catch (Exception e) {
			logger.error("TbBizJzgjbxxManagerExt deletePhoto() is error!", e);
		}
	}

	/**
	 * 校验照片是否合格
	 * 
	 * @see com.becom.jspt2016.service.ext.ITbBizJzgjbxxManagerExt#checkPhoto(org.nestframework.action.FileItem)
	 */
	@Override
	public Map<String, String> checkPhoto(FileItem photo) {

		Map<String, String> ret = new HashMap<String, String>();
		if (photo != null && photo.getFileName() != null) {
			//获取照片大小
			long filesize = photo.getSize();
			//获取照片长宽
			BufferedImage bi = null;
			try {
				bi = ImageIO.read(photo.getFile());
			} catch (IOException e) {
				e.printStackTrace();
			}
			int width = bi.getWidth(); // 像素
			int height = bi.getHeight(); // 像素
			//获取相片格式
			String fileName = photo.getFileName();
			String endName = getExtName(fileName);
			/*	if ((filesize / 1024) > MaxSize) {
					ret.put("isValid", "false");
					ret.put("msg", "照片大小应小于60k!");
					return ret;
				}
			*/
			/*if (width != 26) {
				ret.put("isValid", "false");
				ret.put("msg", "照片宽度不等于26mm!");
				return ret;
			}
			if (width != 26) {
				ret.put("isValid", "false");
				ret.put("msg", "照片高度不等于32mm!");
				return ret;
			}*/
			/*	if (!endName.equals(ExtName)) {
					ret.put("isValid", "false");
					ret.put("msg", "照片格式不正确!");
					return ret;
				}*/
			ret.put("isValid", "true");
			ret.put("msg", "照片通过验证");
			return ret;
		}
		ret.put("isValid", "false");
		ret.put("msg", "文件不存在!");
		return ret;
	}

	/**
	 * 获取文件格式名称
	 * @param filename
	 * @return
	 */
	private String getExtName(String filename) {
		String extName = "";
		if (filename != null) {
			extName = filename.substring(filename.lastIndexOf(".") + 1);
		}
		return extName;
	}

	/**
	 * 验证能否报送(教师)
	 * (non-Javadoc)
	 * @see com.becom.jspt2016.service.ext.ITbBizJzgjbxxManagerExt#submitted(com.becom.jspt2016.model.TbBizJzgjbxx)
	 */
	@Override
	public CheckShDto submitted(TbBizJzgjbxx jbxx) {
		String checksql = "";
		if (jbxx.getTbXxjgb().getSsxd().equals(GX)) {
			checksql = "TbBizJzgjbxxManagerExt.submitted.queryGX";
		}
		if (jbxx.getTbXxjgb().getSsxd().equals(GZ)) {
			checksql = "TbBizJzgjbxxManagerExt.submitted.queryGZ";
		}
		if (jbxx.getTbXxjgb().getSsxd().equals(ZXX)) {
			checksql = "TbBizJzgjbxxManagerExt.submitted.queryZXX";
		}
		if (jbxx.getTbXxjgb().getSsxd().equals(ZZ) || jbxx.getTbXxjgb().getSsxd().equals(TJ)) {
			checksql = "TbBizJzgjbxxManagerExt.submitted.queryZZTJ";
		}
		if (jbxx.getTbXxjgb().getSsxd().equals(YEY)) {
			checksql = "TbBizJzgjbxxManagerExt.submitted.queryYEY";
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sfsc", SFSC);
		params.put("jsid", jbxx.getJsid());
		StringBuffer sb = new StringBuffer();
		sb.append(WBSSHZT).append(",").append(XXBTGSHZT).append(",").append(QXBTGSHZT);
		params.put("shzts", sb.toString().split(","));
		try {
			List<CheckShDto> dtos = this.findList(checksql, params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					CheckShDto dto = new CheckShDto();
					dto.setCount(rs.getInt("coun"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("TbBizJzgjbxxManagerExt submitted() is error!", e);
		}
		return null;

	}

	/**
	 * 验证能否报送(机构)
	 * (non-Javadoc)
	 * @see com.becom.jspt2016.service.ext.ITbBizJzgjbxxManagerExt#submitted(com.becom.jspt2016.model.TbBizJzgjbxx)
	 */
	@Override
	public CheckShDto syssubmitted(TbBizJzgjbxx jbxx) {

		String checksql = "";
		if (jbxx.getTbXxjgb().getSsxd().equals(GX)) {
			checksql = "TbBizJzgjbxxManagerExt.syssubmitted.queryGX";
		}
		if (jbxx.getTbXxjgb().getSsxd().equals(GZ)) {
			checksql = "TbBizJzgjbxxManagerExt.syssubmitted.queryGZ";
		}
		if (jbxx.getTbXxjgb().getSsxd().equals(ZXX)) {
			checksql = "TbBizJzgjbxxManagerExt.syssubmitted.queryZXX";
		}
		if (jbxx.getTbXxjgb().getSsxd().equals(ZZ) || jbxx.getTbXxjgb().getSsxd().equals(TJ)) {
			checksql = "TbBizJzgjbxxManagerExt.syssubmitted.queryZZTJ";
		}
		if (jbxx.getTbXxjgb().getSsxd().equals(YEY)) {
			checksql = "TbBizJzgjbxxManagerExt.syssubmitted.queryYEY";
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sfsc", SFSC);
		params.put("jsid", jbxx.getJsid());
		StringBuffer sb = new StringBuffer();
		sb.append(WBSSHZT).append(",").append(XXBTGSHZT).append(",").append(QXBTGSHZT);
		params.put("shzts", sb.toString().split(","));
		try {
			List<CheckShDto> dtos = this.findList(checksql, params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					CheckShDto dto = new CheckShDto();
					dto.setCount(rs.getInt("coun"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("TbBizJzgjbxxManagerExt syssubmitted() is error!", e);
		}
		return null;
	}

	/**
	 * 报送信息(教师)
	 * @see com.becom.jspt2016.service.ext.ITbBizJzgjbxxManagerExt#submitInfo(com.becom.jspt2016.model.TbBizJzgjbxx)
	 */
	@Override
	public void submitInfo(TbBizJzgjbxx jzgjbxx) {
		SubmitCommentDto dto = new SubmitCommentDto();
		dto.setAduitStatus(DSHZT);
		dto.setJsId(jzgjbxx.getJsid());
		dto.setSsxd(jzgjbxx.getTbXxjgb().getSsxd());
		dto.setSubmitPersonId(jzgjbxx.getJsid());
		dto.setXxlb(XXLB);
		dto.setSubmitPersonType(BSRLB);
		dto.setSfsc(SFSC);
		dto.setShjg(SHZ);
		// 调用各个表的修改
		updateCJBSHJ(dto);
		updateTbJzgjbxx(dto);
		updateGJBZJHYBZ(dto);
		updateGJYY(dto);
		updateGNPX(dto);
		updateGWPR(dto);
		updateGZJL(dto);
		updateHJXX(dto);
		updateJLLGB(dto);
		updateJNJZSQ(dto);
		updateJYJX(dto);
		updateLWXX(dto);
		updateLXXX(dto);
		updateNDKH(dto);
		updateRJZZQ(dto);
		updateRXRCXM(dto);
		updateSDCF(dto);
		updateSDKH(dto);
		updateSDRY(dto);
		updateWYZP(dto);
		updateXMKT(dto);
		updateXXJL(dto);
		updateYXHFX(dto);
		updateYYJN(dto);
		updateZiGeXXB(dto);
		updateZSB(dto);
		updateZXBgGHYJBG(dto);
		updateZYJSZWPRB(dto);
		updateZZXX(dto);

		TbJzgxxsh tbJzgxxsh = tbJzgxxshManager.getByProperty("jsid", jzgjbxx.getJsid());
		if (null == tbJzgxxsh) {
			insertTbJzgxxsh(dto);
		} else {
			updateTbJzgxxsh(dto);
		}
	}

	/**
	 * 报送信息（院校）
	 * @see com.becom.jspt2016.service.ext.ITbBizJzgjbxxManagerExt#submitInfo(com.becom.jspt2016.model.TbBizJzgjbxx)
	 */
	@Override
	public void yxsubmitInfo(TbBizJzgjbxx jzgjbxx, long bsrid) {
		SubmitCommentDto dto = new SubmitCommentDto();
		dto.setAduitStatus(YXTGSHZT);
		dto.setJsId(jzgjbxx.getJsid());
		dto.setSsxd(jzgjbxx.getTbXxjgb().getSsxd());
		dto.setSubmitPersonId(bsrid);
		dto.setXxlb(XXLB);
		dto.setSubmitPersonType(JGBSRLB);
		dto.setSfsc(SFSC);
		if (jzgjbxx.getTbXxjgb().getSsxd().equals(GX) || jzgjbxx.getTbXxjgb().getSsxd().equals(GZ)) {
			dto.setShjg(SHTG);
		} else {
			dto.setShjg(SHZ);
		}
		// 调用各个表的修改
		updateCJBSHJ(dto);
		updateTbJzgjbxx(dto);
		updateGJBZJHYBZ(dto);
		updateGJYY(dto);
		updateGNPX(dto);
		updateGWPR(dto);
		updateGZJL(dto);
		updateHJXX(dto);
		updateJLLGB(dto);
		updateJNJZSQ(dto);
		updateJYJX(dto);
		updateLWXX(dto);
		updateLXXX(dto);
		updateNDKH(dto);
		updateRJZZQ(dto);
		updateRXRCXM(dto);
		updateSDCF(dto);
		updateSDKH(dto);
		updateSDRY(dto);
		updateWYZP(dto);
		updateXMKT(dto);
		updateXXJL(dto);
		updateYXHFX(dto);
		updateYYJN(dto);
		updateZiGeXXB(dto);
		updateZSB(dto);
		updateZXBgGHYJBG(dto);
		updateZYJSZWPRB(dto);
		updateZZXX(dto);
		TbJzgxxsh tbJzgxxsh = tbJzgxxshManager.getByProperty("jsid", jzgjbxx.getJsid());
		if (null == tbJzgxxsh) {
			insertTbJzgxxsh(dto);
		} else {
			updateTbJzgxxsh(dto);
		}
	}

	/**
	 * 修改教职工信息审核表
	 *
	 * @param dto
	 */
	private void updateTbJzgxxsh(SubmitCommentDto dto) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", dto.getJsId());
		params.put("bsrlb", dto.getSubmitPersonType());
		params.put("bsr", dto.getSubmitPersonId());
		params.put("aduitStatus", dto.getAduitStatus());
		params.put("shjg", dto.getShjg());
		params.put("bssj", new Timestamp(System.currentTimeMillis()));
		try {
			ISqlElement se = processSql(params, "TbBizJzgjbxxManagerExt.updateTbJzgxxsh.update");
			getJdbcTemplate().update(se.getSql(), se.getParams());
		} catch (Exception e) {
			logger.error("TbBizJzgjbxxManagerExt updateTbJzgxxsh() is error!", e);
		}
	}

	/**
	 * 添加教职工信息审核表
	 *
	 * @param dto
	 */
	private void insertTbJzgxxsh(final SubmitCommentDto dto) {

		StringBuffer sql = new StringBuffer();
		String sqlbegin = "";
		String sqlend = "";

		sqlbegin = "insert into TB_JZGXXSH (SH_ID,JSID,SSXD,XXLB,BSRLB,BSR,BSSJ,SHZT,SHSJ,SHR_USER_ID,SHJG)";
		sqlend = "values (SEQ_TB_JZGXXSH.nextval,?,?,?,?,?,?,?,?,?,?)";
		sql.append(sqlbegin).append(sqlend);
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, dto.getJsId());
				ps.setString(2, dto.getSsxd());
				ps.setString(3, dto.getXxlb());
				ps.setString(4, dto.getSubmitPersonType());
				ps.setLong(5, dto.getSubmitPersonId());
				ps.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
				ps.setString(7, dto.getAduitStatus());
				ps.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
				ps.setLong(9, 0);
				ps.setString(10, dto.getShjg());
			}
		});

	}

	/**
	 * 取消报送
	 * (non-Javadoc)
	 * @see com.becom.jspt2016.service.ext.ITbBizJzgjbxxManagerExt#cancelSubmit(com.becom.jspt2016.model.TbBizJzgjbxx)
	 */
	@Override
	public void cancelSubmit(TbBizJzgjbxx jzgjbxx) {
		SubmitCommentDto dto = new SubmitCommentDto();
		dto.setAduitStatus(WBSSHZT);
		dto.setJsId(jzgjbxx.getJsid());
		dto.setSsxd(jzgjbxx.getTbXxjgb().getSsxd());
		dto.setSubmitPersonId(jzgjbxx.getJsid());
		dto.setXxlb(XXLB);
		dto.setSubmitPersonType(BSRLB);
		dto.setSfsc(SFSC);
		dto.setShjg(null);
		TbJzgxxsh tbJzgxxsh = tbJzgxxshManager.getByProperty("jsid", jzgjbxx.getJsid());
		if (tbJzgxxsh == null) {
			return;
		} else {
			if (tbJzgxxsh.getShzt().equals(WBSSHZT)) {
				return;
			}
			updateTbJzgxxsh(dto);
			updateTbJzgjbxx(dto);
			updateCJBSHJ(dto);
			updateGJBZJHYBZ(dto);
			updateGJYY(dto);
			updateGNPX(dto);
			updateGWPR(dto);
			updateGZJL(dto);
			updateHJXX(dto);
			updateJLLGB(dto);
			updateJNJZSQ(dto);
			updateJYJX(dto);
			updateLWXX(dto);
			updateLXXX(dto);
			updateNDKH(dto);
			updateRJZZQ(dto);
			updateRXRCXM(dto);
			updateSDCF(dto);
			updateSDKH(dto);
			updateSDRY(dto);
			updateWYZP(dto);
			updateXMKT(dto);
			updateXXJL(dto);
			updateYXHFX(dto);
			updateYYJN(dto);
			updateZiGeXXB(dto);
			updateZSB(dto);
			updateZXBgGHYJBG(dto);
			updateZYJSZWPRB(dto);
			updateZZXX(dto);
		}

	}

	/**
	 * 教师基本报送
	 *
	 * @param suDto
	 */
	public int updateTbJzgjbxx(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateTbJzgjbxx.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateTbJzgjbxx() is error!", e);
		}
		return result;
	}

	/**
	 * 教师资格报送
	 *
	 * @param suDto
	 */
	public int updateZiGeXXB(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateZiGeXXB.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateZiGeXXB() is error!", e);
		}
		return result;
	}

	/**
	 * 教职工专业技术职务报送
	 *
	 * @param suDto
	 */
	public int updateZYJSZWPRB(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateZYJSZWPRB.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateZYJSZWPRB() is error!", e);
		}
		return result;
	}

	/**
	 * 交流轮岗报送
	 *
	 * @param suDto
	 */
	public int updateJLLGB(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateJLLGB.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateJLLGB() is error!", e);
		}
		return result;
	}

	/**
	 * 教职工入选人才项目报送
	 *
	 * @param suDto
	 */
	public int updateRXRCXM(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateRXRCXM.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateRXRCXM() is error!", e);
		}
		return result;
	}

	/**
	 * 国内培训报送
	 *
	 * @param suDto
	 */
	public int updateGNPX(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateGNPX.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateGNPX() is error!", e);
		}
		return result;
	}

	/**
	 * 学习经历报送
	 *
	 * @param suDto
	 */
	public int updateXXJL(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateXXJL.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateXXJL() is error!", e);
		}
		return result;
	}

	/**
	 * 岗位聘任报送
	 *
	 * @param suDto
	 */
	public int updateGWPR(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateGWPR.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateGWPR() is error!", e);
		}
		return result;
	}

	/**
	 * 工作经历报送
	 *
	 * @param suDto
	 */
	public int updateGZJL(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateGZJL.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateGZJL() is error!", e);
		}
		return result;
	}

	/**
	 * 师德处分报送
	 *
	 * @param suDto
	 */
	public int updateSDCF(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateSDCF.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateSDCF() is error!", e);
		}
		return result;
	}

	/**
	 * 师德考核报送
	 *
	 * @param suDto
	 */
	public int updateSDKH(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateSDKH.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateSDKH() is error!", e);
		}
		return result;
	}

	/**
	 * 师德荣誉报送
	 *
	 * @param suDto
	 */
	public int updateSDRY(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateSDRY.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateSDRY() is error!", e);
		}
		return result;
	}

	/**
	 * 年度考核报送
	 *
	 * @param suDto
	 */
	public int updateNDKH(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateNDKH.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateNDKH() is error!", e);
		}
		return result;
	}

	/**
	 * 教职工技能及证书  其他技能报送
	 *
	 * @param suDto
	 */
	public int updateJNJZSQ(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateJNJZSQ.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateJNJZSQ() is error!", e);
		}
		return result;
	}

	/**
	 * 教职工技能及证书  语言技能报送
	 *
	 * @param suDto
	 */
	public int updateYYJN(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateYYJN.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateYYJN() is error!", e);
		}
		return result;
	}

	/**
	 * 教职工技能及证书  证书报送
	 *
	 * @param suDto
	 */
	public int updateZSB(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateZSB.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateZSB() is error!", e);
		}
		return result;
	}

	/**
	 * 教职科研成果及奖项   专利及软件著作权报送
	 *
	 * @param suDto
	 */
	public int updateRJZZQ(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateRJZZQ.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateRJZZQ() is error!", e);
		}
		return result;
	}

	/**
	 *  教职科研成果及奖项   国家医药报送
	 *
	 * @param suDto
	 */
	public int updateGJYY(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateGJYY.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateGJYY() is error!", e);
		}
		return result;
	}

	/**
	 *  教职科研成果及奖项   国家标准及行业标准报送
	 *
	 * @param suDto
	 */
	public int updateGJBZJHYBZ(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateGJBZJHYBZ.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateGJBZJHYBZ() is error!", e);
		}
		return result;
	}

	/**
	 *  教职科研成果及奖项    指导学生参加比赛报送
	 *
	 * @param suDto
	 */
	public int updateCJBSHJ(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateCJBSHJ.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateCJBSHJ() is error!", e);
		}
		return result;
	}

	/**
	 *  教职科研成果及奖项   文艺作品报送
	 *
	 * @param suDto
	 */
	public int updateWYZP(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateWYZP.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateWYZP() is error!", e);
		}
		return result;
	}

	/**
	 *  教职科研成果及奖项   获奖报送
	 *
	 * @param suDto
	 */
	public int updateHJXX(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateHJXX.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateHJXX() is error!", e);
		}
		return result;
	}

	/**
	 *  教职科研成果及奖项   论文信息报送
	 *
	 * @param suDto
	 */
	public int updateLWXX(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateLWXX.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateLWXX() is error!", e);
		}
		return result;
	}

	/**
	 *  教职科研成果及奖项   著作信息报送
	 *
	 * @param suDto
	 */
	public int updateZZXX(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateZZXX.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateZZXX() is error!", e);
		}
		return result;
	}

	/**
	 *  教职科研成果及奖项    项目课题信息报送
	 *
	 * @param suDto
	 */
	public int updateXMKT(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateXMKT.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateXMKT() is error!", e);
		}
		return result;
	}

	/**
	 *   教职工教育教学信息报送
	 *
	 * @param suDto
	 */
	public int updateJYJX(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateJYJX.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateJYJX() is error!", e);
		}
		return result;
	}

	/**
	 *   教职工海外研修和访学信息报送
	 *
	 * @param suDto
	 */
	public int updateYXHFX(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateYXHFX.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateYXHFX() is error!", e);
		}
		return result;
	}

	/**
	 *   教职工联系信息信息报送
	 *
	 * @param suDto
	 */
	public int updateLXXX(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateLXXX.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateLXXX() is error!", e);
		}
		return result;
	}

	/**
	 *   教职工科研成果及获奖    咨询报告或研究报告信息报送
	 *
	 * @param suDto
	 */
	public int updateZXBgGHYJBG(SubmitCommentDto suDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jdId", suDto.getJsId());
		params.put("aduitStatus", suDto.getAduitStatus());
		params.put("submitPersonType", suDto.getSubmitPersonType());
		params.put("sfsc", suDto.getSfsc());
		params.put("shjg", suDto.getShjg());
		try {
			ISqlElement processSql = processSql(params, "TbBizJzgjbxxManagerExt.updateZXBgGHYJBG.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJzgjbxxManagerExt updateZXBgGHYJBG() is error!", e);
		}
		return result;
	}

	public ITbJzgxxshManager getTbJzgxxshManager() {
		return tbJzgxxshManager;
	}

	public void setTbJzgxxshManager(ITbJzgxxshManager tbJzgxxshManager) {
		this.tbJzgxxshManager = tbJzgxxshManager;
	}

	public ITbXxjgbManager getTbXxjgbManager() {
		return tbXxjgbManager;
	}

	public void setTbXxjgbManager(ITbXxjgbManager tbXxjgbManager) {
		this.tbXxjgbManager = tbXxjgbManager;
	}

	public ITeacherAccountManageExt getTeacherAccountManageExt() {
		return teacherAccountManageExt;
	}

	public void setTeacherAccountManageExt(ITeacherAccountManageExt teacherAccountManageExt) {
		this.teacherAccountManageExt = teacherAccountManageExt;
	}

}
