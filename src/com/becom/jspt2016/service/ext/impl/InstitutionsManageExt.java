/**
 * InstitutionsManageExt.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.commons.hibernate.IPage;
import org.nestframework.commons.hibernate.IRowHandler;
import org.nestframework.commons.hibernate.ISqlElement;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.DisCodesDto;
import com.becom.jspt2016.dto.InstitutionsInfoDto;
import com.becom.jspt2016.dto.TeacherAccountInfoDto;
import com.becom.jspt2016.dto.UserDto;
import com.becom.jspt2016.dto.ZxdwlbZdxbDto;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.service.ext.IInstitutionsManageExt;
import com.becom.jspt2016.service.ext.ISysUserManagerExt;

/**
 * 机构管理业务层)
 * <p>
 *包括区级和市级
 * @author   kuangxiang
 * @Date	 2016年10月18日
 */

public class InstitutionsManageExt extends JdbcRootManager implements IInstitutionsManageExt {
	protected Logger logger = Logger.getLogger(this.getClass());
	/**
	 * 是否在用1-在用 0-禁用 
	 */
	private static final String SFZY = "1";
	/**
	 *禁用
	 */
	private static final String NOSFZY = "0";
	/**
	 * 所属学段10-本科院校20-高等职业学校30-中小学校40-中等职业学校50-特殊教育学校60-幼儿园
	 */
	private static final String SSXD = "10";
	@Spring
	private ISysUserManagerExt sysUserManagerExt;

	/**
	 * 搜索
	 */
	@Override
	public IPage doSearch(Map<String, Object> param, int pageNo, int pageSize) {
		try {
			param.put("sfzy", SFZY);//是否在用
			IPage<InstitutionsInfoDto> dtos = this.findPage("InstitutionsManageExt.doSearch.query",
					"InstitutionsManageExt.doSearch.count", param, pageNo, pageSize,
					new IRowHandler<InstitutionsInfoDto>() {
						public InstitutionsInfoDto handleRow(ResultSet rs) {
							InstitutionsInfoDto dto = new InstitutionsInfoDto();
							try {
								dto.setInstitutionId(Integer.valueOf(rs.getString("institutionId")));
								dto.setInstitutionCode(rs.getString("institutionCode"));
								dto.setInstitutionName(rs.getString("institutionName"));
								dto.setInstitutionAddress(rs.getString("institutionAddress"));
								dto.setContactPerson(rs.getString("contactPerson"));
								dto.setContactTel(rs.getString("contactTel"));
								dto.setEmail(rs.getString("email"));
								dto.setDistrictCode(rs.getString("districtCode"));
								dto.setJbdw(rs.getString("jbdw"));
								dto.setJbzlx(rs.getString("jbzlx"));
								dto.setResidentType(rs.getString("residentType"));
								dto.setSchoolType(rs.getString("schoolType"));
								dto.setAffiliatedSchool(rs.getString("affiliatedSchool"));
								dto.setUnitType(rs.getString("unitType"));
								dto.setDisName(rs.getString("disName"));
								dto.setUnitCode(rs.getString("unitCode"));
								dto.setCount(rs.getInt("count"));
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			return dtos;
		} catch (Exception e) {
			logger.error("InstitutionsManageExt doSearch() is error!", e);
		}
		return null;
	}

	/**
	 * 添加机构
	 */
	@Override
	public void add(final InstitutionsInfoDto institutionDto, final String userType) {
		StringBuffer sql = new StringBuffer();
		String inCode = "";
		if ("50".equals(institutionDto.getAffiliatedSchool())
				|| ("30".equals(institutionDto.getAffiliatedSchool()) && "1".equals(institutionDto.getUnitType()))) {
			//需要手添
			inCode = institutionDto.getInstitutionCode();

		} else {
			inCode = generateInstitutionCode(institutionDto, userType);
		}
		final String iCode = inCode;
		sql.append(
				"insert into TB_XXJGB(XXJGID,XXJGDM,XXJGMC,QXDM,DZ,BXLX,ZDCXLX,JBDW,JBZLX,LXR,LXDH,EMAIL,CJSJ,SFZY,SSXD,ZXXFL,DWZZDM)")
				.append("values (SEQ_TB_XXJGB.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				//获取当前管理员信息
				ps.setString(1, iCode);//机构代码
				ps.setString(2, institutionDto.getInstitutionName());
				ps.setString(3, institutionDto.getDistrictCode());
				ps.setString(4, institutionDto.getInstitutionAddress());
				ps.setString(5, institutionDto.getSchoolType());
				ps.setString(6, institutionDto.getResidentType());
				ps.setString(7, institutionDto.getJbdw());
				ps.setString(8, institutionDto.getJbzlx());
				ps.setString(9, institutionDto.getContactPerson());
				ps.setString(10, institutionDto.getContactTel());
				ps.setString(11, institutionDto.getEmail());
				ps.setTimestamp(12, new Timestamp(System.currentTimeMillis()));
				ps.setString(13, SFZY);
				ps.setString(14, institutionDto.getAffiliatedSchool());
				ps.setString(15, institutionDto.getUnitType());
				ps.setString(16, institutionDto.getUnitCode());
				//生成机构管理员	新增机构时，系统自动创建机构用户，用户名为机构代码，并将用户的角色自动设置为机构管理员，密码默认为123456.
			}
		});
		institutionDto.setInstitutionCode(inCode);
		sysUserManagerExt.add(institutionDto);

	}

	/**
	 * 根据规则生成机构代码
	 * @param institutionDto 机构信息Dto
	 * @param userType 用户类型
	 */
	private String generateInstitutionCode(final InstitutionsInfoDto institutionDto, String userType) {
		String institutionCode = "";
		if ("10".equals(institutionDto.getAffiliatedSchool())) {
			//本科院校
			institutionCode = "0100" + "D";
			String s = getSequenceBK();
			for (int i = 0; i < 3 - s.length(); i++) {
				institutionCode += "0";
			}
			institutionCode += getSequenceBK();
		}
		if ("20".equals(institutionDto.getAffiliatedSchool())) {
			//高等职业院校
			institutionCode = "0100" + "C";
			String s = getSequenceGZ();
			for (int i = 0; i < 3 - s.length(); i++) {
				institutionCode += "0";
			}
			institutionCode += getSequenceGZ();
		}
		if ("30".equals(institutionDto.getAffiliatedSchool())) {
			//中小学
			/*if("1".equals(institutionDto.getUnitType())){
				//中小学
				 institutionCode=institutionDto.getDistrictCode()+;
			}*/
			if ("2".equals(institutionDto.getUnitType()) || "9".equals(institutionDto.getUnitType())) {
				//直属机构
				if ("1".equals(userType)) {//市级
					institutionCode = "0100" + "9";
					String s = getSequenceZS();
					for (int i = 0; i < 3 - s.length(); i++) {
						institutionCode += "0";
					}
					institutionCode += getSequenceZS();
				} else if ("2".equals(userType)) {//区级
					institutionCode = institutionDto.getDistrictCode().substring(2, 6) + "9";
					String s = getSequenceZS();
					for (int i = 0; i < 3 - s.length(); i++) {
						institutionCode += "0";
					}
					institutionCode += getSequenceZS();
				}
			}
			if ("3".equals(institutionDto.getUnitType())) {
				//研修机构
				if ("1".equals(userType)) {//市级
					institutionCode = "0100" + "E";
					String s = getSequenceYX();
					for (int i = 0; i < 3 - s.length(); i++) {
						institutionCode += "0";
					}
					institutionCode += getSequenceYX();
				} else if ("2".equals(userType)) {//区级
					institutionCode = institutionDto.getDistrictCode().substring(2, 6) + "E";
					String s = getSequenceYX();
					for (int i = 0; i < 3 - s.length(); i++) {
						institutionCode += "0";
					}
					institutionCode += getSequenceYX();
				}
			}
			if ("4".equals(institutionDto.getUnitType())) {
				//校外机构
				if ("1".equals(userType)) {//市级
					institutionCode = "0100" + "F";
					String s = getSequenceXW();
					for (int i = 0; i < 3 - s.length(); i++) {
						institutionCode += "0";
					}
					institutionCode += getSequenceXW();
				} else if ("2".equals(userType)) {//区级
					institutionCode = institutionDto.getDistrictCode().substring(2, 6) + "F";
					String s = getSequenceXW();
					for (int i = 0; i < 3 - s.length(); i++) {
						institutionCode += "0";
					}
					institutionCode += getSequenceXW();
				}
			}
		}
		if ("40".equals(institutionDto.getAffiliatedSchool())) {
			//中等职业学校
			institutionCode = institutionDto.getDistrictCode().substring(2, 6) + "B";
			String s = getSequenceZD();
			for (int i = 0; i < 3 - s.length(); i++) {
				institutionCode += "0";
			}
			institutionCode += getSequenceZD();
		}
		/*if ("50".equals(institutionDto.getAffiliatedSchool())) {
			//特殊教育学校
			institutionCode=institutionDto.getDistrictCode()+;
		}*/
		if ("60".equals(institutionDto.getAffiliatedSchool())) {
			//幼儿园
			institutionCode = institutionDto.getDistrictCode().substring(2, 6) + "A";
			String s = getSequenceYEY();
			for (int i = 0; i < 3 - s.length(); i++) {
				institutionCode += "0";
			}
			institutionCode += getSequenceYEY();
		}
		return institutionCode;
	}

	/**
	 * 查询机构信息
	 */
	@Override
	public InstitutionsInfoDto get(long institutionId) {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("institutionId", institutionId);
			List<InstitutionsInfoDto> dtos = this.findList("InstitutionsManageExt.get.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					InstitutionsInfoDto dto = new InstitutionsInfoDto();
					dto.setInstitutionId(Integer.valueOf(rs.getString("institutionId")));
					dto.setInstitutionCode(rs.getString("institutionCode"));
					dto.setInstitutionName(rs.getString("institutionName"));
					dto.setInstitutionAddress(rs.getString("institutionAddress"));
					dto.setContactPerson(rs.getString("contactPerson"));
					dto.setContactTel(rs.getString("contactTel"));
					dto.setEmail(rs.getString("email"));
					dto.setDistrictCode(rs.getString("districtCode"));
					dto.setJbdw(rs.getString("jbdw"));
					dto.setJbzlx(rs.getString("jbzlx"));
					dto.setResidentType(rs.getString("residentType"));
					dto.setSchoolType(rs.getString("schoolType"));
					dto.setAffiliatedSchool(rs.getString("affiliatedSchool"));
					dto.setUnitType(rs.getString("unitType"));
					dto.setUnitCode(rs.getString("unitCode"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("InstitutionsManageExt get() is error!", e);
		}
		return null;
	}

	/**
	 * 编辑
	 */
	@Override
	public int edit(InstitutionsInfoDto institutionDto) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("institutionId", institutionDto.getInstitutionId());
		params.put("institutionName", institutionDto.getInstitutionName());
		params.put("institutionAddress", institutionDto.getInstitutionAddress());
		params.put("contactPerson", institutionDto.getContactPerson());
		params.put("contactTel", institutionDto.getContactTel());
		params.put("email", institutionDto.getEmail());
		params.put("jbdw", institutionDto.getJbdw());
		params.put("jbzlx", institutionDto.getJbzlx());
		params.put("residentType", institutionDto.getResidentType());
		params.put("schoolType", institutionDto.getSchoolType());
		params.put("unitCode", institutionDto.getUnitCode());
		try {
			ISqlElement processSql = processSql(params, "InstitutionsManageExt.edit.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("InstitutionsManageExt edit() is error!", e);
		}
		return result;
	}

	/**
	 * 删除机构
	 */
	@Override
	public int delete(String[] ck) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ids", ck);
		params.put("sfzy", NOSFZY);
		try {
			ISqlElement processSql = processSql(params, "InstitutionsManageExt.delete.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("InstitutionsManageExt delete() is error!", e);
		}
		deleteUsers(ck);
		return result;

	}

	/**
	 * 删除对应的机构管理员
	 * 
	 * <p>
	 * TODO(这里描述这个方法详情– 可选)
	 * TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	public int deleteUsers(String[] ck) {
		String[] a = new String[ck.length];
		for (int i = 0; i < ck.length; i++) {
			InstitutionsInfoDto institutionsInfo = get(Long.parseLong(ck[i]));
			a[i] = "'" + institutionsInfo.getInstitutionCode() + "'";
		}
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ids", a);
		params.put("sfzy", NOSFZY);
		try {
			ISqlElement processSql = processSql(params, "InstitutionsManageExt.deleteUsers.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("InstitutionsManageExt deleteUsers() is error!", e);
		}
		return result;
	}

	/**
	 * 获取用户
	 * 
	 */
	@Override
	public UserDto getUser(long userId) {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("userId", userId);
			List<UserDto> dtos = this.findList("InstitutionsManageExt.getUser.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					UserDto dto = new UserDto();
					dto.setUserId(Integer.valueOf(rs.getString("userId")));
					dto.setUserType(rs.getString("userType"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("InstitutionsManageExt getUser() is error!", e);
		}
		return null;
	}

	/**
	 * 获取机构信息
	 * <p>
	 * 通过机构代码
	 * 
	 */
	@Override
	public InstitutionsInfoDto getInstitution(String institutionCode) {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("institutionCode", institutionCode);
			List<InstitutionsInfoDto> dtos = this.findList("InstitutionsManageExt.getInstitution.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							InstitutionsInfoDto dto = new InstitutionsInfoDto();
							dto.setInstitutionId(Integer.valueOf(rs.getString("institutionId")));
							dto.setInstitutionCode(rs.getString("institutionCode"));
							dto.setInstitutionName(rs.getString("institutionName"));
							dto.setInstitutionAddress(rs.getString("institutionAddress"));
							dto.setContactPerson(rs.getString("contactPerson"));
							dto.setContactTel(rs.getString("contactTel"));
							dto.setEmail(rs.getString("email"));
							dto.setDistrictCode(rs.getString("districtCode"));
							dto.setJbdw(rs.getString("jbdw"));
							dto.setJbzlx(rs.getString("jbzlx"));
							dto.setResidentType(rs.getString("residentType"));
							dto.setSchoolType(rs.getString("schoolType"));
							dto.setAffiliatedSchool(rs.getString("affiliatedSchool"));
							return dto;
						}
					});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("InstitutionsManageExt getInstitution() is error!", e);
		}
		return null;
	}

	/**
	 * 获取机构信息
	 * <p>
	 * 通过机构代码
	 * 
	 */
	@Override
	public boolean getInstitutionByName(String name,String jgId) {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("name", name);
			List<InstitutionsInfoDto> dtos = this.findList("InstitutionsManageExt.getInstitutionByName.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							InstitutionsInfoDto dto = new InstitutionsInfoDto();
							dto.setInstitutionId(Integer.valueOf(rs.getString("institutionId")));
							dto.setInstitutionCode(rs.getString("institutionCode"));
							dto.setInstitutionName(rs.getString("institutionName"));
							dto.setInstitutionAddress(rs.getString("institutionAddress"));
							dto.setContactPerson(rs.getString("contactPerson"));
							dto.setContactTel(rs.getString("contactTel"));
							dto.setEmail(rs.getString("email"));
							dto.setDistrictCode(rs.getString("districtCode"));
							dto.setJbdw(rs.getString("jbdw"));
							dto.setJbzlx(rs.getString("jbzlx"));
							dto.setResidentType(rs.getString("residentType"));
							dto.setSchoolType(rs.getString("schoolType"));
							dto.setAffiliatedSchool(rs.getString("affiliatedSchool"));
							return dto;
						}
					});
			if (dtos != null) {
				if(dtos.size() > 1){
					return true;
				}else if(dtos.size() == 1 && jgId != null){
					return !dtos.get(0).getInstitutionId().toString().equals(jgId);
				}else{
					return dtos.size() > 0;
				}
			}else{
				return false;
			}
		} catch (Exception e) {
			logger.error("InstitutionsManageExt getInstitution() is error!", e);
		}
		return false;
	}

	/**
	 * 获取序列
	 * <p>
	 *获取本科序列
	 */
	public String getSequenceBK() {
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			List<InstitutionsInfoDto> list = this.findList("InstitutionsManageExt.getSequenceBK.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							InstitutionsInfoDto dto = new InstitutionsInfoDto();
							dto.setInstitutionName(rs.getString("institutionName"));
							return dto;
						}
					});
			return list.get(0).getInstitutionName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取序列
	 * <p>
	 *获取幼儿园序列
	 *
	 */
	public String getSequenceYEY() {
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			List<InstitutionsInfoDto> list = this.findList("InstitutionsManageExt.getSequenceYEY.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							InstitutionsInfoDto dto = new InstitutionsInfoDto();
							dto.setInstitutionName(rs.getString("institutionName"));
							return dto;
						}
					});
			return list.get(0).getInstitutionName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * 获取序列
	 * <p>
	 *中等职业学校
	 */
	public String getSequenceZD() {
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			List<InstitutionsInfoDto> list = this.findList("InstitutionsManageExt.getSequenceZD.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							InstitutionsInfoDto dto = new InstitutionsInfoDto();
							dto.setInstitutionName(rs.getString("institutionName"));
							return dto;
						}
					});
			return list.get(0).getInstitutionName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取序列
	 * <p>
	 *高等职业学校
	 */
	public String getSequenceGZ() {
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			List<InstitutionsInfoDto> list = this.findList("InstitutionsManageExt.getSequenceGZ.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							InstitutionsInfoDto dto = new InstitutionsInfoDto();
							dto.setInstitutionName(rs.getString("institutionName"));
							return dto;
						}
					});
			return list.get(0).getInstitutionName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取序列
	 * <p>
	 *研修机构
	 */
	public String getSequenceYX() {
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			List<InstitutionsInfoDto> list = this.findList("InstitutionsManageExt.getSequenceYX.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							InstitutionsInfoDto dto = new InstitutionsInfoDto();
							dto.setInstitutionName(rs.getString("institutionName"));
							return dto;
						}
					});
			return list.get(0).getInstitutionName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取序列
	 * <p>
	 *校外机构
	 */
	public String getSequenceXW() {
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			List<InstitutionsInfoDto> list = this.findList("InstitutionsManageExt.getSequenceXW.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							InstitutionsInfoDto dto = new InstitutionsInfoDto();
							dto.setInstitutionName(rs.getString("institutionName"));
							return dto;
						}
					});
			return list.get(0).getInstitutionName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取序列
	 * <p>
	 *直属机构
	 */
	public String getSequenceZS() {
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			List<InstitutionsInfoDto> list = this.findList("InstitutionsManageExt.getSequenceZS.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							InstitutionsInfoDto dto = new InstitutionsInfoDto();
							dto.setInstitutionName(rs.getString("institutionName"));
							return dto;
						}
					});
			return list.get(0).getInstitutionName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public ISysUserManagerExt getSysUserManagerExt() {
		return sysUserManagerExt;
	}

	public void setSysUserManagerExt(ISysUserManagerExt sysUserManagerExt) {
		this.sysUserManagerExt = sysUserManagerExt;
	}

	/**
	 * 获取区县列表
	 * 
	 */
	@Override
	public List<DisCodesDto> queryDisCodes(String code) {
		Map<String, Object> params = new HashMap<String, Object>();
		if (code != null && !"".equals(code)) {
			params.put("code", code);
		}
		try {
			List<DisCodesDto> list = this.findList("InstitutionsManageExt.queryDisCodes.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							DisCodesDto dto = new DisCodesDto();
							dto.setDisCode(rs.getString("disCode"));
							dto.setDisName(rs.getString("disName"));
							return dto;
						}
					});
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 查询老师
	 * 
	 */
	@Override
	public List<TeacherAccountInfoDto> queryTeachers(long institutionId) {
		Map<String, Object> params = new HashMap<String, Object>();
		if (institutionId > 0) {
			params.put("xxjgId", institutionId);
		}
		try {
			List<TeacherAccountInfoDto> list = this.findList("InstitutionsManageExt.queryTeachers.query", params,
					new RowMapper() {
						public Object mapRow(ResultSet rs, int arg1) throws SQLException {
							TeacherAccountInfoDto dto = new TeacherAccountInfoDto();
							dto.setJsId(Integer.valueOf(rs.getString("jsId")));
							dto.setEducationId(rs.getString("educationId"));
							dto.setTeacherName(rs.getString("teacherName"));
							dto.setCardNo(rs.getString("cardNo"));
							dto.setBirthday(rs.getString("birthday"));
							dto.setAccountStatus(rs.getString("accountStatus"));
							return dto;
						}
					});
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 修改密码
	 * 
	 */
	@Override
	public int modifyPassWord(long userId, String newPassWord) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", userId);
		params.put("newpw", newPassWord);
		params.put("modifyTime", new Timestamp(System.currentTimeMillis()));

		try {
			ISqlElement processSql = processSql(params, "InstitutionsManageExt.modifyPassWord.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("InstitutionsManageExt modifyPassWord() is error!", e);
		}
		return result;

	}

	/**
	 * 查询用户
	 * 
	 */
	@Override
	public SysUser queryOldPassWord(long userId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", userId);
		try {
			List<SysUser> list = this.findList("InstitutionsManageExt.queryOldPassWord.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					SysUser dto = new SysUser();
					dto.setUserPwd(rs.getString("passWord"));

					return dto;
				}
			});
			return list.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}
	
	/**
	 * 获取字典信息
	 * @param params
	 * @return
	 */
	public List findNameByType(Map<String, Object> params) {
		try {
			List<ZxdwlbZdxbDto> dtos = this.findList("InstitutionsManageExt.findNameByType.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					ZxdwlbZdxbDto dto = new ZxdwlbZdxbDto();
					dto.setZdxmc(rs.getString("zdxbm") + "-" + rs.getString("zdxmc"));
					dto.setZdxbm(rs.getString("zdxbm"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos;
			}
		} catch (Exception e) {
			logger.error("gwlbSelct doSearch() is error!", e);
		}
		return null;
	}
}
