/**
 * TbBizJszgxxManagerExt.java
 * com.becom.jspt2016.service.ext.impl
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.commons.hibernate.IPage;
import org.nestframework.commons.hibernate.IRowHandler;
import org.nestframework.commons.hibernate.ISqlElement;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.dto.TbBizJszgxxDto;
import com.becom.jspt2016.model.TbBizJszgxx;
import com.becom.jspt2016.service.ext.ITbBizJszgxxManagerExt;

/**
 * 教师资格管理ext
 *
 * @author   zhangchunming 
 * @Date	 2016年10月24日 	 
 */
public class TbBizJszgxxManagerExt extends JdbcRootManager implements ITbBizJszgxxManagerExt {

	protected Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 是否删除(未删除)
	 */
	public String SFSC = "0";

	/**
	 * 是否删除(已删除)
	 */
	public String SC = "1";

	/**
	 * 未报送
	 */
	public String SHZT = "01";

	/**
	 * 查询（教师资格信息）
	 * @see com.becom.jspt2016.service.ext.ITbBizJszgxxManagerExt#doSearch(long)
	 */
	@Override
	public IPage<TbBizJszgxx> doSearch(long jsid, int pageNo, int pageSize) {

		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("sfsc", SFSC);
			params.put("jsid", jsid);
			IPage<TbBizJszgxx> dtos = this.findPage("TbBizJszgxxManagerExt.doSearch.query",
					"TbBizJszgxxManagerExt.doSearch.count", params, pageNo, pageSize, new IRowHandler<TbBizJszgxx>() {
						public TbBizJszgxx handleRow(ResultSet rs) {
							TbBizJszgxx dto = new TbBizJszgxx();
							try {
								dto.setJszgId(rs.getLong("jszg_id"));
								dto.setJszgzzl(rs.getString("jszgzzl"));
								dto.setJszgzhm(rs.getString("jszgzhm"));
								dto.setRjxk(rs.getString("rjxk"));
								dto.setZsbfrq(rs.getString("zsbfrq"));
								dto.setZsrdjg(rs.getString("zsrdjg"));
								dto.setCjsj(rs.getDate("cjsj"));
								dto.setShzt(rs.getString("shzt"));
							} catch (SQLException e) {
								e.printStackTrace();
							}
							return dto;
						}
					});
			System.out.println(dtos.getPageElements());
			return dtos;
		} catch (Exception e) {
			logger.error("TbBizJszgxxManagerExt doSearch() is error!", e);
		}
		return null;

	}

	/**
	 * 添加
	 * @see com.becom.jspt2016.service.ext.ITbBizJszgxxManagerExt#add(com.becom.jspt2016.model.TbBizJszgxx)
	 */
	@Override
	public void add(final TbBizJszgxx tbBizJszgxx) {
		StringBuffer sql = new StringBuffer();

		sql.append(
				"insert into TB_BIZ_JSZGXX (JSZG_ID,JSID,JSZGZZL,JSZGZHM,RJXK,ZSBFRQ,ZSRDJG,CJSJ,BSRLB,SHZT,SHSJ,SFSC,cjr) ")
				.append("values (SEQ_TB_BIZ_JSZGXX.nextval,?,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, tbBizJszgxx.getTbBizJzgjbxx().getJsid());
				ps.setString(2, tbBizJszgxx.getJszgzzl());
				ps.setString(3, tbBizJszgxx.getJszgzhm());
				ps.setString(4, tbBizJszgxx.getRjxk());
				ps.setString(5, tbBizJszgxx.getZsbfrq());
				ps.setString(6, tbBizJszgxx.getZsrdjg());
				ps.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
				ps.setString(8, "4");
				ps.setString(9, SHZT);
				ps.setTimestamp(10, new Timestamp(System.currentTimeMillis()));
				ps.setString(11, SFSC);
				ps.setString(12, String.valueOf(tbBizJszgxx.getTbBizJzgjbxx().getJsid()));
			}
		});
	}

	/**
	 * 去编辑
	 * @see com.becom.jspt2016.service.ext.ITbBizJzgxxjlxxManagerExt#get(java.lang.String)
	 */
	@Override
	public TbBizJszgxxDto toEdit(long jszgid) {

		try {

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jszgid", jszgid);
			List<TbBizJszgxxDto> dtos = this.findList("TbBizJszgxxManagerExt.toEdit.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					TbBizJszgxxDto dto = new TbBizJszgxxDto();
					dto.setJszgid(rs.getLong("jszg_id"));
					dto.setJszgzzl(rs.getString("jszgzzl"));
					dto.setJszgzhm(rs.getString("jszgzhm"));
					dto.setRjxk(rs.getString("rjxk"));
					dto.setZsbfrq(rs.getString("zsbfrq"));
					dto.setZsrdjg(rs.getString("zsrdjg"));
					return dto;
				}
			});
			if (dtos != null && dtos.size() > 0) {
				return dtos.get(0);
			}
		} catch (Exception e) {
			logger.error("TbBizJszgxxManagerExt toEdit() is error!", e);
		}

		return null;

	}

	/**
	 * 编辑
	 * @see com.becom.jspt2016.service.ext.ITbBizJszgxxManagerExt#edit(java.util.Map)
	 */
	@Override
	public void edit(Map<String, Object> params) {

		try {
			ISqlElement se = processSql(params, "TbBizJszgxxManagerExt.edit.update");
			getJdbcTemplate().update(se.getSql(), se.getParams());
		} catch (Exception e) {
			logger.error("TbBizJszgxxManagerExt edit() is error!", e);
		}

	}

	/**
	 *删除 
	 */
	@Override
	public int delete(String[] ids) {
		int result = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ids", ids);
		params.put("sfsc", SC);
		try {
			ISqlElement processSql = processSql(params, "TbBizJszgxxManagerExt.delete.update");
			getJdbcTemplate().update(processSql.getSql(), processSql.getParams());
		} catch (Exception e) {
			result = 1;
			logger.error("TbBizJszgxxManagerExt delete() is error!", e);
		}
		return result;
	}

}
