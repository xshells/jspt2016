/**
 * IProtTechAppoinManagerExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.List;
import java.util.Map;

import org.nestframework.commons.hibernate.IPage;

import com.becom.jspt2016.dto.TbCfgZdxbInfoDto;
import com.becom.jspt2016.model.TbBizJzgzyjszwprxx;

/**
 * 岗位聘任接口
 * @author   董锡福
 * @Date	 2016年10月19日 	 
 */
public interface IProtTechAppoinManagerExt {
	IPage<TbBizJzgzyjszwprxx> doSearch(Map<String, Object> params, int pageNo, int pageSize);

	public void SubAddpoint(TbBizJzgzyjszwprxx kh);

	//修改
	public int updateProtech(TbBizJzgzyjszwprxx tbbizjzgzyjszwprxx);

	//删除
	public int removeProTech(String[] ck);

	//回显
	public TbBizJzgzyjszwprxx editProtech(long zyjszwId);

	//树结构
	public List<TbCfgZdxbInfoDto> searchData();
}
