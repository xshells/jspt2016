/**
 * ILoginUserManagerExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.List;
import java.util.Map;

import com.becom.jspt2016.dto.TbJzgxxshLogDto;
import com.becom.jspt2016.dto.TeacherNumDto;
import com.becom.jspt2016.dto.UserTodoDto;
import com.becom.jspt2016.model.SysFunc;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbxx;

/**
 * 用户权限
 * <p>
 *
 * @author   YuChengCheng
 * @Date	 2016年10月16日 	 
 */
public interface ILoginUserManagerExt {

	/**
	 * 
	 * 根据EduId查询功能列表(教師)
	 * <p>
	 *
	 * @param EduId 教育id
	 * @return 功能列表
	 */
	public List<SysFunc> getMenuByEduId(String eduId);

	/**
	 * 
	 * 根据userId查询功能列表(机构)
	 * <p>
	 *
	 * @param userId
	 * @return 功能列表 
	 */
	public List<SysFunc> getMenuByUserId(String userId);

	/**
	 * 
	 * 根据用户名和密码查找机构信息(用户表)
	 * <p>
	 *
	 * @param userName
	 * @param pwd
	 * @return SysUser
	 */
	public SysUser findOrgByUserNameAndPwd(String userName, String pwd);

	/**
	 * 
	 * 根据教育id查找教师信息
	 * <p>
	 *
	 * @param eduId
	 * @return TbBizJzgjbxx
	 */
	public TbBizJzgjbxx findTeaByEduId(String eduId);

	/**
	 * 根据机构用户加载教师管理菜单
	 * @param jgId
	 * @return
	 */
	public List<SysFunc> getMenuByJgId(String jgId);

	/**
	 * 根据教师id获取教师用户信息
	 * @param tid
	 * @return
	 */
	public TbBizJzgjbxx findTeaByJsId(String tid);

	public SysUser handleUser(SysUser user);

	public List<SysFunc> findFunAll();

	/**
	 * 教师审核状态
	 * <p>
	 *
	 *
	 * @param jsid 教师id
	 * @return 待办事项
	 */
	public TbJzgxxshLogDto teacherTodo(long jsid);

	/**
	 * 用户待办事项
	 *
	 * @param userId 用户id
	 * @param managerType 用户类型
	 * @param id 校id或区县id
	 * @return 待办事项列表
	 */
	public List<UserTodoDto> userTodo(long userId, String managerType, String id);

	/**
	 * 教师数量
	 * <p>
	 * 区县、市教师数量
	 *
	 * @param managerType 用户类型（区县：'2',市：'1'）
	 * @param string 
	 * @return 教师数量统计信息
	 */
	public Map<String, Object> getTeacherNum(String managerType, String string, String ssxd);

	List<TeacherNumDto> queryTeacherNum(String managerType, String id);

}
