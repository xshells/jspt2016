/**
 * ITeacherChangeExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.Map;

import org.nestframework.commons.hibernate.IPage;

import com.becom.jspt2016.dto.TeacherAccountInfoDto;
import com.becom.jspt2016.dto.TeacherChangeDto;

/**
 * 教师变动业务接口
 * <p>
 * TODO()
 *
 * @author   fmx
 * @Date	 2016年10月26日 	 
 */
public interface ITeacherChangeExt {

	/**
	 * 搜索
	 * <p>
	 *
	 * @param param
	 * @param pageNo
	 * @param pageSize
	*/

	public IPage doSearch(Map<String, Object> param, int pageNo, int pageSize);

	/**
	 * 添加
	 * <p>
	 *
	 * @param institutionsInfo
	*/

	public void add(TeacherChangeDto changeDto);

	/**
	 * 编辑
	 * <p>
	 *
	 * @param institutionsInfo TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	*/

	public int edit(TeacherChangeDto changeDto);

	/**
	 *删除
	 * <p>
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	*/

	public int delete(String[] ck);
	
	/**
	 * 获取指定对象
	 * @param param
	 * @return
	 */
	public TeacherChangeDto get(Map<String, Object> param);
	
	/**
	 * 根据身份证号查询教师信息
	 * @param card
	 * @return
	 */
	public Map<String, Object> getTeacher(String card);
	
	/**
	 * 调出审核列表加载，功能已被废弃
	 * @param param
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public IPage doSearchC(Map<String, Object> param, int pageNo, int pageSize);
	
	/**
	 * 获取机构信息
	 * @param jgId
	 * @return
	 */
	public Map<String, Object> getJg(String jgId);
	/**
	 * 调出审核 审核，功能已被废弃
	 * @param param
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public int sub(Map<String, Object> param);
	/**
	 * 调出审核 审核全部，功能已被废弃
	 * @param param
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public int subAll(Map<String, Object> param);
	
	/**
	 * 查询教师调动信息
	 * @param param
	 * @return
	 */
	public IPage<TeacherAccountInfoDto> findTeacherMove(Map<String, Object> param, int pageNo, int pageSize);

}
