/**
 * ITbCfgXzqhdmManagerExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.List;

import com.becom.jspt2016.dto.TbCfgXzqhdmDto;

/**
 * 行政区划代码服务
 *
 * @author   ZhuanJunxiang
 * @Date	 2016年10月22日 	 
 */
public interface ITbCfgXzqhdmManagerExt {

	List<TbCfgXzqhdmDto> findAll();

}
