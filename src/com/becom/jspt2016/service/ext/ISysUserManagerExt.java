/**
 * ISysUserManagerExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import com.becom.jspt2016.dto.InstitutionsInfoDto;

/**
 * 用户接口
 * <p>
 * @author   kuangxiang
 * @Date	 2016年10月21日
 */

public interface ISysUserManagerExt {

	/**
	 * 添加用户
	 * <p>
	 * 添加机构时添加用户
	 *
	 * @param inCode 机构代码
	*/

	public void add(InstitutionsInfoDto institutionDto);

}
