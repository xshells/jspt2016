/**
 * ITbJzgxxshManagerExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import com.becom.jspt2016.model.TbJzgxxsh;

/**
 * 教职工信息审核服务
 *
 * @author   zhangchunming
 * @Date	 2016年10月28日 	 
 */
public interface ITbJzgxxshManagerExt {

	/**
	 * 查询
	 * <p>
	 * 根据教师id查询审核状况
	 *
	 * @param jsid
	 */
	public TbJzgxxsh query(long jsid);

}
