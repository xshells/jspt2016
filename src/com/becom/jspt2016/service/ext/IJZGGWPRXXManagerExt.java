/**
 * IJZGGWPRXXManagerExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.List;
import java.util.Map;

import org.nestframework.commons.hibernate.IPage;

import com.becom.jspt2016.dto.TbCfgZdxbInfoDto;
import com.becom.jspt2016.model.TbBizJzggwprxx;

/**
 *教职工岗位聘任接口
 * @author   董锡福
 * @Date	 2016年10月16日 	 
 */
public interface IJZGGWPRXXManagerExt {
	IPage<TbBizJzggwprxx> doSearch(Map<String, Object> params, int pageNo, int pageSize);

	//添加
	public void SubAddpoint(TbBizJzggwprxx kh);

	//修改 
	public int updateJzggwprxx(TbBizJzggwprxx gw);

	//删除
	public int removeGwpr(String[] ck);

	//回显
	public TbBizJzggwprxx editJzggwprxx(long gwprId);

	//权限查询
	public String getXd(Map<String, Object> params);

	//岗位等级字典查询
	public List SelectPostGrade(Map<String, Object> params);

	//岗位等级字典中小学查询
	public List<TbCfgZdxbInfoDto> queryConclusion();

	//党政职务下拉高校
	public List<TbCfgZdxbInfoDto> queryDzzw();

	//党政职务下拉中等
	public List<TbCfgZdxbInfoDto> queryMiddle();
}
