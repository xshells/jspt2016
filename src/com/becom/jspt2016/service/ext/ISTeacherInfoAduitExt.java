/**
 * ISTeacherInfoAduitAction.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.Map;

import org.nestframework.commons.hibernate.IPage;

import com.becom.jspt2016.dto.InstitutionsInfoDto;
import com.becom.jspt2016.model.SysUser;

/**
 * 教师信息审核
 * <p>
 *学校机构接口
 * @author   kuangxiang
 * @Date	 2016年10月27日
 */

public interface ISTeacherInfoAduitExt {

	/**
	 * 搜索
	 * <p>
	 *
	 * @param param
	 * @param pageNo
	 * @param pageSize
	*/

	public IPage doSearch(Map<String, Object> param, int pageNo, int pageSize);

	/**
	 * 审核
	 * <p>
	 * 学校审核
	 *
	 * @param ck 教职工Id 数组
	 * @param isAdopt 是否通过
	 * @param aduitOpinion  审核意见
	 * @param institution 机构信息
	 * @param sUser  登录用户信息
	 * @param ip 登录这ip地址
	*/

	public void aduit(String[] ck, String isAdopt, String aduitOpinion, InstitutionsInfoDto institution, SysUser sUser,
			String ip);

}
