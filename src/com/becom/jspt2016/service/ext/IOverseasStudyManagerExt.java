/**
 * ITbBizJzghwyxExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.Map;

import org.nestframework.commons.hibernate.IPage;

import com.becom.jspt2016.dto.HwyxxxInfoDto;
import com.becom.jspt2016.model.TbBizJzghwyxxx;

/**
 * 海外研修
 * @author   wxl
 * @Date	 2016年10月19日 	 
 */
public interface IOverseasStudyManagerExt {

	/**
	 * 海外研修(访学)列表--目前定位一条
	 * <p>
	 * @param param查询参数
	 * @param pageNo 页码
	 * @param pageSize 每页显示个数
	*/
	IPage<TbBizJzghwyxxx> pageSearch(Map<String, Object> params, int pageNo, int pageSize);

	/**
	 *添加海外研修(访学)
	 * <p>
	 * @param hwyxxx 海外研修(访学)对象
	*/
	public void insertOverseasStudy(TbBizJzghwyxxx hwyxxx);

	/**
	 *   删除
	 * <p>
	 * @param ids 海外研修(访学)对象
	*/
	public int delete(String[] ids);

	/**
	 *   查询
	 * <p>
	 * @param hwyxId 海外研修(访学)对象
	*/
	public HwyxxxInfoDto getOverseasStudy(long hwyxId);

	/**
	 *   更新
	 * <p>
	 * @param btBizJzghwyxInfo 海外研修(访学)对象
	*/
	public int updateOverseasStudy(TbBizJzghwyxxx btBizJzghwyxInfo);

}
