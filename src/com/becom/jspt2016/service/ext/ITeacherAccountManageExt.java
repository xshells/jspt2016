/**
 * ITeacherAccountManageExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.Map;

import org.nestframework.commons.hibernate.IPage;

import com.becom.jspt2016.dto.InstitutionsInfoDto;
import com.becom.jspt2016.dto.TeacherAccountInfoDto;
import com.becom.jspt2016.model.TbBizJzgjbxx;

/**
 * 教师账号管理接口
 * <p>
 * @author   kuangxiang
 * @author   zhangchunming
 * @Date	 2016年10月17日
 * @Date	 2016年11月1日 
 */

public interface ITeacherAccountManageExt {

	/**
	 *删除
	 * <p>
	 * @param ids 教职工Id
	*/

	public int delete(String[] ids);

	/**
	 * 找回账号
	 * <p>
	 * @param ids教职工Id
	*/
	public int retrieveAccount(String[] ids);

	/**
	 * 教师账号管理列表
	 * <p>
	 *
	 * @param param查询参数
	 * @param pageNo 页码
	 * @param pageSize 每页显示个数
	*/

	public IPage doSearch(Map<String, Object> param, int pageNo, int pageSize);

	/**
	 * 添加
	 * <p>
	 *添加教师账号
	 * @param teacherInfo 老师基本信息对象
	*/

	public void add(final TbBizJzgjbxx teacherInfo, final InstitutionsInfoDto institution);

	/**
	 * 编辑
	 * <p>
	 * @param teacherInfo 教师基本信息
	*/

	public int edit(TbBizJzgjbxx teacherInfo, String editName);

	/**
	 * TODO(这里用一句话描述这个方法的作用)
	 * <p>
	 * TODO(这里描述这个方法详情– 可选)
	 *
	 * @param jsId
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	*/

	public TeacherAccountInfoDto get(long jsId);

	/**
	 * TODO(这里用一句话描述这个方法的作用)
	 * <p>
	 * TODO(这里描述这个方法详情– 可选)
	 *
	 * @param disCode TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	*/

	public String getSequenceSJ(String disCode);

	/**
	 * TODO(这里用一句话描述这个方法的作用)
	 * <p>
	 * TODO(这里描述这个方法详情– 可选)
	 *
	 * @param string
	 * @param disCode
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	*/

	public String getEduId(String string, String disCode);

	public TeacherAccountInfoDto checkAddHm(String shenfen, String tCNo);

	public TeacherAccountInfoDto checkEditHm(String shenfen, String tCNo, long jsId);
	
	public IPage findTeacherInfo(Map<String, Object> param, int pageNo, int pageSize);
	public int updateCallOut(String[] ids) ;
}
