/**
 * IInstitutionsManageExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.List;
import java.util.Map;

import org.nestframework.commons.hibernate.IPage;

import com.becom.jspt2016.dto.DisCodesDto;
import com.becom.jspt2016.dto.InstitutionsInfoDto;
import com.becom.jspt2016.dto.TeacherAccountInfoDto;
import com.becom.jspt2016.dto.UserDto;
import com.becom.jspt2016.model.SysUser;

/**
 * 机构管理业务接口
 * <p>
 * 包括区级和市级
 * @author   kuangxiang
 * @Date	 2016年10月18日
 */

public interface IInstitutionsManageExt {

	/**
	 * 搜索
	 * <p>
	 * 机构管理搜索
	 *
	 * @param param
	 * @param pageNo
	 * @param pageSize
	*/

	public IPage doSearch(Map<String, Object> param, int pageNo, int pageSize);

	/**
	 * 添加
	 * <p>
	 * 添加机构
	 *
	 * @param institutionsInfo 机构信息
	*/

	public void add(InstitutionsInfoDto institutionDto, String userType);

	/**
	 * 查询机构信息
	 * <p>
	 * 通过机构Id
	 *
	 * @param institutionId 机构Id
	*/

	public InstitutionsInfoDto get(long institutionId);

	/**
	 * 编辑
	 * <p>
	 * 编辑机构
	 *
	 * @param institutionsInfo TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	*/

	public int edit(InstitutionsInfoDto institutionDto);

	/**
	 *删除
	 * <p>
	 * 删除机构
	 *
	 * @param ck  机 构Id组成的字符串
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	*/

	public int delete(String[] ck);

	/**
	 * 获取用户
	 * <p>
	 * @param userId 用户Id
	*/

	public UserDto getUser(long userId);

	/**
	 * 查询机构信息
	 * <p>
	 *
	 * @param institutionCode 机构代码
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	*/

	public InstitutionsInfoDto getInstitution(String institutionCode);

	/**
	 * 获取区县列表
	 * <p>
	 * TODO(这里描述这个方法详情– 可选)
	 *
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	*/

	public List<DisCodesDto> queryDisCodes(String code);

	/**
	 * 查询老师
	 * <p>
	 *
	 * @param institutionId机构Id
	*/

	public List<TeacherAccountInfoDto> queryTeachers(long institutionId);

	/**
	 * 修改密码
	 * <p>
	 *
	 * @param userId用户Id
	 * @param newPassWord 新密码
	*/

	public int modifyPassWord(long userId, String newPassWord);

	/**
	 * 查询旧密码
	 * <p>
	 * TODO(这里描述这个方法详情– 可选)
	 *
	 * @param oldPassWord
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	*/

	public SysUser queryOldPassWord(long userId);
	
	public boolean getInstitutionByName(String institutionCode,String jgId);
	
	public List findNameByType(Map<String, Object> params);

}
