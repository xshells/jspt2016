/**
 * IInlandTrainManagerExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.Map;

import org.nestframework.commons.hibernate.IPage;

import com.becom.jspt2016.dto.GnpxxxInfoDto;
import com.becom.jspt2016.model.TbBizJzggnpxxx;

/**
 *     国内培训接口
 * @author   wxl
 * @Date	 2016年10月17日 	 
 */
public interface IInlandTrainManagerExt {

	/**
	 * 国内培训列表
	 * <p>
	 * @param param查询参数
	 * @param pageNo 页码
	 * @param pageSize 每页显示个数
	*/
	IPage<TbBizJzggnpxxx> pageSearch(Map<String, Object> params, int pageNo, int pageSize);

	/**
	 *更新
	 * <p>
	 * @param tbBizJzggnpxInfo 国内培训信息
	*/
	public int updateInlandTrain(TbBizJzggnpxxx tbBizJzggnpxInfo);

	/**
	 *添加国内培训
	 * <p>
	 * @param gnpxxx 国内培训对象
	*/
	public void insertInlandTrain(TbBizJzggnpxxx gnpxxx);

	/**
	 *根据id获取对象信息
	 * <p>
	 * @param gnpxId 国内培训Id
	*/
	public GnpxxxInfoDto getInlandTrain(long gnpxId);

	/**
	 *删除
	 * <p>
	 * @param ids 国内培训Id
	*/
	public int delete(String[] ids);

	/**
	 *查询
	 * <p>
	 * @param ids 国内培训Id
	*/
	public String getXd(Map<String, Object> params);

}
