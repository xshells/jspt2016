/**
 * IContactWayManagerExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import com.becom.jspt2016.model.TbBizJzglxfsxx;

/**
 * 	联系方式接口
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   wxl
 * @Date	 2016年10月21日 	 
 */
public interface IContactWayManagerExt {

	/**
	 * 联系方式查询
	 * <p>
	 * @param param 教职工id
	*/
	public TbBizJzglxfsxx getContactWay(long jsid);

	/**
	 * 添加联系方式
	 * <p>
	 * @param param 联系方式对象
	*/
	public void insertContactWay(TbBizJzglxfsxx lxfsxx);

	/**
	 * 更新联系方式
	 * <p>
	 * @param param 已有的联系方式id
	*/
	public int updateContactWay(TbBizJzglxfsxx contactWayInfo);

}
