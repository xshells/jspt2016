/**
 * ITbBizJzgsdxxManager.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.List;
import java.util.Map;

import org.nestframework.commons.hibernate.IPage;

import com.becom.jspt2016.dto.TbBizJzgrxrcxmxxInfoDto;
import com.becom.jspt2016.dto.TbCfgZdxbInfoDto;
import com.becom.jspt2016.model.TbBizJzgrxrcxmxx;

/**
 * 入选人才项目接口
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   shanjigang
 * @Date	 2016年10月16日 	 
 */
public interface ITbBizJzgrxrcxmxxManagerExt {
	public IPage<TbBizJzgrxrcxmxxInfoDto> doQuery(Map<String, Object> params, int pageNo, int pageSize);

	public void addSeclectTeach(final TbBizJzgrxrcxmxx jyjx, final String jsid);

	public void editSeclectTeach(String rxrcxmmc, String rxny, String xmid);

	public void delSeclectTeach(String[] ids);

	public List<TbCfgZdxbInfoDto> searchData();

	public List<TbCfgZdxbInfoDto> searchDatazxx();

	public TbBizJzgrxrcxmxxInfoDto editSelectedTalent(long jsid, long xmid);
}
