/**
 * ITbBizJzgsdxxManager.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.List;
import java.util.Map;

import org.nestframework.commons.hibernate.IPage;

import com.becom.jspt2016.dto.TbBizJzgsdxxInfoDto;
import com.becom.jspt2016.dto.TbCfgZdxbInfoDto;
import com.becom.jspt2016.model.TbBizJzgsdcfxx;
import com.becom.jspt2016.model.TbBizJzgsdkhxx;

/**
 * 师德信息接口
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   shanjigang
 * @Date	 2016年10月16日 	 
 */
public interface ITbBizJzgsdxxManagerExt {
	public IPage<TbBizJzgsdxxInfoDto> doQuery(Map<String, Object> params, int pageNo, int pageSize);

	public void editKaohe(long khid, String khny, String khjl, String sdkhdwmc);

	public void editChufen(long cfid, String cflb, String cfyy, String ccfsrq);

	public void delKaohe(String[] ids);

	public void delChufen(String[] ids);

	public TbBizJzgsdxxInfoDto editTeacherMorality(long jsid, long khid, long cfid);

	public void addTeacherMorality(final TbBizJzgsdkhxx kh, final TbBizJzgsdcfxx cf);

	public void delTeacherMorality(String[] ids);

	public List<TbCfgZdxbInfoDto> queryConclusion();

	public void addPunishmentInfo(final TbBizJzgsdcfxx cf);

	public void delPunishment(String[] ids);

	public TbBizJzgsdxxInfoDto editPunishmentInfo(long jsid, long cfid);

	public IPage<TbBizJzgsdxxInfoDto> doQuerycf(Map<String, Object> params, int pageNo, int pageSize);
}
