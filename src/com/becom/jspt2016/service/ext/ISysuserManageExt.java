package com.becom.jspt2016.service.ext;

import java.util.List;
import java.util.Map;

import org.nestframework.commons.hibernate.IPage;

import com.becom.jspt2016.dto.SysuserDto;

/**
 * 用户管理业务接口
 * <p>
 * @author   fmx
 */

public interface ISysuserManageExt {

	/**
	 * 搜索
	 * <p>
	 *
	 * @param param
	 * @param pageNo
	 * @param pageSize
	*/

	public IPage doSearch(Map<String, Object> param, int pageNo, int pageSize);

	/**
	 * 添加
	 * <p>
	 *
	 * @param institutionsInfo
	*/

	public void add(SysuserDto userDto);

	/**
	 * 编辑
	 * <p>
	 * 编辑机构
	 *
	 * @param institutionsInfo TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	*/

	public int edit(SysuserDto userDto);

	/**
	 *删除
	 * <p>
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	*/

	public int delete(String[] ck);
	
	/**
	 * 获取角色
	 * @param param
	 * @return
	 */
	public List<Map<String,Object>> getRoleList(Map<String, Object> param);
	
	/**
	 * 判断账户是否存在
	 * @param param
	 * @return
	 */
	public Boolean isExist(Map<String, Object> param);
	
	/**
	 * 获取user对象
	 * @param userId
	 * @return
	 */
	public SysuserDto getUser(String userId);
	/**
	 * 设置状态
	 * @param ck
	 * @param status
	 * @return
	 */
	public int setStatus(String[] ck,String status);
	
	/**
	 * 重置密码
	 * @param ck
	 * @return
	 */
	public int reset(String[] ck);
	public List<Map<String,Object>> getFwList();
	
	/**
	 * 获取区名
	 * @param quid
	 * @return
	 */
	public String getQu(String quid);
	
	/**
	 * 获取机构名 
	 * @param jgId
	 * @return
	 */
	public Map<String, Object> getJg(String jgId);
	
	public boolean isCf(String jsId);
	public boolean isSd(String jsId);

}
