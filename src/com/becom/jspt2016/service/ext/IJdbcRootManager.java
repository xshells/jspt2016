/**
 * 
 */
package com.becom.jspt2016.service.ext;

import org.nestframework.commons.hibernate.IJdbcManager;

/**
 * @author audin
 *
 */
public interface IJdbcRootManager extends IJdbcManager {

}
