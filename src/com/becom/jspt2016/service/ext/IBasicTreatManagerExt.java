/**
 * IBasicTreatManagerExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.Map;

import org.nestframework.commons.hibernate.IPage;

import com.becom.jspt2016.model.TbBizJzgjbdyxx;

/**
 * 基本待遇接口(这里用一句话描述这个类的作用)
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   dongxifu
 * @Date	 2016年10月20日 	 
 */
public interface IBasicTreatManagerExt {
	IPage<TbBizJzgjbdyxx> doSearch(Map<String, Object> params, int pageNo, int pageSize);

	//添加
	public void SubAddpoint(TbBizJzgjbdyxx dy);

	//执行修改
	public int updateBasicTreat(TbBizJzgjbdyxx jbdyId);

	//删除
	public int removeBasicTreat(String[] ck);

	//回显
	public TbBizJzgjbdyxx editBasicTreat(long jbdyId);
	
	public String getXd(Map<String,Object> params);
}
