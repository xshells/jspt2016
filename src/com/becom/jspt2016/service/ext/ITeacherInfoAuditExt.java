/**
 * ITeacherInfoAuditExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
/**
 * ITeacherInfoAuditExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.List;
import java.util.Map;

import org.nestframework.commons.hibernate.IPage;

import com.becom.jspt2016.dto.AduitCommentDto;
import com.becom.jspt2016.dto.AduitLogDto;
import com.becom.jspt2016.dto.InstitutionsInfoDto;
import com.becom.jspt2016.model.SysUser;

/**
 * TODO(这里用一句话描述这个类的作用)
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 * @author   kuangxiang
 * @Date	 2016年10月25日
 */

public interface ITeacherInfoAuditExt {

	/**
	 * 搜索
	 * <p>
	 * @param param
	 * @param pageNo
	 * @param pageSize
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	*/

	public IPage doSearch(Map<String, Object> param, int pageNo, int pageSize);

	/**
	 *审核
	 * <p>
	 *
	 * @param ck
	*/

	public void aduit(String[] ck, String isAdopt, String aduitOpinion, InstitutionsInfoDto institution, SysUser sUser,
			String ip);

	/**
	 * 查询审核意见
	 * <p>
	 * @param jsId 教职工Id
	*/

	public List<AduitLogDto> queryAduitComment(long jsId);

	/**
	 * 审核基本信息表
	 * <p>
	 *
	 * @param jsId  教职工ID
	 * @param aduitStatus 审核状态
	 * @param aduitResult 审核结果
	 */
	public int aduitBasicInfo(AduitCommentDto adDto);

	/**
	 * 更新审核表
	 * 1.基本信息表关联机构变查询所属学段，
	 * <p>
	 *
	 */
	public int updateAduit(AduitCommentDto adDto);

	/**
	 * 添加审核日志表
	 * <p>
	 *
	 * @param AduitCommentDto 审核内容dto
	 */
	public void addAduitLog(final AduitCommentDto adDto);

	/**
	 * 教职工基本信息子表审核
	 * <p>
	 * @param adDto 审核内容dto
	 */
	public void childrenTableAduit(AduitCommentDto adDto);

	/**
	 * 查询审核表信息
	 */
	public long fetchAduitBInfo(long jsId);

}
