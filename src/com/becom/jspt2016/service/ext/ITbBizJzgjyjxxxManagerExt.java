/**
 * ITbBizJzgsdxxManager.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.List;
import java.util.Map;

import org.nestframework.commons.hibernate.IPage;

import com.becom.jspt2016.dto.TbBizJzgjyjxxxInfoDto;
import com.becom.jspt2016.dto.TbCfgZdxbInfoDto;
import com.becom.jspt2016.model.TbBizJzgjyjxxx;

/**
 * 师德信息接口
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   shanjigang
 * @Date	 2016年10月16日 	 
 */
public interface ITbBizJzgjyjxxxManagerExt {
	public IPage<TbBizJzgjyjxxxInfoDto> doQuery(Map<String, Object> params, int pageNo, int pageSize);

	public void addTeacherMorality(final TbBizJzgjyjxxx jyjx, final String jsid);

	public void editEducationTeach(String xn, String xq, String dslb, String xzycsxkly, String sfwbkssk, String rkzk,
			String xnbzkkcjxkss, String cjsj, int jyid, String rkxklb, String jrgz, String rjkc, String rjxd,
			String rkkclb, String pjzks);

	public void delEducationTeach(String[] ids);

	public TbBizJzgjyjxxxInfoDto editEduction(int jsid);

	public List<TbCfgZdxbInfoDto> queryConclusion();

	public List<TbCfgZdxbInfoDto> searchData();

	public List<TbCfgZdxbInfoDto> searchDatagz();

	/**
	 * 获取所属学段
	 */
	public String getXd(Map<String, Object> params);

	public List<TbCfgZdxbInfoDto> searchDataxkly();

	public List<TbCfgZdxbInfoDto> searchDatarjkczxx();

	public List<TbCfgZdxbInfoDto> searchDatarjkcts();

	public String getRjkc(String rjkc);

	public String getRjkctj(String rjkc);
}
