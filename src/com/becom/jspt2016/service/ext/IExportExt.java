package com.becom.jspt2016.service.ext;

import java.util.List;
import java.util.Map;

import com.becom.jspt2016.dto.InstitutionsInfoDto;

/**
 * 导入导出服务接口
 * <p>
 * @author   fmx
 */

public interface IExportExt {


	public List<InstitutionsInfoDto> getInstitutionsList(Map<String, Object> param);
	
	public String getZdxName(String zdbs,String zdxbm);
	public String getQXDM(String name);
	public String getZDXBM(String zdbs,String name);
	public void add(final InstitutionsInfoDto institutionDto, final String userType);
	public List<List<Object>> getTeacher(Map<String, Object> param);
	public void addTeacher(final Map<String, Object> map, final InstitutionsInfoDto institution);
	public boolean isExistTea(String cardNo,String name) throws Exception;
	public InstitutionsInfoDto getInstitution(String jgId);
	public String getTeacherInfo(String cardNo,String name) throws Exception;
}
