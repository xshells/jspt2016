/**
 * IExchangeRotationManagerExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.Map;

import org.nestframework.commons.hibernate.IPage;

import com.becom.jspt2016.dto.ExchangeRotationInfoDto;

/**
 * 交流轮岗接口
 * <p>
 * @author   kuangxiang
 * @Date	 2016年10月20日
 */

public interface IExchangeRotationManagerExt {

	/**
	 * 搜索
	 * <p>
	 *查询交流轮岗信息列表
	 * @param param
	 * @param pageNo
	 * @param pageSize
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	*/

	public IPage doSearch(Map<String, Object> param, int pageNo, int pageSize);

	/**
	 * 删除
	 * <p>
	 *删除交流轮岗信息
	 * @param ck
	*/

	public int delete(String[] ck);

	/**
	 * 添加
	 * <p>
	 * @param exchangeRotationInfoDto 交流轮岗信息
	*/

	public void add(ExchangeRotationInfoDto exchangeRotationInfoDto, Long jsId);

	/**
	 * 跳到编辑页面
	 * <p>
	 * @param jllgId交流轮岗ID
	*/

	public ExchangeRotationInfoDto get(long jllgId);

	/**
	 * 更新
	 * <p>
	 * @param exchangeRotationInfoDto
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	*/

	public int edit(ExchangeRotationInfoDto exchangeRotationInfoDto);

}
