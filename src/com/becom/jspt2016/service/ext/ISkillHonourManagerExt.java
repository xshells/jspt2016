/**
 * ISkillHonourManagerExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.Map;

import org.nestframework.commons.hibernate.IPage;

import com.becom.jspt2016.dto.SkillHonourInfoDto;
import com.becom.jspt2016.model.TbBizJzgyynlxx;

/**
 * 技能及证书接口
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 * @author   wxl
 * @Date	 2016年10月20日 	 
 */
public interface ISkillHonourManagerExt {
	/**
	 * 技能及证书列表
	 * <p>
	 * @param param查询参数
	 * @param pageNo 页码
	 * @param pageSize 每页显示个数
	*/
	IPage<TbBizJzgyynlxx> pageSearch(Map<String, Object> params, int pageNo, int pageSize);

	/**
	 *删除
	 * <p>
	 * @param ids 技能及证书Id
	*/
	public int deleteSkillHonour(String[] ids);

	/**
	 *添加技能及证书
	 * <p>
	 * @param gnpxxx 技能及证书对象
	*/
	public void insertSkillHonour(TbBizJzgyynlxx yynlxx);

	/**
	 *根据id获取对象信息
	 * <p>
	 * @param gnpxId 技能及证书Id
	*/
	public SkillHonourInfoDto getSkillHonour(long jnzsId);

	/**
	 *更新
	 * <p>
	 * @param tbBizJzggnpxInfo 技能及证书信息
	*/
	public int updateSkillHonour(TbBizJzgyynlxx tbBizJzgyynlInfo);

	/**
	 *查询
	 * <p>
	 * 添加之前查询，语种不能添加重复
	 * @param jsid 教师id
	 * @param yz  语种
	*/
	public TbBizJzgyynlxx search(Long jsid, String yz);

	/**
	 *查询
	 * <p>
	 *@param jsid 教师id
	 *@param yz  语种
	 *@param jnzsId  技能证书id
	*/
	public TbBizJzgyynlxx searchUpdate(Long jsid, Long jnzsId, String yz);
}
