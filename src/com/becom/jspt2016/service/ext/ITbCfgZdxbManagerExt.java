/**
 * ITbCfgZdxbManagerExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.List;

import com.becom.jspt2016.dto.JzglyZdxbDto;
import com.becom.jspt2016.dto.ZxdwlbZdxbDto;
import com.becom.jspt2016.model.TbCfgZdxbId;

/**
 * 查询字典表
 * <p>
 *
 * @author   YuChengCheng
 * @Date	 2016年10月18日 	 
 */
public interface ITbCfgZdxbManagerExt {

	public List<TbCfgZdxbId> findAllInfo();

	public List<JzglyZdxbDto> findJzgly(String ssxd);

	public List<ZxdwlbZdxbDto> findZxdwlb();

	public List<TbCfgZdxbId> findNameByType(String dictType, String schoolType);

	public String findNameBybsbmtype(String dictBs, String dictBm,
			String schoolType);
}
