/**
 * ITeacherInforManagerExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.List;
import java.util.Map;

import org.nestframework.commons.hibernate.IPage;

import com.becom.jspt2016.dto.TeacherInfoAuditDto;
import com.becom.jspt2016.dto.TeacherInformtionDto;

/**
 * 教师基本信息接口
 * @author   董锡福
 * @Date	 2016年10月24日 	 
 */
public interface ITeacherInforManagerExt {
	IPage<TeacherInfoAuditDto> doSearch(Map<String, Object> params, int pageNo, int pageSize);

	public List<TeacherInformtionDto> queryAdress();
}
