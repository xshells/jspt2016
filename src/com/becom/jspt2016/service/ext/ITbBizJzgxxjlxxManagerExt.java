/**
 * ITbBizJzgxxjlxxManagerExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.List;
import java.util.Map;

import org.nestframework.commons.hibernate.IPage;

import com.becom.jspt2016.dto.TbBizJzgxxjlxxDto;
import com.becom.jspt2016.dto.TbCfgZdxbInfoDto;
import com.becom.jspt2016.model.TbBizJzgxxjlxx;

/**
 * 教职工学习经历管理
 *
 * @author   zhangchunming
 * 
 * @Date	 2016年10月17日 	 
 */
public interface ITbBizJzgxxjlxxManagerExt {

	public IPage<TbBizJzgxxjlxx> doSearch(Map<String, Object> map, int pageNo, int pageSize);

	public void add(TbBizJzgxxjlxx tbBizJzgxxjlxx, String ssxd);

	public TbBizJzgxxjlxxDto get(long xlId);

	public void edit(Map<String, Object> params, String ssxd);

	int delete(String[] ids);

	public List<TbCfgZdxbInfoDto> queryConclusion(String zdxbm);

	public List<TbCfgZdxbInfoDto> queryConclusion();
}
