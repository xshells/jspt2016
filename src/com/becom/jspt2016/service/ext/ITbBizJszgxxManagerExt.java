/**
 * ITbBizJszgxxManagerExt.java
 * com.becom.jspt2016.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.service.ext;

import java.util.Map;

import org.nestframework.commons.hibernate.IPage;

import com.becom.jspt2016.dto.TbBizJszgxxDto;
import com.becom.jspt2016.model.TbBizJszgxx;

/**
 * 教师资格管理
 *
 * @author   zhangchunming
 * @Date	 2016年10月24日 	 
 */
public interface ITbBizJszgxxManagerExt {

	IPage<TbBizJszgxx> doSearch(long jsid, int pageNo, int pageSize);

	void add(TbBizJszgxx tbBizJszgxx);

	TbBizJszgxxDto toEdit(long jszgid);

	void edit(Map<String, Object> params);

	int delete(String[] ck);

}
