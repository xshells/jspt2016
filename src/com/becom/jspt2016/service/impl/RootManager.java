package com.becom.jspt2016.service.impl;

import java.io.Serializable;

import org.nestframework.commons.hibernate.HibernateManagerSupport;

import com.becom.jspt2016.service.IRootManager;

public abstract class RootManager<T, K extends Serializable> extends HibernateManagerSupport<T, K> implements IRootManager<T, K> {

}