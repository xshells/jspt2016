package com.becom.jspt2016.service;
// Generated 2016-10-16 11:29:37 by Hibernate Tools 3.2.0.CR1 with nest-tools


import java.io.Serializable;
import java.util.Date;
import java.util.List;

public interface IBaseManager<T, K extends Serializable> extends IRootManager<T, K> {
	public T get(K id);
	public void save(T instance);
	public void remove(T instance);
	public void removeById(K id);
	public List<T> findAll();
	public List<T> findByExample(T instance);
	public List<T> findByExample(T instance, int firstResult, int maxResults);
	
	public Date getCurrentDateOfDatabase(); 
}
