package com.becom.jspt2016.model;

// Generated 2016-10-16 11:29:08 by Hibernate Tools 3.2.0.CR1

import java.util.Date;

/**
 * TbBizJzgjyjxxx generated by hbm2java
 */
public class TbBizJzgjyjxxx implements java.io.Serializable {

	private long jyjxId;
	private TbBizJzgjbxx tbBizJzgjbxx;
	private String xn;
	private String xq;
	private String rjxd;
	private String rkzk;
	private String rkzkqtqk;
	private String rkkclb;
	private String rkxklb;
	private String rjkc;
	private String pjzks;
	private String drdqtgz;
	private String pjmzqtgzzhjxkss;
	private String jrgz;
	private String jrqtgzmc;
	private String sfwbkssk;
	private String dslb;
	private String xzycsxkly;
	private String xnbzkkcjxkss;
	private String xnqtgzzhkss;
	private String bsrlb;
	private String shzt;
	private Date shsj;
	private String shjg;
	private String sfsc;
	private String cjr;
	private Date cjsj;
	private String xgr;
	private Date xgsj;

	public TbBizJzgjyjxxx() {
	}

	public TbBizJzgjyjxxx(long jyjxId, TbBizJzgjbxx tbBizJzgjbxx, String bsrlb, String shzt, Date shsj, String shjg,
			String sfsc, Date cjsj) {
		this.jyjxId = jyjxId;
		this.tbBizJzgjbxx = tbBizJzgjbxx;
		this.bsrlb = bsrlb;
		this.shzt = shzt;
		this.shsj = shsj;
		this.shjg = shjg;
		this.sfsc = sfsc;
		this.cjsj = cjsj;
	}

	public TbBizJzgjyjxxx(long jyjxId, TbBizJzgjbxx tbBizJzgjbxx, String xn, String xq, String rjxd, String rkzk,
			String rkzkqtqk, String rkkclb, String rkxklb, String rjkc, String pjzks, String drdqtgz,
			String pjmzqtgzzhjxkss, String jrgz, String jrqtgzmc, String sfwbkssk, String dslb, String xzycsxkly,
			String xnbzkkcjxkss, String xnqtgzzhkss, String bsrlb, String shzt, Date shsj, String shjg, String sfsc,
			String cjr, Date cjsj, String xgr, Date xgsj) {
		this.jyjxId = jyjxId;
		this.tbBizJzgjbxx = tbBizJzgjbxx;
		this.xn = xn;
		this.xq = xq;
		this.rjxd = rjxd;
		this.rkzk = rkzk;
		this.rkzkqtqk = rkzkqtqk;
		this.rkkclb = rkkclb;
		this.rkxklb = rkxklb;
		this.rjkc = rjkc;
		this.pjzks = pjzks;
		this.drdqtgz = drdqtgz;
		this.pjmzqtgzzhjxkss = pjmzqtgzzhjxkss;
		this.jrgz = jrgz;
		this.jrqtgzmc = jrqtgzmc;
		this.sfwbkssk = sfwbkssk;
		this.dslb = dslb;
		this.xzycsxkly = xzycsxkly;
		this.xnbzkkcjxkss = xnbzkkcjxkss;
		this.xnqtgzzhkss = xnqtgzzhkss;
		this.bsrlb = bsrlb;
		this.shzt = shzt;
		this.shsj = shsj;
		this.shjg = shjg;
		this.sfsc = sfsc;
		this.cjr = cjr;
		this.cjsj = cjsj;
		this.xgr = xgr;
		this.xgsj = xgsj;
	}

	public long getJyjxId() {
		return this.jyjxId;
	}

	public void setJyjxId(long jyjxId) {
		this.jyjxId = jyjxId;
	}

	public TbBizJzgjbxx getTbBizJzgjbxx() {
		return this.tbBizJzgjbxx;
	}

	public void setTbBizJzgjbxx(TbBizJzgjbxx tbBizJzgjbxx) {
		this.tbBizJzgjbxx = tbBizJzgjbxx;
	}

	public String getXn() {
		return this.xn;
	}

	public void setXn(String xn) {
		this.xn = xn;
	}

	public String getXq() {
		return this.xq;
	}

	public void setXq(String xq) {
		this.xq = xq;
	}

	public String getRjxd() {
		return this.rjxd;
	}

	public void setRjxd(String rjxd) {
		this.rjxd = rjxd;
	}

	public String getRkzk() {
		return this.rkzk;
	}

	public void setRkzk(String rkzk) {
		this.rkzk = rkzk;
	}

	public String getRkzkqtqk() {
		return this.rkzkqtqk;
	}

	public void setRkzkqtqk(String rkzkqtqk) {
		this.rkzkqtqk = rkzkqtqk;
	}

	public String getRkkclb() {
		return this.rkkclb;
	}

	public void setRkkclb(String rkkclb) {
		this.rkkclb = rkkclb;
	}

	public String getRkxklb() {
		return this.rkxklb;
	}

	public void setRkxklb(String rkxklb) {
		this.rkxklb = rkxklb;
	}

	public String getRjkc() {
		return this.rjkc;
	}

	public void setRjkc(String rjkc) {
		this.rjkc = rjkc;
	}

	public String getPjzks() {
		return this.pjzks;
	}

	public void setPjzks(String pjzks) {
		this.pjzks = pjzks;
	}

	public String getDrdqtgz() {
		return this.drdqtgz;
	}

	public void setDrdqtgz(String drdqtgz) {
		this.drdqtgz = drdqtgz;
	}

	public String getPjmzqtgzzhjxkss() {
		return this.pjmzqtgzzhjxkss;
	}

	public void setPjmzqtgzzhjxkss(String pjmzqtgzzhjxkss) {
		this.pjmzqtgzzhjxkss = pjmzqtgzzhjxkss;
	}

	public String getJrgz() {
		return this.jrgz;
	}

	public void setJrgz(String jrgz) {
		this.jrgz = jrgz;
	}

	public String getJrqtgzmc() {
		return this.jrqtgzmc;
	}

	public void setJrqtgzmc(String jrqtgzmc) {
		this.jrqtgzmc = jrqtgzmc;
	}

	public String getSfwbkssk() {
		return this.sfwbkssk;
	}

	public void setSfwbkssk(String sfwbkssk) {
		this.sfwbkssk = sfwbkssk;
	}

	public String getDslb() {
		return this.dslb;
	}

	public void setDslb(String dslb) {
		this.dslb = dslb;
	}

	public String getXzycsxkly() {
		return this.xzycsxkly;
	}

	public void setXzycsxkly(String xzycsxkly) {
		this.xzycsxkly = xzycsxkly;
	}

	public String getXnbzkkcjxkss() {
		return this.xnbzkkcjxkss;
	}

	public void setXnbzkkcjxkss(String xnbzkkcjxkss) {
		this.xnbzkkcjxkss = xnbzkkcjxkss;
	}

	public String getXnqtgzzhkss() {
		return this.xnqtgzzhkss;
	}

	public void setXnqtgzzhkss(String xnqtgzzhkss) {
		this.xnqtgzzhkss = xnqtgzzhkss;
	}

	public String getBsrlb() {
		return this.bsrlb;
	}

	public void setBsrlb(String bsrlb) {
		this.bsrlb = bsrlb;
	}

	public String getShzt() {
		return this.shzt;
	}

	public void setShzt(String shzt) {
		this.shzt = shzt;
	}

	public Date getShsj() {
		return this.shsj;
	}

	public void setShsj(Date shsj) {
		this.shsj = shsj;
	}

	public String getShjg() {
		return this.shjg;
	}

	public void setShjg(String shjg) {
		this.shjg = shjg;
	}

	public String getSfsc() {
		return this.sfsc;
	}

	public void setSfsc(String sfsc) {
		this.sfsc = sfsc;
	}

	public String getCjr() {
		return this.cjr;
	}

	public void setCjr(String cjr) {
		this.cjr = cjr;
	}

	public Date getCjsj() {
		return this.cjsj;
	}

	public void setCjsj(Date cjsj) {
		this.cjsj = cjsj;
	}

	public String getXgr() {
		return this.xgr;
	}

	public void setXgr(String xgr) {
		this.xgr = xgr;
	}

	public Date getXgsj() {
		return this.xgsj;
	}

	public void setXgsj(Date xgsj) {
		this.xgsj = xgsj;
	}

}
