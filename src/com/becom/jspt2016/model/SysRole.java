package com.becom.jspt2016.model;

// Generated 2016-10-16 11:29:08 by Hibernate Tools 3.2.0.CR1

import java.util.HashSet;
import java.util.Set;

/**
 * SysRole generated by hbm2java
 */
public class SysRole implements java.io.Serializable {

	private long roleId;

	private String roleName;

	private String userType;

	private boolean sfzy;

	private Set<SysUser> sysUsers = new HashSet<SysUser>(0);

	private Set<SysRoleFunc> sysRoleFuncs = new HashSet<SysRoleFunc>(0);

	public SysRole() {
	}

	public SysRole(Long roleId) {
		this.roleId = roleId;
	}

	public SysRole(long roleId, String roleName, boolean sfzy) {
		this.roleId = roleId;
		this.roleName = roleName;
		this.sfzy = sfzy;
	}

	public SysRole(long roleId, String roleName, String userType, boolean sfzy,
			Set<SysUser> sysUsers, Set<SysRoleFunc> sysRoleFuncs) {
		this.roleId = roleId;
		this.roleName = roleName;
		this.userType = userType;
		this.sfzy = sfzy;
		this.sysUsers = sysUsers;
		this.sysRoleFuncs = sysRoleFuncs;
	}

	public long getRoleId() {
		return this.roleId;
	}

	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getUserType() {
		return this.userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public boolean isSfzy() {
		return this.sfzy;
	}

	public void setSfzy(boolean sfzy) {
		this.sfzy = sfzy;
	}

	public Set<SysUser> getSysUsers() {
		return this.sysUsers;
	}

	public void setSysUsers(Set<SysUser> sysUsers) {
		this.sysUsers = sysUsers;
	}

	public Set<SysRoleFunc> getSysRoleFuncs() {
		return this.sysRoleFuncs;
	}

	public void setSysRoleFuncs(Set<SysRoleFunc> sysRoleFuncs) {
		this.sysRoleFuncs = sysRoleFuncs;
	}

}
