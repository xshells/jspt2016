package com.becom.jspt2016.model;
// Generated 2016-10-16 11:29:08 by Hibernate Tools 3.2.0.CR1


import java.util.Date;

/**
 * SysUser generated by hbm2java
 */
public class SysUser  implements java.io.Serializable {


     private long userId;
     private SysRole sysRole;
     private String userName;
     private String userPwd;
     private String userXm;
     private String userType;
     private String id;
     private String roleName;
     private boolean sfzy;
     private String bz;
     private Date cjsj;
     private Date xgsj;

    public SysUser() {
    }

	
    public SysUser(long userId, SysRole sysRole, String userName, String userPwd, boolean sfzy, Date cjsj) {
        this.userId = userId;
        this.sysRole = sysRole;
        this.userName = userName;
        this.userPwd = userPwd;
        this.sfzy = sfzy;
        this.cjsj = cjsj;
    }
    public SysUser(long userId, SysRole sysRole, String userName, String userPwd, String userXm, String userType, String id, String roleName, boolean sfzy, String bz, Date cjsj, Date xgsj) {
       this.userId = userId;
       this.sysRole = sysRole;
       this.userName = userName;
       this.userPwd = userPwd;
       this.userXm = userXm;
       this.userType = userType;
       this.id = id;
       this.roleName = roleName;
       this.sfzy = sfzy;
       this.bz = bz;
       this.cjsj = cjsj;
       this.xgsj = xgsj;
    }
   
    public long getUserId() {
        return this.userId;
    }
    
    public void setUserId(long userId) {
        this.userId = userId;
    }
    public SysRole getSysRole() {
        return this.sysRole;
    }
    
    public void setSysRole(SysRole sysRole) {
        this.sysRole = sysRole;
    }
    public String getUserName() {
        return this.userName;
    }
    
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getUserPwd() {
        return this.userPwd;
    }
    
    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }
    public String getUserXm() {
        return this.userXm;
    }
    
    public void setUserXm(String userXm) {
        this.userXm = userXm;
    }
    public String getUserType() {
        return this.userType;
    }
    
    public void setUserType(String userType) {
        this.userType = userType;
    }
    public String getId() {
        return this.id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    public String getRoleName() {
        return this.roleName;
    }
    
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
    public boolean isSfzy() {
        return this.sfzy;
    }
    
    public void setSfzy(boolean sfzy) {
        this.sfzy = sfzy;
    }
    public String getBz() {
        return this.bz;
    }
    
    public void setBz(String bz) {
        this.bz = bz;
    }
    public Date getCjsj() {
        return this.cjsj;
    }
    
    public void setCjsj(Date cjsj) {
        this.cjsj = cjsj;
    }
    public Date getXgsj() {
        return this.xgsj;
    }
    
    public void setXgsj(Date xgsj) {
        this.xgsj = xgsj;
    }




}


