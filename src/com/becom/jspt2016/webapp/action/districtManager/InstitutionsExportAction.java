/**
 * InstitutionsExportAction.java
*/

package com.becom.jspt2016.webapp.action.districtManager;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.nestframework.action.FileItem;
import org.nestframework.addons.spring.Spring;

import com.becom.jspt2016.dto.InstitutionsInfoDto;
import com.becom.jspt2016.model.TbXxjgb;
import com.becom.jspt2016.service.ext.IExportExt;
import com.becom.jspt2016.webapp.action.BaseAction;

/**
 * @author   fmx
 */

public class InstitutionsExportAction extends BaseAction {
	@Spring
	private IExportExt exportExt;

	protected Logger logger = Logger.getLogger(this.getClass());

	public FileItem file;

	public TbXxjgb institutionsInfo = new TbXxjgb();

	/**
	 * 机构Id
	 */
	public long institutionId;

	/**
	 * 机构Id组成的字符串
	 */
	public String[] ck;

	/**
	 * 机构名称   搜索条件使用
	 */
	public String institutionNameQuery;

	/**
	 * 机构代码   搜索条件使用
	 */
	public String institutionCodeQuery;

	/**
	 * 区县代码 搜索条件使用
	 */
	public String districtCodeQuery;

	/**
	 * 办学类型 搜索条件使用
	 */
	public String schoolTypeQuery;

	/**
	 * 驻地城乡类型
	 */
	public String residentTypeQuery;

	/**
	 * 单位类别
	 */
	public String unitTypeQuery;

	/**
	 * 所属学段
	 */
	public String affiliatedSchoolQuery;

	public void export(HttpServletRequest request, HttpServletResponse response) {
		String filename = "";
		
		List<InstitutionsInfoDto> list = new ArrayList<InstitutionsInfoDto>();
		try {
			Map<String, Object> param = new HashMap<String, Object>();
			if (institutionNameQuery != null
					&& !"".equals(institutionNameQuery.replaceAll(" ", ""))) {
				param.put("institutionNameQuery",
						institutionNameQuery.replaceAll(" ", ""));
			}
			if (institutionCodeQuery != null
					&& !"".equals(institutionCodeQuery.replaceAll(" ", ""))) {
				param.put("institutionCodeQuery",
						institutionCodeQuery.replaceAll(" ", ""));
			}
			if (districtCodeQuery != null
					&& !"".equals(districtCodeQuery.replaceAll(" ", ""))) {
				param.put("districtCodeQuery",
						districtCodeQuery.replaceAll(" ", ""));
			}
			if (schoolTypeQuery != null
					&& !"".equals(schoolTypeQuery.replaceAll(" ", ""))) {
				param.put("schoolTypeQuery",
						schoolTypeQuery.replaceAll(" ", ""));
			}
			if (residentTypeQuery != null
					&& !"".equals(residentTypeQuery.replaceAll(" ", ""))) {
				param.put("residentTypeQuery",
						residentTypeQuery.replaceAll(" ", ""));
			}
			if (affiliatedSchoolQuery != null
					&& !"".equals(affiliatedSchoolQuery.replaceAll(" ", ""))) {
				param.put("affiliatedSchoolQuery",
						affiliatedSchoolQuery.replaceAll(" ", ""));
			}
			if (unitTypeQuery != null
					&& !"".equals(unitTypeQuery.replaceAll(" ", ""))) {
				param.put("unitTypeQuery", unitTypeQuery.replaceAll(" ", ""));
			}
			list = exportExt.getInstitutionsList(param);

			String[] title = { "序号", "所在区*", "所属学段*", "单位类别", "办学类型*",
					"（驻地）城乡类型", "组织单位代码", "单位代码", "单位名称*", "举办单位*", "举办者类型*",
					"地址*", "联系人", "联系电话", "邮箱" };
			
			filename = constantBean.get("tmpRealPath") + "/Danwei" + System.currentTimeMillis()+ ".xlsx";

			export2007(request,response, list, "单位信息", title);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		try {
//			JSONObject json = new JSONObject();
//			response.setContentType("text/xml;charset=UTF-8");
//			PrintWriter out = response.getWriter();
//			json.put("fileName", filename);
//			out.write(json.toString());
//			out.flush();
//			out.close();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}

	}

	protected void export2007(HttpServletRequest request,String path,
			List<InstitutionsInfoDto> list, String fileName, String[] title) {
		String[] header = title;

		XSSFWorkbook wb = new XSSFWorkbook();
		XSSFSheet sheet = wb.createSheet(fileName);
		XSSFRow row = sheet.createRow((int) 0);
		XSSFCellStyle style = wb.createCellStyle();

		XSSFFont font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("宋体");
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		style.setFillForegroundColor(HSSFColor.GREY_80_PERCENT.index);
		style.setFont(font);

		XSSFCell cell = null;
		for (int i = 0; i < header.length; i++) {
			cell = row.createCell((short) i);
			cell.setCellValue(header[i]);
			cell.setCellStyle(style);
		}

		if (list == null) {
			return;
		}

		for (int i = 0; i < list.size(); i++) {
			row = sheet.createRow((int) i + 1);
			InstitutionsInfoDto z = list.get(i);

			row.createCell((short) 0).setCellValue(z.getInstitutionId() + "");
			row.createCell((short) 1).setCellValue(z.getDisName());
			row.createCell((short) 2).setCellValue(
					exportExt.getZdxName("XXJG_SSXD", z.getAffiliatedSchool()));

			String utype = z.getUnitType();
			if (utype != null) {
				row.createCell((short) 3).setCellValue(
						exportExt.getZdxName("XXJG_ZXXFL", utype));
			} else {
				row.createCell((short) 3).setCellValue("无");
			}

			row.createCell((short) 4).setCellValue(
					exportExt.getZdxName("XXJG_BXLX", z.getSchoolType()));
			row.createCell((short) 5).setCellValue(
					exportExt.getZdxName("XXJG_ZDCXLX", z.getResidentType()));
			row.createCell((short) 6).setCellValue(z.getUnitCode());
			row.createCell((short) 7).setCellValue(z.getInstitutionCode());
			row.createCell((short) 8).setCellValue(z.getInstitutionName());
			row.createCell((short) 9).setCellValue(z.getJbdw());
			row.createCell((short) 10).setCellValue(
					exportExt.getZdxName("XXJG_JBZLX", z.getJbzlx()));
			row.createCell((short) 11).setCellValue(z.getInstitutionAddress());
			row.createCell((short) 12).setCellValue(z.getContactPerson());
			row.createCell((short) 13).setCellValue(z.getContactTel());
			row.createCell((short) 14).setCellValue(z.getEmail());
		}

		 try {
				OutputStream out = new FileOutputStream(path);
				wb.write(out);
				out.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	protected void export2007(HttpServletRequest request,HttpServletResponse response,
			List<InstitutionsInfoDto> list, String fileName, String[] title) {
		String[] header = title;

		XSSFWorkbook wb = new XSSFWorkbook();
		XSSFSheet sheet = wb.createSheet(fileName);
		XSSFRow row = sheet.createRow((int) 0);
		XSSFCellStyle style = wb.createCellStyle();

		XSSFFont font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("宋体");
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		style.setFillForegroundColor(HSSFColor.GREY_80_PERCENT.index);
		style.setFont(font);

		XSSFCell cell = null;
		for (int i = 0; i < header.length; i++) {
			cell = row.createCell((short) i);
			cell.setCellValue(header[i]);
			cell.setCellStyle(style);
		}

		if (list == null) {
			return;
		}

		for (int i = 0; i < list.size(); i++) {
			row = sheet.createRow((int) i + 1);
			InstitutionsInfoDto z = list.get(i);

			row.createCell((short) 0).setCellValue(z.getInstitutionId() + "");
			row.createCell((short) 1).setCellValue(z.getDisName());
			row.createCell((short) 2).setCellValue(
					exportExt.getZdxName("XXJG_SSXD", z.getAffiliatedSchool()));

			String utype = z.getUnitType();
			if (utype != null) {
				row.createCell((short) 3).setCellValue(
						exportExt.getZdxName("XXJG_ZXXFL", utype));
			} else {
				row.createCell((short) 3).setCellValue("无");
			}

			row.createCell((short) 4).setCellValue(
					exportExt.getZdxName("XXJG_BXLX", z.getSchoolType()));
			row.createCell((short) 5).setCellValue(
					exportExt.getZdxName("XXJG_ZDCXLX", z.getResidentType()));
			row.createCell((short) 6).setCellValue(z.getUnitCode());
			row.createCell((short) 7).setCellValue(z.getInstitutionCode());
			row.createCell((short) 8).setCellValue(z.getInstitutionName());
			row.createCell((short) 9).setCellValue(z.getJbdw());
			row.createCell((short) 10).setCellValue(
					exportExt.getZdxName("XXJG_JBZLX", z.getJbzlx()));
			row.createCell((short) 11).setCellValue(z.getInstitutionAddress());
			row.createCell((short) 12).setCellValue(z.getContactPerson());
			row.createCell((short) 13).setCellValue(z.getContactTel());
			row.createCell((short) 14).setCellValue(z.getEmail());
		}

		try {
			response.setContentType("application/force-download");
			
			String agent = (String)request.getHeader("USER-AGENT");     
        	if(agent != null && agent.toLowerCase().indexOf("firefox") > 0) {// FF      
        	    String enableFileName = "=?UTF-8?B?" + (new String(Base64.encodeBase64((fileName+".xlsx").getBytes("UTF-8")))) + "?=";    
        	    response.setHeader("Content-Disposition", "attachment; filename=" + enableFileName);    
        	}else{
			response.setHeader("Content-Disposition", "attachment;filename=\""
					+ java.net.URLEncoder.encode(fileName, "UTF-8") + ".xlsx"
					+ "\" ");
        	}
			wb.write(response.getOutputStream());
			response.getOutputStream().close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	protected void export2003(HttpServletResponse response, List<InstitutionsInfoDto> list,String filename,String[] title){
		String[] header = title;
		
		HSSFWorkbook wb = new HSSFWorkbook();  
	    HSSFSheet sheet = wb.createSheet(filename);  
	    HSSFRow row = sheet.createRow((int) 0);  
	    HSSFCellStyle style = wb.createCellStyle(); 
	    
	    HSSFFont font = wb.createFont();
	    font.setFontHeightInPoints((short) 11);
	    font.setFontName("宋体");
	    font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
	    
	    style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
	    style.setFillForegroundColor(HSSFColor.GREY_80_PERCENT.index);
	    style.setFont(font);
	
	    HSSFCell cell = null;
	    for(int i=0;i<header.length;i++){
	    	cell = row.createCell((short) i);  
	        cell.setCellValue(header[i]);  
	        cell.setCellStyle(style);
	    }
	    
	    if(list == null){
	    	return;
	    }
	
	    for (int i = 0; i < list.size(); i++)  
	    {  
	        row = sheet.createRow((int) i + 1);
	        
	        InstitutionsInfoDto z = list.get(i);
	        row.createCell((short) 0).setCellValue(z.getInstitutionId());  
	        row.createCell((short) 1).setCellValue(z.getDisName());  
	        row.createCell((short) 2).setCellValue(exportExt.getZdxName("XXJG_SSXD", z.getAffiliatedSchool()));
	        
	        String utype = z.getUnitType();
	        if(utype != null){
	        	 row.createCell((short) 3).setCellValue(exportExt.getZdxName("XXJG_ZXXFL", utype));
	        }else{
	        	row.createCell((short) 3).setCellValue("无");
	        }
	        
	        row.createCell((short) 4).setCellValue(exportExt.getZdxName("XXJG_BXLX", z.getSchoolType()));
	        row.createCell((short) 5).setCellValue(exportExt.getZdxName("XXJG_ZDCXLX", z.getResidentType()));
	        row.createCell((short) 6).setCellValue(z.getUnitCode());
	        row.createCell((short) 7).setCellValue(z.getInstitutionCode());
	        row.createCell((short) 8).setCellValue(z.getInstitutionName());
	        row.createCell((short) 9).setCellValue(z.getJbdw());
	        row.createCell((short) 10).setCellValue(exportExt.getZdxName("XXJG_JBZLX", z.getJbzlx()));
	        row.createCell((short) 11).setCellValue(z.getInstitutionAddress());
	        row.createCell((short) 12).setCellValue(z.getContactPerson());
	        row.createCell((short) 13).setCellValue(z.getContactTel());
	        row.createCell((short) 14).setCellValue(z.getEmail());
	    }  
	   
	    String fileName = "机构信息.xls";
	    try  
	    {  
	    	response.setContentType("application/force-download");
			response.setHeader("Content-Disposition", "attachment;filename=\"" + java.net.URLEncoder.encode(fileName, "UTF-8") + ".xls" + "\" ");
	        wb.write(response.getOutputStream());  
			response.getOutputStream().close();  
	    }  
	    catch (Exception e)  
	    {  
	        e.printStackTrace();  
	    }  
	}
	*/
}
