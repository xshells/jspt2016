/**
 * TeacherInfoAudit.java
 * com.becom.jspt2016.webapp.action.districtManager
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webapp.action.districtManager;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.dto.AduitLogDto;
import com.becom.jspt2016.dto.InstitutionsInfoDto;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.service.ext.IInstitutionsManageExt;
import com.becom.jspt2016.service.ext.ITeacherInfoAuditExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 *教师信息审核
 * <p>
 * 区机构
 * @author   kuangxiang
 * @Date	 2016年10月25日
 */

public class TeacherInfoAuditAction extends PageAction {
	/**
	 * 机构用户
	 */
	public SysUser sUser;

	/**
	 * 老师用户
	 */
	public TbBizJzgjbxx teacher;
	@Spring
	private ITeacherInfoAuditExt teacherInfoAuditExt;
	@Spring
	private IInstitutionsManageExt institutionsManageExt;
	public String userType;
	/**
	 * 教育Id
	 */
	public String eduIdQuery;
	/**
	 * 身份证号
	 */
	public String cardNoQuery;
	/**
	 * 老师姓名
	 */
	public String teacherNameQuery;
	/**
	 * 审核状态的
	 */
	public String aduitStutasQuery;
	public String ck;
	/**
	 * 审核是否通过   引用字典：TB_CFG_ZDB.JSXX_SHJG  0-审核中 1-通过 2-不通过 
	 */
	public String isAdopt;

	/**
	 * 教职工Id
	 */
	public long jsId;
	/**
	 * 学校审核通过
	 */
	private static final String SCHOOLADUITADOPT = "11";
	/**
	 * 区审核不通过
	 */
	private static final String QUSHZTNO = "22";
	/**
	 * 区审核通过
	 */
	private static final String QUSHZTYES = "21";

	/**
	 * 审核意见
	 */
	public String aduitOpinion;
	public String ssxd;
	protected Logger logger = Logger.getLogger(this.getClass());

	public String affiliatedSchoolQuery;
	public String unitTypeQuery;
	public String institutionNameQuery;

	/**
	 * 
	 * 搜索
	 * <p>
	 * 机构管理查询
	 */
	@DefaultAction
	public Object doSearch(HttpServletRequest request) {
		try {
			//获取用户信息
			String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
			//机构用户
			if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
				sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);

			} else {//教师用户
				teacher = (TbBizJzgjbxx) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
			}
			//用户类型 1：市级2：区级
			userType = sUser.getUserType();
			Map<String, Object> param = new HashMap<String, Object>();
			InstitutionsInfoDto institution = institutionsManageExt.getInstitution(sUser.getUserName());
			param.put("disCode", sUser.getId());
			if (eduIdQuery != null && !"".equals(eduIdQuery.replaceAll(" ", ""))) {
				param.put("eduIdQuery", eduIdQuery.replaceAll(" ", ""));
			}
			if (cardNoQuery != null && !"".equals(cardNoQuery.replaceAll(" ", ""))) {
				param.put("cardNoQuery", cardNoQuery.replaceAll(" ", ""));
			}
			if (teacherNameQuery != null && !"".equals(teacherNameQuery.replaceAll(" ", ""))) {
				param.put("teacherNameQuery", teacherNameQuery.replaceAll(" ", ""));
			}

			if (affiliatedSchoolQuery != null && !"".equals(affiliatedSchoolQuery.replaceAll(" ", ""))) {
				param.put("affiliatedSchoolQuery", affiliatedSchoolQuery.replaceAll(" ", ""));
			}
			if (unitTypeQuery != null && !"".equals(unitTypeQuery.replaceAll(" ", ""))) {
				param.put("unitTypeQuery", unitTypeQuery.replaceAll(" ", ""));
			}
			if (institutionNameQuery != null && !"".equals(institutionNameQuery.replaceAll(" ", ""))) {
				param.put("institutionNameQuery", institutionNameQuery.replaceAll(" ", ""));
			}

			if (aduitStutasQuery != null && !"".equals(aduitStutasQuery.replaceAll(" ", ""))) {
				String[] status = { aduitStutasQuery.replaceAll(" ", "") };
				param.put("aduitStutasQuery", status);
			} else {
				String[] status = { SCHOOLADUITADOPT, QUSHZTNO, QUSHZTYES };
				param.put("aduitStutasQuery", status);//校级审核通过,区级审核通过，区级审核不通过
			}

			pageObj = teacherInfoAuditExt.doSearch(param, pageNo, pageSize);
			if (institution != null) { //市级  区级  没有机构
				ssxd = institution.getAffiliatedSchool();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "teacherInfoAudit.jsp";
	}

	/**
	 * 审核功能
	 * <p>
	 *区级审核
	 */
	public Object aduit(HttpServletRequest request) {
		/*
		try {
			if (isAdopt != null && !"".equals(isAdopt)) {
				isAdopt = new String(request.getParameter("isAdopt").getBytes("iso-8859-1"), "utf-8");
			}
			if (aduitOpinion != null && !"".equals(aduitOpinion)) {
				aduitOpinion = new String(request.getParameter("aduitOpinion").getBytes("iso-8859-1"), "utf-8");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}*/

		//获取用户信息
		String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
		//机构用户
		if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
			sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);

		} else {//教师用户
			teacher = (TbBizJzgjbxx) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
		}
		InstitutionsInfoDto institution = institutionsManageExt.getInstitution(sUser.getUserName());

		//获取Ip
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		ip = ip.equals("0:0:0:0:0:0:0:1") ? "127.0.0.1" : ip;
		String[] split = ck.split(",");
		teacherInfoAuditExt.aduit(split, isAdopt, aduitOpinion, institution, sUser, ip);
		/*int result = 0;
		try {
			if (result == 0) {
				message = "审核成功";
			}
		} catch (Exception e) {
			if (result == 1) {
				message = "审核失败";
			}
			e.printStackTrace();
		}*/
		return doSearch(request);
	}

	/**
	 * 查询审核意见
	 * <p>
	 */
	public Object queryAduitComment(HttpServletResponse response) {

		try {
			JSONObject json = new JSONObject();
			List<AduitLogDto> list = teacherInfoAuditExt.queryAduitComment(jsId);
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("list", list);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("teacherInfoAuditExt queryAduitComment() is error!", e);
		}
		return null;
	}
}
