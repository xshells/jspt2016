/**
 * InstitutionsManageAction.java
 * com.becom.jspt2016.webapp.action.districtManager
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webapp.action.districtManager;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.dto.DisCodesDto;
import com.becom.jspt2016.dto.InstitutionsInfoDto;
import com.becom.jspt2016.dto.TeacherAccountInfoDto;
import com.becom.jspt2016.dto.UserDto;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbXxjgb;
import com.becom.jspt2016.service.ext.IInstitutionsManageExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 * 机构管理页面
 * <p>
 * 包括区级和市级
 * @author   kuangxiang
 * @Date	 2016年10月18日
 */

public class InstitutionsManageAction extends PageAction {
	@Spring
	private IInstitutionsManageExt institutionsManageExt;
	protected Logger logger = Logger.getLogger(this.getClass());

	public TbXxjgb institutionsInfo = new TbXxjgb();
	/**
	 * 机构Id
	 */
	public long institutionId;
	/**
	 * 机构Id组成的字符串
	 */
	public String[] ck;
	/**
	 * 机构名称   搜索条件使用
	 */
	public String institutionNameQuery;
	/**
	 * 机构代码   搜索条件使用
	 */
	public String institutionCodeQuery;

	/**
	 * 区县代码 搜索条件使用
	 */
	public String districtCodeQuery;
	/**
	 * 办学类型 搜索条件使用
	 */
	public String schoolTypeQuery;
	/**
	 * 驻地城乡类型
	 */
	public String residentTypeQuery;
	/**
	 * 单位类别
	 */
	public String unitTypeQuery;

	/**
	 * 所属学段
	 */
	public String affiliatedSchoolQuery;

	public InstitutionsInfoDto institutionDto = new InstitutionsInfoDto();
	public UserDto user = new UserDto();
	/**
	 * 机构代码
	 */
	public String institutionCode;
	/**
	 * 机构用户
	 */
	public SysUser sUser;

	/**
	 * 老师用户
	 */
	public TbBizJzgjbxx teacher;
	/**
	 * 用户类型
	 */
	public String userType;
	/**
	 * 区县列表
	 */
	public List<DisCodesDto> disCodes;
	/**
	 * 新密码
	 */
	public String newPassWord;
	/**
	 * 旧密码
	 */
	public String oldPassWord;

	/**
	 * 
	 * 搜索
	 * <p>
	 * 机构管理查询
	 */
	@DefaultAction
	public Object doSearch(HttpServletRequest request) {
		try {
			//获取用户信息
			String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
			//机构用户
			if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
				sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);

			} else {//教师用户
				teacher = (TbBizJzgjbxx) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
			}

			//用户类型 1：市级2：区级
			userType = sUser.getUserType();
			//获取区市列表
			if ("1".equals(userType)) {
				disCodes = institutionsManageExt.queryDisCodes(null);
			} else if ("2".equals(userType)) {
				//InstitutionsInfoDto institution = institutionsManageExt.getInstitution("01011016");
				disCodes = institutionsManageExt.queryDisCodes(sUser.getId());
			}
			Map<String, Object> param = new HashMap<String, Object>();
			if (institutionNameQuery != null && !"".equals(institutionNameQuery.replaceAll(" ", ""))) {
				param.put("institutionNameQuery", "%" + institutionNameQuery.replaceAll(" ", "") + "%");
			}
			if (institutionCodeQuery != null && !"".equals(institutionCodeQuery.replaceAll(" ", ""))) {
				param.put("institutionCodeQuery", "%" + institutionCodeQuery.replaceAll(" ", "") + "%");
			}
			if ("2".equals(userType)) {
				param.put("districtCodeQuery", disCodes.get(0).getDisCode());
			} else {
				if (districtCodeQuery != null && !"".equals(districtCodeQuery.replaceAll(" ", ""))) {
					param.put("districtCodeQuery", districtCodeQuery.replaceAll(" ", ""));
				}
			}
			if (schoolTypeQuery != null && !"".equals(schoolTypeQuery.replaceAll(" ", ""))) {
				param.put("schoolTypeQuery", schoolTypeQuery.replaceAll(" ", ""));
			}
			if (residentTypeQuery != null && !"".equals(residentTypeQuery.replaceAll(" ", ""))) {
				param.put("residentTypeQuery", residentTypeQuery.replaceAll(" ", ""));
			}
			if (affiliatedSchoolQuery != null && !"".equals(affiliatedSchoolQuery.replaceAll(" ", ""))) {
				param.put("affiliatedSchoolQuery", affiliatedSchoolQuery.replaceAll(" ", ""));
			}
			if (unitTypeQuery != null && !"".equals(unitTypeQuery.replaceAll(" ", ""))) {
				param.put("unitTypeQuery", unitTypeQuery.replaceAll(" ", ""));
			}
			pageObj = institutionsManageExt.doSearch(param, pageNo, pageSize);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "institutionsManageList.jsp";
	}

	/**
	 * 添加
	 * <p>
	 *添加机构
	 * @param request
	 * @param response
	 */
	public Object add(HttpServletRequest request, HttpServletResponse response) {
		//获取用户信息
		String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
		//机构用户
		if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
			sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);

		} else {//教师用户
			teacher = (TbBizJzgjbxx) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
		}

		//用户类型 1：市级2：区级

		userType = sUser.getUserType();
		institutionsManageExt.add(institutionDto, userType);
		return doSearch(request);

	}

	/**
	 * 跳到编辑页面
	 */
	public void toEdit(HttpServletRequest request, HttpServletResponse response) {

		try {
			JSONObject json = new JSONObject();
			InstitutionsInfoDto ins = institutionsManageExt.get(institutionId);
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("ins", ins);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("InstitutionsManageAction toEdit() is error!", e);
		}
	}

	/**
	 * 编辑
	 */
	public Object edit(HttpServletRequest request, HttpServletResponse response) {
		int result = 0;
		try {
			result = institutionsManageExt.edit(institutionDto);
			if (result == 0) {
				message = "编辑成功";
			}
		} catch (Exception e) {
			if (result == 1) {
				message = "编辑失败";
			}
			e.printStackTrace();
		}
		return doSearch(request);
	}

	/**
	 * 
	 * 删除
	 * <p>
	 * 删除机构
	 */
	public Object delete(HttpServletRequest request, HttpServletResponse response) {

		int result = 0;
		try {
			result = institutionsManageExt.delete(ck);
			if (result == 0) {
				message = "删除成功";
			}
		} catch (Exception e) {
			if (result == 1) {
				message = "删除失败";
			}
			e.printStackTrace();
		}
		return doSearch(request);
	}

	/**
	 * 查询机构信息，
	 * <p>
	 * 通过机构代码查询
	 */
	public void fetchInstitution(HttpServletResponse response) {

		try {
			JSONObject json = new JSONObject();
			InstitutionsInfoDto ins = institutionsManageExt.getInstitution(institutionCode);
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("ins", ins);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("InstitutionsManageAction fetchInstitution() is error!", e);
		}
	}

	/**
	 * 查询机构信息，
	 * <p>
	 * 通过机构代码查询
	 */
	public void fetchInstitutionByName(HttpServletResponse response, HttpServletRequest request) {

		String name = request.getParameter("jgName");
		String jgId = request.getParameter("jgId");

		try {
			JSONObject json = new JSONObject();
			boolean bool = institutionsManageExt.getInstitutionByName(name,jgId);
			String ins = null;
			if(bool){
				ins = "yes";
			}
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("ins", ins);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("InstitutionsManageAction fetchInstitution() is error!", e);
		}
	}

	/**
	 * 查询老师
	 * <p>
	 *通过机构Id查询属于这个机构的且没有被删除的老师
	 *	
	 * @param response 
	 */
	public void queryTeachers(HttpServletResponse response) {
		try {
			JSONObject json = new JSONObject();
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			List<TeacherAccountInfoDto> teacherList = institutionsManageExt.queryTeachers(institutionId);
			if (teacherList.size() > 0) {
				json.put("ins", false);
			} else {
				json.put("ins", true);
			}
			out.write(json.toString());
			out.flush();
			out.close();

		} catch (Exception e) {
			logger.error("InstitutionsManageAction queryTeachers() is error!", e);
		}
	}

	/**
	 * 跳到修改密码页面
	 */
	public Object toModifyPassWord() {
		return "change_password.jsp";
	}

	/**
	 * 判断旧密码是否正确
	 * <p>
	 */
	public void isOldPassWordRight(HttpServletRequest request, HttpServletResponse response) {
		try {
			//获取用户信息
			String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
			//机构用户
			if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
				sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);

			} else {//教师用户
				teacher = (TbBizJzgjbxx) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
			}
			JSONObject json = new JSONObject();
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			SysUser user = institutionsManageExt.queryOldPassWord(sUser.getUserId());
			if (user != null) {
				if ((user.getUserPwd()).equals(oldPassWord)) {
					json.put("ins", true);
				} else {
					json.put("ins", false);
				}
			} else {
				json.put("ins", false);
			}
			out.write(json.toString());
			out.flush();
			out.close();

		} catch (Exception e) {
			logger.error("InstitutionsManageAction isOldPassWordRight() is error!", e);
		}
	}

	/**
	 * 修改密码
	 * <p>
	 */
	public void modifyPassWord(HttpServletRequest request, HttpServletResponse response) {
		//获取用户信息
		int result = 0;
		try {
			//获取用户信息
			String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
			//机构用户
			if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
				sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);

			} else {//教师用户
				teacher = (TbBizJzgjbxx) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
			}
			JSONObject json = new JSONObject();
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			result = institutionsManageExt.modifyPassWord(sUser.getUserId(), newPassWord);
			if (result == 0) {
				json.put("ins", true);
				sUser.setUserPwd(newPassWord);
				request.getSession().setAttribute(Constant.KEY_LOGIN_USER, sUser);
			} else if (result == 1) {
				json.put("ins", false);
			}
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("InstitutionsManageAction isOldPassWordRight() is error!", e);
		}
	}
	
	
		//级联办学类型
		public void getNameByType(HttpServletRequest request, HttpServletResponse response) {
			try {
				String ssxd = request.getParameter("ssxd");
				String dictType = request.getParameter("dictType");
				response.setContentType("text/xml;charset=UTF-8");
				JSONObject json = new JSONObject();
				Map<String, Object> param = new HashMap<String, Object>();
				param.put("ssxd", ssxd);
				param.put("dictType", dictType);
				
				List<Map<String, Object>> seleclist = institutionsManageExt.findNameByType(param);
				PrintWriter out = response.getWriter();
				json.put("dto", seleclist);
				out.write(json.toString());
				out.flush();
				out.close();
			} catch (Exception e) {

				e.printStackTrace();
			}
		}

	public static void main(String[] args) {
		String tableMd5 = DigestUtils.md5Hex("01064010" + "e10adc3949ba59abbe56e057f20f883e" + Constant.KEY_STRING);
		String md5 = DigestUtils.md5Hex("111111");
		System.out.println("token:" + tableMd5);
		System.out.println("密码:" + md5);
	}
}
