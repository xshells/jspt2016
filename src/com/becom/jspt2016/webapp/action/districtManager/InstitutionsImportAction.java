package com.becom.jspt2016.webapp.action.districtManager;

import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.nestframework.action.FileItem;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.dto.InstitutionsInfoDto;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.service.ext.IExportExt;
import com.becom.jspt2016.webapp.action.BaseAction;


public class InstitutionsImportAction extends BaseAction {
	@Spring
	private IExportExt exportExt;
	protected Logger logger = Logger.getLogger(this.getClass());
	
	public FileItem fileImport;
	
	/**
	 * 机构用户
	 */
	public SysUser sUser;

	/**
	 * 老师用户
	 */
	public TbBizJzgjbxx teacher;
	/**
	 * 用户类型
	 */
	public String userType;

	@DefaultAction
	public Object importInfo(HttpServletRequest request, HttpServletResponse response){
		String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
		if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
			sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
		} else {
			teacher = (TbBizJzgjbxx) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
		}
		userType = sUser.getUserType();
		
		try {
			if(fileImport != null && fileImport.isUploaded()){
				InputStream in = fileImport.getInputStream();
				readXls(in);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public void importFile(HttpServletRequest request, HttpServletResponse response){
		String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
		if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
			sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
		} else {
			teacher = (TbBizJzgjbxx) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
		}
		userType = sUser.getUserType();
		
		Map<String,String> result = new HashMap<String,String>();
		
		try {
			if(fileImport != null && fileImport.isUploaded()){
				InputStream in = fileImport.getInputStream();
				readXls(in);
			}
			
			result.put("status", "1");
			result.put("msg", "导入成功！");
		} catch (Exception e) {
			result.put("status", "0");
			result.put("msg", "导入失败！");
		}
		
		try {
			JSONObject json = new JSONObject();
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("result", result);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void importFile2(HttpServletRequest request, HttpServletResponse response){
		
		try {
			
			System.out.println(fileImport.getFileName());
			
			JSONObject json = new JSONObject();
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("ins", 1);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
    public void readXls(InputStream is) throws Exception {
    	Workbook hssfWorkbook =  WorkbookFactory.create(is);
        for (int numSheet = 0; numSheet < hssfWorkbook.getNumberOfSheets(); numSheet++) {
            Sheet hssfSheet = hssfWorkbook.getSheetAt(numSheet);
            if (hssfSheet == null) {
                continue;
            }
            for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
                Row hssfRow = hssfSheet.getRow(rowNum);
                if (hssfRow != null) {
                	
                	if("".equals(getValue(hssfRow.getCell(0)))){
                		break;
                	}
                	
                	InstitutionsInfoDto dto = new InstitutionsInfoDto();
                	dto.setInstitutionId(Integer.parseInt(getValue(hssfRow.getCell(0))));
                	dto.setDistrictCode(exportExt.getQXDM(getValue(hssfRow.getCell(1))));
                	dto.setAffiliatedSchool(exportExt.getZDXBM("XXJG_SSXD", getValue(hssfRow.getCell(2))));
                	dto.setUnitType(exportExt.getZDXBM("XXJG_ZXXFL", getValue(hssfRow.getCell(3))));
                	dto.setSchoolType(exportExt.getZDXBM("XXJG_BXLX", getValue(hssfRow.getCell(4))));
                	dto.setResidentType(exportExt.getZDXBM("XXJG_ZDCXLX", getValue(hssfRow.getCell(5))));
                	dto.setUnitCode(getValue(hssfRow.getCell(6)));
                	dto.setInstitutionCode(getValue(hssfRow.getCell(7)));
                	dto.setInstitutionName(getValue(hssfRow.getCell(8)));
                	dto.setJbdw(getValue(hssfRow.getCell(9)));
                	dto.setJbzlx(exportExt.getZDXBM("XXJG_JBZLX",getValue(hssfRow.getCell(10))));
                	dto.setInstitutionAddress(getValue(hssfRow.getCell(11)));
                	dto.setContactPerson(getValue(hssfRow.getCell(12)));
                	dto.setContactTel(getValue(hssfRow.getCell(13)));
                	dto.setEmail(getValue(hssfRow.getCell(14)));
                	
                	exportExt.add(dto, userType);
                }
            }
        }
    }
    
    //获取单元格值
    protected String getValue(Cell cell){
    	String result = "";
    	if(cell!=null){
    		int type = cell.getCellType();
    		switch(type){
    		case Cell.CELL_TYPE_STRING:
    			result = cell.getStringCellValue();
    			break;
    		case Cell.CELL_TYPE_NUMERIC:
    			result = Double.valueOf(cell.getNumericCellValue()).intValue()+"";
    			break;
    		default:
    			result = "";
    		}
    	}
    	return result;
    }
    
}
