package com.becom.jspt2016.webapp.action.sysuserManager;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.dto.SysuserDto;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.service.ITbBizJzgjbxxManager;
import com.becom.jspt2016.service.ext.IInstitutionsManageExt;
import com.becom.jspt2016.service.ext.ISysuserManageExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 * 用户管理action
 * @author fmx
 *
 */
public class SysuserManageAction extends PageAction {
	protected Logger logger = Logger.getLogger(this.getClass());
	@Spring
	private ISysuserManageExt sysuserManageExt;
	@Spring
	private IInstitutionsManageExt institutionsManageExt;

	@Spring
	private ITbBizJzgjbxxManager tbBizJzgjbxxManager;

	//变量
	/////////////////////
	public List<Map<String, Object>> roleList;
	public List<Map<String, Object>> fwList;

	public SysuserDto sysuserDto = new SysuserDto();

	public String account;
	public String userId;
	/**
	 * 机构Id组成的字符串
	 */
	public String[] ck;
	public String status;
	/**
	 * 机构用户
	 */
	public SysUser sUser;
	/**
	 * 用户类型
	 */
	public String userType;

	/**
	 * 查询变量
	 */
	public String accountQuery;
	public String nameQuery;
	public String yhfwQuery;
	public String roleIdQuery;
	public String statusQuery;
	public String affiliatedSchoolQuery;

	@DefaultAction
	public Object doSearch(HttpServletRequest request) {
		Map<String, Object> param = new HashMap<String, Object>();

		if (accountQuery != null && !"".equals(accountQuery.replaceAll(" ", ""))) {
			param.put("account", accountQuery.replaceAll(" ", ""));
		}
		if (nameQuery != null && !"".equals(nameQuery.replaceAll(" ", ""))) {
			param.put("name", nameQuery.replaceAll(" ", ""));
		}
		if (yhfwQuery != null && !"".equals(yhfwQuery.replaceAll(" ", ""))) {
			param.put("yhfw", yhfwQuery.replaceAll(" ", ""));
		}
		if (roleIdQuery != null && !"".equals(roleIdQuery.replaceAll(" ", ""))) {
			param.put("roleId", roleIdQuery.replaceAll(" ", ""));
		}
		if (statusQuery != null && !"".equals(statusQuery.replaceAll(" ", ""))
				&& !"2".equals(statusQuery.replaceAll(" ", ""))) {
			param.put("status", statusQuery.replaceAll(" ", ""));
		}

		//获取用户信息
		String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
		//机构用户
		if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
			sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);

		}

		//用户类型 1：市级2：区级
		userType = sUser.getUserType();
		if ("2".equals(userType)) {
			param.put("utype", 2);
			param.put("quId", sUser.getId());
		} else if ("1".equals(userType)) {
			param.put("stype", 1);
		}
		if (affiliatedSchoolQuery != null && !"".equals(affiliatedSchoolQuery.replaceAll(" ", ""))) {
			String[] ssxds = { affiliatedSchoolQuery.replaceAll(" ", "") };
			param.put("xd", ssxds);
		} else {
			if ("2".equals(userType)) {
				String[] ssxds = { "30", "40", "50", "60" };
				param.put("xd", ssxds);
			} else if ("1".equals(userType)) {
				String[] ssxds = { "10", "20", "30", "40", "50", "60" };
				param.put("xd", ssxds);
			}
		}
		pageObj = sysuserManageExt.doSearch(param, pageNo, pageSize);
		roleList = sysuserManageExt.getRoleList(new HashMap<String, Object>());
		fwList = sysuserManageExt.getFwList();
		return "sysuserList.jsp";
	}

	public Object setStatus(HttpServletRequest request) {
		status = request.getParameter("status");
		int result = 0;
		try {
			result = sysuserManageExt.setStatus(ck, status);
			if (result == 0) {
				message = "设置成功";
			}
		} catch (Exception e) {
			if (result == 1) {
				message = "设置失败";
			}
			e.printStackTrace();
		}
		return doSearch(request);
	}

	public Object reset(HttpServletRequest request) {

		int result = 0;
		try {
			result = sysuserManageExt.reset(ck);
			if (result == 0) {
				message = "重置成功";
			}
		} catch (Exception e) {
			if (result == 1) {
				message = "重置失败";
			}
			e.printStackTrace();
		}
		return doSearch(request);
	}

	////////////多做的功能代码
	///////////////////////////////////////////////////////////
	public void isExist(HttpServletResponse response) {

		try {
			JSONObject json = new JSONObject();

			Map<String, Object> param = new HashMap<String, Object>();
			param.put("account", account);
			boolean bool = sysuserManageExt.isExist(param);
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("is", bool ? "1" : "0");
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("sysuserManageExt isExist() is error!", e);
		}
	}

	public Object add(HttpServletRequest request, HttpServletResponse response) {
		sysuserManageExt.add(sysuserDto);
		return doSearch(request);

	}

	/**
	 * 跳到编辑页面
	 */
	public void toEdit(HttpServletRequest request, HttpServletResponse response) {

		try {
			JSONObject json = new JSONObject();
			SysuserDto user = sysuserManageExt.getUser(userId);
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("ins", user);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("SysuserManageAction toEdit() is error!", e);
		}
	}

	/**
	 * 编辑
	 */
	public Object edit(HttpServletRequest request, HttpServletResponse response) {
		int result = 0;
		try {
			result = sysuserManageExt.edit(sysuserDto);
			if (result == 0) {
				message = "编辑成功";
			}
		} catch (Exception e) {
			if (result == 1) {
				message = "编辑失败";
			}
			e.printStackTrace();
		}
		return doSearch(request);
	}

	/**
	 * 
	 * 删除
	 * <p>
	 */
	public Object delete(HttpServletRequest request, HttpServletResponse response) {

		int result = 0;
		try {
			result = sysuserManageExt.delete(ck);
			if (result == 0) {
				message = "删除成功";
			}
		} catch (Exception e) {
			if (result == 1) {
				message = "删除失败";
			}
			e.printStackTrace();
		}
		return doSearch(request);
	}

	/**
	 * 判断教师是否有师德信息和处分信息
	 */
	public void isSdAndCf(HttpServletRequest request, HttpServletResponse response) {
		String jsId = request.getParameter("jsId");

		TbBizJzgjbxx t = tbBizJzgjbxxManager.get(Long.parseLong(jsId));
		String ssd = t.getTbXxjgb().getSsxd();

		String result = "0";

		if ("10".equals(ssd) || "20".equals(ssd)) {
			result = "1";
		} else {
			if (sysuserManageExt.isCf(jsId) && sysuserManageExt.isSd(jsId)) {
				result = "1";
			}
		}

		try {
			JSONObject json = new JSONObject();
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("isOk", result);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("SysuserManageAction toEdit() is error!", e);
		}
	}

	public void isSdAndCfArr(HttpServletRequest request, HttpServletResponse response) {
		String jsIdArr = request.getParameter("jsIdArr");
		Map<String, String> result = new HashMap<String, String>();

		StringBuffer sb = new StringBuffer();
		String[] ids = jsIdArr.split(",");
		for (int i = 0; i < ids.length; i++) {

			TbBizJzgjbxx t = tbBizJzgjbxxManager.get(Long.parseLong(ids[i]));
			String ssd = t.getTbXxjgb().getSsxd();
			if ("10".equals(ssd) || "20".equals(ssd)) {
				continue;
			} else {
				if (!sysuserManageExt.isCf(ids[i]) || !sysuserManageExt.isSd(ids[i])) {
					sb.append(ids[i]).append(",");
				}
			}

		}

		String msg = sb.toString();
		if (!"".equals(msg.trim())) {
			result.put("status", "0");
			result.put("msg", msg);
		} else {
			result.put("status", "1");
		}

		try {
			JSONObject json = new JSONObject();
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("isOk", result);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("SysuserManageAction toEdit() is error!", e);
		}
	}
}
