/**
 * LoginAction.java
 * com.becom.jspt2016.webapp.action
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webapp.action.sysuserManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.codec.binary.Base64;
import org.nestframework.action.FileItem;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.ConstantBean;
import com.becom.jspt2016.service.ext.ITbBizJzgjbxxManagerExt;
import com.becom.jspt2016.webapp.action.BaseAction;
import com.becom.jspt2016.webapp.util.UploadFileUtils;

/**
 * 文件操作模块，上传和下载
 * <p>
 * 教师变动管理
 * @author   fmx
 */
public class FileAction extends BaseAction {

	public FileItem file;

	@Spring
	private ITbBizJzgjbxxManagerExt tbBizJzgjbxxManagerExt;

	@Spring
	private UploadFileUtils uploadFileUtil;

	@Spring
	private ConstantBean constantBean;

	@DefaultAction
	public void doaction() {
	}

	/**
	 * 文件上传
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void upload(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String realPath = request.getSession().getServletContext().getRealPath("/");

		Map<String, Object> map = new HashMap<String, Object>();
		if (file != null && file.getFile() != null) {
			Map<String, String> reg = tbBizJzgjbxxManagerExt.checkPhoto(file);
			if (!reg.get("isValid").equals("true")) {
				message = reg.get("msg");
				System.out.println(message);
				map.put("status", "0");
				map.put("msg", message);
			} else {

				String filename = file.getFileName();
				String suffix = filename.substring(filename.lastIndexOf("."));
				long prefix = System.currentTimeMillis();
				String newName = prefix + suffix;
				String reletivepath = constantBean.get("imageUrl") + "/" + newName;
				String diskpath = realPath + constantBean.get("imageUrl");
				boolean isOk = uploadFileUtil.upload4CopyFile(newName, diskpath, file.getFile());
				if (isOk) {
					map.put("status", "1");
					map.put("msg", "上传成功！");
					map.put("filepath", reletivepath);
				} else {
					map.put("status", "0");
					map.put("msg", "上传失败");
				}
			}

		} else {
			map.put("status", "0");
			map.put("msg", "上传失败");
		}

		try {
			JSONObject json = new JSONObject();
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("result", map);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 照片删除
	 * @param response
	 */
	public void delphoto(HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", "1");
		map.put("msg", "删除成功！");

		try {
			JSONObject json = new JSONObject();
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("result", map);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 下载文件
	 * @param request
	 * @param response
	 * @throws UnsupportedEncodingException
	 * @throws MalformedURLException 
	 */
	public void getFile(HttpServletRequest request,HttpServletResponse response) throws Exception{
		
		String fileName = request.getParameter("fileName");
		if(fileName == null){
			throw new Exception("fileName param is not send!");
		}
		
		String gb = fileName;
		if(gb.contains("company")){
			gb = "单位导入模版";
		}else if(gb.contains("kaohe")){
			gb = "考核信息导入模版";
		}else if(gb.contains("shide")){
			gb = "师德信息导入模版";
		}else if(gb.contains("teacher")){
			gb = "人员导入模版";
		}else if(gb.contains("daiyu")){
			gb = "基本待遇导入模版";
		}else if(gb.contains("Error")){
			gb = "导入失败人员记录信息";
		}else if(gb.contains("Danwei")){
			gb = "单位信息";
		}
		
		gb = gb + ".xlsx";
		
		
//		String serverRealPath = request.getSession().getServletContext().getRealPath("/");
//		File file = new File(serverRealPath + "/" + constantBean.get("tmpPath") + "/" + gb);
		File file = new File(constantBean.get("tmpRealPath") + "/" + fileName);
		downloadFile(file.getPath(),request,response,gb);
	}
	
	protected void downloadFile(String path,HttpServletRequest request, HttpServletResponse response,String filename) {
		InputStream in = null;
		try {
			File file = new File(path);
			in = new FileInputStream(file);
			response.setContentType("application/force-download");
			response.setHeader("Content-length", String.valueOf(in.available()));
			
			String agent = (String)request.getHeader("USER-AGENT");     
        	if(agent != null && agent.toLowerCase().indexOf("firefox") > 0) {// FF      
        	    String enableFileName = "=?UTF-8?B?" + (new String(Base64.encodeBase64(filename.getBytes("UTF-8")))) + "?=";    
        	    response.setHeader("Content-Disposition", "attachment; filename=" + enableFileName);    
        	}else{
        		response.setHeader("Content-Disposition", "attachment;filename=\"" + java.net.URLEncoder.encode(filename, "UTF-8") + "\" ");
        	}
			
			if(path.contains("Error") || path.contains("Danwei")){
				if(file.canExecute()){
					file.delete();
				}
			}
			byte[] bt = new byte[in.available()];
			in.read(bt);
			response.getOutputStream().write(bt);
			response.getOutputStream().flush();
		} catch (FileNotFoundException e) {
			if (in != null)
				try {
					in.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			e.printStackTrace();
		} catch (IOException e) {
			if (in != null)
				try {
					in.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			e.printStackTrace();
		}
	}

}
