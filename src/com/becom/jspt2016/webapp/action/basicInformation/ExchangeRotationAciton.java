/**
 * ExchangeRotationAciton.java
 * com.becom.jspt2016.webapp.action.basicInformation
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webapp.action.basicInformation;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.dto.ExchangeRotationInfoDto;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbBizJzgjllgxx;
import com.becom.jspt2016.model.TbJzgxxsh;
import com.becom.jspt2016.service.ext.IExchangeRotationManagerExt;
import com.becom.jspt2016.service.ext.ITbJzgxxshManagerExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 *交流轮岗
 * <p>
 * 中小学
 * @author   kuangxiang
 * @Date	 2016年10月20日
 */

public class ExchangeRotationAciton extends PageAction {
	protected Logger logger = Logger.getLogger(this.getClass());
	public String[] ck;
	@Spring
	private IExchangeRotationManagerExt exchangeRotationManagerExt;
	@Spring
	private ITbJzgxxshManagerExt tbJzgxxshManagerExt;
	public ExchangeRotationInfoDto exRotInfoDto = new ExchangeRotationInfoDto();
	/**
	 * 交流轮岗Id
	 */
	public long jllgId;
	/**
	 * 教职工id
	 */
	public long jsId;
	/**
	 * 老师用户
	 */
	public TbBizJzgjbxx teacher;
	/**
	 * 机构用户
	 */
	public SysUser sUser;

	public TbJzgxxsh tbJzgxxsh;

	/**
	 * 搜索
	 * <p>
	 * @param request
	 */
	@DefaultAction
	public Object doSearch(HttpServletRequest request) {
		try {
			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("jsId", jbxx.getJsid());
			pageObj = exchangeRotationManagerExt.doSearch(param, pageNo, pageSize);
			tbJzgxxsh = tbJzgxxshManagerExt.query(jbxx.getJsid());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "exchangeRotationList.jsp";
	}

	/**
	 * 
	 * 删除
	 * <p>
	 * 删除教师账号
	 *
	 */
	public Object delete(HttpServletRequest request, HttpServletResponse response) {

		int result = 0;
		try {
			result = exchangeRotationManagerExt.delete(ck);
			if (result == 0) {
				message = "删除成功";
			}
		} catch (Exception e) {
			if (result == 1) {
				message = "删除失败";
			}
			e.printStackTrace();
		}
		return doSearch(request);
	}

	/**
	 * 添加
	 * <p>
	 *添加老师账号
	 * @param request
	 * @param response
	 */
	public Object add(HttpServletRequest request, HttpServletResponse response) {

		TbBizJzgjllgxx jllgxx = new TbBizJzgjllgxx();
		teacher = getSessionTeacher(request);
		jsId = teacher.getJsid();
		exRotInfoDto.setJsId(jsId);
		exchangeRotationManagerExt.add(exRotInfoDto, jsId);
		return doSearch(request);

	}

	/**
	 * 跳到编辑页面
	 */
	public void toEdit(HttpServletRequest request, HttpServletResponse response) {
		try {
			JSONObject json = new JSONObject();
			ExchangeRotationInfoDto ex = exchangeRotationManagerExt.get(jllgId);
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("ex", ex);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("ExchangeRotationAciton toEdit() is error!", e);
		}
	}

	/**
	 * 编辑
	 */
	public Object edit(HttpServletRequest request, HttpServletResponse response) {
		int result = 0;
		try {
			result = exchangeRotationManagerExt.edit(exRotInfoDto);
			if (result == 0) {
				message = "编辑成功";
			}
		} catch (Exception e) {
			if (result == 1) {
				message = "编辑失败";
			}
			e.printStackTrace();
		}
		return doSearch(request);
	}
}
