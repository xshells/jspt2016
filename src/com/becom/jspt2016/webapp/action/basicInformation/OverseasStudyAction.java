/**
 * OverseasStudyAction.java
 * com.becom.jspt2016.webapp.action.basicInformation
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webapp.action.basicInformation;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.dto.HwyxxxInfoDto;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzghwyxxx;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbJzgxxsh;
import com.becom.jspt2016.service.ITbBizJzghwyxxxManager;
import com.becom.jspt2016.service.ext.IOverseasStudyManagerExt;
import com.becom.jspt2016.service.ext.ITbJzgxxshManagerExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 * 海外研修（访学）
 * @author   wxl
 * @Date	 2016年10月19日 	 
 */
public class OverseasStudyAction extends PageAction {
	/**
	 * 海外研修(访学)Id组成的字符窜
	 */
	public String[] ck;
	/**
	 * 海外研修id
	 */
	public long hwyxId;
	/**
	 * 教师id
	 */
	public long jsid;
	/**
	 * 是否有海外研修经历
	 */
	public String sfjyhwyxjl;
	/**
	 * 开始日期
	 */
	public String ksrq;
	/**
	 * 结束日期
	 */
	public String jsrq;
	/**
	 * 国家地区
	 */
	public String gjdq;
	/**
	 * 培训机构名称
	 */
	public String pxjgmc;
	/**
	 * 培训项目名称
	 */
	public String pxxmmc;
	/**
	 * 项目组织单位名称
	 */
	public String xmzzdwmc;
	/**
	 * 不能为空项
	 * 报送人类别
	 */
	public String bsrlb;
	/**
	 * 审核状态
	 */
	public String shzt;
	/**
	 * 审核时间
	 */
	public Date shsj;
	/**
	 * 是否删除
	 */
	public String sfsc;
	/**
	 * 创建时间
	 */
	public Date cjsj;
	/**
	 *	用户实体类
	 */
	public SysUser sUser;

	/**
	 * 老师用户
	 */
	public TbBizJzgjbxx teacher;

	public TbJzgxxsh tbJzgxxsh;

	protected Logger logger = Logger.getLogger(this.getClass());

	public TbBizJzghwyxxx btBizJzghwyxInfo = new TbBizJzghwyxxx();

	@Spring
	public ITbBizJzghwyxxxManager tbBizJzghwyxxxManager;

	@Spring
	public IOverseasStudyManagerExt overseasStudyManagerExt;

	@Spring
	private ITbJzgxxshManagerExt tbJzgxxshManagerExt;

	@DefaultAction
	/**
	 * 
	 * 搜索
	 * <p>
	 * 海外培训(访学)列表
	 */
	public Object doSearch(HttpServletRequest request) {
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			jsid = jbxx.getJsid();
			params.put("jsid", jsid);
			pageObj = overseasStudyManagerExt.pageSearch(params, pageNo, pageSize);
			tbJzgxxsh = tbJzgxxshManagerExt.query(jsid);

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("OverseasStudyAction doSearch() is error!", e);
		}
		return "listoverseasstudy.jsp";
	}

	/**
	 * 
	 * 添加
	 * <p>
	 * 海外培训(访学)
	 */
	public Object addOverseasStudy(HttpServletRequest request) {

		Map<String, Object> params = new HashMap<String, Object>();
		pageObj = overseasStudyManagerExt.pageSearch(params, pageNo, pageSize);
		if (pageObj.getPageElements().isEmpty()) {
			//如果count>0 提示只能添加一条记录
			try {
				TbBizJzghwyxxx hwyxxx = new TbBizJzghwyxxx();
				teacher = getSessionTeacher(request);
				hwyxxx.setTbBizJzgjbxx(teacher);
				hwyxxx.setSfjyhwyxjl(sfjyhwyxjl);
				hwyxxx.setCjsj(cjsj);
				overseasStudyManagerExt.insertOverseasStudy(hwyxxx);
				params.put("rgt_msg", "添加成功!");
			} catch (Exception e) {
				logger.error("OverseasStudyAction addOverseasStudy() error!", e);
			}
		} else {
			params.put("error_msg", "只能添加一条数据");
			//提示错误
		}
		return doSearch(request);
	}

	/**
	 * 
	 * 删除
	 * <p>
	 * 删除(假删)海外培训(访学)
	 */
	public Object deleteUpdateOverseasStudy(HttpServletRequest request) {
		int result = 0;
		try {
			result = overseasStudyManagerExt.delete(ck);
			if (result == 1) {
				message = "删除成功";
			}
		} catch (Exception e) {
			if (result == 0) {
				message = "删除失败";
			}
			logger.error("OverseasStudyAction deleteUpdateOverseasStudy error!", e);
		}
		return doSearch(request);
	}

	/**
	 * 
	 * 查询
	 * <p>
	 * 根据id查询海外培训(访学)对象信息
	 */
	public void toUpdateOverseasStudy(HttpServletRequest request, HttpServletResponse response) {
		try {
			JSONObject jo = new JSONObject();
			HwyxxxInfoDto hwyxxx = overseasStudyManagerExt.getOverseasStudy(hwyxId);
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			jo.put("hwyxxx", hwyxxx);
			out.write(jo.toString());
			out.flush();
			out.close();
		} catch (IOException e) {
			logger.error("OverseasStudyAction toUpdateOverseasStudy is error!", e);

		}
	}

	/**
	 * 
	 * 更新
	 * <p>
	 * 根据id更新海外培训(访学)对象信息
	 */
	public Object updateOverseasStudy(HttpServletRequest request, HttpServletResponse response) {

		btBizJzghwyxInfo.setHwyxId(hwyxId);
		btBizJzghwyxInfo.setSfjyhwyxjl(sfjyhwyxjl);
		overseasStudyManagerExt.updateOverseasStudy(btBizJzghwyxInfo);
		try {
		} catch (Exception e) {
			logger.error("OverseasStudyAction updateOverseasStudy is error!", e);
		}
		return doSearch(request);
	}
}
