/**
 * PostAppointAction.java
 * com.becom.jspt2016.webapp.action.basicInformation
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.webapp.action.basicInformation;

import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.dto.TbCfgZdxbInfoDto;
import com.becom.jspt2016.dto.ZxdwlbZdxbDto;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzggwprxx;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbJzgxxsh;
import com.becom.jspt2016.service.ITbBizJzggwprxxManager;
import com.becom.jspt2016.service.ext.IJZGGWPRXXManagerExt;
import com.becom.jspt2016.service.ext.ITbJzgxxshManagerExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 * 岗位类别
 * <p>
 * @author   董锡福
 * @Date	 2016年10月20日 	 
 */
public class PostAppointAction extends PageAction {

	/**
	 * id数组
	 */
	public String[] ck;

	public long gwprId;//岗位聘任Id

	public String dzzw;//党政职务（或校级职务）

	public String gwlb;//岗位类别

	public String gwgj;//岗位等级

	public String sfsjt;//是否双肩挑

	public String sfzzfdy;//是否为辅导员

	public String dzjb;//党政级别

	public String prksny;//聘任开始年月

	public String rzksny;//任职开始年月

	public String sjtgwlb;//双肩挑岗位类别

	public String sjtgwdj;//双肩挑岗位等级

	public String sfzzcsxlzxgz;//是否专职从事心理咨询工作

	public String sfcyxlzxzgzs;//是否持有心理咨询资格证书

	public String sfjrqtgw;//是否兼任其他岗位

	public String jrgwlb;//兼任岗位类别

	public String jrgwdj;//兼任岗位等级

	public String zdxbm;//字典项编码

	@Spring
	public IJZGGWPRXXManagerExt jzggwprxxmanagerExt;

	@Spring
	public ITbBizJzggwprxxManager tbbizjzggwprxxmanager;

	@Spring
	private ITbJzgxxshManagerExt tbJzgxxshManagerExt;

	public TbBizJzggwprxx tbbizjzggwprxx;//岗位聘任

	public ZxdwlbZdxbDto zxdwlbzdxbdto;//字典dto

	public SysUser sUser;//用户实体类

	public TbJzgxxsh tbJzgxxsh;

	public TbBizJzgjbxx teacher;//教师实体类

	public String ssxd; //所属学段

	public long jsid; //教师Id

	public String dto1;//树
	public List<TbCfgZdxbInfoDto> conclusion;
	public List<TbCfgZdxbInfoDto> dzzwList;//高校
	public List<TbCfgZdxbInfoDto> dzzwMiddle;//中等

	@DefaultAction
	public Object doSearch(HttpServletRequest request) {
		try {

			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			Map<String, Object> param = new HashMap<String, Object>();

			long orgId = jbxx.getTbXxjgb().getXxjgid();
			jsid = jbxx.getJsid();
			param.put("jsid", jsid);
			param.put("orgId", orgId);
			pageObj = jzggwprxxmanagerExt.doSearch(param, pageNo, pageSize);
			ssxd = jzggwprxxmanagerExt.getXd(param);//查询角色
			tbJzgxxsh = tbJzgxxshManagerExt.query(jsid);
			conclusion = jzggwprxxmanagerExt.queryConclusion();//岗位等级下拉数据
			dzzwList = jzggwprxxmanagerExt.queryDzzw();//党政职务下拉数据本科
			dzzwMiddle = jzggwprxxmanagerExt.queryMiddle();//党政职务下拉数据中等，中小学

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "postAppoinList.jsp";
	}

	//添加提交
	public Object SubAddpoint(HttpServletRequest request, HttpServletResponse response) {
		try {
			TbBizJzggwprxx gw = new TbBizJzggwprxx();

			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			gw.setTbBizJzgjbxx(jbxx);

			gw.setDzzw(dzzw); //党政职务（或校级职务）   
			gw.setGwlb(gwlb); //岗位类别          
			gw.setGwgj(gwgj); //岗位等级          
			gw.setSfsjt(sfsjt); //是否双肩挑         
			gw.setSfzzfdy(sfzzfdy); //是否为辅导员        
			gw.setDzjb(dzjb); //党政级别          
			gw.setPrksny(prksny); //聘任开始年月        
			gw.setRzksny(rzksny); //任职开始年月      

			gw.setSjtgwlb(sjtgwlb);//双肩挑岗位类别
			gw.setSjtgwdj(sjtgwdj);//双肩挑岗位等级
			gw.setSfzzcsxlzxgz(sfzzcsxlzxgz);//是否专职从事心理咨询工作
			gw.setSfcyxlzxzgzs(sfcyxlzxzgzs);//是否持有心理咨询资格证书
			gw.setSfjrqtgw(sfjrqtgw);//是否兼任其他岗位--(中等职业学校用户)
			gw.setJrgwlb(jrgwlb);//兼任岗位类别
			gw.setJrgwdj(jrgwdj);//兼任岗位等级
			gw.setCjsj(new Date()); //创建时间
			gw.setShsj(new Date()); //审核时间
			jzggwprxxmanagerExt.SubAddpoint(gw);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return doSearch(request);
	}

	//修改前回显
	public void editPostAppoint(HttpServletRequest request, HttpServletResponse response) {
		try {
			JSONObject json = new JSONObject();
			tbbizjzggwprxx = jzggwprxxmanagerExt.editJzggwprxx(gwprId);
			PrintWriter out = response.getWriter();
			response.setContentType("text/xml;charset=UTF-8");
			json.put("dto", tbbizjzggwprxx);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	//删除
	public Object delPostAppoint(HttpServletRequest request, HttpServletResponse response) {
		try {
			jzggwprxxmanagerExt.removeGwpr(ck);
		} catch (Exception e) {

			e.printStackTrace();
		}
		return doSearch(request);
	}

	//执行修改
	public Object toSubAppoint(HttpServletRequest request, HttpServletResponse response) {
		int result = 0;
		TbBizJzggwprxx gw = new TbBizJzggwprxx();
		gw.setGwprId(gwprId);
		gw.setDzzw(dzzw); //党政职务（或校级职务）   
		gw.setGwlb(gwlb); //岗位类别          
		gw.setGwgj(gwgj); //岗位等级          
		gw.setSfsjt(sfsjt); //是否双肩挑         --

		gw.setSjtgwlb(sjtgwlb);//双肩挑岗位类别
		gw.setSjtgwdj(sjtgwdj);//双肩挑岗位等级
		gw.setSfzzcsxlzxgz(sfzzcsxlzxgz);//是否专职从事心理咨询工作
		gw.setSfcyxlzxzgzs(sfcyxlzxzgzs);//是否持有心理咨询资格证书--
		gw.setSfjrqtgw(sfjrqtgw);//是否兼任其他岗位--(中等职业学校用户)
		gw.setJrgwlb(jrgwlb);//兼任岗位类别
		gw.setJrgwdj(jrgwdj);//兼任岗位等级
		gw.setSfzzfdy(sfzzfdy); //是否为辅导员        
		gw.setDzjb(dzjb); //党政级别          
		gw.setPrksny(prksny); //聘任开始年月        
		gw.setRzksny(rzksny); //任职开始年月        
		gw.setCjsj(new Date()); //创建时间
		gw.setShsj(new Date()); //审核时间
		try {
			result = jzggwprxxmanagerExt.updateJzggwprxx(gw);
			if (result == 0) {
				message = "修改成功";
			}
		} catch (Exception e) {
			if (result == 1) {
				message = "修改失败";
			}
			e.printStackTrace();
		}
		return doSearch(request);
	}

	//岗位类别与岗位等级联动
	public void SelectPostGrade(HttpServletRequest request, HttpServletResponse response) {
		try {
			String ssxd = request.getParameter("ssxd");
			response.setContentType("text/xml;charset=UTF-8");
			JSONObject json = new JSONObject();
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("ssxd", ssxd);
			if (!"5".equals("zdxbm")) {
				if ("1".equals(zdxbm) || "2".equals(zdxbm)) {
					param.put("zdxbm", "1%");
				} else if ("3".equals(zdxbm)) {
					param.put("zdxbm", "2%");
				} else if ("4".equals(zdxbm)) {
					param.put("zdxbm", "3%");
				}
			}
			List<Map<String, Object>> seleclist = jzggwprxxmanagerExt.SelectPostGrade(param);
			PrintWriter out = response.getWriter();
			json.put("dto", seleclist);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	/*	public List<TbCfgZdxbInfoDto> queryDzzw{
			conclusion = tbBizJzgjyjxxxManagerExt.queryConclusion();

		}*/
}
