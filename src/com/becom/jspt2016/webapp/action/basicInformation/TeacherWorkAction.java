/**
 * BaseInfoAction.java
 * com.becom.jspt2016.webapp.action.basicInformation
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webapp.action.basicInformation;

import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.dto.TbBizJzggzjlxxDto;
import com.becom.jspt2016.model.TbBizJzggzjlxx;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbJzgxxsh;
import com.becom.jspt2016.service.ITbBizJzggzjlxxManager;
import com.becom.jspt2016.service.ITbBizJzgjbxxManager;
import com.becom.jspt2016.service.ext.ITbBizJzggzjlxxManagerExt;
import com.becom.jspt2016.service.ext.ITbJzgxxshManagerExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 * 工作经历
 *
 * @author   zhangchunming
 * @Date	 2016年10月19日 	 
 */
public class TeacherWorkAction extends PageAction<TbBizJzggzjlxx> {

	protected Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 工作经历Id组成的字符窜
	 */
	public String[] ck;
	/**
	 * 工作经历Id
	 */
	public long gzjlId;
	/**
	 * 教职工id
	 */
	public long jsid;
	/**
	 * 任职单位名称
	 */
	public String rzdwmc;
	/**
	 * 任职开始年月
	 */
	public String rzksny;
	/**
	 * 任职结束年月
	 */
	public String rzjsny;
	/**
	 * 单位性质类别
	 */
	public String dwxzlb;
	/**
	 * 任职岗位
	 */
	public String rzgw;
	/**
	 * 是否为现等级
	 */
	public String sfwxdj;
	/**
	 * 报送人用户类别
	 */
	public String bsrlb;
	/**
	 * 	审核状态
	 */
	public String shzt;
	/**
	 * 审核时间
	 */
	public Date shsj;
	/**
	 * 审核结果
	 */
	public String shjg;
	/**
	 * 是否删除
	 */
	public String sfsc;
	/**
	 * 创建人
	 */
	public String cjr;
	/**
	 * 创建时间
	 */
	public Date cjsj;
	/**
	 * 修改人
	 */
	public String xgr;
	/**
	 * 修改时间
	 */
	public Date xgsj;
	/**
	 * 返回页面数据
	 */
	public String ssxd;

	public TbBizJzggzjlxx tbBizJzggzjlxx;

	public TbBizJzgjbxx tbBizJzgjbxx;

	public TbJzgxxsh tbJzgxxsh;

	@Spring
	private ITbBizJzggzjlxxManager tbBizJzggzjlxxManager;

	@Spring
	private ITbBizJzggzjlxxManagerExt tbBizJzggzjlxxManagerExt;

	@Spring
	private ITbBizJzgjbxxManager tbBizJzgjbxxManager;

	@Spring
	private ITbJzgxxshManagerExt tbJzgxxshManagerExt;

	@DefaultAction
	public Object doSearch(HttpServletRequest request) {
		try {
			TbBizJzgjbxx jbxxx = getSessionTeacher(request);
			Map<String, Object> map = new HashMap<String, Object>();
			System.out.println(jbxxx.getJsid());
			map.put("jsid", jbxxx.getJsid());
			pageObj = tbBizJzggzjlxxManagerExt.doSearch(map, pageNo, pageSize);
			tbBizJzgjbxx = tbBizJzgjbxxManager.get(jbxxx.getJsid());
			tbJzgxxsh = tbJzgxxshManagerExt.query(jbxxx.getJsid());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "workexperience.jsp";
	}

	/**
	 * 添加
	 *
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 * @throws Exception 
	 */
	public void add(HttpServletRequest request) throws Exception {

		TbBizJzggzjlxx tbBizJzggzjlxx = new TbBizJzggzjlxx();
		TbBizJzgjbxx jbxxx = getSessionTeacher(request);
		tbBizJzggzjlxx.setTbBizJzgjbxx(jbxxx);
		tbBizJzggzjlxx.setRzdwmc(rzdwmc);
		tbBizJzggzjlxx.setRzksny(rzksny);
		tbBizJzggzjlxx.setRzjsny(rzjsny);
		tbBizJzggzjlxx.setDwxzlb(dwxzlb);
		tbBizJzggzjlxx.setRzgw(rzgw);
		tbBizJzggzjlxxManagerExt.add(tbBizJzggzjlxx);
	}

	/**
	 * 
	 * 删除
	 *
	 * @param request
	 */
	public Object deleteGz(HttpServletRequest request, HttpServletResponse response) {
		try {
			TbBizJzgjbxx jbxxx = getSessionTeacher(request);
			if (jbxxx.getJsid() != jsid) {
				throw new Exception("教师只能操作自己的基本信息");
			}
			Map<String, Object> params = new HashMap<String, Object>();
			tbBizJzggzjlxxManagerExt.deleteGz(ck);

		} catch (Exception e) {
			logger.error("TbBizJzggzjlxxManagerAction deleteGz() is error!", e);
		}
		return doSearch(request);
	}

	/**
	 *去编辑页面
	 *
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	public void toEdit(HttpServletRequest request, HttpServletResponse response) {
		try {
			TbBizJzgjbxx jbxxx = getSessionTeacher(request);
			if (jbxxx.getJsid() != jsid) {
				throw new Exception("教师只能操作自己的基本信息");
			}
			JSONObject json = new JSONObject();
			TbBizJzggzjlxxDto tbBizJzggzjlxx1 = tbBizJzggzjlxxManagerExt.get(gzjlId);
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("tbBizJzggzjlxx", tbBizJzggzjlxx1);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("TbBizJzggzjlxxManagerAction toEdit() is error!", e);
		}
	}

	/**
	 *编辑
	 *
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 * @throws Exception 
	 */
	public void edit(HttpServletRequest request, HttpServletResponse response) throws Exception {
		TbBizJzgjbxx jbxxx = getSessionTeacher(request);
		if (jbxxx.getJsid() != jsid) {
			throw new Exception("教师只能操作自己的基本信息");
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("gzjlid", gzjlId);
		params.put("rzdwmc", rzdwmc);
		params.put("rzksny", rzksny);
		params.put("rzjsny", rzjsny);
		params.put("dwxzlb", dwxzlb);
		params.put("rzgw", rzgw);
		tbBizJzggzjlxxManagerExt.edit(params);
	}

}
