package com.becom.jspt2016.webapp.action.basicInformation;

/**
 * TeacherMoralityAction.java
 * com.becom.jspt2016.webapp.action.morality
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.dto.PostAppointDto;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbdyxx;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbJzgxxsh;
import com.becom.jspt2016.service.ITbBizJzgjbdyxxManager;
import com.becom.jspt2016.service.ext.IBasicTreatManagerExt;
import com.becom.jspt2016.service.ext.ITbJzgxxshManagerExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 * 基本待遇
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   董锡福
 * @Date	 2016年10月20日 	 
 */
public class BasicTreatManagerAction extends PageAction {

	/**
	 * id数组
	 */
	public String[] ck;

	/**
	 * 年度
	 */
	public String nd;
	/**
	 * 年工资收入
	 */
	public String ngzsr;
	/**
	 * 基本工资
	 */
	public String jbgz;
	/**
	 * 绩效工资
	 */
	public String jxgz;
	/**
	 * 乡村教师生活补助
	 */
	public String xcjsshbz;
	/**
	 * 其他津贴补贴
	 */
	public String jtbt;
	/**
	 * 其他
	 */
	public String qt;
	/**
	 * 五险一金
	 */
	public String wxyj;
	/**
	 * 生活补助
	 */
	public String shbz;
	/**
	 * 年龄
	 */
	public int age;
	/**
	 * 聘任单位名称
	 */
	public String PRDWMC;
	/**
	 * 特教津贴补贴
	 */
	public String tjjtbt;

	public List<PostAppointDto> PrList;
	/**
	 * 基本待遇Id
	 */
	public long jbdyId;

	public SysUser sUser;//用户实体类
	public TbBizJzgjbxx teacher;//教师实体类
	public String ssxd; //所属学段
	public long jsid; //教师Id
	public TbJzgxxsh tbJzgxxsh;

	@Spring
	public ITbBizJzgjbdyxxManager tbbizjzgjbdyxxmanager;

	public TbBizJzgjbdyxx tbbizjzgjbdyxx;

	@Spring
	public IBasicTreatManagerExt basictreatmanagerExt;

	@Spring
	private ITbJzgxxshManagerExt tbJzgxxshManagerExt;

	/**
	 * 
	 * TODO(基本待遇信息查询)
	 * @param request pageObj
	 * @return 
	 */
	@DefaultAction
	public Object doSearch(HttpServletRequest request) {

			teacher = getSessionTeacher(request);
			long orgId = teacher.getTbXxjgb().getXxjgid();
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("orgId", orgId);
			jsid = teacher.getJsid();
			param.put("jsid", jsid);
			pageObj = basictreatmanagerExt.doSearch(param, pageNo, pageSize);
			ssxd = basictreatmanagerExt.getXd(param);//查询角色
			tbJzgxxsh = tbJzgxxshManagerExt.query(jsid);
//			ssxd = tbJzgxxsh.getSsxd();
		return "basicTreatList.jsp";
	}

	/**
	 * TODO(基本待遇添加方法)
	 */
	public Object SubAddTreat(HttpServletRequest request, HttpServletResponse response) {
		try {
			teacher = getSessionTeacher(request);
			TbBizJzgjbdyxx dy = new TbBizJzgjbdyxx();
			dy.setTbBizJzgjbxx(teacher);
			dy.setNd(nd);//年度
			dy.setNgzsr(ngzsr);//年工资收入
			dy.setJbgz(jbgz);//基本工资
			dy.setJxgz(jxgz);//绩效工资
			dy.setXcjsshbz(xcjsshbz);//乡村教师生活补助
			dy.setQt(qt);//其他
			dy.setWxyj(wxyj);//五险一金
			dy.setTjjtbt(tjjtbt);//特教津贴补贴
			dy.setJtbt(jtbt);//其他津贴补贴
			dy.setShsj(new Date());//审核时间
			dy.setCjsj(new Date());//创建时间
			basictreatmanagerExt.SubAddpoint(dy);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return doSearch(request);
	}

	//修改前回显
	public void editbasicTreat(HttpServletRequest request, HttpServletResponse response) {
		try {
			JSONObject json = new JSONObject();
			tbbizjzgjbdyxx = basictreatmanagerExt.editBasicTreat(jbdyId);
			PrintWriter out = response.getWriter();
			response.setContentType("text/xml;charset=UTF-8");
			json.put("dto", tbbizjzgjbdyxx);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	//删除
	public Object delBasicTreat(HttpServletRequest request, HttpServletResponse response) {
		try {
			basictreatmanagerExt.removeBasicTreat(ck);
		} catch (Exception e) {

			e.printStackTrace();
		}
		return doSearch(request);
	}

	//执行修改
	public Object toUpdateTreat(HttpServletRequest request, HttpServletResponse response) {
		TbBizJzgjbdyxx dy = new TbBizJzgjbdyxx();
		dy.setJbdyId(jbdyId);//id
		dy.setNd(nd);//年度
		dy.setNgzsr(ngzsr);//年工资收入
		dy.setJbgz(jbgz);//基本工资
		dy.setJxgz(jxgz);//绩效工资
		dy.setXcjsshbz(xcjsshbz);//乡村教师生活补助
		dy.setQt(qt);//其他

		dy.setWxyj(wxyj);//五险一金

		dy.setJtbt(jtbt);//其他津贴补贴
		int result = 0;
		try {
			result = basictreatmanagerExt.updateBasicTreat(dy);
			if (result == 0) {
				message = "修改成功";
			}
		} catch (Exception e) {
			if (result == 1) {
				message = "修改失败";
			}
			e.printStackTrace();
		}
		return doSearch(request);
	}

}
