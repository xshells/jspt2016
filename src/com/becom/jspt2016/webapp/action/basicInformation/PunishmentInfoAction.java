/**
 * TeacherMoralityAction.java
 * com.becom.jspt2016.webapp.action.morality
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webapp.action.basicInformation;

import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.dto.TbBizJzgsdxxInfoDto;
import com.becom.jspt2016.dto.TbCfgZdxbInfoDto;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbBizJzgsdcfxx;
import com.becom.jspt2016.model.TbJzgxxsh;
import com.becom.jspt2016.service.ITbBizJzgjbxxManager;
import com.becom.jspt2016.service.ITbBizJzgsdcfxxManager;
import com.becom.jspt2016.service.ITbBizJzgsdkhxxManager;
import com.becom.jspt2016.service.ext.ITbBizJzgsdxxManagerExt;
import com.becom.jspt2016.service.ext.ITbJzgxxshManagerExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 * 师德处分信息
 *
 * @author   shanjigang
 * @Date	 2016年10月16日 	 
 */
public class PunishmentInfoAction extends PageAction {
	public int jsid;//教师id
	public int khid;//考核年月
	public int cfid;//处分id
	public String khny;//考核年月
	public String khjl;//考核记录
	public String cflb;//处分类别
	public String cfyy;//处分年月
	public String ccfsrq;//处分发生日期
	public String cjsj;//创建时间
	public TbBizJzgsdcfxx tbBizJzgsdcfxx;
	public TbBizJzgjbxx tbBizJzgjbxx;
	public String[] ck;//删除ids
	public String ssxd;
	@Spring
	private ITbBizJzgsdxxManagerExt tbBizJzgsdxxManagerExt;
	@Spring
	private ITbBizJzgsdkhxxManager tbBizJzgsdkhxxManager;
	@Spring
	private ITbBizJzgsdcfxxManager tbBizJzgsdcfxxManager;
	@Spring
	private ITbBizJzgjbxxManager tbBizJzgjbxxManager;
	@Spring
	private ITbJzgxxshManagerExt tbJzgxxshManagerExt;
	protected Logger logger = Logger.getLogger(this.getClass());
	public TbBizJzgsdxxInfoDto dto;
	public List<TbCfgZdxbInfoDto> conclusion;
	public String sdkhdwmc;
	public TbJzgxxsh tbJzgxxsh;

	@DefaultAction
	public Object doSearch(HttpServletRequest request) {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			params.put("jsid", jbxx.getJsid());
			pageObj = tbBizJzgsdxxManagerExt.doQuerycf(params, pageNo, pageSize);
			conclusion = tbBizJzgsdxxManagerExt.queryConclusion();
			tbJzgxxsh = tbJzgxxshManagerExt.query(jbxx.getJsid());
			ssxd = tbBizJzgjbxxManager.get(jbxx.getJsid()).getTbXxjgb().getSsxd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "punishmentList.jsp";
	}

	//添加教师处分信息
	public Object addPunishmentInfo(HttpServletRequest request, HttpServletResponse response) {
		try {
			response.setCharacterEncoding("UTF-8");
			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			jbxx.setJsid(jbxx.getJsid());
			Date currentTime = new Date();
			TbBizJzgsdcfxx cfxx = new TbBizJzgsdcfxx();
			cfxx.setTbBizJzgjbxx(jbxx);
			cfxx.setCcfsrq(ccfsrq);
			cfxx.setShsj(currentTime);
			cfxx.setCjsj(currentTime);
			cfxx.setCflb(cflb);
			cfxx.setCcfsrq(ccfsrq);
			cfxx.setCfyy(cfyy);
			cfxx.setTbBizJzgjbxx(jbxx);
			cfxx.setCjr(String.valueOf(jbxx.getJsid()));
			tbBizJzgsdxxManagerExt.addPunishmentInfo(cfxx);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return doSearch(request);
	}

	//修改教师处分信息
	public Object editPunishmentInfo(HttpServletRequest request, HttpServletResponse response) {
		try {
			response.setCharacterEncoding("UTF-8");
			tbBizJzgsdxxManagerExt.editChufen(cfid, cflb, cfyy, ccfsrq);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return doSearch(request);
	}

	//删除教师处分信息
	public Object delPunishmentInfo(HttpServletRequest request, HttpServletResponse response) {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			tbBizJzgsdxxManagerExt.delPunishment(ck);

		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return doSearch(request);
	}

	//页面编辑回显
	public void doQuery(HttpServletRequest request, HttpServletResponse reponse) {
		try {
			reponse.setContentType("text/xml;charset=UTF-8");
			JSONObject json = new JSONObject();
			TbBizJzgjbxx jbxx = (TbBizJzgjbxx) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
			dto = tbBizJzgsdxxManagerExt.editPunishmentInfo(jbxx.getJsid(), cfid);
			conclusion = tbBizJzgsdxxManagerExt.queryConclusion();
			PrintWriter out = reponse.getWriter();
			json.put("dto", dto);
			json.put("conclusion", conclusion);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return;
	}

}
