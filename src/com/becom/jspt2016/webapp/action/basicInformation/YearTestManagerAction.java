/**
 * YearTestManagerAction.java
 * com.becom.jspt2016.webapp.action.basicInformation
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.webapp.action.basicInformation;

/**
 * TeacherMoralityAction.java
 * com.becom.jspt2016.webapp.action.morality
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbBizJzgndkhxx;
import com.becom.jspt2016.model.TbJzgxxsh;
import com.becom.jspt2016.service.ITbBizJzgndkhxxManager;
import com.becom.jspt2016.service.ext.ITbJzgxxshManagerExt;
import com.becom.jspt2016.service.ext.IYearTestManagerExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 * 年度考核
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   董锡福
 * @Date	 2016年10月21日 	 
 */
public class YearTestManagerAction extends PageAction {

	/**
	 * id数组
	 */
	public String[] ck;

	public String khnd;//考核年度

	public String khdwmc;//考核单位名称

	public String khjg;//考核结果

	public long ndkhId; //考核id

	@Spring
	public ITbBizJzgndkhxxManager tbbizjzgndkhxxmanager;

	public TbBizJzgndkhxx tbbizjzgndkhxx;//考核年度实体

	public TbJzgxxsh tbJzgxxsh;

	@Spring
	public IYearTestManagerExt yeartestmanagerExt;//考核年度接口

	@Spring
	private ITbJzgxxshManagerExt tbJzgxxshManagerExt;

	public SysUser sUser;//用户实体类

	public TbBizJzgjbxx teacher;//教师实体类

	public String ssxd; //所属学段

	public long jsid; //教师Id

	public String xxjgmc; //学校机构名称

	/**
	 *   年度考核查询方法
	 * @param request
	 * @return pageObj
	 */
	@DefaultAction
	public Object doSearch(HttpServletRequest request) {
		try {
			//获取用户信息
			String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
			//机构用户
			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			long orgId = jbxx.getTbXxjgb().getXxjgid();
			Map<String, Object> param = new HashMap<String, Object>();
			jsid = jbxx.getJsid();
			param.put("orgId", orgId);
			param.put("jsid", jsid);
			pageObj = yeartestmanagerExt.doSearch(param, pageNo, pageSize);
			xxjgmc = yeartestmanagerExt.getOrgNm(param);//查询学校机构名称
			tbJzgxxsh = tbJzgxxshManagerExt.query(jsid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "yearTest.jsp";
	}

	/**
	 * 增加
	 * 添加年度考核信息
	 */
	public Object SubAddYearTest(HttpServletRequest request, HttpServletResponse response) {
		try {
			TbBizJzgndkhxx kh = new TbBizJzgndkhxx();
			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			kh.setTbBizJzgjbxx(jbxx);//教师基本信息
			kh.setKhnd(khnd); //考核年度      
			kh.setKhjg(khjg); //考核结果      
			kh.setKhdwmc(khdwmc); //考核单位名称
			kh.setShsj(new Date());
			kh.setCjsj(new Date());
			yeartestmanagerExt.SubAddYearTest(kh);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return doSearch(request);
	}

	/**
	 * 回显
	 * 修改前回显选中信息
	 */
	public void editbasicYearTest(HttpServletRequest request, HttpServletResponse response) {
		try {
			response.setContentType("text/xml;charset=UTF-8");
			JSONObject json = new JSONObject();
			tbbizjzgndkhxx = yeartestmanagerExt.editYearTest(ndkhId);
			PrintWriter out = response.getWriter();
			json.put("dto", tbbizjzgndkhxx);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	//删除
	public Object delBasicYearTest(HttpServletRequest request, HttpServletResponse response) {
		try {
			yeartestmanagerExt.removeYearTest(ck);
		} catch (Exception e) {

			e.printStackTrace();
		}
		return doSearch(request);
	}

	//执行修改
	public Object toUpdateYearTest(HttpServletRequest request, HttpServletResponse response) {
		TbBizJzgndkhxx kh = new TbBizJzgndkhxx();
		kh.setNdkhId(ndkhId);//年度考核Id
		kh.setKhnd(khnd); //考核年度      
		kh.setKhjg(khjg); //考核结果      
		kh.setKhdwmc(khdwmc); //考核单位名称
		int result = 0;
		try {
			result = yeartestmanagerExt.updateYearTest(kh);
			if (result == 0) {
				message = "修改成功";
			}
		} catch (Exception e) {
			if (result == 1) {
				message = "修改失败";
			}
			e.printStackTrace();
		}
		return doSearch(request);
	}

}
