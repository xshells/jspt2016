/**
 * TeacherMoralityAction.java
 * com.becom.jspt2016.webapp.action.morality
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webapp.action.basicInformation;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.dto.TbBizJzgrxrcxmxxInfoDto;
import com.becom.jspt2016.dto.TbCfgZdxbInfoDto;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbBizJzgrxrcxmxx;
import com.becom.jspt2016.model.TbJzgxxsh;
import com.becom.jspt2016.service.ext.ITbBizJzgjyjxxxManagerExt;
import com.becom.jspt2016.service.ext.ITbBizJzgrxrcxmxxManagerExt;
import com.becom.jspt2016.service.ext.ITbJzgxxshManagerExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 * 师德
 * <p>
 * TODO(这里描述这个类补充说明 – 可选)
 *
 * @author   shanjigang
 * @Date	 2016年10月16日 	 
 */
public class SelectedTalentAction extends PageAction {
	/**
	 * 入选项目名称
	 */
	public String rxrcxmmc;

	/**
	 * 入选年月
	 */
	public String rxny;

	/**
	 * 创建时间
	 */
	public Date cjsj;

	/**
	 * 教师id
	 */
	public long jsid;

	/**
	 * 项目id
	 */
	public long xmid;

	public String[] ck;

	public TbBizJzgrxrcxmxxInfoDto dto;

	public TbJzgxxsh tbJzgxxsh;

	@Spring
	private ITbBizJzgrxrcxmxxManagerExt tbBizJzgrxrcxmxxManagerExt;

	protected Logger logger = Logger.getLogger(this.getClass());

	public String dto1;

	public String dtozxx;

	public String ssxd;

	@Spring
	private ITbBizJzgjyjxxxManagerExt tbBizJzgjyjxxxManagerExt;

	@Spring
	private ITbJzgxxshManagerExt tbJzgxxshManagerExt;

	@DefaultAction
	public Object doSearch(HttpServletRequest request) {
		try {
			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			long orgId = jbxx.getTbXxjgb().getXxjgid();
			Map<String, Object> params1 = new HashMap<String, Object>();
			params1.put("orgId", orgId);
			ssxd = tbBizJzgjyjxxxManagerExt.getXd(params1);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsid", jbxx.getJsid());
			//入选人才高校高职
			pageObj = tbBizJzgrxrcxmxxManagerExt.doQuery(params, pageNo, pageSize);
			List<TbCfgZdxbInfoDto> funcs = tbBizJzgrxrcxmxxManagerExt.searchData();
			tbJzgxxsh = tbJzgxxshManagerExt.query(jbxx.getJsid());
			dto1 = seachData(funcs);
			//入选人才中小学中职
			List<TbCfgZdxbInfoDto> list = tbBizJzgrxrcxmxxManagerExt.searchDatazxx();
			dtozxx = seachData(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "selectedTalentList.jsp";
	}

	//添加入选人才项目
	public Object addSeclectTalent(HttpServletRequest request, HttpServletResponse response) {
		try {
			boolean reap = false;
			rxrcxmmc = new String(request.getParameter("rxrcxmmc").getBytes("iso-8859-1"), "utf-8");
			response.setCharacterEncoding("UTF-8");
			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			jsid = jbxx.getJsid();
			Date currentTime = new Date();
			TbBizJzgrxrcxmxx rxxm = new TbBizJzgrxrcxmxx();
			rxxm.setRxrcxmmc(rxrcxmmc);
			rxxm.setCjsj(currentTime);
			rxxm.setRxny(rxny);
			rxxm.setBsrlb("4");
			rxxm.setShzt("1");
			rxxm.setShsj(currentTime);
			rxxm.setShjg("1");
			rxxm.setCjsj(currentTime);
			rxxm.setSfsc("0");
			rxxm.setCjr(String.valueOf(jsid));
			if (reap == false) {
				tbBizJzgrxrcxmxxManagerExt.addSeclectTeach(rxxm, String.valueOf(jsid));
				reap = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return doSearch(request);
	}

	//编辑入选人才项目
	public Object editSelectedTalent(HttpServletRequest request, HttpServletResponse response) {
		try {
			rxrcxmmc = new String(request.getParameter("rxrcxmmc").getBytes("iso-8859-1"), "utf-8");
			response.setCharacterEncoding("UTF-8");
			tbBizJzgrxrcxmxxManagerExt.editSeclectTeach(rxrcxmmc, rxny, String.valueOf(xmid));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return doSearch(request);
	}

	//删除入选人才项目
	public Object delSelectTalent(HttpServletRequest request, HttpServletResponse response) {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			tbBizJzgrxrcxmxxManagerExt.delSeclectTeach(ck);

		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return doSearch(request);
	}

	public String seachData(List<TbCfgZdxbInfoDto> funcs) {
		TbCfgZdxbInfoDto f = null;
		List<TbCfgZdxbInfoDto> f1 = new ArrayList<TbCfgZdxbInfoDto>();
		List<TbCfgZdxbInfoDto> f2 = new ArrayList<TbCfgZdxbInfoDto>();
		List<TbCfgZdxbInfoDto> f3 = new ArrayList<TbCfgZdxbInfoDto>();
		//查出一级节点
		for (int i = 0; i < funcs.size(); i++) {
			f = funcs.get(i);
			if ((f.getFzdx() == null)) {
				f1.add(f);//一级节点
				/*funcs.remove(i);
				i--;*/
			}
		}
		//查出二级节点
		for (int i = 0; i < f1.size(); i++) {
			for (int j = 0; j < funcs.size(); j++) {
				if (f1.get(i).getZdxbm().equals(funcs.get(j).getFzdx())) {
					f2.add(funcs.get(j));
					/*f2.remove(j);
					j--;*/
				}

			}
		}
		//查询三级节点
		for (int i = 0; i < f2.size(); i++) {
			for (int j = 0; j < funcs.size(); j++) {
				if (f2.get(i).getZdxbm().equals(funcs.get(j).getFzdx())) {
					f3.add(funcs.get(j));
					/*f2.remove(j);
					j--;*/
				}

			}
		}

		//拼接json
		StringBuilder sb = new StringBuilder("[");
		TbCfgZdxbInfoDto sf1 = null;
		TbCfgZdxbInfoDto sf2 = null;
		TbCfgZdxbInfoDto sf3 = null;
		for (int i = 0; i < f1.size(); i++) {
			sf1 = f1.get(i);
			sb.append("{");//1
			sb.append("'id':" + sf1.getZdxbm());
			sb.append(",'text':" + "'" + sf1.getZdxmc() + "'");
			sb.append(",'children':[");//2
			for (int j = 0; j < f2.size(); j++) {
				sf2 = f2.get(j);
				if (sf1.getZdxbm().equals(sf2.getFzdx())) {
					sb.append("{");
					sb.append("'id':" + sf2.getZdxbm());
					sb.append(",'text':" + "'" + sf2.getZdxmc() + "'");
					//					sb.append(",'url':" + "'" + sf2.getUrl() + "'");

					if (f3 != null && f3.size() > 0) {
						sb.append(",'children':[");
						for (int j2 = 0; j2 < f3.size(); j2++) {
							sf3 = f3.get(j2);
							if (sf2.getZdxbm().equals(sf3.getFzdx())) {
								sb.append("{");
								sb.append("'id':" + sf3.getZdxbm());
								sb.append(",'text':" + "'" + sf3.getZdxmc() + "'");
								//								sb.append(",'url':" + "'" + sf3.getUrl() + "'");
								sb.append("},");
							}
						}
						if (regexCheckStr(sb.toString())) {
							sb.replace(sb.length() - 1, sb.length(), "");
						}
						sb.append("]");

					}

					sb.append("},");//
				}
			}
			if (regexCheckStr(sb.toString())) {
				sb.replace(sb.length() - 1, sb.length(), "");
			}
			sb.append("]");//2

			sb.append("},");//1end
		}

		if (regexCheckStr(sb.toString())) {
			sb.replace(sb.length() - 1, sb.length(), "");
		}
		sb.append("]");//最外一层
		return sb.toString();
	}

	private boolean regexCheckStr(String data) {
		if (data.endsWith(",")) {
			return true;
		}
		return false;
	}

	//查询入选人才项目每条信息
	public void doQuery(HttpServletRequest request, HttpServletResponse reponse) {
		try {
			reponse.setContentType("text/xml;charset=UTF-8");
			JSONObject json = new JSONObject();
			TbBizJzgjbxx jbxx = (TbBizJzgjbxx) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
			jsid = jbxx.getJsid();
			dto = tbBizJzgrxrcxmxxManagerExt.editSelectedTalent(jsid, xmid);
			PrintWriter out = reponse.getWriter();
			json.put("dto", dto);
			out.write(json.toString());
			out.flush();
			out.close();
			//			return "../basicInformation/middleSmallList.jsp";
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return;
	}
}
