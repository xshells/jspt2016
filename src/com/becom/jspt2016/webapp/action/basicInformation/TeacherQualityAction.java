/**
 * TeacherQualityAction.java
 * com.becom.jspt2016.webapp.action.basicInformation
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webapp.action.basicInformation;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.dto.TbBizJszgxxDto;
import com.becom.jspt2016.model.TbBizJszgxx;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbJzgxxsh;
import com.becom.jspt2016.service.ITbBizJzgjbxxManager;
import com.becom.jspt2016.service.ext.ITbBizJszgxxManagerExt;
import com.becom.jspt2016.service.ext.ITbJzgxxshManagerExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 * 教师资格管理
 *
 * @author   zhangchunming
 * @Date	 2016年10月24日 	 
 */
public class TeacherQualityAction extends PageAction<TbBizJszgxx> {

	protected Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 传参数
	 */
	public long jszgid;
	public String jszgzzl;
	public String jszgzhm;
	public String rjxk;
	public String zsbfrq;
	public String zsrdjg;
	public String[] ck;
	public long jsid;

	public TbBizJzgjbxx tbBizJzgjbxx;

	public TbJzgxxsh tbJzgxxsh;

	@Spring
	private ITbBizJszgxxManagerExt tbBizJszgxxManagerExt;

	@Spring
	private ITbBizJzgjbxxManager tbBizJzgjbxxManager;

	@Spring
	private ITbJzgxxshManagerExt tbJzgxxshManagerExt;

	/**
	 * 查询
	 *
	 * @param request
	 */
	@DefaultAction
	public Object doSearch(HttpServletRequest request) {
		try {
			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			pageObj = tbBizJszgxxManagerExt.doSearch(jbxx.getJsid(), pageNo, pageSize);
			tbBizJzgjbxx = tbBizJzgjbxxManager.get(jbxx.getJsid());
			tbJzgxxsh = tbJzgxxshManagerExt.query(jbxx.getJsid());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "teacherquality.jsp";
	}

	/**
	 * 添加
	 *
	 */
	public void add(HttpServletRequest request) {
		try {
			TbBizJzgjbxx tjbxx = getSessionTeacher(request);
			System.out.println(tjbxx.getJsid() + "aaaa" + jsid);
			if (tjbxx.getJsid() != jsid) {
				throw new Exception("教师只能操作自己的基本信息");
			}
			TbBizJszgxx tbBizJszgxx = new TbBizJszgxx();
			TbBizJzgjbxx jbxx = new TbBizJzgjbxx();
			jbxx.setJsid(tjbxx.getJsid());
			tbBizJszgxx.setTbBizJzgjbxx(jbxx);
			tbBizJszgxx.setJszgzzl(jszgzzl);
			tbBizJszgxx.setJszgzhm(jszgzhm);
			tbBizJszgxx.setRjxk(rjxk);
			tbBizJszgxx.setZsbfrq(zsbfrq);
			tbBizJszgxx.setZsrdjg(zsrdjg);
			tbBizJszgxxManagerExt.add(tbBizJszgxx);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 去编辑
	 */
	public void toEdit(HttpServletRequest request, HttpServletResponse response) {
		try {
			TbBizJzgjbxx tjbxx = getSessionTeacher(request);
			if (tjbxx.getJsid() != jsid) {
				throw new Exception("教师只能操作自己的基本信息");
			}
			JSONObject json = new JSONObject();
			TbBizJszgxxDto tbBizJszgxx = tbBizJszgxxManagerExt.toEdit(jszgid);
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("tbBizJszgxx", tbBizJszgxx);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("TeacherQualityAction toEdit() is error!", e);
		}
	}

	/**
	 *编辑
	 * @throws Exception 
	 *
	 */
	public void edit(HttpServletRequest request, HttpServletResponse response) throws Exception {
		TbBizJzgjbxx tjbxx = getSessionTeacher(request);
		if (tjbxx.getJsid() != jsid) {
			throw new Exception("教师只能操作自己的基本信息");
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jszgid", jszgid);
		params.put("jszgzzl", jszgzzl);
		params.put("jszgzhm", jszgzhm);
		params.put("rjxk", rjxk);
		params.put("zsbfrq", zsbfrq);
		params.put("zsrdjg", zsrdjg);
		tbBizJszgxxManagerExt.edit(params);
	}

	/**
	 * 
	 * 删除
	 *
	 * @param request
	 */
	public Object delete(HttpServletRequest request, HttpServletResponse response) {
		try {
			TbBizJzgjbxx tjbxx = getSessionTeacher(request);
			if (tjbxx.getJsid() != jsid) {
				throw new Exception("教师只能操作自己的基本信息");
			}
			Map<String, Object> params = new HashMap<String, Object>();
			tbBizJszgxxManagerExt.delete(ck);

		} catch (Exception e) {
			logger.error("TeacherQualityAction delete() is error!", e);
		}
		return doSearch(request);
	}

}
