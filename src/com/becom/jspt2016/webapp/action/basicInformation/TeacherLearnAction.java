/**
 * LearnExperienceAction.java
 * com.becom.jspt2016.webapp.action.basicInformation
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webapp.action.basicInformation;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.dto.TbBizJzgxxjlxxDto;
import com.becom.jspt2016.dto.TbCfgZdxbInfoDto;
import com.becom.jspt2016.dto.ZxdwlbZdxbDto;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbBizJzgxxjlxx;
import com.becom.jspt2016.model.TbJzgxxsh;
import com.becom.jspt2016.service.ITbBizJzgjbxxManager;
import com.becom.jspt2016.service.ITbBizJzgxxjlxxManager;
import com.becom.jspt2016.service.ext.ITbBizJzgxxjlxxManagerExt;
import com.becom.jspt2016.service.ext.ITbCfgZdxbManagerExt;
import com.becom.jspt2016.service.ext.ITbJzgxxshManagerExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 * 学习经历
 *
 * @author   zhangchunming
 * @Date	 2016年10月17日 	 
 */
public class TeacherLearnAction extends PageAction<TbBizJzgxxjlxx> {

	protected Logger logger = Logger.getLogger(this.getClass());
	/**
	 * 学习经历id数组
	 */
	public String[] ck;

	public String hdxl;
	public String hdxldgjdq;
	public String hdxldyxhjg;
	public String sfsflzy;
	public String sxzy;
	public String rxny;
	public String byny;
	public String xwcc;
	public String xwmc;
	public String hdxwdgjdq;
	public String hdxwdyxhjg;
	public String xwsyny;
	public String xxfs;
	public String zxdwlb;
	public long xlId;
	public String ssxd;
	public long jsid;

	public String zxdwlblist;

	public TbBizJzgjbxx tbBizJzgjbxx;

	public TbJzgxxsh tbJzgxxsh;

	@Spring
	private ITbBizJzgxxjlxxManager tbBizJzgxxjlxxManager;

	@Spring
	private ITbBizJzgxxjlxxManagerExt tbBizJzgxxjlxxManagerExt;

	@Spring
	private ITbCfgZdxbManagerExt tbCfgZdxbManagerExt;

	@Spring
	private ITbBizJzgjbxxManager tbBizJzgjbxxManager;

	@Spring
	private ITbJzgxxshManagerExt tbJzgxxshManagerExt;
	public List<TbCfgZdxbInfoDto> conclusion;
	public String zdxbm;
	public List<TbCfgZdxbInfoDto> conclusion1;

	@DefaultAction
	public Object doSearch(HttpServletRequest request) {
		try {
			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("jsid", jbxx.getJsid());
			pageObj = tbBizJzgxxjlxxManagerExt.doSearch(map, pageNo, pageSize);
			tbBizJzgjbxx = tbBizJzgjbxxManager.get(jbxx.getJsid());
			zxdwlblist = findZxdwlb();
			conclusion = tbBizJzgxxjlxxManagerExt.queryConclusion();
			tbJzgxxsh = tbJzgxxshManagerExt.query(jbxx.getJsid());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "learnexperience.jsp";
	}

	/**
	 * 添加
	 * @throws Exception 
	 *
	 */
	public void add(HttpServletRequest request) throws Exception {
		TbBizJzgjbxx tjbxx = getSessionTeacher(request);
		if (tjbxx.getJsid() != jsid) {
			throw new Exception("教师只能操作自己的基本信息");
		}
		TbBizJzgxxjlxx tbBizJzgxxjlxx = new TbBizJzgxxjlxx();
		TbBizJzgjbxx jbxx = new TbBizJzgjbxx();
		jbxx.setJsid(tjbxx.getJsid());
		tbBizJzgxxjlxx.setTbBizJzgjbxx(jbxx);
		tbBizJzgxxjlxx.setHdxl(hdxl);
		tbBizJzgxxjlxx.setHdxldgjdq(hdxldgjdq);
		tbBizJzgxxjlxx.setHdxldyxhjg(hdxldyxhjg);
		tbBizJzgxxjlxx.setSxzy(sxzy);
		tbBizJzgxxjlxx.setRxny(rxny);
		tbBizJzgxxjlxx.setByny(byny);
		tbBizJzgxxjlxx.setXwcc(xwcc);
		tbBizJzgxxjlxx.setSfsflzy(sfsflzy);
		tbBizJzgxxjlxx.setXwmc(xwmc);
		tbBizJzgxxjlxx.setHdxwdgjdq(hdxwdgjdq);
		tbBizJzgxxjlxx.setHdxwdyxhjg(hdxwdyxhjg);
		tbBizJzgxxjlxx.setXwsyny(xwsyny);
		tbBizJzgxxjlxx.setXxfs(xxfs);
		tbBizJzgxxjlxx.setZxdwlb(zxdwlb);
		tbBizJzgxxjlxxManagerExt.add(tbBizJzgxxjlxx, ssxd);
	}

	/**
	 * 
	 * 删除
	 *
	 * @param request
	 */
	public Object delete(HttpServletRequest request, HttpServletResponse response) {

		try {
			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			if (jbxx.getJsid() != jsid) {
				throw new Exception("教师只能操作自己的基本信息");
			}
			Map<String, Object> params = new HashMap<String, Object>();
			tbBizJzgxxjlxxManagerExt.delete(ck);

		} catch (Exception e) {
			logger.error("TeacherLearnAction delete() is error!", e);
		}
		return doSearch(request);
	}

	/**
	 *编辑
	 *
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 * @throws Exception 
	 */
	public void edit(HttpServletRequest request, HttpServletResponse response) throws Exception {
		TbBizJzgjbxx tjbxx = getSessionTeacher(request);
		if (tjbxx.getJsid() != jsid) {
			throw new Exception("教师只能操作自己的基本信息");
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("xlId", xlId);
		params.put("hdxl", hdxl);
		params.put("hdxldgjdq", hdxldgjdq);
		params.put("hdxldyxhjg", hdxldyxhjg);
		params.put("sxzy", sxzy);
		params.put("rxny", rxny);
		params.put("byny", byny);
		params.put("xwcc", xwcc);
		params.put("xwmc", xwmc);
		params.put("sfsflzy", sfsflzy);
		params.put("hdxwdgjdq", hdxwdgjdq);
		params.put("hdxwdyxhjg", hdxwdyxhjg);
		params.put("xwsyny", xwsyny);
		params.put("xxfs", xxfs);
		params.put("zxdwlb", zxdwlb);
		tbBizJzgxxjlxxManagerExt.edit(params, ssxd);
	}

	/**
	 * 查找在学单位类别
	 *
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	public String findZxdwlb() {
		List<ZxdwlbZdxbDto> list = tbCfgZdxbManagerExt.findZxdwlb();
		ZxdwlbZdxbDto f = null;
		List<ZxdwlbZdxbDto> f1 = new ArrayList<ZxdwlbZdxbDto>();
		List<ZxdwlbZdxbDto> f2 = new ArrayList<ZxdwlbZdxbDto>();
		for (int i = 0; i < list.size(); i++) {
			f = list.get(i);
			if (1 == f.getZdxbm().length()) {//1
				f1.add(f);
				list.remove(i);
				i--;
			}
			if (2 == f.getZdxbm().length()) {//2
				f2.add(f);
				list.remove(i);
				i--;
			}

		}
		//拼接json
		StringBuilder sb = new StringBuilder("[");
		ZxdwlbZdxbDto sf1 = null;
		ZxdwlbZdxbDto sf2 = null;
		for (int i = 0; i < f1.size(); i++) {
			sf1 = f1.get(i);
			sb.append("{");
			sb.append("'id':" + "'" + sf1.getZdxbm() + "'");
			sb.append(",'text':" + "'" + sf1.getZdxmc() + "'");
			if (f2 != null && f2.size() > 0) {
				sb.append(",'children':[");
				for (int j = 0; j < f2.size(); j++) {
					sf2 = f2.get(j);
					if (sf1.getZdxbm().equals(sf2.getFzdx())) {
						sb.append("{");
						sb.append("'id':" + "'" + sf2.getZdxbm() + "'");
						sb.append(",'text':" + "'" + sf2.getZdxmc() + "'");
						sb.append("},");
					}
				}
				if (regexCheckStr(sb.toString())) {
					sb.replace(sb.length() - 1, sb.length(), "");
				}

				sb.append("]");
			}

			sb.append("},");
		}
		if (regexCheckStr(sb.toString())) {
			sb.replace(sb.length() - 1, sb.length(), "");
		}
		sb.append("]");

		return sb.toString();

	}

	/**
	 * 处理字符串
	 */
	private boolean regexCheckStr(String data) {
		if (data.endsWith(",")) {
			return true;
		}
		return false;
	}

	/**
	 *去编辑页面
	 *
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	public void toEdit(HttpServletRequest request, HttpServletResponse response) {
		try {
			TbBizJzgjbxx tjbxx = getSessionTeacher(request);
			if (tjbxx.getJsid() != jsid) {
				throw new Exception("教师只能操作自己的基本信息");
			}
			JSONObject json = new JSONObject();
			TbBizJzgxxjlxxDto tbBizJzgxxjlxx = tbBizJzgxxjlxxManagerExt.get(xlId);
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("tbBizJzgxxjlxx", tbBizJzgxxjlxx);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("TeacherLearnAction toEdit() is error!", e);
		}
	}

	public void doQuery(HttpServletRequest request, HttpServletResponse reponse) {
		try {
			reponse.setContentType("text/xml;charset=UTF-8");
			JSONObject json = new JSONObject();
			conclusion1 = tbBizJzgxxjlxxManagerExt.queryConclusion(zdxbm);
			PrintWriter out = reponse.getWriter();
			json.put("dto", conclusion1);
			out.write(json.toString());
			out.flush();
			out.close();

		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}

		return;
	}

}
