/**
 * ContactWayAction.java
 * com.becom.jspt2016.webapp.action.basicInformation
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webapp.action.basicInformation;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbBizJzglxfsxx;
import com.becom.jspt2016.model.TbJzgxxsh;
import com.becom.jspt2016.service.ITbBizJzglxfsxxManager;
import com.becom.jspt2016.service.ext.IContactWayManagerExt;
import com.becom.jspt2016.service.ext.ITbJzgxxshManagerExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 * 联系方式
 * <p>
 *
 * @author   wxl
 * @Date	 2016年10月21日 	 
 */
public class ContactWayAction extends PageAction {

	/**
	 *  国内培训Id
	 */
	public long lxfsId;
	/**
	 *  教职工Id
	 */
	public long jsid;
	/**
	 *  培训年度
	 */
	public String sj;
	/**
	 *  培训类别
	 */
	public String email;
	/**
	 *  创建时间
	 */
	public Date cjsj;
	/**
	 *  是否删除
	 */
	public String sfsc;
	/**
	 *  报送人类别
	 */
	public String bsrlb;
	/**
	 *  审核状态
	 */
	public String shzt;
	/**
	 *  审核时间
	 */
	public Date shsj;
	/**
	 *  审核结果
	 */
	public String shjg;

	public SysUser sUser;//用户实体类

	public TbJzgxxsh tbJzgxxsh;

	public TbBizJzglxfsxx contactWay;
	public TbBizJzglxfsxx contactWayInfo = new TbBizJzglxfsxx();

	@Spring
	public ITbBizJzglxfsxxManager tbBizJzglxfsxxManager;
	@Spring
	public IContactWayManagerExt contactWayManagerExt;

	@Spring
	private ITbJzgxxshManagerExt tbJzgxxshManagerExt;

	protected Logger logger = Logger.getLogger(this.getClass());

	/**
	 *   目前--jsid 从service -contactWayManagerExt 中赋值
	 * 		之后jsid 从session里获取
	 * @param request
	 * @return 返回为教师id为jsid 的联系方式信息
	 * 			不为空---可以编辑然后保存---按钮显示“编辑”
	 * 			为空-----可以编辑然后保存---按钮显示“保存”
	 */
	@DefaultAction
	public Object doSearch(HttpServletRequest request) {
		try {

			//获取用户信息
			String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
			//机构用户
			if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
				sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
			} else {//教师用户
				TbBizJzgjbxx jbxx = (TbBizJzgjbxx) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
				jsid = jbxx.getJsid();
				contactWay = contactWayManagerExt.getContactWay(jsid);
				tbJzgxxsh = tbJzgxxshManagerExt.query(jsid);
			}

			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			jsid = jbxx.getJsid();
			contactWay = contactWayManagerExt.getContactWay(jsid);
		} catch (Exception e) {
			logger.error("ContactWayAction doSearch is error!", e);
		}
		return "listcontactway.jsp";
	}

	/**
	 * 增加
	 * <p>
	 * 添加联系方式信息
	 */
	public Object addContactWay(HttpServletRequest request) {
		//long jsid = 1;
		try {
			//增加之前先查一下，如果为空，表示可以添加，走添加的方法
			TbBizJzglxfsxx lxfsxx = new TbBizJzglxfsxx();
			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			lxfsxx.setTbBizJzgjbxx(jbxx);
			lxfsxx.setSj(sj.trim());
			lxfsxx.setEmail(email.trim());
			lxfsxx.setCjsj(cjsj);
			lxfsxx.setSfsc(sfsc);
			lxfsxx.setBsrlb(bsrlb);
			lxfsxx.setShzt(shzt);
			lxfsxx.setShsj(shsj);
			lxfsxx.setShjg(shjg);
			contactWayManagerExt.insertContactWay(lxfsxx);
		} catch (Exception e) {
			logger.error("ContactWayAction addContactWay is error!", e);
		}
		return doSearch(request);
	}

	/**
	 * 
	 *  修改
	 * <p>	
	 * @param request
	 * @param response
	 * @return 在增加的时候判断，有数据走更新方法
	 */
	public Object updateContactWay(HttpServletRequest request, HttpServletResponse response) {

		contactWayInfo.setLxfsId(lxfsId);
		contactWayInfo.setSj(sj.trim());
		contactWayInfo.setEmail(email.trim());

		contactWayManagerExt.updateContactWay(contactWayInfo);

		return doSearch(request);

	}
}
