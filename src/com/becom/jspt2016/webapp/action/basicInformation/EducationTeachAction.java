/**
 * EducationTeachAction.java
 * com.becom.jspt2016.webapp.action.basicInformation
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webapp.action.basicInformation;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.dto.TbBizJzgjyjxxxInfoDto;
import com.becom.jspt2016.dto.TbCfgZdxbInfoDto;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbBizJzgjyjxxx;
import com.becom.jspt2016.model.TbJzgxxsh;
import com.becom.jspt2016.service.ext.ITbBizJzgjyjxxxManagerExt;
import com.becom.jspt2016.service.ext.ITbJzgxxshManagerExt;
import com.becom.jspt2016.service.impl.TbBizJzgjbxxManager;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 * 教育教学信息
 *
 * @author   shanjigang
 * @Date	 2016年10月19日 	 
 */
public class EducationTeachAction extends PageAction {
	public int jyid;
	public int jsid;
	public String xn;
	public String xq;
	public String rkzk;
	public String sfwbkssk;
	public String dslb;
	public String xzycsxkly;
	public String xnbzkkcjxkss;
	public String cjsj;
	public String sfsc;
	public String[] ck;
	@Spring
	private ITbBizJzgjyjxxxManagerExt tbBizJzgjyjxxxManagerExt;
	protected Logger logger = Logger.getLogger(this.getClass());
	public TbBizJzgjyjxxxInfoDto dto;
	public List<TbCfgZdxbInfoDto> conclusion;
	public String dto1;
	public String dto2;
	public TbBizJzgjbxxManager tbBizJzgjbxxManager;
	public String ssxd;
	public String rkxklb;
	public String jrgz;
	public String rjkc;
	public String rjxd;
	public String rkkclb;
	public String pjzks;
	public String rkxkdto;
	/**
	 * 任教课程中小学
	 */
	public String rjkczxxdto;
	/**
	 * 任教课程特殊
	 */
	public String rjkctjdto;

	public TbJzgxxsh tbJzgxxsh;

	@Spring
	private ITbJzgxxshManagerExt tbJzgxxshManagerExt;

	/**
	 * 
	 * 查询列表信息
	 *
	 * @param request
	 * @return list
	 */
	@DefaultAction
	public Object doSearch(HttpServletRequest request) {
		try {
			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			long orgId = jbxx.getTbXxjgb().getXxjgid();
			Map<String, Object> params1 = new HashMap<String, Object>();
			params1.put("orgId", orgId);
			ssxd = tbBizJzgjyjxxxManagerExt.getXd(params1);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsid", jbxx.getJsid());
			pageObj = tbBizJzgjyjxxxManagerExt.doQuery(params, pageNo, pageSize);
			List<TbCfgZdxbInfoDto> funcs = tbBizJzgjyjxxxManagerExt.searchData();
			//学科目录高校
			dto1 = seachData(funcs);
			List<TbCfgZdxbInfoDto> list = tbBizJzgjyjxxxManagerExt.searchDatagz();
			//学科目录高职
			dto2 = seachData(list);
			List<TbCfgZdxbInfoDto> rjxklist = tbBizJzgjyjxxxManagerExt.searchDataxkly();
			//任课学科类别
			rkxkdto = seachData(rjxklist);
			List<TbCfgZdxbInfoDto> rjkczxxlist = tbBizJzgjyjxxxManagerExt.searchDatarjkczxx();
			//任教课程中小学
			rjkczxxdto = seachData(rjkczxxlist);
			List<TbCfgZdxbInfoDto> rjkctslist = tbBizJzgjyjxxxManagerExt.searchDatarjkcts();
			//任教课程特殊学校
			rjkctjdto = seachData(rjkctslist);
			conclusion = tbBizJzgjyjxxxManagerExt.queryConclusion();
			tbJzgxxsh = tbJzgxxshManagerExt.query(jbxx.getJsid());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "educationTeachList.jsp";
	}

	/**
	 * 
	 * 添加教育教学信息
	 *
	 * @param request
	 * @param response
	 * @return 教育教学对象
	 */
	public Object addEducationTeach(HttpServletRequest request, HttpServletResponse response) {
		try {

			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			jsid = Integer.parseInt(String.valueOf(jbxx.getJsid()));
			xzycsxkly = new String(request.getParameter("xzycsxkly").getBytes("iso-8859-1"), "utf-8");
			rjkc = new String(request.getParameter("rjkc").getBytes("iso-8859-1"), "utf-8");
			rkxklb = new String(request.getParameter("rkxklb").getBytes("iso-8859-1"), "utf-8");
			rkkclb = new String(request.getParameter("rkkclb").getBytes("iso-8859-1"), "utf-8");
			response.setCharacterEncoding("UTF-8");
			Date currentTime = new Date();
			TbBizJzgjyjxxx jyxx = new TbBizJzgjyjxxx();
			jyxx.setShsj(currentTime);
			jyxx.setXn(xn);
			jyxx.setXq(xq);
			jyxx.setRkzk(rkzk);
			jyxx.setSfwbkssk(sfwbkssk);
			jyxx.setDslb(dslb);
			jyxx.setShzt("01");
			jyxx.setXzycsxkly(xzycsxkly);
			jyxx.setXnbzkkcjxkss(xnbzkkcjxkss);
			jyxx.setCjsj(currentTime);
			jyxx.setSfsc("0");
			jyxx.setRkxklb(rkxklb);
			jyxx.setJrgz(jrgz);
			jyxx.setRjkc(rjkc);
			jyxx.setRjxd(rjxd);
			jyxx.setRkkclb(rkkclb);
			jyxx.setPjzks(pjzks);
			jyxx.setCjr(String.valueOf(jsid));
			tbBizJzgjyjxxxManagerExt.addTeacherMorality(jyxx, String.valueOf(jsid));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return doSearch(request);
	}

	/**
	 * 
	 * 编辑教育教学信息
	 *
	 * @param request
	 * @param response
	 * @return 教育教学对象
	 */
	public Object editEducationTeach(HttpServletRequest request, HttpServletResponse response) {
		try {
			xq = new String(request.getParameter("xq").getBytes("iso-8859-1"), "utf-8");
			dslb = new String(request.getParameter("dslb").getBytes("iso-8859-1"), "utf-8");
			xzycsxkly = new String(request.getParameter("xzycsxkly").getBytes("iso-8859-1"), "utf-8");
			sfwbkssk = new String(request.getParameter("sfwbkssk").getBytes("iso-8859-1"), "utf-8");
			rkzk = new String(request.getParameter("rkzk").getBytes("iso-8859-1"), "utf-8");
			xnbzkkcjxkss = new String(request.getParameter("xnbzkkcjxkss").getBytes("iso-8859-1"), "utf-8");
			rkxklb = new String(request.getParameter("rkxklb").getBytes("iso-8859-1"), "utf-8");
			jrgz = new String(request.getParameter("jrgz").getBytes("iso-8859-1"), "utf-8");
			rjkc = new String(request.getParameter("rjkc").getBytes("iso-8859-1"), "utf-8");
			rjxd = new String(request.getParameter("rjxd").getBytes("iso-8859-1"), "utf-8");
			rkkclb = new String(request.getParameter("rkkclb").getBytes("iso-8859-1"), "utf-8");
			pjzks = new String(request.getParameter("pjzks").getBytes("iso-8859-1"), "utf-8");
			response.setCharacterEncoding("UTF-8");
			tbBizJzgjyjxxxManagerExt.editEducationTeach(xn, xq, dslb, xzycsxkly, sfwbkssk, rkzk, xnbzkkcjxkss, cjsj,
					jyid, rkxklb, jrgz, rjkc, rjxd, rkkclb, pjzks);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return doSearch(request);
	}

	/**
	 * 
	 * 删除教育教学信息
	 *
	 * @param request
	 * @param response
	 * @return 
	 */
	public Object delEducationTeach(HttpServletRequest request, HttpServletResponse response) {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			tbBizJzgjyjxxxManagerExt.delEducationTeach(ck);

		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return doSearch(request);
	}

	/**
	 * 
	 * 查询教育教学信息
	 *
	 * @param request
	 * @param reponse 
	 */
	public void doQuery(HttpServletRequest request, HttpServletResponse reponse) {
		try {
			reponse.setContentType("text/xml;charset=UTF-8");
			JSONObject json = new JSONObject();
			dto = tbBizJzgjyjxxxManagerExt.editEduction(jyid);
			conclusion = tbBizJzgjyjxxxManagerExt.queryConclusion();
			PrintWriter out = reponse.getWriter();
			json.put("dto", dto);
			json.put("conclusion", conclusion);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return;
	}

	/**
	 * 
	 * 查询数据
	 *
	 * @param funcs
	 * @return 字符串
	 */
	public String seachData(List<TbCfgZdxbInfoDto> funcs) {
		TbCfgZdxbInfoDto f = null;
		List<TbCfgZdxbInfoDto> f1 = new ArrayList<TbCfgZdxbInfoDto>();
		List<TbCfgZdxbInfoDto> f2 = new ArrayList<TbCfgZdxbInfoDto>();
		List<TbCfgZdxbInfoDto> f3 = new ArrayList<TbCfgZdxbInfoDto>();
		//查出一级节点
		for (int i = 0; i < funcs.size(); i++) {
			f = funcs.get(i);
			if ((f.getFzdx() == null)) {
				f1.add(f);//一级节点
			}
		}
		//查出二级节点
		for (int i = 0; i < f1.size(); i++) {
			for (int j = 0; j < funcs.size(); j++) {
				if (f1.get(i).getZdxbm().equals(funcs.get(j).getFzdx())) {
					f2.add(funcs.get(j));
				}

			}
		}

		//拼接json
		StringBuilder sb = new StringBuilder("[");
		TbCfgZdxbInfoDto sf1 = null;
		TbCfgZdxbInfoDto sf2 = null;
		TbCfgZdxbInfoDto sf3 = null;
		for (int i = 0; i < f1.size(); i++) {
			sf1 = f1.get(i);
			sb.append("{");//1
			sb.append("'id':" + sf1.getZdxbm());
			sb.append(",'text':" + "'" + sf1.getZdxmc() + "'");
			sb.append(",'children':[");//2
			for (int j = 0; j < f2.size(); j++) {
				sf2 = f2.get(j);
				if (sf1.getZdxbm().equals(sf2.getFzdx())) {
					sb.append("{");
					sb.append("'id':" + sf2.getZdxbm());
					sb.append(",'text':" + "'" + sf2.getZdxmc() + "'");

					sb.append("},");//
				}
			}
			if (regexCheckStr(sb.toString())) {
				sb.replace(sb.length() - 1, sb.length(), "");
			}
			sb.append("]");//2

			sb.append("},");//1end
		}

		if (regexCheckStr(sb.toString())) {
			sb.replace(sb.length() - 1, sb.length(), "");
		}
		sb.append("]");//最外一层
		return sb.toString();
	}

	private boolean regexCheckStr(String data) {
		if (data.endsWith(",")) {
			return true;
		}
		return false;
	}
}
