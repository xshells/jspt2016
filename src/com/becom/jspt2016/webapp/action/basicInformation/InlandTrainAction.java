/**
 * InlandTrainAction.java
 * com.becom.jspt2016.webapp.action.mediumsmallaction
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webapp.action.basicInformation;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.dto.GnpxxxInfoDto;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzggnpxxx;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbJzgxxsh;
import com.becom.jspt2016.service.ITbBizJzggnpxxxManager;
import com.becom.jspt2016.service.ITbBizJzgjbxxManager;
import com.becom.jspt2016.service.ext.IInlandTrainManagerExt;
import com.becom.jspt2016.service.ext.ITbJzgxxshManagerExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 * 学校模块
 * <p>
 * 国内信息管理
 *
 * @author   wangxiaolei
 * @Date	 2016年10月17日 	 
 */
public class InlandTrainAction extends PageAction {
	/**
	 * 国内培训Id组成的字符窜
	 */
	public String[] ck;

	/**
	 *  国内培训Id
	 */
	public long gnpxId;

	/**
	 *  培训年度
	 */
	public String pxnd;

	/**
	 *  培训类别
	 */
	public String pxlb;

	/**
	 *  培训项目名称
	 */
	public String pxxmmc;

	/**
	 *  培训机构名称
	 */
	public String pxjgmc;

	/**
	 *  培训方式
	 */
	public String pxfs;

	/**
	 *  培训获得学时
	 */
	public String pxhdxs;

	/**
	 *  培训获得学分
	 */
	public String pxhdxf;

	/**
	 *  创建时间
	 */
	public Date cjsj;

	/**
	 *  是否删除
	 */
	public String sfsc;

	/**
	 *  教职工id
	 */
	public long jsid;

	/**
	 *  报送人类别
	 */
	public String bsrlb;

	/**
	 *  审核状态
	 */
	public String shzt;
	/**
	 *  审核时间
	 */
	public Date shsj;

	/**
	 *  审核结果
	 */
	public String shjg;

	public SysUser sUser;//用户实体类
	public TbBizJzgjbxx teacher;//教师实体类
	public String ssxd; //所属学段

	public String userType;

	public TbJzgxxsh tbJzgxxsh;

	public TbBizJzggnpxxx tbBizJzggnpxxx;

	public TbBizJzggnpxxx tbBizJzggnpxInfo = new TbBizJzggnpxxx();

	protected Logger logger = Logger.getLogger(this.getClass());

	@Spring
	public ITbBizJzggnpxxxManager tbBizJzggnpxxxManager;

	@Spring
	public IInlandTrainManagerExt inlandTrainManagerExt;

	@Spring
	public ITbBizJzgjbxxManager tbBizJzgjbxxManager;

	@Spring
	private ITbJzgxxshManagerExt tbJzgxxshManagerExt;

	@DefaultAction
	/**
	 * 
	 * 搜索
	 * <p>
	 * 分页查询列出国内培训信息
	 * session里拿jsid，通过jsid 查找 进入列表      
	 */
	public Object doSearch(HttpServletRequest request) {

		try {
			//获取用户信息
			String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
			//机构用户
			if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
				sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
				userType = "1";
			} else {
				//教师用户
				userType = "2";
			}
			TbBizJzgjbxx jbxx = getSessionTeacher(request);

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("orgId", jbxx.getTbXxjgb().getXxjgid());
			params.put("jsid", jbxx.getJsid());
			pageObj = inlandTrainManagerExt.pageSearch(params, pageNo, pageSize);
			ssxd = inlandTrainManagerExt.getXd(params);//查询角色
			tbJzgxxsh = tbJzgxxshManagerExt.query(jbxx.getJsid());

		} catch (Exception e) {
			logger.error("InlandTrainAction doSearch is error!", e);
		}
		return "listinlandtrain.jsp";
	}

	/**
	 * 
	 * 增加
	 * <p>
	 * 添加国内培训信息
	 */
	public Object addInlandTrain(HttpServletRequest request) {

		try {
			TbBizJzggnpxxx gnpxxx = new TbBizJzggnpxxx();
			teacher = getSessionTeacher(request);
			gnpxxx.setTbBizJzgjbxx(teacher);
			gnpxxx.setPxnd(pxnd);
			gnpxxx.setPxlb(pxlb);
			gnpxxx.setPxxmmc(pxxmmc);
			gnpxxx.setPxjgmc(pxjgmc);
			gnpxxx.setPxfs(pxfs);
			gnpxxx.setPxhdxs(pxhdxs);
			gnpxxx.setPxhdxf(pxhdxf);
			gnpxxx.setCjsj(cjsj);
			gnpxxx.setSfsc(sfsc);
			gnpxxx.setBsrlb(bsrlb);
			gnpxxx.setShzt(shzt);
			gnpxxx.setShsj(shsj);
			gnpxxx.setShjg(shjg);
			inlandTrainManagerExt.insertInlandTrain(gnpxxx);
		} catch (Exception e) {
			logger.error("InlandTrainAction addInlandTrain is error!", e);
		}
		return doSearch(request);
	}

	/**
	 * 
	 * 搜索
	 * <p>
	 * 根据id获取国内培训对象信息
	 */
	public void toUpdateInlandTrain(HttpServletRequest request, HttpServletResponse response) {

		try {
			JSONObject jo = new JSONObject();
			GnpxxxInfoDto gnpxxx = inlandTrainManagerExt.getInlandTrain(gnpxId);
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			jo.put("gnpxxx", gnpxxx);
			out.write(jo.toString());
			out.flush();
			out.close();
		} catch (IOException e) {
			logger.error("InlandTrainAction toUpdateInlandTrain is error!", e);

		}
	}

	/**
	 * 
	 * 更新
	 * <p>
	 * 更新国内培训对象信息
	 */
	public Object updateInlandTrain(HttpServletRequest request, HttpServletResponse response) {

		int result = 0;
		try {
			//修改乱码
			pxxmmc = new String(request.getParameter("pxxmmc").getBytes("iso-8859-1"), "utf-8");
			pxjgmc = new String(request.getParameter("pxjgmc").getBytes("iso-8859-1"), "utf-8");
			tbBizJzggnpxInfo.setGnpxId(gnpxId);
			tbBizJzggnpxInfo.setPxnd(pxnd);
			tbBizJzggnpxInfo.setPxlb(pxlb);
			tbBizJzggnpxInfo.setPxxmmc(pxxmmc);
			tbBizJzggnpxInfo.setPxjgmc(pxjgmc);
			tbBizJzggnpxInfo.setPxfs(pxfs);
			tbBizJzggnpxInfo.setPxhdxs(pxhdxs);
			tbBizJzggnpxInfo.setPxhdxf(pxhdxf);
			result = inlandTrainManagerExt.updateInlandTrain(tbBizJzggnpxInfo);
		} catch (Exception e) {
			logger.error("InlandTrainAction updateInlandTrain is error!", e);
		}
		return doSearch(request);
	}

	/**
	 * 
	 * 删除
	 * <p>
	 * 删除国内培训信息
	 * 0-显示
	 * 1-已删除
	 */
	public Object delete(HttpServletRequest request, HttpServletResponse response) {

		int result = 0;
		try {
			result = inlandTrainManagerExt.delete(ck);
			if (result == 1) {
				message = "删除成功";
			}
		} catch (Exception e) {
			if (result == 0) {
				message = "删除失败";
			}
			logger.error("InlandTrainAction delete is error!", e);
		}
		return doSearch(request);
	}
}
