/**
 * ProtTechAppoinAction.java
 * com.becom.jspt2016.webapp.action.basicInformation
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.webapp.action.basicInformation;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.dto.TbCfgZdxbInfoDto;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbBizJzgzyjszwprxx;
import com.becom.jspt2016.model.TbJzgxxsh;
import com.becom.jspt2016.service.ITbBizJzgzyjszwprxxManager;
import com.becom.jspt2016.service.ext.IProtTechAppoinManagerExt;
import com.becom.jspt2016.service.ext.ITbJzgxxshManagerExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 * 教职工专业技术职务聘任
 * <p>
 * @author   董锡福
 * @Date	 2016年10月20日 	 
 */
public class ProtTechAppoinAction extends PageAction {

	/**
	 * id数组
	 */
	public String[] ck;

	public String PRZYJSZW;//聘任专业技术职务

	public String PRKSNY;//聘任开始年月

	public String PRJSNY;//聘任结束年月

	public String PRDWMC;//聘任单位名称

	public long zyjszwId;//专业技术职务Id

	public String dto1;//树
	public SysUser sUser;//用户实体类

	public TbBizJzgjbxx teacher;//教师实体类
	public String ssxd; //所属学段
	public long jsid; //教师Id

	public TbJzgxxsh tbJzgxxsh;

	@Spring
	public ITbBizJzgzyjszwprxxManager jzgzyjszwprxxmanager;

	public TbBizJzgzyjszwprxx tbbizjzgzyjszwprxx;

	@Spring
	public IProtTechAppoinManagerExt prottechappoinmanagerExt;
	@Spring
	private ITbJzgxxshManagerExt tbJzgxxshManagerExt;

	@DefaultAction
	public Object doSearch(HttpServletRequest request) {
		try {
			teacher = getSessionTeacher(request);
			Map<String, Object> param = new HashMap<String, Object>();
			jsid = teacher.getJsid();
			param.put("jsid", jsid);
			pageObj = prottechappoinmanagerExt.doSearch(param, pageNo, pageSize);
			List<TbCfgZdxbInfoDto> funcs = prottechappoinmanagerExt.searchData();
			dto1 = seachData(funcs);
			tbJzgxxsh = tbJzgxxshManagerExt.query(jsid);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "protTechAppoinList.jsp";

	}

	//添加提交
	public Object SubAddtech(HttpServletRequest request, HttpServletResponse response) {
		try {
			TbBizJzgzyjszwprxx tech = new TbBizJzgzyjszwprxx();
			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			tech.setTbBizJzgjbxx(jbxx);
			tech.setPrzyjszw(PRZYJSZW);//聘任专业技术职务
			tech.setPrdwmc(PRDWMC);//聘任单位名称
			tech.setPrksny(PRKSNY);//聘任开始年月
			tech.setPrjsny(PRJSNY);//聘任结束年月
			tech.setShsj(new Date());
			tech.setCjsj(new Date());

			prottechappoinmanagerExt.SubAddpoint(tech);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return doSearch(request);
	}

	//修改前回显
	public void editPostAppoint(HttpServletRequest request, HttpServletResponse response) {
		try {
			response.setContentType("text/xml;charset=UTF-8");
			JSONObject json = new JSONObject();
			tbbizjzgzyjszwprxx = prottechappoinmanagerExt.editProtech(zyjszwId);
			PrintWriter out = response.getWriter();
			json.put("dto", tbbizjzgzyjszwprxx);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	//删除
	public Object delPostAppoint(HttpServletRequest request, HttpServletResponse response) {
		try {
			prottechappoinmanagerExt.removeProTech(ck);
		} catch (Exception e) {

			e.printStackTrace();
		}
		return doSearch(request);
	}

	//执行修改
	public Object toUpAppoint(HttpServletRequest request, HttpServletResponse response) {
		int result = 0;
		try {
			TbBizJzgzyjszwprxx zyjs = new TbBizJzgzyjszwprxx();
			zyjs.setPrdwmc(PRDWMC);
			zyjs.setPrzyjszw(PRZYJSZW);
			zyjs.setPrksny(PRKSNY);//聘任开始年月
			zyjs.setPrjsny(PRJSNY);//聘任结束年月
			zyjs.setZyjszwId(zyjszwId);
			result = prottechappoinmanagerExt.updateProtech(zyjs);
			if (result == 0) {
				message = "修改成功";
			}
		} catch (Exception e) {
			if (result == 1) {
				message = "修改失败";
			}
			e.printStackTrace();
		}
		return doSearch(request);
	}

	public String seachData(List<TbCfgZdxbInfoDto> funcs) {
		TbCfgZdxbInfoDto f = null;
		List<TbCfgZdxbInfoDto> f1 = new ArrayList<TbCfgZdxbInfoDto>();
		List<TbCfgZdxbInfoDto> f2 = new ArrayList<TbCfgZdxbInfoDto>();
		List<TbCfgZdxbInfoDto> f3 = new ArrayList<TbCfgZdxbInfoDto>();
		//查出一级节点
		for (int i = 0; i < funcs.size(); i++) {
			f = funcs.get(i);
			if ((f.getFzdx() == null)) {
				f1.add(f);//一级节点
				/*funcs.remove(i);
				i--;*/
			}
		}
		//查出二级节点
		for (int i = 0; i < f1.size(); i++) {
			for (int j = 0; j < funcs.size(); j++) {
				if (f1.get(i).getZdxbm().equals(funcs.get(j).getFzdx())) {
					f2.add(funcs.get(j));
					/*f2.remove(j);
					j--;*/
				}

			}
		}
		//查询三级节点
		for (int i = 0; i < f2.size(); i++) {
			for (int j = 0; j < funcs.size(); j++) {
				if (f2.get(i).getZdxbm().equals(funcs.get(j).getFzdx())) {
					f3.add(funcs.get(j));
					/*f2.remove(j);
					j--;*/
				}

			}
		}

		//拼接json
		StringBuilder sb = new StringBuilder("[");
		TbCfgZdxbInfoDto sf1 = null;
		TbCfgZdxbInfoDto sf2 = null;
		TbCfgZdxbInfoDto sf3 = null;
		for (int i = 0; i < f1.size(); i++) {
			sf1 = f1.get(i);
			sb.append("{");//1
			sb.append("'id':" + sf1.getZdxbm());
			sb.append(",'text':" + "'" + sf1.getZdxmc() + "'");
			sb.append(",'children':[");//2
			for (int j = 0; j < f2.size(); j++) {
				sf2 = f2.get(j);
				if (sf1.getZdxbm().equals(sf2.getFzdx())) {
					sb.append("{");
					sb.append("'id':" + sf2.getZdxbm());
					sb.append(",'text':" + "'" + sf2.getZdxmc() + "'");
					//					sb.append(",'url':" + "'" + sf2.getUrl() + "'");

					if (f3 != null && f3.size() > 0) {
						sb.append(",'children':[");
						for (int j2 = 0; j2 < f3.size(); j2++) {
							sf3 = f3.get(j2);
							if (sf2.getZdxbm().equals(sf3.getFzdx())) {
								sb.append("{");
								sb.append("'id':" + sf3.getZdxbm());
								sb.append(",'text':" + "'" + sf3.getZdxmc() + "'");
								//								sb.append(",'url':" + "'" + sf3.getUrl() + "'");
								sb.append("},");
							}
						}
						if (regexCheckStr(sb.toString())) {
							sb.replace(sb.length() - 1, sb.length(), "");
						}
						sb.append("]");

					}

					sb.append("},");//
				}
			}
			if (regexCheckStr(sb.toString())) {
				sb.replace(sb.length() - 1, sb.length(), "");
			}
			sb.append("]");//2

			sb.append("},");//1end
		}

		if (regexCheckStr(sb.toString())) {
			sb.replace(sb.length() - 1, sb.length(), "");
		}
		sb.append("]");//最外一层
		return sb.toString();
	}

	private boolean regexCheckStr(String data) {
		if (data.endsWith(",")) {
			return true;
		}
		return false;
	}

}
