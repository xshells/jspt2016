/**
 * BaseInfoAction.java
 * com.becom.jspt2016.webapp.action.basicInformation
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webapp.action.basicInformation;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.common.ConstantBean;
import com.becom.jspt2016.dto.CheckShDto;
import com.becom.jspt2016.dto.JzglyZdxbDto;
import com.becom.jspt2016.dto.TbCfgXzqhdmDto;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbCfgXzqhdm;
import com.becom.jspt2016.model.TbJzgxxsh;
import com.becom.jspt2016.model.TbXxjgb;
import com.becom.jspt2016.service.ITbBizJzgjbxxManager;
import com.becom.jspt2016.service.ITbCfgXzqhdmManager;
import com.becom.jspt2016.service.ITbXxjgbManager;
import com.becom.jspt2016.service.ext.ITbBizJzgjbxxManagerExt;
import com.becom.jspt2016.service.ext.ITbCfgXzqhdmManagerExt;
import com.becom.jspt2016.service.ext.ITbCfgZdxbManagerExt;
import com.becom.jspt2016.service.ext.ITbJzgxxshManagerExt;
import com.becom.jspt2016.webapp.action.PageAction;
import com.becom.jspt2016.webapp.util.UploadFileUtils;

/**
 * 基础信息
 * <p>
 * 教师基础信息
 *
 * @author   zhangchunming
 * @Date	 2016年10月17日 	 
 */
public class TeacherBaseInfoAction extends PageAction<TbBizJzgjbxx> {

	protected Logger logger = Logger.getLogger(this.getClass());

	public String grzp;

	/**
	 * 文件名称
	 */
	public String fileName;
	/**
	 * 教职工id
	 */
	public long jsid;

	/**
	 * 姓名
	 */
	public String xm;
	/**
	 * 性别
	 */
	public String xb;
	/**
	 * 身份证件类型
	 */
	public String sfzjlx;

	/**
	 * 身份证件号码
	 */
	public String sfzjh;

	/**
	 * 出生日期
	 */
	public String csrq;
	/**
	 * 国籍地区
	 */
	public String gjdq;
	/**
	 * 出生地
	 */
	public String csd;
	/**
	 * 民族
	 */
	public String mz;
	/**
	 * 政治面貌
	 */
	public String[] zzmm;
	/**
	 * 参加工作年月
	 */
	public String cjgzny;
	/**
	 * 进本校年月
	 */
	public String jbxny;
	/**
	 * 教职工来源
	 */
	public String jzgly;
	/**
	 * 教职工类别
	 */
	public String jzglb;
	/**
	 * 是否在编
	 */
	public String sfzb;
	/**
	 * 用人形式
	 */
	public String yrxs;
	/**
	 * 学缘结构
	 */
	public String[] xyjg;
	/**
	 * 签订合同情况
	 */
	public String qdhtqk;
	/**
	 * 是否双师
	 */
	public String sfss;
	/**
	 * 是否具备职业技能等级证书
	 */
	public String sfjbzyjndjzs;
	/**
	 * 企业工作（实践）时长
	 */
	public String qygzsjsc;
	/**
	 * 继续教育编号
	 */
	public String jxjybh;
	/**
	 * 是否全日制师范类专业
	 */
	public String sfsflzyby;
	/**
	 * 是否受过特教专业培养培训
	 */
	public String sfsgtjzypypx;
	/**
	 * 是否有特殊教育从业证书
	 */
	public String sfytsjycyzs;
	/**
	 * 信息技术应用能力
	 */
	public String xxjsyynl;
	/**
	 * 是否属于免费（公费）师范生
	 */
	public String sfsymfsfs;
	/**
	 * 是否参加基层服务项目
	 */
	public String sfstgjs;
	/**
	 * 是否心理健康教育教师
	 */
	public String sfxljkjyjs;
	/**
	 * 从事特教起始年月
	 */
	public String cstjqsny;
	/**
	 * 所属学段
	 */
	public String ssxd;
	/**
	 * 工作情况
	 */
	public String zgqk;
	/**
	 * 是否受过学前教育培养培训
	 */
	public String sfsgxqjyzypypx;
	/**
	 * 是否学前教育专业毕业
	 */
	public String sfxqjyzyby;
	/**
	 * 骨干教师类型
	 */
	public String ggjslx;

	/**
	 * 教师id字符串
	 */
	public String jsids;

	/**
	 * 树节点id
	 */
	public String infoid;

	/**
	 * 返回页面数据
	 */
	public TbBizJzgjbxx tbBizJzgjbxx;

	public TbCfgXzqhdm tbCfgXzqhdm;
	public TbCfgXzqhdm tbCfgXzqhdm2;

	public String imageUrl;

	public String jzglylist;
	public String csdlist;
	public CheckShDto shDto;
	public TbXxjgb tbXxjgb;
	public TbJzgxxsh tbJzgxxsh;

	@Spring
	private ITbBizJzgjbxxManager tbBizJzgjbxxManager;

	@Spring
	private ITbCfgXzqhdmManager tbCfgXzqhdmManager;

	@Spring
	private ITbBizJzgjbxxManagerExt tbBizJzgjbxxManagerExt;

	@Spring
	private ITbCfgXzqhdmManagerExt tbCfgXzqhdmManagerExt;

	@Spring
	private UploadFileUtils uploadFileUtil;

	@Spring
	private ConstantBean constantBean;

	@Spring
	private ITbCfgZdxbManagerExt tbCfgZdxbManagerExt;

	@Spring
	private ITbXxjgbManager tbXxjgbManager;

	@Spring
	private ITbJzgxxshManagerExt tbJzgxxshManagerExt;

	@DefaultAction
	public Object baseInfo(HttpServletRequest request) {
		//TODO 获取教师id
		try {

			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			if (jbxx != null) {
				tbBizJzgjbxx = tbBizJzgjbxxManager.get(jbxx.getJsid());
				imageUrl = constantBean.get("imageUrl");
				String xzqhdm = tbBizJzgjbxx.getTbXxjgb().getQxdm();
				tbCfgXzqhdm = tbCfgXzqhdmManager.getByProperty("xzqhdm", xzqhdm);
				tbCfgXzqhdm2 = tbCfgXzqhdmManager.getByProperty("xzqhdm", tbBizJzgjbxx.getCsd());
				jzglylist = findJzgly(tbBizJzgjbxx.getTbXxjgb().getSsxd());
				csdlist = csdData();

				tbJzgxxsh = tbJzgxxshManagerExt.query(jbxx.getJsid());
				return "baseinfo.jsp";
			} else {
				/**
				 * 机构人员添加教师基本信息跳转的页面
				 */
				SysUser sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
				/**
				 * 获取学校（机构）id
				 */
				String jgid = sUser.getId();
				tbXxjgb = tbXxjgbManager.get(Long.valueOf(jgid));
				System.out.println(tbXxjgb.getSsxd() + "用户学段");
				tbCfgXzqhdm = tbCfgXzqhdmManager.getByProperty("xzqhdm", tbXxjgb.getQxdm());
				jzglylist = findJzgly(tbXxjgb.getSsxd());
				csdlist = csdData();
				return "addbaseinfo.jsp";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取审核状态
	 *
	 */
	public void getShzt(HttpServletRequest request, HttpServletResponse response) {
		try {
			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			JSONObject json = new JSONObject();
			TbJzgxxsh xxsh = tbJzgxxshManagerExt.query(jbxx.getJsid());
			if (xxsh == null) {
				xxsh = new TbJzgxxsh();
			}
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("tbJzgxxsh", xxsh);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("TeacherLearnAction toEdit() is error!", e);
		}

	}

	/**
	 * 保存用户
	 *
	 * @param request
	 * @throws Exception 
	 */
	public void saveUser(HttpServletRequest request) throws Exception {
		/***组织提交参数 start***/
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("grzp", grzp);
		params.put("gjdq", gjdq);
		params.put("csd", csd);
		params.put("xb", xb);
		params.put("mz", mz);
		if (zzmm == null) {
			params.put("zzmm", null);
		} else {
			StringBuffer sb = new StringBuffer();
			for (String zm : zzmm) {
				sb.append(",").append(zm);
			}
			params.put("zzmm", sb.substring(1).toString());
		}
		params.put("cjgzny", cjgzny);
		params.put("jbxny", jbxny);
		params.put("jzgly", jzgly);
		params.put("jzglb", jzglb);
		params.put("sfzb", sfzb);
		params.put("csrq", csrq);
		params.put("qdhtqk", qdhtqk);
		params.put("yrxs", yrxs);
		params.put("sfss", sfss);
		params.put("sfjbzyjndjzs", sfjbzyjndjzs);
		params.put("qygzsjsc", qygzsjsc);
		params.put("jxjybh", jxjybh);
		params.put("sfsflzyby", sfsflzyby);
		params.put("sfsgtjzypypx", sfsgtjzypypx);
		params.put("sfytsjycyzs", sfytsjycyzs);
		params.put("xxjsyynl", xxjsyynl);
		params.put("sfsymfsfs", sfsymfsfs);
		params.put("sfstgjs", sfstgjs);
		params.put("sfxljkjyjs", sfxljkjyjs);
		params.put("cstjqsny", cstjqsny);
		params.put("sfsgxqjyzypypx", sfsgxqjyzypypx);
		params.put("sfxqjyzyby", sfxqjyzyby);
		if (ssxd.equals("10")) {
			StringBuffer sb1 = new StringBuffer();
			for (String xy : xyjg) {
				sb1.append(",").append(xy);
			}
			params.put("xyjg", sb1.substring(1).toString());
		}
		params.put("ggjslx", ggjslx);
		/***组织提交参数 end***/
		TbBizJzgjbxx jbxx = getSessionTeacher(request);
		if (jbxx != null) {
			if (jbxx.getJsid() != jsid) {
				throw new Exception("教师只能操作自己的基本信息");
			}
			params.put("jsid", jbxx.getJsid());
			//params.put("jsid", 1);
			tbBizJzgjbxxManagerExt.saveUser(params, ssxd);
		} else {
			throw new Exception("没有教师信息");
		}

	}

	/**
	 * 添加用户
	 *
	 * @param request
	 * @throws Exception 
	 */
	public void add(HttpServletRequest request) {

		/***组织提交参数 start***/
		/**
		 * 机构人员添加教师基本信息跳转的页面
		 */
		SysUser sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
		/**
		 * 获取学校（机构）id
		 */
		long jgid = Long.valueOf(sUser.getId());
		TbBizJzgjbxx jbxx = new TbBizJzgjbxx();
		jbxx.setXm(xm);
		jbxx.setXb(xb);
		jbxx.setZgqk(zgqk);
		jbxx.setSfzjlx(sfzjlx);
		jbxx.setSfzjh(sfzjh);
		jbxx.setCsrq(csrq);
		jbxx.setGjdq(gjdq);
		jbxx.setCsd(csd);
		jbxx.setMz(mz);

		if (zzmm == null) {
			jbxx.setZzmm(null);
		} else {
			StringBuffer sb = new StringBuffer();
			for (String zm : zzmm) {
				sb.append(",").append(zm);
			}
			jbxx.setZzmm(sb.substring(1).toString());
		}
		jbxx.setCjgzny(cjgzny);
		jbxx.setJbxny(jbxny);
		jbxx.setJzgly(jzgly);
		jbxx.setJzglb(jzglb);
		jbxx.setSfzb(sfzb);
		if (ssxd.equals("10")) {
			StringBuffer sb1 = new StringBuffer();
			for (String xy : xyjg) {
				sb1.append(",").append(xy);
			}
			jbxx.setXyjg(sb1.substring(1).toString());
		}
		jbxx.setQdhtqk(qdhtqk);
		jbxx.setYrxs(yrxs);
		jbxx.setGrzp(grzp);
		jbxx.setSfss(sfss);
		jbxx.setSfjbzyjndjzs(sfjbzyjndjzs);
		jbxx.setQygzsjsc(qygzsjsc);
		jbxx.setJxjybh(jxjybh);
		jbxx.setSfsflzyby(sfsflzyby);
		jbxx.setSfsgtjzypypx(sfsgtjzypypx);
		jbxx.setSfytsjycyzs(sfytsjycyzs);
		jbxx.setXxjsyynl(xxjsyynl);
		jbxx.setSfsymfsfs(sfsymfsfs);
		jbxx.setSfstgjs(sfstgjs);
		jbxx.setSfxljkjyjs(sfxljkjyjs);
		jbxx.setCstjqsny(cstjqsny);
		jbxx.setSfsgxqjyzypypx(sfsgxqjyzypypx);
		jbxx.setSfxqjyzyby(sfxqjyzyby);
		jbxx.setGgjslx(ggjslx);
		/***组织提交参数 end***/

		String eduId = tbBizJzgjbxxManagerExt.add(jbxx, jgid);
		TbBizJzgjbxx jbxxx = tbBizJzgjbxxManager.getByProperty("eduId", eduId);
		/**
		 * 将教师对象放入session
		 */
		request.getSession().setAttribute(Constant.KEY_SESSION_TEACHER, jbxxx);

	}

	/**
	* 删除照片
	*
	* @param request
	 * @throws Exception 
	*/

	public void deletePhoto(HttpServletRequest request) throws Exception {

		TbBizJzgjbxx jbxx = getSessionTeacher(request);
		if (jbxx != null) {
			if (jbxx.getJsid() != jsid) {
				throw new Exception("教师只能操作自己的基本信息");
			}
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsid", jsid);
			params.put("grzp", null);
			tbBizJzgjbxxManagerExt.deletePhoto(params);
		} else {
			throw new Exception("教师信息不存在！");
		}
	}

	/**
	 * 查找出生地
	 *
	 */

	public String csdData() {

		List<TbCfgXzqhdmDto> xzqhdms = tbCfgXzqhdmManagerExt.findAll();
		TbCfgXzqhdmDto f = null;
		List<TbCfgXzqhdmDto> f1 = new ArrayList<TbCfgXzqhdmDto>();
		List<TbCfgXzqhdmDto> f2 = new ArrayList<TbCfgXzqhdmDto>();
		List<TbCfgXzqhdmDto> f3 = new ArrayList<TbCfgXzqhdmDto>();
		//查出一级节点
		for (int i = 0; i < xzqhdms.size(); i++) {
			f = xzqhdms.get(i);
			if ((f.getLevel() == 2)) {
				f1.add(f);//一级节点
				/*funcs.remove(i);
				i--;*/
			}
		}
		//查出二级节点
		for (int i = 0; i < f1.size(); i++) {
			for (int j = 0; j < xzqhdms.size(); j++) {
				if (f1.get(i).getId().equals(xzqhdms.get(j).getPid())) {
					f2.add(xzqhdms.get(j));
					/*f2.remove(j);
					j--;*/
				}

			}
		}
		//查询三级节点
		for (int i = 0; i < f2.size(); i++) {
			for (int j = 0; j < xzqhdms.size(); j++) {
				if (f2.get(i).getId().equals(xzqhdms.get(j).getPid())) {
					f3.add(xzqhdms.get(j));
					/*f2.remove(j);
					j--;*/
				}

			}
		}

		//拼接json
		StringBuilder sb = new StringBuilder("[");
		TbCfgXzqhdmDto sf1 = null;
		TbCfgXzqhdmDto sf2 = null;
		TbCfgXzqhdmDto sf3 = null;
		for (int i = 0; i < f1.size(); i++) {
			sf1 = f1.get(i);
			sb.append("{");//1
			sb.append("'id':" + sf1.getId());
			sb.append(",'text':" + "'" + sf1.getName() + "'");
			sb.append(",'children':[");//2
			for (int j = 0; j < f2.size(); j++) {
				sf2 = f2.get(j);
				if (sf1.getId().equals(sf2.getPid())) {
					sb.append("{");
					sb.append("'id':" + sf2.getId());
					sb.append(",'text':" + "'" + sf2.getName() + "'");
					//					sb.append(",'url':" + "'" + sf2.getUrl() + "'");

					if (f3 != null && f3.size() > 0) {
						sb.append(",'children':[");
						for (int j2 = 0; j2 < f3.size(); j2++) {
							sf3 = f3.get(j2);
							if (sf2.getId().equals(sf3.getPid())) {
								sb.append("{");
								sb.append("'id':" + sf3.getId());
								sb.append(",'text':" + "'" + sf3.getName() + "'");
								//								sb.append(",'url':" + "'" + sf3.getUrl() + "'");
								sb.append("},");
							}
						}
						if (regexCheckStr(sb.toString())) {
							sb.replace(sb.length() - 1, sb.length(), "");
						}
						sb.append("]");

					}

					sb.append("},");//
				}
			}
			if (regexCheckStr(sb.toString())) {
				sb.replace(sb.length() - 1, sb.length(), "");
			}
			sb.append("]");//2

			sb.append("},");//1end
		}

		if (regexCheckStr(sb.toString())) {
			sb.replace(sb.length() - 1, sb.length(), "");
		}
		sb.append("]");//最外一层
		return sb.toString();
	}

	/**
	 * 查找教职工来源
	 *
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	public String findJzgly(String ssxd) {
		List<JzglyZdxbDto> list = tbCfgZdxbManagerExt.findJzgly(ssxd);
		JzglyZdxbDto f = null;
		List<JzglyZdxbDto> f1 = new ArrayList<JzglyZdxbDto>();
		List<JzglyZdxbDto> f2 = new ArrayList<JzglyZdxbDto>();
		for (int i = 0; i < list.size(); i++) {
			f = list.get(i);
			if (2 == f.getZdxbm().length()) {//1
				f1.add(f);
				list.remove(i);
				i--;
			}
			if (3 == f.getZdxbm().length()) {//2
				f2.add(f);
				list.remove(i);
				i--;
			}

		}
		//拼接json
		StringBuilder sb = new StringBuilder("[");
		JzglyZdxbDto sf1 = null;
		JzglyZdxbDto sf2 = null;
		for (int i = 0; i < f1.size(); i++) {
			sf1 = f1.get(i);
			sb.append("{");
			sb.append("'id':" + sf1.getZdxbm());
			sb.append(",'text':" + "'" + sf1.getZdxmc() + "'");
			if (f2 != null && f2.size() > 0) {
				sb.append(",'children':[");
				for (int j = 0; j < f2.size(); j++) {
					sf2 = f2.get(j);
					if (sf1.getZdxbm().equals(sf2.getFzdx())) {
						sb.append("{");
						sb.append("'id':" + sf2.getZdxbm());
						sb.append(",'text':" + "'" + sf2.getZdxmc() + "'");
						sb.append("},");
					}
				}
				if (regexCheckStr(sb.toString())) {
					sb.replace(sb.length() - 1, sb.length(), "");
				}

				sb.append("]");
			}

			sb.append("},");
		}
		if (regexCheckStr(sb.toString())) {
			sb.replace(sb.length() - 1, sb.length(), "");
		}
		sb.append("]");

		return sb.toString();

	}

	/**
	 * 处理字符串
	 */
	private boolean regexCheckStr(String data) {
		if (data.endsWith(",")) {
			return true;
		}
		return false;
	}

	/**
	 * 教师报送
	 * @throws Exception 
	 */
	public void submitted(HttpServletRequest request) throws Exception {
		TbBizJzgjbxx jbxx = getSessionTeacher(request);
		TbBizJzgjbxx jzgjbxx = tbBizJzgjbxxManager.get(jbxx.getJsid());
		shDto = tbBizJzgjbxxManagerExt.submitted(jzgjbxx);
		if (shDto.getCount() > 0) {
			tbBizJzgjbxxManagerExt.submitInfo(jzgjbxx);
		} else {
			throw new Exception("信息不完善或已报送,不可报送!");
		}
	}

	/**
	 * 院校报送
	 * @throws Exception 
	 */
	public void yxSubmitted(HttpServletRequest request) throws Exception {
		String[] jsidarray = jsids.split(",");
		boolean cansubmit = true;
		for (String jsId : jsidarray) {
			TbBizJzgjbxx jzgjbxx = tbBizJzgjbxxManager.get(Long.valueOf(jsId));
			shDto = tbBizJzgjbxxManagerExt.syssubmitted(jzgjbxx);
			if (shDto.getCount() == 0) {
				cansubmit = false;
				break;
			}
		}
		if (cansubmit) {
			SysUser sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
			long bsrid = sUser.getUserId();
			for (String jsid : jsidarray) {
				TbBizJzgjbxx jzgjbxx = tbBizJzgjbxxManager.get(Long.valueOf(jsid));
				tbBizJzgjbxxManagerExt.yxsubmitInfo(jzgjbxx, bsrid);
			}
		} else {
			throw new Exception("信息不完善或已报送,不可报送!");
		}
	}

	/**
	 * 教师取消报送
	 * @throws Exception 
	 */
	public void cancelSubmit(HttpServletRequest request) throws Exception {
		TbBizJzgjbxx jbxx = getSessionTeacher(request);
		TbBizJzgjbxx jzgjbxx = tbBizJzgjbxxManager.get(jbxx.getJsid());
		shDto = tbBizJzgjbxxManagerExt.submitted(jzgjbxx);
		try {
			tbBizJzgjbxxManagerExt.cancelSubmit(jzgjbxx);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 判断是否打开
	 * @param request
	 * @param response
	 */
	public void checkBaseInfo(HttpServletRequest request, HttpServletResponse response) {

		String result = "0";
		TbBizJzgjbxx jbxx = tbBizJzgjbxxManager.get(getSessionTeacher(request).getJsid());
		if ((jbxx != null && jbxx.getSfwcbtx() != null && jbxx.getSfwcbtx() == 1)
				|| "4001".equals(request.getParameter("infoid"))) {///4001是基本信息菜单的id
			result = "1";
		}

		try {
			JSONObject json = new JSONObject();
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("open", result);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("teacherChangeExt toEdit() is error!", e);
		}
	}
}
