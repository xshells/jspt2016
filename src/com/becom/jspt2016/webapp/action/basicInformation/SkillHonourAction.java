/**
 * SkillHonour.java
 * com.becom.jspt2016.webapp.action.basicInformation
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webapp.action.basicInformation;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.dto.SkillHonourInfoDto;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbBizJzgyynlxx;
import com.becom.jspt2016.model.TbJzgxxsh;
import com.becom.jspt2016.service.ITbBizJzgyynlxxManager;
import com.becom.jspt2016.service.ext.ISkillHonourManagerExt;
import com.becom.jspt2016.service.ext.ITbJzgxxshManagerExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 * 			技能及证书
 * <p>
 * @author   wxl
 * @Date	 2016年10月20日 	 
 */
public class SkillHonourAction extends PageAction {

	/**
	 * 技能及证书Id组成的字符窜
	 */
	public String[] ck;
	/**
	 * 技能证书id
	 */
	public long jnzsId;
	/**
	 * 教职工id
	 */
	public long jsId;
	/**
	 * 语种
	 */
	public String yz;
	/**
	 * 掌握程度
	 */
	public String zwcd;
	/**
	 * 报送人类别
	 */
	public String bsrlb;
	/**
	 * 审核状态
	 */
	public String shzt;
	/**
	 * 审核时间
	 */
	public Date shsj;
	/**
	 * 审核结果
	 */
	public String shjg;
	/**
	 * 是否删除
	 */
	public String sfsc;
	/**
	 * 创建时间
	 */
	public Date cjsj;
	/**
	 * 教职工id
	 */
	public long jsid;
	/**
	 *	用户实体类
	 */
	public SysUser sUser;

	/**
	 * 老师用户
	 */
	public TbBizJzgjbxx teacher;
	public TbJzgxxsh tbJzgxxsh;
	protected Logger logger = Logger.getLogger(this.getClass());
	public TbBizJzgyynlxx tbBizJzgyynlInfo = new TbBizJzgyynlxx();

	@Spring
	public ITbBizJzgyynlxxManager tbBizJzgyynlxxManager;
	@Spring
	public ISkillHonourManagerExt skillHonourManagerExt;
	@Spring
	private ITbJzgxxshManagerExt tbJzgxxshManagerExt;

	@DefaultAction
	/**
	 * 
	 * 搜索
	 * <p>
	 * 列出技能及证书信息
	 */
	public Object doSearch(HttpServletRequest request) {
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			params.put("jsid", jbxx.getJsid());
			pageObj = skillHonourManagerExt.pageSearch(params, pageNo, pageSize);
			tbJzgxxsh = tbJzgxxshManagerExt.query(jbxx.getJsid());

		} catch (Exception e) {
			logger.error("SkillHonourAction doSearch is error!", e);
		}

		return "listskillhonour.jsp";
	}

	/**
	 * 
	 * 删除
	 * <p>
	 * 删除(假删)技能及证书信息
	 * 0-显示
	 * 1-已删除
	 */
	public Object deleteSkillHonour(HttpServletRequest request, HttpServletResponse response) {

		int result = 0;
		try {
			result = skillHonourManagerExt.deleteSkillHonour(ck);
			if (result == 1) {
				message = "删除成功";
			}
		} catch (Exception e) {
			if (result == 0) {
				message = "删除失败";
			}
			logger.error("SkillHonourAction delete is error!", e);
		}
		return doSearch(request);
	}

	/**
	 * 
	 * 查询
	 * <p>
	 * 所选的技能
	 */
	public void search(HttpServletRequest request, HttpServletResponse response) {
		TbBizJzgjbxx jbxx = getSessionTeacher(request);
		jsid = jbxx.getJsid();
		String result = "0";
		TbBizJzgyynlxx xx = skillHonourManagerExt.search(jsid, yz);
		if (xx == null) {///4001是基本信息菜单的id
			result = "1";
		}
		try {
			JSONObject json = new JSONObject();
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("result", result);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("SkillHonourAction search() is error!", e);
		}

	}

	/**
	 * 
	 * 增加
	 * <p>
	 * 技能及证书信息
	 */
	public Object addSkillHonour(HttpServletRequest request) {
		try {
			TbBizJzgyynlxx yynlxx = new TbBizJzgyynlxx();
			teacher = getSessionTeacher(request);
			yynlxx.setTbBizJzgjbxx(teacher);
			yynlxx.setYz(yz);
			yynlxx.setZwcd(zwcd);
			yynlxx.setCjsj(cjsj);
			yynlxx.setSfsc(sfsc);
			yynlxx.setBsrlb(bsrlb);
			yynlxx.setShzt(shzt);
			yynlxx.setShsj(shsj);
			yynlxx.setShjg(shjg);
			skillHonourManagerExt.insertSkillHonour(yynlxx);
		} catch (Exception e) {
			logger.error("SkillHonourAction addSkillHonour is error!", e);
		}
		return doSearch(request);
	}

	/**
	 * 
	 * 搜索
	 * <p>
	 * 根据id获取技能及证书对象信息
	 */
	public void toUpdateSkillHonour(HttpServletRequest request, HttpServletResponse response) {

		try {
			JSONObject jo = new JSONObject();
			SkillHonourInfoDto skillHonour = skillHonourManagerExt.getSkillHonour(jnzsId);
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			jo.put("skillHonour", skillHonour);
			out.write(jo.toString());
			out.flush();
			out.close();
		} catch (IOException e) {
			logger.error("SkillHonourAction toUpdateSkillHonour is error!", e);
		}
	}

	/**
	 * 
	 * 查询--更新
	 * <p>
	 * 所选的技能
	 */
	public void searchUpdate(HttpServletRequest request, HttpServletResponse response) {
		TbBizJzgjbxx jbxx = getSessionTeacher(request);
		jsid = jbxx.getJsid();
		String result = "0";
		TbBizJzgyynlxx xxUpdate = skillHonourManagerExt.searchUpdate(jsid, jnzsId, yz);
		System.out.println(xxUpdate == null);
		if (xxUpdate == null) {///4001是基本信息菜单的id
			result = "1";
		}
		try {
			JSONObject json = new JSONObject();
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("result", result);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("SkillHonourAction search() is error!", e);
		}
	}

	/**
	 * 
	 * 更新
	 * <p>
	 * 更新技能及证书对象信息
	 */
	public Object updateSkillHonour(HttpServletRequest request, HttpServletResponse response) {

		tbBizJzgyynlInfo.setJnzsId(jnzsId);
		tbBizJzgyynlInfo.setYz(yz);
		tbBizJzgyynlInfo.setZwcd(zwcd);
		skillHonourManagerExt.updateSkillHonour(tbBizJzgyynlInfo);
		try {
		} catch (Exception e) {
			logger.error("SkillHonourAction SkillHonour is error!", e);
		}
		return doSearch(request);
	}

}
