/**
 * TeacherMoralityAction.java
 * com.becom.jspt2016.webapp.action.morality
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webapp.action.basicInformation;

import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.dto.TbBizJzgsdxxInfoDto;
import com.becom.jspt2016.dto.TbCfgZdxbInfoDto;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbBizJzgsdcfxx;
import com.becom.jspt2016.model.TbBizJzgsdkhxx;
import com.becom.jspt2016.model.TbJzgxxsh;
import com.becom.jspt2016.service.ITbBizJzgjbxxManager;
import com.becom.jspt2016.service.ITbBizJzgsdcfxxManager;
import com.becom.jspt2016.service.ITbBizJzgsdkhxxManager;
import com.becom.jspt2016.service.ext.ITbBizJzgjyjxxxManagerExt;
import com.becom.jspt2016.service.ext.ITbBizJzgsdxxManagerExt;
import com.becom.jspt2016.service.ext.ITbJzgxxshManagerExt;
import com.becom.jspt2016.service.ext.IYearTestManagerExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 * 师德信息
 *
 * @author   shanjigang
 * @Date	 2016年10月16日 	 
 */
public class TeacherMoralityAction extends PageAction {
	public int jsid;//教师id

	public int khid;//考核id

	public int khidd;//考核id(编辑)

	public int cfid;//处分id

	public String khny;//考核年月

	public String khjl;//考核记录

	public String cflb;//处分类别

	public String cfyy;//处分发生日期

	public String ccfsrq;//处分发生日期

	public String cjsj;//创建时间

	public TbBizJzgsdcfxx tbBizJzgsdcfxx;

	public TbBizJzgjbxx tbBizJzgjbxx;

	public String[] ck;//删除的ids

	@Spring
	private ITbBizJzgsdxxManagerExt tbBizJzgsdxxManagerExt;

	@Spring
	private ITbBizJzgsdkhxxManager tbBizJzgsdkhxxManager;

	@Spring
	private ITbBizJzgsdcfxxManager tbBizJzgsdcfxxManager;

	@Spring
	private ITbBizJzgjbxxManager tbBizJzgjbxxManager;

	protected Logger logger = Logger.getLogger(this.getClass());

	public TbBizJzgsdxxInfoDto dto;//单条师德信息数据

	public List<TbCfgZdxbInfoDto> conclusion;//师德考核记录下拉框

	public String sdkhdwmc;//师德考核单位名称

	public String ssxd;//所属学段

	public TbJzgxxsh tbJzgxxsh;

	@Spring
	private ITbBizJzgjyjxxxManagerExt tbBizJzgjyjxxxManagerExt;

	@Spring
	private ITbJzgxxshManagerExt tbJzgxxshManagerExt;

	public String xxjgmc; //学校机构名称

	@Spring
	public IYearTestManagerExt yeartestmanagerExt;//考核年度接口

	@DefaultAction
	public Object doSearch(HttpServletRequest request) {
		try {
			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			long orgId = jbxx.getTbXxjgb().getXxjgid();
			Map<String, Object> params1 = new HashMap<String, Object>();
			params1.put("orgId", orgId);
			ssxd = tbBizJzgjyjxxxManagerExt.getXd(params1);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsid", jbxx.getJsid());
			tbJzgxxsh = tbJzgxxshManagerExt.query(jbxx.getJsid());
			pageObj = tbBizJzgsdxxManagerExt.doQuery(params, pageNo, pageSize);
			conclusion = tbBizJzgsdxxManagerExt.queryConclusion();
			Map<String, Object> params2 = new HashMap<String, Object>();
			params2.put("orgId", orgId);
			params2.put("jsid", jbxx.getJsid());

			xxjgmc = yeartestmanagerExt.getOrgNm(params2);//查询学校机构名称

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "middleSmallList.jsp";
	}

	/**
	 * 
	 * 添加师德考核信息
	 *
	 * @param request
	 * @param response
	 * @return 
	 */
	public Object addTeacherMorality(HttpServletRequest request, HttpServletResponse response) {
		try {
			//response.setContentType("text/xml;charset=UTF-8");
			//sdkhdwmc = new String(request.getParameter("sdkhdwmc").getBytes("iso-8859-1"), "utf-8");
			//response.setCharacterEncoding("UTF-8");
			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			Date currentTime = new Date();
			TbBizJzgsdkhxx khxx = new TbBizJzgsdkhxx();
			TbBizJzgsdcfxx cfxx = new TbBizJzgsdcfxx();
			khxx.setTbBizJzgjbxx(jbxx);
			khxx.setSdkhny(khny);
			khxx.setShsj(currentTime);
			khxx.setCjsj(currentTime);
			khxx.setSdkhny(khny);
			khxx.setSdkhjl(khjl);
			khxx.setSdkhdwmc(sdkhdwmc);
			khxx.setCjr(String.valueOf(jbxx.getJsid()));
			tbBizJzgsdxxManagerExt.addTeacherMorality(khxx, cfxx);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return doSearch(request);
	}

	/**
	 * 
	 * 编辑师德考核信息
	 *
	 * @param request
	 * @param response
	 * @return 
	 */
	public Object editTeacherMorality(HttpServletRequest request, HttpServletResponse response) {
		try {
			//response.setContentType("text/xml;charset=UTF-8");
			//sdkhdwmc = new String(request.getParameter("sdkhdwmc").getBytes("iso-8859-1"), "utf-8");
			//response.setCharacterEncoding("UTF-8");
			tbBizJzgsdxxManagerExt.editKaohe(khidd, khny, khjl, sdkhdwmc);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return doSearch(request);
	}

	/**
	 * 
	 * 删除师德考核信息
	 *
	 * @param request
	 * @param response
	 * @return 
	 */
	public Object delTeacherMorality(HttpServletRequest request, HttpServletResponse response) {
		try {

			Map<String, Object> params = new HashMap<String, Object>();
			tbBizJzgsdxxManagerExt.delTeacherMorality(ck);

		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return doSearch(request);
	}

	/**
	 * 
	 * 编辑回显
	 *
	 * @param request
	 * @param reponse 
	 */
	public void doQuery(HttpServletRequest request, HttpServletResponse reponse) {
		try {
			reponse.setContentType("text/xml;charset=UTF-8");
			JSONObject json = new JSONObject();
			TbBizJzgjbxx jbxx = getSessionTeacher(request);
			dto = tbBizJzgsdxxManagerExt.editTeacherMorality(jbxx.getJsid(), khid, cfid);
			conclusion = tbBizJzgsdxxManagerExt.queryConclusion();
			PrintWriter out = reponse.getWriter();
			json.put("dto", dto);
			json.put("conclusion", conclusion);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt doSearch() is error!", e);
		}
		return;
	}

}
