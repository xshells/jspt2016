/**
 * LoginAction.java
 * com.becom.jspt2016.webapp.action
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webapp.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.digest.DigestUtils;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.common.ConstantBean;
import com.becom.jspt2016.model.SysFunc;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.service.ext.ILoginUserManagerExt;

/**
 * 登陆接口  用户权限
 * <p>
 *
 * @author   YuChengCheng
 * @Date	 2016年10月16日 	 
 */
public class LoginTestAction extends BaseAction {

	/**
	 * 机构用户验证
	 * <p>
	 *
	 * @param organization_code
	 * @param pwd_md5
	 * @param token
	 * @return organizationLoginResult
	 */
	public String organizationLogin(HttpServletRequest request) {

		String organization_code = request.getParameter("organization_code");
		String pwd_md5 = request.getParameter("pwd_md5");
		String token = request.getParameter("token");

		if (organization_code == null) {
			organizationLoginResult = Constant.FOUR;
			return organizationLoginResult;
		}

		if (pwd_md5 == null) {
			organizationLoginResult = Constant.FOUR;
			return organizationLoginResult;
		}
		try {
			sUser = loginUserManagerExt.findOrgByUserNameAndPwd(organization_code, pwd_md5);
			if (sUser == null) {
				organizationLoginResult = Constant.FOUR;
				return organizationLoginResult;
			}
			String tableMd5 = DigestUtils.md5Hex(sUser.getUserName() + sUser.getUserPwd() + Constant.KEY_STRING);
			if (tableMd5.equals(token)) {
				organizationLoginResult = constantBean.get("loginUrl");
				request.getSession().setAttribute(Constant.KEY_LOGIN_USER, sUser);
				request.getSession().setAttribute(Constant.KEY_LOGIN_USER_TYPE, Constant.LOGIN_ORGANIAZTION);
			} else {
				organizationLoginResult = Constant.FOUR;
			}
		} catch (Exception e) {
			e.printStackTrace();
			organizationLoginResult = Constant.FOUR;
		}
		//		jsonOrganizationLoginResult = JSONArray
		//				.fromObject(organizationLoginResult);

		return organizationLoginResult;
	}

	/**
	 * 教师用户验证
	 * <p>
	 *
	 * @param teacher_edu_id
	 * @param login_time
	 * @param token
	 * @return teacherLoginResult
	 */
	public String teacherLogin(HttpServletRequest request) {

		String teacher_edu_id = request.getParameter("teacher_edu_id");
		String login_time = request.getParameter("login_time");
		String token = request.getParameter("token");

		if (teacher_edu_id == null) {
			teacherLoginResult = Constant.FOUR;
			return teacherLoginResult;
		}
		try {
			teacher = loginUserManagerExt.findTeaByEduId(teacher_edu_id);
			if (teacher == null) {
				teacherLoginResult = Constant.FOUR;
				return teacherLoginResult;
			}
			String tableMd5 = DigestUtils.md5Hex(teacher.getEduId() + login_time + Constant.KEY_STRING);
			if (tableMd5.equals(token)) {
				teacherLoginResult = constantBean.get("loginUrl");
				request.getSession().setAttribute(Constant.KEY_LOGIN_USER, teacher);
				request.getSession().setAttribute(Constant.KEY_LOGIN_USER_TYPE, Constant.LOGIN_TEACHER);
			} else {
				teacherLoginResult = Constant.FOUR;
			}
		} catch (Exception e) {
			e.printStackTrace();
			teacherLoginResult = Constant.FOUR;
		}

		return teacherLoginResult;
	}

	@DefaultAction
	public Object showMenuTree(HttpServletRequest request) {

		String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
		//机构用户
		if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
			sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);

			if (String.valueOf(sUser.getUserId()) != null) {
				funcList = loginUserManagerExt.getMenuByUserId(String.valueOf(sUser.getUserId()));
			}
			sUser = loginUserManagerExt.handleUser(sUser);
		} else {//教师用户
			teacher = (TbBizJzgjbxx) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
			if (teacher.getEduId() != null) {
				funcList = loginUserManagerExt.getMenuByEduId(teacher.getEduId());
			}
		}
		//		funcList = loginUserManagerExt.getMenuByUserId("2");
		//组装数据
		jsonArray = assembleDate(funcList);

		//		jsonArray = JSONArray.fromObject("");
		return "menuTreeList.jsp";
	}

	public String assembleDate(List<SysFunc> funcs) {
		SysFunc f = null;
		List<SysFunc> f1 = new ArrayList<SysFunc>();
		List<SysFunc> f2 = new ArrayList<SysFunc>();
		List<SysFunc> f3 = new ArrayList<SysFunc>();
		for (int i = 0; i < funcs.size(); i++) {
			f = funcs.get(i);
			if ("1".equals(f.getFuncLevel().toString())) {//1
				f1.add(f);
				funcs.remove(i);
				i--;
			}
			if ("2".equals(f.getFuncLevel().toString())) {//2
				f2.add(f);
				funcs.remove(i);
				i--;
			}
			if ("3".equals(f.getFuncLevel().toString())) {//3
				f3.add(f);
				funcs.remove(i);
				i--;
			}
		}

		//拼接json
		StringBuilder sb = new StringBuilder("[");
		SysFunc sf1 = null;
		SysFunc sf2 = null;
		SysFunc sf3 = null;
		for (int i = 0; i < f1.size(); i++) {
			sf1 = f1.get(i);
			sb.append("{");//1
			sb.append("'id':" + sf1.getFuncId());
			sb.append(",'text':" + "'" + sf1.getFuncName() + "'");
			sb.append(",'children':[");//2
			for (int j = 0; j < f2.size(); j++) {
				sf2 = f2.get(j);
				if (sf1.getFuncId() == sf2.getParentFuncId()) {
					sb.append("{");
					sb.append("'id':" + sf2.getFuncId());
					sb.append(",'text':" + "'" + sf2.getFuncName() + "'");
					sb.append(",'url':" + "'" + sf2.getUrl() + "'");

					if (f3 != null && f3.size() > 0) {
						sb.append(",'children':[");
						for (int j2 = 0; j2 < f3.size(); j2++) {
							sf3 = f3.get(j2);
							if (sf2.getFuncId() == sf3.getParentFuncId()) {
								sb.append("{");
								sb.append("'id':" + sf3.getFuncId());
								sb.append(",'text':" + "'" + sf3.getFuncName() + "'");
								sb.append(",'url':" + "'" + sf3.getUrl() + "'");
								sb.append("},");
							}
						}
						if (regexCheckStr(sb.toString())) {
							sb.replace(sb.length() - 1, sb.length(), "");
						}
						sb.append("]");

					}

					sb.append("},");//
				}
			}
			if (regexCheckStr(sb.toString())) {
				sb.replace(sb.length() - 1, sb.length(), "");
			}
			sb.append("]");//2

			sb.append("},");//1end
		}

		if (regexCheckStr(sb.toString())) {
			sb.replace(sb.length() - 1, sb.length(), "");
		}
		sb.append("]");//最外一层
		return sb.toString();
	}

	private boolean regexCheckStr(String data) {
		if (data.endsWith(",")) {
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		LoginAction a = new LoginAction();
		System.out.println("输出信息：===========" + DigestUtils.md5Hex("1" + "17:40:36" + "JSPT_DLJK"));
	}

	@Spring
	public ConstantBean constantBean;

	@Spring
	public ILoginUserManagerExt loginUserManagerExt;

	public List<SysFunc> funcList;

	public String jsonArray;

	public String teacherLoginResult;//教师返回值

	public String organizationLoginResult;//机构返回值

	public SysUser sUser;

	public TbBizJzgjbxx teacher;

}
