package com.becom.jspt2016.webapp.action;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.Before;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.common.ConstantBean;
import com.becom.jspt2016.dto.登录用户;
import com.becom.jspt2016.model.TbBizJzgjbxx;

/**
 * @author wanghai
 *
 */
public abstract class BaseAction {

	public String message;

	public String error;

	public 登录用户 userDto;

	public String orderType;

	public String orderBy;

	public boolean hideOrShowSearchFlag = true;

	public int year;

	public int preYear;

	public int mouth;

	@Spring
	public ConstantBean constantBean;

	/**
	 * ��ȡ��¼�û���Session����.
	 * 
	 * @param req
	 * @return
	 */
	@Before
	public void init(HttpServletRequest request, HttpServletResponse response) {
		//		userDto=(登录用户) req.getSession().getAttribute(Constant.KEY_LOGIN_USER);
		Calendar now = Calendar.getInstance();
		year = now.get(Calendar.YEAR);
		preYear = now.get(Calendar.YEAR) - 1;
		mouth = now.get(Calendar.MONTH) + 1;//月份需加1

	}

	public Object toLoginPage() {
		return "/login.jsp";
	}

	/**
	 * 获取教师用户信息
	 * @param request
	 * @return
	 */
	protected TbBizJzgjbxx getSessionTeacher(HttpServletRequest request) {
		TbBizJzgjbxx jbxx = null;
		String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
		if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
			Object obj = request.getSession().getAttribute(Constant.KEY_SESSION_TEACHER);
			if (obj != null) {
				jbxx = (TbBizJzgjbxx) obj;
			}
			request.setAttribute("SESSION_ISPUB", "0");
		} else {//教师用户
			jbxx = (TbBizJzgjbxx) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
			request.setAttribute("SESSION_ISPUB", "1");
		}
		return jbxx;
	}

	protected String getActionType(HttpServletRequest request) {
		Object obj = request.getSession().getAttribute(Constant.SESSION_ACTION_TYPE);
		if (obj == null) {
			return "";
		}
		return (String) obj;
	}

	public static void main(String[] args) {
		Calendar now = Calendar.getInstance();
		System.out.println(now.get(Calendar.YEAR));
	}
}
