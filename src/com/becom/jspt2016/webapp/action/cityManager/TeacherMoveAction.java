/**
 * TeacherInformatAction.java
 * com.becom.jspt2016.webapp.action.cityManager
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webapp.action.cityManager;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.service.ITbBizJzgjbxxManager;
import com.becom.jspt2016.service.ext.IInstitutionsManageExt;
import com.becom.jspt2016.service.ext.ITeacherAccountManageExt;
import com.becom.jspt2016.service.ext.ITeacherChangeExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 *教师待调动信息
 * @author   dgf
 */
public class TeacherMoveAction extends PageAction {
	@Spring
	private ITeacherAccountManageExt teacherAccountManageExt;

	protected Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 教职工Id组成的字符窜
	 */
	public String[] ck;

	/**
	 * 教职工Id
	 */
	public long jsId;

	/**
	 * 教育Id	
	 */
	public String tEduId;

	public String tEduIdQuery;

	/**
	 * 身份证号
	 */
	public String tCNo;

	public String tCNoQuery;

	/**
	 * 教师姓名
	 */
	public String tName;

	public String tNameQuery;

	/**
	 * 账号状态
	 */
	public String tAccountStatus;

	public String tAccountStatusQuery;

	/**
	 * 身份类型
	 */
	public String shenfen;

	/**
	 * 老师用户
	 */
	public TbBizJzgjbxx teacher;

	/**
	 * 机构用户
	 */
	public SysUser sUser;
	
	/**
	 * 教师基本信息service
	 */
	@Spring
	private ITbBizJzgjbxxManager tbBizJzgjbxxManager;
	
	@Spring
	private ITeacherChangeExt teacherChangeExt;
	
	@Spring
	private IInstitutionsManageExt institutionsManageExt;
	
	/**
	 * 
	 * 搜索
	 * <p>
	 * 教师账号列表展示
	 */
	@DefaultAction
	public Object doSearch(HttpServletRequest request) {
		try {
			//获取用户信息
			String loginType = (String) request.getSession().getAttribute(
					Constant.KEY_LOGIN_USER_TYPE);
			//机构用户
			if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
				sUser = (SysUser) request.getSession().getAttribute(
						Constant.KEY_LOGIN_USER);

			} else {//教师用户
				teacher = (TbBizJzgjbxx) request.getSession().getAttribute(
						Constant.KEY_LOGIN_USER);
			}

			Map<String, Object> param = new HashMap<String, Object>();
			if (tEduIdQuery != null
					&& !"".equals(tEduIdQuery.replaceAll(" ", ""))) {
				param.put("tEduId", tEduIdQuery.replaceAll(" ", ""));
			}
			if (tCNoQuery != null && !"".equals(tCNoQuery.replaceAll(" ", ""))) {
				param.put("tCNo", tCNoQuery.replaceAll(" ", ""));
			}
			if (tNameQuery != null
					&& !"".equals(tNameQuery.replaceAll(" ", ""))) {
				param.put("tName", tNameQuery.replaceAll(" ", ""));
			}
			param.put("sfsc", 2);
			pageObj = teacherChangeExt.findTeacherMove(param, pageNo, pageSize);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "teacherMove.jsp";
	}
	
	/**
	 * 
	 * 删除
	 * <p>
	 * 删除教师账号
	 *
	 */
	public Object delete(HttpServletRequest request,
			HttpServletResponse response) {

		int result = 0;
		try {
			result = teacherAccountManageExt.delete(ck);
			if (result == 0) {
				message = "删除成功";
			}
		} catch (Exception e) {
			if (result == 1) {
				message = "删除失败";
			}
			e.printStackTrace();
		}
		return doSearch(request);
	}
}
