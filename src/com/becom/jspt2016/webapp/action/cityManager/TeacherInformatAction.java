/**
 * TeacherInformatAction.java
 * com.becom.jspt2016.webapp.action.cityManager
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webapp.action.cityManager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.dto.DisCodesDto;
import com.becom.jspt2016.dto.TeacherInformtionDto;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.service.ITbBizJzgjbxxManager;
import com.becom.jspt2016.service.ext.IInstitutionsManageExt;
import com.becom.jspt2016.service.ext.ITeacherInforManagerExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 *教师基本信息
 * @author   Dongxifu
 * @Date	 2016年10月24日 	 
 */
public class TeacherInformatAction extends PageAction {
	String qxdm;//区县代码
	String edu_id;//教育Id   
	String xm;//姓名
	String xb;//性别
	String sfzjh;//身份证号
	String csrq;//出生日期
	String shsj;//审核时间
	/**
	 * 机构用户
	 */
	public SysUser sUser;

	/**
	 * 老师用户
	 */
	public TbBizJzgjbxx teacher;

	@Spring
	public ITbBizJzgjbxxManager tbbizjzgjbxxmanager;//教职工基本信息Manager

	public TbBizJzgjbxx tbbizjzgjbxx;//教职工基本信息实体
	public List<TeacherInformtionDto> list;
	@Spring
	public ITeacherInforManagerExt teacherinformanagerext;
	/**
	 * 区级审核通过
	 */
	public static final String QUJIADUITYES = "21";
	/**
	 * 教育Id
	 */
	public String eduIdQuery;
	/**
	 * 身份证号
	 */
	public String cardNoQuery;
	/**
	 * 老师姓名
	 */
	public String teacherNameQuery;
	/**
	 * 所在区的
	 */
	public String disCodeQuery;
	
	public String affiliatedSchoolQuery;
	public String unitTypeQuery;
	public String institutionNameQuery;
	
	
	
	public String userType;
	@Spring
	private IInstitutionsManageExt institutionsManageExt;
	/**
	 * 区县列表
	 */
	public List<DisCodesDto> disCodes;

	/**
	 * 
	 * 搜索
	 * <p>
	 * 教师信息查询
	 */
	@DefaultAction
	public Object doSearch(HttpServletRequest request) {
		try {
			//获取用户信息
			String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
			//机构用户
			if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
				sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);

			} else {//教师用户
				teacher = (TbBizJzgjbxx) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
			}
			//用户类型 1：市级2：区级
			userType = sUser.getUserType();
			Map<String, Object> param = new HashMap<String, Object>();
			if (eduIdQuery != null && !"".equals(eduIdQuery.replaceAll(" ", ""))) {
				param.put("eduIdQuery", eduIdQuery.replaceAll(" ", ""));
			}
			if (cardNoQuery != null && !"".equals(cardNoQuery.replaceAll(" ", ""))) {
				param.put("cardNoQuery", cardNoQuery.replaceAll(" ", ""));
			}
			if (teacherNameQuery != null && !"".equals(teacherNameQuery.replaceAll(" ", ""))) {
				param.put("teacherNameQuery", teacherNameQuery.replaceAll(" ", ""));
			}
			if (disCodeQuery != null && !"".equals(disCodeQuery.replaceAll(" ", ""))) {
				param.put("disCodeQuery", disCodeQuery.replaceAll(" ", ""));
			}
			
			if (affiliatedSchoolQuery != null && !"".equals(affiliatedSchoolQuery.replaceAll(" ", ""))) {
				param.put("affiliatedSchoolQuery", affiliatedSchoolQuery.replaceAll(" ", ""));
			}
			if (unitTypeQuery != null && !"".equals(unitTypeQuery.replaceAll(" ", ""))) {
				param.put("unitTypeQuery", unitTypeQuery.replaceAll(" ", ""));
			}
			if (institutionNameQuery != null && !"".equals(institutionNameQuery.replaceAll(" ", ""))) {
				param.put("institutionNameQuery", institutionNameQuery.replaceAll(" ", ""));
			}
			
			param.put("aduitStutasQuery", QUJIADUITYES);//区级审核通过的

			pageObj = teacherinformanagerext.doSearch(param, pageNo, pageSize);
			disCodes = institutionsManageExt.queryDisCodes(null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "TeaInformation.jsp";
	}
}
