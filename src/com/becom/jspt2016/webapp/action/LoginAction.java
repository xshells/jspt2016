/**
 * LoginAction.java
 * com.becom.jspt2016.webapp.action
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webapp.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.codec.digest.DigestUtils;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;
import org.nestframework.data.Json;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.common.ConstantBean;
import com.becom.jspt2016.dto.TbJzgxxshLogDto;
import com.becom.jspt2016.dto.UserTodoDto;
import com.becom.jspt2016.model.SysFunc;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbJzgxxsh;
import com.becom.jspt2016.service.ITbXxjgbManager;
import com.becom.jspt2016.service.ext.ILoginUserManagerExt;
import com.becom.jspt2016.service.ext.ITbJzgxxshManagerExt;

/**
 * 登陆接口  用户权限
 * <p>
 *
 * @author   YuChengCheng
 * @Date	 2016年10月16日 	 
 */
public class LoginAction extends BaseAction {
	public String userType;

	public String managerType;

	/**
	 * 学校机构用户类型
	 */
	public static final String XXJGTYPE = "3";
	/**
	 * 区县用户类型
	 */
	public static final String QXTYPE = "2";
	/**
	 * 市用户类型
	 */
	public static final String STYPE = "1";

	/**
	 * 教师待办事项
	 */
	public TbJzgxxshLogDto tbJzgxxshLogDto;

	public String ssxd;

	/**
	 * 机构、区县待办事项
	 */
	public List<UserTodoDto> sysTodoList;

	public TbJzgxxsh tbJzgxxsh;

	public String unloginUrl;

	public Map<String, Object> teacherNum;

	@Spring
	private ITbJzgxxshManagerExt tbJzgxxshManagerExt;

	@Spring
	private ITbXxjgbManager tbXxjgbManager;

	/**
	 * 机构用户验证
	 * <p>
	 *
	 * @param organization_code
	 * @param pwd_md5
	 * @param token
	 * @return organizationLoginResult
	 */
	public Object organizationLogin(HttpServletRequest request) {

		String organization_code = request.getParameter("organization_code");
		String pwd_md5 = request.getParameter("pwd_md5");
		String token = request.getParameter("token");

		if (organization_code == null) {
			return "404.jsp";
		}

		if (pwd_md5 == null) {
			return "404.jsp";
		}
		try {
			sUser = loginUserManagerExt.findOrgByUserNameAndPwd(organization_code, pwd_md5);
			if (sUser == null) {
				return "404.jsp";
			}
			String tableMd5 = DigestUtils.md5Hex(sUser.getUserName() + sUser.getUserPwd() + Constant.KEY_STRING);
			if (!tableMd5.equals(token)) {
				return "404.jsp";
			}
			request.getSession().setAttribute(Constant.KEY_LOGIN_USER, sUser);
			request.getSession().setAttribute(Constant.KEY_LOGIN_USER_TYPE, Constant.LOGIN_ORGANIAZTION);
			//			result = constantBean.get("loginUrl");
			return showMenuTree(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Json
	public Object organizationLoginJson(HttpServletRequest request) {

		String organization_code = request.getParameter("organization_code");
		String pwd_md5 = request.getParameter("pwd_md5");
		String token = request.getParameter("token");

		if (organization_code == null) {
			organizationLoginResult.put("organizationLoginResult", Constant.FOUR);
			return organizationLoginResult;
		}

		if (pwd_md5 == null) {
			organizationLoginResult.put("organizationLoginResult", Constant.FOUR);
			return organizationLoginResult;
		}
		try {
			sUser = loginUserManagerExt.findOrgByUserNameAndPwd(organization_code, pwd_md5);
			if (sUser == null) {
				organizationLoginResult.put("organizationLoginResult", Constant.FOUR);
				return organizationLoginResult;
			}
			String tableMd5 = DigestUtils.md5Hex(sUser.getUserName() + sUser.getUserPwd() + Constant.KEY_STRING);
			if (tableMd5.equals(token)) {

				String orgResult = constantBean.get("loginUrl") + "organizationLogin&" + "organization_code="
						+ organization_code + "&pwd_md5=" + pwd_md5 + "&token=" + token;
				organizationLoginResult.put("organizationLoginResult", orgResult);

				request.getSession().setAttribute(Constant.KEY_LOGIN_USER, sUser);
				request.getSession().setAttribute(Constant.KEY_LOGIN_USER_TYPE, Constant.LOGIN_ORGANIAZTION);
			} else {
				organizationLoginResult.put("organizationLoginResult", Constant.FOUR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			organizationLoginResult.put("organizationLoginResult", Constant.FOUR);
		}
		return organizationLoginResult;
	}

	/**
	 * 教师用户验证
	 * <p>
	 *
	 * @param teacher_edu_id
	 * @param login_time
	 * @param token
	 * @return teacherLoginResult
	 */
	public Object teacherLogin(HttpServletRequest request) {

		String teacher_edu_id = request.getParameter("teacher_edu_id");
		String login_time = request.getParameter("login_time");
		String token = request.getParameter("token");

		if (teacher_edu_id == null) {
			return "404.jsp";
		}
		try {
			teacher = loginUserManagerExt.findTeaByEduId(teacher_edu_id);
			if (teacher == null) {
				return "404.jsp";
			}
			String tableMd5 = DigestUtils.md5Hex(teacher.getEduId() + login_time + Constant.KEY_STRING);
			if (!tableMd5.equals(token)) {
				return "404.jsp";
			}
			request.getSession().setAttribute(Constant.KEY_LOGIN_USER, teacher);
			request.getSession().setAttribute(Constant.KEY_LOGIN_USER_TYPE, Constant.LOGIN_TEACHER);
			//			result = constantBean.get("loginUrl");
			return showMenuTree(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@DefaultAction
	public Object showMenuTree(HttpServletRequest request) {

		String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
		//机构用户
		if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
			sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);

			if (String.valueOf(sUser.getUserId()) != null) {
				funcList = loginUserManagerExt.getMenuByUserId(String.valueOf(sUser.getUserId()));
			}

			sUser = loginUserManagerExt.handleUser(sUser);//加载用户姓名
			managerType = sUser.getUserType();
			//TODO 查询教师数量
			if (managerType.equals(QXTYPE) || managerType.equals(STYPE) || managerType.equals(XXJGTYPE)) {
				if (managerType.equals(XXJGTYPE)) {
					teacherNum = loginUserManagerExt.getTeacherNum(managerType, sUser.getId(),
							tbXxjgbManager.get(Long.valueOf(sUser.getId())).getSsxd());
				} else {
					teacherNum = loginUserManagerExt.getTeacherNum(managerType, sUser.getId(), null);
				}
			}
			if (managerType.equals(QXTYPE) || managerType.equals(XXJGTYPE)) {
				sysTodoList = loginUserManagerExt.userTodo(sUser.getUserId(), managerType, sUser.getId());
			}
			if (managerType.equals(XXJGTYPE)) {
				ssxd = tbXxjgbManager.get(Long.valueOf(sUser.getId())).getSsxd();
			}
			//e10adc3949ba59abbe56e057f20f883e=123456
		} else {//教师用户
			teacher = (TbBizJzgjbxx) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
			if (teacher.getEduId() != null) {

				tbJzgxxshLogDto = loginUserManagerExt.teacherTodo(teacher.getJsid());
				tbJzgxxsh = tbJzgxxshManagerExt.query(teacher.getJsid());
				funcList = loginUserManagerExt.getMenuByEduId(teacher.getEduId());
			}
		}
		//		funcList = loginUserManagerExt.getMenuByUserId("2");
		//组装数据
		jsonArray = assembleDate(funcList);

		if (sUser != null) {
			userType = "1";
		} else {
			userType = "2";
		}
		unloginUrl = constantBean.get("unloginUrl");
		//		jsonArray = JSONArray.fromObject("");
		return "menuTreeList.jsp";
	}

	public void loginout(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
		String outUrl = "";
		//机构用户
		if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
			outUrl = constantBean.get("unloginUrlJG");
		} else {
			outUrl = constantBean.get("unloginUrl");
		}
		request.getSession().removeAttribute(Constant.KEY_LOGIN_USER);
		response.sendRedirect(outUrl);
	}

	public String assembleDate(List<SysFunc> funcs) {
		SysFunc f = null;
		List<SysFunc> f1 = new ArrayList<SysFunc>();
		List<SysFunc> f2 = new ArrayList<SysFunc>();
		List<SysFunc> f3 = new ArrayList<SysFunc>();
		for (int i = 0; i < funcs.size(); i++) {
			f = funcs.get(i);
			if ("1".equals(f.getFuncLevel().toString())) {//1
				f1.add(f);
				funcs.remove(i);
				i--;
			}
			if ("2".equals(f.getFuncLevel().toString())) {//2
				f2.add(f);
				funcs.remove(i);
				i--;
			}
			if ("3".equals(f.getFuncLevel().toString())) {//3
				f3.add(f);
				funcs.remove(i);
				i--;
			}
		}

		//拼接json
		StringBuilder sb = new StringBuilder("[");
		SysFunc sf1 = null;
		SysFunc sf2 = null;
		SysFunc sf3 = null;
		for (int i = 0; i < f1.size(); i++) {
			sf1 = f1.get(i);
			sb.append("{");//1
			sb.append("'id':" + sf1.getFuncId());
			sb.append(",'text':" + "'" + sf1.getFuncName() + "'");
			sb.append(",'children':[");//2
			for (int j = 0; j < f2.size(); j++) {
				sf2 = f2.get(j);
				if (sf1.getFuncId() == sf2.getParentFuncId()) {
					sb.append("{");
					sb.append("'id':" + sf2.getFuncId());
					sb.append(",'text':" + "'" + sf2.getFuncName() + "'");
					sb.append(",'url':" + "'" + sf2.getUrl() + "'");

					if (f3 != null && f3.size() > 0) {
						sb.append(",'children':[");
						for (int j2 = 0; j2 < f3.size(); j2++) {
							sf3 = f3.get(j2);
							if (sf2.getFuncId() == sf3.getParentFuncId()) {
								sb.append("{");
								sb.append("'id':" + sf3.getFuncId());
								sb.append(",'text':" + "'" + sf3.getFuncName() + "'");
								sb.append(",'url':" + "'" + sf3.getUrl() + "'");
								sb.append("},");
							}
						}
						if (regexCheckStr(sb.toString())) {
							sb.replace(sb.length() - 1, sb.length(), "");
						}
						sb.append("]");

					}

					sb.append("},");//
				}
			}
			if (regexCheckStr(sb.toString())) {
				sb.replace(sb.length() - 1, sb.length(), "");
			}
			sb.append("]");//2

			sb.append("},");//1end
		}

		if (regexCheckStr(sb.toString())) {
			sb.replace(sb.length() - 1, sb.length(), "");
		}
		sb.append("]");//最外一层
		return sb.toString();
	}

	private boolean regexCheckStr(String data) {
		if (data.endsWith(",")) {
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		LoginAction a = new LoginAction();
		/*	System.out.println("教师输出信息：==========="
					+ DigestUtils.md5Hex("90100011" + "17:40:36" + "JSPT_DLJK"));*/

		System.out.println("输出信息：===========" + DigestUtils.md5Hex("sj" + "111111" + "JSPT_DLJK"));

	}

	@Spring
	public ConstantBean constantBean;

	@Spring
	public ILoginUserManagerExt loginUserManagerExt;

	public List<SysFunc> funcList;

	public String jsonArray;

	public String result;//教师返回值

	public JSONObject organizationLoginResult = new JSONObject();//机构返回值

	public SysUser sUser;

	public TbBizJzgjbxx teacher;

}
