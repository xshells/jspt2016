/**
 * TeacherAccountManageAction.java
 * com.becom.jspt2016.webapp.action.school
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.webapp.action.schoolManager;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.dto.InstitutionsInfoDto;
import com.becom.jspt2016.dto.TeacherAccountInfoDto;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.service.ITbBizJzgjbxxManager;
import com.becom.jspt2016.service.ext.IInstitutionsManageExt;
import com.becom.jspt2016.service.ext.ITeacherAccountManageExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 * 学校模块
 * <p>
 * 教师待调出查询
 * @author   
 * @author   
 * @Date	 2016年11月12日
 * @Date	 2016年11月12日 
 */

public class TeacherStayCallOutAction extends PageAction {
	@Spring
	private ITeacherAccountManageExt teacherAccountManageExt;

	protected Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 教职工Id组成的字符窜
	 */
	public String[] ck;

	/**
	 * 教职工Id
	 */
	public long jsId;

	
	/**
	 * 删除状态2：待调出
	 */
	private static final String RSFSCR= "2";
	/**
	 * 教育Id	
	 */
	public String tEduId;

	public String tEduIdQuery;

	/**
	 * 身份证号
	 */
	public String tCNo;

	public String tCNoQuery;

	/**
	 * 教师姓名
	 */
	public String tName;

	public String tNameQuery;

	/**
	 * 账号状态
	 */
	public String tAccountStatus;

	public String tAccountStatusQuery;

	/**
	 * 身份类型
	 */
	public String shenfen;

	/**
	 * 性别
	 */
	public String sex;

	/**
	 * 生日
	 */
	public String birthday;

	/**
	 * 老师用户
	 */
	public TbBizJzgjbxx teacher;

	/**
	 * 机构用户
	 */
	public SysUser sUser;

	/**
	 * 用户类型
	 */
	public String userType;

	public TbBizJzgjbxx teacherInfo = new TbBizJzgjbxx();

	/**
	 * 教师基本信息service
	 */
	@Spring
	private ITbBizJzgjbxxManager tbBizJzgjbxxManager;

	@Spring
	private IInstitutionsManageExt institutionsManageExt;

	/**
	 * 
	 * 搜索
	 * <p>
	 * 教师账号列表展示
	 */
	@DefaultAction
	public Object doSearch(HttpServletRequest request) {
		try {
			//获取用户信息
			String loginType = (String) request.getSession().getAttribute(
					Constant.KEY_LOGIN_USER_TYPE);
			//机构用户
			if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
				sUser = (SysUser) request.getSession().getAttribute(
						Constant.KEY_LOGIN_USER);

			} else {//教师用户
				teacher = (TbBizJzgjbxx) request.getSession().getAttribute(
						Constant.KEY_LOGIN_USER);
			}

			Map<String, Object> param = new HashMap<String, Object>();
			if (tEduIdQuery != null
					&& !"".equals(tEduIdQuery.replaceAll(" ", ""))) {
				param.put("tEduId", tEduIdQuery.replaceAll(" ", ""));
			}
			if (tCNoQuery != null && !"".equals(tCNoQuery.replaceAll(" ", ""))) {
				param.put("tCNo", tCNoQuery.replaceAll(" ", ""));
			}
			if (tNameQuery != null
					&& !"".equals(tNameQuery.replaceAll(" ", ""))) {
				param.put("tName", tNameQuery.replaceAll(" ", ""));
			}
			if (tAccountStatusQuery != null
					&& !"".equals(tAccountStatusQuery.replaceAll(" ", ""))) {
				param.put("tAccountStatus", tAccountStatusQuery);
			}
			//获取机构信息 通过机构代码
			InstitutionsInfoDto institution = institutionsManageExt
					.getInstitution(sUser.getUserName());
			param.put("xxjgId", institution.getInstitutionId());//机构id
			param.put("sfsc", RSFSCR);
			pageObj = teacherAccountManageExt.findTeacherInfo(param, pageNo, pageSize);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "teacherStayCallOut.jsp";
	}

	/**
	 * 
	 * 删除
	 * <p>
	 * 删除教师账号
	 *
	 */
	public Object delete(HttpServletRequest request,
			HttpServletResponse response) {

		int result = 0;
		try {
			result = teacherAccountManageExt.delete(ck);
			if (result == 0) {
				message = "删除成功";
			}
		} catch (Exception e) {
			if (result == 1) {
				message = "删除失败";
			}
			e.printStackTrace();
		}
		return doSearch(request);
	}

	/**
	 * 找回账号
	 * <p>
	 * 老师账号找回
	 */
	public Object retrieveAccount(HttpServletRequest request,
			HttpServletResponse response) {
		int result = 0;
		try {
			result = teacherAccountManageExt.retrieveAccount(ck);
			if (result == 0) {
				message = "找回成功";
			}
		} catch (Exception e) {
			if (result == 1) {
				message = "找回失败";
			}
			e.printStackTrace();
		}
		return doSearch(request);
	}

	/**
	 * 添加
	 * <p>
	 *添加老师账号
	 * @param request
	 * @param response
	 */
	public Object add(HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/xml;charset=UTF-8");
		//获取用户信息
		String loginType = (String) request.getSession().getAttribute(
				Constant.KEY_LOGIN_USER_TYPE);
		//机构用户
		if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
			sUser = (SysUser) request.getSession().getAttribute(
					Constant.KEY_LOGIN_USER);

		} else {//教师用户
			teacher = (TbBizJzgjbxx) request.getSession().getAttribute(
					Constant.KEY_LOGIN_USER);
		}
		//获取机构信息 通过机构代码
		InstitutionsInfoDto institution = institutionsManageExt
				.getInstitution(sUser.getUserName());
		teacherInfo.setSfzjlx(shenfen);
		teacherInfo.setSfzjh(tCNo);
		teacherInfo.setXm(tName);
		teacherInfo.setXb(sex);
		teacherInfo.setCsrq(birthday);
		teacherAccountManageExt.add(teacherInfo, institution);
		return doSearch(request);

	}

	/**
	 * 跳到编辑页面
	 */
	public void toEdit(HttpServletRequest request, HttpServletResponse response) {

		try {
			JSONObject json = new JSONObject();
			TeacherAccountInfoDto tbBizJzgjbxx = teacherAccountManageExt
					.get(jsId);
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("tbBizJzgjbxx", tbBizJzgjbxx);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("TeacherAccountManageAction toEdit() is error!", e);
		}
	}

	/**
	 * 编辑
	 */
	public Object edit(HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/xml;charset=UTF-8");
		teacherInfo.setJsid(jsId);
		teacherInfo.setSfzjlx(shenfen);
		teacherInfo.setSfzjh(tCNo);
		teacherInfo.setXm(tName);
		teacherInfo.setXb(sex);
		teacherInfo.setCsrq(birthday);
		//teacherAccountManageExt.edit(teacherInfo);
		//获取用户信息
		String loginType = (String) request.getSession().getAttribute(
				Constant.KEY_LOGIN_USER_TYPE);
		//机构用户
		if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
			sUser = (SysUser) request.getSession().getAttribute(
					Constant.KEY_LOGIN_USER);

		} else {//教师用户
			teacher = (TbBizJzgjbxx) request.getSession().getAttribute(
					Constant.KEY_LOGIN_USER);
		}
		String editName = sUser.getUserName();//当前修改人用户名
		int result = 0;
		try {
			result = teacherAccountManageExt.edit(teacherInfo, editName);
			if (result == 0) {
				message = "编辑成功";
			}
		} catch (Exception e) {
			if (result == 1) {
				message = "编辑失败";
			}
			e.printStackTrace();
		}
		return doSearch(request);
	}

	public Object dEduId() {
		String disCode = "110102000000";
		String sequenceSJ = teacherAccountManageExt.getEduId("", disCode);
		System.out.println("sdfadfasdfasfasfas" + sequenceSJ);
		return null;
	}

	/**
	 * 校验添加的证件号码
	 */
	public void checkAddHm(HttpServletRequest request,
			HttpServletResponse response) {

		try {
			JSONObject json = new JSONObject();
			TeacherAccountInfoDto tbBizJzgjbxx = teacherAccountManageExt
					.checkAddHm(shenfen, tCNo);
			int result = 0;
			if (tbBizJzgjbxx == null) {
				result = 1;
			}
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("isValid", result);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("TeacherAccountManageAction toEdit() is error!", e);
		}
	}

	/**
	 * 校验编辑的证件号码
	 */
	public void checkEditHm(HttpServletRequest request,
			HttpServletResponse response) {

		try {
			JSONObject json = new JSONObject();
			TeacherAccountInfoDto tbBizJzgjbxx = teacherAccountManageExt
					.checkEditHm(shenfen, tCNo, jsId);
			int result = 0;
			if (tbBizJzgjbxx == null) {
				result = 1;
			}
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("isValid", result);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("TeacherAccountManageAction toEdit() is error!", e);
		}
	}
}
