package com.becom.jspt2016.webapp.action.schoolManager;

import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.dto.TeacherChangeDto;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.service.ext.ITeacherChangeExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 * 学校模块
 * <p>
 * 教师变动管理
 * @author   fmx
 */

public class TeacherChangeConfirmAction extends PageAction {
	protected Logger logger = Logger.getLogger(this.getClass());
	@Spring
	private ITeacherChangeExt teacherChangeExt;
	public SysUser sUser;
	public TbBizJzgjbxx teacher;
	public String userType;
	
	////查询条件start////
	public String eduIdQuery;
	public String cardNoQuery;
	public String nameQuery;
	public String inNameQuery;
	public String statusQuery;
	
	////添加、编辑条件start////
	public TeacherChangeDto teacherChangeDto = new TeacherChangeDto();
	public String[] ck;
	
	///审核条件
	public String idT;
	public String statusT;
	public String statusContent;

	/**
	 * 
	 * 搜索
	 * <p>
	 * 教师账号列表展示
	 */
	@DefaultAction
	public Object doSearch(HttpServletRequest request) {
		Map<String, Object> param = new HashMap<String, Object>();
		
		if (eduIdQuery != null && !"".equals(eduIdQuery.replaceAll(" ", ""))) {
			param.put("eduIdQuery", eduIdQuery.replaceAll(" ", ""));
		}
		if (cardNoQuery != null && !"".equals(cardNoQuery.replaceAll(" ", ""))) {
			param.put("cardNoQuery", cardNoQuery.replaceAll(" ", ""));
		}
		if (nameQuery != null && !"".equals(nameQuery.replaceAll(" ", ""))) {
			param.put("nameQuery", nameQuery.replaceAll(" ", ""));
		}
		if (inNameQuery != null && !"".equals(inNameQuery.replaceAll(" ", ""))) {
			param.put("inNameQuery", inNameQuery);
		}
		if (statusQuery != null && !"".equals(statusQuery.replaceAll(" ", ""))) {
			param.put("statusQuery", statusQuery);
		}
		
		pageObj = teacherChangeExt.doSearchC(param, pageNo, pageSize);
		return "teacherConfirm.jsp";
	}

	public Object subAll(HttpServletRequest request, HttpServletResponse response) {
		int result = 0;
		
		
		//获取用户信息
		String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
		//机构用户
		if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
			sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);

		} else {//教师用户
			teacher = (TbBizJzgjbxx) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
		}
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("ids", ck);
		param.put("status", 2);
		param.put("spUser", sUser.getUserName());
		param.put("spUid", sUser.getUserId());
		param.put("spTime", new Timestamp(System.currentTimeMillis()));
		
		try {
			result = teacherChangeExt.subAll(param);;
			if (result == 0) {
				message = "审批成功";
			}
		} catch (Exception e) {
			if (result == 1) {
				message = "审批失败";
			}
			e.printStackTrace();
		}
		return doSearch(request);
	}

	public Object sub(HttpServletRequest request, HttpServletResponse response) {
		//获取用户信息
		String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
		//机构用户
		if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
			sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);

		} else {//教师用户
			teacher = (TbBizJzgjbxx) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
		}
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("id", idT);
		param.put("status", statusT);
		param.put("spUser", sUser.getUserName());
		param.put("spUid", sUser.getUserId());
		param.put("spTime", new Timestamp(System.currentTimeMillis()));
		
		teacherChangeExt.sub(param);
		return doSearch(request);
	}

}
