/**
 * YearTestImportAction.java
 * com.becom.jspt2016.webapp.action.schoolManager
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.webapp.action.schoolManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.nestframework.action.FileItem;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbBizJzgsdcfxx;
import com.becom.jspt2016.model.TbBizJzgsdkhxx;
import com.becom.jspt2016.service.ITbBizJzgjbxxManager;
import com.becom.jspt2016.service.ext.IExportExt;
import com.becom.jspt2016.service.ext.ILoginUserManagerExt;
import com.becom.jspt2016.service.ext.ITbBizJzgsdxxManagerExt;
import com.becom.jspt2016.webapp.action.BaseAction;

/**
 * 师德信息导入
 * <p>
 * @author   fmx
 * @Date	 2016年10月31日
 */
public class ShiDeInfoImportAction extends BaseAction {

	protected Logger logger = Logger.getLogger(this.getClass());
	@Spring
	private IExportExt exportExt;
	@Spring
	public ILoginUserManagerExt loginUserManagerExt;
	@Spring
	private ITbBizJzgsdxxManagerExt tbBizJzgsdxxManagerExt;

	@Spring
	private ITbBizJzgjbxxManager tbBizJzgjbxxManager;

	public FileItem fileImport;

	@DefaultAction
	public Object importInfo(HttpServletRequest request, HttpServletResponse response) {
		//获取用户信息
		String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
		//机构用户
		if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
			SysUser sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);

			try {
				if (fileImport != null && fileImport.isUploaded()) {
					InputStream in = fileImport.getInputStream();
					readXls(in, sUser);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return null;
	}

	public void importFile(HttpServletRequest request, HttpServletResponse response) {
		//获取用户信息
		String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
		Map<String, String> result = new HashMap<String, String>();
		//机构用户
		if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
			SysUser sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);

			try {
				if (fileImport != null && fileImport.isUploaded()) {
					InputStream in = fileImport.getInputStream();
					readXls(in, sUser);
				}

				result.put("status", "1");
				result.put("msg", "导入成功！");
			} catch (Exception e) {
				result.put("status", "0");
				result.put("msg", "导入失败！");
				e.printStackTrace();
			}
		} else {
			result.put("status", "0");
			result.put("msg", "导入失败！");
		}

		try {
			JSONObject json = new JSONObject();
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("result", result);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void readXls(InputStream is, SysUser user) throws IOException, InvalidFormatException {
		Workbook hssfWorkbook = WorkbookFactory.create(is);

		for (int numSheet = 0; numSheet < hssfWorkbook.getNumberOfSheets(); numSheet++) {
			Sheet hssfSheet = hssfWorkbook.getSheetAt(numSheet);
			if (hssfSheet == null) {
				continue;
			}

			if (numSheet == 0) { ////师德考核信息
				for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
					Row hssfRow = hssfSheet.getRow(rowNum);
					if (hssfRow != null) {

						String eduId = getValue(hssfRow.getCell(1));//教育ID

						if (eduId != null && "".equals(eduId.trim())) {
							break;
						}

						TbBizJzgjbxx teacher = loginUserManagerExt.findTeaByEduId(eduId);
						TbBizJzgjbxx reteacher = tbBizJzgjbxxManager.get(teacher.getJsid());
						TbBizJzgsdkhxx khxx = new TbBizJzgsdkhxx();
						khxx.setTbBizJzgjbxx(reteacher);
						khxx.setSdkhny(getValue(hssfRow.getCell(3)));
						khxx.setShsj(new Date());
						khxx.setCjsj(new Date());
						khxx.setSdkhjl(getValue(hssfRow.getCell(4)).split("-")[0]);
						khxx.setSdkhdwmc(getValue(hssfRow.getCell(5)));
						khxx.setCjr(String.valueOf(user.getUserId()));//注入当前机构用户ID
						tbBizJzgsdxxManagerExt.addTeacherMorality(khxx, null);
					} else {
						break;
					}
				}
			} else if (numSheet == 1) { ////师德处罚信息
				for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
					Row hssfRow = hssfSheet.getRow(rowNum);
					if (hssfRow != null) {

						String eduId = getValue(hssfRow.getCell(1));//教育ID

						if (eduId != null && "".equals(eduId.trim())) {
							break;
						}

						TbBizJzgjbxx teacher = loginUserManagerExt.findTeaByEduId(eduId);
						TbBizJzgjbxx reteacher = tbBizJzgjbxxManager.get(teacher.getJsid());
						TbBizJzgsdcfxx cfxx = new TbBizJzgsdcfxx();
						cfxx.setTbBizJzgjbxx(reteacher);

						Date ccfsrq = HSSFDateUtil.getJavaDate(hssfRow.getCell(5).getNumericCellValue());
						cfxx.setCcfsrq(handleDate(ccfsrq));//处罚发生日期
						cfxx.setShsj(new Date());
						cfxx.setCjsj(new Date());
						cfxx.setCflb(getValue(hssfRow.getCell(3)).split("-")[0]);//处罚类别
						cfxx.setCfyy(getValue(hssfRow.getCell(4)).split("-")[0]);
						cfxx.setCjr(String.valueOf(user.getUserId()));
						tbBizJzgsdxxManagerExt.addPunishmentInfo(cfxx);
					} else {
						break;
					}
				}
			}

		}
	}

	protected String handleDate(Date datestr) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

		return format.format(datestr);
	}

	protected String handleDate(String datestr) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		if (datestr != null) {
			String str = datestr;
			if (str.contains("/")) {
				SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");
				try {
					return format.format(fmt.parse(str));
				} catch (ParseException e) {
					return str;
				}
			} else if (str.contains("-")) {
				return str;
			} else {
				SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
				try {
					return format.format(fmt.parse(str));
				} catch (ParseException e) {
					return str;
				}
			}
		}
		return datestr;
	}

	//获取单元格值
	protected String getValue(Cell cell) {
		String result = "";
		if (cell != null) {
			int type = cell.getCellType();
			switch (type) {
			case Cell.CELL_TYPE_STRING:
				result = cell.getStringCellValue();
				break;
			case Cell.CELL_TYPE_NUMERIC:
				result = Double.valueOf(cell.getNumericCellValue()).intValue() + "";
				break;
			default:
				result = "";
			}
		}
		return result;
	}
}
