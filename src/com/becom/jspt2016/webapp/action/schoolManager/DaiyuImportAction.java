/**
 * DaiyuImportAction.java
 * com.becom.jspt2016.webapp.action.schoolManager
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.webapp.action.schoolManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.nestframework.action.FileItem;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbdyxx;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.service.ext.IBasicTreatManagerExt;
import com.becom.jspt2016.service.ext.IExportExt;
import com.becom.jspt2016.service.ext.ILoginUserManagerExt;
import com.becom.jspt2016.webapp.action.BaseAction;

/**
 * 基本待遇信息导入
 * <p>
 * @author   fmx
 * @Date	 2016年11月12日
 */
public class DaiyuImportAction extends BaseAction {

	protected Logger logger = Logger.getLogger(this.getClass());
	@Spring
	private IExportExt exportExt;
	@Spring
	public ILoginUserManagerExt loginUserManagerExt;

	@Spring
	public IBasicTreatManagerExt basictreatmanagerExt;

	public FileItem fileImport;

	@DefaultAction
	public Object importInfo(HttpServletRequest request, HttpServletResponse response) {
		//获取用户信息
		String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
		//机构用户
		if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
			SysUser sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);

			try {
				if (fileImport != null && fileImport.isUploaded()) {
					InputStream in = fileImport.getInputStream();
					readXls(in, sUser);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return null;
	}

	public void importFile(HttpServletRequest request, HttpServletResponse response) {
		//获取用户信息
		String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
		Map<String, String> result = new HashMap<String, String>();
		//机构用户
		if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
			SysUser sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);

			try {
				if (fileImport != null && fileImport.isUploaded()) {
					InputStream in = fileImport.getInputStream();
					readXls(in, sUser);
				}

				result.put("status", "1");
				result.put("msg", "导入成功！");
			} catch (Exception e) {
				result.put("status", "0");
				result.put("msg", "导入失败！");
				e.printStackTrace();
			}
		} else {
			result.put("status", "0");
			result.put("msg", "导入失败！");
		}

		try {
			JSONObject json = new JSONObject();
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("result", result);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void readXls(InputStream is, SysUser user) throws IOException, InvalidFormatException {
		Workbook hssfWorkbook = WorkbookFactory.create(is);

		for (int numSheet = 0; numSheet < hssfWorkbook.getNumberOfSheets(); numSheet++) {
			Sheet hssfSheet = hssfWorkbook.getSheetAt(numSheet);
			if (hssfSheet == null) {
				continue;
			}

			if (numSheet == 0) { ////师德考核信息
				for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
					Row hssfRow = hssfSheet.getRow(rowNum);
					if (hssfRow != null) {

						String eduId = getValue(hssfRow.getCell(1));//教育ID

						if (eduId != null && "".equals(eduId.trim())) {
							break;
						}

						TbBizJzgjbxx teacher = loginUserManagerExt.findTeaByEduId(eduId);

						TbBizJzgjbdyxx dy = new TbBizJzgjbdyxx();
						dy.setTbBizJzgjbxx(teacher);
						dy.setNd(getValue(hssfRow.getCell(3)));//年度
						dy.setNgzsr(getValue(hssfRow.getCell(4)));//年工资收入
						dy.setJbgz(getValue(hssfRow.getCell(5)));//基本工资
						dy.setJxgz(getValue(hssfRow.getCell(6)));//绩效工资
						dy.setXcjsshbz(getValue(hssfRow.getCell(7)));//乡村教师生活补助
						dy.setQt(getValue(hssfRow.getCell(8)));//其他
						String wxyj = getValue(hssfRow.getCell(9));
						String[] wxyjarr;
						if (wxyj.contains(",")) {
							wxyjarr = wxyj.split(",");
							String wxyjStr = "";
							for (int i = 0; i < wxyjarr.length; i++) {
								wxyjStr += "," + wxyjarr[i].split("-")[0];
							}
							dy.setWxyj(wxyjStr.substring(1));//五险一金
						} else if (wxyj.contains("，")) {
							wxyjarr = wxyj.split("，");
							String wxyjStr = "";
							for (int i = 0; i < wxyjarr.length; i++) {
								wxyjStr += "," + wxyjarr[i].split("-")[0];
							}
							dy.setWxyj(wxyjStr.substring(1));//五险一金
						} else {
							dy.setWxyj(getValue(hssfRow.getCell(9)).split("-")[0]);//五险一金
						}
						dy.setTjjtbt("");//特教津贴补贴
						dy.setJtbt("");//其他津贴补贴
						dy.setShsj(new Date());//审核时间
						dy.setCjsj(new Date());//创建时间
						basictreatmanagerExt.SubAddpoint(dy);
					} else {
						break;
					}
				}
			}
		}
	}

	protected String handleDate(String datestr) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		if (datestr != null) {
			String str = datestr;
			if (str.contains("/")) {
				SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");
				try {
					return format.format(fmt.parse(str));
				} catch (ParseException e) {
					return str;
				}
			} else if (str.contains("-")) {
				return str;
			} else {
				SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
				try {
					return format.format(fmt.parse(str));
				} catch (ParseException e) {
					return str;
				}
			}
		}
		return datestr;
	}

	//获取单元格值
	protected String getValue(Cell cell) {
		String result = "";
		if (cell != null) {
			int type = cell.getCellType();
			switch (type) {
			case Cell.CELL_TYPE_STRING:
				result = cell.getStringCellValue();
				break;
			case Cell.CELL_TYPE_NUMERIC:
				result = Double.valueOf(cell.getNumericCellValue()).intValue() + "";
				break;
			default:
				result = "";
			}
		}
		return result;
	}
}
