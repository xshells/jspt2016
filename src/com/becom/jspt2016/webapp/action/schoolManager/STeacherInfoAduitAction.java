/**
 * STeacherInfoAduitAction.java
 * com.becom.jspt2016.webapp.action.schoolManager
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webapp.action.schoolManager;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.dto.InstitutionsInfoDto;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.service.ext.IInstitutionsManageExt;
import com.becom.jspt2016.service.ext.ISTeacherInfoAduitExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 * 教师信息审核
 * <p>
 * 学校机构
 * @author   kuangxiang
 * @Date	 2016年10月27日
 */

public class STeacherInfoAduitAction extends PageAction {
	/**
	 * 机构用户
	 */
	public SysUser sUser;

	/**
	 * 老师用户
	 */
	public TbBizJzgjbxx teacher;
	/**
	 * 教育Id
	 */
	public String eduIdQuery;
	/**
	 * 身份证号
	 */
	public String cardNoQuery;
	/**
	 * 老师姓名
	 */
	public String teacherNameQuery;
	/**
	 * 审核状态的
	 */
	public String aduitStutasQuery;
	public String userType;
	public String ssxd;
	@Spring
	private ISTeacherInfoAduitExt sTeacherInfoAduitExt;
	@Spring
	private IInstitutionsManageExt institutionsManageExt;
	/**
	 * 报送状态  也就是待审核
	 */
	private static final String WAITADUIT = "02";
	/**
	 * 11-学校（机构）审核通过
	 */
	private static final String SADUITYES = "11";
	/**
	 * 12-学校（机构）审核不通过
	 */
	private static final String SADUITNO = "12";
	/**
	 * 审核意见
	 */
	public String aduitOpinion;
	/**
	 * 审核是否通过   引用字典：TB_CFG_ZDB.JSXX_SHJG  0-审核中 1-通过 2-不通过 
	 */
	public String isAdopt;
	public String ck;

	/**
	 * 
	 * 搜索
	 * 
	 * 学校审核教师信息
	 * 1.是报送状态的教师
	 * 2.是本学校的教师
	 * 3.未删除的老师
	 * <p>
	 * 机构管理查询
	 */
	@DefaultAction
	public Object doSearch(HttpServletRequest request) {
		try {
			//获取用户信息
			String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
			//机构用户
			if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
				sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);

			} else {//教师用户
				teacher = (TbBizJzgjbxx) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
			}
			//用户类型 1：市级2：区级
			userType = sUser.getUserType();
			Map<String, Object> param = new HashMap<String, Object>();
			InstitutionsInfoDto institution = institutionsManageExt.getInstitution(sUser.getUserName());
			param.put("jgId", institution.getInstitutionId());
			if (eduIdQuery != null && !"".equals(eduIdQuery.replaceAll(" ", ""))) {
				param.put("eduIdQuery", eduIdQuery.replaceAll(" ", ""));
			}
			if (cardNoQuery != null && !"".equals(cardNoQuery.replaceAll(" ", ""))) {
				param.put("cardNoQuery", cardNoQuery.replaceAll(" ", ""));
			}
			if (teacherNameQuery != null && !"".equals(teacherNameQuery.replaceAll(" ", ""))) {
				param.put("teacherNameQuery", teacherNameQuery.replaceAll(" ", ""));
			}
			if (aduitStutasQuery != null && !"".equals(aduitStutasQuery.replaceAll(" ", ""))) {
				String[] status = { aduitStutasQuery.replaceAll(" ", "") };
				param.put("aduitStutasQuery", status);
			} else {
				String[] status = { WAITADUIT, SADUITYES, SADUITNO };
				param.put("aduitStutasQuery", status);//待审核,学校审核通过，学校审核不通过
			}

			pageObj = sTeacherInfoAduitExt.doSearch(param, pageNo, pageSize);
			ssxd = institution.getAffiliatedSchool();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "sTeacherInfoAduit.jsp";
	}

	/**
	 * 审核功能
	 * <p>
	 *区级审核
	 */
	public Object aduit(HttpServletRequest request) {
		/*
		try {
			if (isAdopt != null && !"".equals(isAdopt)) {
				isAdopt = new String(request.getParameter("isAdopt").getBytes("iso-8859-1"), "utf-8");
			}
			if (aduitOpinion != null && !"".equals(aduitOpinion)) {
				aduitOpinion = new String(request.getParameter("aduitOpinion").getBytes("iso-8859-1"), "utf-8");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		 */
		
		//获取用户信息
		String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
		//机构用户
		if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
			sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);

		} else {//教师用户
			teacher = (TbBizJzgjbxx) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
		}
		InstitutionsInfoDto institution = institutionsManageExt.getInstitution(sUser.getUserName());

		//获取Ip
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		ip = ip.equals("0:0:0:0:0:0:0:1") ? "127.0.0.1" : ip;
		String[] split = ck.split(",");
		sTeacherInfoAduitExt.aduit(split, isAdopt, aduitOpinion, institution, sUser, ip);
		/*int result = 0;
		try {
			if (result == 0) {
				message = "审核成功";
			}
		} catch (Exception e) {
			if (result == 1) {
				message = "审核失败";
			}
			e.printStackTrace();
		}*/
		return doSearch(request);
	}

	public static void main(String[] args) {
		String s = "70,68,69,73";
		String[] split = s.split(",");
		System.out.println("sdfasdfasdf+++++++:" + split.length);
	}

}
