/**
 * YearTestImportAction.java
 * com.becom.jspt2016.webapp.action.schoolManager
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.webapp.action.schoolManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.nestframework.action.FileItem;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbBizJzgndkhxx;
import com.becom.jspt2016.service.ext.ILoginUserManagerExt;
import com.becom.jspt2016.service.ext.IYearTestManagerExt;
import com.becom.jspt2016.webapp.action.BaseAction;

/**
 * 年度信息导入
 * <p>
 * @author   fmx
 * @Date	 2016年10月31日
 */
public class YearTestImportAction extends BaseAction {

	protected Logger logger = Logger.getLogger(this.getClass());
	@Spring
	public IYearTestManagerExt yeartestmanagerExt;//考核年度接口
	@Spring
	public ILoginUserManagerExt loginUserManagerExt;

	public FileItem fileImport;

	@DefaultAction
	public Object importInfo(HttpServletRequest request, HttpServletResponse response) {

		try {
			if (fileImport != null && fileImport.isUploaded()) {
				InputStream in = fileImport.getInputStream();
				readXls(in);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public void importFile(HttpServletRequest request, HttpServletResponse response) {
		//获取用户信息
		String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
		Map<String, String> result = new HashMap<String, String>();

		try {
			if (fileImport != null && fileImport.isUploaded()) {
				InputStream in = fileImport.getInputStream();
				readXls(in);
			}

			result.put("status", "1");
			result.put("msg", "导入成功！");
		} catch (Exception e) {
			result.put("status", "0");
			result.put("msg", "导入失败！");
		}

		try {
			JSONObject json = new JSONObject();
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("result", result);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void readXls(InputStream is) throws IOException, InvalidFormatException {
		Workbook hssfWorkbook = WorkbookFactory.create(is);

		for (int numSheet = 0; numSheet < hssfWorkbook.getNumberOfSheets(); numSheet++) {
			Sheet hssfSheet = hssfWorkbook.getSheetAt(numSheet);
			if (hssfSheet == null) {
				continue;
			}

			if (numSheet == 0) {
				for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
					Row hssfRow = hssfSheet.getRow(rowNum);
					if (hssfRow != null) {

						String eduId = getValue(hssfRow.getCell(1));//教育ID
						if (eduId != null && "".equals(eduId.trim())) {
							break;
						}

						TbBizJzgjbxx teacher = loginUserManagerExt.findTeaByEduId(eduId);

						TbBizJzgndkhxx kh = new TbBizJzgndkhxx();
						kh.setTbBizJzgjbxx(teacher);//教师基本信息
						kh.setKhnd(getValue(hssfRow.getCell(3))); //考核年度      
						kh.setKhjg(getValue(hssfRow.getCell(5)).split("-")[0]); //考核结果      
						kh.setKhdwmc(getValue(hssfRow.getCell(4))); //考核单位名称
						kh.setShsj(new Date());
						kh.setCjsj(new Date());
						yeartestmanagerExt.SubAddYearTest(kh);
					} else {
						break;
					}
				}
			}
		}
	}

	//获取单元格值
	protected String getValue(Cell cell) {
		String result = "";
		if (cell != null) {
			int type = cell.getCellType();
			switch (type) {
			case Cell.CELL_TYPE_STRING:
				result = cell.getStringCellValue();
				break;
			case Cell.CELL_TYPE_NUMERIC:
				result = Double.valueOf(cell.getNumericCellValue()).intValue() + "";
				break;
			default:
				result = "";
			}
		}
		return result;
	}
}
