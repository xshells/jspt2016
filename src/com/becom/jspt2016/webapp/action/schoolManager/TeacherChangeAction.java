/**
 * TeacherChangeAction.java
 * com.becom.jspt2016.webapp.action
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.webapp.action.schoolManager;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.dto.TeacherChangeDto;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbJzgxxsh;
import com.becom.jspt2016.service.ITbBizJzgjbxxManager;
import com.becom.jspt2016.service.ext.ITbJzgxxshManagerExt;
import com.becom.jspt2016.service.ext.ITeacherChangeExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 * 学校模块
 * <p>
 * 教师变动管理
 * @author   fmx
 */
public class TeacherChangeAction extends PageAction {
	protected Logger logger = Logger.getLogger(this.getClass());
	@Spring
	private ITeacherChangeExt teacherChangeExt;
	@Spring
	private ITbBizJzgjbxxManager tbBizJzgjbxxManager;
	@Spring
	private ITbJzgxxshManagerExt tbJzgxxshManagerExt;
	public SysUser sUser;
	public TbBizJzgjbxx teacher;
	public String userType;
	public String jsid;

	////查询条件start////
	public String eduIdQuery;
	public String cardNoQuery;
	public String nameQuery;
	public String outNameQuery;
	public String statusQuery;

	////添加、编辑条件start////
	public TeacherChangeDto teacherChangeDto = new TeacherChangeDto();
	public String[] ck;

	///加载老师信息条件
	public String cardNoT;
	public String idT;

	/**
	 * 
	 * 搜索
	 * <p>
	 * 教师账号列表展示
	 */
	@DefaultAction
	public Object doSearch(HttpServletRequest request) {
		Map<String, Object> param = new HashMap<String, Object>();

		//获取用户信息
		String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
		//机构用户
		if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
			sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
			param.put("xxjgid", sUser.getId());
		}
		if (eduIdQuery != null && !"".equals(eduIdQuery.replaceAll(" ", ""))) {
			param.put("eduIdQuery", eduIdQuery.replaceAll(" ", ""));
		}
		if (cardNoQuery != null && !"".equals(cardNoQuery.replaceAll(" ", ""))) {
			param.put("cardNoQuery", cardNoQuery.replaceAll(" ", ""));
		}
		if (nameQuery != null && !"".equals(nameQuery.replaceAll(" ", ""))) {
			param.put("nameQuery", nameQuery.replaceAll(" ", ""));
		}
		if (outNameQuery != null && !"".equals(outNameQuery.replaceAll(" ", ""))) {
			param.put("outNameQuery", outNameQuery);
		}
		if (statusQuery != null && !"".equals(statusQuery.replaceAll(" ", ""))) {
			param.put("statusQuery", statusQuery);
		}

		pageObj = teacherChangeExt.doSearch(param, pageNo, pageSize);
		return "teacherChange.jsp";
	}

	/**
	 * 
	 * 删除
	 * <p>
	 * 删除教师账号
	 *
	 */
	public Object delete(HttpServletRequest request, HttpServletResponse response) {

		int result = 0;
		try {
			result = teacherChangeExt.delete(ck);
			;
			if (result == 0) {
				message = "删除成功";
			}
		} catch (Exception e) {
			if (result == 1) {
				message = "删除失败";
			}
			e.printStackTrace();
		}
		return doSearch(request);
	}

	/**
	 * 添加
	 * <p>
	 * @param request
	 * @param response
	 */
	public Object add(HttpServletRequest request, HttpServletResponse response) {
		//获取用户信息
		String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
		//机构用户
		if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
			sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);

		} else {//教师用户
			teacher = (TbBizJzgjbxx) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
		}

		teacherChangeDto.setInId(Integer.parseInt(sUser.getId()));
		teacherChangeDto.setSqrid(sUser.getUserId());
		teacherChangeDto.setSqrname(sUser.getUserName());
		//		teacherChangeDto.setInId(5522);
		teacherChangeExt.add(teacherChangeDto);
		return doSearch(request);

	}

	/**
	 * 跳到编辑页面
	 */
	public void toEdit(HttpServletRequest request, HttpServletResponse response) {

		try {
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("id", idT);
			TeacherChangeDto dto = teacherChangeExt.get(param);
			JSONObject json = new JSONObject();
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("obj", dto);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("teacherChangeExt toEdit() is error!", e);
		}
	}

	/**
	 * 编辑
	 */
	public Object edit(HttpServletRequest request, HttpServletResponse response) {
		int result = 0;
		try {
			result = teacherChangeExt.edit(teacherChangeDto);
			if (result == 0) {
				message = "编辑成功";
			}
		} catch (Exception e) {
			if (result == 1) {
				message = "编辑失败";
			}
			e.printStackTrace();
		}
		return doSearch(request);
	}

	/**
	 * 查询教师信息
	 * @param request
	 * @param response
	 */
	public void getTeacher(HttpServletRequest request, HttpServletResponse response) {
		try {
			JSONObject json = new JSONObject();
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			Map<String, Object> teaMap = teacherChangeExt.getTeacher(cardNoT);
			long tid = Long.parseLong(teaMap.get("tId").toString());
			String ssxd = tbBizJzgjbxxManager.get(tid).getTbXxjgb().getSsxd();
			teaMap.put("ssxd", ssxd);
			json.put("tmap", teaMap);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("TeacherAccountManageAction toEdit() is error!", e);
		}
	}

	/**
	 * 获取审核状态
	 *
	 */
	public void getShzt(HttpServletRequest request, HttpServletResponse response) {
		try {
			JSONObject json = new JSONObject();
			TbJzgxxsh xxsh = tbJzgxxshManagerExt.query(Long.valueOf(jsid));
			if (xxsh == null) {
				xxsh = new TbJzgxxsh();
			}
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("tbJzgxxsh", xxsh);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("TeacherLearnAction toEdit() is error!", e);
		}

	}
}
