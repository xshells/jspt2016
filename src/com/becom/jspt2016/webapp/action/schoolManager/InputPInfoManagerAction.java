/**
 * InputPInfoManagerAction.java
 * com.becom.jspt2016.webapp.action.schoolManager
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webapp.action.schoolManager;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.dto.CheckShDto;
import com.becom.jspt2016.dto.InstitutionsInfoDto;
import com.becom.jspt2016.model.SysFunc;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.model.TbJzgxxsh;
import com.becom.jspt2016.service.ITbBizJzgjbxxManager;
import com.becom.jspt2016.service.ext.IInputPInfoManagerExt;
import com.becom.jspt2016.service.ext.IInstitutionsManageExt;
import com.becom.jspt2016.service.ext.ILoginUserManagerExt;
import com.becom.jspt2016.service.ext.ITbBizJzgjbxxManagerExt;
import com.becom.jspt2016.service.ext.ITbJzgxxshManagerExt;
import com.becom.jspt2016.webapp.action.PageAction;

/**
 * 个人信息录入
 * <p>
 * 学校机构的个人信息录入
 * @author   kuangxiang
 * @Date	 2016年10月24日
 */

public class InputPInfoManagerAction extends PageAction {
	protected Logger logger = Logger.getLogger(this.getClass());

	@Spring
	public ILoginUserManagerExt loginUserManagerExt;
	/**
	 * 老师用户
	 */
	public TbBizJzgjbxx teacher;
	/**
	 * 机构用户
	 */
	public SysUser sUser;
	/**
	 * 教育Id 查询使用
	 */
	public String eduIdQuery;
	/**
	 * 证件号码 查询使用
	 */
	public String cardNOQuery;
	/**
	 * 老师名字 查询使用
	 */
	public String teacherNameQuery;
	/**
	 * 进本校年月开始 查询使用
	 */
	public String comeSchoolDay1Query;
	/**
	 * 进本校年月结束 查询使用
	 */
	public String comeSchoolDay2Query;
	/**
	 * 教师来源 查询使用
	 */
	public String teacherSourceQuery;
	/**
	 * 教职工类型 查询使用
	 */
	public String teacherTypeQuery;
	/**
	 * 人员状态 查询使用
	 */
	public String personStatusQuery;
	/**
	 * 审核状态 查询使用
	 */
	public String reviewStatusQuery;
	/**
	 * 是否在编 查询使用
	 */
	public String isSeriesQuery;

	public CheckShDto shDto;

	public String ssxd;
	@Spring
	public IInputPInfoManagerExt inputPInfoManagerExt;
	@Spring
	private IInstitutionsManageExt institutionsManageExt;
	@Spring
	private ITbBizJzgjbxxManager tbBizJzgjbxxManager;
	@Spring
	private ITbBizJzgjbxxManagerExt tbBizJzgjbxxManagerExt;

	@Spring
	private ITbJzgxxshManagerExt tbJzgxxshManagerExt;

	public String[] ck;

	/**
	 * 获取tree使用
	 */
	public List<SysFunc> funcList;
	public String tid;

	/**
	 * 
	 * 搜索
	 * <p>
	 * 教师账号列表展示
	 */
	@DefaultAction
	public Object doSearch(HttpServletRequest request) {

		try {
			//获取用户信息
			String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
			//机构用户
			if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
				sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);

			} else {//教师用户
				teacher = (TbBizJzgjbxx) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
			}

			Map<String, Object> param = new HashMap<String, Object>();
			InstitutionsInfoDto institution = institutionsManageExt.getInstitution(sUser.getUserName());
			param.put("jgId", institution.getInstitutionId());//机构id
			if (eduIdQuery != null && !"".equals(eduIdQuery.replaceAll(" ", ""))) {
				param.put("eduIdQuery", eduIdQuery.replaceAll(" ", ""));
			}
			if (cardNOQuery != null && !"".equals(cardNOQuery.replaceAll(" ", ""))) {
				param.put("cardNOQuery", cardNOQuery.replaceAll(" ", ""));
			}
			if (teacherNameQuery != null && !"".equals(teacherNameQuery.replaceAll(" ", ""))) {
				param.put("teacherNameQuery", teacherNameQuery.replaceAll(" ", ""));
			}
			if (comeSchoolDay1Query != null && !"".equals(comeSchoolDay1Query.replaceAll(" ", ""))) {
				param.put("comeSchoolDay1Query", comeSchoolDay1Query.replaceAll(" ", ""));
			}
			if (comeSchoolDay2Query != null && !"".equals(comeSchoolDay2Query.replaceAll(" ", ""))) {
				param.put("comeSchoolDay2Query", comeSchoolDay2Query.replaceAll(" ", ""));
			}
			if (teacherSourceQuery != null && !"".equals(teacherSourceQuery.replaceAll(" ", ""))) {
				param.put("teacherSourceQuery", teacherSourceQuery.replaceAll(" ", ""));
			}
			if (teacherTypeQuery != null && !"".equals(teacherTypeQuery.replaceAll(" ", ""))) {
				param.put("teacherTypeQuery", teacherTypeQuery.replaceAll(" ", ""));
			}
			if (personStatusQuery != null && !"".equals(personStatusQuery.replaceAll(" ", ""))) {
				param.put("personStatusQuery", personStatusQuery.replaceAll(" ", ""));
			}
			if (reviewStatusQuery != null && !"".equals(reviewStatusQuery.replaceAll(" ", ""))) {
				param.put("reviewStatusQuery", reviewStatusQuery.replaceAll(" ", ""));
			}
			if (isSeriesQuery != null && !"".equals(isSeriesQuery.replaceAll(" ", ""))) {
				param.put("isSeriesQuery", isSeriesQuery.replaceAll(" ", ""));
			}
			pageObj = inputPInfoManagerExt.doSearch(param, pageNo, pageSize);
			ssxd = institution.getAffiliatedSchool();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "inputPInfoL.jsp";
	}

	/**
	 * 删除
	 * <p>
	 *删除个人信息
	 *
	 * @param request
	 * @param response
	 * @return TODO(这里描述每个参数,如果有返回值描述返回值,如果有异常描述异常)
	 */
	public Object delete(HttpServletRequest request, HttpServletResponse response) {

		int result = 0;
		try {
			result = inputPInfoManagerExt.delete(ck);
			if (result == 0) {
				message = "删除成功";
			}
		} catch (Exception e) {
			if (result == 1) {
				message = "删除失败";
			}
			e.printStackTrace();
		}
		return doSearch(request);
	}

	/**
	 * 获取审核状态
	 * @param request
	 * @param response
	 * @return
	 */
	public void shStatus(HttpServletRequest request, HttpServletResponse response) {

		String jsid = request.getParameter("jsId");
		if (jsid != null && !"".equals(jsid)) {
			jsid = jsid.substring(0, jsid.length() - 1);
		}
		StringBuffer sb = new StringBuffer();

		String[] ids = jsid.split(",");
		for (int i = 0; i < ids.length; i++) {
			TbJzgxxsh sdf = tbJzgxxshManagerExt.query(Long.parseLong(ids[i]));

			if (sdf == null) {
				continue;
			}
			String xxshzt = sdf.getShzt();
			if (xxshzt != null && !"".equals(xxshzt) && !"01".equals(xxshzt) && !"12".equals(xxshzt)
					&& !"22".equals(xxshzt)) {
				sb.append("序号：" + ids[i]).append(",");
			}
		}

		String jsonStr = sb.toString().trim();

		if (jsonStr.endsWith(",")) {
			jsonStr.substring(0, jsonStr.length() - 1);
		}

		try {
			JSONObject json = new JSONObject();
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("result", jsonStr);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("teacherChangeExt toEdit() is error!", e);
		}
	}

	/**
	 * 判断是否打开
	 * @param request
	 * @param response
	 */
	public void isHaveTea(HttpServletRequest request, HttpServletResponse response) {

		String result = "0";

		if ((getSessionTeacher(request) != null
				&& tbBizJzgjbxxManager.get(getSessionTeacher(request).getJsid()).getSfwcbtx() != null && tbBizJzgjbxxManager
				.get(getSessionTeacher(request).getJsid()).getSfwcbtx() == 1)
				|| "4001".equals(request.getParameter("infoid"))) {///4001是基本信息菜单的id
			result = "1";
		}

		try {
			JSONObject json = new JSONObject();
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("open", result);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("teacherChangeExt toEdit() is error!", e);
		}
	}

	/**
	 * 判断是否打开（编辑）
	 * @param request
	 * @param response
	 */
	public void checkTeacher(HttpServletRequest request, HttpServletResponse response) {

		String result = "0";
		if ((getSessionTeacher(request) != null && tbBizJzgjbxxManager.get(getSessionTeacher(request).getJsid()).getSfwcbtx() != null && tbBizJzgjbxxManager
				.get(getSessionTeacher(request).getJsid()).getSfwcbtx() == 1)
				|| "4001".equals(request.getParameter("infoid"))) {///4001是基本信息菜单的id
			result = "1";
		}

		try {
			JSONObject json = new JSONObject();
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("open", result);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("teacherChangeExt toEdit() is error!", e);
		}
	}

	/**
	 * 构建树
	 * @param request
	 * @param response
	 */
	public void getTree(HttpServletRequest request, HttpServletResponse response) {

		//获取用户信息
		String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
		//机构用户
		if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
			sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
		}
		TbBizJzgjbxx jbxxxx = null;
		if (tid != null) {
			TbBizJzgjbxx sessionTeacher = loginUserManagerExt.findTeaByJsId(tid);
			request.getSession().setAttribute(Constant.KEY_SESSION_TEACHER, sessionTeacher);
			request.getSession().setAttribute(Constant.SESSION_ACTION_TYPE, "edit");
			jbxxxx = tbBizJzgjbxxManager.get(Long.valueOf(tid));
			request.getSession().setAttribute(Constant.KEY_SESSION_TEACHER, jbxxxx);
			//			funcList = loginUserManagerExt.getMenuByEduId(sessionTeacher.getEduId());
		} else {///新增操作，要清除sessionTeacher
			request.getSession().removeAttribute(Constant.KEY_SESSION_TEACHER);
			request.getSession().setAttribute(Constant.SESSION_ACTION_TYPE, "add");
		}

		//		funcList = loginUserManagerExt.getMenuByJgId(sUser.getId());
		funcList = loginUserManagerExt.findFunAll();
		String jsonArray = "";
		if ((sUser.getId() == null && jbxxxx != null) || ("2".equals(sUser.getUserType()))) {
			jsonArray = assembleDate(funcList, jbxxxx.getTbXxjgb().getSsxd());
		} else {
			InstitutionsInfoDto jg = institutionsManageExt.get(Long.parseLong(sUser.getId()));
			jsonArray = assembleDate(funcList, jg.getAffiliatedSchool());
		}

		try {
			//组装数据

			JSONObject json = new JSONObject();
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("tree", jsonArray);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("teacherChangeExt toEdit() is error!", e);
		}
	}

	public String assembleDate(List<SysFunc> funcs, String ssxd) {
		SysFunc f = null;
		List<SysFunc> f1 = new ArrayList<SysFunc>();
		List<SysFunc> f2 = new ArrayList<SysFunc>();
		List<SysFunc> f3 = new ArrayList<SysFunc>();
		for (int i = 0; i < funcs.size(); i++) {
			f = funcs.get(i);
			if (("10".equals(ssxd) || "20".equals(ssxd)) && (f.getFuncId() == 4009 || f.getFuncId() == 4006)) {
				continue;
			}

			if ("10".equals(ssxd) && (f.getFuncId() == 4013 || f.getFuncId() == 4016)) {
				continue;
			}
			
			if ("60".equals(ssxd) && (f.getFuncId() == 4010 || f.getFuncId() == 4016)) {
				continue;
			}

			if ("1".equals(f.getFuncLevel().toString())) {//1
				f1.add(f);
				funcs.remove(i);
				i--;
			}
			if ("2".equals(f.getFuncLevel().toString())) {//2
				f2.add(f);
				funcs.remove(i);
				i--;
			}
			if ("3".equals(f.getFuncLevel().toString())) {//3
				f3.add(f);
				funcs.remove(i);
				i--;
			}
		}

		//拼接json
		StringBuilder sb = new StringBuilder("[");
		SysFunc sf1 = null;
		SysFunc sf2 = null;
		SysFunc sf3 = null;
		for (int i = 0; i < f1.size(); i++) {
			sf1 = f1.get(i);
			sb.append("{");//1
			sb.append("'id':" + sf1.getFuncId());
			sb.append(",'text':" + "'" + sf1.getFuncName() + "'");
			sb.append(",'children':[");//2
			for (int j = 0; j < f2.size(); j++) {
				sf2 = f2.get(j);
				if (sf1.getFuncId() == sf2.getParentFuncId()) {
					sb.append("{");
					sb.append("'id':" + sf2.getFuncId());
					sb.append(",'text':" + "'" + sf2.getFuncName() + "'");
					sb.append(",'url':" + "'" + sf2.getUrl() + "'");

					if (f3 != null && f3.size() > 0) {
						sb.append(",'children':[");
						for (int j2 = 0; j2 < f3.size(); j2++) {
							sf3 = f3.get(j2);
							if (sf2.getFuncId() == sf3.getParentFuncId()) {
								sb.append("{");
								sb.append("'id':" + sf3.getFuncId());
								sb.append(",'text':" + "'" + sf3.getFuncName() + "'");
								sb.append(",'url':" + "'" + sf3.getUrl() + "'");
								sb.append("},");
							}
						}
						if (regexCheckStr(sb.toString())) {
							sb.replace(sb.length() - 1, sb.length(), "");
						}
						sb.append("]");
					}
					sb.append("},");//
				}
			}
			if (regexCheckStr(sb.toString())) {
				sb.replace(sb.length() - 1, sb.length(), "");
			}
			sb.append("]");//2

			sb.append("},");//1end
		}

		if (regexCheckStr(sb.toString())) {
			sb.replace(sb.length() - 1, sb.length(), "");
		}
		sb.append("]");//最外一层
		return sb.toString();
	}

	private boolean regexCheckStr(String data) {
		if (data.endsWith(",")) {
			return true;
		}
		return false;
	}

	/**
	 * 院校报送全部
	 * @throws Exception 
	 */
	public void yxSubmittedAll(HttpServletRequest request) throws Exception {

		//获取用户信息
		String loginType = (String) request.getSession().getAttribute(Constant.KEY_LOGIN_USER_TYPE);
		//机构用户
		if (Constant.LOGIN_ORGANIAZTION.equals(loginType)) {
			sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);

		} else {//教师用户
			teacher = (TbBizJzgjbxx) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
		}

		Map<String, Object> param = new HashMap<String, Object>();
		InstitutionsInfoDto institution = institutionsManageExt.getInstitution(sUser.getUserName());
		param.put("jgId", institution.getInstitutionId());//机构id
		if (eduIdQuery != null && !"".equals(eduIdQuery.replaceAll(" ", ""))) {
			param.put("eduIdQuery", eduIdQuery.replaceAll(" ", ""));
		}
		if (cardNOQuery != null && !"".equals(cardNOQuery.replaceAll(" ", ""))) {
			param.put("cardNOQuery", cardNOQuery.replaceAll(" ", ""));
		}
		if (teacherNameQuery != null && !"".equals(teacherNameQuery.replaceAll(" ", ""))) {
			param.put("teacherNameQuery", teacherNameQuery.replaceAll(" ", ""));
		}
		if (comeSchoolDay1Query != null && !"".equals(comeSchoolDay1Query.replaceAll(" ", ""))) {
			param.put("comeSchoolDay1Query", comeSchoolDay1Query.replaceAll(" ", ""));
		}
		if (comeSchoolDay2Query != null && !"".equals(comeSchoolDay2Query.replaceAll(" ", ""))) {
			param.put("comeSchoolDay2Query", comeSchoolDay2Query.replaceAll(" ", ""));
		}
		if (teacherSourceQuery != null && !"".equals(teacherSourceQuery.replaceAll(" ", ""))) {
			param.put("teacherSourceQuery", teacherSourceQuery.replaceAll(" ", ""));
		}
		if (teacherTypeQuery != null && !"".equals(teacherTypeQuery.replaceAll(" ", ""))) {
			param.put("teacherTypeQuery", teacherTypeQuery.replaceAll(" ", ""));
		}
		if (personStatusQuery != null && !"".equals(personStatusQuery.replaceAll(" ", ""))) {
			param.put("personStatusQuery", personStatusQuery.replaceAll(" ", ""));
		}
		if (reviewStatusQuery != null && !"".equals(reviewStatusQuery.replaceAll(" ", ""))) {
			param.put("reviewStatusQuery", reviewStatusQuery.replaceAll(" ", ""));
		}
		if (isSeriesQuery != null && !"".equals(isSeriesQuery.replaceAll(" ", ""))) {
			param.put("isSeriesQuery", isSeriesQuery.replaceAll(" ", ""));
		}
		List<TbBizJzgjbxx> list = inputPInfoManagerExt.findAll(param);
		if (list == null) {
			throw new Exception("没有可以保送的数据!");
		}
		boolean cansubmit = true;
		for (TbBizJzgjbxx jb : list) {
			TbBizJzgjbxx jzgjbxx = tbBizJzgjbxxManager.get(jb.getJsid());
			shDto = tbBizJzgjbxxManagerExt.syssubmitted(jzgjbxx);
			if (shDto.getCount() == 0) {
				cansubmit = false;
				break;
			}
		}
		if (cansubmit) {
			SysUser sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
			long bsrid = sUser.getUserId();
			for (TbBizJzgjbxx jb : list) {
				TbBizJzgjbxx jzgjbxx = tbBizJzgjbxxManager.get(jb.getJsid());
				tbBizJzgjbxxManagerExt.yxsubmitInfo(jzgjbxx, bsrid);
			}
		} else {
			throw new Exception("信息不完善或已报送,不可报送!");
		}
	}

}
