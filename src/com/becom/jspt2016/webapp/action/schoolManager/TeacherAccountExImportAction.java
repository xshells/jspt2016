/**
 * TeacherAccountExImportAction.java
 * com.becom.jspt2016.webapp.action.schoolManager
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.webapp.action.schoolManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.nestframework.action.FileItem;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.dto.InstitutionsInfoDto;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.service.ext.IExportExt;
import com.becom.jspt2016.webapp.action.BaseAction;
import com.becom.jspt2016.webapp.util.ExcelUtil;
import com.becom.jspt2016.webapp.util.IDValidator;

/**
 * 教师账户导入导出
 * <p>
 * @author   fmx
 * @Date	 2016年10月27日
 */
public class TeacherAccountExImportAction extends BaseAction {
	@Spring
	private IExportExt exportExt;
	protected Logger logger = Logger.getLogger(this.getClass());

	public FileItem fileImport;

	/**
	 * 教职工Id
	 */
	public long jsId;
	/**
	 * 教育Id	
	 */
	public String tEduId;
	public String tEduIdQuery;
	/**
	 * 身份证号
	 */
	public String tCNo;
	public String tCNoQuery;
	/**
	 * 教师姓名
	 */
	public String tName;
	public String tNameQuery;
	/**
	 * 账号状态
	 */
	public String tAccountStatus;
	public String tAccountStatusQuery;

	@DefaultAction
	public Object importInfo(HttpServletRequest request, HttpServletResponse response) {

		SysUser sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
		String realPath = constantBean.get("tmpRealPath") + "/error.xlsx";
		InstitutionsInfoDto institution = exportExt.getInstitution(sUser.getId());

		try {
			if (fileImport != null && fileImport.isUploaded()) {
				InputStream in = fileImport.getInputStream();
				readXls(realPath, request, response, in, institution);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public void importFile(HttpServletRequest request, HttpServletResponse response) {
		SysUser sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
		String filename = "/Error" + System.currentTimeMillis() + ".xlsx";
		String realPath = constantBean.get("tmpRealPath") + filename;

		InstitutionsInfoDto institution = exportExt.getInstitution(sUser.getId());
		Map<String, String> result = new HashMap<String, String>();
		boolean flag = true;
		try {
			if (fileImport != null && fileImport.isUploaded()) {
				InputStream in = fileImport.getInputStream();
				flag = readXls(realPath, request, response, in, institution);
			}

			result.put("status", "1");
			result.put("msg", "导入成功！");
		} catch (Exception e) {
			e.printStackTrace();
			result.put("status", "0");
			result.put("msg", "导入失败！");
		}

		if (!flag) {
			result.put("status", "2");
			result.put("msg", "导入过程中有失败出现,点击确定下载失败记录！");
			result.put("file", filename);
		}

		try {
			JSONObject json = new JSONObject();
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("result", result);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected boolean readXls(String realPath, HttpServletRequest request, HttpServletResponse response,
			InputStream is, InstitutionsInfoDto institution) throws IOException, InvalidFormatException {
		/**
		 * 失败集合
		 */
		List<List<Object>> errorlist = new ArrayList<List<Object>>();

		/******** 数据解析开始 start ****************/
		Workbook hssfWorkbook = WorkbookFactory.create(is);
		for (int numSheet = 0; numSheet < hssfWorkbook.getNumberOfSheets(); numSheet++) {
			Sheet hssfSheet = hssfWorkbook.getSheetAt(numSheet);
			if (hssfSheet == null) {
				continue;
			}

			if (numSheet == 1) {//需求变为人员信息在第2个sheet里面
				for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
					Row hssfRow = hssfSheet.getRow(rowNum);
					if (hssfRow != null) {

						/******* 单行数据解析并入库 start *************/
						boolean isFail = false;
						String errorMsg = "";

						String cardNo = getValue(hssfRow.getCell(3));
						String name = getValue(hssfRow.getCell(1));
						if ("".equals(name.trim())) {
							break;
						}

						String cardType = getValue(hssfRow.getCell(2)).split("-")[0];
						String birth = "";
						if ("1".equals(cardType)) {
							if (cardNo == null || (cardNo.length() != 15 && cardNo.length() != 18)) {
								errorMsg = "教师身份证号位数不正确（应为15位或者18位）。";
								isFail = true;
							} else if (!IDValidator.isIDCard(cardNo)) {
								errorMsg = "教师身份证号格式不正确，请核对身份证号。";
								isFail = true;
							} else {
								birth = getBirthDate(cardNo);
							}
						}

						if (!isFail) {
							try {
								String jgId = exportExt.getTeacherInfo(cardNo, name);
								if (jgId != null && !"".equals(jgId.trim())) {
									isFail = true;
									if (jgId.equals(institution.getInstitutionId().toString())) {
										errorMsg = "教师在本单位已存在。";
									} else {
										errorMsg = "系统中已存在该教师，导入失败，请在系统中执行调入操作。";
									}
								}
							} catch (Exception e) {
								errorMsg = "验证老师信息异常，请联系系统管理员";
								isFail = true;
							}
						}

						if (isFail) {
							List<Object> failRow = new ArrayList<Object>();
							failRow.add(getValue(hssfRow.getCell(0)));//序号
							failRow.add(name);//教师姓名
							failRow.add(getValue(hssfRow.getCell(2)));//身份证类型
							failRow.add(cardNo);//身份证号
							failRow.add(errorMsg);//添加错误信息

							errorlist.add(failRow);//添加进失败集合
							continue;
						}

						Map<String, Object> map = new HashMap<String, Object>();
						map.put("jgId", institution.getInstitutionId());
						map.put("name", name);
						map.put("cardType", getValue(hssfRow.getCell(2)).split("-")[0]);
						map.put("cardNo", cardNo);
						map.put("birth", birth);

						exportExt.addTeacher(map, institution);//教师信息入库，解析一个入库一个
						/******* 单行数据解析并入库 end *************/
					} else {
						break;
					}
				}
			}
		}
		/******** 数据解析开始 end ****************/

		if (errorlist.size() > 0) {
			exportFailInfo(realPath, request, response, errorlist, "");
			errorlist.clear();//释放list中对象的引用
			return false;
		} else {
			return true;
		}
	}

	//获取生日
	protected static String getBirthDate(String cardNo) {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat formatNew = new SimpleDateFormat("yyyy-MM-dd");

		String birth = "";
		if (cardNo != null && cardNo.trim().length() == 18) {
			birth = cardNo.trim().substring(6, 14);
		} else if (cardNo != null && cardNo.trim().length() == 15) {
			birth = "19" + cardNo.trim().substring(6, 12);
		}

		if (!"".equals(birth)) {
			try {
				birth = formatNew.format(format.parse(birth));
			} catch (ParseException e) {
			}
		}

		return birth;
	}

	//获取单元格值
	protected String getValue(Cell cell) {
		String result = "";
		if (cell != null) {
			int type = cell.getCellType();
			switch (type) {
			case Cell.CELL_TYPE_STRING:
				result = cell.getStringCellValue();
				break;
			case Cell.CELL_TYPE_NUMERIC:
				result = Double.valueOf(cell.getNumericCellValue()).intValue() + "";
				break;
			default:
				result = "";
			}
		}
		return result;
	}

	/**
	 * 导出失败信息
	 * @param request
	 * @param contentList
	 * @param response
	 */
	protected void exportFailInfo(String realPath, HttpServletRequest request, HttpServletResponse response,
			List<List<Object>> contentList, String num) {
		String[] title = { "序号", "教师姓名", "身份证类型", "身份证号", "失败原因" };
		ExcelUtil excel = new ExcelUtil();
		excel.export2007(request, realPath, contentList, "导入失败人员记录信息" + num, title);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * 导出用户信息
	 * @param request
	 * @param response
	 * 
	 */
	public void export(HttpServletRequest request, HttpServletResponse response) {

		try {
			SysUser sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);

			Map<String, Object> param = new HashMap<String, Object>();
			if (tEduIdQuery != null && !"".equals(tEduIdQuery.replaceAll(" ", ""))) {
				param.put("tEduId", tEduIdQuery.replaceAll(" ", ""));
			}
			if (tCNoQuery != null && !"".equals(tCNoQuery.replaceAll(" ", ""))) {
				param.put("tCNo", tCNoQuery.replaceAll(" ", ""));
			}
			if (tNameQuery != null && !"".equals(tNameQuery.replaceAll(" ", ""))) {
				param.put("tName", tNameQuery.replaceAll(" ", ""));
			}
			if (tAccountStatusQuery != null && !"".equals(tAccountStatusQuery.replaceAll(" ", ""))) {
				param.put("tAccountStatus", tAccountStatusQuery);
			}
			//获取机构信息 通过机构代码
			InstitutionsInfoDto institution = exportExt.getInstitution(sUser.getId());
			param.put("xxjgId", institution.getInstitutionId());//机构id

			List<List<Object>> list = new ArrayList<List<Object>>();
			list = exportExt.getTeacher(param);
			String[] title = { "序号", "教育ID", "教师姓名*", "身份证类型*", "身份证号*" };

			ExcelUtil excel = new ExcelUtil();
			excel.export2007(request, response, list, "教师帐号信息", title);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
