/**
 * TeacherAccountManageAction.java
 * com.becom.jspt2016.webapp.action.school
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.webapp.action.schoolManager;

import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.annotation.DefaultAction;

import com.becom.jspt2016.common.Constant;
import com.becom.jspt2016.dto.DisCodesDto;
import com.becom.jspt2016.dto.InstitutionsInfoDto;
import com.becom.jspt2016.model.SysUser;
import com.becom.jspt2016.model.TbBizJzgjbxx;
import com.becom.jspt2016.service.ext.IInstitutionsManageExt;
import com.becom.jspt2016.service.ext.ITeacherAccountManageExt;
import com.becom.jspt2016.webapp.action.BaseAction;
import com.sun.accessibility.internal.resources.accessibility;

import net.sf.json.JSONObject;

/**
 * 学校模块
 * <p>
 * 机构信息管理
 * @author   
 * @author   
 * @Date	 2016年11月11日
 * @Date	 2016年11月11日 
 */

public class SchoolInfoManageAction extends BaseAction {
	@Spring
	private ITeacherAccountManageExt teacherAccountManageExt;
	protected Logger logger = Logger.getLogger(this.getClass());

	public TbBizJzgjbxx teacher;
	/**
	 * 机构用户
	 */
	public SysUser sUser;

	
	public InstitutionsInfoDto schoolInfo = new InstitutionsInfoDto();
	
	/**
	 * 区县列表
	 */
	public List<DisCodesDto> disCodes;
	@Spring
	private IInstitutionsManageExt institutionsManageExt;
	/**
	 * 机构Id
	 */
	public String institutionCode;
	/**
	 * 
	 * 
	 * <p>
	 * 
	 */
	@DefaultAction
	public Object baseInfo(HttpServletRequest request, HttpServletResponse response) {
		try {
			
		   sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
		   InstitutionsInfoDto institution = institutionsManageExt.getInstitution(sUser.getUserName());

		   schoolInfo = institutionsManageExt.get(institution.getInstitutionId());
	
			//获取区市列表
			disCodes = institutionsManageExt.queryDisCodes(schoolInfo.getDistrictCode());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "schoolBaseInfo.jsp";
	}

	
	/**
	 * 查询机构信息，
	 * <p>
	 * 通过机构代码查询
	 */
	public void fetchInstitutionByName(HttpServletResponse response, HttpServletRequest request) {

		String name = request.getParameter("jgName");

		try {
			JSONObject json = new JSONObject();
			   sUser = (SysUser) request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
			   InstitutionsInfoDto institution = institutionsManageExt.getInstitution(sUser.getUserName());
			   schoolInfo = institutionsManageExt.get(institution.getInstitutionId());
			boolean ins = institutionsManageExt.getInstitutionByName(name,String.valueOf(institution.getInstitutionId()));
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			if(ins){
				json.put("ins", "1");
			}else{
				json.put("ins", null);
			}
			
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("InstitutionsManageAction fetchInstitution() is error!", e);
		}
	}
	
	/**
	 * 查询机构信息，
	 * <p>
	 * 通过机构代码查询
	 */
	public void fetchInstitution(HttpServletResponse response) {

		try {
			JSONObject json = new JSONObject();
			InstitutionsInfoDto ins = institutionsManageExt.getInstitution(institutionCode);
			response.setContentType("text/xml;charset=UTF-8");
			PrintWriter out = response.getWriter();
			json.put("ins", ins);
			out.write(json.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("InstitutionsManageAction fetchInstitution() is error!", e);
		}
	}
	
	/**
	 * 编辑
	 */
	public Object edit(HttpServletRequest request, HttpServletResponse response) {
		int result = 0;
		try {
			result = institutionsManageExt.edit(schoolInfo);
			if (result == 0) {
				message = "编辑成功";
			}
		} catch (Exception e) {
			if (result == 1) {
				message = "编辑失败";
			}
			e.printStackTrace();
		}
		return baseInfo(request,response);
	}
	
}
