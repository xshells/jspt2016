package com.becom.jspt2016.webapp.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.becom.jspt2016.model.TbCfgZdxb;
import com.becom.jspt2016.model.TbCfgZdxbId;
import com.becom.jspt2016.service.ITbCfgZdxbManager;
import com.becom.jspt2016.service.ext.ITbCfgZdxbManagerExt;

/**
 * YuChengCheng
 */
public class InitDictionUtil {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(InitDictionUtil.class);

	private List<TbCfgZdxbId> sysDictes = new ArrayList<TbCfgZdxbId>();

	private List<TbCfgZdxbId> sysSelectDictes = new ArrayList<TbCfgZdxbId>();

	private static Map<String, TbCfgZdxbId> dictMap = new ConcurrentHashMap<String, TbCfgZdxbId>();

	public void findAllSysDict() {

		if (sysDictes == null || sysDictes.size() == 0) {
			sysDictes = tbCfgZdxbManagerExt.findAllInfo();
			dictMap = new ConcurrentHashMap<String, TbCfgZdxbId>();
			for (TbCfgZdxbId sysDictInfo : sysDictes) {
				dictMap.put(sysDictInfo.getZdbs() + sysDictInfo.getZdxbm(),
						sysDictInfo);
			}
		}
		/*	sysDictes = tbCfgZdxbManagerExt.findAllInfo();
			dictMap = new ConcurrentHashMap<String, TbCfgZdxbId>();
			if (sysDictes.size() > 0 && sysDictes != null) {
				for (TbCfgZdxbId sysDictInfo : sysDictes) {
					dictMap.put(sysDictInfo.getZdbs() + sysDictInfo.getZdxbm(),
							sysDictInfo);
				}
			}*/

	}

	public String getDictNameByDictId(String dictId) {
		String ret = "";
		try {
			if (dictMap != null) {
				if (null != dictMap.get(dictId)) {
					ret = dictMap.get(dictId).getZdxmc().toString();
				}
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage() + ex.fillInStackTrace());
		}
		return ret;
	}

	public String getDictNameByDictIdAndType(String dictId, String dictbm,
			String schoolType) {
		String ret = "";
		try {
			/*	if (dictMap != null) {
					if (null != dictMap.get(dictId)) {
						ret = dictMap.get(dictId).getZdxmc().toString();
					}
				}*/
			ret = tbCfgZdxbManagerExt.findNameBybsbmtype(dictId, dictbm,
					schoolType);
		} catch (Exception ex) {
			logger.error(ex.getMessage() + ex.fillInStackTrace());
		}
		return ret;
	}

	public List<TbCfgZdxbId> getDictByDictType(String dictType,
			String schoolType) {
		List<TbCfgZdxbId> ret = new ArrayList<TbCfgZdxbId>();
		try {
			/*	if (sysDictes != null && sysDictes.size() > 0) {
					for (TbCfgZdxbId diction : sysDictes) {
						if (diction.getZdbs().compareTo(dictType) == 0) {

							ret.add(diction);
						}
					}
				}*/
			//查询语句 组装数据
			sysSelectDictes = tbCfgZdxbManagerExt.findNameByType(dictType,
					schoolType);
			if (sysSelectDictes != null && sysSelectDictes.size() > 0) {
				for (TbCfgZdxbId diction : sysSelectDictes) {
					ret.add(diction);
				}
			}

		} catch (Exception ex) {
			logger.error(ex.getMessage() + ex.fillInStackTrace());
		}
		//				Collections.sort(ret, new DictionComparator());
		return ret;
	}

	public List<TbCfgZdxbId> getDictByDictType(String dictType) {
		List<TbCfgZdxbId> ret = new ArrayList<TbCfgZdxbId>();
		try {
			if (sysDictes != null && sysDictes.size() > 0) {
				for (TbCfgZdxbId diction : sysDictes) {
					if (diction.getZdbs().compareTo(dictType) == 0) {

						ret.add(diction);
					}
				}
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage() + ex.fillInStackTrace());
		}
		//				Collections.sort(ret, new DictionComparator());
		return ret;
	}

	public String getDictIdByDictTypeAndDictCode(String dictType,
			String dictCode) {

		String dictId = null;
		if (sysDictes != null && sysDictes.size() > 0) {

			for (TbCfgZdxbId diction : sysDictes) {
				if (diction.getZdxbm().compareTo(dictType) == 0) {
					if (diction.getZdxbm().equals(dictCode)) {
						dictId = diction.getZdxbm().toString();
						break;
					}
				}
			}
		}
		return dictId;
	}

	private ITbCfgZdxbManager sysDictManager;

	public ITbCfgZdxbManagerExt tbCfgZdxbManagerExt;

	public ITbCfgZdxbManagerExt getTbCfgZdxbManagerExt() {
		return tbCfgZdxbManagerExt;
	}

	public void setTbCfgZdxbManagerExt(ITbCfgZdxbManagerExt tbCfgZdxbManagerExt) {
		this.tbCfgZdxbManagerExt = tbCfgZdxbManagerExt;
	}

	public void setSysDictManager(ITbCfgZdxbManager sysDictManager) {
		this.sysDictManager = sysDictManager;
	}

	public List<TbCfgZdxbId> getSysDictes() {

		return sysDictes;
	}

	public List<TbCfgZdxbId> getSysSelectDictes() {
		return sysSelectDictes;
	}

	public Map<String, TbCfgZdxbId> getDictMap() {
		return dictMap;
	}

	class DictionComparator implements Comparator<TbCfgZdxb> {

		public int compare(TbCfgZdxb o1, TbCfgZdxb o2) {

			if (new Integer(o1.getId().getZdxbm()).compareTo(new Integer(o2
					.getId().getZdxbm())) > 0) {
				return 1;
			}
			if (new Integer(o1.getId().getZdxbm()).compareTo(new Integer(o2
					.getId().getZdxbm())) < 0) {
				return -1;
			}
			return 0;
		}
	}
}
