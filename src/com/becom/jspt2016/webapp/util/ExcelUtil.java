package com.becom.jspt2016.webapp.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtil {
	private SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	
	public void setFmt(SimpleDateFormat fmt){
		this.fmt = fmt;
	}

	public boolean export2007(HttpServletResponse response, List<List<Object>> list,String filename,String[] title){
		String[] header = title;
		
		XSSFWorkbook wb = new XSSFWorkbook();  
        XSSFSheet sheet = wb.createSheet(filename);  
        XSSFRow row = sheet.createRow((int) 0);  
        XSSFCellStyle style = wb.createCellStyle(); 
        
        XSSFFont font = wb.createFont();
        font.setFontHeightInPoints((short) 11);
        font.setFontName("宋体");
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style.setFillForegroundColor(HSSFColor.GREY_80_PERCENT.index);
        style.setFont(font);
  
        XSSFCell cell = null;
        for(int i=0;i<header.length;i++){
        	cell = row.createCell((short) i);  
            cell.setCellValue(header[i]);  
            cell.setCellStyle(style);
        }
        
        if(list == null){
        	return true;
        }
  
        for (int i = 0; i < list.size(); i++){  
            row = sheet.createRow((int) i + 1);
            
            List<Object> clist = list.get(i);
            for(int n=0;n<clist.size();n++) {
            	Object value = clist.get(n);
            	if(value instanceof Date){
            		row.createCell((short)n).setCellValue(fmt.format(value));
            	}else{
            		row.createCell((short)n).setCellValue(clist.get(n).toString());
            	}
            }
        }  
       
        try{  
        	response.setContentType("application/force-download");
			response.setHeader("Content-Disposition", "attachment;filename=\"" + java.net.URLEncoder.encode(filename, "UTF-8") + ".xlsx" + "\" ");
            wb.write(response.getOutputStream());  
			response.getOutputStream().close();
			return true;
        }catch (Exception e){  
        	return false;  
        }  
	}
	
	/****/
	public boolean export2007(HttpServletRequest request,HttpServletResponse response, List<List<Object>> list,String filename,String[] title){
		String[] header = title;
		
		XSSFWorkbook wb = new XSSFWorkbook();  
        XSSFSheet sheet = wb.createSheet(filename);  
        XSSFRow row = sheet.createRow((int) 0);  
        XSSFCellStyle style = wb.createCellStyle(); 
        
        XSSFFont font = wb.createFont();
        font.setFontHeightInPoints((short) 11);
        font.setFontName("宋体");
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style.setFillForegroundColor(HSSFColor.GREY_80_PERCENT.index);
        style.setFont(font);
  
        XSSFCell cell = null;
        for(int i=0;i<header.length;i++){
        	cell = row.createCell((short) i);  
            cell.setCellValue(header[i]);  
            cell.setCellStyle(style);
        }
        
        if(list == null){
        	return true;
        }
  
        for (int i = 0; i < list.size(); i++){  
            row = sheet.createRow((int) i + 1);
            
            List<Object> clist = list.get(i);
            for(int n=0;n<clist.size();n++) {
            	Object value = clist.get(n);
            	if(value instanceof Date){
            		row.createCell((short)n).setCellValue(fmt.format(value));
            	}else{
            		row.createCell((short)n).setCellValue(clist.get(n).toString());
            	}
            }
        }  
       
        try{
        	response.setContentType("application/force-download");
        	String agent = (String)request.getHeader("USER-AGENT");     
        	if(agent != null && agent.toLowerCase().indexOf("firefox") > 0) {// FF      
        	    String enableFileName = "=?UTF-8?B?" + (new String(Base64.encodeBase64((filename+".xlsx").getBytes("UTF-8")))) + "?=";    
        	    response.setHeader("Content-Disposition", "attachment; filename=" + enableFileName);    
        	}else{
        		response.setHeader("Content-Disposition", "attachment;filename=\"" + java.net.URLEncoder.encode(filename, "UTF-8") + ".xlsx" + "\" ");
        	}
        	
            wb.write(response.getOutputStream());  
			response.getOutputStream().close();
			return true;
        }catch (Exception e){  
        	return false;  
        }  
	}
	
	public boolean export2007(HttpServletRequest request,String path, List<List<Object>> list,String filename,String[] title){
		String[] header = title;
		
		XSSFWorkbook wb = new XSSFWorkbook();  
        XSSFSheet sheet = wb.createSheet(filename);  
        XSSFRow row = sheet.createRow((int) 0);  
        XSSFCellStyle style = wb.createCellStyle(); 
        
        XSSFFont font = wb.createFont();
        font.setFontHeightInPoints((short) 11);
        font.setFontName("宋体");
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style.setFillForegroundColor(HSSFColor.GREY_80_PERCENT.index);
        style.setFont(font);
  
        XSSFCell cell = null;
        for(int i=0;i<header.length;i++){
        	cell = row.createCell((short) i);  
            cell.setCellValue(header[i]);  
            cell.setCellStyle(style);
        }
        
        if(list == null){
        	return true;
        }
  
        for (int i = 0; i < list.size(); i++){  
            row = sheet.createRow((int) i + 1);
            
            List<Object> clist = list.get(i);
            for(int n=0;n<clist.size();n++) {
            	Object value = clist.get(n);
            	if(value instanceof Date){
            		row.createCell((short)n).setCellValue(fmt.format(value));
            	}else{
            		row.createCell((short)n).setCellValue(clist.get(n).toString());
            	}
            }
        }  
       
        try {
			OutputStream out = new FileOutputStream(path);
			wb.write(out);
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        return true;
	}
}
