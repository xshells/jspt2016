package com.becom.jspt2016.webapp.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.becom.jspt2016.common.Constant;

public class UserSessionObjectFilter implements Filter {

	public void destroy() {
	}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain fc) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		String requestURL = request.getRequestURI();
		if (requestURL != null
				&& (requestURL.contains("LoginAction") || requestURL.contains("loginAction")
						|| requestURL.contains("LoginTestAction") || requestURL.contains("loginTestAction"))
						|| requestURL.contains("/webservice")) {
			fc.doFilter(req, res);
		} else {
			Object obj = request.getSession().getAttribute(Constant.KEY_LOGIN_USER);
			if (obj == null) {
				if( request.getHeader("x-requested-with") != null && request.getHeader("x-requested-with").equals("XMLHttpRequest") ) {
					response.setHeader("sessionstatus", "timeout"); 
			    }else{
			    	response.sendRedirect("http://teacher.bjedu.gov.cn/Public/logout"); 
			    }
			} else {
				fc.doFilter(req, res);
			}
		}
	}

	public void init(FilterConfig arg0) throws ServletException {
	}

}
