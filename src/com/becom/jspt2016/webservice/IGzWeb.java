/**
 * IGzWeb.java
 * com.becom.jspt2016.webservice
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.webservice;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import com.becom.jspt2016.webservice.entity.GzBasicInfo;
import com.becom.jspt2016.webservice.entity.GzEducationInfo;
import com.becom.jspt2016.webservice.entity.ResultInfo;
/**
 * 高职接口
 * @author fmx
 *
 */
@WebService
@SOAPBinding(style=Style.RPC)
public interface IGzWeb extends IBaseWeb{
	
	/**
	 * 维护基本信息
	 * @return
	 */
	public ResultInfo updateGzBasicInfo(@WebParam(name="basicInfo")GzBasicInfo basicInfo);
	
	
	/**
	 * 添加教育教学
	 * @return
	 */
	public ResultInfo addGzEducationInfo(@WebParam(name="educationInfo")GzEducationInfo educationInfo);
}
