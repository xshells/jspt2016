/**
 * IWebServiceExt.java
 * com.becom.jspt2016.webservice.service
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.webservice.service;

import com.becom.jspt2016.webservice.entity.ContactInfo;
import com.becom.jspt2016.webservice.entity.GxBasicInfo;
import com.becom.jspt2016.webservice.entity.GxEducationInfo;
import com.becom.jspt2016.webservice.entity.GzBasicInfo;
import com.becom.jspt2016.webservice.entity.GzEducationInfo;
import com.becom.jspt2016.webservice.entity.LearnInfo;
import com.becom.jspt2016.webservice.entity.MajorInfo;
import com.becom.jspt2016.webservice.entity.OverseasStudyInfo;
import com.becom.jspt2016.webservice.entity.PersonnelInfo;
import com.becom.jspt2016.webservice.entity.PostInfo;
import com.becom.jspt2016.webservice.entity.ResultInfo;
import com.becom.jspt2016.webservice.entity.SkillInfo;
import com.becom.jspt2016.webservice.entity.TeacherQuaInfo;
import com.becom.jspt2016.webservice.entity.WorkInfo;
import com.becom.jspt2016.webservice.entity.YearTestInfo;

/**
 * webservice 业务接口
 * @author fmx
 *
 */
public interface IWebServiceExt {

	/**
	 * 添加联系方式
	 * @param contactInfo
	 * @return
	 */
	public ResultInfo addContactInfo(ContactInfo contactInfo);
	/**
	 * 更新联系方式
	 * @param contactInfo
	 * @return
	 */
//	public ResultInfo updateContactInfo(ContactInfo contactInfo);
	/**
	 * 添加海外研修
	 * @param overseaInfo
	 * @return
	 */
	public ResultInfo addOverseasStudyInfo(OverseasStudyInfo overseaInfo);
	/**
	 * 添加海外研修
	 * @param overseaInfo
	 * @return
	 */
//	public ResultInfo updateOverseasStudyInfo(OverseasStudyInfo overseaInfo);
	/**
	 * 添加 技能与证书
	 * @param skillInfo
	 * @return
	 */
	public ResultInfo addSkillInfo(SkillInfo skillInfo);
	/**
	 * 添加  入选人才项目
	 * @param personnelInfo
	 * @return
	 */
	public ResultInfo addPersonnelInfo(PersonnelInfo personnelInfo);
	/**
	 * 添加教育资格
	 * @param teacherQuaInfo
	 * @return
	 */
	public ResultInfo addTeacherQuaInfo(TeacherQuaInfo teacherQuaInfo);
	/**
	 * 添加年度考核
	 * @param yearTestInfo
	 * @return
	 */
	public ResultInfo addYearTestInfo(YearTestInfo yearTestInfo);
	/**
	 * 添加专业技术聘任
	 * @param majorInfo
	 * @return
	 */
	public ResultInfo addMajorInfo(MajorInfo majorInfo);
	/**
	 * 添加  工作经历
	 * @param workInfo
	 * @return
	 */
	public ResultInfo addWorkInfo(WorkInfo workInfo);
	/**
	 * 添加  岗位聘任
	 * @param postInfo
	 * @return
	 */
	public ResultInfo addPostInfo(PostInfo postInfo);
	/**
	 * 添加  学习经历
	 * @param learnInfo
	 * @return
	 */
	public ResultInfo addLearnInfo(LearnInfo learnInfo);
	/**
	 * 添加gxg教育教学
	 * @param educationInfo
	 * @return
	 */
	public ResultInfo addGxEducationInfo(GxEducationInfo educationInfo);
	/**
	 * 添加gz教育教学
	 * @param educationInfo
	 * @return
	 */
	public ResultInfo addGzEducationInfo(GzEducationInfo educationInfo);
	/**
	 * 添加gx教师基本信息
	 * @param basicInfo
	 * @return
	 */
	public ResultInfo addGxBasicInfo(GxBasicInfo basicInfo);
	/**
	 * 更新gx教师基本信息
	 * @param basicInfo
	 * @return
	 */
//	public ResultInfo updateGxBasicInfo(GxBasicInfo basicInfo);
	/**
	 * 添加gz教师基本信息
	 * @param basicInfo
	 * @return
	 */
	public ResultInfo addGzBasicInfo(GzBasicInfo basicInfo);
	/**
	 * 更新gz教师基本信息
	 * @param basicInfo
	 * @return
	 */
//	public ResultInfo updateGzBasicInfo(GzBasicInfo basicInfo);
}
