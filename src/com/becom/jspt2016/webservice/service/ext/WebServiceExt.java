/**
 * WebServiceExt.java
 * com.becom.jspt2016.webservice.service.ext
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.webservice.service.ext;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nestframework.addons.spring.Spring;
import org.nestframework.commons.hibernate.ISqlElement;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.becom.jspt2016.common.ConstantBean;
import com.becom.jspt2016.service.ext.impl.JdbcRootManager;
import com.becom.jspt2016.webapp.util.IDValidator;
import com.becom.jspt2016.webservice.entity.ContactInfo;
import com.becom.jspt2016.webservice.entity.GxBasicInfo;
import com.becom.jspt2016.webservice.entity.GxEducationInfo;
import com.becom.jspt2016.webservice.entity.GzBasicInfo;
import com.becom.jspt2016.webservice.entity.GzEducationInfo;
import com.becom.jspt2016.webservice.entity.LearnInfo;
import com.becom.jspt2016.webservice.entity.MajorInfo;
import com.becom.jspt2016.webservice.entity.OverseasStudyInfo;
import com.becom.jspt2016.webservice.entity.PersonnelInfo;
import com.becom.jspt2016.webservice.entity.PostInfo;
import com.becom.jspt2016.webservice.entity.ResultInfo;
import com.becom.jspt2016.webservice.entity.SkillInfo;
import com.becom.jspt2016.webservice.entity.TeacherQuaInfo;
import com.becom.jspt2016.webservice.entity.WorkInfo;
import com.becom.jspt2016.webservice.entity.YearTestInfo;
import com.becom.jspt2016.webservice.service.IWebServiceExt;
import com.becom.jspt2016.webservice.util.ParamUtil;

/**
 * webservice 业务实现
 * @author fmx
 *
 */
public class WebServiceExt extends JdbcRootManager implements IWebServiceExt {
	protected Logger logger = Logger.getLogger(WebServiceExt.class);
	
	@Spring
	private ConstantBean constantBean;
	
	public ConstantBean getConstantBean() {
		return constantBean;
	}

	public void setConstantBean(ConstantBean constantBean) {
		this.constantBean = constantBean;
	}

	/** 公共业务方法  start**/
	
	/**
	 * 根据证件号获取教师基本信息，用于数据操作
	 * @param cardNo
	 * @return
	 */
	public Map<String, Object> getTeacherByCard(String cardNo) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("cardNo", cardNo);
		try {
			List<Map<String,Object>> rlist = this.findList("WebServiceExt.getTeacherByCard.query", param, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					Map<String,Object> teacher = new HashMap<String,Object>();
					teacher.put("jgId", rs.getLong("jgId"));
					teacher.put("jsId", rs.getLong("jsId"));
					teacher.put("eduId", rs.getString("eduId"));
					teacher.put("name", rs.getString("name"));
					teacher.put("cardType", rs.getString("cardType"));
					teacher.put("cardNo", rs.getString("cardNo"));
					return teacher;
				}
			});
			return rlist.size() > 0 ? rlist.get(0) : null;
		} catch (Exception e) {
			logger.error("WebServiceExt getTeacherByCard() is error!", e);
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 根据机构名称和代码  获取机构ID
	 * @param jgName
	 * @param jgCode
	 * @return
	 */
	public Long getJgIdByName(String jgName,String jgCode) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("jgName", jgName);
		param.put("jgCode", jgCode);
		try {
			List<Long> rlist = this.findList("WebServiceExt.getJgIdByName.query", param, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					return rs.getLong("jgId");
				}
			});
			return rlist.size() > 0 ? rlist.get(0) : null;
		} catch (Exception e) {
			logger.error("WebServiceExt getJgIdByName() is error!", e);
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取市级序列用于计算教育ID
	 * @return
	 */
	public String getSequenceSHIJI() {
		Map<String, Object> param = new HashMap<String, Object>();
		try {
			List<String> rlist = this.findList("WebServiceExt.getSequenceSHIJI.query", param, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					return rs.getString("gxedu");
				}
			});
			return rlist.get(0);
		} catch (Exception e) {
			logger.error("WebServiceExt getSequenceSHIJI() is error!", e);
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 获取教育Id
	 * @param  affiliatedSchool 所属学段
	 * @param disCode 区市代码也就是所在地
	 */
	public String getEduId() {
		//直接判断教师所在机构是哪个区或者市，是哪个就走哪个不用判断是哪个学段
		String eduId = "900";
		String s = getSequenceSHIJI();
		for (int i = 0; i < 5 - s.length(); i++) {
			eduId += "0";
		}
		eduId += s;
		return eduId;
	}
	
	private ResultInfo getResultInfo(boolean flag,String errMsg){
		ResultInfo result = new ResultInfo();
		result.setFlag(flag);
		result.setMsg(errMsg);
		return result;
	}
	/** 业务方法 start **/
	
	///联系方式操作  start
	public ResultInfo addContactInfo(final ContactInfo contactInfo) {
		final Map<String, Object>  teacher = getTeacherByCard(contactInfo.getCardNo().trim());
		if(teacher == null){
			return getResultInfo(false,"没有指定的教师信息，请检查身份证号是否正确！");
		}
		
		final Long jgId = getJgIdByName(contactInfo.getSchoolName(), contactInfo.getSchoolCode());
		if(jgId == null){
			return getResultInfo(false,"学校信息不存在！");
		}
		
		if(teacher.get("jgId") == null || jgId != (Long)teacher.get("jgId")){
			return getResultInfo(false,"教师和学校不对应！");
		}
		///判读是否修改
		if(getContactInfo(teacher.get("jsId"))){
			return updateContactInfo(contactInfo);
		}
		
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO TB_BIZ_JZGLXFSXX(lxfs_id,jsid,sj,email,cjsj,sfsc,bsrlb,shzt,shsj,shjg,cjr) ").append(
				"values (SEQ_TB_BIZ_JZGLXFSXX.nextval,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, (Long)(teacher.get("jsId")));
				ps.setString(2, contactInfo.getPhone());
				ps.setString(3, contactInfo.getEmail());
				ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
				ps.setString(5, "0");
				ps.setString(6, "3");//3 校级，4 市级
				ps.setString(7, "01");
				ps.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
				ps.setString(9, null);
				ps.setLong(10, jgId);//创建人暂时保存机构Id
			}
		});
		
		return getResultInfo(true,"添加成功！");
	}

	/**
	 * 获取已有的联系方式信息
	 * @param jsId
	 * @return
	 */
	public boolean getContactInfo(Object jsId){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jsId", jsId);
		
		try {
			List<Integer> rlist = this.findList("WebServiceExt.getContactInfo.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					return rs.getInt("count");
				}
			});
			return rlist.size() > 0 ? rlist.get(0) > 0 : false;
		} catch (Exception e) {
			logger.error("WebServiceExt getContactInfo() is error!", e);
			e.printStackTrace();
		}
		
		return true;
	}
	
	public ResultInfo updateContactInfo(ContactInfo contactInfo) {
		final Map<String, Object>  teacher = getTeacherByCard(contactInfo.getCardNo().trim());
		if(teacher == null){
			return getResultInfo(false,"没有指定的教师信息，请检查身份证号是否正确！");
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jsId", teacher.get("jsId"));
		params.put("sj", contactInfo.getPhone());
		params.put("email", contactInfo.getEmail());
		try {
			ISqlElement se = processSql(params, "WebServiceExt.updateContactInfo.update");
			int count = getJdbcTemplate().update(se.getSql(), se.getParams());
			if(count > 0){
				return getResultInfo(true,"更新成功！");
			}
		} catch (Exception e) {
			logger.error("WebServiceExt updateContactInfo is error!", e);
		}
		return getResultInfo(false,"更新失败！");
	}
	////联系方式操作 end

	
	////海外研修 start
	public ResultInfo addOverseasStudyInfo(final OverseasStudyInfo overseaInfo) {
		final Map<String, Object>  teacher = getTeacherByCard(overseaInfo.getCardNo().trim());
		if(teacher == null){
			return getResultInfo(false,"没有指定的教师信息，请检查身份证号是否正确！");
		}
		
		final Long jgId = getJgIdByName(overseaInfo.getSchoolName(), overseaInfo.getSchoolCode());
		if(jgId == null){
			return getResultInfo(false,"学校信息不存在！");
		}
		
		if(teacher.get("jgId") == null || jgId != (Long)teacher.get("jgId")){
			return getResultInfo(false,"教师和学校不对应！");
		}
		
		if(getOverseasStudyInfo(teacher.get("jsId"))){
			return updateOverseasStudyInfo(overseaInfo);
		}
		
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO TB_BIZ_JZGHWYXXX(hwyx_Id,jsid,sfjyhwyxjl,bsrlb,shzt,shsj,sfsc,cjsj,cjr) ").append(
				"values (SEQ_TB_BIZ_JZGHWYXXX.nextval,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, (Long)(teacher.get("jsId")));//
				ps.setString(2, overseaInfo.getIsStudy().split("-")[0]);
				ps.setString(3, "3");
				ps.setString(4, "01");
				ps.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				ps.setString(6, "0");
				ps.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
				ps.setLong(8, jgId);
			}
		});
		
		return getResultInfo(true,"添加成功！");
	}
	
	public boolean getOverseasStudyInfo(Object jsId){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jsId", jsId);
		
		try {
			List<Integer> rlist = this.findList("WebServiceExt.getOverseasStudyInfo.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					return rs.getInt("count");
				}
			});
			return rlist.size() > 0 ? rlist.get(0) > 0 : false;
		} catch (Exception e) {
			logger.error("WebServiceExt getOverseasStudyInfo() is error!", e);
			e.printStackTrace();
		}
		
		return true;
	}

	public ResultInfo updateOverseasStudyInfo(OverseasStudyInfo overseaInfo) {
		final Map<String, Object>  teacher = getTeacherByCard(overseaInfo.getCardNo().trim());
		if(teacher == null){
			return getResultInfo(false,"没有指定的教师信息，请检查身份证号是否正确！");
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jsId", teacher.get("jsId"));
		params.put("sfjyhwyxjl", overseaInfo.getIsStudy().split("-")[0]);
		try {
			ISqlElement se = processSql(params, "WebServiceExt.updateOverseasStudyInfo.update");
			int count = getJdbcTemplate().update(se.getSql(), se.getParams());
			if(count > 0){
				return getResultInfo(true,"更新成功！");
			}
		} catch (Exception e) {
			logger.error("SysTeacherManagerExt updateOverseasStudy() is error!", e);
		}
		return getResultInfo(false,"更新失败！");
	}
	////海外研修 end

	///专业技能
	public ResultInfo addSkillInfo(final SkillInfo skillInfo) {
		final Map<String, Object>  teacher = getTeacherByCard(skillInfo.getCardNo().trim());
		if(teacher == null){
			return getResultInfo(false,"没有指定的教师信息，请检查身份证号是否正确！");
		}
		
		if(isExistLanguage(teacher.get("jsId"),skillInfo.getLanguage())){
			return getResultInfo(false,"已存在语种信息--“" + skillInfo.getLanguage() + "”！");
		}
		final Long jgId = getJgIdByName(skillInfo.getSchoolName(), skillInfo.getSchoolCode());
		if(jgId == null){
			return getResultInfo(false,"学校信息不存在！");
		}
		
		if(teacher.get("jgId") == null || jgId != (Long)teacher.get("jgId")){
			return getResultInfo(false,"教师和学校不对应！");
		}
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO TB_BIZ_JZGYYNLXX(jnzs_id,jsid,yz,zwcd,cjsj,sfsc,bsrlb,shzt,shsj,shjg,cjr) ").append(
				"values (SEQ_TB_BIZ_JZGYYNLXX.nextval,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, (Long)(teacher.get("jsId")));
				ps.setString(2, skillInfo.getLanguage().split("-")[0]);
				ps.setString(3, skillInfo.getLevel().split("-")[0]);
				ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
				ps.setString(5, "0");
				ps.setString(6, "3");
				ps.setString(7, "01");
				ps.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
				ps.setString(9, null);
				ps.setLong(10, jgId);
			}
		});
		return getResultInfo(true,"添加成功！");
	}
	
	/**
	 * 判断语种是否存在
	 * @param jsId
	 * @param language
	 * @return
	 */
	public boolean isExistLanguage(Object jsId,Object language){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jsId", jsId);
		params.put("yz", language);
		
		try {
			List<Integer> rlist = this.findList("WebServiceExt.isExistLanguage.query", params, new RowMapper() {
				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					return rs.getInt("count");
				}
			});
			return rlist.size() > 0 ? rlist.get(0) > 0 : false;
		} catch (Exception e) {
			logger.error("WebServiceExt isExistLanguage() is error!", e);
			e.printStackTrace();
		}
		
		return true;
	}
	///专业技能 end
	
	
	//添加  入选人才项目
	public ResultInfo addPersonnelInfo(final PersonnelInfo personnelInfo) {
		final Map<String, Object>  teacher = getTeacherByCard(personnelInfo.getCardNo().trim());
		if(teacher == null){
			return getResultInfo(false,"没有指定的教师信息，请检查身份证号是否正确！");
		}
		final Long jgId = getJgIdByName(personnelInfo.getSchoolName(), personnelInfo.getSchoolCode());
		if(jgId == null){
			return getResultInfo(false,"学校信息不存在！");
		}
		
		if(teacher.get("jgId") == null || jgId != (Long)teacher.get("jgId")){
			return getResultInfo(false,"教师和学校不对应！");
		}
		StringBuffer sql = new StringBuffer();
		sql.append(" insert into tb_biz_jzgrxrcxmxx (RXRCXM_ID ,JSID,BSRLB,SHZT,SHSJ,SHJG,SFSC,CJSJ,rxrcxmmc,rxny,cjr)")
				.append("values (SEQ_TB_BIZ_JZGRXRCXMXX.nextval,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, (Long)(teacher.get("jsId")));
				ps.setString(2, "3");
				ps.setString(3, "01");
				ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
				ps.setString(5, "");
				ps.setString(6, "0");
				ps.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
				ps.setString(8, personnelInfo.getProName().split("-")[0]);
				ps.setString(9, personnelInfo.getYear());
				ps.setLong(10, jgId);
			}
		});
		
		return getResultInfo(true,"添加成功！");
	}

	///添加  教师资格
	public ResultInfo addTeacherQuaInfo(final TeacherQuaInfo teacherQuaInfo) {
		final Map<String, Object>  teacher = getTeacherByCard(teacherQuaInfo.getCardNo().trim());
		if(teacher == null){
			return getResultInfo(false,"没有指定的教师信息，请检查身份证号是否正确！");
		}
		
		final Long jgId = getJgIdByName(teacherQuaInfo.getSchoolName(), teacherQuaInfo.getSchoolCode());
		if(jgId == null){
			return getResultInfo(false,"学校信息不存在！");
		}
		
		if(teacher.get("jgId") == null || jgId != (Long)teacher.get("jgId")){
			return getResultInfo(false,"教师和学校不对应！");
		}
		StringBuffer sql = new StringBuffer();
		sql.append(
				"insert into TB_BIZ_JSZGXX (JSZG_ID,JSID,JSZGZZL,JSZGZHM,RJXK,ZSBFRQ,ZSRDJG,CJSJ,BSRLB,SHZT,SHSJ,SFSC,cjr) ")
				.append("values (SEQ_TB_BIZ_JSZGXX.nextval,?,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, (Long)(teacher.get("jsId")));
				ps.setString(2, teacherQuaInfo.getQualification().split("-")[0]);
				ps.setString(3, "");
				ps.setString(4, "");
				ps.setString(5, "");
				ps.setString(6, "");
				ps.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
				ps.setString(8, "3");
				ps.setString(9, "01");
				ps.setTimestamp(10, new Timestamp(System.currentTimeMillis()));
				ps.setString(11, "0");
				ps.setString(12, String.valueOf(jgId));
			}
		});
		
		return getResultInfo(true,"添加成功！");
	}

	///添加 年度考核信息
	public ResultInfo addYearTestInfo(final YearTestInfo yearTestInfo) {
		final Map<String, Object>  teacher = getTeacherByCard(yearTestInfo.getCardNo().trim());
		if(teacher == null){
			return getResultInfo(false,"没有指定的教师信息，请检查身份证号是否正确！");
		}
		final Long jgId = getJgIdByName(yearTestInfo.getSchoolName(), yearTestInfo.getSchoolCode());
		if(jgId == null){
			return getResultInfo(false,"学校信息不存在！");
		}
		
		if(teacher.get("jgId") == null || jgId != (Long)teacher.get("jgId")){
			return getResultInfo(false,"教师和学校不对应！");
		}
		StringBuffer sql = new StringBuffer();
		sql.append("insert into TB_BIZ_JZGNDKHXX(NDKH_ID,JSID,KHND,KHJG,KHDWMC,BSRLB,SHZT,SHSJ,SHJG,SFSC,CJR,CJSJ)")
				.append("values (SEQ_TB_BIZ_JZGNDKHXX.nextval,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, (Long)(teacher.get("jsId")));
				ps.setString(2, yearTestInfo.getYear());//考核年度
				ps.setString(3, yearTestInfo.getResult().split("-")[0]);//考核结果
				ps.setString(4, yearTestInfo.getKhdwName());//考核单位名称
				ps.setString(5, "3");//报送人类别
				ps.setString(6, "01");//审核状态
				ps.setTimestamp(7, new Timestamp(System.currentTimeMillis()));//审核时间
				ps.setString(8, "");//审核结果
				ps.setString(9, "0");//是否删除
				ps.setLong(10, jgId);//创建人
				ps.setTimestamp(11, new Timestamp(System.currentTimeMillis()));//创建时间
			}
		});
		
		return getResultInfo(true,"添加成功！");
	}

	//添加  专业技术聘任
	public ResultInfo addMajorInfo(final MajorInfo majorInfo) {
		final Map<String, Object>  teacher = getTeacherByCard(majorInfo.getCardNo().trim());
		if(teacher == null){
			return getResultInfo(false,"没有指定的教师信息，请检查身份证号是否正确！");
		}
		final Long jgId = getJgIdByName(majorInfo.getSchoolName(), majorInfo.getSchoolCode());
		if(jgId == null){
			return getResultInfo(false,"学校信息不存在！");
		}
		
		if(teacher.get("jgId") == null || jgId != (Long)teacher.get("jgId")){
			return getResultInfo(false,"教师和学校不对应！");
		}
		StringBuffer sql = new StringBuffer();
		sql.append(
				"insert into TB_BIZ_JZGZYJSZWPRXX(ZYJSZW_ID,JSID,PRZYJSZW,PRDWMC,PRKSNY,PRJSNY,BSRLB,SHZT,SHSJ,SHJG,SFSC,CJR,CJSJ)")
				.append("values (SEQ_TB_BIZ_JZGZYJSZWPRXX.nextval,?,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, (Long)(teacher.get("jsId")));
				ps.setString(2, majorInfo.getDuties().split("-")[0]);
				ps.setString(3, "");
				ps.setString(4, majorInfo.getStartDate());
				ps.setString(5, "");
				ps.setString(6, "3");
				ps.setString(7, "01");
				ps.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
				ps.setString(9, "");//
				ps.setString(10, "0");//是否删除
				ps.setLong(11, jgId);//创建人
				ps.setTimestamp(12, new Timestamp(System.currentTimeMillis()));//创建时间
			}
		});
		
		return getResultInfo(true,"添加成功！");
	}

	//添加   工作经历
	public ResultInfo addWorkInfo(final WorkInfo workInfo) {
		final Map<String, Object>  teacher = getTeacherByCard(workInfo.getCardNo().trim());
		if(teacher == null){
			return getResultInfo(false,"没有指定的教师信息，请检查身份证号是否正确！");
		}
		final Long jgId = getJgIdByName(workInfo.getSchoolName(), workInfo.getSchoolCode());
		if(jgId == null){
			return getResultInfo(false,"学校信息不存在！");
		}
		
		if(teacher.get("jgId") == null || jgId != (Long)teacher.get("jgId")){
			return getResultInfo(false,"教师和学校不对应！");
		}
		StringBuffer sql = new StringBuffer();
		sql.append(
				"insert into TB_BIZ_JZGGZJLXX (GZJL_ID,JSID,RZDWMC,RZKSNY,RZJSNY,DWXZLB,RZGW,SFSC,CJSJ,BSRLB,SHZT,SHSJ,CJR) ")
				.append("values (SEQ_TB_BIZ_JZGGZJLXX.nextval,?,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, (Long)(teacher.get("jsId")));
				ps.setString(2, workInfo.getRzdwmc());
				ps.setString(3, workInfo.getRzksny());
				ps.setString(4, workInfo.getRzjsny());
				ps.setString(5, workInfo.getDwxzlb().split("-")[0]);
				ps.setString(6, workInfo.getRzgw());
				ps.setString(7, "0");
				ps.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
				//必填项默认填什么？
				ps.setString(9, "3");
				ps.setString(10, "01");
				ps.setTimestamp(11, new Timestamp(System.currentTimeMillis()));
				ps.setLong(12, jgId);
			}
		});
		
		return getResultInfo(true,"添加成功！");
	}

	//添加   岗位聘任
	public ResultInfo addPostInfo(final PostInfo postInfo) {
		final Map<String, Object>  teacher = getTeacherByCard(postInfo.getCardNo().trim());
		if(teacher == null){
			return getResultInfo(false,"没有指定的教师信息，请检查身份证号是否正确！");
		}
		final Long jgId = getJgIdByName(postInfo.getSchoolName(), postInfo.getSchoolCode());
		if(jgId == null){
			return getResultInfo(false,"学校信息不存在！");
		}
		
		if(teacher.get("jgId") == null || jgId != (Long)teacher.get("jgId")){
			return getResultInfo(false,"教师和学校不对应！");
		}
		StringBuffer sql = new StringBuffer();
		sql.append(
				"insert into TB_BIZ_JZGGWPRXX(GWPR_ID,JSID,DZZW,gwlb,gwgj,sfsjt,sjtgwlb,sjtgwdj,sfzzcsxlzxgz,sfcyxlzxzgzs,Sfjrqtgw,Jrgwlb,Jrgwdj,sfzzfdy,dzjb,prksny,rzksny,BSRLB,SHZT,SFSC,CJR,CJSJ)")
				.append("values (SEQ_TB_BIZ_JZGGWPRXX.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, (Long)(teacher.get("jsId")));
				ps.setString(2, postInfo.getDzzw().split("-")[0]); //党政职务（或校级职务）        
				ps.setString(3, postInfo.getGwlx().split("-")[0]); //岗位类别               
				ps.setString(4, postInfo.getGwdj().split("-")[0]); //岗位等级              
				ps.setString(5, postInfo.getSfsjt().split("-")[0]); //是否双肩挑      ----
				ps.setString(6, ""); //双肩挑岗位类别     
				ps.setString(7, ""); //双肩挑岗位等级     
				
				ps.setString(8, ""); //是否专职从事心理咨询工作     
				ps.setString(9, ""); //是否持有心理咨询资格证书  --
				ps.setString(10, "");//是否兼任其他岗位--(中等职业学校用户)
				ps.setString(11, "");//兼任岗位类别
				ps.setString(12, "");//兼任岗位等级

				ps.setString(13, postInfo.getSfwfdy().split("-")[0]);//是否为辅导员 
				ps.setString(14, postInfo.getDzjb().split("-")[0]); //党政级别   
				ps.setString(15, postInfo.getPrksny()); //聘任开始年月 
				ps.setString(16, postInfo.getRzksny()); //任职开始年月     
				ps.setString(17, "3");//报送人类别    
				ps.setString(18, "01");//审核状态
				ps.setString(19, "0");//是否删除
				ps.setLong(20, jgId);//创建人id
				ps.setTimestamp(21, new Timestamp(System.currentTimeMillis()));//创建时间
			}
		});
		
		return getResultInfo(true,"添加成功！");
	}

	//添加   学习经历
	public ResultInfo addLearnInfo(final LearnInfo learnInfo) {
		final Map<String, Object>  teacher = getTeacherByCard(learnInfo.getCardNo().trim());
		if(teacher == null){
			return getResultInfo(false,"没有指定的教师信息，请检查身份证号是否正确！");
		}
		final Long jgId = getJgIdByName(learnInfo.getSchoolName(), learnInfo.getSchoolCode());
		if(jgId == null){
			return getResultInfo(false,"学校信息不存在！");
		}
		
		if(teacher.get("jgId") == null || jgId != (Long)teacher.get("jgId")){
			return getResultInfo(false,"教师和学校不对应！");
		}
		StringBuffer sql = new StringBuffer();
		sql.append("insert into TB_BIZ_JZGXXJLXX (XL_ID,JSID,HDXL,HDXLDGJDQ,HDXLDYXHJG,SXZY,RXNY,BYNY,XWCC,XWMC,HDXWDGJDQ,HDXWDYXHJG,XWSYNY,XXFS,ZXDWLB,CJSJ,SFSC,BSRLB,SHZT,SHSJ,SFSFLZY,CJR)")
			.append("values (SEQ_TB_BIZ_JZGXXJLXX.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, (Long)(teacher.get("jsId")));
				ps.setString(2, learnInfo.getHdxl().split("-")[0]);//获得学历
				ps.setString(3, learnInfo.getHdxldgjdq().split("-")[0]);//获得学历的国家（地区）
				ps.setString(4, learnInfo.getHdxldyxhjg());//获得学历的院校和机构
				ps.setString(5, learnInfo.getSxzy());//所学专业
				ps.setString(6, learnInfo.getRxny());//入学年月
				ps.setString(7, learnInfo.getByny());//毕业年月
				ps.setString(8, learnInfo.getXwcc().split("-")[0]);//学位层次
				ps.setString(9, learnInfo.getXwmc().split("-")[0]);//学位名称
				ps.setString(10, learnInfo.getHdxwdgjdq().split("-")[0]);//获得学位的国家（地区）
				ps.setString(11, learnInfo.getHdxwdyxhjg());//获得学位的院校和机构
				ps.setString(12, learnInfo.getXwsyny());//学位授予年月
				ps.setString(13, learnInfo.getXxfs().split("-")[0]);//学习方式
				ps.setString(14, learnInfo.getZxdwlb().split("-")[0]);//在学单位类别
				ps.setTimestamp(15, new Timestamp(System.currentTimeMillis()));
				ps.setString(16, "0");
				ps.setString(17, "3");
				ps.setString(18, "01");
				ps.setTimestamp(19, new Timestamp(System.currentTimeMillis()));
				ps.setString(20, "");//是否师范类专业
				ps.setLong(21, jgId);
			}
		});
		
		return getResultInfo(true,"添加成功！");
	}

	//添加   教育教学 start
	public ResultInfo addGxEducationInfo(final GxEducationInfo educationInfo) {
		final Map<String, Object>  teacher = getTeacherByCard(educationInfo.getCardNo().trim());
		if(teacher == null){
			return getResultInfo(false,"没有指定的教师信息，请检查身份证号是否正确！");
		}
		final Long jgId = getJgIdByName(educationInfo.getSchoolName(), educationInfo.getSchoolCode());
		if(jgId == null){
			return getResultInfo(false,"学校信息不存在！");
		}
		
		if(teacher.get("jgId") == null || jgId != (Long)teacher.get("jgId")){
			return getResultInfo(false,"教师和学校不对应！");
		}
		StringBuffer sql = new StringBuffer();
		sql.append(
				"insert into tb_biz_jzgjyjxxx (JYJX_ID,JSID,BSRLB,SHZT,SHSJ,SHJG,SFSC,CJSJ,XN,XQ,SFWBKSSK,DSLB,RKZK,XZYCSXKLY,XNBZKKCJXKSS,rkxklb,jrgz,rjkc,rjxd,rkkclb,pjzks,cjr)")
				.append("values (SEQ_TB_BIZ_JZGJYJXXX.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, (Long)(teacher.get("jsId")));
				ps.setString(2, "3");
				ps.setString(3, "01");
				ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
				ps.setString(5, "");
				ps.setString(6, "0");
				ps.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
				ps.setString(8, educationInfo.getYear());//学年
				ps.setString(9, educationInfo.getTerm().split("-")[0]);//学期
				ps.setString(10, educationInfo.getIsBen().split("-")[0]);//是否为本科生上课
				ps.setString(11, educationInfo.getTeacherType().split("-")[0]);//导师类别
				ps.setString(12, educationInfo.getCourse().split("-")[0]);//任课状况
				ps.setString(13, educationInfo.getResearch().split("-")[0]);//现主要从事学科领域
				ps.setString(14, String.valueOf(educationInfo.getClassHour()));//课程教学课时数
				ps.setString(15, "");//任课学科类别
				ps.setString(16, "");//兼任工作
				ps.setString(17, "");//任教课程
				ps.setString(18, "");//任教学段
				ps.setString(19, "");//任课课程类别
				ps.setString(20, "");//平均每周课堂教学课时数
				ps.setString(21, String.valueOf(jgId));//创建人
			}
		});
		
		return getResultInfo(true,"添加成功！");
	}

	@Override
	public ResultInfo addGzEducationInfo(final GzEducationInfo educationInfo) {
		final Map<String, Object>  teacher = getTeacherByCard(educationInfo.getCardNo().trim());
		if(teacher == null){
			return getResultInfo(false,"没有指定的教师信息，请检查身份证号是否正确！");
		}
		final Long jgId = getJgIdByName(educationInfo.getSchoolName(), educationInfo.getSchoolCode());
		if(jgId == null){
			return getResultInfo(false,"学校信息不存在！");
		}
		
		if(teacher.get("jgId") == null || jgId != (Long)teacher.get("jgId")){
			return getResultInfo(false,"教师和学校不对应！");
		}
		StringBuffer sql = new StringBuffer();
		sql.append(
				"insert into tb_biz_jzgjyjxxx (JYJX_ID,JSID,BSRLB,SHZT,SHSJ,SHJG,SFSC,CJSJ,XN,XQ,SFWBKSSK,DSLB,RKZK,XZYCSXKLY,XNBZKKCJXKSS,rkxklb,jrgz,rjkc,rjxd,rkkclb,pjzks,cjr)")
				.append("values (SEQ_TB_BIZ_JZGJYJXXX.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setLong(1, (Long)(teacher.get("jsId")));
				ps.setString(2, "3");
				ps.setString(3, "01");
				ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
				ps.setString(5, "");
				ps.setString(6, "0");
				ps.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
				ps.setString(8, educationInfo.getYear());//学年
				ps.setString(9, educationInfo.getTerm().split("-")[0]);//学期
				ps.setString(10, "");//是否为本科生上课
				ps.setString(11, "");//导师类别
				ps.setString(12, educationInfo.getCourse().split("-")[0]);//任课状况
				ps.setString(13, educationInfo.getResearch().split("-")[0]);//现主要从事学科领域
				ps.setString(14, String.valueOf(educationInfo.getClassHour()));//课程教学课时数
				ps.setString(15, "");//任课学科类别
				ps.setString(16, "");//兼任工作
				ps.setString(17, "");//任教课程
				ps.setString(18, "");//任教学段
				ps.setString(19, educationInfo.getCourseType().split("-")[0]);//任课课程类别
				ps.setString(20, "");//平均每周课堂教学课时数
				ps.setString(21, String.valueOf(jgId));//创建人
			}
		});
		
		return getResultInfo(true,"添加成功！");
	}
	//添加   教育教学 end

	//本科院校基本信息  start
	public ResultInfo addGxBasicInfo(final GxBasicInfo basicInfo) {
		if(basicInfo.getCardType().trim().startsWith("1") && !IDValidator.isIDCard(basicInfo.getCardNo())){
			return getResultInfo(false, "身份证信息无效！");
		}
		
		final Map<String, Object>  teacher = getTeacherByCard(basicInfo.getCardNo().trim());
		
		final Long jgId = getJgIdByName(basicInfo.getSchoolName(), basicInfo.getSchoolCode());
		if(jgId == null){
			return getResultInfo(false,"学校信息不存在！");
		}
		
		if(teacher != null){
			if(teacher.get("jgId") == null || jgId != (Long)teacher.get("jgId")){
				return getResultInfo(false,"教师和学校不对应！");
			}
			return updateGxBasicInfo(basicInfo);
		}
		
		final String eduid = getEduId();//教育Id需要重写
		
		StringBuffer sql = new StringBuffer();
		sql.append(
				"insert into TB_BIZ_JZGJBXX(JSID,XXJGID,EDU_ID,SFZZ,SFSFLZYBY,SFSGTJZYPYPX,SFYTSJYCYZS,XXJSYYNL,SFSYMFSFS,SFSTGJS,BSRLB,SHZT,SHSJ,SHJG,SFSC,CJSJ,XM,XB,SFZJLX,SFZJH,CSRQ,ZGQK,"
						+ "GJDQ,CSD,MZ,ZZMM,CJGZNY,JBXNY,JZGLY,JZGLB,SFZB,XYJG,QDHTQK,YRXS,GRZP,SFSS,SFJBZYJNDJZS,QYGZSJSC,JXJYBH,SFXLJKJYJS,CSTJQSNY,SFSGXQJYZYPYPX,SFXQJYZYBY,SFWCBTX,GGJSLX) ")
				.append("values (SEQ_TB_BIZ_JZGJBXX.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {

				ps.setLong(1, jgId);//机构Id
				ps.setString(2, eduid);
				ps.setString(3, "1");//在职
				ps.setString(4, "");//是否全日制特殊教育专业毕业
				ps.setString(5, "");//是否受过特教专业培养培训
				ps.setString(6, "");//是否有特殊教育从业证书
				ps.setString(7, "");//信息技术应用能力
				ps.setString(8, "");//是否属于免费（公费）师范生
				ps.setString(9, "");//是否参加基层服务项目
				ps.setString(10, "3");
				ps.setString(11, "01");
				ps.setTimestamp(12, new Timestamp(System.currentTimeMillis()));
				ps.setString(13, "0");//审核结果
				ps.setString(14, "0");
				ps.setTimestamp(15, new Timestamp(System.currentTimeMillis()));
				ps.setString(16, basicInfo.getTeacherName());//姓名
				ps.setString(17, basicInfo.getSex().split("-")[0]);//性别
				ps.setString(18, basicInfo.getCardType().split("-")[0]);//身份证类型
				ps.setString(19, basicInfo.getCardNo());//身份证号
				ps.setString(20, basicInfo.getBirthday());//出生日期
				ps.setString(21, basicInfo.getPeopleStatus().split("-")[0]);//人员状态
				ps.setString(22, basicInfo.getCountryArea().split("-")[0]);//国籍/地区
				ps.setString(23, basicInfo.getHomeSpace().split("-")[0]);//出生地
				ps.setString(24, basicInfo.getVolk().split("-")[0]);//民族
				ps.setString(25, ParamUtil.getFieldValue(basicInfo.getPoliticalStatus()));//政治面貌
				ps.setString(26, basicInfo.getWorkDate());//参加工作年月
				ps.setString(27, basicInfo.getSchoolDate());//进本校年月
				ps.setString(28, basicInfo.getTeacherSource().split("-")[0]);//教职工来源
				ps.setString(29, basicInfo.getTeacherType().split("-")[0]);//教职工类别
				ps.setString(30, basicInfo.getIsRank().split("-")[0]);//是否在编
				ps.setString(31, ParamUtil.getFieldValue(basicInfo.getCollegeType()));//学缘结构
				ps.setString(32, basicInfo.getContractStatus().split("-")[0]);//签订合同情况
				ps.setString(33, basicInfo.getUsePeopleType().split("-")[0]);//用人形式
				ps.setString(34, basicInfo.getSchoolCode().trim()+"/"+basicInfo.getCardNo().trim()+".jpg");//个人照片
				ps.setString(35, "");//是否“双师型”教师
				ps.setString(36, "");//是否具备职业技能等级证书
				ps.setString(37, "");//企业工作(实践)时长
				ps.setString(38, "");//继续教育编号
				ps.setString(39, "");//是否心理健康教育教师
				ps.setString(40, "");//从事特教起始年月
				ps.setString(41, "");//是否受过特教专业培养培训
				ps.setString(42, "");//是否全日制特殊教育专业毕业
				ps.setLong(43, 0);
				ps.setString(44, "");//骨干教师类型
			}
		});
		
		return getResultInfo(true,"添加成功！");
	}

	public ResultInfo updateGxBasicInfo(GxBasicInfo basicInfo) {
		final Map<String, Object>  teacher = getTeacherByCard(basicInfo.getCardNo().trim());
		if(teacher == null){
			return getResultInfo(false,"没有指定的教师信息，请检查身份证号是否正确！");
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jsid", teacher.get("jsId"));
//		params.put("xm", basicInfo.getTeacherName());
		params.put("gjdq", basicInfo.getCountryArea().split("-")[0]);
		params.put("csd", basicInfo.getHomeSpace().split("-")[0]);
		params.put("mz", basicInfo.getVolk().split("-")[0]);
		params.put("xb", basicInfo.getSex().split("-")[0]);
		params.put("zzmm", ParamUtil.getFieldValue(basicInfo.getPoliticalStatus()));
		params.put("cjgzny", basicInfo.getWorkDate());
		params.put("jbxny", basicInfo.getSchoolDate());
		params.put("jzgly", basicInfo.getTeacherSource().split("-")[0]);
		params.put("jzglb", basicInfo.getTeacherType().split("-")[0]);
		params.put("sfzb", basicInfo.getIsRank().split("-")[0]);
		params.put("csrq", basicInfo.getBirthday());
		params.put("yrxs", basicInfo.getUsePeopleType().split("-")[0]);
		//params.put("grzp", "");
		params.put("xyjg", ParamUtil.getFieldValue(basicInfo.getCollegeType()));
//		params.put("sfss", teacher.get("jsId"));
		params.put("qdhtqk", basicInfo.getContractStatus().split("-")[0]);
//		params.put("sfjbzyjndjzs", teacher.get("jsId"));
//		params.put("qygzsjsc", teacher.get("jsId"));
//		params.put("sfwcbtx", teacher.get("jsId"));
		try {
			ISqlElement se = processSql(params, "WebServiceExt.updateGxBasicInfo.update");
			int count = getJdbcTemplate().update(se.getSql(), se.getParams());
			if(count > 0){
				return getResultInfo(true,"更新成功！");
			}
		} catch (Exception e) {
			logger.error("WebServiceExt updateContactInfo is error!", e);
		}
		return getResultInfo(false,"更新失败！");
	}
	//本科院校基本信息  end
	
	//高职院校基本信息 start
	public ResultInfo addGzBasicInfo(final GzBasicInfo basicInfo) {
		if(basicInfo.getCardType().trim().startsWith("1") && !IDValidator.isIDCard(basicInfo.getCardNo())){
			return getResultInfo(false, "身份证信息无效！");
		}
		
		final Map<String, Object>  teacher = getTeacherByCard(basicInfo.getCardNo().trim());
		
		final Long jgId = getJgIdByName(basicInfo.getSchoolName(), basicInfo.getSchoolCode());
		if(jgId == null){
			return getResultInfo(false,"学校信息不存在！");
		}
		
		if(teacher != null){
			if(teacher.get("jgId") == null || jgId != (Long)teacher.get("jgId")){
				return getResultInfo(false,"教师和学校不对应！");
			}
			return updateGzBasicInfo(basicInfo);
		}
		
		final String eduid = getEduId();//教育Id需要重写
		
		StringBuffer sql = new StringBuffer();
		sql.append(
				"insert into TB_BIZ_JZGJBXX(JSID,XXJGID,EDU_ID,SFZZ,SFSFLZYBY,SFSGTJZYPYPX,SFYTSJYCYZS,XXJSYYNL,SFSYMFSFS,SFSTGJS,BSRLB,SHZT,SHSJ,SHJG,SFSC,CJSJ,XM,XB,SFZJLX,SFZJH,CSRQ,ZGQK,"
						+ "GJDQ,CSD,MZ,ZZMM,CJGZNY,JBXNY,JZGLY,JZGLB,SFZB,XYJG,QDHTQK,YRXS,GRZP,SFSS,SFJBZYJNDJZS,QYGZSJSC,JXJYBH,SFXLJKJYJS,CSTJQSNY,SFSGXQJYZYPYPX,SFXQJYZYBY,SFWCBTX,GGJSLX) ")
				.append("values (SEQ_TB_BIZ_JZGJBXX.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		this.getJdbcTemplate().update(sql.toString(), new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {

				ps.setLong(1, jgId);//机构Id
				ps.setString(2, eduid);
				ps.setString(3, "1");//在职
				ps.setString(4, "");//是否全日制特殊教育专业毕业
				ps.setString(5, "");//是否受过特教专业培养培训
				ps.setString(6, "");//是否有特殊教育从业证书
				ps.setString(7, "");//信息技术应用能力
				ps.setString(8, "");//是否属于免费（公费）师范生
				ps.setString(9, "");//是否参加基层服务项目
				ps.setString(10, "3");
				ps.setString(11, "01");
				ps.setTimestamp(12, new Timestamp(System.currentTimeMillis()));
				ps.setString(13, "0");//审核结果
				ps.setString(14, "0");
				ps.setTimestamp(15, new Timestamp(System.currentTimeMillis()));
				ps.setString(16, basicInfo.getTeacherName());//姓名
				ps.setString(17, basicInfo.getSex().split("-")[0]);//性别
				ps.setString(18, basicInfo.getCardType().split("-")[0]);//身份证类型
				ps.setString(19, basicInfo.getCardNo());//身份证号
				ps.setString(20, basicInfo.getBirthday());//出生日期
				ps.setString(21, basicInfo.getPeopleStatus().split("-")[0]);//人员状态
				ps.setString(22, basicInfo.getCountryArea().split("-")[0]);//国籍/地区
				ps.setString(23, basicInfo.getHomeSpace().split("-")[0]);//出生地
				ps.setString(24, basicInfo.getVolk().split("-")[0]);//民族
				ps.setString(25, ParamUtil.getFieldValue(basicInfo.getPoliticalStatus()));//政治面貌
				ps.setString(26, basicInfo.getWorkDate());//参加工作年月
				ps.setString(27, basicInfo.getSchoolDate());//进本校年月
				ps.setString(28, basicInfo.getTeacherSource().split("-")[0]);//教职工来源
				ps.setString(29, basicInfo.getTeacherType().split("-")[0]);//教职工类别
				ps.setString(30, basicInfo.getIsRank().split("-")[0]);//是否在编
				ps.setString(31, "");//学缘结构
				ps.setString(32, basicInfo.getContractStatus().split("-")[0]);//签订合同情况
				ps.setString(33, basicInfo.getUsePeopleType().split("-")[0]);//用人形式
				ps.setString(34, basicInfo.getSchoolCode().trim()+"/"+basicInfo.getCardNo().trim()+".jpg");//个人照片
				ps.setString(35, basicInfo.getIsDouble().split("-")[0]);//是否“双师型”教师
				ps.setString(36, basicInfo.getIsSkillHonour().split("-")[0]);//是否具备职业技能等级证书
				ps.setString(37, basicInfo.getTimeLength().split("-")[0]);//企业工作(实践)时长
				ps.setString(38, "");//继续教育编号
				ps.setString(39, "");//是否心理健康教育教师
				ps.setString(40, "");//从事特教起始年月
				ps.setString(41, "");//是否受过特教专业培养培训
				ps.setString(42, "");//是否全日制特殊教育专业毕业
				ps.setLong(43, 0);
				ps.setString(44, "");//骨干教师类型
			}
		});
		
		return getResultInfo(true,"添加成功！");
	}
	
	public ResultInfo updateGzBasicInfo(GzBasicInfo basicInfo) {
		final Map<String, Object>  teacher = getTeacherByCard(basicInfo.getCardNo().trim());
		if(teacher == null){
			return getResultInfo(false,"没有指定的教师信息，请检查身份证号是否正确！");
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jsid", teacher.get("jsId"));
//		params.put("xm", basicInfo.getTeacherName());
		params.put("gjdq", basicInfo.getCountryArea().split("-")[0]);
		params.put("csd", basicInfo.getHomeSpace().split("-")[0]);
		params.put("mz", basicInfo.getVolk().split("-")[0]);
		params.put("xb", basicInfo.getSex().split("-")[0]);
		params.put("zzmm", ParamUtil.getFieldValue(basicInfo.getPoliticalStatus()));
		params.put("cjgzny", basicInfo.getWorkDate());
		params.put("jbxny", basicInfo.getSchoolDate());
		params.put("jzgly", basicInfo.getTeacherSource().split("-")[0]);
		params.put("jzglb", basicInfo.getTeacherType().split("-")[0]);
		params.put("sfzb", basicInfo.getIsRank().split("-")[0]);
		params.put("csrq", basicInfo.getBirthday());
		params.put("yrxs", basicInfo.getUsePeopleType().split("-")[0]);
//		params.put("grzp", "");
		params.put("sfss", basicInfo.getIsDouble().split("-")[0]);
		params.put("qdhtqk", basicInfo.getContractStatus().split("-")[0]);
		params.put("sfjbzyjndjzs", basicInfo.getIsSkillHonour().split("-")[0]);
		params.put("qygzsjsc", basicInfo.getTimeLength().split("-")[0]);
//		params.put("sfwcbtx", "");
		try {
			ISqlElement se = processSql(params, "WebServiceExt.updateGzBasicInfo.update");
			int count = getJdbcTemplate().update(se.getSql(), se.getParams());
			if(count > 0){
				return getResultInfo(true,"更新成功！");
			}
		} catch (Exception e) {
			logger.error("WebServiceExt updateContactInfo is error!", e);
		}
		return getResultInfo(false,"更新失败！");
	}
	//高职院校基本信息 end
	/** 业务方法  end ***/
}
