package com.becom.jspt2016.webservice.entity;

/**
 * 返回状态信息
 * @author fmx
 *
 */
public class ResultInfo {

	/**
	 * true 成功  false 失败
	 */
	private Boolean flag;

	/**
	 * 错误描述
	 */
	private String msg;

	public Boolean getFlag() {
		return flag;
	}

	public void setFlag(Boolean flag) {
		this.flag = flag;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
