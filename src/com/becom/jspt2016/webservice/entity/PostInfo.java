/**
 * PostInfo.java
 * com.becom.jspt2016.webservice.entity
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webservice.entity;

/**
 * 岗位信息
 * <p>
 * 岗位聘任信息
 *
 * @author   zhangchunming
 * @Date	 2016年11月15日 	 
 */
public class PostInfo{
	/**
	 * 学校代码
	 */
	private String schoolCode;
	/**
	 * 学校名称
	 */
	private String schoolName;
	/**
	 * 姓名
	 */
	private String teacherName;
	/**
	 * 身份证号
	 */
	private String cardNo;
	/**
	 * 岗位类型
	 */
	private String gwlx;
	/**
	 * 岗位等级
	 */
	private String gwdj;
	/**
	 * 聘任开始年月
	 */
	private String prksny;
	/**
	 *是否双肩挑
	 */
	private String sfsjt;
	/**
	 * 双肩挑岗位类别
	 */
//	private String sjtgwlb;
	/**
	 * 双肩挑岗位等级
	 */
//	private String sjtgwdj;
	/**
	 *是否为辅导员
	 */
	private String sfwfdy;
	/**
	 *党政职务
	 */
	private String dzzw;
	/**
	 * 任职开始年月
	 */
	private String rzksny;
	/**
	 * 党政级别
	 */
	private String dzjb;

	public String getGwlx() {
		return gwlx;
	}

	public void setGwlx(String gwlx) {
		this.gwlx = gwlx;
	}

	public String getGwdj() {
		return gwdj;
	}

	public void setGwdj(String gwdj) {
		this.gwdj = gwdj;
	}

	public String getPrksny() {
		return prksny;
	}

	public void setPrksny(String prksny) {
		this.prksny = prksny;
	}

	public String getSfsjt() {
		return sfsjt;
	}

	public void setSfsjt(String sfsjt) {
		this.sfsjt = sfsjt;
	}

	/*
	public String getSjtgwlb() {
		return sjtgwlb;
	}

	public void setSjtgwlb(String sjtgwlb) {
		this.sjtgwlb = sjtgwlb;
	}

	public String getSjtgwdj() {
		return sjtgwdj;
	}

	public void setSjtgwdj(String sjtgwdj) {
		this.sjtgwdj = sjtgwdj;
	}
	*/

	public String getSfwfdy() {
		return sfwfdy;
	}

	public void setSfwfdy(String sfwfdy) {
		this.sfwfdy = sfwfdy;
	}

	public String getDzzw() {
		return dzzw;
	}

	public void setDzzw(String dzzw) {
		this.dzzw = dzzw;
	}

	public String getRzksny() {
		return rzksny;
	}

	public void setRzksny(String rzksny) {
		this.rzksny = rzksny;
	}

	public String getDzjb() {
		return dzjb;
	}

	public void setDzjb(String dzjb) {
		this.dzjb = dzjb;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

}
