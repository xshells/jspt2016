/**
 * WorkInfo.java
 * com.becom.jspt2016.webservice.entity
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webservice.entity;

/**
 * 工作信息
 * <p>
 * 工作经历信息
 *
 * @author   zhangchunming
 * @Date	 2016年11月15日 	 
 */
public class WorkInfo{
	/**
	 * 学校代码
	 */
	private String schoolCode;
	/**
	 * 学校名称
	 */
	private String schoolName;
	/**
	 * 姓名
	 */
	private String teacherName;
	/**
	 * 身份证号
	 */
	private String cardNo;
	/**
	 * 任职单位名称
	 */
	private String rzdwmc;
	/**
	 * 任职开始年月
	 */
	private String rzksny;
	/**
	 * 任职结束年月
	 */
	private String rzjsny;
	/**
	 * 单位性质类别
	 */
	private String dwxzlb;
	/**
	 * 任职岗位
	 */
	private String rzgw;

	public String getRzdwmc() {
		return rzdwmc;
	}

	public void setRzdwmc(String rzdwmc) {
		this.rzdwmc = rzdwmc;
	}

	public String getRzksny() {
		return rzksny;
	}

	public void setRzksny(String rzksny) {
		this.rzksny = rzksny;
	}

	public String getRzjsny() {
		return rzjsny;
	}

	public void setRzjsny(String rzjsny) {
		this.rzjsny = rzjsny;
	}

	public String getDwxzlb() {
		return dwxzlb;
	}

	public void setDwxzlb(String dwxzlb) {
		this.dwxzlb = dwxzlb;
	}

	public String getRzgw() {
		return rzgw;
	}

	public void setRzgw(String rzgw) {
		this.rzgw = rzgw;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

}
