/**
 * GzEducationInfo.java
 * com.becom.jspt2016.webservice.entity
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.webservice.entity;

/**
 * 高职教育教学信息
 * @author   fmx
 * @Date	 2016年11月22日
 */
public class GzEducationInfo {
	/**
	 * 学校代码
	 */
	private String schoolCode;
	/**
	 * 学校名称
	 */
	private String schoolName;
	/**
	 * 姓名
	 */
	private String teacherName;
	/**
	 * 身份证号
	 */
	private String cardNo;
	/**
	 * 年度
	 */
	private String year;
	/**
	 * 学期
	 */
	private String term;
	/**
	 * 任课情况
	 */
	private String course;
	/**
	 * 任课课程类别
	 */
	private String courseType;
	/**
	 * 现主要从事学科领域
	 */
	private String research;
	/**
	 * 课程教学课时数
	 */
	private Integer classHour;

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public String getCourseType() {
		return courseType;
	}

	public void setCourseType(String courseType) {
		this.courseType = courseType;
	}

	public String getResearch() {
		return research;
	}

	public void setResearch(String research) {
		this.research = research;
	}

	public Integer getClassHour() {
		return classHour;
	}

	public void setClassHour(Integer classHour) {
		this.classHour = classHour;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
}
