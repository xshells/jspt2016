/**
 * BasicInfo_gz.java
 * com.becom.jspt2016.webservice.entity
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webservice.entity;

/**
 * 高职基本信息
 * 
 * @author   wangxiaolei
 * @Date	 2016年11月15日 	 
 */
public class GzBasicInfo {
	/**
	 * 学校代码
	 */
	private String schoolCode;
	/**
	 * 学校名称
	 */
	private String schoolName;
	/**
	 * 姓名
	 */
	private String teacherName;
	/**
	 * 身份证号
	 */
	private String cardNo;
	/**
	 * 性别
	 */
	private String sex;
	/**
	 * 国籍/地区
	 */
	private String countryArea;
	/**
	 * 身份证件类型
	 */
	private String cardType;

	/**
	 * 出生日期
	 */
	private String birthday;
	/**
	 * 出生地
	 */
	private String homeSpace;
	/**
	 * 民族
	 */
	private String volk;
	/**
	 * 政治面貌
	 */
	private String politicalStatus;
	/**
	 * 参加工作年月
	 */
	private String workDate;
	/**
	 * 进本校年月
	 */
	private String schoolDate;
	/**
	 * 教职工来源
	 */
	private String teacherSource;
	/**
	 * 教职工类别
	 */
	private String teacherType;
	/**
	 * 是否在编
	 */
	private String isRank;
	/**
	 * 用人形式
	 */
	private String usePeopleType;
	/**
	 * 签订合同状况
	 */
	private String contractStatus;
	/**
	 * 是否"双师类型"
	 */
	private String isDouble;
	/**
	 * 是否具备职业技能等级证书
	 */
	private String isSkillHonour;
	/**
	 * 企业工作(实践)时长
	 */
	private String timeLength;
	/**
	 * 人员状态
	 */
	private String peopleStatus;

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getCountryArea() {
		return countryArea;
	}

	public void setCountryArea(String countryArea) {
		this.countryArea = countryArea;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getHomeSpace() {
		return homeSpace;
	}

	public void setHomeSpace(String homeSpace) {
		this.homeSpace = homeSpace;
	}

	public String getVolk() {
		return volk;
	}

	public void setVolk(String volk) {
		this.volk = volk;
	}

	public String getPoliticalStatus() {
		return politicalStatus;
	}

	public void setPoliticalStatus(String politicalStatus) {
		this.politicalStatus = politicalStatus;
	}

	public String getWorkDate() {
		return workDate;
	}

	public void setWorkDate(String workDate) {
		this.workDate = workDate;
	}

	public String getSchoolDate() {
		return schoolDate;
	}

	public void setSchoolDate(String schoolDate) {
		this.schoolDate = schoolDate;
	}

	public String getTeacherSource() {
		return teacherSource;
	}

	public void setTeacherSource(String teacherSource) {
		this.teacherSource = teacherSource;
	}

	public String getTeacherType() {
		return teacherType;
	}

	public void setTeacherType(String teacherType) {
		this.teacherType = teacherType;
	}

	public String getIsRank() {
		return isRank;
	}

	public void setIsRank(String isRank) {
		this.isRank = isRank;
	}

	public String getUsePeopleType() {
		return usePeopleType;
	}

	public void setUsePeopleType(String usePeopleType) {
		this.usePeopleType = usePeopleType;
	}

	public String getContractStatus() {
		return contractStatus;
	}

	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}

	public String getIsDouble() {
		return isDouble;
	}

	public void setIsDouble(String isDouble) {
		this.isDouble = isDouble;
	}

	public String getIsSkillHonour() {
		return isSkillHonour;
	}

	public void setIsSkillHonour(String isSkillHonour) {
		this.isSkillHonour = isSkillHonour;
	}

	public String getTimeLength() {
		return timeLength;
	}

	public void setTimeLength(String timeLength) {
		this.timeLength = timeLength;
	}

	public String getPeopleStatus() {
		return peopleStatus;
	}

	public void setPeopleStatus(String peopleStatus) {
		this.peopleStatus = peopleStatus;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

}
