/**
 * LearnInfo.java
 * com.becom.jspt2016.webservice.entity
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/

package com.becom.jspt2016.webservice.entity;

/**
 * 学习信息
 * <p>
 * 学习经历信息
 *
 * @author   zhangchunming
 * @Date	 2016年11月15日 	 
 */
public class LearnInfo{
	/**
	 * 学校代码
	 */
	private String schoolCode;
	/**
	 * 学校名称
	 */
	private String schoolName;
	/**
	 * 姓名
	 */
	private String teacherName;
	/**
	 * 身份证号
	 */
	private String cardNo;
	/**
	 * 获得学历
	 */
	private String hdxl;
	/**
	 * 获得学历的国家(地区)
	 */
	private String hdxldgjdq;
	/**
	 * 获得学历的院校或机构
	 */
	private String hdxldyxhjg;
	/**
	 * 所学专业
	 */
	private String sxzy;
	/**
	 * 入学年月
	 */
	private String rxny;
	/**
	 * 毕业年月
	 */
	private String byny;
	/**
	 * 学位层次
	 */
	private String xwcc;
	/**
	 * 学位名称
	 */
	private String xwmc;
	/**
	 * 获得学位的国家(地区)
	 */
	private String hdxwdgjdq;
	/**
	 * 获得学位的院校或机构
	 */
	private String hdxwdyxhjg;
	/**
	 * 学位授予年月
	 */
	private String xwsyny;
	/**
	 * 学习方式
	 */
	private String xxfs;
	/**
	 * 在学单位类别
	 */
	private String zxdwlb;

	public String getHdxl() {
		return hdxl;
	}

	public void setHdxl(String hdxl) {
		this.hdxl = hdxl;
	}

	public String getHdxldgjdq() {
		return hdxldgjdq;
	}

	public void setHdxldgjdq(String hdxldgjdq) {
		this.hdxldgjdq = hdxldgjdq;
	}

	public String getHdxldyxhjg() {
		return hdxldyxhjg;
	}

	public void setHdxldyxhjg(String hdxldyxhjg) {
		this.hdxldyxhjg = hdxldyxhjg;
	}

	public String getSxzy() {
		return sxzy;
	}

	public void setSxzy(String sxzy) {
		this.sxzy = sxzy;
	}

	public String getRxny() {
		return rxny;
	}

	public void setRxny(String rxny) {
		this.rxny = rxny;
	}

	public String getByny() {
		return byny;
	}

	public void setByny(String byny) {
		this.byny = byny;
	}

	public String getXwcc() {
		return xwcc;
	}

	public void setXwcc(String xwcc) {
		this.xwcc = xwcc;
	}

	public String getXwmc() {
		return xwmc;
	}

	public void setXwmc(String xwmc) {
		this.xwmc = xwmc;
	}

	public String getHdxwdgjdq() {
		return hdxwdgjdq;
	}

	public void setHdxwdgjdq(String hdxwdgjdq) {
		this.hdxwdgjdq = hdxwdgjdq;
	}

	public String getHdxwdyxhjg() {
		return hdxwdyxhjg;
	}

	public void setHdxwdyxhjg(String hdxwdyxhjg) {
		this.hdxwdyxhjg = hdxwdyxhjg;
	}

	public String getXwsyny() {
		return xwsyny;
	}

	public void setXwsyny(String xwsyny) {
		this.xwsyny = xwsyny;
	}

	public String getXxfs() {
		return xxfs;
	}

	public void setXxfs(String xxfs) {
		this.xxfs = xxfs;
	}

	public String getZxdwlb() {
		return zxdwlb;
	}

	public void setZxdwlb(String zxdwlb) {
		this.zxdwlb = zxdwlb;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

}
