/**
 * WebServlet.java
 * com.becom.jspt2016.webservice
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.webservice;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;

import org.apache.cxf.Bus;
import org.apache.cxf.BusFactory;
import org.apache.cxf.jaxws.JaxWsServerFactoryBean;
import org.apache.cxf.transport.servlet.CXFNonSpringServlet;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.becom.jspt2016.webservice.soap.BaseWebImp;

/**
 * 服务发布类
 * 现使用cxf2.7版本与nest框架集成出现问题，所以采用不与spring集成的方式实现
 * @author fmx
 *
 */
public class WebServlet extends CXFNonSpringServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void loadBus(ServletConfig sc) {
		super.loadBus(sc);
		
		Bus bus = getBus();  
		BusFactory.setDefaultBus(bus); 
		
		//获取spring上下文
		ServletContext servletContext = sc.getServletContext();
		ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		
		//获取业务bean
		Object objService = ctx.getBean("webServiceExt");
		
		//创建本科院校接口服务
		JaxWsServerFactoryBean serverGxFactoryBean = new JaxWsServerFactoryBean();
		serverGxFactoryBean.setServiceClass(IGxWeb.class);
		serverGxFactoryBean.setAddress("/bk"); 
		serverGxFactoryBean.setServiceBean(new BaseWebImp(objService));  
		serverGxFactoryBean.create();
		
		//创建高职院校接口服务
		JaxWsServerFactoryBean serverGzFactoryBean = new JaxWsServerFactoryBean();
		serverGzFactoryBean.setServiceClass(IGzWeb.class);
		serverGzFactoryBean.setAddress("/gz"); 
		serverGzFactoryBean.setServiceBean(new BaseWebImp(objService));  
		serverGzFactoryBean.create();
	}

}
