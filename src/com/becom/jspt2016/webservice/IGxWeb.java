/**
 * IGxWeb.java
 * com.becom.jspt2016.webservice
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.webservice;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import com.becom.jspt2016.webservice.entity.GxBasicInfo;
import com.becom.jspt2016.webservice.entity.GxEducationInfo;
import com.becom.jspt2016.webservice.entity.ResultInfo;
/**
 * 本科接口
 * @author fmx
 *
 */
@WebService
@SOAPBinding(style=Style.RPC)
public interface IGxWeb extends IBaseWeb{
	
	/**
	 * 维护基本信息
	 * @return
	 */
	public ResultInfo updateGxBasicInfo(@WebParam(name="basicInfo")GxBasicInfo basicInfo);
	
	
	/**
	 * 添加教育教学
	 * @return
	 */
	public ResultInfo addGxEducationInfo(@WebParam(name="educationInfo")GxEducationInfo educationInfo);
}
