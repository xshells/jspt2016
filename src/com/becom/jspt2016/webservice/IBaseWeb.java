/**
 * IBaseWeb.java
 * com.becom.jspt2016.webservice
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.webservice;

import javax.jws.WebParam;
import javax.jws.WebService;

import com.becom.jspt2016.webservice.entity.ContactInfo;
import com.becom.jspt2016.webservice.entity.LearnInfo;
import com.becom.jspt2016.webservice.entity.MajorInfo;
import com.becom.jspt2016.webservice.entity.OverseasStudyInfo;
import com.becom.jspt2016.webservice.entity.PersonnelInfo;
import com.becom.jspt2016.webservice.entity.PostInfo;
import com.becom.jspt2016.webservice.entity.ResultInfo;
import com.becom.jspt2016.webservice.entity.SkillInfo;
import com.becom.jspt2016.webservice.entity.TeacherQuaInfo;
import com.becom.jspt2016.webservice.entity.WorkInfo;
import com.becom.jspt2016.webservice.entity.YearTestInfo;
/**
 * 基础接口
 * @author fmx
 *
 */
@WebService
public interface IBaseWeb {
	
	/**
	 * 添加工作经历
	 * @return
	 */
	public ResultInfo addWorkInfo(@WebParam(name="workInfo")WorkInfo workInfo);
	
	/**
	 * 添加学习经历
	 * @return
	 */
	public ResultInfo addLearnInfo(@WebParam(name="learnInfo")LearnInfo learnInfo);
	
	/**
	 * 添加岗位聘任
	 * @return
	 */
	public ResultInfo addPostInfo(@WebParam(name="postInfo")PostInfo postInfo);
	
	/**
	 * 添加专业职务聘任
	 * @return
	 */
	public ResultInfo addMajorInfo(@WebParam(name="majorInfo")MajorInfo majorInfo);
	
	/**
	 * 添加年度考核信息
	 * @return
	 */
	public ResultInfo addYearTestInfo(@WebParam(name="yearTestInfo")YearTestInfo yearTestInfo);
	
	/**
	 * 添加教师资格
	 * @return
	 */
	public ResultInfo addTeacherQuaInfo(@WebParam(name="teacherQuaInfo")TeacherQuaInfo teacherQuaInfo);
	
	/**
	 * 添加入选人才项目信息
	 * @return
	 */
	public ResultInfo addPersonnelInfo(@WebParam(name="personnelInfo")PersonnelInfo personnelInfo);
	
	/**
	 * 添加技能信息
	 * @return
	 */
	public ResultInfo addSkillInfo(@WebParam(name="skillInfo")SkillInfo skillInfo);
	
	/**
	 * 维护海外研修信息
	 * @return
	 */
	public ResultInfo updateOverseaStudyInfo(@WebParam(name="overseaStudyInfo")OverseasStudyInfo overseaStudyInfo);
	
	/**
	 * 维护联系方式
	 * @return
	 */
	public ResultInfo updateContactInfo(@WebParam(name="contactInfo")ContactInfo contactInfo);
}
