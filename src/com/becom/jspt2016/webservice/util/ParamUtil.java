/**
 * ParamUtil.java
 * com.becom.jspt2016.webservice.util
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.webservice.util;

import java.lang.reflect.Method;

import com.becom.jspt2016.webservice.entity.ContactInfo;
import com.becom.jspt2016.webservice.entity.GxBasicInfo;
import com.becom.jspt2016.webservice.entity.GzBasicInfo;
import com.becom.jspt2016.webservice.entity.LearnInfo;
import com.becom.jspt2016.webservice.entity.ResultInfo;
import com.becom.jspt2016.webservice.entity.WorkInfo;
import com.becom.jspt2016.webservice.entity.YearTestInfo;
/**
 * 参数工具类
 * @author fmx
 *
 */
public class ParamUtil {
	
	/**
	 * 字段长度验证
	 * @param data
	 * @return
	 */
	public static ResultInfo isValid(Object data){
		ResultInfo result = new ResultInfo();
		result.setFlag(false);
		
		if(data instanceof ContactInfo){
			ContactInfo obj = (ContactInfo)data;
			if(!isEmailValid(obj.getEmail())){
				result.setMsg("邮箱格式无效！");
				return result;
			}
			if(!isPhone(obj.getPhone())){
				result.setMsg("电话号码无效！");
				return result;
			}
		}else if(data instanceof YearTestInfo){
			YearTestInfo obj = (YearTestInfo)data;
			if(obj.getKhdwName().length() > 40){
				result.setMsg("考核单位名称超出长度！");
				return result;
			}
		}else if(data instanceof WorkInfo){
			WorkInfo obj = (WorkInfo)data;
			if(obj.getRzdwmc().length() > 45){
				result.setMsg("任职单位名称超出长度！");
				return result;
			}
			
			if(obj.getRzgw().length() > 45){
				result.setMsg("任职岗位超出长度！");
				return result;
			}
		}else if(data instanceof LearnInfo){
			LearnInfo obj = (LearnInfo)data;
			if(obj.getSxzy().length() > 45){
				result.setMsg("所学专业超出长度！");
				return result;
			}
			
			if(obj.getHdxldyxhjg().length() > 45){
				result.setMsg("获得学历的院校或机构超出长度！");
				return result;
			}
			
			if(obj.getHdxwdyxhjg().length() > 45){
				result.setMsg("获得学位的院校或机构超出长度！");
				return result;
			}
			
		}else if(data instanceof GxBasicInfo){
			GxBasicInfo obj = (GxBasicInfo)data;
			if(obj.getTeacherName().length() > 45){
				result.setMsg("姓名超出长度！");
				return result;
			}
		}else if(data instanceof GzBasicInfo){
			GzBasicInfo obj = (GzBasicInfo)data;
			if(obj.getTeacherName().length() > 45){
				result.setMsg("姓名超出长度！");
				return result;
			}
		}
		
		result.setFlag(true);
		return result;
	}

	/**
	 * 判断是否纯数字
	 * @param phone
	 * @return
	 */
	public static boolean isPhone(String phone){
		if (!phone.matches("[0-9]+")) {  
            return false;  
        }
        return true;
	}
	
	/**
	 * 验证邮箱
	 * @param email
	 * @return
	 */
	public static boolean isEmailValid(String email) {  
        if (!email.matches("[\\w\\.\\-]+@([\\w\\-]+\\.)+[\\w\\-]+")) {  
            return false;  
        }
        return true;
	}
	
	/**
	 * 获取多选字段的值
	 * @param fieldValue
	 * @return
	 */
	public static String getFieldValue(String fieldValue){
		if(fieldValue == null || "".equals(fieldValue)){
			return fieldValue;
		}
		
		if(fieldValue.contains("，")){
			fieldValue = fieldValue.replaceAll("，", ",");
		}
		
		if(fieldValue.contains(",")){
			StringBuilder sb = new StringBuilder();
			String[] fields = fieldValue.split(",");
			for(int i=0;i<fields.length;i++){
				if(fields[i].contains("-")){
					sb.append(",").append(fields[i].split("-")[0]);
				}
			}
			
			return sb.toString().substring(1);
		}else{
			if(fieldValue.contains("-")){
				return fieldValue.split("-")[0];
			}else{
				return fieldValue;
			}
		}
		
	}
	
	/**
	 * 参数验证
	 * @param obj
	 * @throws Exception
	 */
	public static ResultInfo validateParam(Object obj) throws Exception{
		boolean flag = false;
		if(obj != null){
			StringBuffer params = new StringBuffer();
			Method[] methods = obj.getClass().getDeclaredMethods();
			for(int i=0;i<methods.length;i++){
				Method m = methods[i];
				if(m.getName().startsWith("get")){
					String fieldName = getNameByMethod(m.getName());
					Object field = m.invoke(obj);
					if(field == null){
						params.append(",").append(fieldName);
						flag = true;
					}else{
						if(field instanceof String && ("".equals(field.toString()) || "?".equals(field.toString()))){
							params.append(",").append(fieldName);
							flag = true;
						}
					}
				}
			}
			
			if(flag){
				ResultInfo result = new ResultInfo();
				result.setFlag(false);
				result.setMsg(params.toString().substring(1) + "属性值为空或数据异常，请确保为非空有效值");
				return result;
			}
		}else{
			ResultInfo result = new ResultInfo();
			result.setFlag(false);
			result.setMsg("参数不能为空");
			return result;
		}
		
		return null;
	}
	
	public static String getNameByMethod(String methodName){
		if(methodName != null){
			return methodName.substring(3,4).toLowerCase() + methodName.substring(4);
		}else{
			return "";
		}
	}
	
	public static void main(String[] args){
		System.out.println(isPhone("01821032766"));
	}
}
