/**
 * BaseWebImp.java
 * com.becom.jspt2016.webservice.soap
 * Copyright (c) 2016, 北京聚智未来科技有限公司版权所有.
*/
package com.becom.jspt2016.webservice.soap;

import org.apache.log4j.Logger;

import com.becom.jspt2016.webservice.IGxWeb;
import com.becom.jspt2016.webservice.IGzWeb;
import com.becom.jspt2016.webservice.entity.ContactInfo;
import com.becom.jspt2016.webservice.entity.GxBasicInfo;
import com.becom.jspt2016.webservice.entity.GxEducationInfo;
import com.becom.jspt2016.webservice.entity.GzBasicInfo;
import com.becom.jspt2016.webservice.entity.GzEducationInfo;
import com.becom.jspt2016.webservice.entity.LearnInfo;
import com.becom.jspt2016.webservice.entity.MajorInfo;
import com.becom.jspt2016.webservice.entity.OverseasStudyInfo;
import com.becom.jspt2016.webservice.entity.PersonnelInfo;
import com.becom.jspt2016.webservice.entity.PostInfo;
import com.becom.jspt2016.webservice.entity.ResultInfo;
import com.becom.jspt2016.webservice.entity.SkillInfo;
import com.becom.jspt2016.webservice.entity.TeacherQuaInfo;
import com.becom.jspt2016.webservice.entity.WorkInfo;
import com.becom.jspt2016.webservice.entity.YearTestInfo;
import com.becom.jspt2016.webservice.service.IWebServiceExt;
import com.becom.jspt2016.webservice.util.ParamUtil;

/**
 * 公共实现类
 * @author fmx
 *
 */
public class BaseWebImp implements IGzWeb,IGxWeb{
	protected Logger logger = Logger.getLogger(BaseWebImp.class);
	
	private IWebServiceExt webServiceExt;
	
	public BaseWebImp(Object webServiceExt){
		this.webServiceExt = (IWebServiceExt)webServiceExt;
	}

	public ResultInfo addWorkInfo(WorkInfo workInfo) {
		return addData(workInfo);
	}

	public ResultInfo addLearnInfo(LearnInfo learnInfo) {
		return addData(learnInfo);
	}

	public ResultInfo addPostInfo(PostInfo postInfo) {
		return addData(postInfo);
	}

	public ResultInfo addMajorInfo(MajorInfo majorInfo) {
		return addData(majorInfo);
	}

	public ResultInfo addYearTestInfo(YearTestInfo yearTestInfo) {
		return addData(yearTestInfo);
	}

	public ResultInfo addTeacherQuaInfo(TeacherQuaInfo teacherQuaInfo) {
		return addData(teacherQuaInfo);
	}

	public ResultInfo addPersonnelInfo(PersonnelInfo personnelInfo) {
		return addData(personnelInfo);
	}


	public ResultInfo addSkillInfo(SkillInfo skillInfo) {
		return addData(skillInfo);
	}

	public ResultInfo updateOverseaStudyInfo(OverseasStudyInfo overseaStudyInfo) {
		return addData(overseaStudyInfo);
	}

	public ResultInfo updateContactInfo(ContactInfo contactInfo) {
		return addData(contactInfo);
	}

	/** 本科院校 start **/
	public ResultInfo addGxEducationInfo(GxEducationInfo educationInfo) {
		return addData(educationInfo);
	}
	
	public ResultInfo updateGxBasicInfo(GxBasicInfo basicInfo) {
		return addData(basicInfo);
	}
	/** 本科院校 end **/
	
	/** 高职 start **/
	public ResultInfo addGzEducationInfo(GzEducationInfo educationInfo) {
		return addData(educationInfo);
	}
	
	public ResultInfo updateGzBasicInfo(GzBasicInfo basicInfo) {
		return addData(basicInfo);
	}
	/** 高职 end **/
	
	/**
	 * 处理数据，在这里做参数验证
	 * @param obj
	 * @param type
	 * @return
	 */
	public ResultInfo addData(Object obj){
		ResultInfo result = new ResultInfo();
		try {
			ResultInfo vresult = ParamUtil.validateParam(obj);
			if(vresult != null){
				return vresult;
			}
			
			ResultInfo resultV = ParamUtil.isValid(obj);
			if(!resultV.getFlag()){
				return resultV;
			}
			
			return addBase(obj);
		} catch (Exception e) {
			result.setFlag(false);
			result.setMsg("服务器出错了！");
			logger.error("校验参数异常！", e);
		}
		
		return result;
	}
	
	//公共添加实现
	public ResultInfo addBase(Object data){
		ResultInfo result = new ResultInfo();
		if(data instanceof ContactInfo){
			return webServiceExt.addContactInfo((ContactInfo)data);
		}else if(data instanceof OverseasStudyInfo){
			return webServiceExt.addOverseasStudyInfo((OverseasStudyInfo)data);
		}else if(data instanceof SkillInfo){
			return webServiceExt.addSkillInfo((SkillInfo)data);
		}else if(data instanceof PersonnelInfo){
			return webServiceExt.addPersonnelInfo((PersonnelInfo)data);
		}else if(data instanceof TeacherQuaInfo){
			return webServiceExt.addTeacherQuaInfo((TeacherQuaInfo)data);
		}else if(data instanceof YearTestInfo){
			return webServiceExt.addYearTestInfo((YearTestInfo)data);
		}else if(data instanceof MajorInfo){
			return webServiceExt.addMajorInfo((MajorInfo)data);
		}else if(data instanceof PostInfo){
			return webServiceExt.addPostInfo((PostInfo)data);
		}else if(data instanceof LearnInfo){
			return webServiceExt.addLearnInfo((LearnInfo)data);
		}else if(data instanceof WorkInfo){
			return webServiceExt.addWorkInfo((WorkInfo)data);
		}else if(data instanceof GxEducationInfo){
			return webServiceExt.addGxEducationInfo((GxEducationInfo)data);
		}else if(data instanceof GzEducationInfo){
			return webServiceExt.addGzEducationInfo((GzEducationInfo)data); 
		}else if(data instanceof GxBasicInfo){
			return webServiceExt.addGxBasicInfo((GxBasicInfo)data);
		}else if(data instanceof GzBasicInfo){
			return webServiceExt.addGzBasicInfo((GzBasicInfo)data);
		}else{
			result.setFlag(false);
			result.setMsg("传入数据有问题！");
		}
		return result;
	}

	//公共编辑实现
	
	/*
	public ResultInfo editBase(Object data){
		ResultInfo result = new ResultInfo();
		if(data instanceof ContactInfo){
			return webServiceExt.updateContactInfo((ContactInfo)data);
		}else if(data instanceof OverseasStudyInfo){
			return webServiceExt.updateOverseasStudyInfo((OverseasStudyInfo)data);
		}else if(data instanceof GxBasicInfo){
			return webServiceExt.updateGxBasicInfo((GxBasicInfo)data);
		}else if(data instanceof GzBasicInfo){
			return webServiceExt.updateGzBasicInfo((GzBasicInfo)data);
		}else{
			result.setFlag(false);
			result.setMsg("传入数据有问题！");
		}
		
		return result;
	}
	*/
}
