package com.becom.jspt2016.common;

public class Constant {

	/**
	 */
	public static final int SYSTEM_FATHER_ID = 0;

	/**
	 */
	public static final int IS_USED = 1;

	/**
	 * 登陸用戶
	 */
	public static final String KEY_LOGIN_USER = "sessionUser";

	/**
	 * 登陸用戶類型
	 */
	public static final String KEY_LOGIN_USER_TYPE = "sessionUserType";

	/**
	 * 登陸用戶類型 教師
	 */
	public static final String LOGIN_TEACHER = "teacher";

	/**
	 * 登陸用戶類型 机构
	 */
	public static final String LOGIN_ORGANIAZTION = "organization";

	/**
	 */
	public static final String KEY_LOGIN_CHECKCODE = "loginCheckCode";

	/**
	 * 密钥串
	 */
	public static final String KEY_STRING = "JSPT_DLJK";

	/**
	 * 404
	 */
	public static final String FOUR = "404";
	
	
	/**
	 * 机构录入个人基本信息时创建的教师对象
	 */
	public static final String KEY_SESSION_TEACHER = "teacherInfo";
	public static final String SESSION_ACTION_TYPE = "actionType";
	
}